/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.integration.sapcatalogue.setup;

import com.waters.hybris.integration.sapcatalogue.constants.WatersintegrationsapcatalogueConstants;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;

import java.util.List;

@SystemSetup(extension = WatersintegrationsapcatalogueConstants.EXTENSIONNAME)
public class WatersintegrationsapcatalogueSystemSetup extends AbstractSystemSetup {

    @Override
    public List<SystemSetupParameter> getInitializationOptions() {
        return null;
    }

    @SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
    public void createProjectData(final SystemSetupContext context) {
        importImpexFile(context, "/watersintegrationsapcatalogue/import/cronjobs.impex", true);
    }
}
