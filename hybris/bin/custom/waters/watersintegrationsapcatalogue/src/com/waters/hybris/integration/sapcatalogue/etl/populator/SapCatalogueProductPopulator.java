package com.waters.hybris.integration.sapcatalogue.etl.populator;

import com.sap.document.sap.rfc.functions.ZCATPRODUCTS;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.strategies.ProductImageResolutionStrategy;
import de.hybris.platform.catalog.model.KeywordModel;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class SapCatalogueProductPopulator implements Populator<WatersProductModel, ZCATPRODUCTS>
{
	private static final Logger LOG = LoggerFactory.getLogger(SapCatalogueProductPopulator.class);

	private static final String WATERS_CORPORATION = "Waters Corporation";
	private static final String EA = "EA";
	private static final String ANY_METHOD = "Any Method";
	private static final String THREE = "3";
	private static final String SPACE = " ";
	private static final String SLASH = "/";
	private static final String COMMA = ",";

	private ProductImageResolutionStrategy productImageResolutionStrategy;
	private ConfigurationService configurationService;

	@SuppressFBWarnings(justification = "Ignore the java reflect findbugs warning")
	@Override
	public void populate(final WatersProductModel source, final ZCATPRODUCTS target)
	{
		if (CollectionUtils.isEmpty(source.getClassificationClasses()))
		{
			LOG.error("Unclassified product received for data population: " + source.getCode());
			return;
		}
		final Map<String, String> featureValuesMap = new HashMap<>();
		collateProductFeatures(source, featureValuesMap);
		collateAdditionalFields(source, featureValuesMap);

		for (final Field field : target.getClass().getDeclaredFields())
		{
			final String fieldMappedValue = getFieldMappedValue(field.getName());
			if (StringUtils.isNotEmpty(fieldMappedValue) && fieldMappedValue.contains(COMMA))
			{
				final String[] fieldInfo = fieldMappedValue.split(COMMA);
				final String fieldValue = featureValuesMap.get(fieldInfo[0]);
				if (StringUtils.isNotEmpty(fieldValue))
				{
					final String truncatedFieldValue = fieldValue.substring(0, Math.min(fieldValue.length(), Integer.parseInt(fieldInfo[1])));
					try
					{
						field.setAccessible(true);
						field.set(target, truncatedFieldValue);
					}
					catch (final IllegalAccessException | IllegalArgumentException e)
					{
						LOG.error("Error setting field " + field.getName(), e);
					}
				}
			}
		}
	}

	protected String getFieldMappedValue(final String fieldName)
	{
		return getConfigurationService().getConfiguration().getString("waters.sapcatalogue.mapping." + fieldName);
	}

	private void collateAdditionalFields(final WatersProductModel source, final Map<String, String> featureValuesMap)
	{
		final Optional<String> materialValue = getMaterialValue(featureValuesMap, "PlateMaterial", "filterHousingMaterial", "SPEMaterial", "SmallComponentsMaterial");
		materialValue.ifPresent(s -> featureValuesMap.put("MaterialValue", s));
		if (StringUtils.isNotEmpty(featureValuesMap.get("ColumnpHRangeMin")) && StringUtils.isNotEmpty(featureValuesMap.get("ColumnpHRangeMax")))
		{
			featureValuesMap.put("ColumnpHRange", featureValuesMap.get("ColumnpHRangeMin") + " " + featureValuesMap.get("ColumnpHRangeMax"));
		}
		if (StringUtils.isNotEmpty(featureValuesMap.get("ColumnMolecularWeightRangeMin")) && StringUtils.isNotEmpty(featureValuesMap.get("ColumnMolecularWeightRangeMax")))
		{
			featureValuesMap
				.put("ColumnMolecularWeightRange", featureValuesMap.get("ColumnMolecularWeightRangeMin") + " - " + featureValuesMap.get("ColumnMolecularWeightRangeMax"));
		}
		featureValuesMap.put("PkGuom", EA);
		featureValuesMap.put("Fgname", WATERS_CORPORATION);
		featureValuesMap.put("ShipMeth", ANY_METHOD);
		featureValuesMap.put("LeadTime", THREE);
		featureValuesMap.put("ProdDesc", source.getSummary(Locale.ENGLISH));
		featureValuesMap.put("ProductCode", source.getCode());
		featureValuesMap.put("Fgpn", source.getCode());
		featureValuesMap.put("DynamicName", source.getDynamicName());
		featureValuesMap.put("Category", source.getClassificationClasses().get(0).getName());

		addImageValues(source, featureValuesMap);
		addKeywords(source, featureValuesMap);

		featureValuesMap.put("TechDataUrl", getTechDataUrl(source));
	}

	private void addKeywords(final WatersProductModel source, final Map<String, String> featureValuesMap)
	{
		if (CollectionUtils.isEmpty(source.getKeywords()))
		{
			return;
		}
		final String keywords = source.getKeywords().stream().map(KeywordModel::getKeyword).collect(Collectors.joining(COMMA));
		featureValuesMap.put("Keywords", keywords);
	}

	private void addImageValues(final WatersProductModel source, final Map<String, String> featureValuesMap)
	{
		final String primaryImageUrl = getProductImageResolutionStrategy().resolveImageUrl(source.getPrimaryImage(), Locale.ENGLISH);
		featureValuesMap.put("ImgUrl", primaryImageUrl);
		if (StringUtils.isNotEmpty(primaryImageUrl) && primaryImageUrl.contains(SLASH))
		{
			featureValuesMap.put("ImgName", primaryImageUrl.substring(primaryImageUrl.lastIndexOf(SLASH) + 1));
		}
	}

	protected String getTechDataUrl(final WatersProductModel source)
	{
		return getConfigurationService().getConfiguration().getString("waters.sapcatalogue.techdataurl") + source.getCode();
	}

	private Optional<String> getMaterialValue(final Map<String, String> featureValuesMap, final String... fields)
	{
		for (final String field : fields)
		{
			if (StringUtils.isNotEmpty(featureValuesMap.get(field)))
			{
				return Optional.ofNullable(featureValuesMap.get(field));
			}
		}
		return Optional.empty();
	}

	private void collateProductFeatures(final WatersProductModel watersProductModel, final Map<String, String> featureValuesMap)
	{
		watersProductModel.getFeatures().forEach(feature ->
		{
			final ClassAttributeAssignmentModel classificationAttributeAssignment = feature.getClassificationAttributeAssignment();
			if (classificationAttributeAssignment != null && classificationAttributeAssignment.getClassificationAttribute() != null)
			{
				processFeature(featureValuesMap, feature, classificationAttributeAssignment);
			}
		});
	}

	private void processFeature(final Map<String, String> featureValuesMap, final ProductFeatureModel feature, final ClassAttributeAssignmentModel classificationAttributeAssignment)
	{
		final String featureCode = classificationAttributeAssignment.getClassificationAttribute().getCode();
		if (feature.getValue() instanceof ClassificationAttributeValueModel)
		{
			final ClassificationAttributeValueModel classificationAttributeValueModel = (ClassificationAttributeValueModel) feature.getValue();
			final String featureValue = classificationAttributeValueModel.getName();
			collateProductFeature(featureValuesMap, feature, featureCode, featureValue);
		}
		else
		{
			final String featureValue = feature.getValue().toString();
			collateProductFeature(featureValuesMap, feature, featureCode, featureValue);
		}
	}

	private void collateProductFeature(final Map<String, String> featureValuesMap, final ProductFeatureModel feature, final String featureCode, final String featureValue)
	{
		if (StringUtils.isEmpty(featureValue))
		{
			return;
		}
		if (feature.getUnit() != null)
		{
			collateFeatureValue(featureValuesMap, featureCode, featureValue + SPACE + feature.getUnit().getSymbol());
		}
		else
		{
			collateFeatureValue(featureValuesMap, featureCode, featureValue);
		}
	}

	private void collateFeatureValue(final Map<String, String> featureValuesMap, final String featureCode, final String featureValue)
	{
		if (StringUtils.isNotEmpty(featureValuesMap.get(featureCode)))
		{
			featureValuesMap.put(featureCode, featureValuesMap.get(featureCode) + COMMA + featureValue);
		}
		else
		{
			featureValuesMap.put(featureCode, featureValue);
		}
	}

	public ProductImageResolutionStrategy getProductImageResolutionStrategy()
	{
		return productImageResolutionStrategy;
	}

	@Required
	public void setProductImageResolutionStrategy(final ProductImageResolutionStrategy productImageResolutionStrategy)
	{
		this.productImageResolutionStrategy = productImageResolutionStrategy;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
