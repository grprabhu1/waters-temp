package com.waters.hybris.integration.sapcatalogue.service.impl;

import com.sap.document.sap.rfc.functions.ZHYBRISTOSAP_Service;
import com.waters.hybris.integration.sapcatalogue.service.SapCatalogueService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;

public class WatersSapCatalogueService implements SapCatalogueService
{
	private static final Logger LOG = LoggerFactory.getLogger(WatersSapCatalogueService.class);

	private ConfigurationService configurationService;

	@Override
	public ZHYBRISTOSAP_Service getSapCatalogueWebservice()
	{
		Authenticator.setDefault(new Authenticator()
		{
			@Override
			protected PasswordAuthentication getPasswordAuthentication()
			{
				return new PasswordAuthentication(getConfigurationService().getConfiguration().getString("waters.sapcatalogue.wsdl.username"),
					getConfigurationService().getConfiguration().getString("waters.sapcatalogue.wsdl.password").toCharArray());
			}
		});

		try
		{
			return new ZHYBRISTOSAP_Service(new URL(getConfigurationService().getConfiguration().getString("waters.sapcatalogue.wsdl.location")));
		}
		catch (final MalformedURLException e)
		{
			LOG.error("Error in getting wsdl", e);
			return null;
		}
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
