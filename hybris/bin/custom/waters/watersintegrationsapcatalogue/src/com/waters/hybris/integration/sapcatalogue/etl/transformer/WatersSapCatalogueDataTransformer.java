package com.waters.hybris.integration.sapcatalogue.etl.transformer;

import com.sap.document.sap.rfc.functions.TABLEOFZCATPRODUCTS;
import com.sap.document.sap.rfc.functions.TABLEOFZSTPRDCAT;
import com.sap.document.sap.rfc.functions.ZCATPRODUCTS;
import com.sap.document.sap.rfc.functions.ZLNLOADPRDCATALOGUE;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.integration.core.etl.transformer.DefaultDataTransformer;
import de.hybris.platform.converters.Populator;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public class WatersSapCatalogueDataTransformer extends DefaultDataTransformer<List<WatersProductModel>, ZLNLOADPRDCATALOGUE> {

    private Populator<WatersProductModel, ZCATPRODUCTS> sapCatalogueProductPopulator;

    @Override
    public ZLNLOADPRDCATALOGUE transform(final List<WatersProductModel> watersProductModels) {
        final ZLNLOADPRDCATALOGUE zlnloadprdcatalogue = new ZLNLOADPRDCATALOGUE();
        zlnloadprdcatalogue.setITRETURN(new TABLEOFZSTPRDCAT());
        zlnloadprdcatalogue.setITPRDCAT(new TABLEOFZCATPRODUCTS());

        watersProductModels.forEach(watersProductModel -> {
            final ZCATPRODUCTS zcatproducts = new ZCATPRODUCTS();
            getSapCatalogueProductPopulator().populate(watersProductModel, zcatproducts);
            zlnloadprdcatalogue.getITPRDCAT().getItem().add(zcatproducts);
        });
        return zlnloadprdcatalogue;
    }

    public Populator<WatersProductModel, ZCATPRODUCTS> getSapCatalogueProductPopulator() {
        return sapCatalogueProductPopulator;
    }

    @Required
    public void setSapCatalogueProductPopulator(final Populator<WatersProductModel, ZCATPRODUCTS> sapCatalogueProductPopulator) {
        this.sapCatalogueProductPopulator = sapCatalogueProductPopulator;
    }
}
