package com.waters.hybris.integration.sapcatalogue.etl.producer;

import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import com.neoworks.hybris.etl.producers.DataProducer;
import com.waters.hybris.core.model.model.WatersProductModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.paginated.PaginatedFlexibleSearchParameter;
import de.hybris.platform.servicelayer.search.paginated.PaginatedFlexibleSearchService;
import de.hybris.platform.servicelayer.search.paginated.util.PaginatedSearchUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public class WatersSapCatalogueDataProducer implements DataProducer<List<WatersProductModel>> {
	private static final int DEFAULT_BATCH_SIZE = 50;
	private static final String PAGE_NUMBER = "pageNumber";
	private static final String WATERS_SAPCATALOGUE_BATCHSIZE = "waters.sapcatalogue.batchsize";

	private PaginatedFlexibleSearchService paginatedFlexibleSearchService;
	private String loadItemsQuery;
	private ConfigurationService configurationService;
	private EtlContext context;
	private SystemArea systemArea = SystemArea.ETL;

	@Override
	public List<WatersProductModel> get() {
		final Integer pageNumber = (Integer) getContext().getParameters().get(PAGE_NUMBER);
		getContext().getParameters().put(PAGE_NUMBER, pageNumber + 1);
		final int pageSize = getConfigurationService().getConfiguration().getInt(WATERS_SAPCATALOGUE_BATCHSIZE, DEFAULT_BATCH_SIZE);
		final SearchPageData<WatersProductModel> searchPageData = PaginatedSearchUtils.createSearchPageDataWithPagination(pageSize, pageNumber, false);
		final PaginatedFlexibleSearchParameter parameter = new PaginatedFlexibleSearchParameter();
		final FlexibleSearchQuery query = new FlexibleSearchQuery(getLoadItemsQuery(), getContext().getParameters());
		parameter.setFlexibleSearchQuery(query);
		parameter.setSearchPageData(searchPageData);
		final SearchPageData<WatersProductModel> paginatedSearchPageData = getPaginatedFlexibleSearchService().search(parameter);
		if (CollectionUtils.isEmpty(paginatedSearchPageData.getResults())) {
			return null;
		}
		getContext().getAuditService().audit(getSystemArea(), Status.SUCCESS, "Data producer has found {} items to produce for {}", paginatedSearchPageData.getResults().size(), getContext().getActivityDescription());
		return paginatedSearchPageData.getResults();
	}

	@Override
	public void init(final EtlContext etlContext) throws EtlValidationException {
		this.context = etlContext;
		getContext().getParameters().putIfAbsent(PAGE_NUMBER, 0);
	}

	public EtlContext getContext() {
		return context;
	}

	@Override
	public void start() throws EtlTransformationException {
		// empty
	}

	@Override
	public void end(final boolean b) throws EtlTransformationException {
		// empty
	}

	public PaginatedFlexibleSearchService getPaginatedFlexibleSearchService() {
		return paginatedFlexibleSearchService;
	}

	@Required
	public void setPaginatedFlexibleSearchService(final PaginatedFlexibleSearchService paginatedFlexibleSearchService) {
		this.paginatedFlexibleSearchService = paginatedFlexibleSearchService;
	}

	public String getLoadItemsQuery() {
		return loadItemsQuery;
	}

	@Required
	public void setLoadItemsQuery(final String loadItemsQuery) {
		this.loadItemsQuery = loadItemsQuery;
	}

	public ConfigurationService getConfigurationService() {
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	public SystemArea getSystemArea() {
		return systemArea;
	}

	@Required
	public void setSystemArea(final SystemArea systemArea) {
		this.systemArea = systemArea;
	}
}
