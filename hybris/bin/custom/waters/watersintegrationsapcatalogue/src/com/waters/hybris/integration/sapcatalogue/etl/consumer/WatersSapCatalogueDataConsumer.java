package com.waters.hybris.integration.sapcatalogue.etl.consumer;

import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.etl.consumers.DataConsumer;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.sap.document.sap.rfc.functions.TABLEOFZCATPRODUCTS;
import com.sap.document.sap.rfc.functions.TABLEOFZSTPRDCAT;
import com.sap.document.sap.rfc.functions.ZCATPRODUCTS;
import com.sap.document.sap.rfc.functions.ZHYBRISTOSAP_Service;
import com.sap.document.sap.rfc.functions.ZLNLOADPRDCATALOGUE;
import com.sap.document.sap.rfc.functions.ZLNLOADPRDCATALOGUEException;
import com.waters.hybris.integration.sapcatalogue.service.impl.WatersSapCatalogueService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.ws.Holder;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

public class WatersSapCatalogueDataConsumer implements DataConsumer<ZLNLOADPRDCATALOGUE>
{
	private static final Logger LOG = LoggerFactory.getLogger(WatersSapCatalogueDataConsumer.class);
	private static final String IDELTABLE = "1";

	private WatersSapCatalogueService watersSapCatalogueService;
	private ConfigurationService configurationService;
	private EtlContext context;
	private SystemArea systemArea = SystemArea.ETL;
	private ZHYBRISTOSAP_Service zhybristosapService = null;

	@SuppressFBWarnings(value = "REC_CATCH_EXCEPTION", justification = "We want to catch exceptions to so stop the export from stopping.")
	@Override
	public void put(final ZLNLOADPRDCATALOGUE zlnloadprdcatalogue)
	{
		try
		{
			this.logWsRequest(zlnloadprdcatalogue);
			if (this.getZhybristosapService() == null)
			{
				LOG.error("Unable to get Sap catalogue web service");
				this.getContext().getAuditService().audit(this.getSystemArea(), Status.FAILURE, "Unable to get Sap catalogue web service");
				return;
			}
			this.getZhybristosapService().getZhybristosap().zlnLOADPRDCATALOGUE(new Holder<>(zlnloadprdcatalogue.getITPRDCAT()),
				new Holder<>(zlnloadprdcatalogue.getITRETURN()), zlnloadprdcatalogue.getIDELTABLE());
		}
		catch (final Exception e)
		{
			zlnloadprdcatalogue.getITPRDCAT().getItem().forEach(watersProduct ->
			{
				LOG.error("Error sending product code to sap catalogue web service: " + watersProduct.getMATNR(), e.getMessage());
				this.getContext().getAuditService().audit(this.getSystemArea(), Status.FAILURE, "Error sending product code to sap catalogue web service: " + watersProduct.getMATNR());
			});
		}
	}

	@SuppressFBWarnings(value = "REC_CATCH_EXCEPTION", justification = "We want to catch exceptions to so stop the export from stopping.")
	@Override
	public void init(final EtlContext etlContext)
	{
		this.context = etlContext;
		try
		{
			LOG.info("Calling SAP Catalogue Delete");
			doSAPDeleteCall();
		}
		catch (final ZLNLOADPRDCATALOGUEException e)
		{
			LOG.error("Product catalogue exception while making the delete call", e);
		}
		catch (final Exception e)
		{
			LOG.error("Unable to locate sap catalogue web service", e);
		}
	}

	private void doSAPDeleteCall() throws ZLNLOADPRDCATALOGUEException
	{
		final ZLNLOADPRDCATALOGUE zlnloadprdcatalogue = new ZLNLOADPRDCATALOGUE();
		final TABLEOFZCATPRODUCTS tableofzcatproducts = new TABLEOFZCATPRODUCTS();
		tableofzcatproducts.getItem().add(new ZCATPRODUCTS());
		zlnloadprdcatalogue.setITPRDCAT(tableofzcatproducts);
		zlnloadprdcatalogue.setITRETURN(new TABLEOFZSTPRDCAT());
		zlnloadprdcatalogue.setIDELTABLE(IDELTABLE);
		logWsRequest(zlnloadprdcatalogue);
		setZhybristosapService(this.getWatersSapCatalogueService().getSapCatalogueWebservice());
		this.getZhybristosapService().getZhybristosap().zlnLOADPRDCATALOGUE(new Holder<>(zlnloadprdcatalogue.getITPRDCAT()),
			new Holder<>(zlnloadprdcatalogue.getITRETURN()), zlnloadprdcatalogue.getIDELTABLE());
	}

	@Override
	public void start() throws EtlTransformationException
	{
		// empty
	}

	@Override
	public void end(final boolean b) throws EtlTransformationException
	{
		// empty
	}

	private void logWsRequest(final ZLNLOADPRDCATALOGUE zlnloadprdcatalogue)
	{
		if (!this.getConfigurationService().getConfiguration().getBoolean("waters.sapcatalogue.logwsrequest", false))
		{
			return;
		}
		try
		{
			final JAXBContext context = JAXBContext.newInstance(zlnloadprdcatalogue.getClass());
			final Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			final OutputStream outputStream = new ByteArrayOutputStream();
			marshaller.marshal(zlnloadprdcatalogue, outputStream);
			LOG.debug(outputStream.toString());
		}
		catch (final JAXBException e)
		{
			LOG.error("Error in logging request object", e);
		}
	}

	public EtlContext getContext()
	{
		return this.context;
	}

	public WatersSapCatalogueService getWatersSapCatalogueService()
	{
		return this.watersSapCatalogueService;
	}

	@Required
	public void setWatersSapCatalogueService(final WatersSapCatalogueService watersSapCatalogueService)
	{
		this.watersSapCatalogueService = watersSapCatalogueService;
	}

	public ConfigurationService getConfigurationService()
	{
		return this.configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public SystemArea getSystemArea()
	{
		return this.systemArea;
	}

	@Required
	public void setSystemArea(final SystemArea systemArea)
	{
		this.systemArea = systemArea;
	}

	public ZHYBRISTOSAP_Service getZhybristosapService()
	{
		return this.zhybristosapService;
	}

	public void setZhybristosapService(final ZHYBRISTOSAP_Service zhybristosapService)
	{
		this.zhybristosapService = zhybristosapService;
	}
}
