package com.waters.hybris.integration.sapcatalogue.service;

import com.sap.document.sap.rfc.functions.ZHYBRISTOSAP_Service;

public interface SapCatalogueService
{
	ZHYBRISTOSAP_Service getSapCatalogueWebservice();
}
