
package com.sap.document.sap.rfc.functions;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ZCAT_PRODUCTS complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ZCAT_PRODUCTS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MANDT" type="{urn:sap-com:document:sap:rfc:functions}clnt3"/>
 *         &lt;element name="MATNR" type="{urn:sap-com:document:sap:rfc:functions}char18"/>
 *         &lt;element name="ZCATEGORY" type="{urn:sap-com:document:sap:rfc:functions}char75"/>
 *         &lt;element name="ZZUNSPSC" type="{urn:sap-com:document:sap:rfc:functions}char12"/>
 *         &lt;element name="ZEN_MAKTX" type="{urn:sap-com:document:sap:rfc:functions}char255"/>
 *         &lt;element name="ZSEARCH" type="{urn:sap-com:document:sap:rfc:functions}char1024"/>
 *         &lt;element name="ZPKG_UOM" type="{urn:sap-com:document:sap:rfc:functions}unit3"/>
 *         &lt;element name="ZIMG_NAME" type="{urn:sap-com:document:sap:rfc:functions}char60"/>
 *         &lt;element name="ZIMG_URL" type="{urn:sap-com:document:sap:rfc:functions}char255"/>
 *         &lt;element name="ZMFG_NAME" type="{urn:sap-com:document:sap:rfc:functions}char35"/>
 *         &lt;element name="ZMFG_PN" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZSEPARATION" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZEND_CAPPED" type="{urn:sap-com:document:sap:rfc:functions}char15"/>
 *         &lt;element name="ZINNER_DIA" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZSHIP_METH" type="{urn:sap-com:document:sap:rfc:functions}char20"/>
 *         &lt;element name="ZSURFACE_AREA" type="{urn:sap-com:document:sap:rfc:functions}char10"/>
 *         &lt;element name="ZTECH_DATA_URL" type="{urn:sap-com:document:sap:rfc:functions}char255"/>
 *         &lt;element name="ZTRADE_NAME" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZLEAD_TIME" type="{urn:sap-com:document:sap:rfc:functions}char3"/>
 *         &lt;element name="ZCARBON_LOAD" type="{urn:sap-com:document:sap:rfc:functions}char10"/>
 *         &lt;element name="ZBASE_MAT" type="{urn:sap-com:document:sap:rfc:functions}char25"/>
 *         &lt;element name="ZPARTICLE_SHAPE" type="{urn:sap-com:document:sap:rfc:functions}char10"/>
 *         &lt;element name="ZPARTICLE_SIZE" type="{urn:sap-com:document:sap:rfc:functions}char32"/>
 *         &lt;element name="ZPHASE" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZLENGTH" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZPORE_SIZE" type="{urn:sap-com:document:sap:rfc:functions}char32"/>
 *         &lt;element name="ZPH_RANGE" type="{urn:sap-com:document:sap:rfc:functions}char15"/>
 *         &lt;element name="ZFORMAT" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZGLASS_TYP" type="{urn:sap-com:document:sap:rfc:functions}char25"/>
 *         &lt;element name="ZSAMPLE_VOL" type="{urn:sap-com:document:sap:rfc:functions}char15"/>
 *         &lt;element name="ZHOLD_UP_VOL" type="{urn:sap-com:document:sap:rfc:functions}char15"/>
 *         &lt;element name="ZCLOSURE_TYP" type="{urn:sap-com:document:sap:rfc:functions}char15"/>
 *         &lt;element name="ZSORBENT_WGHT" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZDIMENSION" type="{urn:sap-com:document:sap:rfc:functions}char32"/>
 *         &lt;element name="ZPACK_SIZE" type="{urn:sap-com:document:sap:rfc:functions}char32"/>
 *         &lt;element name="ZMOL_WGHT_RANGE" type="{urn:sap-com:document:sap:rfc:functions}char25"/>
 *         &lt;element name="ZCAP_TYP" type="{urn:sap-com:document:sap:rfc:functions}char15"/>
 *         &lt;element name="ZCOLMN_INNER_DIA" type="{urn:sap-com:document:sap:rfc:functions}char15"/>
 *         &lt;element name="ZCOLMN_LENGTH" type="{urn:sap-com:document:sap:rfc:functions}char15"/>
 *         &lt;element name="ZFILTER_DIA" type="{urn:sap-com:document:sap:rfc:functions}char10"/>
 *         &lt;element name="ZION_XCHG_CPTY" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZLIGAND" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZVIAL_CAPCITY" type="{urn:sap-com:document:sap:rfc:functions}char10"/>
 *         &lt;element name="ZFILTER_SUPPORT" type="{urn:sap-com:document:sap:rfc:functions}char20"/>
 *         &lt;element name="ZFILTER_MAT" type="{urn:sap-com:document:sap:rfc:functions}char20"/>
 *         &lt;element name="ZPRDT_FAMILY" type="{urn:sap-com:document:sap:rfc:functions}char20"/>
 *         &lt;element name="ZCERTIFICATION" type="{urn:sap-com:document:sap:rfc:functions}char50"/>
 *         &lt;element name="ZCOMPATIBLE" type="{urn:sap-com:document:sap:rfc:functions}char1024"/>
 *         &lt;element name="ZMATL_OF_CONSTRT" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZOUTER_DIA" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZFITTING_TYPE" type="{urn:sap-com:document:sap:rfc:functions}char25"/>
 *         &lt;element name="ZAPPS" type="{urn:sap-com:document:sap:rfc:functions}char50"/>
 *         &lt;element name="ZDETECTORS" type="{urn:sap-com:document:sap:rfc:functions}char160"/>
 *         &lt;element name="ZPROD_TYPE" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZSOLVENTS" type="{urn:sap-com:document:sap:rfc:functions}char50"/>
 *         &lt;element name="VMSTA" type="{urn:sap-com:document:sap:rfc:functions}char2"/>
 *         &lt;element name="ZEX_MAKTX" type="{urn:sap-com:document:sap:rfc:functions}char255"/>
 *         &lt;element name="ZBARREL_SIZE" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZBONDING_TECH" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZCOLOR" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZCONCENTRATION" type="{urn:sap-com:document:sap:rfc:functions}char60"/>
 *         &lt;element name="ZGPC_BRAND" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZGPC_MODE" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZGPC_STANDARD" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZHARDWARE_TYPE" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZINST_MODEL" type="{urn:sap-com:document:sap:rfc:functions}char150"/>
 *         &lt;element name="ZMASS_SPEC_COM" type="{urn:sap-com:document:sap:rfc:functions}char60"/>
 *         &lt;element name="ZMAX_INJ_VOL" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZMAX_RESI_VOL" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZPACKAGING" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZPM_KIT" type="{urn:sap-com:document:sap:rfc:functions}char3"/>
 *         &lt;element name="ZQC_TESTED" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZREPLACE_SCH" type="{urn:sap-com:document:sap:rfc:functions}char15"/>
 *         &lt;element name="ZSAMP_MATRIX" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZSEARCH_CAT" type="{urn:sap-com:document:sap:rfc:functions}char76"/>
 *         &lt;element name="ZSEPTA_TYPE" type="{urn:sap-com:document:sap:rfc:functions}char70"/>
 *         &lt;element name="ZSHIPP_FORM" type="{urn:sap-com:document:sap:rfc:functions}char50"/>
 *         &lt;element name="ZSILANOL_ACT" type="{urn:sap-com:document:sap:rfc:functions}char50"/>
 *         &lt;element name="ZSITE_CATEGORY" type="{urn:sap-com:document:sap:rfc:functions}char54"/>
 *         &lt;element name="Z_SIZE" type="{urn:sap-com:document:sap:rfc:functions}char50"/>
 *         &lt;element name="Z_STORAGE" type="{urn:sap-com:document:sap:rfc:functions}char50"/>
 *         &lt;element name="Z_TECHNOLOGY" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZTUB_FITTINGS" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="Z_USP_CLASS" type="{urn:sap-com:document:sap:rfc:functions}char16"/>
 *         &lt;element name="ZVIAL_TYPE" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ZVOLUME_CAP" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZWATER_WET" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZCARE_URL" type="{urn:sap-com:document:sap:rfc:functions}char500"/>
 *         &lt;element name="ZMSDS_URL" type="{urn:sap-com:document:sap:rfc:functions}char500"/>
 *         &lt;element name="ZSECOND_CHEMISTRY" type="{urn:sap-com:document:sap:rfc:functions}char40"/>
 *         &lt;element name="ZMETHODS" type="{urn:sap-com:document:sap:rfc:functions}char150"/>
 *         &lt;element name="ZPART_CAT" type="{urn:sap-com:document:sap:rfc:functions}char100"/>
 *         &lt;element name="ZSYSTEM" type="{urn:sap-com:document:sap:rfc:functions}char100"/>
 *         &lt;element name="ZPRD_DESC" type="{urn:sap-com:document:sap:rfc:functions}char1024"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZCAT_PRODUCTS", propOrder = {
    "mandt",
    "matnr",
    "zcategory",
    "zzunspsc",
    "zenmaktx",
    "zsearch",
    "zpkguom",
    "zimgname",
    "zimgurl",
    "zmfgname",
    "zmfgpn",
    "zseparation",
    "zendcapped",
    "zinnerdia",
    "zshipmeth",
    "zsurfacearea",
    "ztechdataurl",
    "ztradename",
    "zleadtime",
    "zcarbonload",
    "zbasemat",
    "zparticleshape",
    "zparticlesize",
    "zphase",
    "zlength",
    "zporesize",
    "zphrange",
    "zformat",
    "zglasstyp",
    "zsamplevol",
    "zholdupvol",
    "zclosuretyp",
    "zsorbentwght",
    "zdimension",
    "zpacksize",
    "zmolwghtrange",
    "zcaptyp",
    "zcolmninnerdia",
    "zcolmnlength",
    "zfilterdia",
    "zionxchgcpty",
    "zligand",
    "zvialcapcity",
    "zfiltersupport",
    "zfiltermat",
    "zprdtfamily",
    "zcertification",
    "zcompatible",
    "zmatlofconstrt",
    "zouterdia",
    "zfittingtype",
    "zapps",
    "zdetectors",
    "zprodtype",
    "zsolvents",
    "vmsta",
    "zexmaktx",
    "zbarrelsize",
    "zbondingtech",
    "zcolor",
    "zconcentration",
    "zgpcbrand",
    "zgpcmode",
    "zgpcstandard",
    "zhardwaretype",
    "zinstmodel",
    "zmassspeccom",
    "zmaxinjvol",
    "zmaxresivol",
    "zpackaging",
    "zpmkit",
    "zqctested",
    "zreplacesch",
    "zsampmatrix",
    "zsearchcat",
    "zseptatype",
    "zshippform",
    "zsilanolact",
    "zsitecategory",
    "zsize",
    "zstorage",
    "ztechnology",
    "ztubfittings",
    "zuspclass",
    "zvialtype",
    "zvolumecap",
    "zwaterwet",
    "zcareurl",
    "zmsdsurl",
    "zsecondchemistry",
    "zmethods",
    "zpartcat",
    "zsystem",
    "zprddesc"
})
@SuppressWarnings("PMD")
@SuppressFBWarnings
public class ZCATPRODUCTS {

    @XmlElement(name = "MANDT", required = true)
    protected String mandt;
    @XmlElement(name = "MATNR", required = true)
    protected String matnr;
    @XmlElement(name = "ZCATEGORY", required = true)
    protected String zcategory;
    @XmlElement(name = "ZZUNSPSC", required = true)
    protected String zzunspsc;
    @XmlElement(name = "ZEN_MAKTX", required = true)
    protected String zenmaktx;
    @XmlElement(name = "ZSEARCH", required = true)
    protected String zsearch;
    @XmlElement(name = "ZPKG_UOM", required = true)
    protected String zpkguom;
    @XmlElement(name = "ZIMG_NAME", required = true)
    protected String zimgname;
    @XmlElement(name = "ZIMG_URL", required = true)
    protected String zimgurl;
    @XmlElement(name = "ZMFG_NAME", required = true)
    protected String zmfgname;
    @XmlElement(name = "ZMFG_PN", required = true)
    protected String zmfgpn;
    @XmlElement(name = "ZSEPARATION", required = true)
    protected String zseparation;
    @XmlElement(name = "ZEND_CAPPED", required = true)
    protected String zendcapped;
    @XmlElement(name = "ZINNER_DIA", required = true)
    protected String zinnerdia;
    @XmlElement(name = "ZSHIP_METH", required = true)
    protected String zshipmeth;
    @XmlElement(name = "ZSURFACE_AREA", required = true)
    protected String zsurfacearea;
    @XmlElement(name = "ZTECH_DATA_URL", required = true)
    protected String ztechdataurl;
    @XmlElement(name = "ZTRADE_NAME", required = true)
    protected String ztradename;
    @XmlElement(name = "ZLEAD_TIME", required = true)
    protected String zleadtime;
    @XmlElement(name = "ZCARBON_LOAD", required = true)
    protected String zcarbonload;
    @XmlElement(name = "ZBASE_MAT", required = true)
    protected String zbasemat;
    @XmlElement(name = "ZPARTICLE_SHAPE", required = true)
    protected String zparticleshape;
    @XmlElement(name = "ZPARTICLE_SIZE", required = true)
    protected String zparticlesize;
    @XmlElement(name = "ZPHASE", required = true)
    protected String zphase;
    @XmlElement(name = "ZLENGTH", required = true)
    protected String zlength;
    @XmlElement(name = "ZPORE_SIZE", required = true)
    protected String zporesize;
    @XmlElement(name = "ZPH_RANGE", required = true)
    protected String zphrange;
    @XmlElement(name = "ZFORMAT", required = true)
    protected String zformat;
    @XmlElement(name = "ZGLASS_TYP", required = true)
    protected String zglasstyp;
    @XmlElement(name = "ZSAMPLE_VOL", required = true)
    protected String zsamplevol;
    @XmlElement(name = "ZHOLD_UP_VOL", required = true)
    protected String zholdupvol;
    @XmlElement(name = "ZCLOSURE_TYP", required = true)
    protected String zclosuretyp;
    @XmlElement(name = "ZSORBENT_WGHT", required = true)
    protected String zsorbentwght;
    @XmlElement(name = "ZDIMENSION", required = true)
    protected String zdimension;
    @XmlElement(name = "ZPACK_SIZE", required = true)
    protected String zpacksize;
    @XmlElement(name = "ZMOL_WGHT_RANGE", required = true)
    protected String zmolwghtrange;
    @XmlElement(name = "ZCAP_TYP", required = true)
    protected String zcaptyp;
    @XmlElement(name = "ZCOLMN_INNER_DIA", required = true)
    protected String zcolmninnerdia;
    @XmlElement(name = "ZCOLMN_LENGTH", required = true)
    protected String zcolmnlength;
    @XmlElement(name = "ZFILTER_DIA", required = true)
    protected String zfilterdia;
    @XmlElement(name = "ZION_XCHG_CPTY", required = true)
    protected String zionxchgcpty;
    @XmlElement(name = "ZLIGAND", required = true)
    protected String zligand;
    @XmlElement(name = "ZVIAL_CAPCITY", required = true)
    protected String zvialcapcity;
    @XmlElement(name = "ZFILTER_SUPPORT", required = true)
    protected String zfiltersupport;
    @XmlElement(name = "ZFILTER_MAT", required = true)
    protected String zfiltermat;
    @XmlElement(name = "ZPRDT_FAMILY", required = true)
    protected String zprdtfamily;
    @XmlElement(name = "ZCERTIFICATION", required = true)
    protected String zcertification;
    @XmlElement(name = "ZCOMPATIBLE", required = true)
    protected String zcompatible;
    @XmlElement(name = "ZMATL_OF_CONSTRT", required = true)
    protected String zmatlofconstrt;
    @XmlElement(name = "ZOUTER_DIA", required = true)
    protected String zouterdia;
    @XmlElement(name = "ZFITTING_TYPE", required = true)
    protected String zfittingtype;
    @XmlElement(name = "ZAPPS", required = true)
    protected String zapps;
    @XmlElement(name = "ZDETECTORS", required = true)
    protected String zdetectors;
    @XmlElement(name = "ZPROD_TYPE", required = true)
    protected String zprodtype;
    @XmlElement(name = "ZSOLVENTS", required = true)
    protected String zsolvents;
    @XmlElement(name = "VMSTA", required = true)
    protected String vmsta;
    @XmlElement(name = "ZEX_MAKTX", required = true)
    protected String zexmaktx;
    @XmlElement(name = "ZBARREL_SIZE", required = true)
    protected String zbarrelsize;
    @XmlElement(name = "ZBONDING_TECH", required = true)
    protected String zbondingtech;
    @XmlElement(name = "ZCOLOR", required = true)
    protected String zcolor;
    @XmlElement(name = "ZCONCENTRATION", required = true)
    protected String zconcentration;
    @XmlElement(name = "ZGPC_BRAND", required = true)
    protected String zgpcbrand;
    @XmlElement(name = "ZGPC_MODE", required = true)
    protected String zgpcmode;
    @XmlElement(name = "ZGPC_STANDARD", required = true)
    protected String zgpcstandard;
    @XmlElement(name = "ZHARDWARE_TYPE", required = true)
    protected String zhardwaretype;
    @XmlElement(name = "ZINST_MODEL", required = true)
    protected String zinstmodel;
    @XmlElement(name = "ZMASS_SPEC_COM", required = true)
    protected String zmassspeccom;
    @XmlElement(name = "ZMAX_INJ_VOL", required = true)
    protected String zmaxinjvol;
    @XmlElement(name = "ZMAX_RESI_VOL", required = true)
    protected String zmaxresivol;
    @XmlElement(name = "ZPACKAGING", required = true)
    protected String zpackaging;
    @XmlElement(name = "ZPM_KIT", required = true)
    protected String zpmkit;
    @XmlElement(name = "ZQC_TESTED", required = true)
    protected String zqctested;
    @XmlElement(name = "ZREPLACE_SCH", required = true)
    protected String zreplacesch;
    @XmlElement(name = "ZSAMP_MATRIX", required = true)
    protected String zsampmatrix;
    @XmlElement(name = "ZSEARCH_CAT", required = true)
    protected String zsearchcat;
    @XmlElement(name = "ZSEPTA_TYPE", required = true)
    protected String zseptatype;
    @XmlElement(name = "ZSHIPP_FORM", required = true)
    protected String zshippform;
    @XmlElement(name = "ZSILANOL_ACT", required = true)
    protected String zsilanolact;
    @XmlElement(name = "ZSITE_CATEGORY", required = true)
    protected String zsitecategory;
    @XmlElement(name = "Z_SIZE", required = true)
    protected String zsize;
    @XmlElement(name = "Z_STORAGE", required = true)
    protected String zstorage;
    @XmlElement(name = "Z_TECHNOLOGY", required = true)
    protected String ztechnology;
    @XmlElement(name = "ZTUB_FITTINGS", required = true)
    protected String ztubfittings;
    @XmlElement(name = "Z_USP_CLASS", required = true)
    protected String zuspclass;
    @XmlElement(name = "ZVIAL_TYPE", required = true)
    protected String zvialtype;
    @XmlElement(name = "ZVOLUME_CAP", required = true)
    protected String zvolumecap;
    @XmlElement(name = "ZWATER_WET", required = true)
    protected String zwaterwet;
    @XmlElement(name = "ZCARE_URL", required = true)
    protected String zcareurl;
    @XmlElement(name = "ZMSDS_URL", required = true)
    protected String zmsdsurl;
    @XmlElement(name = "ZSECOND_CHEMISTRY", required = true)
    protected String zsecondchemistry;
    @XmlElement(name = "ZMETHODS", required = true)
    protected String zmethods;
    @XmlElement(name = "ZPART_CAT", required = true)
    protected String zpartcat;
    @XmlElement(name = "ZSYSTEM", required = true)
    protected String zsystem;
    @XmlElement(name = "ZPRD_DESC", required = true)
    protected String zprddesc;

    /**
     * Gets the value of the mandt property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMANDT() {
        return mandt;
    }

    /**
     * Sets the value of the mandt property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMANDT(String value) {
        this.mandt = value;
    }

    /**
     * Gets the value of the matnr property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMATNR() {
        return matnr;
    }

    /**
     * Sets the value of the matnr property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMATNR(String value) {
        this.matnr = value;
    }

    /**
     * Gets the value of the zcategory property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZCATEGORY() {
        return zcategory;
    }

    /**
     * Sets the value of the zcategory property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZCATEGORY(String value) {
        this.zcategory = value;
    }

    /**
     * Gets the value of the zzunspsc property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZZUNSPSC() {
        return zzunspsc;
    }

    /**
     * Sets the value of the zzunspsc property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZZUNSPSC(String value) {
        this.zzunspsc = value;
    }

    /**
     * Gets the value of the zenmaktx property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZENMAKTX() {
        return zenmaktx;
    }

    /**
     * Sets the value of the zenmaktx property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZENMAKTX(String value) {
        this.zenmaktx = value;
    }

    /**
     * Gets the value of the zsearch property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSEARCH() {
        return zsearch;
    }

    /**
     * Sets the value of the zsearch property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSEARCH(String value) {
        this.zsearch = value;
    }

    /**
     * Gets the value of the zpkguom property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZPKGUOM() {
        return zpkguom;
    }

    /**
     * Sets the value of the zpkguom property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZPKGUOM(String value) {
        this.zpkguom = value;
    }

    /**
     * Gets the value of the zimgname property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZIMGNAME() {
        return zimgname;
    }

    /**
     * Sets the value of the zimgname property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZIMGNAME(String value) {
        this.zimgname = value;
    }

    /**
     * Gets the value of the zimgurl property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZIMGURL() {
        return zimgurl;
    }

    /**
     * Sets the value of the zimgurl property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZIMGURL(String value) {
        this.zimgurl = value;
    }

    /**
     * Gets the value of the zmfgname property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZMFGNAME() {
        return zmfgname;
    }

    /**
     * Sets the value of the zmfgname property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZMFGNAME(String value) {
        this.zmfgname = value;
    }

    /**
     * Gets the value of the zmfgpn property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZMFGPN() {
        return zmfgpn;
    }

    /**
     * Sets the value of the zmfgpn property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZMFGPN(String value) {
        this.zmfgpn = value;
    }

    /**
     * Gets the value of the zseparation property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSEPARATION() {
        return zseparation;
    }

    /**
     * Sets the value of the zseparation property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSEPARATION(String value) {
        this.zseparation = value;
    }

    /**
     * Gets the value of the zendcapped property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZENDCAPPED() {
        return zendcapped;
    }

    /**
     * Sets the value of the zendcapped property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZENDCAPPED(String value) {
        this.zendcapped = value;
    }

    /**
     * Gets the value of the zinnerdia property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZINNERDIA() {
        return zinnerdia;
    }

    /**
     * Sets the value of the zinnerdia property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZINNERDIA(String value) {
        this.zinnerdia = value;
    }

    /**
     * Gets the value of the zshipmeth property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSHIPMETH() {
        return zshipmeth;
    }

    /**
     * Sets the value of the zshipmeth property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSHIPMETH(String value) {
        this.zshipmeth = value;
    }

    /**
     * Gets the value of the zsurfacearea property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSURFACEAREA() {
        return zsurfacearea;
    }

    /**
     * Sets the value of the zsurfacearea property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSURFACEAREA(String value) {
        this.zsurfacearea = value;
    }

    /**
     * Gets the value of the ztechdataurl property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZTECHDATAURL() {
        return ztechdataurl;
    }

    /**
     * Sets the value of the ztechdataurl property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZTECHDATAURL(String value) {
        this.ztechdataurl = value;
    }

    /**
     * Gets the value of the ztradename property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZTRADENAME() {
        return ztradename;
    }

    /**
     * Sets the value of the ztradename property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZTRADENAME(String value) {
        this.ztradename = value;
    }

    /**
     * Gets the value of the zleadtime property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZLEADTIME() {
        return zleadtime;
    }

    /**
     * Sets the value of the zleadtime property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZLEADTIME(String value) {
        this.zleadtime = value;
    }

    /**
     * Gets the value of the zcarbonload property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZCARBONLOAD() {
        return zcarbonload;
    }

    /**
     * Sets the value of the zcarbonload property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZCARBONLOAD(String value) {
        this.zcarbonload = value;
    }

    /**
     * Gets the value of the zbasemat property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZBASEMAT() {
        return zbasemat;
    }

    /**
     * Sets the value of the zbasemat property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZBASEMAT(String value) {
        this.zbasemat = value;
    }

    /**
     * Gets the value of the zparticleshape property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZPARTICLESHAPE() {
        return zparticleshape;
    }

    /**
     * Sets the value of the zparticleshape property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZPARTICLESHAPE(String value) {
        this.zparticleshape = value;
    }

    /**
     * Gets the value of the zparticlesize property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZPARTICLESIZE() {
        return zparticlesize;
    }

    /**
     * Sets the value of the zparticlesize property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZPARTICLESIZE(String value) {
        this.zparticlesize = value;
    }

    /**
     * Gets the value of the zphase property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZPHASE() {
        return zphase;
    }

    /**
     * Sets the value of the zphase property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZPHASE(String value) {
        this.zphase = value;
    }

    /**
     * Gets the value of the zlength property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZLENGTH() {
        return zlength;
    }

    /**
     * Sets the value of the zlength property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZLENGTH(String value) {
        this.zlength = value;
    }

    /**
     * Gets the value of the zporesize property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZPORESIZE() {
        return zporesize;
    }

    /**
     * Sets the value of the zporesize property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZPORESIZE(String value) {
        this.zporesize = value;
    }

    /**
     * Gets the value of the zphrange property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZPHRANGE() {
        return zphrange;
    }

    /**
     * Sets the value of the zphrange property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZPHRANGE(String value) {
        this.zphrange = value;
    }

    /**
     * Gets the value of the zformat property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZFORMAT() {
        return zformat;
    }

    /**
     * Sets the value of the zformat property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZFORMAT(String value) {
        this.zformat = value;
    }

    /**
     * Gets the value of the zglasstyp property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZGLASSTYP() {
        return zglasstyp;
    }

    /**
     * Sets the value of the zglasstyp property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZGLASSTYP(String value) {
        this.zglasstyp = value;
    }

    /**
     * Gets the value of the zsamplevol property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSAMPLEVOL() {
        return zsamplevol;
    }

    /**
     * Sets the value of the zsamplevol property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSAMPLEVOL(String value) {
        this.zsamplevol = value;
    }

    /**
     * Gets the value of the zholdupvol property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZHOLDUPVOL() {
        return zholdupvol;
    }

    /**
     * Sets the value of the zholdupvol property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZHOLDUPVOL(String value) {
        this.zholdupvol = value;
    }

    /**
     * Gets the value of the zclosuretyp property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZCLOSURETYP() {
        return zclosuretyp;
    }

    /**
     * Sets the value of the zclosuretyp property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZCLOSURETYP(String value) {
        this.zclosuretyp = value;
    }

    /**
     * Gets the value of the zsorbentwght property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSORBENTWGHT() {
        return zsorbentwght;
    }

    /**
     * Sets the value of the zsorbentwght property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSORBENTWGHT(String value) {
        this.zsorbentwght = value;
    }

    /**
     * Gets the value of the zdimension property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZDIMENSION() {
        return zdimension;
    }

    /**
     * Sets the value of the zdimension property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZDIMENSION(String value) {
        this.zdimension = value;
    }

    /**
     * Gets the value of the zpacksize property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZPACKSIZE() {
        return zpacksize;
    }

    /**
     * Sets the value of the zpacksize property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZPACKSIZE(String value) {
        this.zpacksize = value;
    }

    /**
     * Gets the value of the zmolwghtrange property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZMOLWGHTRANGE() {
        return zmolwghtrange;
    }

    /**
     * Sets the value of the zmolwghtrange property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZMOLWGHTRANGE(String value) {
        this.zmolwghtrange = value;
    }

    /**
     * Gets the value of the zcaptyp property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZCAPTYP() {
        return zcaptyp;
    }

    /**
     * Sets the value of the zcaptyp property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZCAPTYP(String value) {
        this.zcaptyp = value;
    }

    /**
     * Gets the value of the zcolmninnerdia property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZCOLMNINNERDIA() {
        return zcolmninnerdia;
    }

    /**
     * Sets the value of the zcolmninnerdia property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZCOLMNINNERDIA(String value) {
        this.zcolmninnerdia = value;
    }

    /**
     * Gets the value of the zcolmnlength property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZCOLMNLENGTH() {
        return zcolmnlength;
    }

    /**
     * Sets the value of the zcolmnlength property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZCOLMNLENGTH(String value) {
        this.zcolmnlength = value;
    }

    /**
     * Gets the value of the zfilterdia property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZFILTERDIA() {
        return zfilterdia;
    }

    /**
     * Sets the value of the zfilterdia property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZFILTERDIA(String value) {
        this.zfilterdia = value;
    }

    /**
     * Gets the value of the zionxchgcpty property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZIONXCHGCPTY() {
        return zionxchgcpty;
    }

    /**
     * Sets the value of the zionxchgcpty property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZIONXCHGCPTY(String value) {
        this.zionxchgcpty = value;
    }

    /**
     * Gets the value of the zligand property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZLIGAND() {
        return zligand;
    }

    /**
     * Sets the value of the zligand property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZLIGAND(String value) {
        this.zligand = value;
    }

    /**
     * Gets the value of the zvialcapcity property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZVIALCAPCITY() {
        return zvialcapcity;
    }

    /**
     * Sets the value of the zvialcapcity property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZVIALCAPCITY(String value) {
        this.zvialcapcity = value;
    }

    /**
     * Gets the value of the zfiltersupport property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZFILTERSUPPORT() {
        return zfiltersupport;
    }

    /**
     * Sets the value of the zfiltersupport property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZFILTERSUPPORT(String value) {
        this.zfiltersupport = value;
    }

    /**
     * Gets the value of the zfiltermat property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZFILTERMAT() {
        return zfiltermat;
    }

    /**
     * Sets the value of the zfiltermat property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZFILTERMAT(String value) {
        this.zfiltermat = value;
    }

    /**
     * Gets the value of the zprdtfamily property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZPRDTFAMILY() {
        return zprdtfamily;
    }

    /**
     * Sets the value of the zprdtfamily property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZPRDTFAMILY(String value) {
        this.zprdtfamily = value;
    }

    /**
     * Gets the value of the zcertification property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZCERTIFICATION() {
        return zcertification;
    }

    /**
     * Sets the value of the zcertification property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZCERTIFICATION(String value) {
        this.zcertification = value;
    }

    /**
     * Gets the value of the zcompatible property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZCOMPATIBLE() {
        return zcompatible;
    }

    /**
     * Sets the value of the zcompatible property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZCOMPATIBLE(String value) {
        this.zcompatible = value;
    }

    /**
     * Gets the value of the zmatlofconstrt property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZMATLOFCONSTRT() {
        return zmatlofconstrt;
    }

    /**
     * Sets the value of the zmatlofconstrt property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZMATLOFCONSTRT(String value) {
        this.zmatlofconstrt = value;
    }

    /**
     * Gets the value of the zouterdia property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZOUTERDIA() {
        return zouterdia;
    }

    /**
     * Sets the value of the zouterdia property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZOUTERDIA(String value) {
        this.zouterdia = value;
    }

    /**
     * Gets the value of the zfittingtype property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZFITTINGTYPE() {
        return zfittingtype;
    }

    /**
     * Sets the value of the zfittingtype property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZFITTINGTYPE(String value) {
        this.zfittingtype = value;
    }

    /**
     * Gets the value of the zapps property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZAPPS() {
        return zapps;
    }

    /**
     * Sets the value of the zapps property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZAPPS(String value) {
        this.zapps = value;
    }

    /**
     * Gets the value of the zdetectors property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZDETECTORS() {
        return zdetectors;
    }

    /**
     * Sets the value of the zdetectors property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZDETECTORS(String value) {
        this.zdetectors = value;
    }

    /**
     * Gets the value of the zprodtype property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZPRODTYPE() {
        return zprodtype;
    }

    /**
     * Sets the value of the zprodtype property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZPRODTYPE(String value) {
        this.zprodtype = value;
    }

    /**
     * Gets the value of the zsolvents property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSOLVENTS() {
        return zsolvents;
    }

    /**
     * Sets the value of the zsolvents property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSOLVENTS(String value) {
        this.zsolvents = value;
    }

    /**
     * Gets the value of the vmsta property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getVMSTA() {
        return vmsta;
    }

    /**
     * Sets the value of the vmsta property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setVMSTA(String value) {
        this.vmsta = value;
    }

    /**
     * Gets the value of the zexmaktx property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZEXMAKTX() {
        return zexmaktx;
    }

    /**
     * Sets the value of the zexmaktx property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZEXMAKTX(String value) {
        this.zexmaktx = value;
    }

    /**
     * Gets the value of the zbarrelsize property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZBARRELSIZE() {
        return zbarrelsize;
    }

    /**
     * Sets the value of the zbarrelsize property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZBARRELSIZE(String value) {
        this.zbarrelsize = value;
    }

    /**
     * Gets the value of the zbondingtech property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZBONDINGTECH() {
        return zbondingtech;
    }

    /**
     * Sets the value of the zbondingtech property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZBONDINGTECH(String value) {
        this.zbondingtech = value;
    }

    /**
     * Gets the value of the zcolor property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZCOLOR() {
        return zcolor;
    }

    /**
     * Sets the value of the zcolor property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZCOLOR(String value) {
        this.zcolor = value;
    }

    /**
     * Gets the value of the zconcentration property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZCONCENTRATION() {
        return zconcentration;
    }

    /**
     * Sets the value of the zconcentration property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZCONCENTRATION(String value) {
        this.zconcentration = value;
    }

    /**
     * Gets the value of the zgpcbrand property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZGPCBRAND() {
        return zgpcbrand;
    }

    /**
     * Sets the value of the zgpcbrand property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZGPCBRAND(String value) {
        this.zgpcbrand = value;
    }

    /**
     * Gets the value of the zgpcmode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZGPCMODE() {
        return zgpcmode;
    }

    /**
     * Sets the value of the zgpcmode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZGPCMODE(String value) {
        this.zgpcmode = value;
    }

    /**
     * Gets the value of the zgpcstandard property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZGPCSTANDARD() {
        return zgpcstandard;
    }

    /**
     * Sets the value of the zgpcstandard property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZGPCSTANDARD(String value) {
        this.zgpcstandard = value;
    }

    /**
     * Gets the value of the zhardwaretype property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZHARDWARETYPE() {
        return zhardwaretype;
    }

    /**
     * Sets the value of the zhardwaretype property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZHARDWARETYPE(String value) {
        this.zhardwaretype = value;
    }

    /**
     * Gets the value of the zinstmodel property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZINSTMODEL() {
        return zinstmodel;
    }

    /**
     * Sets the value of the zinstmodel property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZINSTMODEL(String value) {
        this.zinstmodel = value;
    }

    /**
     * Gets the value of the zmassspeccom property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZMASSSPECCOM() {
        return zmassspeccom;
    }

    /**
     * Sets the value of the zmassspeccom property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZMASSSPECCOM(String value) {
        this.zmassspeccom = value;
    }

    /**
     * Gets the value of the zmaxinjvol property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZMAXINJVOL() {
        return zmaxinjvol;
    }

    /**
     * Sets the value of the zmaxinjvol property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZMAXINJVOL(String value) {
        this.zmaxinjvol = value;
    }

    /**
     * Gets the value of the zmaxresivol property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZMAXRESIVOL() {
        return zmaxresivol;
    }

    /**
     * Sets the value of the zmaxresivol property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZMAXRESIVOL(String value) {
        this.zmaxresivol = value;
    }

    /**
     * Gets the value of the zpackaging property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZPACKAGING() {
        return zpackaging;
    }

    /**
     * Sets the value of the zpackaging property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZPACKAGING(String value) {
        this.zpackaging = value;
    }

    /**
     * Gets the value of the zpmkit property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZPMKIT() {
        return zpmkit;
    }

    /**
     * Sets the value of the zpmkit property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZPMKIT(String value) {
        this.zpmkit = value;
    }

    /**
     * Gets the value of the zqctested property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZQCTESTED() {
        return zqctested;
    }

    /**
     * Sets the value of the zqctested property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZQCTESTED(String value) {
        this.zqctested = value;
    }

    /**
     * Gets the value of the zreplacesch property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZREPLACESCH() {
        return zreplacesch;
    }

    /**
     * Sets the value of the zreplacesch property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZREPLACESCH(String value) {
        this.zreplacesch = value;
    }

    /**
     * Gets the value of the zsampmatrix property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSAMPMATRIX() {
        return zsampmatrix;
    }

    /**
     * Sets the value of the zsampmatrix property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSAMPMATRIX(String value) {
        this.zsampmatrix = value;
    }

    /**
     * Gets the value of the zsearchcat property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSEARCHCAT() {
        return zsearchcat;
    }

    /**
     * Sets the value of the zsearchcat property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSEARCHCAT(String value) {
        this.zsearchcat = value;
    }

    /**
     * Gets the value of the zseptatype property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSEPTATYPE() {
        return zseptatype;
    }

    /**
     * Sets the value of the zseptatype property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSEPTATYPE(String value) {
        this.zseptatype = value;
    }

    /**
     * Gets the value of the zshippform property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSHIPPFORM() {
        return zshippform;
    }

    /**
     * Sets the value of the zshippform property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSHIPPFORM(String value) {
        this.zshippform = value;
    }

    /**
     * Gets the value of the zsilanolact property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSILANOLACT() {
        return zsilanolact;
    }

    /**
     * Sets the value of the zsilanolact property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSILANOLACT(String value) {
        this.zsilanolact = value;
    }

    /**
     * Gets the value of the zsitecategory property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSITECATEGORY() {
        return zsitecategory;
    }

    /**
     * Sets the value of the zsitecategory property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSITECATEGORY(String value) {
        this.zsitecategory = value;
    }

    /**
     * Gets the value of the zsize property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSIZE() {
        return zsize;
    }

    /**
     * Sets the value of the zsize property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSIZE(String value) {
        this.zsize = value;
    }

    /**
     * Gets the value of the zstorage property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSTORAGE() {
        return zstorage;
    }

    /**
     * Sets the value of the zstorage property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSTORAGE(String value) {
        this.zstorage = value;
    }

    /**
     * Gets the value of the ztechnology property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZTECHNOLOGY() {
        return ztechnology;
    }

    /**
     * Sets the value of the ztechnology property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZTECHNOLOGY(String value) {
        this.ztechnology = value;
    }

    /**
     * Gets the value of the ztubfittings property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZTUBFITTINGS() {
        return ztubfittings;
    }

    /**
     * Sets the value of the ztubfittings property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZTUBFITTINGS(String value) {
        this.ztubfittings = value;
    }

    /**
     * Gets the value of the zuspclass property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZUSPCLASS() {
        return zuspclass;
    }

    /**
     * Sets the value of the zuspclass property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZUSPCLASS(String value) {
        this.zuspclass = value;
    }

    /**
     * Gets the value of the zvialtype property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZVIALTYPE() {
        return zvialtype;
    }

    /**
     * Sets the value of the zvialtype property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZVIALTYPE(String value) {
        this.zvialtype = value;
    }

    /**
     * Gets the value of the zvolumecap property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZVOLUMECAP() {
        return zvolumecap;
    }

    /**
     * Sets the value of the zvolumecap property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZVOLUMECAP(String value) {
        this.zvolumecap = value;
    }

    /**
     * Gets the value of the zwaterwet property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZWATERWET() {
        return zwaterwet;
    }

    /**
     * Sets the value of the zwaterwet property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZWATERWET(String value) {
        this.zwaterwet = value;
    }

    /**
     * Gets the value of the zcareurl property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZCAREURL() {
        return zcareurl;
    }

    /**
     * Sets the value of the zcareurl property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZCAREURL(String value) {
        this.zcareurl = value;
    }

    /**
     * Gets the value of the zmsdsurl property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZMSDSURL() {
        return zmsdsurl;
    }

    /**
     * Sets the value of the zmsdsurl property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZMSDSURL(String value) {
        this.zmsdsurl = value;
    }

    /**
     * Gets the value of the zsecondchemistry property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSECONDCHEMISTRY() {
        return zsecondchemistry;
    }

    /**
     * Sets the value of the zsecondchemistry property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSECONDCHEMISTRY(String value) {
        this.zsecondchemistry = value;
    }

    /**
     * Gets the value of the zmethods property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZMETHODS() {
        return zmethods;
    }

    /**
     * Sets the value of the zmethods property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZMETHODS(String value) {
        this.zmethods = value;
    }

    /**
     * Gets the value of the zpartcat property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZPARTCAT() {
        return zpartcat;
    }

    /**
     * Sets the value of the zpartcat property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZPARTCAT(String value) {
        this.zpartcat = value;
    }

    /**
     * Gets the value of the zsystem property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZSYSTEM() {
        return zsystem;
    }

    /**
     * Sets the value of the zsystem property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZSYSTEM(String value) {
        this.zsystem = value;
    }

    /**
     * Gets the value of the zprddesc property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getZPRDDESC() {
        return zprddesc;
    }

    /**
     * Sets the value of the zprddesc property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setZPRDDESC(String value) {
        this.zprddesc = value;
    }

}
