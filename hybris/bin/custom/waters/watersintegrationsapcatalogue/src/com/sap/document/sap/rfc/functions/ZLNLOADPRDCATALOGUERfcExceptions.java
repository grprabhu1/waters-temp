
package com.sap.document.sap.rfc.functions;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ZLN_LOAD_PRD_CATALOGUE.RfcExceptions.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ZLN_LOAD_PRD_CATALOGUE.RfcExceptions">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NO_DELETE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "ZLN_LOAD_PRD_CATALOGUE.RfcExceptions")
@XmlEnum
@SuppressWarnings("PMD")
@SuppressFBWarnings
public enum ZLNLOADPRDCATALOGUERfcExceptions {

    NO_DELETE;

    public String value() {
        return name();
    }

    public static ZLNLOADPRDCATALOGUERfcExceptions fromValue(String v) {
        return valueOf(v);
    }

}
