
package com.sap.document.sap.rfc.functions;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.sap.document.sap.rfc.functions package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
@SuppressWarnings("PMD")
@SuppressFBWarnings
public class ObjectFactory {

    private final static QName _ZLNLOADPRDCATALOGUEException_QNAME = new QName("urn:sap-com:document:sap:rfc:functions", "ZLN_LOAD_PRD_CATALOGUE.Exception");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sap.document.sap.rfc.functions
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ZLNLOADPRDCATALOGUEResponse }
     *
     */
    public ZLNLOADPRDCATALOGUEResponse createZLNLOADPRDCATALOGUEResponse() {
        return new ZLNLOADPRDCATALOGUEResponse();
    }

    /**
     * Create an instance of {@link TABLEOFZCATPRODUCTS }
     *
     */
    public TABLEOFZCATPRODUCTS createTABLEOFZCATPRODUCTS() {
        return new TABLEOFZCATPRODUCTS();
    }

    /**
     * Create an instance of {@link TABLEOFZSTPRDCAT }
     *
     */
    public TABLEOFZSTPRDCAT createTABLEOFZSTPRDCAT() {
        return new TABLEOFZSTPRDCAT();
    }

    /**
     * Create an instance of {@link ZLNLOADPRDCATALOGUERfcException }
     *
     */
    public ZLNLOADPRDCATALOGUERfcException createZLNLOADPRDCATALOGUERfcException() {
        return new ZLNLOADPRDCATALOGUERfcException();
    }

    /**
     * Create an instance of {@link ZLNLOADPRDCATALOGUE }
     *
     */
    public ZLNLOADPRDCATALOGUE createZLNLOADPRDCATALOGUE() {
        return new ZLNLOADPRDCATALOGUE();
    }

    /**
     * Create an instance of {@link ZSTPRDCAT }
     *
     */
    public ZSTPRDCAT createZSTPRDCAT() {
        return new ZSTPRDCAT();
    }

    /**
     * Create an instance of {@link ZCATPRODUCTS }
     *
     */
    public ZCATPRODUCTS createZCATPRODUCTS() {
        return new ZCATPRODUCTS();
    }

    /**
     * Create an instance of {@link RfcExceptionMessage }
     *
     */
    public RfcExceptionMessage createRfcExceptionMessage() {
        return new RfcExceptionMessage();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ZLNLOADPRDCATALOGUERfcException }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:sap-com:document:sap:rfc:functions", name = "ZLN_LOAD_PRD_CATALOGUE.Exception")
    public JAXBElement<ZLNLOADPRDCATALOGUERfcException> createZLNLOADPRDCATALOGUEException(ZLNLOADPRDCATALOGUERfcException value) {
        return new JAXBElement<ZLNLOADPRDCATALOGUERfcException>(_ZLNLOADPRDCATALOGUEException_QNAME, ZLNLOADPRDCATALOGUERfcException.class, null, value);
    }

}
