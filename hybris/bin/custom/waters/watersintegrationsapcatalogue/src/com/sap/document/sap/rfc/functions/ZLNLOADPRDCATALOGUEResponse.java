
package com.sap.document.sap.rfc.functions;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IT_PRDCAT" type="{urn:sap-com:document:sap:rfc:functions}TABLE_OF_ZCAT_PRODUCTS"/>
 *         &lt;element name="IT_RETURN" type="{urn:sap-com:document:sap:rfc:functions}TABLE_OF_ZST_PRDCAT"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itprdcat",
    "itreturn"
})
@XmlRootElement(name = "ZLN_LOAD_PRD_CATALOGUEResponse")
@SuppressWarnings("PMD")
@SuppressFBWarnings
public class ZLNLOADPRDCATALOGUEResponse {

    @XmlElement(name = "IT_PRDCAT", required = true)
    protected TABLEOFZCATPRODUCTS itprdcat;
    @XmlElement(name = "IT_RETURN", required = true)
    protected TABLEOFZSTPRDCAT itreturn;

    /**
     * Gets the value of the itprdcat property.
     *
     * @return
     *     possible object is
     *     {@link TABLEOFZCATPRODUCTS }
     *
     */
    public TABLEOFZCATPRODUCTS getITPRDCAT() {
        return itprdcat;
    }

    /**
     * Sets the value of the itprdcat property.
     *
     * @param value
     *     allowed object is
     *     {@link TABLEOFZCATPRODUCTS }
     *
     */
    public void setITPRDCAT(TABLEOFZCATPRODUCTS value) {
        this.itprdcat = value;
    }

    /**
     * Gets the value of the itreturn property.
     *
     * @return
     *     possible object is
     *     {@link TABLEOFZSTPRDCAT }
     *
     */
    public TABLEOFZSTPRDCAT getITRETURN() {
        return itreturn;
    }

    /**
     * Sets the value of the itreturn property.
     *
     * @param value
     *     allowed object is
     *     {@link TABLEOFZSTPRDCAT }
     *
     */
    public void setITRETURN(TABLEOFZSTPRDCAT value) {
        this.itreturn = value;
    }

}
