package com.waters.hybris.integration.sapcatalogue.etl.populator;

import com.sap.document.sap.rfc.functions.ZCATPRODUCTS;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.strategies.ProductImageResolutionStrategy;
import com.waters.hybris.core.strategies.impl.WatersProductImagePreviewResolutionStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.KeywordModel;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class SapCatalogueProductPopulatorTest
{

	private static final String IMAGE_NAME = "image.png";
	private static final String TEST_IMAGE_URL = "http://waters.com/test/" + IMAGE_NAME;
	private static final String SMALL_COMPONENTS_EXTERNAL_DIAMETER = "SmallComponentsExternalDiameter";
	private static final String SMALL_COMPONENTS_EXTERNAL_DIAMETER_INFO = SMALL_COMPONENTS_EXTERNAL_DIAMETER + ",5";
	private static final String EXTERNAL_DIAMETER_5 = "5";
	private static final String EXTERNAL_DIAMETER_UNIT_SYMBOL = "mm";
	private static final String TECH_DATA_URL = "http://www.waters.com/waters/partDetail.htm?partNumber=";
	private static final String IMG_URL = "ImgUrl,255";
	private static final String IMG_NAME = "ImgName,255";
	private static final String KEYWORDS_VALUE = "Keyword1,Keyword2";
	private static final String KEYWORD_1 = "Keyword1";
	private static final String KEYWORD_2 = "Keyword2";
	private static final String KEYWORDS = "Keywords,1024";
	private static final String MULTI_VALUE_CODE = "multiValueCode";
	private static final String MULTI_VALUE_CODE_INFO = MULTI_VALUE_CODE + ",50";
	private static final String MULTI_VALUE_CODE_INFO_TRIMMED = MULTI_VALUE_CODE + ",5";

	private static final String MULTI_VALUE_1 = "multiValue1";
	private static final String MULTI_VALUE_2 = "multiValue2";
	private static final String MULTI_VALUE_TRIMMED = "multi";

	@Mock
	private final WatersProductModel watersProductModel = new WatersProductModel();

	@Mock
	private final ProductImageResolutionStrategy productImageResolutionStrategy = new WatersProductImagePreviewResolutionStrategy();

	@Mock
	private final ProductFeatureModel smallComponentsExternalDiameterFeature = new ProductFeatureModel();

	@Mock
	private final ClassAttributeAssignmentModel smallComponentsExternalDiameterAssignmentModel = new ClassAttributeAssignmentModel();

	@Mock
	private final ClassificationAttributeModel smallComponentsExternalDiameterAttributeModel = new ClassificationAttributeModel();

	@Mock
	private final ClassificationAttributeValueModel smallComponentsExternalDiameterAttributeValueModel = new ClassificationAttributeValueModel();

	@Mock
	private final ClassificationAttributeUnitModel smallComponentsExternalDiameterUnitModel = new ClassificationAttributeUnitModel();

	@Mock
	private final ClassificationClassModel classificationClassModel = new ClassificationClassModel();

	@Mock
	private final KeywordModel keywordModel1 = new KeywordModel();

	@Mock
	private final KeywordModel keywordModel2 = new KeywordModel();

	@Mock
	private final ProductFeatureModel feature1 = new ProductFeatureModel();

	@Mock
	private final ProductFeatureModel feature2 = new ProductFeatureModel();

	@Mock
	private final ClassAttributeAssignmentModel classAttributeAssignment1 = new ClassAttributeAssignmentModel();

	@Mock
	private final ClassAttributeAssignmentModel classAttributeAssignment2 = new ClassAttributeAssignmentModel();

	@Mock
	private final ClassificationAttributeModel classificationAttributeModel1 = new ClassificationAttributeModel();

	@Mock
	private final ClassificationAttributeModel classificationAttributeModel2 = new ClassificationAttributeModel();

	@InjectMocks
	@Spy
	private final SapCatalogueProductPopulator testObj = new SapCatalogueProductPopulator();

	@Test
	public void testAttributeIsPopulated()
	{
		when(productImageResolutionStrategy.resolveImageUrl(watersProductModel.getPrimaryImage(), Locale.ENGLISH)).thenReturn(TEST_IMAGE_URL);
		when(smallComponentsExternalDiameterFeature.getClassificationAttributeAssignment()).thenReturn(smallComponentsExternalDiameterAssignmentModel);
		when(smallComponentsExternalDiameterAssignmentModel.getClassificationAttribute()).thenReturn(smallComponentsExternalDiameterAttributeModel);
		when(smallComponentsExternalDiameterFeature.getValue()).thenReturn(smallComponentsExternalDiameterAttributeValueModel);
		when(smallComponentsExternalDiameterAttributeModel.getCode()).thenReturn(SMALL_COMPONENTS_EXTERNAL_DIAMETER);
		when(smallComponentsExternalDiameterAttributeValueModel.getName()).thenReturn(EXTERNAL_DIAMETER_5);
		when(smallComponentsExternalDiameterUnitModel.getSymbol()).thenReturn(EXTERNAL_DIAMETER_UNIT_SYMBOL);
		when(smallComponentsExternalDiameterFeature.getUnit()).thenReturn(smallComponentsExternalDiameterUnitModel);

		doReturn(TECH_DATA_URL + watersProductModel.getCode()).when(testObj).getTechDataUrl(watersProductModel);
		doReturn(SMALL_COMPONENTS_EXTERNAL_DIAMETER_INFO).when(testObj).getFieldMappedValue(Mockito.anyString());
		final List<ProductFeatureModel> features = new ArrayList<>();
		features.add(smallComponentsExternalDiameterFeature);
		when(watersProductModel.getFeatures()).thenReturn(features);

		final List<ClassificationClassModel> classificationClasses = new ArrayList<>();
		classificationClasses.add(classificationClassModel);
		when(watersProductModel.getClassificationClasses()).thenReturn(classificationClasses);

		final ZCATPRODUCTS zcatproducts = new ZCATPRODUCTS();
		testObj.populate(watersProductModel, zcatproducts);
		assertEquals(zcatproducts.getZOUTERDIA(), EXTERNAL_DIAMETER_5 + " " + EXTERNAL_DIAMETER_UNIT_SYMBOL);
	}

	@Test
	public void testImageNameAndUrlIsPopulated()
	{
		when(productImageResolutionStrategy.resolveImageUrl(watersProductModel.getPrimaryImage(), Locale.ENGLISH)).thenReturn(TEST_IMAGE_URL);
		when(smallComponentsExternalDiameterFeature.getClassificationAttributeAssignment()).thenReturn(smallComponentsExternalDiameterAssignmentModel);
		when(smallComponentsExternalDiameterAssignmentModel.getClassificationAttribute()).thenReturn(smallComponentsExternalDiameterAttributeModel);
		when(smallComponentsExternalDiameterFeature.getValue()).thenReturn(smallComponentsExternalDiameterAttributeValueModel);
		when(smallComponentsExternalDiameterAttributeModel.getCode()).thenReturn(SMALL_COMPONENTS_EXTERNAL_DIAMETER);
		when(smallComponentsExternalDiameterAttributeValueModel.getName()).thenReturn(EXTERNAL_DIAMETER_5);
		when(smallComponentsExternalDiameterUnitModel.getSymbol()).thenReturn(EXTERNAL_DIAMETER_UNIT_SYMBOL);
		when(smallComponentsExternalDiameterFeature.getUnit()).thenReturn(smallComponentsExternalDiameterUnitModel);

		doReturn(TECH_DATA_URL + watersProductModel.getCode()).when(testObj).getTechDataUrl(watersProductModel);
		doReturn(IMG_URL).when(testObj).getFieldMappedValue(Mockito.anyString());
		final List<ProductFeatureModel> features = new ArrayList<>();
		features.add(smallComponentsExternalDiameterFeature);
		when(watersProductModel.getFeatures()).thenReturn(features);

		final List<ClassificationClassModel> classificationClasses = new ArrayList<>();
		classificationClasses.add(classificationClassModel);
		when(watersProductModel.getClassificationClasses()).thenReturn(classificationClasses);

		final ZCATPRODUCTS zcatproducts = new ZCATPRODUCTS();
		testObj.populate(watersProductModel, zcatproducts);
		assertEquals(zcatproducts.getZIMGURL(), TEST_IMAGE_URL);
	}

	@Test
	public void testImageNameAndNameIsPopulated()
	{
		when(productImageResolutionStrategy.resolveImageUrl(watersProductModel.getPrimaryImage(), Locale.ENGLISH)).thenReturn(TEST_IMAGE_URL);
		when(smallComponentsExternalDiameterFeature.getClassificationAttributeAssignment()).thenReturn(smallComponentsExternalDiameterAssignmentModel);
		when(smallComponentsExternalDiameterAssignmentModel.getClassificationAttribute()).thenReturn(smallComponentsExternalDiameterAttributeModel);
		when(smallComponentsExternalDiameterFeature.getValue()).thenReturn(smallComponentsExternalDiameterAttributeValueModel);
		when(smallComponentsExternalDiameterAttributeModel.getCode()).thenReturn(SMALL_COMPONENTS_EXTERNAL_DIAMETER);
		when(smallComponentsExternalDiameterAttributeValueModel.getName()).thenReturn(EXTERNAL_DIAMETER_5);
		when(smallComponentsExternalDiameterUnitModel.getSymbol()).thenReturn(EXTERNAL_DIAMETER_UNIT_SYMBOL);
		when(smallComponentsExternalDiameterFeature.getUnit()).thenReturn(smallComponentsExternalDiameterUnitModel);

		doReturn(TECH_DATA_URL + watersProductModel.getCode()).when(testObj).getTechDataUrl(watersProductModel);
		doReturn(IMG_NAME).when(testObj).getFieldMappedValue(Mockito.anyString());
		final List<ProductFeatureModel> features = new ArrayList<>();
		features.add(smallComponentsExternalDiameterFeature);
		when(watersProductModel.getFeatures()).thenReturn(features);

		final List<ClassificationClassModel> classificationClasses = new ArrayList<>();
		classificationClasses.add(classificationClassModel);
		when(watersProductModel.getClassificationClasses()).thenReturn(classificationClasses);

		final ZCATPRODUCTS zcatproducts = new ZCATPRODUCTS();
		testObj.populate(watersProductModel, zcatproducts);
		assertEquals(zcatproducts.getZIMGNAME(), IMAGE_NAME);
	}

	@Test
	public void testImageNameAndUrlNotSet()
	{
		when(productImageResolutionStrategy.resolveImageUrl(watersProductModel.getPrimaryImage(), Locale.ENGLISH)).thenReturn(null);
		when(smallComponentsExternalDiameterFeature.getClassificationAttributeAssignment()).thenReturn(smallComponentsExternalDiameterAssignmentModel);
		when(smallComponentsExternalDiameterAssignmentModel.getClassificationAttribute()).thenReturn(smallComponentsExternalDiameterAttributeModel);
		when(smallComponentsExternalDiameterFeature.getValue()).thenReturn(smallComponentsExternalDiameterAttributeValueModel);
		when(smallComponentsExternalDiameterAttributeModel.getCode()).thenReturn(SMALL_COMPONENTS_EXTERNAL_DIAMETER);
		when(smallComponentsExternalDiameterAttributeValueModel.getName()).thenReturn(EXTERNAL_DIAMETER_5);
		when(smallComponentsExternalDiameterUnitModel.getSymbol()).thenReturn(EXTERNAL_DIAMETER_UNIT_SYMBOL);
		when(smallComponentsExternalDiameterFeature.getUnit()).thenReturn(smallComponentsExternalDiameterUnitModel);

		doReturn(TECH_DATA_URL + watersProductModel.getCode()).when(testObj).getTechDataUrl(watersProductModel);
		doReturn(IMG_URL).when(testObj).getFieldMappedValue(Mockito.anyString());
		final List<ProductFeatureModel> features = new ArrayList<>();
		features.add(smallComponentsExternalDiameterFeature);
		when(watersProductModel.getFeatures()).thenReturn(features);

		final List<ClassificationClassModel> classificationClasses = new ArrayList<>();
		classificationClasses.add(classificationClassModel);
		when(watersProductModel.getClassificationClasses()).thenReturn(classificationClasses);

		final ZCATPRODUCTS zcatproducts = new ZCATPRODUCTS();
		testObj.populate(watersProductModel, zcatproducts);
		assertEquals(zcatproducts.getZIMGURL(), null);
		assertEquals(zcatproducts.getZIMGNAME(), null);
	}

	@Test
	public void testKeywords()
	{
		final List<KeywordModel> keywordModels = new ArrayList<>();
		keywordModels.add(keywordModel1);
		keywordModels.add(keywordModel2);
		when(keywordModel1.getKeyword()).thenReturn(KEYWORD_1);
		when(keywordModel2.getKeyword()).thenReturn(KEYWORD_2);
		when(watersProductModel.getKeywords()).thenReturn(keywordModels);

		when(productImageResolutionStrategy.resolveImageUrl(watersProductModel.getPrimaryImage(), Locale.ENGLISH)).thenReturn(null);
		when(smallComponentsExternalDiameterFeature.getClassificationAttributeAssignment()).thenReturn(smallComponentsExternalDiameterAssignmentModel);
		when(smallComponentsExternalDiameterAssignmentModel.getClassificationAttribute()).thenReturn(smallComponentsExternalDiameterAttributeModel);
		when(smallComponentsExternalDiameterFeature.getValue()).thenReturn(smallComponentsExternalDiameterAttributeValueModel);
		when(smallComponentsExternalDiameterAttributeModel.getCode()).thenReturn(SMALL_COMPONENTS_EXTERNAL_DIAMETER);
		when(smallComponentsExternalDiameterAttributeValueModel.getName()).thenReturn(EXTERNAL_DIAMETER_5);
		when(smallComponentsExternalDiameterUnitModel.getSymbol()).thenReturn(EXTERNAL_DIAMETER_UNIT_SYMBOL);
		when(smallComponentsExternalDiameterFeature.getUnit()).thenReturn(smallComponentsExternalDiameterUnitModel);

		doReturn(TECH_DATA_URL + watersProductModel.getCode()).when(testObj).getTechDataUrl(watersProductModel);
		doReturn(KEYWORDS).when(testObj).getFieldMappedValue(Mockito.anyString());
		final List<ProductFeatureModel> features = new ArrayList<>();
		features.add(smallComponentsExternalDiameterFeature);
		when(watersProductModel.getFeatures()).thenReturn(features);

		final List<ClassificationClassModel> classificationClasses = new ArrayList<>();
		classificationClasses.add(classificationClassModel);
		when(watersProductModel.getClassificationClasses()).thenReturn(classificationClasses);

		final ZCATPRODUCTS zcatproducts = new ZCATPRODUCTS();
		testObj.populate(watersProductModel, zcatproducts);
		assertEquals(zcatproducts.getZSEARCH(), KEYWORDS_VALUE);
	}

	@Test
	public void testMultiValueFieldsAreCommaSeparated()
	{
		when(watersProductModel.getClassificationClasses()).thenReturn(Arrays.asList(classificationClassModel));
		when(watersProductModel.getFeatures()).thenReturn(Arrays.asList(feature1, feature2));
		when(feature1.getClassificationAttributeAssignment()).thenReturn(classAttributeAssignment1);
		when(feature2.getClassificationAttributeAssignment()).thenReturn(classAttributeAssignment2);

		when(classAttributeAssignment1.getClassificationAttribute()).thenReturn(classificationAttributeModel1);
		when(classAttributeAssignment2.getClassificationAttribute()).thenReturn(classificationAttributeModel2);

		when(classificationAttributeModel1.getCode()).thenReturn(MULTI_VALUE_CODE);
		when(classificationAttributeModel2.getCode()).thenReturn(MULTI_VALUE_CODE);

		when(feature1.getValue()).thenReturn(MULTI_VALUE_1);
		when(feature2.getValue()).thenReturn(MULTI_VALUE_2);

		doReturn(MULTI_VALUE_CODE_INFO).when(testObj).getFieldMappedValue(Mockito.anyString());
		doReturn(TECH_DATA_URL + watersProductModel.getCode()).when(testObj).getTechDataUrl(watersProductModel);

		final ZCATPRODUCTS zcatproducts = new ZCATPRODUCTS();
		testObj.populate(watersProductModel, zcatproducts);

		assertEquals(zcatproducts.getZGPCBRAND(), MULTI_VALUE_1 + "," + MULTI_VALUE_2);
	}

	@Test
	public void testMultiValueFieldsAreCommaSeparatedWithMaxLimit()
	{
		when(watersProductModel.getClassificationClasses()).thenReturn(Arrays.asList(classificationClassModel));
		when(watersProductModel.getFeatures()).thenReturn(Arrays.asList(feature1, feature2));
		when(feature1.getClassificationAttributeAssignment()).thenReturn(classAttributeAssignment1);
		when(feature2.getClassificationAttributeAssignment()).thenReturn(classAttributeAssignment2);

		when(classAttributeAssignment1.getClassificationAttribute()).thenReturn(classificationAttributeModel1);
		when(classAttributeAssignment2.getClassificationAttribute()).thenReturn(classificationAttributeModel2);

		when(classificationAttributeModel1.getCode()).thenReturn(MULTI_VALUE_CODE);
		when(classificationAttributeModel2.getCode()).thenReturn(MULTI_VALUE_CODE);

		when(feature1.getValue()).thenReturn(MULTI_VALUE_1);
		when(feature2.getValue()).thenReturn(MULTI_VALUE_2);

		doReturn(MULTI_VALUE_CODE_INFO_TRIMMED).when(testObj).getFieldMappedValue(Mockito.anyString());
		doReturn(TECH_DATA_URL + watersProductModel.getCode()).when(testObj).getTechDataUrl(watersProductModel);

		final ZCATPRODUCTS zcatproducts = new ZCATPRODUCTS();
		testObj.populate(watersProductModel, zcatproducts);

		assertEquals(zcatproducts.getZGPCBRAND(), MULTI_VALUE_TRIMMED);
	}
}
