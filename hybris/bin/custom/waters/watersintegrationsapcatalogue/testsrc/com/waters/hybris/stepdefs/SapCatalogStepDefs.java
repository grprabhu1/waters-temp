package com.waters.hybris.stepdefs;

import com.sap.document.sap.rfc.functions.ZCATPRODUCTS;
import com.sap.document.sap.rfc.functions.ZLNLOADPRDCATALOGUE;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.util.StreamUtil;
import com.waters.hybris.core.wiremock.connector.WireMockGateway;
import com.waters.hybris.core.wiremock.stubs.Request;
import com.waters.hybris.stepdefs.integration.helper.IntegrationHelper;
import cucumber.api.java.en.Then;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

public class SapCatalogStepDefs
{
	private static final Logger LOG = LoggerFactory.getLogger(SapCatalogStepDefs.class);

	private static final String SAPCATALOGUE_MOCK_URL = "/sapcatalogue";
	private static final String NS2_ZLN_LOAD_PRD_CATALOGUE_START = "<ns2:ZLN_LOAD_PRD_CATALOGUE";
	private static final String NS2_ZLN_LOAD_PRD_CATALOGUE_END = "</ns2:ZLN_LOAD_PRD_CATALOGUE>";

	private WireMockGateway wireMockGateway;
	private IntegrationHelper integrationHelper;

	@Then("^all published and active products are exported to sap catalogue end point$")
	public void then_all_published_active_products_are_exported_to_sap_catalogue_end_point()
	{
		final Set<String> allPublishedActiveProductCodesInSystem = StreamUtil.nullSafe(getIntegrationHelper().getPublishedActiveProducts())
			.map(WatersProductModel::getCode)
			.collect(Collectors.toSet());

		final Set<String> allProductCodesSentToEndpoint = StreamUtil.nullSafe(Arrays.asList(getWireMockGateway()
			.findRequestsForUrl(SAPCATALOGUE_MOCK_URL).getRequests()))
			.filter(this::containsProductCatalogueData)
			.map(this::getProductCodesSentToEndpoint)
			.flatMap(Collection::stream)
			.collect(Collectors.toSet());

		Assert.assertEquals(allPublishedActiveProductCodesInSystem.size(), allProductCodesSentToEndpoint.size());
		Assert.assertTrue(allProductCodesSentToEndpoint.containsAll(allPublishedActiveProductCodesInSystem));

		getWireMockGateway().resetRequestCount();
	}

	private Set<String> getProductCodesSentToEndpoint(final Request request)
	{
		try
		{
			final JAXBContext context = JAXBContext.newInstance(ZLNLOADPRDCATALOGUE.class);
			final Unmarshaller unmarshaller = context.createUnmarshaller();
			final StringReader reader = new StringReader(getSapCatalogueXml(request));
			final ZLNLOADPRDCATALOGUE zlnloadprdcatalogue = (ZLNLOADPRDCATALOGUE) unmarshaller.unmarshal(reader);
			return StreamUtil.nullSafe(zlnloadprdcatalogue.getITPRDCAT().getItem())
				.map(ZCATPRODUCTS::getMATNR)
				.filter(ObjectUtils::allNotNull)
				.collect(Collectors.toSet());
		}
		catch (final JAXBException e)
		{
			LOG.error("Error in processing request body", e);
			return Collections.emptySet();
		}
	}

	private boolean containsProductCatalogueData(final Request request)
	{
		return request.getBody().contains(NS2_ZLN_LOAD_PRD_CATALOGUE_START) && request.getBody().contains(NS2_ZLN_LOAD_PRD_CATALOGUE_END);
	}

	private String getSapCatalogueXml(final Request request)
	{
		return request.getBody().substring(request.getBody().indexOf(NS2_ZLN_LOAD_PRD_CATALOGUE_START),
			request.getBody().lastIndexOf(NS2_ZLN_LOAD_PRD_CATALOGUE_END) + NS2_ZLN_LOAD_PRD_CATALOGUE_END.length());
	}

	public WireMockGateway getWireMockGateway()
	{
		return wireMockGateway;
	}

	@Required
	public void setWireMockGateway(final WireMockGateway wireMockGateway)
	{
		this.wireMockGateway = wireMockGateway;
	}

	protected IntegrationHelper getIntegrationHelper()
	{
		return integrationHelper;
	}

	@Required
	public void setIntegrationHelper(final IntegrationHelper integrationHelper)
	{
		this.integrationHelper = integrationHelper;
	}
}
