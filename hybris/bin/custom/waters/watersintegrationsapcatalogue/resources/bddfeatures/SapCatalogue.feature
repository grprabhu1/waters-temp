@SapCatalogue @Transaction
Feature: Sap Catalogue

	@AdminUser
	Scenario: Sap Catalogue
		Given there are published and active products in PIM
		When the "watersSapCatalogueJob" has been run
		And the job completed successfully
		Then all published and active products are exported to sap catalogue end point
