package com.waters.service.impl;

import com.waters.service.data.AttributeConfig;
import com.waters.service.parser.impl.DefaultCSVFileParserService;
import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class DefaultCSVFileParserServiceTest
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(DefaultCSVFileParserServiceTest.class.getName());

	DefaultCSVFileParserService fileParserService = new DefaultCSVFileParserService();

	@Test
	public void testParseCSVAttributeFile() throws IOException
	{
		Path inputFile = getFile("testData/TableExport.csv");

		List<AttributeConfig> attributes = fileParserService.parseAttributeFile(inputFile);
		Assert.assertEquals(243, attributes.size());
	}

	private Path getFile(final String fileName){
		final ClassLoader classLoader = getClass().getClassLoader();
		return Paths.get(classLoader.getResource(fileName).getPath());
	}
}



