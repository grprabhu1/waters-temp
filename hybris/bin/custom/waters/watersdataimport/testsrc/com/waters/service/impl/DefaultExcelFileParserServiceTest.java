package com.waters.service.impl;

import com.waters.product.dataimport.FileContent;
import com.waters.product.dataimport.Product;
import com.waters.service.parser.impl.DefaultExcelFileParserService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class DefaultExcelFileParserServiceTest
{
	@InjectMocks
	DefaultExcelFileParserService excelFileParserService = new DefaultExcelFileParserService();

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private Configuration configuration;

	@Before
	public void setUp(){
		given(configurationService.getConfiguration()).willReturn(configuration);
		given(configuration.getString(anyString(), anyString())).willReturn(StringUtils.EMPTY);
	}


	@Test
	public void testParseExcelColumns() throws IOException
	{
		final InputStream inputFile = getFile("testData/Columns.xlsx");

		final MockMultipartFile file = new MockMultipartFile("file", "Columns.xlsx", "multipart/form-data", inputFile);

		final FileContent fileContent = excelFileParserService.parse(file, "");
		Assert.assertEquals(50, fileContent.getProducts().size());
		final Product firstProduct = fileContent.getProducts().iterator().next();

		Assert.assertTrue(firstProduct.getValuesWithUom().keySet().containsAll(Arrays.asList("Units per Package", "Column Carbon Load",
			"Column Inner Diameter", "Column Ion Exchange Capacity", "Column Length", "Column Particle Size")));
		Assert.assertEquals(34, firstProduct.getValues().size());
		Assert.assertEquals(8, firstProduct.getValuesWithUom().size());
	}



	private InputStream getFile(final String fileName){
		final ClassLoader classLoader = getClass().getClassLoader();
		return classLoader.getResourceAsStream(fileName);
	}
}



