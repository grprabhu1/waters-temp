package com.waters.service.data;

import org.apache.commons.lang.StringUtils;

import java.util.Objects;

public class ClassificationAttributeValueData
{
	private String code;
	private String data;

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public String getData()
	{
		return data;
	}

	public void setData(final String data)
	{
		this.data = data;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		final ClassificationAttributeValueData that = (ClassificationAttributeValueData) o;
		return StringUtils.equalsIgnoreCase(getCode(), that.getCode()) &&
			StringUtils.equalsIgnoreCase(getData(), that.getData());
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(getCode().toLowerCase(), getData().toLowerCase());
	}
}
