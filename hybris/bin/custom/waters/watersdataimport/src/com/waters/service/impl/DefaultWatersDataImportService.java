package com.waters.service.impl;

import com.waters.product.dataimport.FileContent;
import com.waters.product.dataimport.ImportType;
import com.waters.service.WatersDataExportService;
import com.waters.service.WatersDataImportImpexService;
import com.waters.service.WatersDataImportService;
import com.waters.service.data.ImpexImportResult;
import com.waters.service.parser.FileParserService;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.impex.ImportResult;
import de.hybris.platform.servicelayer.impex.ImportService;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Optional;

public class DefaultWatersDataImportService implements WatersDataImportService
{

	private FileParserService fileParserService;
	private WatersDataImportImpexService watersDataImportImpexService;
	private WatersDataExportService watersDataExportService;
	private ImportService importService;

	@Override
	public ImpexImportResult generateProductsImpexFromFile(final MultipartFile file, final String fileType)
	{
		final FileContent fileContent = getFileParserService().parse(file, fileType);
		return watersDataImportImpexService.generateProductsImpex(fileContent);
	}

	@Override
	public ImpexImportResult generateClassificationAttributesImpexFromFile(final MultipartFile file, final String fileType, final String importMode)
	{
		final FileContent fileContent = getFileParserService().parse(file, fileType);
		fileContent.setImportType(importMode.equalsIgnoreCase("create") ? ImportType.CREATE : ImportType.UPDATE);
		return watersDataImportImpexService.generateClassificationAttributesImpex(fileContent);
	}

	@Override
	public ImpexImportResult exportClassificationAttributes(){
		final String data = getWatersDataExportService().getClassificationAttributes();
		return new ImpexImportResult(data);
	}

	@Override
	public ImpexImportResult generateAndRun(final MultipartFile file, final String fileType)
	{
		final FileContent fileContent = getFileParserService().parse(file, fileType);
		ImpexImportResult result = watersDataImportImpexService.generateClassificationAttributesImpex(fileContent);
		try
		{
			if (!result.isSuccess())
			{
				return result;
			}
			Optional<String> importResult = importImpex(result.getFileUrl().get());

			if (importResult.isPresent())
			{
				return new ImpexImportResult(importResult.get());
			}

			result = watersDataImportImpexService.generateProductsImpex(fileContent);

			if (!result.isSuccess())
			{
				return result;
			}

			importResult = importImpex(result.getFileUrl().get());

			if (importResult.isPresent())
			{
				return new ImpexImportResult(importResult.get());
			}
		}
		catch (final FileNotFoundException e)
		{
			return new ImpexImportResult(e.getMessage());
		}

		return result;
	}

	private Optional<String> importImpex(final String file) throws FileNotFoundException
	{
		final ImportConfig importConfig = new ImportConfig();
		importConfig.setScript(new StreamBasedImpExResource(new FileInputStream(new File(file)), "UTF-8"));
		importConfig.setLegacyMode(Boolean.FALSE);

		final ImportResult importResult = getImportService().importData(importConfig);
		if (importResult.isError())
		{
			return Optional.of(importResult.getUnresolvedLines().toString());
		}
		return Optional.empty();
	}

	public FileParserService getFileParserService()
	{
		return fileParserService;
	}

	public void setFileParserService(final FileParserService fileParserService)
	{
		this.fileParserService = fileParserService;
	}

	public WatersDataImportImpexService getWatersDataImportImpexService()
	{
		return watersDataImportImpexService;
	}

	public void setWatersDataImportImpexService(final WatersDataImportImpexService watersDataImportImpexService)
	{
		this.watersDataImportImpexService = watersDataImportImpexService;
	}

	public ImportService getImportService()
	{
		return importService;
	}

	@Required
	public void setImportService(final ImportService importService)
	{
		this.importService = importService;
	}

	public WatersDataExportService getWatersDataExportService()
	{
		return watersDataExportService;
	}

	@Required
	public void setWatersDataExportService(final WatersDataExportService watersDataExportService)
	{
		this.watersDataExportService = watersDataExportService;
	}
}
