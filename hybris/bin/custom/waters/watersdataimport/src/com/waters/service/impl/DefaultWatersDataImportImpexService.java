package com.waters.service.impl;

import com.waters.DataImportUtil;
import com.waters.product.dataimport.FileContent;
import com.waters.product.dataimport.ImportType;
import com.waters.product.dataimport.Product;
import com.waters.product.dataimport.UomValue;
import com.waters.service.WatersDataImportImpexService;
import com.waters.service.data.ClassificationAttributeData;
import com.waters.service.data.ImpexImportResult;
import com.waters.service.data.ProductValidationResult;
import com.waters.service.strategy.AttributeConfigProviderStrategy;
import com.waters.service.strategy.ClassificationDataGeneratorStrategy;
import com.waters.service.strategy.ClassificationDataWriterStrategy;
import com.waters.service.strategy.DataImportContext;
import com.waters.service.strategy.ImpexDataWriterStrategy;
import com.waters.service.strategy.impl.InsertClassificationDataGeneratorStrategy;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.waters.constants.WatersdataimportConstants.PRODUCT_EXCLUDED_FIELDS_KEY;
import static com.waters.constants.WatersdataimportConstants.PRODUCT_IDENTIFIER_FIELD_KEY;
import static com.waters.constants.WatersdataimportConstants.PRODUCT_SHARED_FIELDS_KEY;
import static com.waters.constants.WatersdataimportConstants.WATERS_CLASSIFICATION_SYSTEM;
import static com.waters.constants.WatersdataimportConstants.WATERS_CLASSIFICATION_VERSION;

public class DefaultWatersDataImportImpexService implements WatersDataImportImpexService
{

	private static final Logger LOG = LoggerFactory.getLogger(InsertClassificationDataGeneratorStrategy.class);

	private ConfigurationService configurationService;
	private ClassificationSystemService classificationSystemService;
	private ClassificationService classificationService;
	private ClassificationDataGeneratorStrategy classificationDataGeneratorStrategy;
	private ClassificationDataWriterStrategy classificationDataWriterStrategy;
	private ImpexDataWriterStrategy impexDataWriterStrategy;
	private Map<String, Integer> classToPosition;
	private AttributeConfigProviderStrategy attributeConfigProviderStrategy;

	private void setClassAttributeAssignments(final DataImportContext context)
	{
		final ClassificationSystemVersionModel classificationSystemVersionModel = getClassificationSystemService().getSystemVersion(WATERS_CLASSIFICATION_SYSTEM, WATERS_CLASSIFICATION_VERSION);
		try
		{
			JaloSession.getCurrentSession().createLocalSessionContext().setAttribute("disableRestrictions", Boolean.TRUE);
			final ClassificationClassModel classificationClassModel = getClassificationSystemService().getClassForCode(classificationSystemVersionModel, context.getClassificationClass());
			final Map<String, ClassAttributeAssignmentModel> classAttributeAssignmentModels = classificationClassModel.getAllClassificationAttributeAssignments().stream()
				.filter(c -> c.getClassificationAttribute() != null)
				.collect(Collectors.toMap(c -> c.getClassificationAttribute().getCode(), Function.identity()));

			context.setClassAttributeAssignmentModels(classAttributeAssignmentModels);

			final Collection<ClassificationAttributeUnitModel> classificationUnits = getClassificationService().getAttributeUnits(classificationClassModel.getCatalogVersion());
			final Map<String, List<ClassificationAttributeUnitModel>> classificationUnitsByType = classificationUnits.stream().filter(u -> StringUtils.isNotBlank(u.getUnitType())).collect(Collectors.groupingBy(ClassificationAttributeUnitModel::getUnitType));

			context.setUnitsPerUnitType(classificationUnitsByType);
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.debug("Error, classification not found", e);
			throw new IllegalArgumentException("Classification class not found: " + context.getClassificationClass());
		}
		finally
		{
			JaloSession.getCurrentSession().createLocalSessionContext().setAttribute("disableRestrictions", Boolean.FALSE);
		}
	}

	@Override
	public ImpexImportResult generateClassificationAttributesImpex(final FileContent fileContent)
	{
		final DataImportContext context = buildDataImportContext(fileContent.getClassName(), fileContent);

		final Collection<ClassificationAttributeData> result = getClassificationDataGeneratorStrategy().generateClassificationData(fileContent.getProducts(), context);

		return getClassificationDataWriterStrategy().writeClassificationData(result, context);
	}

	public ImpexImportResult generateProductsImpex(final FileContent fileContent)
	{

		final DataImportContext context = buildDataImportContext(fileContent.getClassName(), fileContent);
		final List<Map<String, ProductValidationResult>> translatedProducts = new LinkedList<>();
		final List<Product> cleanProducts = fileContent.getProducts().stream()
			.filter(p -> MapUtils.isNotEmpty(p.getValues()))
			.filter(p -> StringUtils.isNotBlank(p.getValues().get(context.getIdentifierField())))
			.collect(Collectors.toList());

		for (final Product product : cleanProducts)
		{
			final List<ProductValidationResult> translatedFields = translateProduct(product, context);
			final List<ProductValidationResult> invalid = translatedFields.stream().filter(s -> !s.isTranslated()).collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(invalid))
			{
				LOG.error("Error, field have not been translated: " + invalid.stream().map(ProductValidationResult::toString).collect(Collectors.joining(",")));
				final String errorMsg = invalid.stream().map(ProductValidationResult::toString).collect(Collectors.joining("\n"));
				return new ImpexImportResult(errorMsg, "");
			}
			final Map<String, ProductValidationResult> translatedMap = translatedFields.stream().collect(Collectors.toMap(ProductValidationResult::getField, Function.identity()));

			translatedProducts.add(translatedMap);
		}

		return getImpexDataWriterStrategy().writeToImpex(translatedProducts, fileContent, context);
	}

	private List<ProductValidationResult> translateProduct(final Product product, final DataImportContext context)
	{
		final Stream<ProductValidationResult> translatedFields = product.getValues().entrySet().stream().filter(e -> StringUtils.isNotBlank(e.getKey()) && StringUtils.isNotBlank(e.getValue())).map(e -> getAttributeValueCode(e.getKey(), e.getValue(), context));
		final Stream<ProductValidationResult> translatedUomFields = product.getValuesWithUom().entrySet().stream().filter(e -> StringUtils.isNotBlank(e.getKey()) && StringUtils.isNotBlank(e.getValue().getValue())).map(e -> getAttributeValueCodeUom(e, context));

		return Stream.concat(translatedFields, translatedUomFields).collect(Collectors.toList());
	}

	private ProductValidationResult getAttributeValueCodeUom(final Map.Entry<String, UomValue> value, final DataImportContext context)
	{
		final ProductValidationResult result = getAttributeValueCode(value.getKey(), value.getValue().getValue(), context);
		if (result.isTranslated())
		{
			final String keyCode = DataImportUtil.getAttributeCode(value.getKey());
			final ClassAttributeAssignmentModel classAttributeAssignmentModel = context.getClassAttributeAssignmentModels().get(keyCode);
			final List<ClassificationAttributeUnitModel> units = Optional
				.ofNullable(context.getUnitsPerUnitType().get(classAttributeAssignmentModel.getUnit().getUnitType()))
				.orElse(Collections.emptyList());

			final Optional<String> unit = units.stream()
				.filter(c -> c.getSymbol().equalsIgnoreCase(value.getValue().getUom().trim()))
				.map(ClassificationAttributeUnitModel::getCode)
				.findAny();

			final Set<String> uomsTranslated = result.getTranslatedValue().stream().map(s -> String.format("%s:%s", s, unit.orElse(value.getValue().getUom()))).collect(Collectors.toSet());
			result.setTranslatedValue(uomsTranslated);
			result.setTranslated(unit.isPresent());

		}

		return result;
	}

	private ProductValidationResult getAttributeValueCode(final String key, final String value, final DataImportContext context)
	{
		if (context.getIdentifierField().equalsIgnoreCase(key) || context.getExcludeFields().contains(key))
		{
			return ProductValidationResult.buildSuccess(key, value);
		}
		final String keyCode = DataImportUtil.getAttributeCode(key);
		if (context.getClassAttributeAssignmentModels().containsKey(keyCode))
		{
			final ClassAttributeAssignmentModel classAttributeAssignmentModel = context.getClassAttributeAssignmentModels().get(keyCode);

			if (ClassificationAttributeTypeEnum.BOOLEAN.equals(classAttributeAssignmentModel.getAttributeType()))
			{
				if ("true".equalsIgnoreCase(value) || "Yes".equalsIgnoreCase(value))
				{
					return ProductValidationResult.buildSuccess(key, "true");
				}
				else
				{
					return ProductValidationResult.buildSuccess(key, "false");
				}
			}
			else if (ClassificationAttributeTypeEnum.ENUM.equals(classAttributeAssignmentModel.getAttributeType()))
			{

				if (value.contains(";") && BooleanUtils.isFalse(classAttributeAssignmentModel.getMultiValued()))
				{
					LOG.warn("MultiValue in data [" + value + "] for NON multivalue field: " + keyCode);
					return ProductValidationResult.buildFailure(key, value);
				}

				final Stream<String> values = value.contains(";") ? Arrays.stream(value.split(";")) : Stream.of(value);

				final Function<String, Optional<String>> translate = s -> classAttributeAssignmentModel.getAttributeValues().stream()
					.filter(v -> StringUtils.isNotBlank(v.getName()))
					.filter(v -> v.getName().equalsIgnoreCase(s.trim()))
					.map(ClassificationAttributeValueModel::getCode)
					.findAny();

				final Set<Optional<String>> translatedValues = values.map(translate).collect(Collectors.toSet());

				if (translatedValues.stream().allMatch(Optional::isPresent))
				{
					return ProductValidationResult.buildSuccess(key, translatedValues.stream().map(Optional::get).collect(Collectors.toSet()));
				}
			}
			else
			{
				final String cleanUpValue = value.contains(";") ? "\"" + value + "\"" : value;
				return ProductValidationResult.buildSuccess(key, cleanUpValue);
			}

		}
		return ProductValidationResult.buildFailure(key, value);
	}

	private DataImportContext buildDataImportContext(final String classificationClassName, final FileContent fileContent)
	{
		final Set<String> sharedFields = getSetFromProperty(PRODUCT_SHARED_FIELDS_KEY);
		final String identifierField = getConfigurationService().getConfiguration().getString(PRODUCT_IDENTIFIER_FIELD_KEY);
		final Set<String> excludedFields = getSetFromProperty(PRODUCT_EXCLUDED_FIELDS_KEY);
		final ImportType importType = Optional.ofNullable(fileContent.getImportType()).orElse(ImportType.CREATE);

		final DataImportContext context = new DataImportContext();
		if (ImportType.UPDATE.equals(importType))
		{
			final Set<String> fields = new HashSet<>(fileContent.getHeaders());
			excludedFields.forEach(fields::remove);
			fields.remove(identifierField);
			context.setSharedFields(fields);
		}
		else
		{
			context.setSharedFields(sharedFields);
		}
		context.setIdentifierField(identifierField);
		context.setClassificationClass(classificationClassName);
		context.setExcludeFields(excludedFields);
		context.setInitialPosition(getClassToPosition().getOrDefault(classificationClassName, 0).intValue());
		context.setAttributeConfigMap(getAttributeConfigProviderStrategy().loadAttributeConfig());
		setClassAttributeAssignments(context);

		return context;
	}

	private Set<String> getSetFromProperty(final String key)
	{
		final String fields = getConfigurationService().getConfiguration().getString(key);
		if (StringUtils.isNotBlank(fields))
		{
			return Arrays.stream(fields.split(",")).filter(StringUtils::isNotBlank).collect(Collectors.toSet());
		}
		return Collections.emptySet();
	}


	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public ClassificationSystemService getClassificationSystemService()
	{
		return classificationSystemService;
	}

	public void setClassificationSystemService(final ClassificationSystemService classificationSystemService)
	{
		this.classificationSystemService = classificationSystemService;
	}

	public ClassificationService getClassificationService()
	{
		return classificationService;
	}

	public void setClassificationService(final ClassificationService classificationService)
	{
		this.classificationService = classificationService;
	}

	public ClassificationDataGeneratorStrategy getClassificationDataGeneratorStrategy()
	{
		return classificationDataGeneratorStrategy;
	}

	public void setClassificationDataGeneratorStrategy(final ClassificationDataGeneratorStrategy classificationDataGeneratorStrategy)
	{
		this.classificationDataGeneratorStrategy = classificationDataGeneratorStrategy;
	}

	public ClassificationDataWriterStrategy getClassificationDataWriterStrategy()
	{
		return classificationDataWriterStrategy;
	}

	@Required
	public void setClassificationDataWriterStrategy(final ClassificationDataWriterStrategy classificationDataWriterStrategy)
	{
		this.classificationDataWriterStrategy = classificationDataWriterStrategy;
	}

	public ImpexDataWriterStrategy getImpexDataWriterStrategy()
	{
		return impexDataWriterStrategy;
	}

	@Required
	public void setImpexDataWriterStrategy(final ImpexDataWriterStrategy impexDataWriterStrategy)
	{
		this.impexDataWriterStrategy = impexDataWriterStrategy;
	}

	public Map<String, Integer> getClassToPosition()
	{
		return classToPosition;
	}

	@Required
	public void setClassToPosition(final Map<String, Integer> classToPosition)
	{
		this.classToPosition = classToPosition;
	}

	public AttributeConfigProviderStrategy getAttributeConfigProviderStrategy()
	{
		return attributeConfigProviderStrategy;
	}

	public void setAttributeConfigProviderStrategy(final AttributeConfigProviderStrategy attributeConfigProviderStrategy)
	{
		this.attributeConfigProviderStrategy = attributeConfigProviderStrategy;
	}
}
