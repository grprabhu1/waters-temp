package com.waters.service.parser.impl;

import com.waters.constants.WatersdataimportConstants;
import com.waters.product.dataimport.FileContent;
import com.waters.product.dataimport.Product;
import com.waters.product.dataimport.UomValue;
import com.waters.service.parser.ExcelFileParserService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class DefaultExcelFileParserService implements ExcelFileParserService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultExcelFileParserService.class);
	private ConfigurationService configurationService;


	/**
	 * Parses a xlsx file containing normal fileds and fields with unit of measures, returns a FileContent object
	 * Assumptions: the UOM of measure are defined in two adjacent columns. The first column has a header in the format FIELD NAME, the next column header is FIELD NAME UOM
	 *
	 * @param file     the uploaded xlsx file
	 * @param fileType string identifying the classifying categories
	 * @return FileContent object
	 */
	@Override
	public FileContent parse(final MultipartFile file, final String fileType)
	{
		final List<Product> products = new LinkedList<>();
		final Map<String, String> fields = new LinkedHashMap<>();
		final Map<String, String> uomFields = new LinkedHashMap<>();
		final Map<Integer, String> fieldsPosition = new LinkedHashMap<>();
		final Map<Integer, String> uomFieldsPosition = new LinkedHashMap<>();
		final Map<Integer, String> uomValuePosition = new LinkedHashMap<>();
		final Set<String> excludeFields = getExcludedColumns();

		try
		{
			final byte[] byteArr = file.getBytes();
			final InputStream inputStream = new ByteArrayInputStream(byteArr);
			final Workbook workbook = new XSSFWorkbook(inputStream);
			final Sheet sheet = workbook.getSheetAt(0);
			final Iterator<Row> rowsIterator = sheet.iterator();

			parseHeader(fields, uomFields, fieldsPosition, uomFieldsPosition, uomValuePosition, rowsIterator);

			parseValues(products, fields, fieldsPosition, uomFieldsPosition, uomValuePosition, rowsIterator);
		}
		catch (IOException e)
		{
			throw new RuntimeException("Error parsing file", e);
		}

		final List<String> headers = new LinkedList<>();
		headers.addAll(fields.keySet());
		headers.addAll(uomValuePosition.values());
		headers.removeAll(excludeFields);

		final FileContent fileContent = new FileContent();
		fileContent.setProducts(products);
		fileContent.setHeaders(headers);
		fileContent.setClassName(fileType);
		return fileContent;

	}


	private void parseHeader(final Map<String, String> fields, final Map<String, String> uomFields, final Map<Integer, String> fieldsPosition, final Map<Integer, String> uomFieldsPosition, final Map<Integer, String> uomValuePosition, final Iterator<Row> rowsIterator)
	{
		//parsing header
		if (rowsIterator.hasNext())
		{
			final Row currentRow = rowsIterator.next();
			final Iterator<Cell> cellIterator = currentRow.iterator();
			String previousCellContent = null;
			Integer previousCellPosition = null;
			Integer columnIndex;
			Cell cell;
			String cellContent;
			while (cellIterator.hasNext())
			{
				cell = cellIterator.next();
				columnIndex = Integer.valueOf(cell.getColumnIndex());
				cellContent = getCellContent(cell);

				if (StringUtils.containsIgnoreCase(cellContent, "UOM"))
				{
					uomFields.put(cellContent, null);
					uomFieldsPosition.put(columnIndex, cellContent);
					if (previousCellContent == null || previousCellPosition == null)
					{
						throw new RuntimeException("UOM value not found. Make sure the UOM value column precedes the UOM column");
					}
					uomValuePosition.put(previousCellPosition, previousCellContent);

					fieldsPosition.remove(previousCellPosition); //removing the uom field that was previously added to the normal fields list
					fields.remove(previousCellContent); //removing the uom field that was previously added to the normal fields list
				}
				else
				{
					fields.put(cellContent, "");
					fieldsPosition.put(columnIndex, cellContent);
				}

				previousCellContent = cellContent;
				previousCellPosition = columnIndex;
			}
		}
	}

	private void parseValues(final List<Product> products, final Map<String, String> fields, final Map<Integer, String> fieldsPosition, final Map<Integer, String> uomFieldsPosition, final Map<Integer, String> uomValuePosition, final Iterator<Row> rowsIterator)
	{
		while (rowsIterator.hasNext())
		{
			final Row currentRow = rowsIterator.next();
			final Iterator<Cell> cellIterator = currentRow.iterator();
			final HashMap<String, String> values = new LinkedHashMap<>(fields);
			final HashMap<String, UomValue> uomValues = new LinkedHashMap<>();
			Cell cell;
			while (cellIterator.hasNext())
			{
				cell = cellIterator.next();
				Integer columnIndex;
				columnIndex = Integer.valueOf(cell.getColumnIndex());
				if (uomValuePosition.get(columnIndex) != null)
				{
					final UomValue uomValue = new UomValue();
					uomValue.setValue(getCellContent(cell));
					uomValues.put(uomValuePosition.get(columnIndex), uomValue);
				}
				else if (uomFieldsPosition.get(columnIndex) != null)
				{
					final UomValue uomValue = uomValues.get(uomValuePosition.get(columnIndex - 1));
					if (uomValue == null)
					{
						LOG.warn("The uom `" + uomValuePosition.get(columnIndex - 1) + "` doesn't have value. Line: " + cell.getRowIndex() + ", column: " + cell.getColumnIndex());
					}
					else
					{
						uomValue.setUom(getCellContent(cell));
					}
				}
				else if (fieldsPosition.get(columnIndex) != null)
				{
					values.put(fieldsPosition.get(columnIndex), getCellContent(cell));
				}
				else
				{
					LOG.error("Error cell content not in the header");
				}
			}
			final Product product = new Product();
			product.setValues(values);
			product.setValuesWithUom(uomValues);
			products.add(product);
		}
	}

	private String getCellContent(final Cell cell)
	{
		if (CellType.NUMERIC.equals(cell.getCellTypeEnum()))
		{
			return NumberToTextConverter.toText(cell.getNumericCellValue());
		}
		else
		{
			cell.setCellType(CellType.STRING);
			return cell.getStringCellValue();
		}
	}

	private Set<String> getExcludedColumns()
	{
		final String excluedColumns = configurationService.getConfiguration().getString(WatersdataimportConstants.PRODUCT_PARSER_EXCLUDED_COLUMNS);
		if (StringUtils.isNotBlank(excluedColumns))
		{
			return Arrays.stream(excluedColumns.split(",")).map(String::trim).collect(Collectors.toSet());
		}
		return Collections.emptySet();
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
