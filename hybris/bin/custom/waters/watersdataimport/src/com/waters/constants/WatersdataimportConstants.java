/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.constants;

/**
 * Global class for all Watersdataimport constants. You can add global constants for your extension into this class.
 */
public final class WatersdataimportConstants extends GeneratedWatersdataimportConstants
{
	public static final String EXTENSIONNAME = "watersdataimport";

	private WatersdataimportConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PRODUCT_SHARED_FIELDS_KEY = "watersdataimport.product.shared.fields";
	public static final String PRODUCT_IDENTIFIER_FIELD_KEY = "watersdataimport.product.identifier.field";
	public static final String PRODUCT_EXCLUDED_FIELDS_KEY = "watersdataimport.product.excluded.fields";
	public static final String PRODUCT_PARSER_EXCLUDED_COLUMNS = "watersdataimport.parser.excluded.columns";

	public static final String WATERS_OVERRIDE_IMPEX = "watersdataimport.export.hook.override";

	public static final String WATERS_CLASSIFICATION_SYSTEM = "WatersClassification";
	public static final String WATERS_CLASSIFICATION_VERSION = "1.0";
}
