package com.waters.service.data;

import java.util.Optional;

public class ImpexImportResult
{
	private String impex;
	private Optional<String> fileUrl;
	private boolean success;

	public ImpexImportResult(final String impex)
	{
		this.impex = impex;
		this.fileUrl = Optional.empty();
		this.success = false;
	}

	public ImpexImportResult(final String impex, final String fileUrl)
	{
		this.impex = impex;
		this.fileUrl = Optional.of(fileUrl);
		this.success = true;
	}

	public String getImpex()
	{
		return impex;
	}

	public void setImpex(final String impex)
	{
		this.impex = impex;
	}

	public Optional<String> getFileUrl()
	{
		return fileUrl;
	}

	public void setFileUrl(final Optional<String> fileUrl)
	{
		this.fileUrl = fileUrl;
	}

	public boolean isSuccess()
	{
		return success;
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder("ImpexImportResult{");
		sb.append("impex='").append(impex).append('\'');
		sb.append(", fileUrl='").append(fileUrl).append('\'');
		sb.append(", success=").append(success);
		sb.append('}');
		return sb.toString();
	}
}
