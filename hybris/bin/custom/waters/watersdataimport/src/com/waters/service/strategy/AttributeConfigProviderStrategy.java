package com.waters.service.strategy;

import com.waters.service.data.AttributeConfig;

import java.util.Map;

public interface AttributeConfigProviderStrategy
{
	Map<String, AttributeConfig> loadAttributeConfig();
}
