package com.waters.service.strategy;

import com.waters.service.data.ImpexImportResult;

public interface AfterImpexCreatedHook
{
	void afterImpexCreated(final DataImportContext context, final ImpexImportResult result);
}
