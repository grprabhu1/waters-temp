package com.waters.service.strategy.impl;

import com.waters.DataImportUtil;
import com.waters.service.data.ImpexImportResult;
import com.waters.service.strategy.AfterImpexCreatedHook;
import com.waters.service.strategy.DataImportContext;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.waters.constants.WatersdataimportConstants.WATERS_OVERRIDE_IMPEX;

public class DefaultAfterImpexCreatedHook implements AfterImpexCreatedHook
{
	private ConfigurationService configurationService;
	private static final Logger LOG = LoggerFactory.getLogger(DefaultAfterImpexCreatedHook.class);
	private String fileName;

	@Override
	public void afterImpexCreated(final DataImportContext context, final ImpexImportResult result)
	{
		if(result.isSuccess() && getConfigurationService().getConfiguration().getBoolean(WATERS_OVERRIDE_IMPEX,false))
		{
			try
			{
				final String path = DataImportUtil.getWatersPath() + getFileName();
				final String finalPath = String.format(path, context.getClassificationClass().toLowerCase());

				Files.write(Paths.get(finalPath), result.getImpex().getBytes(Charset.forName("UTF-8")));
						}
			catch (final IOException e)
			{
				LOG.error("Error creating one of the files.", e);
			}
		}
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public String getFileName()
	{
		return fileName;
	}

	@Required
	public void setFileName(final String fileName)
	{
		this.fileName = fileName;
	}
}
