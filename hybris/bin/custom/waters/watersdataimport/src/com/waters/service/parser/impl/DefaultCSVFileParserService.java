package com.waters.service.parser.impl;

import com.waters.product.dataimport.FileContent;
import com.waters.service.data.AttributeConfig;
import com.waters.service.parser.CSVFileParserService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class DefaultCSVFileParserService implements CSVFileParserService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultCSVFileParserService.class);

	public static final String CHILD_1 = "Child 1";
	private static final String Y = "Y";

	public static final String WATERS_CLASSIFICATION_1_0_ATTRIBUTE = "Waters Classification 1.0 attribute";
	public static final String DATA_TYPE = "Data  Type";
	public static final String SORTING = "Sorting";
	public static final String MULTI_SELECT = "Multi Select";
	public static final String SEARCHABLE = "Searchable";
	public static final String FACET = "Facet";
	public static final String INTERNAL_ONLY = "Internal only";
	public static final String TRANSLATED_ATTRIBUTE_LABEL = "Translated AttributeConfig Label";
	public static final String TRANSLATED_ATTRIBUTE_VALUE = "Translated AttributeConfig Value";
	public static final String RANGE = "Range";
	public static final String POPULATED_BY_SAP = "Populated by SAP";
	public static final String REQUIRED = "Required";
	public static final String UI_FIELD_TYPE = "UI Field Type";
	public static final String TOP = "Top";
	public static final String CHILD_2 = "Child 2";
	public static final String CHILD_3 = "Child 3";
	public static final String CHILD_4 = "Child 4";
	public static final String HYBRIS_PIM_LABEL = "hybris PIM Label";
	public static final String PUBLIC_WEB_LABEL = "Public web Label";
	public static final String NOTES = "NOTES";
	public static final String HYBRIS_PRODUCT_ATTRIBUTE = "Hybris product attribute";

	@Override
	public FileContent parse(final MultipartFile file, final String fileType)
	{
		throw new UnsupportedOperationException("method not implemented");
	}

	@Override
	public List<AttributeConfig> parseAttributeFile(final Path file)
	{

		try
		{
			final InputStream inputStream = new ByteArrayInputStream(Files.readAllBytes(file));
			final InputStreamReader reader = new InputStreamReader(inputStream, "UTF8");

			final Iterable<CSVRecord> records = CSVFormat.RFC4180.withHeader(TOP, CHILD_1, CHILD_2, CHILD_3, CHILD_4, HYBRIS_PIM_LABEL,
				PUBLIC_WEB_LABEL, SORTING, MULTI_SELECT, SEARCHABLE, FACET, INTERNAL_ONLY, TRANSLATED_ATTRIBUTE_LABEL, TRANSLATED_ATTRIBUTE_VALUE,
				RANGE, POPULATED_BY_SAP, REQUIRED, DATA_TYPE, UI_FIELD_TYPE, NOTES, WATERS_CLASSIFICATION_1_0_ATTRIBUTE, HYBRIS_PRODUCT_ATTRIBUTE).parse(reader);
			return StreamSupport.stream(records.spliterator(), false)
				.filter(r -> r.getRecordNumber() != 1 && StringUtils.isNotBlank(r.get(WATERS_CLASSIFICATION_1_0_ATTRIBUTE)))
				.map(this::populateAttribute).collect(Collectors.toList());

		}
		catch (IOException e)
		{
			LOG.error("Error parsing file", e);
		}

		return Collections.emptyList();
	}

	private AttributeConfig populateAttribute(final CSVRecord r)
	{
		final AttributeConfig attribute = new AttributeConfig();
		attribute.setCode(r.get(WATERS_CLASSIFICATION_1_0_ATTRIBUTE));
		try
		{
			attribute.setSorting(Integer.parseInt(r.get(SORTING).trim()));
		}
		catch (final Exception e)
		{
			attribute.setSorting(0);
		}
		attribute.setMultiSelect(Y.equalsIgnoreCase(r.get(MULTI_SELECT).trim()));
		attribute.setSearchable(Y.equalsIgnoreCase(r.get(SEARCHABLE).trim()));
		attribute.setFacet(Y.equalsIgnoreCase(r.get(FACET).trim()));
		attribute.setInternalOnly(Y.equalsIgnoreCase(r.get(INTERNAL_ONLY).trim()));
		attribute.setTranslatedAttributeLabel(Y.equalsIgnoreCase(r.get(TRANSLATED_ATTRIBUTE_LABEL).trim()));
		attribute.setTranslatedAttributeValue(Y.equalsIgnoreCase(r.get(TRANSLATED_ATTRIBUTE_VALUE).trim()));
		attribute.setRange(r.get(RANGE).trim());
		attribute.setPopulatedBySAP(Y.equalsIgnoreCase(r.get(POPULATED_BY_SAP).trim()));
		attribute.setPopulatedBySAP(Y.equalsIgnoreCase(r.get(REQUIRED).trim()));
		attribute.setDataType(r.get(DATA_TYPE).trim());
		attribute.setFieldType(r.get(UI_FIELD_TYPE).trim());
		attribute.setReadOnly(Y.equalsIgnoreCase(r.get(POPULATED_BY_SAP).trim()));
		attribute.setPublicLabel(r.get(PUBLIC_WEB_LABEL).trim());
		return attribute;
	}

}
