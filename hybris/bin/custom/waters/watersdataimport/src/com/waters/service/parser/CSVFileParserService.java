package com.waters.service.parser;

import com.waters.service.data.AttributeConfig;

import java.nio.file.Path;
import java.util.List;

public interface CSVFileParserService extends FileParserService
{
	List<AttributeConfig> parseAttributeFile(final Path file);
}
