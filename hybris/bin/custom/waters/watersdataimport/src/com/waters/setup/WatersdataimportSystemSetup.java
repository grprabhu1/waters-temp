/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.setup;

import com.waters.constants.WatersdataimportConstants;
import de.hybris.platform.core.initialization.SystemSetup;


@SystemSetup(extension = WatersdataimportConstants.EXTENSIONNAME)
public class WatersdataimportSystemSetup
{
	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{

	}
}
