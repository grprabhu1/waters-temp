package com.waters.service.strategy.impl;

import com.waters.service.data.AttributeConfig;
import com.waters.service.data.ClassificationAttributeData;
import com.waters.service.data.ClassificationAttributeValueData;
import com.waters.service.strategy.ClassificationExportWriter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DefaultConfluenceClassificationExportWriter implements ClassificationExportWriter
{
	private static final List<String> headers = Arrays.asList("Hybris PIM Label", "Public web Label",
		"Sorting", "Multi Select", "Searchable", "Internal only", "Facet", "Translated Attribute Label", "Translated Attribute Value", "Populated by SAP",
		"Required", "UI Field Type", "Waters Classification 1.0 attribute", "Values", "Unit Of Measure", "Unit of Measure Values");

	private static final String WHITE_SPACE = " ";

	private String buildLine(final ClassificationAttributeData a)
	{
		final List<String> data = new LinkedList<>();
		final AttributeConfig config = a.getAttributeConfig();
		data.add(a.getName());
		if (StringUtils.isNotBlank(config.getPublicLabel()))
		{
			data.add(config.getPublicLabel());
		}
		else
		{
			data.add(WHITE_SPACE);
		}
		data.add(Integer.toString(config.getSorting()));
		data.add(convertBoolean(config.isMultiSelect()));
		data.add(convertBoolean(config.isSearchable()));
		data.add(convertBoolean(config.isInternalOnly()));
		data.add(convertBoolean(config.isFacet()));
		data.add(convertBoolean(config.isTranslatedAttributeLabel()));
		data.add(convertBoolean(config.isTranslatedAttributeValue()));
		data.add(convertBoolean(config.isPopulatedBySAP()));
		data.add(convertBoolean(config.isRequired()));
		data.add(config.getDataType());
		data.add(a.getCode());

		if (CollectionUtils.isNotEmpty(a.getValues()))
		{
			data.add(a.getValues().stream().map(ClassificationAttributeValueData::getData).map(d -> d.replace("|", "\\|")).collect(Collectors.joining(",")));
		}
		else
		{
			data.add(WHITE_SPACE);
		}
		if (CollectionUtils.isNotEmpty(a.getUoms()))
		{
			data.add(convertBoolean(CollectionUtils.isNotEmpty(a.getUoms())));
			data.add(a.getUoms().stream().collect(Collectors.joining(",")));
		}
		else
		{
			data.add("N");
			data.add(WHITE_SPACE);
		}

		return data.stream().collect(Collectors.joining("|", "|", "|"));
	}

	@Override
	public String exportClassificationData(final Map<String, List<ClassificationAttributeData>> classificationData)
	{
		final List<String> lines = classificationData.entrySet().stream().flatMap(this::populateClassificationClassData).collect(Collectors.toList());

		return lines.stream().collect(Collectors.joining("\n"));
	}

	public Stream<String> populateClassificationClassData(final Map.Entry<String, List<ClassificationAttributeData>> entry)
	{
		return Stream.concat(buildHeaders(entry.getKey(), entry.getValue().get(0)), entry.getValue().stream().map(this::buildLine));
	}

	private Stream<String> buildHeaders(final String key, final ClassificationAttributeData oneData)
	{
		final List<String> classHeaders = new LinkedList<>();
		classHeaders.add("h1. " + key);
		classHeaders.add("Parent classification classes: "+oneData.getParentClasses().stream().collect(Collectors.joining(",", "*", "*")));
		classHeaders.add(WHITE_SPACE);
		classHeaders.add(headers.stream().collect(Collectors.joining("||", "||", "||")));
		return classHeaders.stream();
	}



	private String convertBoolean(final boolean value)
	{
		return value ? "Y" : "N";
	}


}
