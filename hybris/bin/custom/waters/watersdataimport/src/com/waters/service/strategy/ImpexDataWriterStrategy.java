package com.waters.service.strategy;

import com.waters.product.dataimport.FileContent;
import com.waters.service.data.ImpexImportResult;
import com.waters.service.data.ProductValidationResult;

import java.util.List;
import java.util.Map;

public interface ImpexDataWriterStrategy
{
	ImpexImportResult writeToImpex(final List<Map<String, ProductValidationResult>> products, final FileContent fileContent, final DataImportContext context);
}
