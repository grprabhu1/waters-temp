package com.waters.service.impl;

import com.waters.hybris.core.util.StreamUtil;
import com.waters.service.WatersDataExportService;
import com.waters.service.data.AttributeConfig;
import com.waters.service.data.ClassificationAttributeData;
import com.waters.service.data.ClassificationAttributeValueData;
import com.waters.service.strategy.ClassificationExportWriter;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.waters.constants.WatersdataimportConstants.WATERS_CLASSIFICATION_SYSTEM;
import static com.waters.constants.WatersdataimportConstants.WATERS_CLASSIFICATION_VERSION;

public class DefaultWatersDataExportService implements WatersDataExportService
{
	private ConfigurationService configurationService;
	private ClassificationSystemService classificationSystemService;
	private ClassificationService classificationService;

	private ClassificationExportWriter classificationExportWriter;

	private static final Logger LOG = LoggerFactory.getLogger(DefaultWatersDataExportService.class);


	@Override
	public String getClassificationAttributes()
	{
		try
		{
			JaloSession.getCurrentSession().createLocalSessionContext().setAttribute("disableRestrictions", Boolean.TRUE);

			final ClassificationSystemVersionModel classificationSystemVersionModel = getClassificationSystemService().getSystemVersion(WATERS_CLASSIFICATION_SYSTEM, WATERS_CLASSIFICATION_VERSION);

			final Map<String, List<ClassificationAttributeData>> result = getClassificationSystemService().getRootClassesForSystemVersion(classificationSystemVersionModel).stream()
				.flatMap(this::getSubClasses)
				.distinct()
				.flatMap(this::getClassificationAttributes)
				.collect(Collectors.groupingBy(ClassificationAttributeData::getClassificationClassName, LinkedHashMap::new, Collectors.toList()));

			return getClassificationExportWriter().exportClassificationData(result);

		}
		catch (final UnknownIdentifierException e)
		{
			LOG.debug("Error, classification not found", e);
			throw e;
		}
		finally
		{
			JaloSession.getCurrentSession().createLocalSessionContext().setAttribute("disableRestrictions", Boolean.FALSE);
		}
	}

	private Stream<ClassificationClassModel> getSubClasses(final ClassificationClassModel classificationClassModel)
	{
		final Stream<ClassificationClassModel> subClasses = classificationClassModel.getCategories().stream()
			.filter(ClassificationClassModel.class::isInstance)
			.map(ClassificationClassModel.class::cast)
			.flatMap(this::getSubClasses);

		return Stream.concat(Stream.of(classificationClassModel), subClasses);
	}

	protected Stream<ClassificationAttributeData> getClassificationAttributes(final ClassificationClassModel classificationClassModel)
	{
		final Collection<ClassificationAttributeUnitModel> classificationUnits = getClassificationService().getAttributeUnits(classificationClassModel.getCatalogVersion());
		final Map<String, List<ClassificationAttributeUnitModel>> classificationUnitsByType = classificationUnits.stream().filter(u -> org.apache.commons.lang3.StringUtils.isNotBlank(u.getUnitType())).collect(Collectors.groupingBy(ClassificationAttributeUnitModel::getUnitType));

		final List<ClassificationAttributeData> classificationAttributeData = StreamUtil.nullSafe(classificationClassModel.getDeclaredClassificationAttributeAssignments())
			.map(att -> buildClassificationAttribute(att, classificationUnitsByType))
			.collect(Collectors.toList());

		final List<String> parentClasses = getSuperClasses(classificationClassModel).map(ClassificationClassModel::getName).collect(Collectors.toList());

		classificationAttributeData.forEach(data -> data.setParentClasses(parentClasses));

		return classificationAttributeData.stream();
	}

	private Stream<ClassificationClassModel> getSuperClasses(final ClassificationClassModel classificationClassModel)
	{
		return classificationClassModel.getSupercategories().stream()
			.filter(ClassificationClassModel.class::isInstance)
			.limit(1)
			.map(ClassificationClassModel.class::cast)
			.flatMap(c -> Stream.concat(Stream.of(c), this.getSuperClasses(c)));
	}


	private ClassificationAttributeData buildClassificationAttribute(final ClassAttributeAssignmentModel classAttributeAssignmentModel, final Map<String, List<ClassificationAttributeUnitModel>> classificationUnitsByType)
	{
		final ClassificationAttributeModel classificationAttributeModel = classAttributeAssignmentModel.getClassificationAttribute();
		final ClassificationAttributeData attributeData = new ClassificationAttributeData();
		final AttributeConfig attributeConfig = new AttributeConfig();
		attributeData.setAttributeConfig(attributeConfig);
		attributeData.setClassificationClassName(classAttributeAssignmentModel.getClassificationClass().getName());
		attributeData.setCode(classificationAttributeModel.getCode());
		attributeData.setName(classificationAttributeModel.getName());
		if (StringUtils.isNotBlank(classificationAttributeModel.getPublicWebLabel()))
		{
			attributeConfig.setPublicLabel(classificationAttributeModel.getPublicWebLabel());
		}
		else
		{
			attributeConfig.setPublicLabel(StringUtils.EMPTY);
		}
		attributeConfig.setReadOnly(classAttributeAssignmentModel.isReadOnly());
		attributeConfig.setFieldType(classAttributeAssignmentModel.getAttributeType().getCode());
		attributeConfig.setCode(classificationAttributeModel.getCode());
		attributeConfig.setDataType(classAttributeAssignmentModel.getAttributeType().getCode());
		attributeConfig.setFacet(classAttributeAssignmentModel.isFacet());
		attributeConfig.setInternalOnly(classAttributeAssignmentModel.isInternalOnly());
		attributeConfig.setRequired(BooleanUtils.toBoolean(classAttributeAssignmentModel.getMandatory()));
		attributeConfig.setSearchable(BooleanUtils.toBoolean(classAttributeAssignmentModel.getSearchable()));
		attributeConfig.setSorting(classAttributeAssignmentModel.getPosition() != null ? classAttributeAssignmentModel.getPosition().intValue() : 0);
		attributeConfig.setTranslatedAttributeLabel(Boolean.TRUE.booleanValue());
		attributeConfig.setTranslatedAttributeValue(BooleanUtils.toBoolean(classAttributeAssignmentModel.getLocalized()));
		attributeConfig.setMultiSelect(BooleanUtils.toBoolean(classAttributeAssignmentModel.getMultiValued()));

		Optional.ofNullable(classAttributeAssignmentModel.getUnit())
			.map(ClassificationAttributeUnitModel::getUnitType)
			.ifPresent(attributeConfig::setUnitCode);

		if (StringUtils.isNotBlank(attributeConfig.getUnitCode()))
		{
			final Set<String> units = classificationUnitsByType.getOrDefault(attributeConfig.getUnitCode(), Collections.emptyList()).stream()
				.map(ClassificationAttributeUnitModel::getSymbol)
				.collect(Collectors.toSet());

			attributeData.setUoms(units);
		}

		final Set<ClassificationAttributeValueData> values = StreamUtil.nullSafe(classAttributeAssignmentModel.getAttributeValues()).map(this::buildClassificationAttributeValue).collect(Collectors.toSet());
		attributeData.setValues(values);

		return attributeData;
	}

	private ClassificationAttributeValueData buildClassificationAttributeValue(final ClassificationAttributeValueModel classificationAttributeValueModel)
	{
		final ClassificationAttributeValueData classificationAttributeValueData = new ClassificationAttributeValueData();
		classificationAttributeValueData.setCode(classificationAttributeValueModel.getCode());
		classificationAttributeValueData.setData(classificationAttributeValueModel.getName());
		return classificationAttributeValueData;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public ClassificationSystemService getClassificationSystemService()
	{
		return classificationSystemService;
	}

	@Required
	public void setClassificationSystemService(final ClassificationSystemService classificationSystemService)
	{
		this.classificationSystemService = classificationSystemService;
	}

	public ClassificationService getClassificationService()
	{
		return classificationService;
	}

	@Required
	public void setClassificationService(final ClassificationService classificationService)
	{
		this.classificationService = classificationService;
	}

	public ClassificationExportWriter getClassificationExportWriter()
	{
		return classificationExportWriter;
	}

	@Required
	public void setClassificationExportWriter(final ClassificationExportWriter classificationExportWriter)
	{
		this.classificationExportWriter = classificationExportWriter;
	}
}
