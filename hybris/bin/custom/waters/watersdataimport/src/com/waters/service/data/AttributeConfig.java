package com.waters.service.data;

import org.apache.commons.lang.StringUtils;

public class AttributeConfig
{

    private String code;

    private int sorting;
    private boolean multiSelect;
    private boolean searchable;
    private boolean facet;
    private boolean internalOnly;
    private boolean translatedAttributeLabel;
    private boolean translatedAttributeValue;
    private String range;
    private boolean populatedBySAP;
    private boolean required;
    private boolean readOnly;
    private String dataType;
    private String fieldType;
    private String unitCode;
    private String publicLabel = StringUtils.EMPTY;

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public int getSorting() {
        return sorting;
    }

    public void setSorting(final int sorting) {
        this.sorting = sorting;
    }

    public boolean isMultiSelect() {
        return multiSelect;
    }

    public void setMultiSelect(final boolean multiSelect) {
        this.multiSelect = multiSelect;
    }

    public boolean isSearchable() {
        return searchable;
    }

    public void setSearchable(final boolean searchable) {
        this.searchable = searchable;
    }

    public boolean isFacet() {
        return facet;
    }

    public void setFacet(final boolean facet) {
        this.facet = facet;
    }

    public boolean isInternalOnly() {
        return internalOnly;
    }

    public void setInternalOnly(final boolean internalOnly) {
        this.internalOnly = internalOnly;
    }

    public boolean isTranslatedAttributeLabel() {
        return translatedAttributeLabel;
    }

    public void setTranslatedAttributeLabel(final boolean translatedAttributeLabel) {
        this.translatedAttributeLabel = translatedAttributeLabel;
    }

    public boolean isTranslatedAttributeValue() {
        return translatedAttributeValue;
    }

    public void setTranslatedAttributeValue(final boolean translatedAttributeValue) {
        this.translatedAttributeValue = translatedAttributeValue;
    }

    public String getRange() {
        return range;
    }

    public void setRange(final String range) {
        this.range = range;
    }

    public boolean isPopulatedBySAP() {
        return populatedBySAP;
    }

    public void setPopulatedBySAP(final boolean populatedBySAP) {
        this.populatedBySAP = populatedBySAP;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(final boolean required) {
        this.required = required;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(final String dataType) {
        this.dataType = dataType;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(final String fieldType) {
        this.fieldType = fieldType;
    }

    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(final String unitCode) {
        this.unitCode = unitCode;
    }

	public boolean isReadOnly()
	{
		return readOnly;
	}

	public void setReadOnly(final boolean readOnly)
	{
		this.readOnly = readOnly;
	}

	public String getPublicLabel()
	{
		return publicLabel;
	}

	public void setPublicLabel(final String publicLabel)
	{
		this.publicLabel = publicLabel;
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder("AttributeConfig{");
		sb.append("code='").append(code).append('\'');
		sb.append(", sorting=").append(sorting);
		sb.append(", multiSelect=").append(multiSelect);
		sb.append(", searchable=").append(searchable);
		sb.append(", facet=").append(facet);
		sb.append(", internalOnly=").append(internalOnly);
		sb.append(", translatedAttributeLabel=").append(translatedAttributeLabel);
		sb.append(", translatedAttributeValue=").append(translatedAttributeValue);
		sb.append(", range='").append(range).append('\'');
		sb.append(", populatedBySAP=").append(populatedBySAP);
		sb.append(", required=").append(required);
		sb.append(", readOnly=").append(readOnly);
		sb.append(", dataType='").append(dataType).append('\'');
		sb.append(", fieldType='").append(fieldType).append('\'');
		sb.append(", unitCode='").append(unitCode).append('\'');
		sb.append(", publicLabel='").append(publicLabel).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
