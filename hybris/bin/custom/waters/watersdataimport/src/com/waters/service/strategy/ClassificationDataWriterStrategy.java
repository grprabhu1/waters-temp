package com.waters.service.strategy;

import com.waters.service.data.ClassificationAttributeData;
import com.waters.service.data.ImpexImportResult;

import java.util.Collection;

public interface ClassificationDataWriterStrategy
{
	ImpexImportResult writeClassificationData(final Collection<ClassificationAttributeData> attributesData, final DataImportContext context);
}
