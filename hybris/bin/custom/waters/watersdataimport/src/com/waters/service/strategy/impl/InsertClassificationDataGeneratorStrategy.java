package com.waters.service.strategy.impl;

import com.waters.DataImportUtil;
import com.waters.product.dataimport.Product;
import com.waters.product.dataimport.UomValue;
import com.waters.service.data.ClassificationAttributeData;
import com.waters.service.data.ClassificationAttributeValueData;
import com.waters.service.strategy.ClassificationDataGeneratorStrategy;
import com.waters.service.strategy.DataImportContext;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InsertClassificationDataGeneratorStrategy implements ClassificationDataGeneratorStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(InsertClassificationDataGeneratorStrategy.class);

	@Override
	public Collection<ClassificationAttributeData> generateClassificationData(final Collection<Product> products, final DataImportContext context)
	{
		final Map<String, List<Map.Entry<String, String>>> fields = products.stream()
			.filter(p -> MapUtils.isNotEmpty(p.getValues()))
			.flatMap(p -> p.getValues().entrySet().stream())
			.collect(Collectors.groupingBy(Map.Entry::getKey, Collectors.toList()));

		final Map<String, List<Map.Entry<String, UomValue>>> fieldsWithUom = products.stream()
			.filter(p -> MapUtils.isNotEmpty(p.getValuesWithUom()))
			.flatMap(p -> p.getValuesWithUom().entrySet().stream())
			.collect(Collectors.groupingBy(Map.Entry::getKey, Collectors.toList()));

		final Stream<ClassificationAttributeData> classificationDatas = fields.entrySet().stream()
			.filter(f -> !context.getIdentifierField().equals(f.getKey()))
			.filter(f -> !context.getExcludeFields().contains(f.getKey()))
			.filter(f -> StringUtils.isNotBlank(f.getKey()))
			.map(c -> buildClassificationData(c, context));

		final Stream<ClassificationAttributeData> classificationDatasUom = fieldsWithUom.entrySet().stream()
			.map(c -> buildClassificationDataUom(c, context));

		return Stream.concat(classificationDatas, classificationDatasUom).collect(Collectors.toList());
	}

	private ClassificationAttributeData buildClassificationDataUom(final Map.Entry<String, List<Map.Entry<String, UomValue>>> field, final DataImportContext context)
	{
		final List<Map.Entry<String, String>> entries = field.getValue().stream().map(e -> TempEntry.valueOf(e.getKey(), e.getValue().getValue())).collect(Collectors.toList());

		final ClassificationAttributeData classificationAttributeData = buildClassificationData(TempEntry.valueOf(field.getKey(), entries), context);

		final Set<String> uoms = field.getValue().stream()
			.map(e -> e.getValue().getUom())
			.filter(StringUtils::isNotEmpty)
			.map(String::trim)
			.collect(Collectors.toSet());

		classificationAttributeData.setUoms(uoms);

		return classificationAttributeData;
	}

	private ClassificationAttributeData buildClassificationData(final Map.Entry<String, List<Map.Entry<String, String>>> field, final DataImportContext context)
	{
		final String code = DataImportUtil.getAttributeCode(field.getKey());
		if (context.getSharedFields().contains(field.getKey()) && context.getClassAttributeAssignmentModels().containsKey(code))
		{
			final ClassificationAttributeData classificationData = new ClassificationAttributeData(true);
			classificationData.setCode(code);
			classificationData.setName(field.getKey());
			final Set<String> currentValues = getCurrentClassificationAttributeValues(code, context);

			final Set<ClassificationAttributeValueData> values = field.getValue().stream()
				.distinct()
				.filter(e -> StringUtils.isNotBlank(e.getValue()))
				.flatMap(this::getAttributeValue)
				.filter(e -> !currentValues.contains(e.getValue().trim().toLowerCase()))
				.map(e -> buildClassificationValue(e.getKey(), e.getValue()))
				.collect(Collectors.toSet());

			classificationData.setValues(values);

			getClassificationClassName(code, context).ifPresent(classificationData::setClassificationClassName);

			return classificationData;
		}
		else
		{
			final ClassificationAttributeData classificationData = new ClassificationAttributeData();
			classificationData.setCode(code);
			classificationData.setName(field.getKey());

			final boolean multiValue = field.getValue().stream()
				.filter(s -> StringUtils.isNotBlank(s.getValue()))
				.anyMatch(s -> s.getValue().contains(";"));

			classificationData.setMultiValue(multiValue);
			final Set<ClassificationAttributeValueData> values = field.getValue().stream()
				.distinct()
				.filter(e -> StringUtils.isNotBlank(e.getValue()))
				.flatMap(this::getAttributeValue)
				.map(e -> buildClassificationValue(e.getKey(), e.getValue()))
				.collect(Collectors.toSet());

			classificationData.setValues(values);
			return classificationData;
		}
	}

	private Stream<Map.Entry<String, String>> getAttributeValue(final Map.Entry<String, String> value)
	{
		if (value.getValue().contains(";"))
		{
			return Arrays.stream(value.getValue().split(";")).map(v -> TempEntry.valueOf(value.getKey(), v.trim()));
		}
		return Stream.of(value);
	}

	private Optional<String> getClassificationClassName(final String key, final DataImportContext context){
		return Optional.ofNullable(context.getClassAttributeAssignmentModels().get(key))
			.map(ClassAttributeAssignmentModel::getClassificationClass)
			.map(ClassificationClassModel::getCode);
	}

	private Set<String> getCurrentClassificationAttributeValues(final String key, final DataImportContext context)
	{
		if (context.getClassAttributeAssignmentModels().containsKey(key))
		{
			return context.getClassAttributeAssignmentModels().get(key).getAttributeValues().stream()
				.map(ClassificationAttributeValueModel::getName)
				.map(String::toLowerCase)
				.collect(Collectors.toSet());
		}
		LOG.error("Shared field not found: " + key);
		return Collections.emptySet();
	}

	private ClassificationAttributeValueData buildClassificationValue(final String field, final String value)
	{
		final ClassificationAttributeValueData classificationAttributeValueData = new ClassificationAttributeValueData();
		classificationAttributeValueData.setCode(String.format("%s.%s", DataImportUtil.getAttributeCode(field.trim()), DataImportUtil.getAttributeValueCode(value.trim())));
		classificationAttributeValueData.setData(value.trim());
		return classificationAttributeValueData;
	}

	private static class TempEntry<T, R> implements Map.Entry<T, R>
	{

		private T key;
		private R value;

		private TempEntry(final T key, final R value)
		{
			this.key = key;
			this.value = value;
		}

		@Override
		public T getKey()
		{
			return key;
		}

		@Override
		public R getValue()
		{
			return value;
		}

		@Override
		public R setValue(final R value)
		{
			this.value = value;
			return value;
		}

		public static <K, V> Map.Entry<K, V> valueOf(final K key, final V value)
		{
			return new TempEntry<>(key, value);
		}
	}
}
