package com.waters.service.parser;

import com.waters.product.dataimport.FileContent;
import org.springframework.web.multipart.MultipartFile;

public interface FileParserService
{
	FileContent parse(MultipartFile file, String fileType);
}
