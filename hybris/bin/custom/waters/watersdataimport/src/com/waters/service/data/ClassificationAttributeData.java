package com.waters.service.data;

import java.util.List;
import java.util.Set;

public class ClassificationAttributeData
{
	private String code;
	private String name;
	private Set<ClassificationAttributeValueData> values;
	private Set<String> uoms;
	private boolean shared;
	private boolean multiValue;
	private String classificationClassName;
	private AttributeConfig attributeConfig;
	private List<String> parentClasses;

	public ClassificationAttributeData()
	{
		// Empty constructor
	}

	public ClassificationAttributeData(final boolean shared)
	{
		this.shared = shared;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public Set<ClassificationAttributeValueData> getValues()
	{
		return values;
	}

	public void setValues(final Set<ClassificationAttributeValueData> values)
	{
		this.values = values;
	}


	public Set<String> getUoms()
	{
		return uoms;
	}

	public void setUoms(final Set<String> uoms)
	{
		this.uoms = uoms;
	}

	public boolean isShared()
	{
		return shared;
	}

	public boolean isMultiValue()
	{
		return multiValue;
	}

	public void setMultiValue(final boolean multiValue)
	{
		this.multiValue = multiValue;
	}

	public String getClassificationClassName()
	{
		return classificationClassName;
	}

	public void setClassificationClassName(final String classificationClassName)
	{
		this.classificationClassName = classificationClassName;
	}

	public AttributeConfig getAttributeConfig()
	{
		return attributeConfig;
	}

	public void setAttributeConfig(final AttributeConfig attributeConfig)
	{
		this.attributeConfig = attributeConfig;
	}

	public List<String> getParentClasses()
	{
		return parentClasses;
	}

	public void setParentClasses(final List<String> parentClasses)
	{
		this.parentClasses = parentClasses;
	}
}
