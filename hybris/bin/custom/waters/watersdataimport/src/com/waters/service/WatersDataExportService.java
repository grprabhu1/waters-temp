package com.waters.service;

public interface WatersDataExportService
{
	String getClassificationAttributes();
}
