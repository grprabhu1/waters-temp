package com.waters.service;

import com.waters.product.dataimport.FileContent;
import com.waters.service.data.ImpexImportResult;

public interface WatersDataImportImpexService
{
	ImpexImportResult generateClassificationAttributesImpex(final FileContent fileContent);
	ImpexImportResult generateProductsImpex(final FileContent fileContent);
}
