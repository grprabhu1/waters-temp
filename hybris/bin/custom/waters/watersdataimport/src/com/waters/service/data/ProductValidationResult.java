package com.waters.service.data;

import java.util.Collections;
import java.util.Set;

public class ProductValidationResult
{
	private String field;
	private Set<String> translatedValue;
	private Set<String> value;
	private boolean translated;

	public String getField()
	{
		return field;
	}

	public void setField(final String field)
	{
		this.field = field;
	}

	public Set<String> getTranslatedValue()
	{
		return translatedValue;
	}

	public void setTranslatedValue(final Set<String> translatedValue)
	{
		this.translatedValue = translatedValue;
	}

	public Set<String> getValue()
	{
		return value;
	}

	public void setValue(final Set<String> value)
	{
		this.value = value;
	}

	public boolean isTranslated()
	{
		return translated;
	}

	public void setTranslated(final boolean translated)
	{
		this.translated = translated;
	}

	public static ProductValidationResult buildSuccess(final String field, final String translatedValue){
		return buildSuccess(field, Collections.singleton(translatedValue));
	}

	public static ProductValidationResult buildSuccess(final String field, final Set<String> translatedValue){
		final ProductValidationResult productValidationResult = new ProductValidationResult();
		productValidationResult.setField(field);
		productValidationResult.setTranslatedValue(translatedValue);
		productValidationResult.setTranslated(true);
		return productValidationResult;
	}

	public static ProductValidationResult buildFailure(final String field, final String value){
		return buildFailure(field, Collections.singleton(value));
	}

	public static ProductValidationResult buildFailure(final String field, final Set<String> value){
		final ProductValidationResult productValidationResult = new ProductValidationResult();
		productValidationResult.setField(field);
		productValidationResult.setValue(value);
		return productValidationResult;
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder("ProductValidationResult{");
		sb.append("field='").append(field).append('\'');
		sb.append(", translatedValue=").append(translatedValue);
		sb.append(", value=").append(value);
		sb.append(", translated=").append(translated);
		sb.append('}');
		return sb.toString();
	}
}
