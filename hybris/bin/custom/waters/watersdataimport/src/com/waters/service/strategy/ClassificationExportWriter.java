package com.waters.service.strategy;

import com.waters.service.data.ClassificationAttributeData;

import java.util.List;
import java.util.Map;

public interface ClassificationExportWriter
{
	String exportClassificationData(final Map<String, List<ClassificationAttributeData>> classificationData);
}
