package com.waters.service.strategy;

import com.waters.product.dataimport.Product;
import com.waters.service.data.ClassificationAttributeData;

import java.util.Collection;

public interface ClassificationDataGeneratorStrategy
{
	Collection<ClassificationAttributeData> generateClassificationData(final Collection<Product> products, final DataImportContext context);
}
