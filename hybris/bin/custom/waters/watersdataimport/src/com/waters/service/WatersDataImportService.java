package com.waters.service;

import com.waters.service.data.ImpexImportResult;
import org.springframework.web.multipart.MultipartFile;

public interface WatersDataImportService
{
	ImpexImportResult generateProductsImpexFromFile(final MultipartFile file, final String fileType);
	ImpexImportResult generateClassificationAttributesImpexFromFile(final MultipartFile file, final String fileType, final String importMode);
	ImpexImportResult generateAndRun(final MultipartFile file, final String fileType);
	ImpexImportResult exportClassificationAttributes();
}
