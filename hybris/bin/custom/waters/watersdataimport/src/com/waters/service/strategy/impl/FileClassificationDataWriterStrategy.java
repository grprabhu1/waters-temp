package com.waters.service.strategy.impl;

import com.waters.DataImportUtil;
import com.waters.service.data.AttributeConfig;
import com.waters.service.data.ClassificationAttributeData;
import com.waters.service.data.ClassificationAttributeValueData;
import com.waters.service.data.ImpexImportResult;
import com.waters.service.strategy.AfterImpexCreatedHook;
import com.waters.service.strategy.ClassificationDataWriterStrategy;
import com.waters.service.strategy.DataImportContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FileClassificationDataWriterStrategy implements ClassificationDataWriterStrategy
{
	private static final String NEW_LINE = "\n";
	private static final Logger LOG = LoggerFactory.getLogger(FileClassificationDataWriterStrategy.class);

	private static final Predicate<ClassificationAttributeData> IS_NOT_EMPTY = p -> CollectionUtils.isNotEmpty(p.getValues()) && StringUtils.isNotBlank(p.getCode());
	private static final String ENUM_TYPE = "enum";
	private static final String BOOLEAN_TYPE = "boolean";

	private List<AfterImpexCreatedHook> afterImpexCreatedHooks;

	@Override
	public ImpexImportResult writeClassificationData(final Collection<ClassificationAttributeData> attributesData, final DataImportContext context)
	{
		try
		{

			final List<ClassificationAttributeData> filterAttributes = attributesData.stream()
				.filter(IS_NOT_EMPTY)
				.collect(Collectors.toList());

			final List<ClassificationAttributeData> notSharedAttributes = attributesData.stream()
				.filter(a -> !a.isShared())
				.collect(Collectors.toList());

			final String units = filterAttributes.stream()
				.filter(c -> CollectionUtils.isNotEmpty(c.getUoms()))
				.map(this::printUnit)
				.collect(Collectors.joining(NEW_LINE));

			final String attributeValues = filterAttributes.stream()
				.map(this::printAttributeValues)
				.sorted()
				.collect(Collectors.joining(NEW_LINE));

			final String attributes = notSharedAttributes.stream()
				.map(a -> printAttribute(a, context))
				.collect(Collectors.joining(NEW_LINE));

			final StringBuilder mapping = new StringBuilder();
			final StringBuilder sharedFieldsMapping = new StringBuilder();
			int sort = context.getInitialPosition() + 1;
			for (final ClassificationAttributeData attribute : attributesData)
			{
				if (!context.getSharedFields().contains(attribute.getName()))
				{
					mapping.append(printMapping(attribute, context, sort));
					mapping.append(NEW_LINE);
					sort = sort + 10;
				}
				else if (CollectionUtils.isNotEmpty(attribute.getValues()))
				{
					final String newAddedAttributes = attribute.getValues().stream().map(ClassificationAttributeValueData::getCode).collect(Collectors.joining(","));
					sharedFieldsMapping.append(String.format(";%s;%s;%s", attribute.getClassificationClassName(), attribute.getCode(), newAddedAttributes));
					sharedFieldsMapping.append(NEW_LINE);
				}
			}

			LOG.info("Creating impex");
			final String rootPath = DataImportUtil.getResourcesPath();

			String content = Files.lines(Paths.get(rootPath + "template/classification_class_template.impex")).collect(Collectors.joining(NEW_LINE));

			content = content.replace("%values%", attributeValues);
			content = content.replace("%assignment%", mapping);
			content = content.replace("%units%", units);
			content = content.replace("%attributes%", attributes);
			content = content.replace("%shared_assignment%", sharedFieldsMapping);


			final String path = rootPath + "create_classification_attributes_" + context.getClassificationClass() + ".impex";
			Files.write(Paths.get(path), content.getBytes(Charset.forName("UTF-8")));

			final ImpexImportResult result = new ImpexImportResult(content, path);

			getAfterImpexCreatedHooks().forEach(h -> h.afterImpexCreated(context, result));

			return result;
		}
		catch (final IOException e)
		{
			LOG.error("Error loading/creating files", e);
			return new ImpexImportResult("failure: " + e.getMessage(), "");
		}
	}

	private String printAttribute(final ClassificationAttributeData classificationAttributeData, final DataImportContext context)
	{
		final String publicWebLabel = getAttributeConfig(classificationAttributeData, context)
			.map(AttributeConfig::getPublicLabel)
			.orElse(StringUtils.EMPTY);

		return String.format(";;%s;%s;%s;", classificationAttributeData.getCode(), classificationAttributeData.getName(), publicWebLabel);
	}

	private String printAttributeValues(final ClassificationAttributeData classificationAttributeData)
	{
		final StringBuilder output = new StringBuilder();

		final String result = classificationAttributeData.getValues().stream()
			.map(v -> String.format(";;%s;%s;", v.getCode(), formatData(v.getData())))
			.collect(Collectors.joining(NEW_LINE));

		output.append("# AttributeConfig values for ");
		output.append(classificationAttributeData.getName());
		output.append(NEW_LINE);
		output.append(result);
		output.append(NEW_LINE);

		return output.toString();
	}

	private String formatData(final String data)
	{
		if ("\"".equalsIgnoreCase(data))
		{
			return "\"\"\"\"";
		}
		return data;
	}

	private String printUnit(final ClassificationAttributeData classificationAttributeData)
	{

		final StringBuilder units = new StringBuilder();
		final String typeCode = classificationAttributeData.getCode();
		final String unitForField = classificationAttributeData.getUoms().stream()
			.filter(StringUtils::isNotBlank)
			.map(String::trim)
			.map(s -> String.format(";;%s%s;%s;%sUOM;%s;", typeCode, formatUom(s), formatData(s), typeCode, classificationAttributeData.getName()))
			.collect(Collectors.joining(NEW_LINE));

		units.append(unitForField);
		return units.toString();
	}

	private String formatUom(final String value)
	{
		return DataImportUtil.getAttributeValueCode(value);
	}

	private String printMapping(final ClassificationAttributeData attributeData, final DataImportContext context, final int sort)
	{
		final String list = attributeData.getValues().stream()
			.map(ClassificationAttributeValueData::getCode)
			.sorted()
			.collect(Collectors.joining(","));

		final Optional<AttributeConfig> attributeConfig = getAttributeConfig(attributeData, context);

		if (attributeConfig.isPresent())
		{
			final AttributeConfig config = attributeConfig.get();
			return String.format(";%s;%s;%d;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s",
				context.getClassificationClass(), attributeData.getCode(), config.getSorting(), getType(config),
				isTranslatedAttributeValue(config), attributeData.isMultiValue(), config.isSearchable(), isRequired(config),
				list, config.isInternalOnly(), getUnit(attributeData), config.isReadOnly(), config.isFacet());
		}
		else
		{
			LOG.error("Attribute config for " + context.getClassificationClass().toLowerCase() + "." + attributeData.getCode().toLowerCase() + " or " + attributeData.getCode().toLowerCase() + " not found");

			return String.format(";%s;%s;%d;%s;%s;%s;%s;%s;%s;%s;%s;;;",
				context.getClassificationClass(), attributeData.getCode(), sort, ENUM_TYPE,
				false, attributeData.isMultiValue(), true, false,
				list, false, getUnit(attributeData));
		}
	}

	private boolean isTranslatedAttributeValue(final AttributeConfig config)
	{
		if (ENUM_TYPE.equals(getType(config)) || BOOLEAN_TYPE.equals(getType(config)))
		{
			return false;
		}
		return config.isTranslatedAttributeValue();
	}

	private boolean isRequired(final AttributeConfig config)
	{
		return config.isRequired();
	}

	protected Optional<AttributeConfig> getAttributeConfig(final ClassificationAttributeData attributeData, final DataImportContext context)
	{

		final String attributeConfigCode = String.format("%s.%s", context.getClassificationClass().toLowerCase(), attributeData.getCode().toLowerCase());

		return Optional.ofNullable(Optional
			.ofNullable(context.getAttributeConfigMap().get(attributeConfigCode))
			.orElseGet(() -> context.getAttributeConfigMap().get(attributeData.getCode().toLowerCase())));
	}

	private String getType(final AttributeConfig config)
	{
		if ("Boolean".equalsIgnoreCase(config.getDataType()))
		{
			return BOOLEAN_TYPE;
		}
		else if ("List".equalsIgnoreCase(config.getFieldType()) || "Multiselect".equalsIgnoreCase(config.getFieldType()))
		{
			return ENUM_TYPE;
		}
		return "string";
	}

	private String getUnit(final ClassificationAttributeData attributeData)
	{
		if (CollectionUtils.isNotEmpty(attributeData.getUoms()))
		{
			return String.format("%s%s", attributeData.getCode(), formatUom(attributeData.getUoms().iterator().next()));
		}
		return StringUtils.EMPTY;
	}

	public List<AfterImpexCreatedHook> getAfterImpexCreatedHooks()
	{
		return afterImpexCreatedHooks;
	}

	@Required
	public void setAfterImpexCreatedHooks(final List<AfterImpexCreatedHook> afterImpexCreatedHooks)
	{
		this.afterImpexCreatedHooks = afterImpexCreatedHooks;
	}
}
