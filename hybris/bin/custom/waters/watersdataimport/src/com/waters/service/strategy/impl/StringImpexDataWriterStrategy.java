package com.waters.service.strategy.impl;

import com.waters.DataImportUtil;
import com.waters.product.dataimport.FileContent;
import com.waters.service.data.ImpexImportResult;
import com.waters.service.data.ProductValidationResult;
import com.waters.service.strategy.AfterImpexCreatedHook;
import com.waters.service.strategy.DataImportContext;
import com.waters.service.strategy.ImpexDataWriterStrategy;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StringImpexDataWriterStrategy implements ImpexDataWriterStrategy
{
	private static final String NEW_LINE = "\n";
	private static final Logger LOG = LoggerFactory.getLogger(StringImpexDataWriterStrategy.class);

	private List<AfterImpexCreatedHook> afterImpexCreatedHooks;

	@Override
	public ImpexImportResult writeToImpex(final List<Map<String, ProductValidationResult>> products, final FileContent fileContent, final DataImportContext context)
	{
		final List<String> pendingFields = fileContent.getHeaders().stream().filter(h -> !context.getIdentifierField().equalsIgnoreCase(h)).collect(Collectors.toList());

		final List<String> mappedProducts = products.stream().map(e -> formatIdentifier(e, context) + formatProductFields(e, context, pendingFields)).collect(Collectors.toList());

		try
		{
			LOG.info("Creating impex");
			final String rootPath = DataImportUtil.getResourcesPath();

			final String path = rootPath + "template/update_product_template.impex";

			String content = Files.lines(Paths.get(path)).collect(Collectors.joining(NEW_LINE));

			final String headers = pendingFields.stream().map(f -> translateFieldHeader(f, context)).collect(Collectors.joining(";"));

			content = content + headers + NEW_LINE;
			content = content + mappedProducts.stream().collect(Collectors.joining(NEW_LINE));
			content = content.replace("%defaultClassName%", context.getClassificationClass());


			Files.write(Paths.get(rootPath + "product_import_" + context.getClassificationClass() + ".impex"), content.getBytes(Charset.forName("UTF-8")));


			final ImpexImportResult result =  new ImpexImportResult(content, rootPath + "product_import_" + context.getClassificationClass() + ".impex");

			getAfterImpexCreatedHooks().forEach(h -> h.afterImpexCreated(context, result));

			return result;
		}
		catch (final IOException e)
		{
			LOG.error("Error while trying to write file: ", e);
		}
		return new ImpexImportResult("failure", "");
	}

	private String translateFieldHeader(final String field, final DataImportContext context)
	{
		if (StringUtils.isBlank(field) || context.getExcludeFields().contains(field))
		{
			return DataImportUtil.getAttributeCode(field);
		}
		else
		{
			return String.format("@%s[$clAttrModifiers]", DataImportUtil.getAttributeCode(field));
		}
	}

	private String formatProductFields(final Map<String, ProductValidationResult> data, final DataImportContext context, final List<String> pendingFields)
	{
		return pendingFields.stream().map(f -> formatField(f, data)).collect(Collectors.joining(";"));
	}

	private String formatField(final String field, final Map<String, ProductValidationResult> data)
	{
		if (data.containsKey(field))
		{
			return data.get(field).getTranslatedValue().stream().collect(Collectors.joining(","));
		}
		return StringUtils.EMPTY;
	}

	private String formatIdentifier(final Map<String, ProductValidationResult> product, final DataImportContext context)
	{
		return String.format(";;;%s;", product.get(context.getIdentifierField()).getTranslatedValue().iterator().next());
	}

	public List<AfterImpexCreatedHook> getAfterImpexCreatedHooks()
	{
		return afterImpexCreatedHooks;
	}

	@Required
	public void setAfterImpexCreatedHooks(final List<AfterImpexCreatedHook> afterImpexCreatedHooks)
	{
		this.afterImpexCreatedHooks = afterImpexCreatedHooks;
	}
}
