package com.waters;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataImportUtil
{
	private static final Logger LOG = LoggerFactory.getLogger(DataImportUtil.class);

	public static String getAttributeCode(final String value)
	{
		final StringBuilder builder = new StringBuilder();
		for (final char c : value.toCharArray())
		{
			if (Character.isLetterOrDigit((int) c))
			{
				builder.append(c);
			}
		}
		return builder.toString();
	}

	public static String getAttributeValueCode(final String value)
	{
		final StringBuilder builder = new StringBuilder();
		for (final char c : value.toCharArray())
		{
			if (Character.isWhitespace((int) c) || Character.valueOf(',').equals(c) || Character.valueOf('%').equals(c))
			{
				builder.append("_");
			}
			else if (Character.valueOf(':').equals(c) || Character.valueOf('"').equals(c) || Character.valueOf('/').equals(c))
			{
				builder.append("_");
			}
			else
			{
				builder.append(c);
			}
		}
		return builder.toString();
	}

	public static String getResourcesPath()
	{
		final String rootPath = DataImportUtil.class.getResource(".").getPath();
		final String finalPath = rootPath.replace("waterscore/classes/com/waters/", "watersdataimport/resources/");
		LOG.info("Resources path for Impex:" + finalPath);
		return finalPath;
	}

	public static String getWatersPath()
	{
		final String rootPath = DataImportUtil.class.getResource(".").getPath();
		final String finalPath = rootPath.replace("waterscore/classes/com/waters/", StringUtils.EMPTY);
		LOG.info("Waters path for Impex:" + finalPath);
		return finalPath;
	}
}
