package com.waters.service.strategy;

import com.waters.service.data.AttributeConfig;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class DataImportContext
{
	private String classificationClass;
	private Map<String, ClassAttributeAssignmentModel> classAttributeAssignmentModels;
	private Map<String, List<ClassificationAttributeUnitModel>> unitsPerUnitType;
	private Map<String, AttributeConfig> attributeConfigMap;
	private Set<String> sharedFields;
	private Set<String> excludeFields;
	private String identifierField;
	private int initialPosition;

	public String getClassificationClass()
	{
		return classificationClass;
	}

	public void setClassificationClass(final String classificationClass)
	{
		this.classificationClass = classificationClass;
	}

	public Map<String, ClassAttributeAssignmentModel> getClassAttributeAssignmentModels()
	{
		return classAttributeAssignmentModels;
	}

	public void setClassAttributeAssignmentModels(final Map<String, ClassAttributeAssignmentModel> classAttributeAssignmentModels)
	{
		this.classAttributeAssignmentModels = classAttributeAssignmentModels;
	}

	public Set<String> getSharedFields()
	{
		return sharedFields;
	}

	public void setSharedFields(final Set<String> sharedFields)
	{
		this.sharedFields = sharedFields;
	}

	public String getIdentifierField()
	{
		return identifierField;
	}

	public void setIdentifierField(final String identifierField)
	{
		this.identifierField = identifierField;
	}

	public Set<String> getExcludeFields()
	{
		return excludeFields;
	}

	public void setExcludeFields(final Set<String> excludeFields)
	{
		this.excludeFields = excludeFields;
	}

	public int getInitialPosition()
	{
		return initialPosition;
	}

	public void setInitialPosition(final int initialPosition)
	{
		this.initialPosition = initialPosition;
	}

	public Map<String, List<ClassificationAttributeUnitModel>> getUnitsPerUnitType()
	{
		return unitsPerUnitType;
	}

	public void setUnitsPerUnitType(final Map<String, List<ClassificationAttributeUnitModel>> unitsPerUnitType)
	{
		this.unitsPerUnitType = unitsPerUnitType;
	}

	public Map<String, AttributeConfig> getAttributeConfigMap()
	{
		return attributeConfigMap;
	}

	public void setAttributeConfigMap(final Map<String, AttributeConfig> attributeConfigMap)
	{
		this.attributeConfigMap = attributeConfigMap;
	}
}
