package com.waters.service.strategy.impl;

import com.waters.DataImportUtil;
import com.waters.service.data.AttributeConfig;
import com.waters.service.parser.CSVFileParserService;
import com.waters.service.strategy.AttributeConfigProviderStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DefaultAttributeConfigProviderStrategy implements AttributeConfigProviderStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultAttributeConfigProviderStrategy.class);

	private CSVFileParserService csvFileParserService;
	@Override
	public Map<String, AttributeConfig> loadAttributeConfig()
	{
		LOG.info("Creating impex");
		final String rootPath = DataImportUtil.getResourcesPath();

		final List<AttributeConfig> attributeConfigList = getCsvFileParserService().parseAttributeFile(Paths.get(rootPath + "dataView.csv"));

		return attributeConfigList.stream().collect(Collectors.toMap(a -> a.getCode().trim().toLowerCase(), Function.identity(), loggingMerger(), HashMap::new));
	}

	public CSVFileParserService getCsvFileParserService()
	{
		return csvFileParserService;
	}

	private static BinaryOperator<AttributeConfig> loggingMerger()
	{
		return (u, v) -> {
			LOG.warn("Duplicate attributeConfig with key: " + u.getCode());
			LOG.warn("First item found: "+u.toString());
			LOG.warn("Second item found:"+v.toString());
			return v;
		};
	}

	@Required
	public void setCsvFileParserService(final CSVFileParserService csvFileParserService)
	{
		this.csvFileParserService = csvFileParserService;
	}
}
