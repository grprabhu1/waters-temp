
	angular
		.module('watersDataImport')
		.controller('ImpexController', ImpexController);

	ImpexController.$inject = ['$scope', 'Upload', '$http'];

	function ImpexController($scope, Upload, $http) {
		$scope.isUploading = false;
		$scope.copied=false;
		$scope.autoSelectFileType = function (file, section) {
			$scope.fileFormatError = false;
			if (file) {
				var found = false;
				angular.forEach($scope.fileTypes, function (fileType) {
					if (!found && (file.name.toLowerCase().includes(fileType.key.toLowerCase()) || file.name.toLowerCase().includes(fileType.value.toLowerCase()))) {
						if (section == 'product') {
							$scope.productFileType = fileType;
						} else if (section == 'classification') {
							$scope.classificationFileType = fileType;
						}
						found = true;
					}
				});
				if (!found) {
					$scope.productFileType = null;
					$scope.classificationFileType = null;
				}
			} else {
				$scope.fileFormatError = true;
			}
		};

		$scope.submit = function (url, file, fileType, importMode) {
			$scope.results = null;
			$scope.errors = null;
			$scope.isUploading = true;
			$scope.fileDownloadPath = null;
			Upload.upload({
				url: url,
				data: {
					file: file
				},
				params: {
					fileType: fileType.key,
					importMode: (importMode ? importMode.key : null)
				}
			}).then(function (resp, headers) {
				$scope.isUploading = false;
				console.log(resp);
				$scope.results = resp.data;
				$scope.errors = null;
				console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
				console.log('Headers: ', resp.headers('Location'));
				$scope.fileDownloadPath = resp.headers('Location');
			}, function (resp) {
				console.log('Error status: ' + resp.status);
				$scope.isUploading = false;
				$scope.errors = resp;
				$scope.results = null;
			}, function (evt) {
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
			}).catch(function (err) {
				$scope.isUploading = false;
				$scope.results = null;
				$scope.errors = err;
			});
		};

		$scope.export = function (url) {
			$scope.results = null;
			$scope.errors = null;
			$scope.isUploading = true;
			$scope.fileDownloadPath = null;
			$http.get(url).then(function (resp) {
				$scope.isUploading = false;
				console.log(resp);
				$scope.results = resp.data;
				$scope.errors = null;
				$scope.fileFormatError = false;
				console.log('Success. Response: ' + resp.data);
			}, function (evt) {
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
			}).catch(function (err) {
				$scope.isUploading = false;
				$scope.results = null;
				$scope.errors = err;
			});
		};

		$scope.copyToClipboard = function (name) {
			var copyElement = document.createElement("textarea");
			copyElement.style.position = 'fixed';
			copyElement.style.opacity = '0';
			copyElement.textContent = name;
			var body = document.getElementsByTagName('body')[0];
			body.appendChild(copyElement);
			copyElement.select();
			document.execCommand('copy');
			body.removeChild(copyElement);
			$scope.copied=true;
		};

		$scope.fileTypes = [
			{"key": "ApplicationKits", "value": "Application Kits"},
			{"key": "Bulk", "value": "Bulk"},
			{"key": "Column", "value": "Column"},
			{"key": "ColumnKits", "value": "ColumnKits"},
			{"key": "ConsumablesAndSpares", "value": "Consumables And Spares"},
			{"key": "ColumnAccessories", "value": "Column Accessories"},
			{"key": "Containers", "value": "Containers"},
			{"key": "Filters", "value": "Filters"},
			{"key": "LabEquipment", "value": "Lab Equipment"},
			{"key": "Plates", "value": "Plates"},
			{"key": "Primers", "value": "Primers"},
			{"key": "Quechers", "value": "Quechers"},
			{"key": "SmallComponents", "value": "Small Components"},
			{"key": "Software", "value": "Software"},
			{"key": "SPE", "value": "SPE"},
			{"key": "SPECartridges", "value": "SPE Cartridges"},
			{"key": "SPEPlates", "value": "SPE Plates"},
			{"key": "Standards", "value": "Standards"},
			{"key": "SubAssemblies", "value": "Sub Assemblies"},
			{"key": "VialAndPlateAccessories", "value": "Vial And Plate Accessories"},
			{"key": "Vials", "value": "Vials"}
		];
		$scope.importModes = [
			{"key": "create", "value": "Create"},
			{"key": "update", "value": "Update"}
		];
	}
