<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Welcome to watersdataimport</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
		  integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
		  integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

	<link rel="stylesheet" href="static/css/dataImportStyle.css">
</head>
<body ng-app="watersDataImport" ng-controller="ImpexController">
<div class="container">

	<h2>Waters data import</h2>

	<div class="vspace30">
		<%--SPACER--%>
	</div>
	<ul class="nav nav-tabs" id="myTab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="products-tab" data-toggle="tab" href="#products" role="tab" aria-controls="products"
			   aria-selected="true">Products</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="classattr-tab" data-toggle="tab" href="#classattr" role="tab" aria-controls="classattr"
			   aria-selected="false">Classification attributes</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="classattrExport-tab" data-toggle="tab" href="#classattrExport" role="tab" aria-controls="classattrExport"
			   aria-selected="false">Export Class attributes</a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade show active" id="products" role="tabpanel" aria-labelledby="products-tab">
			<div class="vspace30"></div>
			<form ng-submit="submit('products', productFile, productFileType)" method="post" enctype="multipart/form-data">

				<%--FILE SELECTION --%>
				<div class="form-group">
					<div class="button" ngf-select ng-model="productFile"
						 ng-change="autoSelectFileType(productFile, 'product')" ngf-pattern="'.xlsx'">
						<button type="button" class="btn btn-default">
							<i class="fas fa-file-excel"></i>&nbsp;Click to select the file
						</button>
						<span>&nbsp;&nbsp;&nbsp;<code id="filename">{{productFile.name}}</code></span>
					</div>
				</div>

				<%--FILE TYPE--%>
				<label for="fileType">File type:</label>
				<div class="form-group">
					<select ng-model="productFileType"
							ng-options="fileType as fileType.value for fileType in fileTypes track by fileType.key" id="fileType"
							name="fileType" class="custom-select" required>
					</select>
				</div>

				<div class="form-group">
					<button class="btn btn-success" ng-model="uploadBtn"
							ng-disabled="isUploading || !productFile.name || !productFileType" type="submit">
						<i class="fas fa-spinner fa-spin" ng-if="isUploading"></i>
						<i class="fas fa-upload" ng-if="!isUploading"></i>&nbsp;
						<span ng-if="!isUploading">Upload</span>
						<span ng-if="isUploading"><i>Processing file...</i></span>
					</button>
				</div>
			</form>
		</div>


		<%--CLASSIFICATION ATTRIBUTES TAB--%>

		<div class="tab-pane fade" id="classattr" role="tabpanel" aria-labelledby="classattr-tab">
			<div class="vspace30"></div>
			<form ng-submit="submit('classification-attributes', classificationFile, classificationFileType, classificationImportType)" method="post"
				  enctype="multipart/form-data" novalidate>

				<%--FILE SELECTION --%>
				<div class="form-group">
					<div class="button" ngf-select ng-model="classificationFile"
						 ng-change="autoSelectFileType(classificationFile, 'classification')" ngf-pattern="'.xlsx'">
						<button type="button" class="btn btn-default">
							<i class="fas fa-file-excel"></i>&nbsp;Click to select the file
						</button>
						<span>&nbsp;&nbsp;&nbsp;<code>{{classificationFile.name}}</code></span>
					</div>
				</div>

				<%--FILE TYPE--%>
				<label for="classFileType">File type:</label>
				<div class="form-group">
					<select ng-model="classificationFileType"
							ng-options="fileType as fileType.value for fileType in fileTypes track by fileType.key"
							id="classFileType"
							name="fileType" class="custom-select" required>
					</select>
				</div>

				<%--IMPORT MODE--%>
				<label for="classImportType">Import mode:</label>
				<div class="form-group">
					<select ng-model="classificationImportType"
							ng-options="importMode as importMode.value for importMode in importModes track by importMode.key"
							id="classImportType" class="custom-select" required>
					</select>
				</div>

				<div class="form-group">
					<button class="btn btn-success" ng-model="uploadBtn"
							ng-disabled="isUploading || !classificationFile.name || !classificationFileType || !classificationImportType" type="submit">
						<i class="fas fa-spinner fa-spin" ng-if="isUploading"></i>
						<i class="fas fa-upload" ng-if="!isUploading"></i>&nbsp;
						<span ng-if="!isUploading">Upload</span>
						<span ng-if="isUploading"><i>Processing file...</i></span>
					</button>
				</div>
			</form>
		</div>

		<%--CLASSIFICATION ATTRIBUTES EXPORT--%>

		<div class="tab-pane fade" id="classattrExport" role="tabpanel" aria-labelledby="classattrExport-tab">
			<div class="vspace30"></div>
			<form ng-submit="export('export')" method="post"
				  enctype="multipart/form-data" novalidate>
				<div class="form-group">
					<button class="btn btn-success" ng-model="uploadBtn" type="submit">
						<span ng-if="!isUploading">Export</span>
						<span ng-if="isUploading"><i>Processing file...</i></span>
					</button>
				</div>
			</form>
		</div>

	</div>


	<div class="alert alert-danger" role="alert" ng-if="errors || fileFormatError">
		<h4 class="alert-heading">Error!</h4>
		<p ng-if="errors">There was an error during the import of the file</p>
		<p ng-if="fileFormatError">File format not allowed. Only xlsx files are allowed.</p>
		<hr>
		<p class="mb-0">{{errors}}</p>
	</div>

	<div ng-if="fileDownloadPath" class="file-path-box">
		<label for="filePath">Impex file path: </label>
		<div id="filePath">
			<code>{{fileDownloadPath}}</code>&nbsp;&nbsp;
			<button class="btn btn-default" ng-click="copyToClipboard(fileDownloadPath)"><i class="fas fa-paste"></i></button>
		</div>
		<div class="alert alert-success" ng-if="copied" style="margin-top: 5px;">
			Copied to clipboard!
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	</div>
	<div class="vspace30"></div>
	<div ng-if="results" class="results-box">
		<code>
			<pre>{{results}}</pre>
		</code>
	</div>


</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
		integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.10/angular.min.js"
		integrity="sha256-sk9OZF24HqebsmeR4sKCxeMatokA7KtIK4hHO60qm54=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/danialfarid-angular-file-upload/12.2.13/ng-file-upload.min.js"
		integrity="sha256-TqtYHg6/i06jaAnqVU0twQV7dROa7Um8CpqElzK9024=" crossorigin="anonymous"></script>

<script>
	angular.module('watersDataImport', ['ngFileUpload']);
</script>
<script src="static/controller/impex.controller.js"></script>
</body>
</html>
