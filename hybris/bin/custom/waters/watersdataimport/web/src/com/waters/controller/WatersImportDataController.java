/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.controller;

import com.waters.service.WatersDataImportService;
import com.waters.service.data.ImpexImportResult;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.net.URI;
import java.net.URISyntaxException;


@Controller
public class WatersImportDataController
{

	@Resource(name = "watersDataImportService")
	private WatersDataImportService watersDataImportService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String dataImport(final ModelMap model)
	{
		return "welcome";
	}

	@RequestMapping(value = "/products", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> loadProductsFile(@RequestParam("file") final MultipartFile file, @RequestParam(value = "fileType") final String fileType) throws URISyntaxException
	{
		final ImpexImportResult impexImportResult = watersDataImportService.generateProductsImpexFromFile(file, fileType);
		return ResponseEntity.created(new URI(impexImportResult.getFileUrl().orElse(""))).body(impexImportResult.getImpex());
	}

	@RequestMapping(value = "/classification-attributes", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> loadClassificationAttributesFile(@RequestParam("file") final MultipartFile file,
	                                                               @RequestParam(value = "fileType") final String fileType,
	                                                               @RequestParam(value = "importMode") final String importMode) throws URISyntaxException
	{
		final ImpexImportResult impexImportResult = watersDataImportService.generateClassificationAttributesImpexFromFile(file, fileType, importMode);
		return ResponseEntity.created(new URI(impexImportResult.getFileUrl().orElse(""))).body(impexImportResult.getImpex());
	}

	@RequestMapping(value = "/export", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> exportClassification() throws URISyntaxException
	{
		final ImpexImportResult impexImportResult = watersDataImportService.exportClassificationAttributes();
		return ResponseEntity.created(new URI(impexImportResult.getFileUrl().orElse(""))).body(impexImportResult.getImpex());
	}

}
