package com.waters.hybris.integration.prices.service;

import de.hybris.platform.core.model.c2l.CurrencyModel;

import java.util.Optional;

public interface CurrencyResolver
{
	Optional<CurrencyModel> getCurrencyForWatersCurrencyCode(String watersCurrencyCode);
}
