package com.waters.hybris.integration.prices.dao;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.europe1.model.PriceRowModel;

import java.util.List;

public interface PricesDao {
    void deletePricesNotInSequence(Long sequenceId);

    List<PriceRowModel> getPriceRows(String productId, CurrencyModel currency, UserPriceGroup upg);
}
