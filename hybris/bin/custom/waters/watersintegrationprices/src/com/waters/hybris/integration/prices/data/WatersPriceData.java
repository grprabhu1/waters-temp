package com.waters.hybris.integration.prices.data;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import static org.apache.commons.lang3.StringUtils.trim;

@JsonPropertyOrder({
        "partNumber",
        "salesOrganisation",
        "price",
        "currency",
        "country",
        "productLine"
})
public class WatersPriceData {
    private String partNumber;
    private String salesOrganisation;
    private String price;
    private String currency;
    private String country;
    private String productLine;

    @JsonCreator
    public WatersPriceData(@JsonProperty(value="partNumber", required=true) final String partNumber,
                           @JsonProperty(value="salesOrganisation", required=true) final String salesOrganisation,
                           @JsonProperty(value="price", required=true) final String price,
                           @JsonProperty(value="currency", required=true) final String currency,
                           @JsonProperty(value="country") final String country,
                           @JsonProperty(value="productLine") final String productLine) {
        this.partNumber = trim(partNumber);
        this.salesOrganisation = trim(salesOrganisation);
        this.price = trim(price);
        this.currency = trim(currency);
        this.country = trim(country);
        this.productLine = trim(productLine);
    }

    public String getPartNumber() {
        return partNumber;
    }

    public String getSalesOrganisation() {
        return salesOrganisation;
    }

    public String getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCountry() {
        return country;
    }

    public String getProductLine() {
        return productLine;
    }

}
