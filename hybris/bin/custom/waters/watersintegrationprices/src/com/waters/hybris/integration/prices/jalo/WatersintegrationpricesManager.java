package com.waters.hybris.integration.prices.jalo;

import com.waters.hybris.integration.prices.constants.WatersintegrationpricesConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

@SuppressWarnings("PMD")
public class WatersintegrationpricesManager extends GeneratedWatersintegrationpricesManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( WatersintegrationpricesManager.class.getName() );
	
	public static final WatersintegrationpricesManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (WatersintegrationpricesManager) em.getExtension(WatersintegrationpricesConstants.EXTENSIONNAME);
	}
	
}
