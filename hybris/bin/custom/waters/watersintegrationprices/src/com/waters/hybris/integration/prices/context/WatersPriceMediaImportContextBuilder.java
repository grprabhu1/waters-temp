package com.waters.hybris.integration.prices.context;

import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.util.BatchCommitter;
import com.neoworks.hybris.util.ListBackedBatchCommitter;
import com.waters.hybris.integration.core.context.MediaImportContext;
import com.waters.hybris.integration.core.context.MediaImportContextBuilder;
import com.waters.hybris.integration.core.model.record.FileImportRecordModel;
import de.hybris.platform.core.model.media.MediaModel;

public class WatersPriceMediaImportContextBuilder extends MediaImportContextBuilder {

    @Override
    public MediaImportContext createMediaImportContext(final FileImportRecordModel importRecord)
    {
        final MediaModel media = importRecord.getMedia();
        final AuditContext auditContext = getAuditService().beginAudit(getSystemArea(), null, "Importing media with code [{}]", media.getCode());
        BatchCommitter batchCommitter = null;
        if (isUseBatchCommitter())
        {
            batchCommitter = new ListBackedBatchCommitter(getBatchSize(), getModelServiceSelectionStrategy().selectModelService(getUseTransactionalModelServiceConfigKey()), true);
        }

        final long sequenceId = System.currentTimeMillis();

        final WatersPriceMediaImportContext context = new WatersPriceMediaImportContext(sequenceId, importRecord, auditContext, batchCommitter);
        initializeModelService(context);
        return context;
    }

}
