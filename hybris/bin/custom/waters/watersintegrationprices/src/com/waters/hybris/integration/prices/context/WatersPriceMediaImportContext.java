package com.waters.hybris.integration.prices.context;

import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.util.BatchCommitter;
import com.waters.hybris.integration.core.context.MediaImportContext;
import com.waters.hybris.integration.core.model.record.FileImportRecordModel;


public class WatersPriceMediaImportContext extends MediaImportContext {

    private long sequenceId;

    public WatersPriceMediaImportContext(final long sequenceId, final FileImportRecordModel importRecordModel, final AuditContext auditContext, final BatchCommitter batchCommitter) {
        super(importRecordModel, auditContext, batchCommitter);
        this.sequenceId = sequenceId;
    }

    public long getSequenceId() {
        return sequenceId;
    }

    @Override
    protected boolean isSuccess() {

        if (getTotalError() > 0) {
            return false;
        }

        return super.isSuccess();
    }

    @Override
    protected String generateErrorMessage()
    {
        return String.format("Failed to process all rows for media with code [%s]. Processed: [%d]. Success: [%d]. Errors: [%d].", getMediaCode(), getTotalProcessed(), getTotalSuccess(), getTotalError());
    }

}
