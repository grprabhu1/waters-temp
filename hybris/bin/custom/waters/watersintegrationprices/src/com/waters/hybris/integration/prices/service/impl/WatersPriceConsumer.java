package com.waters.hybris.integration.prices.service.impl;

import com.waters.hybris.integration.core.context.ImportContext;
import com.waters.hybris.integration.core.service.impl.AbstractModelServiceConfigurableConsumer;
import com.waters.hybris.integration.prices.context.WatersPriceMediaImportContext;
import com.waters.hybris.integration.prices.dao.PricesDao;
import com.waters.hybris.integration.prices.data.WatersPriceData;
import com.waters.hybris.integration.prices.service.CurrencyResolver;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import static com.waters.hybris.core.util.PredicateUtils.composeAnd;
import static java.util.Arrays.asList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.trim;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.util.StringUtils.isEmpty;

public class WatersPriceConsumer extends AbstractModelServiceConfigurableConsumer<WatersPriceData, WatersPriceMediaImportContext> {

    protected static final Predicate<WatersPriceData> partNumberPresent = pd -> !isEmpty(trim(pd.getPartNumber()));
    protected static final Predicate<WatersPriceData> pricePresent = pd -> !isEmpty(trim(pd.getPrice()));
    protected static final Predicate<WatersPriceData> salesOrganisationPresent = pd -> !isEmpty(trim(pd.getSalesOrganisation()));
    protected static final Predicate<WatersPriceData> currencyPresent = pd -> !isEmpty(trim(pd.getCurrency()));
    protected static final Optional<Predicate<WatersPriceData>> isValidPredicate = composeAnd(asList(Objects::nonNull, partNumberPresent, pricePresent, salesOrganisationPresent, currencyPresent));

    protected static final String UNIT_PIECES = "pieces";
    protected static final String IS_NET_PROPERTY = "watersintegrationprices.prices.%s.net";
    protected static final String MINQTD = "watersintegrationprices.prices.minqtd";
    protected static final String UNIT = "watersintegrationprices.prices.unit";
    protected static final String UNITFACTOR = "watersintegrationprices.prices.unitfactor";
    protected static final String COMMA = ",";
    protected static final String KEY = "%s-%s-%s";

    private ProductService productService;

    private ModelService modelService;
    private EnumerationService enumerationService;
    private CatalogVersionService catalogVersionService;
    private String catalogId;
    private String catalogVersionId;
    private UnitService unitService;
    private PricesDao pricesDao;
    private ConfigurationService configurationService;
    private CurrencyResolver currencyResolver;

	private static final Logger LOG = getLogger(WatersPriceConsumer.class);

	@Override
    public boolean isValid(final WatersPriceData item, final WatersPriceMediaImportContext context)
    {
        final boolean valid = BooleanUtils.toBoolean(isValidPredicate.map(f -> BooleanUtils.toBooleanObject(f.test(item))).orElse(Boolean.FALSE));

        if (!valid)
        {
            context.getAuditContext().addMessage("price data is not valid for part number {}", item == null ? "unknown" : item.getPartNumber());
        }

        return valid;
    }

    @Override
    public void internalProcess(final WatersPriceData item, final WatersPriceMediaImportContext context)
    {
        final Optional<ProductModel> product = getProduct(item.getPartNumber());

        boolean error = false;

        if (!product.isPresent()) {
			LOG.warn("could not locate product for part number {}", item.getPartNumber());
        }

        final Optional<CurrencyModel> currency = getCurrencyResolver().getCurrencyForWatersCurrencyCode(item.getCurrency());
        if (!currency.isPresent())
        {
	        logWarn("could not locate currency {}", item.getCurrency());
            error = true;
        }

        final Optional<UserPriceGroup> salesOrganisation = getSalesOrganisation(item.getSalesOrganisation());
        if (!salesOrganisation.isPresent())
        {
	        LOG.warn("could not locate sales organisation for {}", item.getSalesOrganisation());
            error = true;
        }

        final Optional<BigDecimal> price = parseBigDecimal(item.getPrice());
        if (!price.isPresent())
        {
			logError("not a valid price {}", item.getPrice());
            error = true;
        }

        if (error) {
            context.incrementError();
        }
        else
        {
        	final String productCode = product.map(ProductModel::getCode).orElse(item.getPartNumber());
            final String key = generateKey(productCode, currency.get(), salesOrganisation.get());

            final PriceRowModel priceRowModel = processPrice(key, context, productCode, currency.get(), price.get(), salesOrganisation.get());

            context.addToBatchAndRegister(key, priceRowModel);

            context.incrementSuccess();
            context.getAuditContext().addMessage("added priceRow for product: {} ", item.getPartNumber());
        }
	}

	protected void logError(final String error, final Object param)
	{
		LOG.error(error, param);
	}

	protected void logWarn(final String error, final Object param)
	{
		LOG.warn(error, param);
	}


    @Override
    public void postAccept(final WatersPriceMediaImportContext context)
    {
        //disable deletions while testing delta feed from SAP
        //getPricesDao().deletePricesNotInSequence(Long.valueOf(context.getSequenceId()));
    }

    protected String generateKey(final String productCode, final CurrencyModel currencyModel, final UserPriceGroup userPriceGroup) {
        return String.format(KEY, productCode, currencyModel.getIsocode(), userPriceGroup);
    }




    protected PriceRowModel processPrice(final String key, final WatersPriceMediaImportContext context, final String productCode, final CurrencyModel currency, final BigDecimal price, final UserPriceGroup so)
    {
        final PriceRowModel priceRowModel = getProcessed(context, key)
                .orElse(getExisting(productCode, currency, so)
                        .orElseGet(() -> createPriceRow(productCode, currency, so)));

        priceRowModel.setSequenceId(context.getSequenceId());
        priceRowModel.setPrice(price.doubleValue());

        return priceRowModel;

    }

    protected Optional<PriceRowModel> getExisting(final String productCode, final CurrencyModel currency, final UserPriceGroup so)
    {
        final List<PriceRowModel> priceRows = getPricesDao().getPriceRows(productCode, currency, so);

        if (CollectionUtils.isNotEmpty(priceRows))
        {
            // ignore other rows, doesn't matter as they will be deleted anyway (sequenceId)
            return of(priceRows.get(0));
        }

        return empty();
    }

    protected Optional<PriceRowModel> getProcessed(final ImportContext context, final String key)
    {
        return ofNullable((PriceRowModel)context.getProcessed(key));
    }


    protected PriceRowModel createPriceRow(final String productCode, final CurrencyModel currency, final UserPriceGroup so) {
        final PriceRowModel priceRow = getModelService().create(PriceRowModel.class);
        priceRow.setProductId(productCode);
        priceRow.setUg(so);
        priceRow.setNet(getIsNet(so));
        priceRow.setCurrency(currency);
        priceRow.setUnit(getUnit());
        priceRow.setMinqtd(getMinqtd());
        priceRow.setUnitFactor(getUnitFactor());

        return priceRow;
    }


    protected Boolean getIsNet(final UserPriceGroup userPriceGroup) {
        final String key = String.format(IS_NET_PROPERTY, userPriceGroup.getCode());
        return getConfigurationService().getConfiguration().getBoolean(key, true);
    }

    protected Long getMinqtd() {
        return getConfigurationService().getConfiguration().getLong(MINQTD, 1l);
    }

    protected UnitModel getUnit() {
        return getUnitService().getUnitForCode(getConfigurationService().getConfiguration().getString(UNIT, UNIT_PIECES));
    }

    protected Integer getUnitFactor() {
        return getConfigurationService().getConfiguration().getInteger(UNITFACTOR, 1);
    }

    protected Optional<BigDecimal> parseBigDecimal(final String price)
    {
        if(StringUtils.isNotBlank(price))
        {
            try
            {
                final String clean = price.replaceAll(COMMA, StringUtils.EMPTY);
                return of(new BigDecimal(clean));
            }
            catch (final Exception e)
            {
            }
        }
        return empty();
    }

    @SuppressFBWarnings(value = "REC_CATCH_EXCEPTION", justification = "Thanks hybris for throwing an exception when you don't find something")
    protected Optional<ProductModel> getProduct(final String productCode)
    {
        try
        {
            final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(getCatalogId(), getCatalogVersionId());
            return ofNullable(getProductService().getProductForCode(catalogVersion, productCode));
        }
        catch (final Exception e)
        {
            return empty();
        }
    }

    protected Optional<UserPriceGroup> getSalesOrganisation(final String salesOrganisation) {
		try {
			return ofNullable(getEnumerationService().getEnumerationValue(UserPriceGroup.class, salesOrganisation));
		}
		catch (final UnknownIdentifierException e)
		{
			return Optional.empty();
		}
    }

    protected ProductService getProductService() {
        return productService;
    }

    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    protected ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    protected EnumerationService getEnumerationService() {
        return enumerationService;
    }

    @Required
    public void setEnumerationService(final EnumerationService enumerationService) {
        this.enumerationService = enumerationService;
    }

    protected CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    protected String getCatalogId() {
        return catalogId;
    }

    @Required
    public void setCatalogId(final String catalogId) {
        this.catalogId = catalogId;
    }

    protected String getCatalogVersionId() {
        return catalogVersionId;
    }

    @Required
    public void setCatalogVersionId(final String catalogVersionId) {
        this.catalogVersionId = catalogVersionId;
    }

    protected UnitService getUnitService() {
        return unitService;
    }

    @Required
    public void setUnitService(final UnitService unitService) {
        this.unitService = unitService;
    }

    protected PricesDao getPricesDao() {
        return pricesDao;
    }

    @Required
    public void setPricesDao(final PricesDao pricesDao) {
        this.pricesDao = pricesDao;
    }

    protected ConfigurationService getConfigurationService() {
        return configurationService;
    }

    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

	protected CurrencyResolver getCurrencyResolver()
	{
		return currencyResolver;
	}

	@Required
	public void setCurrencyResolver(final CurrencyResolver currencyResolver)
	{
		this.currencyResolver = currencyResolver;
	}
}
