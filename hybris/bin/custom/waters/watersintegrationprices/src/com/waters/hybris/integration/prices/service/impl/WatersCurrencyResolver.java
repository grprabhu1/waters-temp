package com.waters.hybris.integration.prices.service.impl;

import com.waters.hybris.core.strategies.ConfigurationPropertyStrategy;
import com.waters.hybris.integration.prices.service.CurrencyResolver;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.springframework.beans.factory.annotation.Required;

import java.util.Map;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class WatersCurrencyResolver implements CurrencyResolver
{
	protected static final String CURRENCY_MAPPING_SAP_HYBRIS_PROPERTY = "currency.mapping.sap.hybris";

	private CommonI18NService commonI18NService;
	private ConfigurationPropertyStrategy configurationPropertyStrategy;

	@Override
	public Optional<CurrencyModel> getCurrencyForWatersCurrencyCode(final String watersCurrencyCode)
	{
		final Map<String, String> currencyMap = getConfigurationPropertyStrategy().getStringMap(CURRENCY_MAPPING_SAP_HYBRIS_PROPERTY);
		try
		{
			return ofNullable(getCommonI18NService().getCurrency(currencyMap.getOrDefault(watersCurrencyCode, watersCurrencyCode)));
		}
		catch (final Exception e)
		{
			return empty();
		}
	}

	protected CommonI18NService getCommonI18NService() {
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService) {
		this.commonI18NService = commonI18NService;
	}

	protected ConfigurationPropertyStrategy getConfigurationPropertyStrategy()
	{
		return configurationPropertyStrategy;
	}

	@Required
	public void setConfigurationPropertyStrategy(final ConfigurationPropertyStrategy configurationPropertyStrategy)
	{
		this.configurationPropertyStrategy = configurationPropertyStrategy;
	}
}
