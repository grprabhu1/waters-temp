package com.waters.hybris.integration.prices.dao.impl;

import com.waters.hybris.integration.prices.dao.PricesDao;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.search.paginated.PaginatedFlexibleSearchParameter;
import de.hybris.platform.servicelayer.search.paginated.PaginatedFlexibleSearchService;
import de.hybris.platform.servicelayer.search.paginated.util.PaginatedSearchUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.IntStream;

public class WatersPricesDao implements PricesDao {

    public static final String FIND_NON_MATCHING_SEQUENCE_IDS = "SELECT {pk} FROM {" + PriceRowModel._TYPECODE + "} WHERE {" + PriceRowModel.SEQUENCEID + "} != ?" + PriceRowModel.SEQUENCEID;

    public static final String GET_PRICE_ROW_FOR_PRODUCT_QUERY =
            "SELECT {" + PriceRowModel.PK + "}" +
                    " FROM {" + PriceRowModel._TYPECODE + "}" +
                    " WHERE {" + PriceRowModel.CURRENCY + "} = ?currency" +
                    " AND {" + PriceRowModel.PRODUCTID + "} = ?productId" +
                    " AND {" + PriceRowModel.UG + "} = ?ug";




    private PaginatedFlexibleSearchService paginatedFlexibleSearchService;
    private FlexibleSearchService flexibleSearchService;
    private ModelService modelService;


    private int pageSize;

    @Override
    public void deletePricesNotInSequence(final Long sequenceId)
    {
        final Map<String, Object> parms = new HashMap<>();
        parms.put(PriceRowModel.SEQUENCEID, sequenceId);
        // remove the first page


        final Function<Integer, SearchPageData<PriceRowModel>> findByPage = (page) -> find(parms, createPage(page));

        final SearchPageData<PriceRowModel> firstPage = findByPage.apply(0);
        getModelService().removeAll(firstPage.getResults());

        // delete all pages.
        IntStream.range(1, firstPage.getPagination().getNumberOfPages())
                .mapToObj(findByPage::apply)
                .forEach(page -> getModelService().removeAll(page.getResults()));

    }

    protected SearchPageData<PriceRowModel> find(final Map<String, Object> parms, final SearchPageData searchPageData) {
        final PaginatedFlexibleSearchParameter parameter = new PaginatedFlexibleSearchParameter();
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_NON_MATCHING_SEQUENCE_IDS);
        parameter.setFlexibleSearchQuery(query);
        parameter.getFlexibleSearchQuery().addQueryParameters(parms);
        parameter.setSearchPageData(searchPageData);
        return this.getPaginatedFlexibleSearchService().search(parameter);
    }

    protected SearchPageData<PriceRowModel> createPage(final int page) {
        return PaginatedSearchUtils.createSearchPageDataWithPagination(getPageSize(), page, true);
    }

    @Override
    public List<PriceRowModel> getPriceRows(final String productId, final CurrencyModel currency, final UserPriceGroup upg)
    {
        final Map<String, Object> params = new HashMap<>();
        params.put(PriceRowModel.CURRENCY, currency);
        params.put(PriceRowModel.PRODUCTID, productId);
        params.put(PriceRowModel.UG, upg);

        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_PRICE_ROW_FOR_PRODUCT_QUERY, params);
        final SearchResult<PriceRowModel> searchResult = getFlexibleSearchService().search(query);

        return searchResult.getResult();
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public int getPageSize() {
        return pageSize;
    }

    @Required
    public void setPageSize(final int pageSize) {
        this.pageSize = pageSize;
    }

    public PaginatedFlexibleSearchService getPaginatedFlexibleSearchService() {
        return paginatedFlexibleSearchService;
    }

    @Required
    public void setPaginatedFlexibleSearchService(final PaginatedFlexibleSearchService paginatedFlexibleSearchService) {
        this.paginatedFlexibleSearchService = paginatedFlexibleSearchService;
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}
