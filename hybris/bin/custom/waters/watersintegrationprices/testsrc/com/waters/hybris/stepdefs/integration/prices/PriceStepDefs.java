package com.waters.hybris.stepdefs.integration.prices;

import com.waters.hybris.integration.prices.dao.PricesDao;
import com.waters.hybris.integration.prices.service.CurrencyResolver;
import com.waters.hybris.stepdefs.integration.prices.dto.PriceDto;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.europe1.model.PriceRowModel;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.Optional;

public class PriceStepDefs
{
	private static final Logger LOG = Logger.getLogger(PriceStepDefs.class);

	private CurrencyResolver currencyResolver;
	private EnumerationService enumerationService;
	private PricesDao pricesDao;

	@Given("^Price feed record setup$")
	public void givenPriceFeedRecordSetup()
	{
		LOG.info("Price feed record setup");
	}

	@Then("^the following prices exists in the system$")
	public void the_following_prices_exists_in_the_system(final List<PriceDto> prices)
	{
		prices.forEach(p -> {
			final Optional<CurrencyModel> currency = getCurrencyResolver().getCurrencyForWatersCurrencyCode(p.getCurrency());
			Assert.assertTrue("Couldn't resolve currency for code [" + p.getCurrency() + "]", currency.isPresent());
			final UserPriceGroup upg = getEnumerationService().getEnumerationValue(UserPriceGroup.class, p.getPriceList());
			Assert.assertNotNull("Couldn't resolve price list for [" + p.getPriceList() + "]", upg);
			final List<PriceRowModel> priceRows = getPriceRows(p.getProductCode(), currency.get(), upg);
			Assert.assertEquals("Found more than one price row", 1, priceRows.size());
			Assert.assertEquals("Price value mismatch", p.getPrice(), priceRows.get(0).getPrice().doubleValue(), 0);
		});
	}

	protected List<PriceRowModel> getPriceRows(final String productCode, final CurrencyModel currency, final UserPriceGroup upg)
	{
		return getPricesDao().getPriceRows(productCode, currency, upg);
	}

	protected CurrencyResolver getCurrencyResolver()
	{
		return currencyResolver;
	}

	@Required
	public void setCurrencyResolver(final CurrencyResolver currencyResolver)
	{
		this.currencyResolver = currencyResolver;
	}

	protected EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

	protected PricesDao getPricesDao()
	{
		return pricesDao;
	}

	@Required
	public void setPricesDao(final PricesDao pricesDao)
	{
		this.pricesDao = pricesDao;
	}
}
