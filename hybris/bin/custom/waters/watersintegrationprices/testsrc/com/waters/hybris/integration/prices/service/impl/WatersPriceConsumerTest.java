package com.waters.hybris.integration.prices.service.impl;

import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.util.BatchCommitter;
import com.waters.hybris.integration.prices.context.WatersPriceMediaImportContext;
import com.waters.hybris.integration.prices.dao.PricesDao;
import com.waters.hybris.integration.prices.data.WatersPriceData;
import com.waters.hybris.integration.prices.service.CurrencyResolver;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WatersPriceConsumerTest {

    public static final String PRODUCT_CODE = "product-code";
    public static final String CURRENCY = "currency";
    public static final String PRICE = "10.00";
    public static final String SALES_ORG = "sales-org";

    private static final UserPriceGroup userPriceGroup1 = UserPriceGroup.valueOf(SALES_ORG);

    @Mock
    private AuditContext auditContext;

    @Mock
    private WatersPriceMediaImportContext context;

    @Mock
    private WatersPriceData data;

    @Mock
    private ProductModel productModel;

    @Mock
    private CatalogVersionModel catalogVersionModel;

    @Mock
    private CurrencyModel currency;

    @Mock
    private PriceRowModel priceRowModel;

    @Mock
    private UnitModel unitModel;

    @Mock
    private ProductService productService;

    @Mock
    private EnumerationService enumerationService;

    @Mock
    private CatalogVersionService catalogVersionService;

    @Mock
    private BatchCommitter batchCommitter;

    @Mock
    private ModelService modelService;

    @Mock
    private UnitService unitService;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Configuration configuration;

    @Mock
    private PricesDao pricesDao;

	@Mock
	private CurrencyResolver currencyResolver;

    @InjectMocks
    @Spy
    private WatersPriceConsumer testObj = new WatersPriceConsumer();

    @Before
    public void setup() {
        doReturn(CURRENCY).when(data).getCurrency();
        doReturn(PRICE).when(data).getPrice();
        doReturn(PRODUCT_CODE).when(data).getPartNumber();
        doReturn(SALES_ORG).when(data).getSalesOrganisation();

        doReturn(auditContext).when(context).getAuditContext();
        doReturn(batchCommitter).when(context).getBatchCommitter();

        doReturn(catalogVersionModel).when(catalogVersionService).getCatalogVersion(anyString(), anyString());
        doReturn(productModel).when(productService).getProductForCode(catalogVersionModel, PRODUCT_CODE);
        doReturn(Optional.of(currency)).when(currencyResolver).getCurrencyForWatersCurrencyCode(CURRENCY);
        doReturn(userPriceGroup1).when(enumerationService).getEnumerationValue(UserPriceGroup.class, SALES_ORG);

        doReturn(unitModel).when(unitService).getUnitForCode(anyString());
        doReturn(configuration).when(configurationService).getConfiguration();
        doReturn(false).when(configuration).getBoolean(anyString());
        doReturn(singletonList(priceRowModel)).when(pricesDao).getPriceRows(PRODUCT_CODE, currency, userPriceGroup1);
    }

    @Test
    public void giveANullRecord_thenReturnFalse() {
        final boolean valid = testObj.isValid(null, context);
        assertThat(valid).isFalse();
    }

    @Test
    public void giveAnEmptyRecord_thenReturnFalse() {
        final boolean valid = testObj.isValid(new WatersPriceData(null, null, null, null, null, null), context);
        assertThat(valid).isFalse();
    }

    @Test
    public void givenARecord_withNoProductCode_thenReturnFalse() {
        doReturn(null).when(data).getPartNumber();
        final boolean valid = testObj.isValid(data, context);
        assertThat(valid).isFalse();
    }

    @Test
    public void givenARecord_withNoSalesOrganisation_thenReturnFalse() {
        doReturn(null).when(data).getSalesOrganisation();
        final boolean valid = testObj.isValid(data, context);
        assertThat(valid).isFalse();
    }

    @Test
    public void givenARecord_withNoPrice_thenReturnFalse() {
        doReturn(null).when(data).getPrice();
        final boolean valid = testObj.isValid(data, context);
        assertThat(valid).isFalse();
    }

    @Test
    public void givenARecord_withNoCurrency_thenReturnFalse() {
        doReturn(null).when(data).getCurrency();
        final boolean valid = testObj.isValid(data, context);
        assertThat(valid).isFalse();
    }


    @Test
    public void givenARecord_withAllData_thenReturnTrue() {
        final boolean valid = testObj.isValid(data, context);
        assertThat(valid).isTrue();
    }

    @Test
    public void givenARecord_withNoReferenceToAValidProduct_thenDoNotLogError()
    {
        doThrow(Exception.class).when(productService).getProductForCode(catalogVersionModel, PRODUCT_CODE);

        testObj.internalProcess(data, context);

		verify(testObj, times(0)).logError(anyString(), anyString());
        verifyZeroInteractions(batchCommitter);
    }

	@Test
	public void givenARecord_withNoReferenceToAValidProduct_thenCreatePriceRows()
	{
		doThrow(Exception.class).when(productService).getProductForCode(catalogVersionModel, PRODUCT_CODE);
		doReturn(Collections.emptyList()).when(pricesDao).getPriceRows(PRODUCT_CODE, currency, userPriceGroup1);
		doReturn(priceRowModel).when(modelService).create(PriceRowModel.class);

		testObj.internalProcess(data, context);

		verify(context, times(1)).incrementSuccess();
		verify(testObj, times(1)).createPriceRow(PRODUCT_CODE, currency, userPriceGroup1);
		verify(context).addToBatchAndRegister(anyString(), any(PriceRowModel.class));
	}

	@Test
	public void givenARecord_withNoReferenceToAValidProduct_thenReuseCreatedPriceRows()
	{
		doThrow(Exception.class).when(productService).getProductForCode(catalogVersionModel, PRODUCT_CODE);
		doReturn(singletonList(priceRowModel)).when(pricesDao).getPriceRows(PRODUCT_CODE, currency, userPriceGroup1);

		testObj.internalProcess(data, context);

		verify(context, times(1)).incrementSuccess();
		verify(testObj, times(0)).createPriceRow(PRODUCT_CODE, currency, userPriceGroup1);
		verify(context).addToBatchAndRegister(anyString(), any(PriceRowModel.class));
	}


    @Test
    public void givenARecord_withNoReferenceToAValidCurrency_thenLogWarn()
    {
        doReturn(empty()).when(currencyResolver).getCurrencyForWatersCurrencyCode(CURRENCY);

        testObj.internalProcess(data, context);

		verify(testObj).logWarn(anyString(), anyString());
        verifyZeroInteractions(batchCommitter);
    }

    @Test
    public void givenARecord_withAnInvalidPrice_thenLogError()
    {
        doReturn("not a price").when(data).getPrice();

        testObj.internalProcess(data, context);

		verify(testObj).logError(anyString(), anyString());
        verifyZeroInteractions(batchCommitter);
    }

    @Test
    public void givenARecord_withValidData_thenSaveRecord()
    {
        doReturn(priceRowModel).when(modelService).create(PriceRowModel.class);

        testObj.internalProcess(data, context);
        verify(context, times(1)).incrementSuccess();
        verify(context).addToBatchAndRegister(anyString(), any(PriceRowModel.class));
    }

    @Test
    public void givenARecord_withValidData_andThereAreNoPriceRows_andPriceRowHasNotBeenProcessed_thenCreateNewRecord_andSaveRecord()
    {
        doReturn(priceRowModel).when(modelService).create(PriceRowModel.class);
        doReturn(emptyList()).when(pricesDao).getPriceRows(PRODUCT_CODE, currency, userPriceGroup1);

        testObj.internalProcess(data, context);
        verify(context, times(1)).incrementSuccess();
        verify(testObj, times(1)).createPriceRow(PRODUCT_CODE, currency, userPriceGroup1);
        verify(context).addToBatchAndRegister(anyString(), any(PriceRowModel.class));
    }

    @Test
    public void givenARecord_withValidData_andThereAreNoPriceRows_andPriceRowHasBeenProcessed_thenCreateNewRecord_andSaveRecord()
    {
        doReturn(priceRowModel).when(context).getProcessed(anyString());


        testObj.internalProcess(data, context);
        verify(context, times(1)).incrementSuccess();
        verify(testObj, times(0)).createPriceRow(PRODUCT_CODE, currency, userPriceGroup1);
        verify(context, times(1)).getProcessed(anyString());
        verify(context).addToBatchAndRegister(anyString(), any(PriceRowModel.class));
    }
}
