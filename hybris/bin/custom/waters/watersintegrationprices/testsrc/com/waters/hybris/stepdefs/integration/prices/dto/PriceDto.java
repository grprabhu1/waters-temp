package com.waters.hybris.stepdefs.integration.prices.dto;

public class PriceDto
{
	private String productCode;
	private String priceList;
	private double price;
	private String currency;

	public String getProductCode()
	{
		return productCode;
	}

	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	public String getPriceList()
	{
		return priceList;
	}

	public void setPriceList(final String priceList)
	{
		this.priceList = priceList;
	}

	public double getPrice()
	{
		return price;
	}

	public void setPrice(final double price)
	{
		this.price = price;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}
}
