@PriceFeed @Transaction
Feature: Price Feed

	@AdminUser
	Scenario: Price  Feed
		Given Price feed record setup
		Given That the Impex "createTestPriceFileImportRecords.impex" to "Setup price feed record" has been executed
		When the "watersPriceFeedImportJob" has been run
		And the job completed successfully
		Then the following prices exists in the system
			|productCode 	   |priceList|price|currency|
			|PROD00001         |AU01|          75.00 |EUR  |
			|PROD00002         |AU01|         400.00 |EUR  |
			|PROD00002         |BE01|          80.00 |EUR  |
			|PROD00003         |BE01|         400.00 |EUR  |
			|PROD00003         |CA01|         380.00 |CAD  |
			|PROD00004         |CA01|         105.00 |CAD  |
			|PROD00004         |DA01|       2,280.00 |DKK  |
			|PROD00005         |DA01|         600.00 |DKK  |
			|PROD00005         |FI01|          80.00 |EUR  |
			|PROD00006         |FI01|         400.00 |EUR  |
			|PROD00006         |FR01|          80.00 |EUR  |
			|PROD00007         |FR01|         400.00 |EUR  |
			|PROD00007         |GE01|          80.00 |EUR  |
			|PROD00008         |GE01|         400.00 |EUR  |
			|PROD00008         |IN01|         22,275 |INR  |
			|PROD00009         |IN01|          3,866 |INR  |
			|PROD00010         |IT01|          80.00 |EUR  |
			|PROD00011         |IT01|         400.00 |EUR  |
			|PROD00011         |JP01|         40,000 |JPY  |
			|PROD00012         |JP01|         12,000 |JPY  |
			|PROD00012         |MX01|         387.00 |USD  |
			|PROD00013         |MX01|         106.00 |USD  |
			|PROD00013         |NL01|          80.00 |EUR  |
			|PROD00014         |NL01|         400.00 |EUR  |
			|PROD00014         |NO01|       2,432.00 |NOK  |
			|PROD00015         |NO01|         640.00 |NOK  |
			|PROD00015         |PR01|         309.00 |USD  |
			|PROD00016         |PR01|          97.23 |USD  |
			|PROD00016         |SP01|          80.00 |EUR  |
			|PROD00017         |SP01|         400.00 |EUR  |
			|PROD00017         |SW01|       2,736.00 |SEK  |
			|PROD00018         |SW01|         699.00 |SEK  |
			|PROD00018         |SZ01|         456.00 |CHF  |
			|PROD00019         |SZ01|          80.00 |CHF  |
			|PROD00020         |UK01|         183.00 |GBP  |
			|PROD00021         |UK01|          30.00 |GBP  |
			|PROD00021         |US01|         309.00 |USD  |
			|PROD00022         |US01|          80.00 |USD  |
			|PROD00022         |AS01|         423.00 |AUD  |
			|PROD00023         |AS01|         128.00 |AUD  |
			|PROD00023         |SG02|         311.00 |USD  |
			|PROD00024         |SG02|          88.00 |USD  |
			|PROD00024         |WN01|       9,300.00 |NTD  |
			|PROD00025         |WN01|       2,816.00 |NTD  |
			|PROD00025         |EI06|          80.00 |EUR  |
			|PROD00026         |EI06|         400.00 |EUR  |
			|PROD00026         |HU01|      82,080.00 |HUF  |
			|PROD00027         |HU01|      20,250.00 |HUF  |
			|PROD00027         |CX01|       8,512.00 |CZK  |
			|PROD00028         |CX01|       2,100.00 |CZK  |
			|PROD00028         |PL01|       1,140.00 |PLN  |
			|PROD00029         |PL01|         281.00 |PLN  |
			|PROD00029         |CN01|       2,883.00 |CNY  |
			|PROD00030         |CN01|       8,800.00 |CNY  |
			|PROD00030         |KS01|        114,400 |WON  |
			|PROD00031         |KS01|        218,700 |WON  |
			|PROD00031         |IS01|       3,741.82 |USD  |
			|PROD00032         |IS01|       1,000.00 |USD  |
			|PROD00032         |BR01|         240.23 |BRL  |
			|PROD00033         |BR01|         657.92 |BRL  |
			|PROD00033         |PO01|          80.00 |EUR  |
			|PROD00034         |PO01|         400.00 |EUR  |
			|PROD00034         |MY02|       1,048.00 |MYR  |
			|PROD00035         |MY02|         264.00 |MYR  |
