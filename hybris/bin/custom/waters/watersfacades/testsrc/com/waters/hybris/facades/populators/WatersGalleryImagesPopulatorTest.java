package com.waters.hybris.facades.populators;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.model.model.ExternalGalleryModel;
import com.waters.hybris.core.model.model.ExternalMediaModel;
import com.waters.hybris.core.strategies.ProductImageResolutionStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WatersGalleryImagesPopulatorTest {

	private static final String IMAGE_URL_TEST = "imageUrlTest";

	@Mock
	private ProductImageResolutionStrategy productImageResolutionStrategy;

	@Mock
	private WatersProductModel watersProductModel;

	@Mock
	private ExternalGalleryModel externalGalleryModel;

	@InjectMocks
	@Spy
	private WatersGalleryImagesPopulator testObj;

	@Test
	public void testGalleryImagesWithNoGallery() {
		when(watersProductModel.getImageGallery()).thenReturn(null);
		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);
		assertTrue(CollectionUtils.isEmpty(productData.getImages()));
	}

	@Test
	public void testGalleryImagesWithNoExternalMedias() {
		when(watersProductModel.getImageGallery()).thenReturn(externalGalleryModel);
		when(externalGalleryModel.getExternalMedias()).thenReturn(null);
		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);
		assertTrue(CollectionUtils.isEmpty(productData.getImages()));
	}

	@Test
	public void testGalleryImages() {
		final ImageData imageData = new ImageData();
		imageData.setImageType(ImageDataType.GALLERY);
		imageData.setUrl(IMAGE_URL_TEST);

		when(watersProductModel.getImageGallery()).thenReturn(externalGalleryModel);
		final Set<ExternalMediaModel> externalMediaModels = new HashSet<>();
		externalMediaModels.add(new ExternalMediaModel());
		when(externalGalleryModel.getExternalMedias()).thenReturn(externalMediaModels);
		when(productImageResolutionStrategy.resolveImageUrl(Mockito.anyObject(), Mockito.anyObject())).thenReturn(IMAGE_URL_TEST);
		when(productImageResolutionStrategy.populateImageData(Mockito.anyObject(), Mockito.anyObject())).thenReturn(imageData);

		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);
		assertEquals(productData.getImages().size(), 1);
		final List<ImageData> images = new ArrayList<>(productData.getImages());
		assertEquals(images.get(0).getImageType(), ImageDataType.GALLERY);
		assertEquals(images.get(0).getUrl(), IMAGE_URL_TEST);
		assertNull(images.get(0).getFormat());
	}

	@Test
	public void testGalleryImagesWithNullExternalMedias() {
		final ImageData imageData = new ImageData();
		imageData.setImageType(ImageDataType.GALLERY);
		imageData.setUrl(IMAGE_URL_TEST);

		when(watersProductModel.getImageGallery()).thenReturn(externalGalleryModel);
		final Set<ExternalMediaModel> externalMediaModels = new HashSet<>();
		externalMediaModels.add(new ExternalMediaModel());
		when(externalGalleryModel.getExternalMedias()).thenReturn(null);

		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);
		assertTrue(productData.getImages().isEmpty());
	}
}
