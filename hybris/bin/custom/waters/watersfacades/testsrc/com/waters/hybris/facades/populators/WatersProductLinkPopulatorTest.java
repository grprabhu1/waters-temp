package com.waters.hybris.facades.populators;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.model.model.ProductLinkModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WatersProductLinkPopulatorTest
{

	private static final String CODE = "code";
	private static final String NAME = "name";
	private static final String URL = "url";
	private static final Integer DISPLAY_ORDER = 0;
	private Locale localeEN = Locale.ENGLISH;
	private Locale localeDE = Locale.GERMAN;
	@Mock
	private ProductLinkModel productLinkModel1, productLinkModel2;

	@Mock
	private WatersProductModel watersProductModel;

	@Mock
	private I18NService i18NService;

	@InjectMocks
	private final WatersProductLinkPopulator testObj = new WatersProductLinkPopulator();

	@Test
	public void testNoProductLinks() {
		when(watersProductModel.getProductLinks()).thenReturn(null);
		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);

		assertTrue(CollectionUtils.isEmpty(productData.getProductLinks()));
	}

	@Test
	public void testMultipleProductLinks() {
		when(i18NService.getCurrentLocale()).thenReturn(localeEN);
		when(productLinkModel1.getCode()).thenReturn(CODE);
		when(productLinkModel1.getUrl()).thenReturn(URL);
		when(productLinkModel1.getName(localeEN)).thenReturn(NAME);
		when(productLinkModel1.getDisplayOrder()).thenReturn(DISPLAY_ORDER);
		when(productLinkModel2.getCode()).thenReturn(CODE);
		when(productLinkModel2.getUrl()).thenReturn(URL);
		when(productLinkModel2.getName(localeEN)).thenReturn(NAME);
		when(productLinkModel2.getDisplayOrder()).thenReturn(DISPLAY_ORDER);
		Set<ProductLinkModel> productLinkModels = new HashSet<>(Arrays.asList(productLinkModel1, productLinkModel2));
		when(watersProductModel.getProductLinks()).thenReturn(productLinkModels);

		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);

		assertEquals(productData.getProductLinks().size(), 2);
		assertEquals(productData.getProductLinks().get(0).getCode(), CODE);
		assertEquals(productData.getProductLinks().get(0).getUrl(), URL);
		assertEquals(productData.getProductLinks().get(0).getName(), NAME);
		assertEquals(productData.getProductLinks().get(0).getDisplayOrder(), DISPLAY_ORDER);
		assertEquals(productData.getProductLinks().get(1).getCode(), CODE);
		assertEquals(productData.getProductLinks().get(1).getUrl(), URL);
		assertEquals(productData.getProductLinks().get(1).getName(), NAME);
		assertEquals(productData.getProductLinks().get(1).getDisplayOrder(), DISPLAY_ORDER);
	}


	@Test
	public void testProductLinksWithGermanLocale() {
		when(i18NService.getCurrentLocale()).thenReturn(localeDE);
		when(productLinkModel1.getCode()).thenReturn(CODE);
		when(productLinkModel1.getUrl()).thenReturn(URL);
		when(productLinkModel1.getName(localeDE)).thenReturn(NAME);
		when(productLinkModel1.getDisplayOrder()).thenReturn(DISPLAY_ORDER);
		Set<ProductLinkModel> productLinkModels = new HashSet<>(Arrays.asList(productLinkModel1));
		when(watersProductModel.getProductLinks()).thenReturn(productLinkModels);

		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);

		assertEquals(productData.getProductLinks().size(), 1);
		assertEquals(productData.getProductLinks().get(0).getCode(), CODE);
		assertEquals(productData.getProductLinks().get(0).getUrl(), URL);
		assertEquals(productData.getProductLinks().get(0).getName(), NAME);
		assertEquals(productData.getProductLinks().get(0).getDisplayOrder(), DISPLAY_ORDER);
	}

}
