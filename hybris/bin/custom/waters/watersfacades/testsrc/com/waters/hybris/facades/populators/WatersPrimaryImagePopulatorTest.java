package com.waters.hybris.facades.populators;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.model.model.ExternalMediaModel;
import com.waters.hybris.core.strategies.ProductImageResolutionStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WatersPrimaryImagePopulatorTest {

	private static final String IMAGE_URL_TEST = "imageUrlTest";

	@Mock
	private ProductImageResolutionStrategy productImageResolutionStrategy;

	@Mock
	private ExternalMediaModel externalMediaModel;

	@Mock
	private WatersProductModel watersProductModel;

	@InjectMocks
	@Spy
	private WatersPrimaryImagePopulator testObj;

	@Test
	public void testPrimaryImageWithNoData() {
		when(watersProductModel.getPrimaryImage()).thenReturn(null);
		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);
		assertTrue(CollectionUtils.isEmpty(productData.getImages()));
	}

	@Test
	public void testPrimaryImage() {
		final ImageData imageData = new ImageData();
		imageData.setImageType(ImageDataType.PRIMARY);
		imageData.setUrl(IMAGE_URL_TEST);

		when(watersProductModel.getPrimaryImage()).thenReturn(externalMediaModel);
		when(productImageResolutionStrategy.resolveImageUrl(Mockito.anyObject(), Mockito.anyObject())).thenReturn(IMAGE_URL_TEST);
		when(productImageResolutionStrategy.populateImageData(Mockito.anyObject(), Mockito.anyObject())).thenReturn(imageData);

		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);
		assertEquals(productData.getImages().size(), 1);
		final List<ImageData> images = new ArrayList<>(productData.getImages());
		assertEquals(images.get(0).getImageType(), ImageDataType.PRIMARY);
		assertEquals(images.get(0).getUrl(), IMAGE_URL_TEST);
		assertNull(images.get(0).getFormat());

	}

	@Test
	public void testPrimaryImageWithNullExternalMediaModel() {
		final ImageData imageData = new ImageData();
		imageData.setImageType(ImageDataType.PRIMARY);
		imageData.setUrl(IMAGE_URL_TEST);

		when(watersProductModel.getPrimaryImage()).thenReturn(null);

		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);
		assertNull(productData.getImages());

	}
}
