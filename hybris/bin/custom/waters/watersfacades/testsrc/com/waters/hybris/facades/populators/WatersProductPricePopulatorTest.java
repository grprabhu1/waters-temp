package com.waters.hybris.facades.populators;

import com.google.common.collect.HashBiMap;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.strategies.impl.DefaultConfigurationPropertyStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WatersProductPricePopulatorTest {

	private static final String GBP = "GBP";
	private static final double PRICE = 25.50d;
	private static final String AC_01 = "AC01";

	@Mock
	private WatersProductModel watersProductModel;

	@Mock
	private PriceRowModel priceRowModel;

	@Mock
	private CurrencyModel currencyModel;

	@Mock
	private HybrisEnumValue hybrisEnumValue;

	@Mock
	private BaseStoreService baseStoreService;

	@Mock
	private BaseStoreModel baseStoreModel;

	@Mock
	private CountryModel countryModel1;

	@Mock
	private CountryModel countryModel2;

	@Mock
	private DefaultConfigurationPropertyStrategy defaultConfigurationPropertyStrategy;

	@InjectMocks
	private final WatersProductPricePopulator testObj = new WatersProductPricePopulator();

	@Test
	public void testPriceWithNoData() {
		when(watersProductModel.getEurope1Prices()).thenReturn(null);
		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);

		assertTrue(CollectionUtils.isEmpty(productData.getPrices()));
	}

	@Test
	public void testPriceDataIsPopulated() {
		when(currencyModel.getIsocode()).thenReturn(WatersProductPricePopulatorTest.GBP);
		when(priceRowModel.getPrice()).thenReturn(WatersProductPricePopulatorTest.PRICE);
		when(priceRowModel.getCurrency()).thenReturn(currencyModel);
		when(hybrisEnumValue.getCode()).thenReturn(WatersProductPricePopulatorTest.AC_01);
		when(priceRowModel.getUg()).thenReturn(hybrisEnumValue);

		when(baseStoreService.getBaseStoreForUid(anyString())).thenReturn(baseStoreModel);
		when(baseStoreModel.getDeliveryCountries()).thenReturn(Arrays.asList(countryModel1, countryModel2));
		when(countryModel1.getIsocode()).thenReturn("FR");
		when(countryModel2.getIsocode()).thenReturn("DE");
		final List<PriceRowModel> priceRowModelList = new ArrayList<>();
		priceRowModelList.add(priceRowModel);
		when(watersProductModel.getEurope1Prices()).thenReturn(priceRowModelList);
		when(defaultConfigurationPropertyStrategy.getStringMap(anyString())).thenReturn(HashBiMap.create(0));

		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);

		assertEquals(productData.getPrices().size(), 1);
		assertEquals(productData.getPrices().get(0).getCurrencyIso(), WatersProductPricePopulatorTest.GBP);
		assertEquals(productData.getPrices().get(0).getValue(), BigDecimal.valueOf(WatersProductPricePopulatorTest.PRICE));
		assertEquals(productData.getPrices().get(0).getCountries(), "FR,DE");
	}


	@Test
	public void testPriceCurrencyIsMappedToSapFormat() {
		when(currencyModel.getIsocode()).thenReturn("TWD");
		when(priceRowModel.getCurrency()).thenReturn(currencyModel);

		when(baseStoreService.getBaseStoreForUid(anyString())).thenReturn(baseStoreModel);
		when(watersProductModel.getEurope1Prices()).thenReturn(Arrays.asList(priceRowModel));
		HashBiMap<String, String> currencyMap = HashBiMap.create(2);
		currencyMap.put("NTD", "TWD");
		when(defaultConfigurationPropertyStrategy.getStringMap(anyString())).thenReturn(currencyMap);

		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);

		assertEquals(productData.getPrices().size(), 1);
		assertEquals(productData.getPrices().get(0).getCurrencyIso(), "NTD");
	}

	@Test
	public void testPriceDataWithoutCountries() {
		when(currencyModel.getIsocode()).thenReturn(WatersProductPricePopulatorTest.GBP);
		when(priceRowModel.getPrice()).thenReturn(WatersProductPricePopulatorTest.PRICE);
		when(priceRowModel.getCurrency()).thenReturn(currencyModel);

		when(baseStoreService.getBaseStoreForUid(anyString())).thenReturn(baseStoreModel);
		when(baseStoreModel.getDeliveryCountries()).thenReturn(null);
		final List<PriceRowModel> priceRowModelList = new ArrayList<>();
		priceRowModelList.add(priceRowModel);
		when(watersProductModel.getEurope1Prices()).thenReturn(priceRowModelList);
		when(defaultConfigurationPropertyStrategy.getStringMap(anyString())).thenReturn(HashBiMap.create(0));

		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);

		assertEquals(productData.getPrices().size(), 1);
		assertEquals(productData.getPrices().get(0).getCountries(), "");
	}


	@Test
	public void testPriceDataWithUGNotFoundInBaseStores() {
		when(currencyModel.getIsocode()).thenReturn(WatersProductPricePopulatorTest.GBP);
		when(priceRowModel.getPrice()).thenReturn(WatersProductPricePopulatorTest.PRICE);
		when(priceRowModel.getCurrency()).thenReturn(currencyModel);
		when(priceRowModel.getUg()).thenReturn(hybrisEnumValue);

		when(baseStoreService.getBaseStoreForUid(anyString())).thenReturn(null);
		final List<PriceRowModel> priceRowModelList = new ArrayList<>();
		priceRowModelList.add(priceRowModel);
		when(watersProductModel.getEurope1Prices()).thenReturn(priceRowModelList);
		when(defaultConfigurationPropertyStrategy.getStringMap(anyString())).thenReturn(HashBiMap.create(0));

		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);

		assertEquals(productData.getPrices().size(), 1);
		assertEquals(productData.getPrices().get(0).getCountries(), "");
	}
}
