package com.waters.hybris.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WatersFeaturePopulatorTest
{

	private static final String ATTRIBUTE_NAME = "Attribute name";
	private static final String ATTRIBUTE_NAME_DE = "Attribute name de";
	private static final Integer POSITION = 123;
	private static final String FEATURE_CODE = "featCode";
	private static final String CLASS_ATTR_DESCRIPTION = "classAttrDesc";
	private static final String CLASS_UNIT_NAME = "unitName";
	private static final String GERMAN_UNIT = "German unit";
	private static final String ENGLISH_UNIT = "English unit";

	@Mock
	private Feature source;

	private FeatureData target;

	@Mock
	private FeatureValue featureValue;

	@Mock
	private ClassAttributeAssignmentModel classAttributeAssignmentModel;

	@Mock
	private ClassificationAttributeUnitModel classificationAttributeUnitModel;

	@Mock
	private Feature feature;

	@Mock
	private ClassificationAttributeModel classificationAttributeModel;

	@Mock
	private ClassificationAttributeValueModel classificationAttributeValueModel;

	@InjectMocks
	@Spy
	private WatersFeaturePopulator testObj;


	@Before
	public void setUp()
	{
		target = new FeatureData();

		given(source.getCode()).willReturn(FEATURE_CODE);
		given(source.getClassAttributeAssignment()).willReturn(classAttributeAssignmentModel);
		given(classAttributeAssignmentModel.getComparable()).willReturn(Boolean.TRUE);
		given(classAttributeAssignmentModel.getDescription()).willReturn(CLASS_ATTR_DESCRIPTION);
		given(classAttributeAssignmentModel.getUnit()).willReturn(classificationAttributeUnitModel);
		given(classificationAttributeUnitModel.getName(Locale.ENGLISH)).willReturn(CLASS_UNIT_NAME);
	}

	@Test
	public void testConvert()
	{
		given(source.getValues()).willReturn(Collections.singletonList(featureValue));
		doReturn(Locale.ENGLISH).when(testObj).getCurrentLocale();

		testObj.populate(source, target);

		Assert.assertEquals(FEATURE_CODE, target.getCode());
		Assert.assertEquals(CLASS_ATTR_DESCRIPTION, target.getDescription());
		Assert.assertEquals(CLASS_UNIT_NAME, target.getFeatureUnit().getName());
		Assert.assertEquals(1, target.getFeatureValues().size());
	}

	@Test
	public void testConvertWithoutPrecision()
	{
		doReturn(Locale.ENGLISH).when(testObj).getCurrentLocale();
		given(featureValue.getValue()).willReturn(new Double("1.000000"));
		given(source.getValues()).willReturn(Collections.singletonList(featureValue));

		testObj.populate(source, target);

		Assert.assertEquals(FEATURE_CODE, target.getCode());
		Assert.assertEquals(CLASS_ATTR_DESCRIPTION, target.getDescription());
		Assert.assertEquals(CLASS_UNIT_NAME, target.getFeatureUnit().getName());
		Assert.assertEquals(1, target.getFeatureValues().size());
		Assert.assertEquals("1", target.getFeatureValues().iterator().next().getValue());
	}

	@Test
	public void testConvertWithPrecision()
	{
		doReturn(Locale.ENGLISH).when(testObj).getCurrentLocale();
		given(featureValue.getValue()).willReturn(new Double("1.523"));
		given(source.getValues()).willReturn(Collections.singletonList(featureValue));

		testObj.populate(source, target);

		Assert.assertEquals(FEATURE_CODE, target.getCode());
		Assert.assertEquals(CLASS_ATTR_DESCRIPTION, target.getDescription());
		Assert.assertEquals(CLASS_UNIT_NAME, target.getFeatureUnit().getName());
		Assert.assertEquals(1, target.getFeatureValues().size());
		Assert.assertEquals("1.523", target.getFeatureValues().iterator().next().getValue());
	}

	@Test
	public void testPublicLabelWithEnLocale()
	{
		mockClassAssignment();
		when(classificationAttributeModel.getPublicWebLabel(Locale.ENGLISH)).thenReturn(ATTRIBUTE_NAME);

		final FeatureData featureData = new FeatureData();
		testObj.populate(feature, featureData);
		assertEquals(featureData.getPublicWebLabel(), ATTRIBUTE_NAME);
	}

	@Test
	public void testPublicLabelWithDeLocale()
	{
		doReturn(Locale.GERMAN).when(testObj).getCurrentLocale();
		when(feature.getClassAttributeAssignment()).thenReturn(classAttributeAssignmentModel);
		when(classAttributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
		when(classificationAttributeModel.getPublicWebLabel(Locale.GERMAN)).thenReturn(ATTRIBUTE_NAME_DE);

		final FeatureData featureData = new FeatureData();
		testObj.populate(feature, featureData);
		assertEquals(featureData.getPublicWebLabel(), ATTRIBUTE_NAME_DE);
	}

	@Test
	public void testPublicLabelWithFallback()
	{
		doReturn(Locale.GERMAN).when(testObj).getCurrentLocale();
		when(feature.getClassAttributeAssignment()).thenReturn(classAttributeAssignmentModel);
		when(classAttributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
		when(classificationAttributeModel.getPublicWebLabel(Locale.ENGLISH)).thenReturn(ATTRIBUTE_NAME);
		when(classificationAttributeModel.getPublicWebLabel(Locale.GERMAN)).thenReturn(null);

		final FeatureData featureData = new FeatureData();
		testObj.populate(feature, featureData);
		assertEquals(featureData.getPublicWebLabel(), ATTRIBUTE_NAME);
	}

	@Test
	public void testFeatureValuesWithDe()
	{
		doReturn(Locale.GERMAN).when(testObj).getCurrentLocale();
		when(feature.getClassAttributeAssignment()).thenReturn(classAttributeAssignmentModel);
		when(classAttributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
		when(classAttributeAssignmentModel.getUnit()).thenReturn(classificationAttributeUnitModel);
		when(classificationAttributeUnitModel.getName(Locale.GERMAN)).thenReturn(GERMAN_UNIT);
		when(classificationAttributeUnitModel.getName(Locale.ENGLISH)).thenReturn(ENGLISH_UNIT);

		final FeatureData featureData = new FeatureData();
		testObj.populate(feature, featureData);
		assertEquals(featureData.getFeatureUnit().getName(), GERMAN_UNIT);
	}

	@Test
	public void testFeatureValuesWithFallbackToEn()
	{
		doReturn(Locale.GERMAN).when(testObj).getCurrentLocale();
		when(feature.getClassAttributeAssignment()).thenReturn(classAttributeAssignmentModel);
		when(classAttributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
		when(classAttributeAssignmentModel.getUnit()).thenReturn(classificationAttributeUnitModel);
		when(classificationAttributeUnitModel.getName(Locale.GERMAN)).thenReturn(null);
		when(classificationAttributeUnitModel.getName(Locale.ENGLISH)).thenReturn(ENGLISH_UNIT);

		final FeatureData featureData = new FeatureData();
		testObj.populate(feature, featureData);
		assertEquals(featureData.getFeatureUnit().getName(), ENGLISH_UNIT);
	}

	@Test
	public void testPosition()
	{
		mockClassAssignment();
		when(classAttributeAssignmentModel.getPosition()).thenReturn(POSITION);

		final FeatureData featureData = new FeatureData();
		testObj.populate(feature, featureData);

		assertEquals(featureData.getPosition(), POSITION);
	}

	@Test
	public void testPositionNull()
	{
		mockClassAssignment();
		when(classAttributeAssignmentModel.getPosition()).thenReturn(null);

		final FeatureData featureData = new FeatureData();
		testObj.populate(feature, featureData);

		assertEquals(featureData.getPosition(), null);
	}

	@Test
	public void testInternalOnly() {
		mockClassAssignment();
		when(classAttributeAssignmentModel.isInternalOnly()).thenReturn(true);

		final FeatureData featureData = new FeatureData();
		testObj.populate(feature, featureData);

		assertEquals(featureData.getInternalOnly(), true);
	}

	private void mockClassAssignment()
	{
		doReturn(Locale.ENGLISH).when(testObj).getCurrentLocale();
		when(feature.getClassAttributeAssignment()).thenReturn(classAttributeAssignmentModel);
		when(classAttributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
	}

	private void mockClassificationAttributeValue()
	{
		given(feature.getValues()).willReturn(Collections.singletonList(featureValue));
		given(featureValue.getValue()).willReturn(classificationAttributeValueModel);
		given(classAttributeAssignmentModel.getAttributeValues()).willReturn(Collections.singletonList(classificationAttributeValueModel));
	}

	@Test
	public void testPopulatesPositionWhenEnum() {

		mockClassAssignment();
		mockClassificationAttributeValue();
		given(classAttributeAssignmentModel.getAttributeType()).willReturn(ClassificationAttributeTypeEnum.ENUM);
		given(classificationAttributeValueModel.getName(Locale.ENGLISH)).willReturn("value1");

		final FeatureData featureData = new FeatureData();
		testObj.populate(feature, featureData);

		assertEquals(1, featureData.getFeatureValues().size());
		assertEquals(Integer.valueOf(1), featureData.getFeatureValues().iterator().next().getPosition());
	}

	@Test
	public void testDoesNOTPopulatePositionWhenSTRING() {

		mockClassAssignment();
		mockClassificationAttributeValue();
		given(classAttributeAssignmentModel.getAttributeType()).willReturn(ClassificationAttributeTypeEnum.STRING);
		given(classificationAttributeValueModel.getName(Locale.ENGLISH)).willReturn("value1");

		final FeatureData featureData = new FeatureData();
		testObj.populate(feature, featureData);

		assertEquals(1, featureData.getFeatureValues().size());
		assertNull(featureData.getFeatureValues().iterator().next().getPosition());
	}

	@Test
	public void testDoesNOTPopulatePosition_WhenItemNotFoundInAttributes() {

		mockClassAssignment();
		mockClassificationAttributeValue();
		given(classAttributeAssignmentModel.getAttributeValues()).willReturn(Collections.emptyList());
		given(classAttributeAssignmentModel.getAttributeType()).willReturn(ClassificationAttributeTypeEnum.ENUM);
		given(classificationAttributeValueModel.getName(Locale.ENGLISH)).willReturn("value1");

		final FeatureData featureData = new FeatureData();
		testObj.populate(feature, featureData);

		assertEquals(1, featureData.getFeatureValues().size());
		assertNull(featureData.getFeatureValues().iterator().next().getPosition());
	}

	@Test
	public void testPopulatesPositionWhenEnum_AndAddsUOMPosition() {

		mockClassAssignment();
		mockClassificationAttributeValue();
		given(featureValue.getUnit()).willReturn(classificationAttributeUnitModel);
		given(classificationAttributeUnitModel.getDisplayOrder()).willReturn(Integer.valueOf(1000));
		given(classificationAttributeUnitModel.getSymbol()).willReturn("%");
		given(classAttributeAssignmentModel.getAttributeType()).willReturn(ClassificationAttributeTypeEnum.ENUM);
		given(classificationAttributeValueModel.getName(Locale.ENGLISH)).willReturn("value1");

		final FeatureData featureData = new FeatureData();
		testObj.populate(feature, featureData);

		assertEquals(1, featureData.getFeatureValues().size());
		assertEquals(Integer.valueOf(1001), featureData.getFeatureValues().iterator().next().getPosition());
		assertEquals("%", featureData.getFeatureValues().iterator().next().getFeatureUnitValue().getValue());
	}

	@Test
	public void testPopulatesPosition_AndUomSymbol_WhenDisplayOrderIsNull() {

		mockClassAssignment();
		mockClassificationAttributeValue();
		given(featureValue.getUnit()).willReturn(classificationAttributeUnitModel);
		given(classificationAttributeUnitModel.getSymbol()).willReturn("%");
		given(classAttributeAssignmentModel.getAttributeType()).willReturn(ClassificationAttributeTypeEnum.ENUM);
		given(classificationAttributeValueModel.getName(Locale.ENGLISH)).willReturn("value1");

		final FeatureData featureData = new FeatureData();
		testObj.populate(feature, featureData);

		assertEquals(1, featureData.getFeatureValues().size());
		assertEquals(Integer.valueOf(1), featureData.getFeatureValues().iterator().next().getPosition());
		assertEquals("%", featureData.getFeatureValues().iterator().next().getFeatureUnitValue().getValue());
	}



}
