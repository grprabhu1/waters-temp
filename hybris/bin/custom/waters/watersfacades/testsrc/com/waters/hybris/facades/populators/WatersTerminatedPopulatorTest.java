package com.waters.hybris.facades.populators;

import com.waters.hybris.core.model.model.WatersProductModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WatersTerminatedPopulatorTest
{
	@Mock
	private WatersProductModel watersProductModel;

	@InjectMocks
	private final WatersTerminatedPopulator testObj = new WatersTerminatedPopulator();

	@Test
	public void testTerminatedTrue() {
		when(watersProductModel.isTerminated()).thenReturn(true);

		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);

		assertEquals(true, productData.getTerminated());
	}

	@Test
	public void testTerminatedFalse() {
		when(watersProductModel.isTerminated()).thenReturn(false);

		final ProductData productData = new ProductData();
		testObj.populate(watersProductModel, productData);

		assertEquals(false, productData.getTerminated());
	}

	@Test
	public void testNotValidProductType() {

		final ProductData productData = new ProductData();
		final ProductModel productModel = new ProductModel();
		testObj.populate(productModel, productData);

		assertEquals(null, productData.getTerminated());
	}

}
