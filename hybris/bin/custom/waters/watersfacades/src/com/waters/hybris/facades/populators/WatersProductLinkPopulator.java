package com.waters.hybris.facades.populators;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.model.model.ProductLinkModel;
import com.waters.hybris.core.util.StreamUtil;
import com.waters.hybris.facades.product.data.ProductLinkData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.stream.Collectors;

public class WatersProductLinkPopulator implements Populator<ProductModel, ProductData>
{
	private I18NService i18NService;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		if (!(productModel instanceof WatersProductModel)) {
			return;
		}
		final WatersProductModel watersProductModel = (WatersProductModel) productModel;
		final List<ProductLinkData> productLinksData = StreamUtil.nullSafe(watersProductModel.getProductLinks())
			.map(this::populateProductLinkData)
			.collect(Collectors.toList());
		productData.setProductLinks(productLinksData);
	}

	private ProductLinkData populateProductLinkData(final ProductLinkModel productLinkModel)
	{
		final ProductLinkData productLinkData = new ProductLinkData();
		productLinkData.setCode(productLinkModel.getCode());
		productLinkData.setName(productLinkModel.getName(getI18NService().getCurrentLocale()));
		productLinkData.setUrl(productLinkModel.getUrl());
		productLinkData.setDisplayOrder(productLinkModel.getDisplayOrder());
		return productLinkData;
	}

	public I18NService getI18NService()
	{
		return i18NService;
	}

	@Required
	public void setI18NService(final I18NService i18NService)
	{
		this.i18NService = i18NService;
	}
}
