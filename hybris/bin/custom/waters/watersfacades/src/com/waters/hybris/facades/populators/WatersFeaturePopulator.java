package com.waters.hybris.facades.populators;

import com.waters.hybris.core.util.StreamUtil;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.FeatureUnitData;
import de.hybris.platform.commercefacades.product.data.FeatureUnitValueData;
import de.hybris.platform.commercefacades.product.data.FeatureValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

public class WatersFeaturePopulator implements Populator<Feature, FeatureData>
{
	private I18NService i18NService;

	@Override
	public void populate(final Feature source, final FeatureData target)
	{
		final Locale locale = getCurrentLocale();
		populateFeatureValues(source, target, locale);
		populateFeatureCustomValues(source, target, locale);
	}

	private void populateFeatureValues(final Feature source, final FeatureData target, final Locale locale)
	{
		final ClassAttributeAssignmentModel classAttributeAssignment = source.getClassAttributeAssignment();
		// Create the feature
		target.setCode(source.getCode());
		target.setComparable(Boolean.TRUE.equals(classAttributeAssignment.getComparable()));
		target.setDescription(classAttributeAssignment.getDescription());
		target.setName(source.getName());
		target.setRange(Boolean.TRUE.equals(classAttributeAssignment.getRange()));

		final ClassificationAttributeUnitModel unit = classAttributeAssignment.getUnit();
		if (unit != null)
		{
			final FeatureUnitData featureUnitData = new FeatureUnitData();

			if (unit.getName(locale) != null)
			{
				featureUnitData.setName(unit.getName(locale));
			}
			else
			{
				featureUnitData.setName(unit.getName(Locale.ENGLISH));
			}
			featureUnitData.setSymbol(unit.getSymbol());
			featureUnitData.setUnitType(unit.getUnitType());
			target.setFeatureUnit(featureUnitData);
		}
		target.setFeatureValues(StreamUtil.nullSafe(source.getValues()).map(featureValue -> getFeatureValueData(classAttributeAssignment, featureValue, locale)).collect(Collectors.toList()));
	}

	private FeatureValueData getFeatureValueData(final ClassAttributeAssignmentModel classAttributeAssignmentModel, final FeatureValue featureValue, final Locale locale)
	{
		final FeatureValueData featureValueData = new FeatureValueData();
		final Object value = featureValue.getValue();
		if (value instanceof ClassificationAttributeValueModel)
		{
			final ClassificationAttributeValueModel classificationAttributeValueModel = (ClassificationAttributeValueModel) value;
			if (StringUtils.isNotEmpty(classificationAttributeValueModel.getName(locale)))
			{
				featureValueData.setValue(classificationAttributeValueModel.getName(locale));
			}
			else
			{
				featureValueData.setValue(classificationAttributeValueModel.getName(Locale.ENGLISH));
			}
			if (ClassificationAttributeTypeEnum.ENUM.equals(classAttributeAssignmentModel.getAttributeType()))
			{
				int indexOf = classAttributeAssignmentModel.getAttributeValues().indexOf(classificationAttributeValueModel);
				if(indexOf != -1)
				{
					indexOf++;
					final int featureUnitPosition = Optional.ofNullable(featureValue.getUnit())
						.map(ClassificationAttributeUnitModel::getDisplayOrder)
						.orElse(Integer.valueOf(0)).intValue();

					featureValueData.setPosition(Integer.valueOf(indexOf + featureUnitPosition));
				}
			}
		}
		else if (NumberUtils.isNumber(String.valueOf(value)))
		{
			featureValueData.setValue(String.valueOf(value).replaceAll("\\.0*$", ""));
		}
		else
		{
			featureValueData.setValue(String.valueOf(value));
		}

		Optional.ofNullable(featureValue.getUnit())
			.map(ClassificationAttributeUnitModel::getSymbol)
			.map(this::createFeatureValueUnit)
			.ifPresent(featureValueData::setFeatureUnitValue);

		return featureValueData;
	}

	public FeatureUnitValueData createFeatureValueUnit(final String value)
	{
		final FeatureUnitValueData featureUnitValueData = new FeatureUnitValueData();
		featureUnitValueData.setValue(value);
		return featureUnitValueData;
	}

	public void populateFeatureCustomValues(final Feature source, final FeatureData target, final Locale locale)
	{
		if (source.getClassAttributeAssignment() == null || source.getClassAttributeAssignment().getClassificationAttribute() == null)
		{
			return;
		}
		final String publicWebLabel = source.getClassAttributeAssignment().getClassificationAttribute().getPublicWebLabel(locale);
		if (StringUtils.isNotEmpty(publicWebLabel))
		{
			target.setPublicWebLabel(publicWebLabel);
		}
		else
		{
			target.setPublicWebLabel(source.getClassAttributeAssignment().getClassificationAttribute().getPublicWebLabel(Locale.ENGLISH));
		}
		target.setPosition(source.getClassAttributeAssignment().getPosition());
		target.setInternalOnly(source.getClassAttributeAssignment().isInternalOnly());
		target.setFacet(source.getClassAttributeAssignment().isFacet());
	}

	protected Locale getCurrentLocale()
	{
		return getI18NService().getCurrentLocale();
	}

	public I18NService getI18NService()
	{
		return i18NService;
	}

	@Required
	public void setI18NService(final I18NService i18NService)
	{
		this.i18NService = i18NService;
	}
}
