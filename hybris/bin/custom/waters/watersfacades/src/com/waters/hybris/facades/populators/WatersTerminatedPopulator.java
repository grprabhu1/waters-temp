package com.waters.hybris.facades.populators;

import com.waters.hybris.core.model.model.WatersProductModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class WatersTerminatedPopulator implements Populator<ProductModel, ProductData>
{
	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		if (productModel instanceof WatersProductModel)
		{
			productData.setTerminated(((WatersProductModel)productModel).isTerminated());
		}
	}
}
