package com.waters.hybris.facades.populators;


import com.google.common.collect.BiMap;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.strategies.impl.DefaultConfigurationPropertyStrategy;
import com.waters.hybris.core.util.StreamUtil;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.C2LItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class WatersProductPricePopulator implements Populator<ProductModel, ProductData> {

	protected static final String CURRENCY_MAPPING_SAP_HYBRIS_PROPERTY = "currency.mapping.sap.hybris";

	private BaseStoreService baseStoreService;
	private DefaultConfigurationPropertyStrategy configurationPropertyStrategy;

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException {
		if (!(source instanceof WatersProductModel)) {
			return;
		}
		target.setPrices(StreamUtil.nullSafe(source.getEurope1Prices()).map(priceRowModel ->
			getPriceData(priceRowModel)).collect(Collectors.toList()));
	}

	private PriceData getPriceData(final PriceRowModel priceRowModel) {
		final BiMap<String, String> sapHybrisCurrencyMap = getConfigurationPropertyStrategy().getStringMap(CURRENCY_MAPPING_SAP_HYBRIS_PROPERTY);
		final String currencyCode = priceRowModel.getCurrency().getIsocode();
		final PriceData priceData = new PriceData();
		priceData.setCurrencyIso(sapHybrisCurrencyMap.inverse().getOrDefault(currencyCode, currencyCode));
		priceData.setValue(BigDecimal.valueOf(priceRowModel.getPrice()));
		priceData.setCountries(priceRowModel.getUg() != null ? getCountriesForSalesOrg(priceRowModel.getUg().getCode()) : Collections.emptyList());
		return priceData;
	}

	/**
	 * Return List of countries that are part of the input salesOrg
	 * @param salesOrg salesOrg for which the countries list is being requested
	 * @return List of countries that are part of the input salesOrg
	 */
	private List<String> getCountriesForSalesOrg(final String salesOrg){
		final BaseStoreModel baseStore = getBaseStoreService().getBaseStoreForUid(salesOrg);
		if(baseStore==null){
			return Collections.emptyList();
		}
		return StreamUtil.nullSafe(baseStore.getDeliveryCountries())
				.map(C2LItemModel::getIsocode)
				.collect(Collectors.toList());
	}

	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	public DefaultConfigurationPropertyStrategy getConfigurationPropertyStrategy()
	{
		return configurationPropertyStrategy;
	}

	public void setConfigurationPropertyStrategy(final DefaultConfigurationPropertyStrategy configurationPropertyStrategy)
	{
		this.configurationPropertyStrategy = configurationPropertyStrategy;
	}
}
