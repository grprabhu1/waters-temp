package com.waters.hybris.facades.populators;

import com.waters.hybris.core.model.model.WatersProductModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class WatersDynamicNamePopulator implements Populator<ProductModel, ProductData>
{
	@Override
	public void populate(final ProductModel watersProductModel, final ProductData productData) throws ConversionException
	{
		if (watersProductModel instanceof WatersProductModel)
		{
			productData.setName(((WatersProductModel) watersProductModel).getDynamicName());
		}
	}
}
