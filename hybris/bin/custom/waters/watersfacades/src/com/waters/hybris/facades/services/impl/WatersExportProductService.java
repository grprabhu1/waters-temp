package com.waters.hybris.facades.services.impl;

import com.google.common.collect.Maps;
import com.waters.hybris.core.model.model.WatersProductModel;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.commerceservices.product.ExportProductService;
import de.hybris.platform.commerceservices.search.flexiblesearch.PagedFlexibleSearchService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.springframework.beans.factory.annotation.Required;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

public class WatersExportProductService implements ExportProductService
{
	private ImpersonationService impersonationService;

	private UserService userService;

	public static final String WATERS_PRODUCT_QUERY = "SELECT {" + WatersProductModel.PK + "} FROM {" + WatersProductModel._TYPECODE + "} WHERE {"
		+ WatersProductModel.CATALOGVERSION + "} IN (?catalogVersions) AND {"+ WatersProductModel.APPROVALSTATUS +"} IN (?approvalStatuses)";

	private static final String MODIFIED_WATERS_PRODUCTS_QUERY = "SELECT DISTINCT {p:" + WatersProductModel.PK + "} "
			+"            FROM {" +WatersProductModel._TYPECODE +" as p "
		    +"			    LEFT JOIN "+ PriceRowModel._TYPECODE +" as pr "
			+" 				 ON {p:" + WatersProductModel.CODE +"} = {pr:"+ PriceRowModel.PRODUCTID +"}}"
			+" 	          WHERE {p:"+ WatersProductModel.CATALOGVERSION + "} IN (?catalogVersions) AND {p:"+ WatersProductModel.APPROVALSTATUS +"} IN (?approvalStatuses) "
			+"             AND ( {p:"+ WatersProductModel.MODIFIEDTIME + "} > ?modifiedTime"
            +"                  OR {pr:"+ PriceRowModel.MODIFIEDTIME + "} > ?modifiedTime )";

	public static final String CATALOG_VERSIONS = "catalogVersions";
	public static final String MODIFIED_TIME = "modifiedTime";
	public static final String APPROVAL_STATUSES = "approvalStatuses";

	private PagedFlexibleSearchService pagedFlexibleSearchService;


	@Override
	public SearchPageData<ProductModel> getAllProducts(final Collection<CatalogVersionModel> catalogVersions, final int start, final int count)
	{
		final Map<String, Object> parameters = Maps.newHashMap();
		parameters.put(CATALOG_VERSIONS, catalogVersions);
		parameters.put(APPROVAL_STATUSES, Arrays.asList(ArticleApprovalStatus.APPROVED, ArticleApprovalStatus.UNAPPROVED));

		final PageableData pageableData = new PageableData();
		pageableData.setCurrentPage(start);
		pageableData.setPageSize(count);

		return searchProductsInImpersonationService(parameters, pageableData, WATERS_PRODUCT_QUERY);
	}

	private SearchPageData<ProductModel> searchProductsInImpersonationService(final Map<String, Object> parameters, final PageableData pageableData, final String watersProductQuery)
	{
		final ImpersonationContext impersonationContext = new ImpersonationContext();
		impersonationContext.setUser(getUserService().getAdminUser());
		try
		{
			return getImpersonationService().executeInContext(impersonationContext,
				(ImpersonationService.Executor<SearchPageData<ProductModel>, Throwable>) () ->
					getPagedFlexibleSearchService().search(watersProductQuery, parameters, pageableData));
		}
		catch (Throwable throwable)
		{
			throwable.printStackTrace();
		}
		return getPagedFlexibleSearchService().search(watersProductQuery, parameters, pageableData);
	}

	@Override
	public SearchPageData<ProductModel> getModifiedProducts(final Collection<CatalogVersionModel> catalogVersions, final Date timestamp, final int start, final int count)
	{
		final Map<String, Object> parameters = Maps.newHashMap();
		parameters.put(CATALOG_VERSIONS, catalogVersions);
		parameters.put(APPROVAL_STATUSES, Arrays.asList(ArticleApprovalStatus.APPROVED, ArticleApprovalStatus.UNAPPROVED));
		parameters.put(MODIFIED_TIME, timestamp);

		final PageableData pageableData = new PageableData();
		pageableData.setCurrentPage(start);
		pageableData.setPageSize(count);

		return searchProductsInImpersonationService(parameters, pageableData, MODIFIED_WATERS_PRODUCTS_QUERY);
	}


	@Required
	public void setPagedFlexibleSearchService(final PagedFlexibleSearchService pagedFlexibleSearchService)
	{
		this.pagedFlexibleSearchService = pagedFlexibleSearchService;
	}

	public PagedFlexibleSearchService getPagedFlexibleSearchService() {
		return pagedFlexibleSearchService;
	}

	public ImpersonationService getImpersonationService()
	{
		return impersonationService;
	}

	public void setImpersonationService(final ImpersonationService impersonationService)
	{
		this.impersonationService = impersonationService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
