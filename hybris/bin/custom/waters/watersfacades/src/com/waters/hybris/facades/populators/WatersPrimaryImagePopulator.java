package com.waters.hybris.facades.populators;


import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.strategies.ProductImageResolutionStrategy;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;

public class WatersPrimaryImagePopulator implements Populator<ProductModel, ProductData>
{

	private ProductImageResolutionStrategy productImageResolutionStrategy;

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		if (!(source instanceof WatersProductModel))
		{
			return;
		}
		if (target.getImages() == null)
		{
			target.setImages(new ArrayList<>());
		}
		final WatersProductModel watersProductModel = (WatersProductModel) source;

		final ImageData primaryImage = getProductImageResolutionStrategy().populateImageData(watersProductModel.getPrimaryImage(), ImageDataType.PRIMARY);
		target.getImages().add(primaryImage);
	}

	public ProductImageResolutionStrategy getProductImageResolutionStrategy()
	{
		return productImageResolutionStrategy;
	}

	public void setProductImageResolutionStrategy(final ProductImageResolutionStrategy productImageResolutionStrategy)
	{
		this.productImageResolutionStrategy = productImageResolutionStrategy;
	}
}
