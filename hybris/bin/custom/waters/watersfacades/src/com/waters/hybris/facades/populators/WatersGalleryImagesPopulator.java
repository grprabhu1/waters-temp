package com.waters.hybris.facades.populators;


import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.strategies.ProductImageResolutionStrategy;
import com.waters.hybris.core.util.StreamUtil;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class WatersGalleryImagesPopulator implements Populator<ProductModel, ProductData>
{
	private ProductImageResolutionStrategy productImageResolutionStrategy;

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		if (!(source instanceof WatersProductModel) || (((WatersProductModel) source).getImageGallery() == null))
		{
			return;
		}
		final WatersProductModel watersProductModel = (WatersProductModel) source;
		if (target.getImages() == null)
		{
			target.setImages(new LinkedList<>());
		}

		final List<ImageData> galleryImages = StreamUtil.nullSafe(watersProductModel.getImageGallery().getExternalMedias())
			.map(externalMediaModel -> getProductImageResolutionStrategy().populateImageData(externalMediaModel, ImageDataType.GALLERY))
			.collect(Collectors.toList());

		target.getImages().addAll(galleryImages);
	}

	public ProductImageResolutionStrategy getProductImageResolutionStrategy()
	{
		return productImageResolutionStrategy;
	}

	public void setProductImageResolutionStrategy(final ProductImageResolutionStrategy productImageResolutionStrategy)
	{
		this.productImageResolutionStrategy = productImageResolutionStrategy;
	}
}
