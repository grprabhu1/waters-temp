package com.waters.hybris.facades.populators;

import com.waters.hybris.core.enums.SalesStatus;
import com.waters.hybris.core.model.model.WatersProductModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class WaterSaleStatusPopulator implements Populator<ProductModel, ProductData>
{
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		if (productModel instanceof WatersProductModel)
		{
			final SalesStatus salesStatus = ((WatersProductModel) productModel).getSalesStatus();
			if (salesStatus != null) {
				productData.setSalesStatus(salesStatus.getCode());
			}
		}
	}
}
