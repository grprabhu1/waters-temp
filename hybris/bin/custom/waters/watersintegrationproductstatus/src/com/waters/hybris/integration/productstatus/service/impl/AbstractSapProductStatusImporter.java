package com.waters.hybris.integration.productstatus.service.impl;

import com.waters.hybris.core.enums.SalesStatus;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.strategies.SavedValuesStrategy;
import com.waters.hybris.integration.core.constants.WatersintegrationcoreConstants;
import com.waters.hybris.integration.core.service.impl.AbstractProductDataConsumer;
import com.waters.hybris.integration.productstatus.context.ProductStatusImportContext;
import com.waters.hybris.integration.productstatus.data.SapProductStatusData;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.hmc.model.SavedValuesModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractSapProductStatusImporter<T extends SapProductStatusData, C extends ProductStatusImportContext> extends AbstractProductDataConsumer<T, C>
{
	private static final Logger LOG = LoggerFactory.getLogger(AbstractSapProductStatusImporter.class);

	private ProductReferenceService productReferenceService;
	private FlexibleSearchService flexibleSearchService;

	private UserService userService;
	private SavedValuesStrategy savedValuesStrategy;
	private Optional<SavedValuesModel> savedValuesModel = Optional.empty();

	@Override
	protected boolean isValid(final T item, final C context)
	{
		return item != null;
	}

	@Override
	public void postAccept(final C context)
	{
		final Set<String> processedItemIds = context.getProcessedItemIds();
		final List<WatersProductModel> statusInactiveProducts = getStatusActiveProducts().stream()
			.filter(product -> !processedItemIds.contains(product.getCode()))
			.collect(Collectors.toList());

		statusInactiveProducts.forEach(product -> makeStatusInactive(product, context));
		getModelService(context).saveAll(statusInactiveProducts);
	}

	protected abstract List<WatersProductModel> getStatusActiveProducts();
	protected abstract void makeStatusInactive(final WatersProductModel product, final C context);

	@Override
	protected void internalProcess(final T item, final C context)
	{
		updateSapProductStatus(item, context);
	}

	protected void updateSapProductStatus(final T item, final C context)
	{
		boolean success = false;

		final ProductModel product = getExistingProductForCode(item.getPartNumber());
		if (product instanceof WatersProductModel)
		{
			success = updateWatersProductWithImportData((WatersProductModel) product, item, context);
			context.getBatchCommitter().add(product);
		}
		else
		{
			LOG.debug("Couldn't find Product [{}] in the system, skipping the record", item.getPartNumber());
		}

		if (success)
		{
			context.incrementSuccess();
		}
		else
		{
			context.incrementError();
		}

		context.getProcessedItemIds().add(item.getPartNumber());
	}

	protected abstract boolean updateWatersProductWithImportData(final WatersProductModel product, final T item, final C context);

	protected boolean updateWatersProductStatus(final WatersProductModel product, final String replacementCode, final SalesStatus withReplacement, final SalesStatus noReplacement, final C context)
	{
		boolean success = false;

		SalesStatus status = null;
		WatersProductModel replacementPart = null;
		final UserModel sapUserModel = getSapUser();

		if (StringUtils.isNotBlank(replacementCode))
		{
			final ProductModel replacement = getExistingProductForCode(replacementCode);
			if (replacement instanceof WatersProductModel)
			{
				replacementPart = (WatersProductModel) replacement;
				status = withReplacement;
			}
			else
			{
				LOG.debug("Couldn't find replacement product [{}] in the system, skipping the record", replacementCode);
			}
		}
		else
		{
			status = noReplacement;
		}

		if (status != null)
		{
			ensureWatersProductReplacement(product, replacementPart, context);

			savedValuesModel = getSavedValuesStrategy().create(product, sapUserModel, status, product.getSalesStatus(), false);
			product.setSalesStatus(status);

			// Workflow status update
			if (status == noReplacement && product.getApprovalStatus() == ArticleApprovalStatus.APPROVED)
			{
				savedValuesModel = getSavedValuesStrategy().create(product, sapUserModel, ArticleApprovalStatus.UNAPPROVED, product.getApprovalStatus(), false);
				product.setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);

				if (LOG.isInfoEnabled())
				{
					LOG.info("Product workflow status of product [{}] updated as [{}] because it was discontinued/obsolete with no replacements", product.getCode(), ArticleApprovalStatus.UNAPPROVED.getCode());
				}
			}
			else if (status == withReplacement && product.getApprovalStatus() == ArticleApprovalStatus.UNAPPROVED)
			{
				savedValuesModel = getSavedValuesStrategy().create(product, sapUserModel, ArticleApprovalStatus.APPROVED, product.getApprovalStatus(), false);
				product.setApprovalStatus(ArticleApprovalStatus.APPROVED);

				if (LOG.isInfoEnabled())
				{
					LOG.info("Product workflow status of product [{}] updated as [{}] because it was discontinued/obsolete with replacements", product.getCode(), ArticleApprovalStatus.APPROVED.getCode());
				}
			}

			if (LOG.isInfoEnabled())
			{
				LOG.info("Product sales status of product [{}] updated as [{}]", product.getCode(), status.getCode());
			}

			success = true;
		}
		if(savedValuesModel!=null && savedValuesModel.isPresent())
		{
			getModelService(context).save(savedValuesModel.get());
		}

		return success;
	}

	protected UserModel getSapUser()
	{
		try
		{
			return getUserService().getUserForUID(WatersintegrationcoreConstants.SAP_USER_UID);
		}
		catch (final Throwable t)
		{
			LOG.warn("Failed to get User with id " + WatersintegrationcoreConstants.SAP_USER_UID);
		}

		return null;
	}

	protected void makeWatersProductSalesStatusActive(final WatersProductModel product, final C context)
	{
		final UserModel sapUserModel = getSapUser();

		ensureWatersProductReplacement(product, null, context);

		savedValuesModel = getSavedValuesStrategy().create(product, sapUserModel, SalesStatus.ACTIVE, product.getSalesStatus(), false);
		product.setSalesStatus(SalesStatus.ACTIVE);

		if (LOG.isInfoEnabled())
		{
			LOG.info("Product sales status of product [{}] updated as [{}]", product.getCode(), SalesStatus.ACTIVE.getCode());
		}

		// Workflow status update
		if (product.getApprovalStatus() == ArticleApprovalStatus.UNAPPROVED)
		{
			savedValuesModel = getSavedValuesStrategy().create(product, sapUserModel, ArticleApprovalStatus.APPROVED, product.getApprovalStatus(), true);
			product.setApprovalStatus(ArticleApprovalStatus.APPROVED);

			if (LOG.isInfoEnabled())
			{
				LOG.info("Product workflow status of product [{}] updated as [{}] because it's sales status is active again", product.getCode(), ArticleApprovalStatus.APPROVED.getCode());
			}
		}
		if(savedValuesModel!=null && savedValuesModel.isPresent())
		{
			getModelService(context).save(savedValuesModel.get());
		}
	}

	protected void ensureWatersProductReplacement(final WatersProductModel source, final WatersProductModel target, final C context)
	{
		final ModelService modelService = getModelService(context);

		Collection<ProductReferenceModel> replacementRefsToRemove = null;

		final Collection<ProductReferenceModel> replacementRefs = getProductReferenceService().getProductReferencesForSourceProduct(source, ProductReferenceTypeEnum.REPLACEMENT_PART, false);
		if (target == null)
		{
			replacementRefsToRemove = replacementRefs;
		}
		else
		{
			ProductReferenceModel replacementPartRef = null;
			if (CollectionUtils.isNotEmpty(replacementRefs))
			{
				replacementPartRef = replacementRefs.stream()
					.filter(prodRef -> target.getCode().equalsIgnoreCase(prodRef.getTarget().getCode()))
					.findFirst()
					.orElse(null);

				replacementRefsToRemove = replacementPartRef != null ? CollectionUtils.removeAll(replacementRefs, Collections.singleton(replacementPartRef)) : replacementRefs;
			}

			if (replacementPartRef == null)
			{
				replacementPartRef = modelService.create(ProductReferenceModel.class);
				replacementPartRef.setSource(source);
				replacementPartRef.setTarget(target);
				replacementPartRef.setReferenceType(ProductReferenceTypeEnum.REPLACEMENT_PART);
				replacementPartRef.setPreselected(true);
			}

			replacementPartRef.setActive(true);
			modelService.save(replacementPartRef);
		}

		if (CollectionUtils.isNotEmpty(replacementRefsToRemove))
		{
			modelService.removeAll(replacementRefsToRemove);
		}
	}

	@Required
	protected ProductReferenceService getProductReferenceService()
	{
		return productReferenceService;
	}

	public void setProductReferenceService(final ProductReferenceService productReferenceService)
	{
		this.productReferenceService = productReferenceService;
	}

	protected FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected SavedValuesStrategy getSavedValuesStrategy()
	{
		return savedValuesStrategy;
	}

	@Required
	public void setSavedValuesStrategy(final SavedValuesStrategy savedValuesStrategy)
	{
		this.savedValuesStrategy = savedValuesStrategy;
	}
}
