package com.waters.hybris.integration.productstatus.service.impl;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.integration.productstatus.constants.WatersintegrationproductstatusConstants;
import com.waters.hybris.integration.productstatus.context.ProductStatusImportContext;
import com.waters.hybris.integration.productstatus.data.SapColdChainPartData;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.hmc.model.SavedValuesModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class SapColdChainPartImporter extends AbstractSapProductStatusImporter<SapColdChainPartData, ProductStatusImportContext>
{
	private static final Logger LOG = LoggerFactory.getLogger(SapColdChainPartImporter.class);

	private static final String GET_COLD_CHAIN_PRODUCTS = "SELECT {p:" + WatersProductModel.PK + "}" +
		" FROM {" + WatersProductModel._TYPECODE + " AS p" +
		" JOIN " + ProductFeatureModel._TYPECODE + " AS pf" +
		" ON {pf:" + ProductFeatureModel.PRODUCT + "} = {p:" + WatersProductModel.PK + "}}" +
		" WHERE {pf:" + ProductFeatureModel.QUALIFIER + "} = ?qualifier" +
		" AND {pf:" + ProductFeatureModel.BOOLEANVALUE + "} = ?status";

	protected boolean updateWatersProductWithImportData(final WatersProductModel product, final SapColdChainPartData item, final ProductStatusImportContext context)
	{
		return updateColdChainValue(product, true, context);
	}

	@Override
	protected List<WatersProductModel> getStatusActiveProducts()
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("status", Boolean.TRUE);
		params.put("qualifier", WatersintegrationproductstatusConstants.CLASSIFICATION_ATTRIBUTE_COLD_CHAIN_SHIPPING_QUALIFIER);

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(GET_COLD_CHAIN_PRODUCTS, params);
		final SearchResult<WatersProductModel> searchResult = getFlexibleSearchService().search(searchQuery);
		return searchResult.getResult();
	}

	@Override
	protected void makeStatusInactive(final WatersProductModel product, final ProductStatusImportContext context)
	{
		updateColdChainValue(product, false, context);
	}

	protected boolean updateColdChainValue(final WatersProductModel product, final boolean value, final ProductStatusImportContext context)
	{
		boolean success = false;

		final FeatureList featureList = getClassificationService().getFeatures(product);
		if (featureList != null)
		{
			success = populateFeatureValue(featureList, WatersintegrationproductstatusConstants.CLASSIFICATION_ATTRIBUTE_COLD_CHAIN_SHIPPING_QUALIFIER, value);
			if(isFeatureListChanged()){
				final UserModel sapUserModel = getSapUser();
				final Optional<SavedValuesModel> savedValuesModel = getSavedValuesStrategy().create(product, sapUserModel, null, null, true);
				if(savedValuesModel!=null && savedValuesModel.isPresent())
				{
					getModelService(context).save(savedValuesModel.get());
				}
				getClassificationService().replaceFeatures(product, featureList);
			}

			if (success && LOG.isInfoEnabled())
			{
				LOG.info("Product [{}] cold chain shipping status has been updated as [{}]", product.getCode(), value);
			}
		}
		else
		{
			LOG.error("Product {[}] has no feature list, check whether the product has been classified", product.getCode());
		}

		return success;
	}
}
