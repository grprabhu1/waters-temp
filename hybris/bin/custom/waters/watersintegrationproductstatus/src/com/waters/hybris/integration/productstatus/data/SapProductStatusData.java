package com.waters.hybris.integration.productstatus.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SapProductStatusData
{
	@JsonProperty(value = "partNumber")
	protected String partNumber;

	public String getPartNumber()
	{
		return partNumber;
	}
}
