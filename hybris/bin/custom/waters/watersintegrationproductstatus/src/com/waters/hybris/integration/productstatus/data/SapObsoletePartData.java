package com.waters.hybris.integration.productstatus.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
	"partNumber",
	"replacementPart",
})
public class SapObsoletePartData extends SapProductStatusData
{
	@JsonProperty(value = "replacementPart")
	private String replacementPart;

	public String getReplacementPart()
	{
		return replacementPart;
	}

	@Override
	public String toString()
	{
		return "SapObsoletePartData{" +
			"partNumber='" + partNumber + '\'' +
			"replacementPart='" + replacementPart + '\'' +
			"}";
	}
}
