package com.waters.hybris.integration.productstatus.service.impl;

import com.waters.hybris.core.enums.SalesStatus;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.integration.productstatus.context.ProductStatusImportContext;
import com.waters.hybris.integration.productstatus.data.SapObsoletePartData;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SapObsoletePartImporter extends AbstractSapProductStatusImporter<SapObsoletePartData, ProductStatusImportContext>
{
	private static final Logger LOG = LoggerFactory.getLogger(SapObsoletePartImporter.class);

	private static final String GET_OBSOLETE_PRODUCTS = "SELECT {" + WatersProductModel.PK + "}" +
		" FROM {" + WatersProductModel._TYPECODE + "}" +
		" WHERE {" + WatersProductModel.SALESSTATUS + "} IN (?statuses)";

	protected boolean updateWatersProductWithImportData(final WatersProductModel product, final SapObsoletePartData item, final ProductStatusImportContext context)
	{
		return updateWatersProductStatus(product, item.getReplacementPart(), SalesStatus.OBSOLETEWITHREPLACEMENT, SalesStatus.OBSOLETENOREPLACEMENT, context);
	}

	@Override
	protected List<WatersProductModel> getStatusActiveProducts()
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("statuses", Arrays.asList(SalesStatus.OBSOLETEWITHREPLACEMENT, SalesStatus.OBSOLETENOREPLACEMENT));

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(GET_OBSOLETE_PRODUCTS, params);
		final SearchResult<WatersProductModel> searchResult = getFlexibleSearchService().search(searchQuery);
		return searchResult.getResult();
	}

	@Override
	public void postAccept(ProductStatusImportContext context) {
		return; // do nothing
	}

	@Override
	protected void makeStatusInactive(final WatersProductModel product, final ProductStatusImportContext context)
	{
		return; // do nothing
	}
}
