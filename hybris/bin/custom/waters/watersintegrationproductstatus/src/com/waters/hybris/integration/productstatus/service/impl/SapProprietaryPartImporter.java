package com.waters.hybris.integration.productstatus.service.impl;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.integration.productstatus.context.ProductStatusImportContext;
import com.waters.hybris.integration.productstatus.data.SapProprietaryPartData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.hmc.model.SavedValuesModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class SapProprietaryPartImporter extends AbstractSapProductStatusImporter<SapProprietaryPartData, ProductStatusImportContext>
{
	private static final Logger LOG = LoggerFactory.getLogger(SapProprietaryPartImporter.class);

	private static final String GET_PROPRIETARY_PRODUCTS = "SELECT {" + WatersProductModel.PK + "}" +
		" FROM {" + WatersProductModel._TYPECODE + "}" +
		" WHERE {" + WatersProductModel.PROPRIETARY + "} = ?status";

	protected boolean updateWatersProductWithImportData(final WatersProductModel product, final SapProprietaryPartData item, final ProductStatusImportContext context)
	{
		updateProprietaryStatus(product, true, context);

		return true;
	}

	@Override
	protected List<WatersProductModel> getStatusActiveProducts()
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("status", Boolean.TRUE);

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(GET_PROPRIETARY_PRODUCTS, params);
		final SearchResult<WatersProductModel> searchResult = getFlexibleSearchService().search(searchQuery);
		return searchResult.getResult();
	}

	@Override
	protected void makeStatusInactive(final WatersProductModel product, final ProductStatusImportContext context)
	{
		if (product.isProprietary())
		{
			updateProprietaryStatus(product, false, context);
		}
	}

	protected void updateProprietaryStatus(final WatersProductModel product, final boolean value, final ProductStatusImportContext context)
	{
		final UserModel sapUserModel = getSapUser();
		final Optional<SavedValuesModel> savedValuesModel = getSavedValuesStrategy().create(product, sapUserModel, value, product.isProprietary(), true);
		if(savedValuesModel!=null && savedValuesModel.isPresent())
		{
			getModelService(context).save(savedValuesModel.get());
		}
		product.setProprietary(value);

		if (LOG.isInfoEnabled())
		{
			LOG.info("Product [{}] proprietary status has been updated as [{}]", product.getCode(), value);
		}
	}
}
