package com.waters.hybris.integration.productstatus.service.impl;

import com.waters.hybris.core.enums.SalesStatus;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.integration.productstatus.context.ProductStatusImportContext;
import com.waters.hybris.integration.productstatus.data.SapDiscontinuedPartData;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SapDiscontinuedPartImporter extends AbstractSapProductStatusImporter<SapDiscontinuedPartData, ProductStatusImportContext>
{
	private static final Logger LOG = LoggerFactory.getLogger(SapDiscontinuedPartImporter.class);

	private static final String GET_DISCONTINUED_PRODUCTS = "SELECT {" + WatersProductModel.PK + "}" +
		" FROM {" + WatersProductModel._TYPECODE + "}" +
		" WHERE {" + WatersProductModel.SALESSTATUS + "} IN (?statuses)";

	protected boolean updateWatersProductWithImportData(final WatersProductModel product, final SapDiscontinuedPartData item, final ProductStatusImportContext context)
	{
		return updateWatersProductStatus(product, item.getReplacementPart(), SalesStatus.DISCONTINUEWITHREPLACEMENT, SalesStatus.DISCONTINUENOREPLACEMENT, context);
	}

	@Override
	protected List<WatersProductModel> getStatusActiveProducts()
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("statuses", Arrays.asList(SalesStatus.DISCONTINUEWITHREPLACEMENT, SalesStatus.DISCONTINUENOREPLACEMENT));

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(GET_DISCONTINUED_PRODUCTS, params);
		final SearchResult<WatersProductModel> searchResult = getFlexibleSearchService().search(searchQuery);
		return searchResult.getResult();
	}

	@Override
	public void postAccept(ProductStatusImportContext context) {
		return; // do nothing
	}

	@Override
	protected void makeStatusInactive(final WatersProductModel product, final ProductStatusImportContext context)
	{
		return; // do nothing
	}
}
