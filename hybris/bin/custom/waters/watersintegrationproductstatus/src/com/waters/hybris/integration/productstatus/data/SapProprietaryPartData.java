package com.waters.hybris.integration.productstatus.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
	"partNumber",
	"ignore1",
})
public class SapProprietaryPartData extends SapProductStatusData
{
	@JsonProperty(value = "ignore1")
	private String ignore1;

	@Override
	public String toString()
	{
		return "SapProprietaryPartData{" +
			"partNumber='" + partNumber + '\'' +
			"}";
	}
}
