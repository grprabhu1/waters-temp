package com.waters.hybris.integration.productstatus.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
	"partNumber",
	"ignore1",
	"ignore2",
	"ignore3",
	"ignore4",
})
public class SapColdChainPartData extends SapProductStatusData
{
	@JsonProperty(value = "ignore1")
	private String ignore1;
	@JsonProperty(value = "ignore2")
	private String ignore2;
	@JsonProperty(value = "ignore3")
	private String ignore3;
	@JsonProperty(value = "ignore4")
	private String ignore4;

	@Override
	public String toString()
	{
		return "SapColdChainPartData{" +
			"partNumber='" + partNumber + '\'' +
			"}";
	}
}
