package com.waters.hybris.integration.productstatus.context;

import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.util.BatchCommitter;
import com.waters.hybris.integration.core.context.MediaImportContext;
import com.waters.hybris.integration.core.context.MediaImportContextBuilder;
import com.waters.hybris.integration.core.model.record.FileImportRecordModel;

public class ProductStatusImportContextBuilder extends MediaImportContextBuilder
{
	@Override
	protected MediaImportContext createContext(final FileImportRecordModel importRecord, final AuditContext auditContext, final BatchCommitter batchCommitter)
	{
		return new ProductStatusImportContext(importRecord, auditContext, batchCommitter);
	}
}
