package com.waters.hybris.stepdefs.integration.productstatus;

import cucumber.api.java.en.Given;
import org.apache.log4j.Logger;

public class ProductStatusStepDefs
{
	private static final Logger LOG = Logger.getLogger(ProductStatusStepDefs.class);

	@Given("^Proprietary part feed record setup$")
	public void givenProprietaryPartFeedRecordSetup()
	{
		LOG.info("Proprietary part feed record setup");
	}

	@Given("^Discontinued part feed record setup$")
	public void givenDiscontinuedPartFeedRecordSetup()
	{
		LOG.info("Discontinued part feed record setup");
	}

	@Given("^Obsolete part feed record setup$")
	public void givenObsoletePartFeedRecordSetup()
	{
		LOG.info("Obsolete part feed record setup");
	}

	@Given("^Cold Chain part feed record setup$")
	public void givenColdChainPartFeedRecordSetup()
	{
		LOG.info("Cold Chain part feed record setup");
	}
}
