package com.waters.hybris.integration.productstatus.context;

import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.util.BatchCommitter;
import com.waters.hybris.integration.core.context.MediaImportContext;
import com.waters.hybris.integration.core.model.record.FileImportRecordModel;

import java.util.HashSet;
import java.util.Set;

public class ProductStatusImportContext extends MediaImportContext
{
	final private Set<String> processedItemIds = new HashSet<>();

	public ProductStatusImportContext(final FileImportRecordModel importRecordModel, final AuditContext auditContext, final BatchCommitter batchCommitter)
	{
		super(importRecordModel, auditContext, batchCommitter);
	}

	public Set<String> getProcessedItemIds()
	{
		return processedItemIds;
	}
}
