@ProprietaryPartFeed @Transaction
Feature: Proprietary Part Feed

	@AdminUser
	Scenario: Proprietary Part Feed
		Given Proprietary part feed record setup
		Given That the Impex "createTestProprietaryPartFileImportRecords.impex" to "Setup proprietary part feed record" has been executed
		When the "proprietaryPartFeedImportJob" has been run
		And the job completed successfully
		Then the following products are proprietary
			| id | catalog | catalogVersion |
			| 186000115 | watersProductCatalog | Staged |
			| 186000116 | watersProductCatalog | Staged |
		And the following products are not proprietary
			| id | catalog | catalogVersion |
			| 186000117 | watersProductCatalog | Staged |
			| 186000118 | watersProductCatalog | Staged |
