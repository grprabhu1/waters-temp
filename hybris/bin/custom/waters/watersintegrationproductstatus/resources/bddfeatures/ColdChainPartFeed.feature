@ColdChainPartFeed @Transaction
Feature: ColdChain Part Feed

	@AdminUser
	Scenario: Cold Chain Part Feed
		Given Cold Chain part feed record setup
		Given That the Impex "createTestColdChainPartFileImportRecords.impex" to "Setup cold chain part feed record" has been executed
		When the "coldChainPartFeedImportJob" has been run
		And the job completed successfully
		Then the following products are cold chain
			| id | catalog | catalogVersion |
			| 186000252 | watersProductCatalog | Staged |
			| 186000253 | watersProductCatalog | Staged |
		And the following products are not cold chain
			| id | catalog | catalogVersion |
			| 186000254 | watersProductCatalog | Staged |
			| 186000255 | watersProductCatalog | Staged |
			| 186000256 | watersProductCatalog | Staged |
