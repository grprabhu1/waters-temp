@DiscontinuedPartFeed @Transaction
Feature: Discontinued Part Feed

	@AdminUser
	Scenario: Discontinued Part Feed
		Given Discontinued part feed record setup
		Given That the Impex "createTestDiscontinuedPartFileImportRecords.impex" to "Setup discontinued part feed record" has been executed
		When the "discontinuedPartFeedImportJob" has been run
		And the job completed successfully
		Then the following products are discontinued
			| id | catalog | catalogVersion |
			| 186008994 | watersProductCatalog | Staged |
			| 186008995 | watersProductCatalog | Staged |
		And the following products are discontinued with replacement
			| id | catalog | catalogVersion |
			| 186008996 | watersProductCatalog | Staged |
			| 186008997 | watersProductCatalog | Staged |
		And the following products are not discontinued
			| id | catalog | catalogVersion |
			| 186008998 | watersProductCatalog | Staged |
