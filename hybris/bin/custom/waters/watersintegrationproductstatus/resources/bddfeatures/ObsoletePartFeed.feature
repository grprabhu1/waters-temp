@ObsoletePartFeed @Transaction
Feature: Obsolete Part Feed

	@AdminUser
	Scenario: Obsolete Part Feed
		Given Obsolete part feed record setup
		Given That the Impex "createTestObsoletePartFileImportRecords.impex" to "Setup obsolete part feed record" has been executed
		When the "obsoletePartFeedImportJob" has been run
		And the job completed successfully
		Then the following products are obsolete
			| id | catalog | catalogVersion |
			| WAT097962 | watersProductCatalog | Staged |
			| WAT097963 | watersProductCatalog | Staged |
		And the following products are obsolete with replacement
			| id | catalog | catalogVersion |
			| WAT097964 | watersProductCatalog | Staged |
			| WAT097965 | watersProductCatalog | Staged |
		And the following products are not obsolete
			| id | catalog | catalogVersion |
			| WAT200500 | watersProductCatalog | Staged |
			| WAT200501 | watersProductCatalog | Staged |
