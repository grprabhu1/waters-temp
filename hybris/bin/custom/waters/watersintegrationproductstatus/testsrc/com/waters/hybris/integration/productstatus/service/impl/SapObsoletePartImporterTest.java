package com.waters.hybris.integration.productstatus.service.impl;

import com.waters.hybris.core.enums.SalesStatus;
import com.waters.hybris.integration.productstatus.data.SapObsoletePartData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SapObsoletePartImporterTest extends AbstractProductStatusImporterTest
{
	@InjectMocks
	private SapObsoletePartImporter importer = new SapObsoletePartImporter();

	@Mock
	private SapObsoletePartData sapObsoletePartData;

	private ProductReferenceModel productReference = new ProductReferenceModel();

	@Test
	public void shouldDoNothing_WhenNotInSystem()
	{
		given(sapObsoletePartData.getPartNumber()).willReturn(NON_EXISTING_PROD_CODE);

		importer.internalProcess(sapObsoletePartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(NON_EXISTING_PROD_CODE));
		verify(product, never()).setSalesStatus(any());
	}

	@Test
	public void shouldDoNothing_WhenWithReplacementButReplacementDoesntExists()
	{
		given(sapObsoletePartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(sapObsoletePartData.getReplacementPart()).willReturn(REPLACEMENT_PROD_CODE);
		given(productService.getProductForCode(eq(catalogVersion), eq(REPLACEMENT_PROD_CODE))).willReturn(null);

		importer.internalProcess(sapObsoletePartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verify(product, never()).setSalesStatus(any());
	}

	@Test
	public void shouldMarkAsObsoleteWithNoReplacementAndNoChangeToWorkflowStatus_WhenNoReplacementAndNotPublished()
	{
		given(sapObsoletePartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(sapObsoletePartData.getReplacementPart()).willReturn(null);
		given(product.getApprovalStatus()).willReturn(ArticleApprovalStatus.PROOF);

		given(productReferenceService.getProductReferencesForSourceProduct(product, ProductReferenceTypeEnum.REPLACEMENT_PART, false)).willReturn(null);

		importer.internalProcess(sapObsoletePartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verify(product, times(1)).setSalesStatus(SalesStatus.OBSOLETENOREPLACEMENT);
		verify(product, never()).setApprovalStatus(any());
	}

	@Test
	public void shouldMarkAsObsoleteWithNoReplacementAndArchive_WhenNoReplacementAndPublished()
	{
		given(sapObsoletePartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(sapObsoletePartData.getReplacementPart()).willReturn(null);
		given(product.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);

		given(productReferenceService.getProductReferencesForSourceProduct(product, ProductReferenceTypeEnum.REPLACEMENT_PART, false)).willReturn(null);

		importer.internalProcess(sapObsoletePartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verify(product, times(1)).setSalesStatus(SalesStatus.OBSOLETENOREPLACEMENT);
		verify(product, times(1)).setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
	}

	@Test
	public void shouldMarkAsObsoleteWithReplacement_WhenWithReplacement()
	{
		given(sapObsoletePartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(sapObsoletePartData.getReplacementPart()).willReturn(REPLACEMENT_PROD_CODE);

		given(productReferenceService.getProductReferencesForSourceProduct(product, ProductReferenceTypeEnum.REPLACEMENT_PART, false)).willReturn(null);
		given(modelService.create(ProductReferenceModel.class)).willReturn(productReference);

		importer.internalProcess(sapObsoletePartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verify(product, times(1)).setSalesStatus(SalesStatus.OBSOLETEWITHREPLACEMENT);

		assertEquals(product, productReference.getSource());
		assertEquals(replacementProduct, productReference.getTarget());
		assertEquals(ProductReferenceTypeEnum.REPLACEMENT_PART, productReference.getReferenceType());
		assertEquals(true, productReference.getActive());
	}
}
