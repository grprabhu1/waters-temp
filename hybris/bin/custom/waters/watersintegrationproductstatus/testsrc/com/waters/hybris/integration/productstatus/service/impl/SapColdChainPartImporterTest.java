package com.waters.hybris.integration.productstatus.service.impl;

import com.waters.hybris.integration.productstatus.data.SapColdChainPartData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SapColdChainPartImporterTest extends AbstractProductStatusImporterTest
{
	@InjectMocks
	private SapColdChainPartImporter importer = new SapColdChainPartImporter();

	@Mock
	private SapColdChainPartData sapColdChainPartData;

	@Mock
	private FeatureList featureList;
	@Mock
	private Feature featureColdChain;
	@Mock
	private FeatureValue oldFeatureColdChainValue;
	@Mock
	private FeatureList notInFeedProductFeatureList;
	@Mock
	private Feature notInFeedProductFeatureColdChain;
	@Mock
	private FeatureValue oldNotInFeedProductFeatureColdChainValue;

	@Test
	public void shouldDoNothing_WhenNotInSystem()
	{
		given(sapColdChainPartData.getPartNumber()).willReturn(NON_EXISTING_PROD_CODE);

		importer.internalProcess(sapColdChainPartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(NON_EXISTING_PROD_CODE));
		verify(classificationService, never()).getFeatures(any());
	}

	@Test
	public void shouldAddFeatureValue_WhenProductInSystemAndNoFeatureValue()
	{
		given(sapColdChainPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(classificationService.getFeatures(product)).willReturn(featureList);
		given(featureList.getFeatureByCode("WatersClassification/1.0/ConsumablesAndSpares.coldchainshipping")).willReturn(featureColdChain);

		importer.internalProcess(sapColdChainPartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verifyFeatureValueAdded(featureColdChain, true);
	}

	@Test
	public void shouldUpdateFeatureValue_WhenProductInSystemAndFeatureValuePresent()
	{
		given(sapColdChainPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(classificationService.getFeatures(product)).willReturn(featureList);
		given(featureList.getFeatureByCode("WatersClassification/1.0/ConsumablesAndSpares.coldchainshipping")).willReturn(featureColdChain);
		given(featureColdChain.getValue()).willReturn(oldFeatureColdChainValue);

		importer.internalProcess(sapColdChainPartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verifyFeatureValueModified(oldFeatureColdChainValue, true);
	}

	@Test
	public void shouldUnMarkAsColdChain_WhenNotPresentInFeed()
	{
		given(sapColdChainPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(classificationService.getFeatures(product)).willReturn(featureList);
		given(featureList.getFeatureByCode("WatersClassification/1.0/ConsumablesAndSpares.coldchainshipping")).willReturn(featureColdChain);
		given(featureColdChain.getValue()).willReturn(oldFeatureColdChainValue);

		given(classificationService.getFeatures(productNotInFeed)).willReturn(notInFeedProductFeatureList);
		given(notInFeedProductFeatureList.getFeatureByCode("WatersClassification/1.0/ConsumablesAndSpares.coldchainshipping")).willReturn(notInFeedProductFeatureColdChain);
		given(notInFeedProductFeatureColdChain.getValue()).willReturn(oldNotInFeedProductFeatureColdChainValue);
		given(flexibleSearchService.search(any(FlexibleSearchQuery.class))).willReturn((SearchResult) searchResult);
		given(searchResult.getResult()).willReturn(Arrays.asList(product, productNotInFeed));

		importer.internalProcess(sapColdChainPartData, context);
		importer.postAccept(context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verifyFeatureValueModified(oldFeatureColdChainValue, true);

		verifyFeatureValueModified(oldNotInFeedProductFeatureColdChainValue, false);
	}


	@Test
	public void shouldNotReplaceTheFeaturesWhenDataDidNotChange()
	{
		given(sapColdChainPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(classificationService.getFeatures(product)).willReturn(featureList);
		given(featureList.getFeatureByCode("WatersClassification/1.0/ConsumablesAndSpares.coldchainshipping")).willReturn(featureColdChain);

		given(featureColdChain.getValue()).willReturn(oldFeatureColdChainValue);
		given(featureColdChain.getValue().getValue()).willReturn(true);
		importer.internalProcess(sapColdChainPartData, context);

		verify(classificationService, never()).replaceFeatures(product, featureList);
	}

	@Test
	public void shouldReplaceTheFeaturesWhenDataDidChange()
	{
		given(sapColdChainPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(classificationService.getFeatures(product)).willReturn(featureList);
		given(featureList.getFeatureByCode("WatersClassification/1.0/ConsumablesAndSpares.coldchainshipping")).willReturn(featureColdChain);

		given(featureColdChain.getValue()).willReturn(oldFeatureColdChainValue);
		given(featureColdChain.getValue().getValue()).willReturn(false);
		importer.internalProcess(sapColdChainPartData, context);

		verify(classificationService, times(1)).replaceFeatures(product, featureList);
	}

	protected void verifyFeatureValueModified(final FeatureValue featureValue, final boolean value)
	{
		verify(featureValue, times(1)).setValue(value);
	}

	protected void verifyFeatureValueAdded(final Feature feature, final boolean value)
	{
		ArgumentCaptor<FeatureValue> argument = ArgumentCaptor.forClass(FeatureValue.class);
		verify(feature).addValue(argument.capture());

		assertEquals(value, argument.getValue().getValue());
	}
}
