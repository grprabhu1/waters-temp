package com.waters.hybris.integration.productstatus.service.impl;

import com.waters.hybris.core.enums.SalesStatus;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.integration.productstatus.data.SapDiscontinuedPartData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SapDiscontinuedPartImporterTest extends AbstractProductStatusImporterTest
{
	@InjectMocks
	private SapDiscontinuedPartImporter importer = new SapDiscontinuedPartImporter();

	@Mock
	private SapDiscontinuedPartData sapDiscontinuedPartData;

	private ProductReferenceModel productReference = new ProductReferenceModel();

	private ProductReferenceModel existingProductReference1 = new ProductReferenceModel();
	private ProductReferenceModel existingProductReference2 = new ProductReferenceModel();

	@Test
	public void shouldDoNothing_WhenNotInSystem()
	{
		given(sapDiscontinuedPartData.getPartNumber()).willReturn(NON_EXISTING_PROD_CODE);

		importer.internalProcess(sapDiscontinuedPartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(NON_EXISTING_PROD_CODE));
		verify(product, never()).setSalesStatus(any());
	}

	@Test
	public void shouldDoNothing_WhenWithReplacementButReplacementDoesntExists()
	{
		given(sapDiscontinuedPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(sapDiscontinuedPartData.getReplacementPart()).willReturn(REPLACEMENT_PROD_CODE);
		given(productService.getProductForCode(eq(catalogVersion), eq(REPLACEMENT_PROD_CODE))).willReturn(null);

		importer.internalProcess(sapDiscontinuedPartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verify(product, never()).setSalesStatus(any());
	}

	@Test
	public void shouldMarkAsDiscontinuedWithNoReplacementAndNoChangeToWorkflowStatus_WhenNoReplacementAndNotPublished()
	{
		given(sapDiscontinuedPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(sapDiscontinuedPartData.getReplacementPart()).willReturn(null);
		given(product.getApprovalStatus()).willReturn(ArticleApprovalStatus.PROOF);

		given(productReferenceService.getProductReferencesForSourceProduct(product, ProductReferenceTypeEnum.REPLACEMENT_PART, false)).willReturn(null);

		importer.internalProcess(sapDiscontinuedPartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verify(product, times(1)).setSalesStatus(SalesStatus.DISCONTINUENOREPLACEMENT);
		verify(product, never()).setApprovalStatus(any());
	}

	@Test
	public void shouldMarkAsDiscontinuedWithNoReplacementAndArchive_WhenNoReplacementAndPublished()
	{
		given(sapDiscontinuedPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(sapDiscontinuedPartData.getReplacementPart()).willReturn(null);
		given(product.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);

		given(productReferenceService.getProductReferencesForSourceProduct(product, ProductReferenceTypeEnum.REPLACEMENT_PART, false)).willReturn(null);

		importer.internalProcess(sapDiscontinuedPartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verify(product, times(1)).setSalesStatus(SalesStatus.DISCONTINUENOREPLACEMENT);
		verify(product, times(1)).setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
	}

	@Test
	public void shouldMarkAsDiscontinuedWithReplacement_WhenWithReplacement()
	{
		given(sapDiscontinuedPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(sapDiscontinuedPartData.getReplacementPart()).willReturn(REPLACEMENT_PROD_CODE);

		given(productReferenceService.getProductReferencesForSourceProduct(product, ProductReferenceTypeEnum.REPLACEMENT_PART, false)).willReturn(null);
		given(modelService.create(ProductReferenceModel.class)).willReturn(productReference);

		importer.internalProcess(sapDiscontinuedPartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verify(product, times(1)).setSalesStatus(SalesStatus.DISCONTINUEWITHREPLACEMENT);

		verify(modelService, times(1)).create(ProductReferenceModel.class);
		assertEquals(product, productReference.getSource());
		assertEquals(replacementProduct, productReference.getTarget());
		assertEquals(ProductReferenceTypeEnum.REPLACEMENT_PART, productReference.getReferenceType());
		assertEquals(true, productReference.getActive());
	}

	@Test
	public void shouldMarkAsDiscontinuedWithNoReplacementRemoveOtherReplacements_WhenWithNoReplacement()
	{
		given(sapDiscontinuedPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(sapDiscontinuedPartData.getReplacementPart()).willReturn(null);

		existingProductReference1.setSource(product);
		existingProductReference1.setReferenceType(ProductReferenceTypeEnum.REPLACEMENT_PART);
		given(productReferenceService.getProductReferencesForSourceProduct(product, ProductReferenceTypeEnum.REPLACEMENT_PART, false)).willReturn(Collections.singleton(existingProductReference1));
		given(modelService.create(ProductReferenceModel.class)).willReturn(productReference);

		importer.internalProcess(sapDiscontinuedPartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verify(product, times(1)).setSalesStatus(SalesStatus.DISCONTINUENOREPLACEMENT);

		ArgumentCaptor<Collection> argument = ArgumentCaptor.forClass(Collection.class);
		verify(modelService).removeAll(argument.capture());
		assertTrue(argument.getValue().contains(existingProductReference1));
	}

	@Test
	public void shouldMarkAsDiscontinuedWithReplacementUseExistingReference_WhenWithReplacement()
	{
		given(sapDiscontinuedPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(sapDiscontinuedPartData.getReplacementPart()).willReturn(REPLACEMENT_PROD_CODE);

		existingProductReference1.setSource(product);
		existingProductReference1.setTarget(replacementProduct);
		existingProductReference1.setReferenceType(ProductReferenceTypeEnum.REPLACEMENT_PART);
		given(productReferenceService.getProductReferencesForSourceProduct(product, ProductReferenceTypeEnum.REPLACEMENT_PART, false)).willReturn(Collections.singleton(existingProductReference1));
		given(modelService.create(ProductReferenceModel.class)).willReturn(productReference);

		importer.internalProcess(sapDiscontinuedPartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verify(product, times(1)).setSalesStatus(SalesStatus.DISCONTINUEWITHREPLACEMENT);
		verify(modelService, never()).create(ProductReferenceModel.class);
		verify(modelService, never()).removeAll(Collections.singleton(existingProductReference1));
	}

	@Test
	public void shouldMarkAsDiscontinuedWithReplacementUseExistingReferenceAndRemoveOther_WhenWithReplacement()
	{
		given(sapDiscontinuedPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(sapDiscontinuedPartData.getReplacementPart()).willReturn(REPLACEMENT_PROD_CODE);

		List<ProductReferenceModel> existingRefs = Arrays.asList(existingProductReference1, existingProductReference2);
		existingProductReference1.setSource(product);
		existingProductReference1.setTarget(replacementProduct);
		existingProductReference1.setReferenceType(ProductReferenceTypeEnum.REPLACEMENT_PART);
		existingProductReference2.setSource(product);
		existingProductReference2.setTarget(mock(WatersProductModel.class));
		existingProductReference2.setReferenceType(ProductReferenceTypeEnum.REPLACEMENT_PART);
		given(productReferenceService.getProductReferencesForSourceProduct(product, ProductReferenceTypeEnum.REPLACEMENT_PART, false)).willReturn(existingRefs);
		given(modelService.create(ProductReferenceModel.class)).willReturn(productReference);

		importer.internalProcess(sapDiscontinuedPartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verify(product, times(1)).setSalesStatus(SalesStatus.DISCONTINUEWITHREPLACEMENT);
		verify(modelService, never()).create(ProductReferenceModel.class);

		ArgumentCaptor<Collection> argument = ArgumentCaptor.forClass(Collection.class);
		verify(modelService).removeAll(argument.capture());
		assertTrue(argument.getValue().contains(existingProductReference2));
	}

	@Test
	public void shouldMarkAsActiveWithApproved_WhenNotInFeed()
	{
		given(sapDiscontinuedPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(sapDiscontinuedPartData.getReplacementPart()).willReturn(null);
		given(product.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);

		given(productReferenceService.getProductReferencesForSourceProduct(product, ProductReferenceTypeEnum.REPLACEMENT_PART, false)).willReturn(null);

		given(flexibleSearchService.search(any(FlexibleSearchQuery.class))).willReturn((SearchResult) searchResult);
		given(searchResult.getResult()).willReturn(Arrays.asList(product, productNotInFeed));
		given(productNotInFeed.getSalesStatus()).willReturn(SalesStatus.DISCONTINUENOREPLACEMENT);
		given(productNotInFeed.getApprovalStatus()).willReturn(ArticleApprovalStatus.UNAPPROVED);

		importer.internalProcess(sapDiscontinuedPartData, context);
		importer.postAccept(context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verify(product, times(1)).setSalesStatus(SalesStatus.DISCONTINUENOREPLACEMENT);
		verify(product, times(1)).setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
		verify(productNotInFeed, times(1)).setSalesStatus(SalesStatus.ACTIVE);
		verify(productNotInFeed, times(1)).setApprovalStatus(ArticleApprovalStatus.APPROVED);
	}
}
