package com.waters.hybris.integration.productstatus.service.impl;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.integration.productstatus.context.ProductStatusImportContext;
import com.waters.hybris.integration.productstatus.data.SapProprietaryPartData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SapProprietaryPartImporterTest extends AbstractProductStatusImporterTest
{
	@InjectMocks
	private SapProprietaryPartImporter importer = new SapProprietaryPartImporter();

	@Mock
	private SapProprietaryPartData sapProprietaryPartData;


	@Test
	public void shouldDoNothing_WhenNotInSystem()
	{
		given(sapProprietaryPartData.getPartNumber()).willReturn(NON_EXISTING_PROD_CODE);

		importer.internalProcess(sapProprietaryPartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(NON_EXISTING_PROD_CODE));
		verify(product, never()).setProprietary(true);
	}

	@Test
	public void shouldMarkAsProprietary_WhenInSystem()
	{
		given(sapProprietaryPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);

		importer.internalProcess(sapProprietaryPartData, context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verify(product, times(1)).setProprietary(true);
	}

	@Test
	public void shouldUnMarkAsProprietary_WhenNotPresentInFeed()
	{
		given(sapProprietaryPartData.getPartNumber()).willReturn(EXISTING_PROD_CODE);
		given(flexibleSearchService.search(any(FlexibleSearchQuery.class))).willReturn((SearchResult) searchResult);
		given(searchResult.getResult()).willReturn(Arrays.asList(product, productNotInFeed));
		given(productNotInFeed.isProprietary()).willReturn(true);

		importer.internalProcess(sapProprietaryPartData, context);
		importer.postAccept(context);

		verify(productService, times(1)).getProductForCode(any(), eq(EXISTING_PROD_CODE));
		verify(product, times(1)).setProprietary(true);

		verify(productNotInFeed, times(1)).setProprietary(false);
	}
}
