package com.waters.hybris.integration.productstatus.service.impl;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.util.BatchCommitter;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.strategies.SavedValuesStrategy;
import com.waters.hybris.integration.core.model.record.FileImportRecordModel;
import com.waters.hybris.integration.core.strategy.ModelServiceSelectionStrategy;
import com.waters.hybris.integration.productstatus.context.ProductStatusImportContext;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.user.UserService;
import org.junit.Before;
import org.mockito.Mock;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

public abstract class AbstractProductStatusImporterTest
{
	protected static final String EXISTING_PROD_CODE = "EXISTS";
	protected static final String NON_EXISTING_PROD_CODE = "NOT_EXISTS";
	protected static final String NOT_IN_FEED__PROD_CODE = "NOT_IN_FEED";
	protected static final String REPLACEMENT_PROD_CODE = "REPLACEMENT";


	@Mock
	protected ProductService productService;
	@Mock
	protected CatalogVersionService catalogVersionService;
	@Mock
	protected ClassificationService classificationService;
	@Mock
	protected TypeService typeService;
	@Mock
	protected ClassificationSystemService classificationSystemService;
	@Mock
	protected ModelServiceSelectionStrategy modelServiceSelectionStrategy;
	@Mock
	protected ModelService modelService;
	@Mock
	protected FlexibleSearchService flexibleSearchService;
	@Mock
	protected MetricRegistry metricRegistry;
	@Mock
	protected Timer timer;
	@Mock
	protected UserService userService;
	@Mock
	protected SavedValuesStrategy savedValuesStrategy;
	@Mock
	protected BatchCommitter batchCommitter;
	@Mock
	protected FileImportRecordModel importRecord;
	@Mock
	protected AuditContext auditContext;
	protected ProductStatusImportContext context;

	@Mock
	protected CatalogVersionModel catalogVersion;
	@Mock
	protected ProductReferenceService productReferenceService;

	@Mock
	protected WatersProductModel product;
	@Mock
	protected UserModel sapUser;
	@Mock
	protected WatersProductModel replacementProduct;
	@Mock
	protected SearchResult<WatersProductModel> searchResult;
	@Mock
	protected WatersProductModel productNotInFeed;

	@Before
	public void setUp()
	{
		context = new ProductStatusImportContext(importRecord, auditContext, batchCommitter);

		given(metricRegistry.timer(anyString())).willReturn(timer);
		given(modelServiceSelectionStrategy.selectModelService(context)).willReturn(modelService);
		given(catalogVersionService.getCatalogVersion(any(), any())).willReturn(catalogVersion);

		given(productService.getProductForCode(eq(catalogVersion), eq(EXISTING_PROD_CODE))).willReturn(product);
		given(product.getCode()).willReturn(EXISTING_PROD_CODE);
		given(productService.getProductForCode(eq(catalogVersion), eq(REPLACEMENT_PROD_CODE))).willReturn(replacementProduct);
		given(replacementProduct.getCode()).willReturn(REPLACEMENT_PROD_CODE);

		given(productNotInFeed.getCode()).willReturn(NOT_IN_FEED__PROD_CODE);
		given(savedValuesStrategy.create(product, sapUser, null, null, false)).willReturn(Optional.empty());
	}
}
