package com.waters.hybris.integration.countryconfig.etl.transformer;

import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.waters.hybris.core.util.StreamUtil;
import com.waters.hybris.integration.core.etl.transformer.DefaultDataTransformer;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.daos.CountryDao;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WatersCountryConfigDataTransformer extends DefaultDataTransformer<Map<String, List<String>>, Map<BaseStoreModel, List<CountryModel>>>
{
	private static final Logger LOG = LoggerFactory.getLogger(WatersCountryConfigDataTransformer.class);
	private static final String NO_BASE_STORE_FOUND_FOR_CODE = "No base store found for code {}";

	private BaseStoreService baseStoreService;
	private CountryDao countryDao;
	private SystemArea systemArea;

	@Override
	public Map<BaseStoreModel, List<CountryModel>> transform(final Map<String, List<String>> jsonMap)
	{
		return StreamUtil.nullSafe(jsonMap.entrySet())
			.map(this::getBaseStore)
			.filter(Optional::isPresent)
			.map(Optional::get)
			.collect(Collectors.toMap(Function.identity(), key -> getCountryList(jsonMap.get(key.getUid()))));
	}

	private List<CountryModel> getCountryList(final List<String> countries)
	{
		return StreamUtil.nullSafe(countries)
			.map(this::getCountry)
			.filter(Optional::isPresent)
			.map(Optional::get)
			.collect(Collectors.toList());
	}

	private Optional<CountryModel> getCountry(final String countryIsoCode)
	{
		return StreamUtil.nullSafe(getCountryDao().findCountriesByCode(countryIsoCode)).findFirst();
	}

	private Optional<BaseStoreModel> getBaseStore(final Map.Entry<String, List<String>> entry)
	{
		try
		{
			return Optional.of(getBaseStoreService().getBaseStoreForUid(entry.getKey()));
		}
		catch (final UnknownIdentifierException e)
		{
			auditFailure(NO_BASE_STORE_FOUND_FOR_CODE, entry.getKey());
			logError(NO_BASE_STORE_FOUND_FOR_CODE, entry.getKey());
			return Optional.empty();
		}
	}

	protected void auditFailure(final String message, final String param)
	{
		getContext().getAuditService().audit(getSystemArea(), Status.FAILURE, message, param, getContext().getActivityDescription());
	}

	protected void logError(final String message, final String param)
	{
		LOG.error(message, param);
	}

	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	public CountryDao getCountryDao()
	{
		return countryDao;
	}

	@Required
	public void setCountryDao(final CountryDao countryDao)
	{
		this.countryDao = countryDao;
	}

	public SystemArea getSystemArea()
	{
		return systemArea;
	}

	@Required
	public void setSystemArea(final SystemArea systemArea)
	{
		this.systemArea = systemArea;
	}

}
