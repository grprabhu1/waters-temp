package com.waters.hybris.integration.countryconfig.etl.producer;

import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import com.neoworks.hybris.etl.producers.DataProducer;
import com.waters.hybris.integration.countryconfig.etl.strategy.CountryConfigExtractStrategy;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.Map;

public class WatersCountryConfigDataProducer implements DataProducer<Map<String, List<String>>>
{
	private EtlContext context;
	private SystemArea systemArea;
	private CountryConfigExtractStrategy countryConfigExtractStrategy;
	private static final String COUNTRYCONFIG_RETRIEVE = "retrieveCountryConfig";

	@Override
	public Map<String, List<String>> get()
	{
		if (Boolean.FALSE.equals(getContext().getParameters().get(COUNTRYCONFIG_RETRIEVE)))
		{
			return null;
		}
		final Map<String, List<String>> countryConfig = getCountryConfigExtractStrategy().getCountryConfig(systemArea, getContext());
		getContext().getAuditService().audit(getSystemArea(), Status.SUCCESS,
			"Country config data produced is {}", countryConfig, getContext().getActivityDescription());
		getContext().getParameters().put(COUNTRYCONFIG_RETRIEVE, Boolean.FALSE);
		return countryConfig;
	}


	@Override
	public void init(final EtlContext etlContext) throws EtlValidationException
	{
		this.context = etlContext;
	}

	public EtlContext getContext()
	{
		return context;
	}

	@Override
	public void start() throws EtlTransformationException
	{
		// empty
	}

	@Override
	public void end(final boolean b) throws EtlTransformationException
	{
		// empty
	}

	public SystemArea getSystemArea()
	{
		return systemArea;
	}

	@Required
	public void setSystemArea(final SystemArea systemArea)
	{
		this.systemArea = systemArea;
	}

	public CountryConfigExtractStrategy getCountryConfigExtractStrategy()
	{
		return countryConfigExtractStrategy;
	}

	@Required
	public void setCountryConfigExtractStrategy(final CountryConfigExtractStrategy countryConfigExtractStrategy)
	{
		this.countryConfigExtractStrategy = countryConfigExtractStrategy;
	}
}
