/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.integration.countryconfig.constants;

/**
 * Global class for all Watersintegrationcountryconfig constants. You can add global constants for your extension into this class.
 */
public final class WatersintegrationcountryconfigConstants extends GeneratedWatersintegrationcountryconfigConstants
{
	public static final String EXTENSIONNAME = "watersintegrationcountryconfig";

	private WatersintegrationcountryconfigConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

    public static final String PLATFORM_LOGO_CODE = "watersintegrationcountryconfigPlatformLogo";
}
