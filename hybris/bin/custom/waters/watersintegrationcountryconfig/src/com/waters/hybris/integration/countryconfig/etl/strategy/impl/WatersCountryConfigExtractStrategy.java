package com.waters.hybris.integration.countryconfig.etl.strategy.impl;

import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.feature.FeatureService;
import com.netflix.hystrix.HystrixCommand;
import com.waters.hybris.core.util.StreamUtil;
import com.waters.hybris.integration.core.hystrix.HystrixCommandExecutionResultLogger;
import com.waters.hybris.integration.core.hystrix.HystrixCommandPropertiesProvider;
import com.waters.hybris.integration.countryconfig.etl.producer.WatersCountryConfigDataProducer;
import com.waters.hybris.integration.countryconfig.etl.strategy.CountryConfigExtractStrategy;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WatersCountryConfigExtractStrategy implements CountryConfigExtractStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(WatersCountryConfigDataProducer.class);
	private static final String SSL = "SSL";
	private static final String COUNTRYCONFIG_WEBSERVICE_URL = "countryconfig.webservice.url";
	private static final String COUNTRYCONFIG_WEBSERVICE_MAX_ATTEMPTS = "countryconfig.webservice.maxattempts";
	private static final String COUNTRYCONFIG_USEHYSTRIX = "countryconfig.usehystrix";
	private static final String COUNTRYCONFIG_GET_DATA = "countryconfig.getData";
	private static final String COUNTRYCONFIG_METRIC_PREFIX_KEY = "countryConfigData.";
	private static final String COUNTRY_CONFIG_LOGGING_SERVICE = "countryConfigLoggingService";
	private static final String COUNTRY_CONFIG_ACTION = "country config";

	private HystrixCommandPropertiesProvider countryConfigCommandsPropertiesProvider;
	private FeatureService featureService;
	private HystrixCommandExecutionResultLogger hystrixCommandExecutionResultLogger;
	private ConfigurationService configurationService;

	@Override
	public Map<String, List<String>> getCountryConfig(final SystemArea systemArea, final EtlContext context)
	{
		return getFeatureService().isFeatureOn(COUNTRYCONFIG_USEHYSTRIX) ?
			getCountryConfigHystrix(systemArea, context) : getCountryConfigData(systemArea, context);
	}

	private Map<String, List<String>> getCountryConfigHystrix(final SystemArea systemArea, final EtlContext context)
	{
		final HystrixCommand<Map<String, List<String>>> command = new HystrixCommand<Map<String, List<String>>>(getCountryConfigCommandsPropertiesProvider()
			.createCommandProperties(COUNTRYCONFIG_GET_DATA))
		{
			@Override
			protected Map<String, List<String>> run() throws Exception
			{
				return getCountryConfigData(systemArea, context);
			}

			@Override
			protected Map<String, List<String>> getFallback()
			{
				context.getAuditService().audit(systemArea, Status.FAILURE, "Error in returning country config data from webservice",
					context.getActivityDescription());
				LOG.error("Error in returning country config data from webservice");
				return null;
			}
		};
		final Map<String, List<String>> countryConfigMap = command.execute();
		getHystrixCommandExecutionResultLogger().logExecutionResult(command, COUNTRY_CONFIG_ACTION, COUNTRYCONFIG_METRIC_PREFIX_KEY, COUNTRY_CONFIG_LOGGING_SERVICE);
		return countryConfigMap;
	}

	@SuppressFBWarnings(value = "REC_CATCH_EXCEPTION", justification = "We want to catch exceptions to so stop the export from stopping.")
	private Map<String, List<String>> getCountryConfigData(final SystemArea systemArea, final EtlContext context)
	{
		try
		{
			final RetryTemplate template = new RetryTemplate();
			template.setRetryPolicy(new SimpleRetryPolicy(getConfigurationService().getConfiguration().getInt(COUNTRYCONFIG_WEBSERVICE_MAX_ATTEMPTS)));
			return template.execute(retryContext -> getCountryConfigFromWebservice());
		}
		catch (final Exception e)
		{
			context.getAuditService().audit(systemArea, Status.FAILURE,
				"Error in returning country config data. Exception = {}", e, context.getActivityDescription());
			LOG.error("Error in getting country config data", e);
			return null;
		}
	}

	private Map<String, List<String>> getCountryConfigFromWebservice() throws NoSuchAlgorithmException, KeyManagementException, IOException
	{
		addAccessTrustManager();
		final URL contryConfigUrl = new URL(getConfigurationService().getConfiguration().getString(COUNTRYCONFIG_WEBSERVICE_URL));
		final JsonObject jsonObject = Json.createReader(contryConfigUrl.openStream()).readObject();
		return StreamUtil.nullSafe(jsonObject.keySet())
			.collect(Collectors.toMap(Function.identity(), key -> getCountriesFromWs(jsonObject, key)));
	}

	private List<String> getCountriesFromWs(final JsonObject jsonObject, final String key)
	{
		return jsonObject.get(key).asJsonArray().stream().map(jsonValue -> ((JsonString) jsonValue).getString()).collect(Collectors.toList());
	}

	private void addAccessTrustManager() throws NoSuchAlgorithmException, KeyManagementException
	{
		final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager()
		{
			@SuppressFBWarnings(justification = "Ignore the null findbugs warning")
			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers()
			{
				return null;
			}

			@Override
			public void checkClientTrusted(final java.security.cert.X509Certificate[] certs, final String authType)
			{
				// Do nothing
			}

			@Override
			public void checkServerTrusted(final java.security.cert.X509Certificate[] certs, final String authType)
			{
				// Do nothing
			}
		}};
		final SSLContext sslContext = SSLContext.getInstance(SSL);
		sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
	}

	public HystrixCommandPropertiesProvider getCountryConfigCommandsPropertiesProvider()
	{
		return countryConfigCommandsPropertiesProvider;
	}

	@Required
	public void setCountryConfigCommandsPropertiesProvider(final HystrixCommandPropertiesProvider countryConfigCommandsPropertiesProvider)
	{
		this.countryConfigCommandsPropertiesProvider = countryConfigCommandsPropertiesProvider;
	}

	public FeatureService getFeatureService()
	{
		return featureService;
	}

	@Required
	public void setFeatureService(final FeatureService featureService)
	{
		this.featureService = featureService;
	}

	public HystrixCommandExecutionResultLogger getHystrixCommandExecutionResultLogger()
	{
		return hystrixCommandExecutionResultLogger;
	}

	@Required
	public void setHystrixCommandExecutionResultLogger(final HystrixCommandExecutionResultLogger hystrixCommandExecutionResultLogger)
	{
		this.hystrixCommandExecutionResultLogger = hystrixCommandExecutionResultLogger;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
