package com.waters.hybris.integration.countryconfig.etl.consumer;

import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.etl.consumers.DataConsumer;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.waters.hybris.core.util.StreamUtil;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class WatersCountryConfigDataConsumer implements DataConsumer<Map<BaseStoreModel, List<CountryModel>>>
{
	private EtlContext context;
	private SystemArea systemArea = SystemArea.ETL;
	private ModelService modelService;
	private BaseStoreService baseStoreService;

	@Override
	public void put(final Map<BaseStoreModel, List<CountryModel>> countryConfigMap)
	{
		getModelService().saveAll(getNewCountryConfig(countryConfigMap));
		getContext().getAuditService().audit(getSystemArea(), Status.SUCCESS, "Delivery countries updated for {} basestores",
			countryConfigMap.keySet().size(), getContext().getActivityDescription());
	}

	@Override
	public void init(final EtlContext etlContext)
	{
		this.context = etlContext;
		final List<BaseStoreModel> oldCountryConfig = getOldCountryConfig();
		getModelService().saveAll(oldCountryConfig);
		getContext().getAuditService().audit(getSystemArea(), Status.SUCCESS, "Delivery countries removed for {} basestores",
			oldCountryConfig.size(), getContext().getActivityDescription());
	}

	private List<BaseStoreModel> getOldCountryConfig()
	{
		return StreamUtil.nullSafe(getBaseStoreService().getAllBaseStores())
			.map(baseStore -> getBaseStoreWithUpdatedCountries(baseStore, Collections.emptyList()))
			.collect(Collectors.toList());
	}

	private List<BaseStoreModel> getNewCountryConfig(final Map<BaseStoreModel, List<CountryModel>> countryModels)
	{
		return StreamUtil.nullSafe(countryModels.entrySet())
			.filter(Objects::nonNull)
			.map(entry -> getBaseStoreWithUpdatedCountries(entry.getKey(), entry.getValue()))
			.collect(Collectors.toList());
	}

	private BaseStoreModel getBaseStoreWithUpdatedCountries(final BaseStoreModel baseStoreModel, final List<CountryModel> countryModels)
	{
		baseStoreModel.setDeliveryCountries(countryModels);
		return baseStoreModel;
	}

	@Override
	public void start() throws EtlTransformationException
	{
		// empty
	}

	@Override
	public void end(final boolean b) throws EtlTransformationException
	{
		// empty
	}

	public EtlContext getContext()
	{
		return this.context;
	}

	public SystemArea getSystemArea()
	{
		return this.systemArea;
	}

	@Required
	public void setSystemArea(final SystemArea systemArea)
	{
		this.systemArea = systemArea;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

}
