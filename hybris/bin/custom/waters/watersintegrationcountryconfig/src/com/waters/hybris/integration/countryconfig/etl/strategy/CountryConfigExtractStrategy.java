package com.waters.hybris.integration.countryconfig.etl.strategy;

import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.etl.context.EtlContext;

import java.util.List;
import java.util.Map;

public interface CountryConfigExtractStrategy
{
	Map<String, List<String>> getCountryConfig(SystemArea systemArea, EtlContext context);
}
