@CountryConfigImport @Transaction
Feature: Country config import

	@AdminUser
	Scenario: Country config import
		Given that following basestores are present in the system
			| storeUid |
			| AS01     |
			| US01     |
		When the "watersCountryConfigJob" has been run
		And the job completed successfully
		Then the following delivery countries returned from the web service are set on base stores
			| storeUid | deliveryCountries                                                                                  |
			| AS01     | FJ,AU,VU,PG,NZ                                                                                     |
			| US01     | GD,EC,CR,CL,CO,CG,BW,BZ,BS,BO,BM,AR,AQ,VI,VE,UY,US,TT,SV,PY,PE,PA,NI,NF,LC,KE,JM,HT,HN,GT,GP,ZM,ZA |

