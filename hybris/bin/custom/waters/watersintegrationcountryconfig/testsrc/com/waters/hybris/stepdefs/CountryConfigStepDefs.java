package com.waters.hybris.stepdefs;

import com.waters.hybris.core.util.StreamUtil;
import com.waters.hybris.stepdefs.dto.BaseStoreDto;
import cucumber.api.java.en.Given;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.stream.Collectors;


public class CountryConfigStepDefs
{
	private static final String COMMA = ",";

	private BaseStoreService baseStoreService;

	@Given("^that following basestores are present in the system")
	public void that_following_basestores_are_present_in_the_system(final List<BaseStoreDto> baseStoreDtoList)
	{
		baseStoreDtoList.forEach(baseStore -> {
			Assert.assertNotNull(getBaseStoreService().getBaseStoreForUid(baseStore.getStoreUid()));
		});
	}

	@Given("^the following delivery countries returned from the web service are set on base stores$")
	public void the_following_delivery_countries_are_set_on_base_stores(final List<BaseStoreDto> baseStoreDtoList)
	{
		StreamUtil.nullSafe(baseStoreDtoList).forEach(basestore -> {
			final String deliveryCountries = StreamUtil.nullSafe(getBaseStoreService()
				.getBaseStoreForUid(basestore.getStoreUid()).getDeliveryCountries())
				.map(CountryModel::getIsocode)
				.collect(Collectors.joining(COMMA));
			Assert.assertEquals(basestore.getDeliveryCountries(), deliveryCountries);
		});
	}

	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}
}
