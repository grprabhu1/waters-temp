package com.waters.hybris.stepdefs.dto;

public class BaseStoreDto
{
	private String storeUid;
	private String deliveryCountries;

	public String getStoreUid()
	{
		return storeUid;
	}

	public void setStoreUid(final String storeUid)
	{
		this.storeUid = storeUid;
	}

	public String getDeliveryCountries()
	{
		return deliveryCountries;
	}

	public void setDeliveryCountries(final String deliveryCountries)
	{
		this.deliveryCountries = deliveryCountries;
	}
}
