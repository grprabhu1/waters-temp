package com.waters.hybris.integration.countryconfig.etl.transformer;

import com.waters.hybris.core.util.StreamUtil;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.daos.CountryDao;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WatersCountryConfigDataTransformerTest
{
	private static final String AS01 = "AS01";
	private static final String US01 = "US01";
	private static final String AU = "AU";
	private static final String NZ = "NZ";
	private static final String US = "US";
	private static final String AAA = "AAA";

	@Mock
	private BaseStoreService baseStoreService;

	@Mock
	private CountryDao countryDao;

	@Mock
	private CountryModel countryModelMockAU;

	@Mock
	private CountryModel countryModelMockNZ;

	@Mock
	private CountryModel countryModelMockUS;

	@Mock
	private BaseStoreModel baseStoreModelAS01;

	@Mock
	private BaseStoreModel baseStoreModelUS01;

	@InjectMocks
	@Spy
	private WatersCountryConfigDataTransformer testObj;

	private final Map<String, List<String>> jsonMap = new HashMap<>();

	private final Map<String, List<String>> jsonInvalidMap = new HashMap<>();

	@Before
	public void setUp()
	{
		jsonMap.put(AS01, Arrays.asList(AU, NZ));
		jsonMap.put(US01, Arrays.asList(US));

		jsonInvalidMap.put(AS01, Arrays.asList(AU, NZ));
		jsonInvalidMap.put(AAA, Arrays.asList(AAA));

		when(countryDao.findCountriesByCode(AU)).thenReturn(Arrays.asList(countryModelMockAU));
		when(countryDao.findCountriesByCode(NZ)).thenReturn(Arrays.asList(countryModelMockNZ));
		when(countryDao.findCountriesByCode(US)).thenReturn(Arrays.asList(countryModelMockUS));
		when(countryDao.findCountriesByCode(AAA)).thenReturn(Collections.emptyList());

		when(countryModelMockAU.getIsocode()).thenReturn(AU);
		when(countryModelMockNZ.getIsocode()).thenReturn(NZ);
		when(countryModelMockUS.getIsocode()).thenReturn(US);

		when(baseStoreService.getBaseStoreForUid(AS01)).thenReturn(baseStoreModelAS01);
		when(baseStoreService.getBaseStoreForUid(US01)).thenReturn(baseStoreModelUS01);
		when(baseStoreService.getBaseStoreForUid(AAA)).thenThrow(new UnknownIdentifierException("Unknown Country"));

		when(baseStoreModelAS01.getUid()).thenReturn(AS01);
		when(baseStoreModelUS01.getUid()).thenReturn(US01);

		doNothing().when(testObj).auditFailure(Mockito.anyString(), Mockito.anyString());
		doNothing().when(testObj).logError(Mockito.anyString(), Mockito.anyString());
	}

	@Test
	public void testTransformWithValidData()
	{
		final Map<BaseStoreModel, List<CountryModel>> transformedResult = testObj.transform(jsonMap);
		assertEquals(transformedResult.size(), 2);
		final Optional<BaseStoreModel> baseStoreAS01 = getBaseStoreForUid(transformedResult, AS01);
		final Optional<BaseStoreModel> baseStoreUS01 = getBaseStoreForUid(transformedResult, US01);
		assertTrue(baseStoreAS01.isPresent());
		assertTrue(baseStoreUS01.isPresent());
		assertEquals(baseStoreAS01.get().getUid(), AS01);
		assertEquals(baseStoreUS01.get().getUid(), US01);
		assertEquals(transformedResult.get(baseStoreModelAS01).size(), 2);
		assertEquals(transformedResult.get(baseStoreModelAS01).get(0).getIsocode(), AU);
		assertEquals(transformedResult.get(baseStoreModelAS01).get(1).getIsocode(), NZ);
		assertEquals(transformedResult.get(baseStoreModelUS01).size(), 1);
		assertEquals(transformedResult.get(baseStoreModelUS01).get(0).getIsocode(), US);
	}

	@Test
	public void testTransformWithInValidData()
	{
		final Map<BaseStoreModel, List<CountryModel>> transformedResult = testObj.transform(jsonInvalidMap);
		assertEquals(transformedResult.size(), 1);
		final Optional<BaseStoreModel> baseStoreAS01 = getBaseStoreForUid(transformedResult, AS01);
		assertTrue(baseStoreAS01.isPresent());
		assertEquals(baseStoreAS01.get().getUid(), AS01);
		assertEquals(transformedResult.get(baseStoreAS01.get()).size(), 2);
		assertEquals(transformedResult.get(baseStoreAS01.get()).get(0).getIsocode(), AU);
		assertEquals(transformedResult.get(baseStoreAS01.get()).get(1).getIsocode(), NZ);
	}

	private Optional<BaseStoreModel> getBaseStoreForUid(final Map<BaseStoreModel, List<CountryModel>> transformedResult, final String uid)
	{
		return StreamUtil.nullSafe(transformedResult.keySet()).filter(baseStoreModel -> uid.equals(baseStoreModel.getUid())).findAny();
	}
}
