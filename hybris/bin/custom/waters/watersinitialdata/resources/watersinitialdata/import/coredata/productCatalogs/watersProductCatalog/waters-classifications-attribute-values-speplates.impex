$productCatalog=watersProductCatalog;;;
$catalogVersion=catalogVersion(catalog(id[default=$productCatalog]),version[default='Staged'])[unique=true,default=$productCatalog:Staged];;;
$classCatalogVersion=catalogVersion(catalog(id[default='WatersClassification']),version[default='1.0'])[unique=true,default='WatersClassification:1.0'];;;
$classSystemVersion=systemVersion(catalog(id[default='WatersClassification']),version[default='1.0'])[unique=true];;;
$class=classificationClass(code,$classCatalogVersion)[unique=true];;;
$attribute=classificationAttribute(code,$classSystemVersion)[unique=true];;;
$unit=unit(code,$classSystemVersion);;;
$lang=en;;;

# Insert Classification Attributes;;;;
INSERT_UPDATE ClassificationAttribute;$classSystemVersion;code[unique=true];name[lang=$lang];publicWebLabel[lang=$lang];
;;SPEPlatesNumberofWells;SPE Plates Number of Wells;Number of Wells;

INSERT_UPDATE ClassificationAttributeUnit;$classSystemVersion;code[unique=true];symbol;unitType;name[lang=en];
;;SPECarbonLoad_;%;SPECarbonLoadUOM;SPE Carbon Load;
;;SPEParticleSizeµm;µm;SPEParticleSizeUOM;SPE Particle Size;
;;SPESorbentWeightmg;mg;SPESorbentWeightUOM;SPE Sorbent Weight;
;;SPEPoreSizeÅ;Å;SPEPoreSizeUOM;SPE Pore Size;
;;SPEpHRangeMaxpH;pH;SPEpHRangeMaxUOM;SPE pH Range Max;
;;SPEIonExchangeCapacityµeq_gram;µeq/gram;SPEIonExchangeCapacityUOM;SPE Ion Exchange Capacity;
;;SPEIonExchangeCapacitymeq_gram;meq/gram;SPEIonExchangeCapacityUOM;SPE Ion Exchange Capacity;

INSERT_UPDATE ClassificationAttributeValue;$classSystemVersion[unique=true];code[unique=true]; name[lang=$lang];
# AttributeConfig values for SPE Carbon Load
;;SPECarbonLoad.6;6;
;;SPECarbonLoad.17;17;
;;SPECarbonLoad.12;12;

# AttributeConfig values for SPE Chemistry
;;SPEChemistry.HLB;HLB;
;;SPEChemistry.Method_Development;Method Development;
;;SPEChemistry.TC18;TC18;
;;SPEChemistry.PRiME_HLB;PRiME HLB;
;;SPEChemistry.C18;C18;
;;SPEChemistry.MCX;MCX;
;;SPEChemistry.MAX;MAX;
;;SPEChemistry.Accell_Plus_QMA;Accell Plus QMA;
;;SPEChemistry.WCX;WCX;
;;SPEChemistry.WAX;WAX;

# AttributeConfig values for SPE Endcap
;;SPEEndcap.Yes;Yes;
;;SPEEndcap.No;No;

# AttributeConfig values for SPE Format
;;SPEFormat.96-well_Plate;96-well Plate;
;;SPEFormat.Sorbent_Selection_Plate;Sorbent Selection Plate;
;;SPEFormat.96-well_µElution_Plate;96-well µElution Plate;

# AttributeConfig values for SPE Ion Exchange Capacity
;;SPEIonExchangeCapacity.0.2;0.2;
;;SPEIonExchangeCapacity.0.6;0.6;
;;SPEIonExchangeCapacity.230;230;
;;SPEIonExchangeCapacity.1;1;
;;SPEIonExchangeCapacity.0.75;0.75;

# AttributeConfig values for SPE Mass Spec Compatibility
;;SPEMassSpecCompatibility.Yes;Yes;

# AttributeConfig values for SPE Material
;;SPEMaterial.polypropylene;polypropylene;

# AttributeConfig values for SPE Mode
;;SPEMode.Mixed_mode;Mixed mode;
;;SPEMode.Reversed-phase;Reversed-phase;
;;SPEMode.Precipitation;Precipitation;
;;SPEMode.Ion-exchange;Ion-exchange;

# AttributeConfig values for SPE Particle Size
;;SPEParticleSize.37-55;37-55;
;;SPEParticleSize.30;30;
;;SPEParticleSize.60;60;
;;SPEParticleSize.55-105;55-105;

# AttributeConfig values for SPE Plates Number of Wells
;;SPEPlatesNumberofWells.96;96;

# AttributeConfig values for SPE Pore Size
;;SPEPoreSize.80;80;
;;SPEPoreSize.300;300;
;;SPEPoreSize.125;125;

# AttributeConfig values for SPE Sorbent Substrate
;;SPESorbentSubstrate.Silica;Silica;
;;SPESorbentSubstrate.Copolymer;Copolymer;

# AttributeConfig values for SPE Sorbent Weight
;;SPESorbentWeight.5;5;
;;SPESorbentWeight.2;2;
;;SPESorbentWeight.3;3;
;;SPESorbentWeight.100;100;
;;SPESorbentWeight.10;10;
;;SPESorbentWeight.30;30;
;;SPESorbentWeight.60;60;
;;SPESorbentWeight.25;25;
;;SPESorbentWeight.40;40;

# AttributeConfig values for SPE Water Wettable
;;SPEWaterWettable.No;No;
;;SPEWaterWettable.Yes;Yes;

# AttributeConfig values for SPE pH Range Max
;;SPEpHRangeMax.14;14;
;;SPEpHRangeMax.10;10;
;;SPEpHRangeMax.8;8;

# AttributeConfig values for SPE pH Range Min
;;SPEpHRangeMin.0;0;
;;SPEpHRangeMin.2;2;


INSERT_UPDATE ClassAttributeAssignment;$class;$attribute;position;attributeType(code[default=string]);localized[default=false];multiValued[default=false];searchable[default=true];mandatory[default=false];attributeValues(code,$classSystemVersion);internalOnly[default=false];$unit;readOnly[default=false];facet[default=false]
;SPEPlates;SPEPlatesNumberofWells;1810;enum;false;false;true;false;SPEPlatesNumberofWells.96;false;;false;true


UPDATE ClassAttributeAssignment;$class;$attribute;attributeValues(code,$classSystemVersion)[mode=append];
;SPE;SPEMassSpecCompatibility;SPEMassSpecCompatibility.Yes
;SPE;SPEFormat;SPEFormat.96-well_Plate,SPEFormat.Sorbent_Selection_Plate,SPEFormat.96-well_µElution_Plate
;SPE;SPESorbentSubstrate;SPESorbentSubstrate.Silica,SPESorbentSubstrate.Copolymer
;SPE;SPEMode;SPEMode.Mixed_mode,SPEMode.Reversed-phase,SPEMode.Precipitation,SPEMode.Ion-exchange
;SPE;SPEMaterial;SPEMaterial.polypropylene
;SPE;SPEEndcap;SPEEndcap.Yes,SPEEndcap.No
;SPE;SPEChemistry;SPEChemistry.HLB,SPEChemistry.Method_Development,SPEChemistry.TC18,SPEChemistry.PRiME_HLB,SPEChemistry.C18,SPEChemistry.MCX,SPEChemistry.MAX,SPEChemistry.Accell_Plus_QMA,SPEChemistry.WCX,SPEChemistry.WAX
;SPE;SPEpHRangeMin;SPEpHRangeMin.0,SPEpHRangeMin.2
;SPE;SPEWaterWettable;SPEWaterWettable.No,SPEWaterWettable.Yes
;SPE;SPECarbonLoad;SPECarbonLoad.6,SPECarbonLoad.17,SPECarbonLoad.12
;SPE;SPEParticleSize;SPEParticleSize.37-55,SPEParticleSize.30,SPEParticleSize.60,SPEParticleSize.55-105
;SPE;SPESorbentWeight;SPESorbentWeight.5,SPESorbentWeight.2,SPESorbentWeight.3,SPESorbentWeight.100,SPESorbentWeight.10,SPESorbentWeight.30,SPESorbentWeight.60,SPESorbentWeight.25,SPESorbentWeight.40
;SPE;SPEPoreSize;SPEPoreSize.80,SPEPoreSize.300,SPEPoreSize.125
;SPE;SPEpHRangeMax;SPEpHRangeMax.14,SPEpHRangeMax.10,SPEpHRangeMax.8
;SPE;SPEIonExchangeCapacity;SPEIonExchangeCapacity.0.2,SPEIonExchangeCapacity.0.6,SPEIonExchangeCapacity.230,SPEIonExchangeCapacity.1,SPEIonExchangeCapacity.0.75
