package com.waters.hybris.initialdata.services.dataimport;

public interface ImpexConfigurationLoader {
    ImpexConfiguration loadConfiguration(String path);
}
