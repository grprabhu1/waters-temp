package com.waters.hybris.initialdata.services.dataimport;

import java.util.List;

public class FileGroup {
    private String version;
    private String name;
    private List<String> files;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public List<String> getFiles() {
        return files;
    }

    public void setFiles(final List<String> files) {
        this.files = files;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }
}
