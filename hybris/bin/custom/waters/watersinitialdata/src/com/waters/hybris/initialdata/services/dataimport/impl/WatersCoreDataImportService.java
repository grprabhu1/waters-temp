package com.waters.hybris.initialdata.services.dataimport.impl;

import com.waters.hybris.initialdata.constants.WatersInitialDataConstants;
import com.waters.hybris.initialdata.services.dataimport.ImpexConfiguration;
import com.waters.hybris.initialdata.services.dataimport.ImpexConfigurationLoader;
import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.core.initialization.SystemSetupContext;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.List;

import static com.waters.hybris.initialdata.constants.WatersInitialDataConstants.IMPORT_WATERS_SYNCH_PRODUCTS;
import static org.slf4j.LoggerFactory.getLogger;

public class WatersCoreDataImportService extends CoreDataImportService
{
	private static final Logger LOG = getLogger(WatersCoreDataImportService.class);

	private ImpexConfigurationLoader impexConfigurationLoader;

	@Override
	public void execute(final AbstractSystemSetup systemSetup, final SystemSetupContext context, final List<ImportData> importData)
	{
		super.execute(systemSetup, context, importData);

		final boolean importWatersData = systemSetup.getBooleanSystemSetupParameter(context, WatersInitialDataConstants.IMPORT_WATERS_PRODUCT_PROPERTY);
		if (importWatersData)
		{
			this.importWatersProductData(systemSetup, context, importData);
		}
	}

	@Override
	protected void importCommonData(final String extensionName)
	{
		super.importCommonData(extensionName);
		this.getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/user-groups.impex", extensionName),
			true);
		this.getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/user-groups-rights.impex", extensionName),
			true);
		this.getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/backoffice-roles.impex", extensionName),
			true);
		this.getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/b2b-user-groups.impex", extensionName),
			true);
		this.getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/b2b-user-groups-rights.impex", extensionName),
			true);
		this.getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/password-policy-brute-force-attempts.impex", extensionName),
			true);
		this.getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/occ.impex", extensionName),
			true);
		this.getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/base-stores.impex", extensionName),
			true);
		this.getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/employees.impex", extensionName),
			true);
	}


	/**
	 * Imports store data related to Waters. Imports a site-override impex if available.
	 *
	 * @param extensionName      the extension name used.
	 * @param storeName          the store to import for.
	 * @param productCatalogName the product catalog used.
	 */
	@Override
	protected void importStore(final String extensionName, final String storeName, final String productCatalogName)
	{
		super.importStore(extensionName, storeName, productCatalogName);

		if (this.getConfigurationService().getConfiguration().getBoolean("setup.siteoverride", false))
		{
			this.getSetupImpexService()
				.importImpexFile(String.format("/%s/import/coredata/stores/%s/site-override.impex", extensionName, storeName), false);
		}
	}

	@Override
	protected void importProductCatalog(final String extensionName, final String productCatalogName)
	{
		super.importProductCatalog(extensionName, productCatalogName);


	}

	private void importWatersClassification(final String extensionName, final String productCatalogName)
	{
		final List<String> watersClassificationFiles = new ArrayList<>();

		// Add file containing classification categories and attribute names
		watersClassificationFiles.add("waters-classifications-categories-attributes.impex");

		// Add file containing common fields and values
		watersClassificationFiles.add("waters-classifications-units.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-consumablesAndSpares.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-product.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-spe.impex");
		watersClassificationFiles.add("waters-classifications-categories-attributes-mapping.impex");

		// Add file containing specific classification attributes
		watersClassificationFiles.add("waters-classifications-attribute-values-applicationkits.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-bulk.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-column.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-containers.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-filters.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-labequipment.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-plates.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-primers.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-quechers.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-smallcomponents.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-software.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-specartridges.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-speplates.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-standards.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-subassemblies.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-vialandplateaccessories.impex");
		watersClassificationFiles.add("waters-classifications-attribute-values-vials.impex");

		//update for the unit displayOrder
		watersClassificationFiles.add("waters-classifications-units-updateOrder.impex");

		// Do import of all waters classification files as added
		watersClassificationFiles.forEach(classificationFile -> {
			this.getSetupImpexService().importImpexFile(String.format(
				"/%s/import/coredata/productCatalogs/%sProductCatalog/" + classificationFile,
				extensionName, productCatalogName), false);
		});

	}

	protected void importWatersProductData(final AbstractSystemSetup systemSetup, final SystemSetupContext context, final List<ImportData> importData)
	{
		for (final ImportData data : importData)
		{
			// create categories
			this.getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/productCatalogs/%sProductCatalog/categories.impex",
					context.getExtensionName(), data.getProductCatalogName()), false);

			// create classification system
			importWatersClassification(context.getExtensionName(), data.getProductCatalogName());

			final ImpexConfiguration configuration = this.getImpexConfigurationLoader().loadConfiguration(String.format("/%s/import/coredata/waters/waters-configuration.json", context.getExtensionName()));
			if (configuration != null)
			{
				configuration.getGroups()
					.forEach(fileGroup -> {
						LOG.info("importing file group {}", fileGroup.getName());
						fileGroup.getFiles().forEach(file -> {
							systemSetup.logInfo(context, String.format("Importing content file [%s]", file));
							final String impexFile = String.format("/%s/import/coredata/waters/%sProductCatalog/" + file, context.getExtensionName(), data.getProductCatalogName());
							this.getSetupImpexService().importImpexFile(impexFile, false);
						});
					});
			}

			if (getConfigurationService().getConfiguration().getBoolean(IMPORT_WATERS_SYNCH_PRODUCTS, false))
			{
				this.synchronizeProductCatalog(systemSetup, context, data.getProductCatalogName(), true);
			}
		}
	}

	public ImpexConfigurationLoader getImpexConfigurationLoader()
	{
		return this.impexConfigurationLoader;
	}

	@Required
	public void setImpexConfigurationLoader(final ImpexConfigurationLoader impexConfigurationLoader)
	{
		this.impexConfigurationLoader = impexConfigurationLoader;
	}



}
