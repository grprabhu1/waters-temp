/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.initialdata.setup;

import com.waters.hybris.initialdata.constants.WatersInitialDataConstants;
import com.waters.hybris.initialdata.services.dataimport.impl.WatersCoreDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * This class provides hooks into the system's initialization and update processes.
 */
@SystemSetup(extension = WatersInitialDataConstants.EXTENSIONNAME)
public class InitialDataSystemSetup extends AbstractSystemSetup {
	public static final String WATERS = "waters";
	private static final Logger LOG = Logger.getLogger(InitialDataSystemSetup.class);

	private static final String IMPORT_CORE_DATA = "importCoreData";
	private static final String ACTIVATE_SOLR_CRON_JOBS = "activateSolrCronJobs";

	private WatersCoreDataImportService watersCoreDataImportService;
	private ConfigurationService configurationService;

	/**
	 * Generates the Dropdown and Multi-select boxes for the project data import
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions() {
		final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();

		params.add(this.createBooleanSystemSetupParameter(InitialDataSystemSetup.IMPORT_CORE_DATA, "Import Core Data", true));
		params.add(this.createBooleanSystemSetupParameter(InitialDataSystemSetup.ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", false));
		params.add(this.createBooleanSystemSetupParameter(WatersInitialDataConstants.IMPORT_WATERS_PRODUCT_PROPERTY, "Import Waters Product Data", false));

		// Add more Parameters here as you require

		return params;
	}

	/**
	 * Implement this method to create initial objects. This method will be called by system creator during
	 * initialization and system update. Be sure that this method can be called repeatedly.
	 *
	 * @param context the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
	public void createEssentialData(final SystemSetupContext context) {
		final List<ImportData> importData = new ArrayList<ImportData>();

		final ImportData watersImportData = new ImportData();
		watersImportData.setProductCatalogName(InitialDataSystemSetup.WATERS);
		watersImportData.setContentCatalogNames(Arrays.asList(InitialDataSystemSetup.WATERS));
		watersImportData.setStoreNames(Arrays.asList(InitialDataSystemSetup.WATERS));
		importData.add(watersImportData);

		this.getWatersCoreDataImportService().execute(this, context, importData);
		this.getEventService().publishEvent(new CoreDataImportedEvent(context, importData));
	}

	/**
	 * Implement this method to create data that is used in your project. This method will be called during the system
	 * initialization. <br>
	 * Add import data for each site you have configured
	 *
	 * <pre>
	 * final List<ImportData> importData = new ArrayList<ImportData>();
	 *
	 * final ImportData sampleImportData = new ImportData();
	 * sampleImportData.setProductCatalogName(SAMPLE_PRODUCT_CATALOG_NAME);
	 * sampleImportData.setContentCatalogNames(Arrays.asList(SAMPLE_CONTENT_CATALOG_NAME));
	 * sampleImportData.setStoreNames(Arrays.asList(SAMPLE_STORE_NAME));
	 * importData.add(sampleImportData);
	 *
	 * getCoreDataImportService().execute(this, context, importData);
	 * getEventService().publishEvent(new CoreDataImportedEvent(context, importData));
	 *
	 * getSampleDataImportService().execute(this, context, importData);
	 * getEventService().publishEvent(new SampleDataImportedEvent(context, importData));
	 * </pre>
	 *
	 * @param context the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
	}

	public WatersCoreDataImportService getWatersCoreDataImportService() {
		return watersCoreDataImportService;
	}

	@Required
	public void setWatersCoreDataImportService(final WatersCoreDataImportService watersCoreDataImportService) {
		this.watersCoreDataImportService = watersCoreDataImportService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	public ConfigurationService getConfigurationService() {
		return configurationService;
	}

}
