package com.waters.hybris.initialdata.services.dataimport.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.waters.hybris.initialdata.services.dataimport.ImpexConfiguration;
import com.waters.hybris.initialdata.services.dataimport.ImpexConfigurationLoader;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.io.IOException;
import java.io.InputStream;


public class DefaultImpexConfigurationLoader implements ImpexConfigurationLoader {
    
    protected static final ObjectMapper JSON_MAPPER = new ObjectMapper();

    @Override
    public ImpexConfiguration loadConfiguration(final String path) {
        try
        {
            return JSON_MAPPER.readValue(getResourceAsStream(path), ImpexConfiguration.class);
        }
        catch (final IOException e)
        {
            throw new ConversionException("Error converting the Json string to ImpexConfiguration DTO.", e);
        }
    }


    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
            value="UI_INHERITANCE_UNSAFE_GETRESOURCE",
            justification="I know what I'm doing")
    protected InputStream getResourceAsStream(final String resource ) {
        final InputStream in = getClass().getResourceAsStream( resource );

        return in == null ? getClass().getResourceAsStream( resource ) : in;
    }
    

}
