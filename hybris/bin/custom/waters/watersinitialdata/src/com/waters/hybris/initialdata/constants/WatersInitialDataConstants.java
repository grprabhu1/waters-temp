/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.initialdata.constants;

/**
 * Global class for all WatersInitialData constants.
 */
public final class WatersInitialDataConstants extends GeneratedWatersInitialDataConstants {
	public static final String EXTENSIONNAME = "watersinitialdata";

	public static final String IMPORT_WATERS_PRODUCT_PROPERTY = "watersinitialdata.initialization.importWatersProduct";
	public static final String IMPORT_WATERS_SYNCH_PRODUCTS = "watersinitialdata.initialization.synchWatersProduct";

	private WatersInitialDataConstants() {
		//empty
	}
}
