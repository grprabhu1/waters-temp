package com.waters.hybris.initialdata.services.dataimport;

import java.util.List;

public class ImpexConfiguration {
    private List<FileGroup> groups;

    public List<FileGroup> getGroups() {
        return groups;
    }

    public void setGroups(final List<FileGroup> groups) {
        this.groups = groups;
    }
}
