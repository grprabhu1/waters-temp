package com.waters.hybris.integration.core.etl.strategy.impl;

import com.neoworks.hybris.etl.context.EtlContext;
import com.waters.hybris.integration.core.etl.strategy.EtlNamingStrategy;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class EtlNamingTimeBasedStrategy implements EtlNamingStrategy
{
	private String fileNamePattern;
	private String timeStampParamKey;
	private String fileNamePatternParamKey;

	@Override
	public String generateName(final EtlContext context)
	{
		final ZonedDateTime timeStamp = (ZonedDateTime) context.getParameters().getOrDefault(getTimeStampParamKey(), ZonedDateTime.now(ZoneOffset.UTC));

		final String pattern = (String) context.getParameters().getOrDefault(getFileNamePatternParamKey(), getFileNamePattern());

		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		return formatter.format(timeStamp);
	}

	protected String getFileNamePattern()
	{
		return fileNamePattern;
	}

	public void setFileNamePattern(final String fileNamePattern)
	{
		this.fileNamePattern = fileNamePattern;
	}

	protected String getTimeStampParamKey()
	{
		return timeStampParamKey;
	}

	public void setTimeStampParamKey(final String timeStampParamKey)
	{
		this.timeStampParamKey = timeStampParamKey;
	}

	protected String getFileNamePatternParamKey()
	{
		return fileNamePatternParamKey;
	}

	public void setFileNamePatternParamKey(final String fileNamePatternParamKey)
	{
		this.fileNamePatternParamKey = fileNamePatternParamKey;
	}
}
