package com.waters.hybris.integration.core.file.filter;

import com.neoworks.hybris.feature.FeatureService;
import org.springframework.beans.factory.annotation.Required;

import java.io.File;

public class FeatureSwitchFileHashFilter extends FileHashFilter
{
	private FeatureService featureService;
	private String featureFlag;

	/**
	 * When we have the given feature flag enabled then we should always by-pass the filter
	 *
	 * @param file
	 * @return
	 */
	@Override
	public boolean accept(final File file)
	{
		return getFeatureService().isFeatureOn(getFeatureFlag()) || super.accept(file);
	}

	protected FeatureService getFeatureService()
	{
		return featureService;
	}

	@Required
	public void setFeatureService(final FeatureService featureService)
	{
		this.featureService = featureService;
	}

	protected String getFeatureFlag()
	{
		return featureFlag;
	}

	@Required
	public void setFeatureFlag(final String featureFlag)
	{
		this.featureFlag = featureFlag;
	}
}
