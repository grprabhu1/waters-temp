package com.waters.hybris.integration.core.etl.action;

import com.neoworks.hybris.etl.action.EtlAction;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.waters.hybris.integration.core.helper.session.SessionBaseSiteBaseStoreHelper;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.store.BaseStoreModel;
import org.springframework.beans.factory.annotation.Required;

public class EtlSessionConfigAction implements EtlAction
{
	private SessionBaseSiteBaseStoreHelper sessionBaseSiteBaseStoreHelper;
	private String baseSiteParamKey;
	private String baseStoreParamKey;

	@Override
	public void execute(final EtlContext context) throws EtlTransformationException
	{
		final Object baseSite = context.getParameters().get(getBaseSiteParamKey());

		if (baseSite instanceof BaseSiteModel)
		{
			getSessionBaseSiteBaseStoreHelper().setBaseSite((BaseSiteModel) baseSite);
		}
		else
		{
			throw new EtlTransformationException("Invalid base site");
		}

		final Object baseStore = context.getParameters().get(getBaseStoreParamKey());
		if (baseStore instanceof BaseStoreModel)
		{
			getSessionBaseSiteBaseStoreHelper().setUserPriceGroup((BaseStoreModel) baseStore);
		}
	}


	protected SessionBaseSiteBaseStoreHelper getSessionBaseSiteBaseStoreHelper()
	{
		return sessionBaseSiteBaseStoreHelper;
	}

	@Required
	public void setSessionBaseSiteBaseStoreHelper(final SessionBaseSiteBaseStoreHelper sessionBaseSiteBaseStoreHelper)
	{
		this.sessionBaseSiteBaseStoreHelper = sessionBaseSiteBaseStoreHelper;
	}

	protected String getBaseStoreParamKey()
	{
		return baseStoreParamKey;
	}

	@Required
	public void setBaseStoreParamKey(final String baseStoreParamKey)
	{
		this.baseStoreParamKey = baseStoreParamKey;
	}

	public String getBaseSiteParamKey()
	{
		return baseSiteParamKey;
	}

	@Required
	public void setBaseSiteParamKey(final String baseSiteParamKey)
	{
		this.baseSiteParamKey = baseSiteParamKey;
	}

}
