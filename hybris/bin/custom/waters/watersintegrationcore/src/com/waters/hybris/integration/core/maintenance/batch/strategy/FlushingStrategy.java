package com.waters.hybris.integration.core.maintenance.batch.strategy;

import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;

public interface FlushingStrategy
{
	int flush(Collection<Object> batch, boolean detachAfterCommit, ModelService modelService);
}
