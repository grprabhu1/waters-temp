package com.waters.hybris.integration.core.context;

import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.util.BatchCommitter;
import com.neoworks.hybris.util.ListBackedBatchCommitter;
import com.waters.hybris.integration.core.model.record.FileImportRecordModel;
import de.hybris.platform.core.model.media.MediaModel;

public class MediaImportContextBuilder extends AbstractImportContextBuilder
{
	public MediaImportContext createMediaImportContext(final FileImportRecordModel importRecord)
	{
		final MediaModel media = importRecord.getMedia();
		final AuditContext auditContext = getAuditService().beginAudit(getSystemArea(), null, "Importing media with code [{}]", media.getCode());
		BatchCommitter batchCommitter = null;
		if (isUseBatchCommitter())
		{
			batchCommitter = new ListBackedBatchCommitter(getBatchSize(), getModelServiceSelectionStrategy().selectModelService(getUseTransactionalModelServiceConfigKey()), true);
		}

		final MediaImportContext context = createContext(importRecord, auditContext, batchCommitter);
		initializeModelService(context);
		return context;
	}

	protected MediaImportContext createContext(final FileImportRecordModel importRecord, final AuditContext auditContext, final BatchCommitter batchCommitter)
	{
		return new MediaImportContext(importRecord, auditContext, batchCommitter);
	}
}
