/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.integration.core.constants;

/**
 * Global class for all Watersintegrationcore constants. You can add global constants for your extension into this class.
 */
public final class WatersintegrationcoreConstants extends GeneratedWatersintegrationcoreConstants
{
	public static final String EXTENSIONNAME = "watersintegrationcore";

	public static final String CATALOG_NAME = "watersProductCatalog";
	public static final String CATALOG_VERSION_NAME = "Staged";
	public static final String CLASSIFICATION_SYSTEM = "WatersClassification";
	public static final String CLASSIFICATION_SYSTEM_VERSION = "1.0";
	public static final String SAP_USER_UID = "SAPUser";

	// implement here constants used by this extension

	private WatersintegrationcoreConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
