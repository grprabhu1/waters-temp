package com.waters.hybris.integration.core.etl.consumer;

import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class SingleXmlStreamDataConsumer<T, K> extends AbstractXmlStreamDataConsumer<T, K>
{
	private static final Logger LOG = LoggerFactory.getLogger(SingleXmlStreamDataConsumer.class);

	private XMLStreamWriter xmlStreamWriter;
	private FileOutputStream fileOutputStream;
	private File file;
	private boolean atLeastOneItem = false;
	private boolean writeBom = false;

	private String dtd;
	private String namespaceURI;


	@Override
	public void init(final EtlContext etlContext) throws EtlValidationException
	{
		super.init(etlContext);
		try
		{
			file = File.createTempFile(getTemporaryFilePrefix(), getTemporaryFileSuffix());
			fileOutputStream = FileUtils.openOutputStream(file, false);

			if (isWriteBom())
			{
				LOG.debug("Writing BOM to file");
				final byte[] bom = new byte[]{(byte) 0xEF, (byte) 0xBB, (byte) 0xBF};
				fileOutputStream.write(bom);
			}

			xmlStreamWriter = createStreamWriter(fileOutputStream);
		}
		catch (final IOException | XMLStreamException e)
		{
			closeStreams();
			throw new EtlValidationException("Failed to initialize the consumer.", e);
		}
	}

	@Override
	public void start() throws EtlTransformationException
	{
		try
		{
			xmlStreamWriter.writeStartDocument();
			if (StringUtils.isNotBlank(getDtd()))
			{
				xmlStreamWriter.writeDTD(getDtd());
			}
			xmlStreamWriter.writeStartElement(getRootElementName());

			if (StringUtils.isNotBlank(getNamespaceURI()))
			{
				xmlStreamWriter.writeDefaultNamespace(getNamespaceURI());
			}
		}
		catch (final XMLStreamException e)
		{
			closeStreams();
			throw new EtlTransformationException("Failed to start the consumer.", e);
		}
	}

	@Override
	public void put(final T item)
	{
		Assert.notNull(item, "item cannot be null");
		try
		{
			getMarshaller().marshal(item, xmlStreamWriter);

			// TODO check if flush is required with large sets of orders or if the streamwriter handles this smoothly
			// xmlStreamWriter.flush();

			atLeastOneItem = true;
			getConsumerTrackingStrategy().markAsConsumed(getEtlContext());
		}
		catch (final JAXBException e)
		{
			LOG.error("Failed to marshal item.", e);
			markAsError("Failed to marshal item.", e);
		}
		catch (final EtlTransformationException e)
		{
			LOG.error("Failed to mark item as consumed.", e);
			markAsError("Failed to mark item as consumed.", e);
		}
	}

	@Override
	public void end(final boolean success) throws EtlTransformationException
	{
		try
		{
			xmlStreamWriter.writeEndDocument();
			xmlStreamWriter.close();
			fileOutputStream.close();

			if (atLeastOneItem)
			{
				getEtlContext().getParameters().put(getFileParamKey(), file.getAbsolutePath());
				getMediaActionList().execute(getEtlContext());
				getConsumerTrackingStrategy().markAllConsumedAsComplete(getEtlContext());
			}
		}
		catch (final IOException | XMLStreamException e)
		{
			closeStreams();
			markAllConsumedAsError("Failed to end the consumer.", e);
			throw new EtlTransformationException("Failed to end the consumer.", e);
		}
		catch (final EtlTransformationException e)
		{
			markAllConsumedAsError("Failed to create media.", e);
			throw new EtlTransformationException("Failed to create media.", e);
		}
	}

	protected void closeStreams()
	{
		if (xmlStreamWriter != null)
		{
			try
			{
				xmlStreamWriter.close();
			}
			catch (final XMLStreamException e)
			{
				LOG.error("Could not close XML stream writer.", e);
			}
		}
		if (fileOutputStream != null)
		{
			try
			{
				fileOutputStream.close();
			}
			catch (final IOException e)
			{
				LOG.error("Could not close file output stream.", e);
			}
		}
	}

	public XMLStreamWriter getXmlStreamWriter()
	{
		return xmlStreamWriter;
	}

	public void setAtLeastOneItem(final boolean atLeastOneItem)
	{
		this.atLeastOneItem = atLeastOneItem;
	}

	public String getDtd()
	{
		return dtd;
	}

	public void setDtd(final String dtd)
	{
		this.dtd = dtd;
	}

	protected boolean isWriteBom()
	{
		return writeBom;
	}

	public void setWriteBom(final boolean writeBom)
	{
		this.writeBom = writeBom;
	}

	public String getNamespaceURI()
	{
		return namespaceURI;
	}

	public void setNamespaceURI(final String namespaceURI)
	{
		this.namespaceURI = namespaceURI;
	}
}
