package com.waters.hybris.integration.core.etl.strategy.impl;

import com.neoworks.hybris.etl.context.EtlContext;
import com.waters.hybris.integration.core.etl.strategy.EtlNamingStrategy;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

public class DefaultEtlMediaNameStrategy implements EtlNamingStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultEtlMediaNameStrategy.class);

	private PersistentKeyGenerator mediaCodeGenerator;

	@Override
	public String generateName(final EtlContext context)
	{
		return String.valueOf(getMediaCodeGenerator().generate());
	}

	protected PersistentKeyGenerator getMediaCodeGenerator()
	{
		return mediaCodeGenerator;
	}

	@Required
	public void setMediaCodeGenerator(final PersistentKeyGenerator mediaCodeGenerator)
	{
		this.mediaCodeGenerator = mediaCodeGenerator;
	}
}
