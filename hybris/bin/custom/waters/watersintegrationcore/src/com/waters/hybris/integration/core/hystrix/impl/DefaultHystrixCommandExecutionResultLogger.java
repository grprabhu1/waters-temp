package com.waters.hybris.integration.core.hystrix.impl;

import com.codahale.metrics.MetricRegistry;
import com.netflix.hystrix.HystrixCommand;
import com.waters.hybris.integration.core.hystrix.HystrixCommandExecutionResultLogger;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

public class DefaultHystrixCommandExecutionResultLogger implements HystrixCommandExecutionResultLogger
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultHystrixCommandExecutionResultLogger.class);

	private MetricRegistry metricRegistry;

	@Override
	public void logExecutionResult(final HystrixCommand<?> command, final String action, final String metricKeyPrefix, final String loggingServiceName)
	{
		final String metricKey = metricKeyPrefix + StringUtils.deleteWhitespace(action);
		if (!command.isSuccessfulExecution())
		{
			@SuppressWarnings("ThrowableResultOfMethodCallIgnored")
			final Throwable ex = command.getFailedExecutionException();
			if (ex != null)
			{
				getMetricRegistry().counter(metricKey + ".error").inc();
				LOG.error("Error calling {} for action [{}].", loggingServiceName, action, ex);
			}

			if (command.isCircuitBreakerOpen())
			{
				getMetricRegistry().counter(metricKey + ".circuitbreakeropen").inc();
				LOG.warn("{} circuit break open for action [{}].", loggingServiceName, action);
			}

			if (command.isResponseFromFallback())
			{
				getMetricRegistry().counter(metricKey + ".answeredfromfallback").inc();
				LOG.warn("Using {} fallback response for action [{}].", loggingServiceName, action);
			}

			if (command.isResponseRejected())
			{
				getMetricRegistry().counter(metricKey + ".rejected").inc();
				LOG.error("{} command rejected by hystrix for action [{}].", loggingServiceName, action);
			}

			if (command.isResponseTimedOut())
			{
				getMetricRegistry().counter(metricKey + ".timeout").inc();
				LOG.warn("{} command timed out for action [{}].", loggingServiceName, action);
			}

		}
		else
		{
			getMetricRegistry().counter(metricKey + ".success").inc();
		}

		if (!command.isExecutedInThread())
		{
			getMetricRegistry().counter(metricKey + ".executedonthread").inc();
			LOG.warn("Executing {} command on calling thread for action [{}].", loggingServiceName, action);
		}
	}

	protected MetricRegistry getMetricRegistry()
	{
		return metricRegistry;
	}

	@Required
	public void setMetricRegistry(final MetricRegistry metricRegistry)
	{
		this.metricRegistry = metricRegistry;
	}
}
