package com.waters.hybris.integration.core.strategy.impl;

import com.waters.hybris.integration.core.strategy.ModelServiceSelectionStrategy;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;

public class DefaultConfigPropertyModelServiceSelectionStrategy implements ModelServiceSelectionStrategy<String>
{
	protected static final boolean DEFAULT_USE_TRANSACTIONAL_MODEL_SERVICE = false;

	private ModelService modelService;
	private ModelService transactionalModelService;
	private ConfigurationService configurationService;

	@Override
	public ModelService selectModelService(final String useTransactionalModelServiceConfigKey)
	{
		if (useTransactionalModelService(useTransactionalModelServiceConfigKey))
		{
			return getTransactionalModelService();
		}
		else
		{
			return getModelService();
		}
	}

	@Override
	public boolean useTransactionalModelService(final String useTransactionalModelServiceConfigKey)
	{
		return getConfigurationService().getConfiguration().getBoolean(useTransactionalModelServiceConfigKey, DEFAULT_USE_TRANSACTIONAL_MODEL_SERVICE);
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected ModelService getTransactionalModelService()
	{
		return transactionalModelService;
	}

	@Required
	public void setTransactionalModelService(final ModelService transactionalModelService)
	{
		this.transactionalModelService = transactionalModelService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
