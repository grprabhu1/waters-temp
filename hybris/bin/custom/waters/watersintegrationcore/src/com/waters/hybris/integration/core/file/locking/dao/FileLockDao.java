package com.waters.hybris.integration.core.file.locking.dao;

import com.waters.hybris.integration.core.model.file.locking.FileLockModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

public interface FileLockDao extends GenericDao<FileLockModel>
{
	FileLockModel getFileLockForPath(String path);

	FileLockModel getFileLockForToken(String token);
}