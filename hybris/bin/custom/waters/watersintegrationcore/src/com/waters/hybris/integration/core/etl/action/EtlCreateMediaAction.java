package com.waters.hybris.integration.core.etl.action;

import com.neoworks.hybris.etl.action.EtlAction;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.waters.hybris.integration.core.etl.strategy.EtlNamingStrategy;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

public class EtlCreateMediaAction implements EtlAction
{
	private static final Logger LOG = LoggerFactory.getLogger(EtlCreateMediaAction.class);

	private ModelService modelService;
	private MediaService mediaService;
	private EtlNamingStrategy etlNamingStrategy;
	private String mediaFolder;
	private String mediaMimeType;
	private String mediaCodePrefix;
	private String mediaCatVersionParamKey;
	private String mediaParamKey;
	private String fileParamKey;
	private EtlNamingStrategy mediaFileNameStrategy;
	private String mediaFolderFromContext;

	@Override
	public void execute(final EtlContext etlContext) throws EtlTransformationException
	{
		final Object catalogVersion = etlContext.getParameters().get(getMediaCatVersionParamKey());
		final CatalogVersionModel dataCatalogVersion;

		if (catalogVersion instanceof CatalogVersionModel)
		{
			dataCatalogVersion = (CatalogVersionModel) catalogVersion;
		}
		else
		{
			throw new EtlTransformationException("Media catalog version must be provided in ETL context parameters");
		}

		final String filePath = (String) etlContext.getParameters().get(getFileParamKey());
		if (filePath == null)
		{
			throw new EtlTransformationException("File path must be provided in ETL context parameters");
		}

		final MediaModel media = createNewMediaModel(etlContext);
		media.setCode(getMediaCodePrefix() + getEtlMediaNameStrategy().generateName(etlContext));
		media.setCatalogVersion(dataCatalogVersion);
		media.setMime(getMediaMimeType());

		if (getMediaFolderFromContext() != null)
		{
			final MediaFolderModel mediaFolderModel = (MediaFolderModel) etlContext.getParameters().get(getMediaFolderFromContext());
			media.setFolder(mediaFolderModel);
		}
		else if (getMediaFolder() != null)
		{
			media.setFolder(getMediaService().getFolder(getMediaFolder()));
		}
		else
		{
			throw new EtlTransformationException("No output media folder set.");
		}

		final String realFileName = getMediaFileNameStrategy().generateName(etlContext);
		media.setRealFileName(realFileName);
		getModelService().save(media);

		etlContext.getParameters().put(getMediaParamKey(), media);
	}

	protected MediaModel createNewMediaModel(final EtlContext etlContext)
	{
		return getModelService().create(MediaModel.class);
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected MediaService getMediaService()
	{
		return mediaService;
	}

	@Required
	public void setMediaService(final MediaService mediaService)
	{
		this.mediaService = mediaService;
	}

	protected EtlNamingStrategy getEtlMediaNameStrategy()
	{
		return etlNamingStrategy;
	}

	@Required
	public void setEtlMediaNameStrategy(final EtlNamingStrategy etlNamingStrategy)
	{
		this.etlNamingStrategy = etlNamingStrategy;
	}

	protected String getMediaFolder()
	{
		return mediaFolder;
	}

	public void setMediaFolder(final String mediaFolder)
	{
		this.mediaFolder = mediaFolder;
	}

	protected String getMediaMimeType()
	{
		return mediaMimeType;
	}

	@Required
	public void setMediaMimeType(final String mediaMimeType)
	{
		this.mediaMimeType = mediaMimeType;
	}

	protected String getMediaCodePrefix()
	{
		return mediaCodePrefix;
	}

	@Required
	public void setMediaCodePrefix(final String mediaCodePrefix)
	{
		this.mediaCodePrefix = mediaCodePrefix;
	}

	protected String getMediaCatVersionParamKey()
	{
		return mediaCatVersionParamKey;
	}

	@Required
	public void setMediaCatVersionParamKey(final String mediaCatVersionParamKey)
	{
		this.mediaCatVersionParamKey = mediaCatVersionParamKey;
	}

	protected String getMediaParamKey()
	{
		return mediaParamKey;
	}

	@Required
	public void setMediaParamKey(final String mediaParamKey)
	{
		this.mediaParamKey = mediaParamKey;
	}

	protected String getFileParamKey()
	{
		return fileParamKey;
	}

	@Required
	public void setFileParamKey(final String fileParamKey)
	{
		this.fileParamKey = fileParamKey;
	}

	protected EtlNamingStrategy getMediaFileNameStrategy()
	{
		return mediaFileNameStrategy;
	}

	@Required
	public void setMediaFileNameStrategy(final EtlNamingStrategy mediaFileNameStrategy)
	{
		this.mediaFileNameStrategy = mediaFileNameStrategy;
	}

	protected String getMediaFolderFromContext()
	{
		return mediaFolderFromContext;
	}

	public void setMediaFolderFromContext(final String mediaFolderFromContext)
	{
		this.mediaFolderFromContext = mediaFolderFromContext;
	}
}
