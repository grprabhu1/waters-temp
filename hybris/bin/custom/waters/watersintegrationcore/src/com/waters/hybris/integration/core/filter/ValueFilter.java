package com.waters.hybris.integration.core.filter;


public interface ValueFilter<T>
{
	boolean matches(T value);
}
