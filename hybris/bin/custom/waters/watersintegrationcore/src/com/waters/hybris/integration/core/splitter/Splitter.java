package com.waters.hybris.integration.core.splitter;

import com.waters.hybris.integration.core.context.ImportContext;
import com.waters.hybris.integration.core.iterator.CloseableIterator;

import java.io.IOException;
import java.io.InputStream;

public interface Splitter<T>
{
	CloseableIterator<T> split(InputStream stream, ImportContext importContext) throws IOException;
}
