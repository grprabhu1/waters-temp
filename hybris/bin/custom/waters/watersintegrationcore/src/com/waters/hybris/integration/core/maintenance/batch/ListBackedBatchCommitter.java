package com.waters.hybris.integration.core.maintenance.batch;

import com.google.common.collect.Sets;
import com.neoworks.hybris.util.BatchCommitter;
import com.waters.hybris.integration.core.maintenance.batch.strategy.FlushingStrategy;
import de.hybris.platform.servicelayer.model.ModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;


public class ListBackedBatchCommitter implements BatchCommitter
{
	private static final Logger LOG = LoggerFactory.getLogger(ListBackedBatchCommitter.class);
	private final ModelService modelService;
	private final int commitThreshold;
	private final FlushingStrategy flushingStrategy;
	private final boolean detachAfterCommit;
	private final Set<Object> forCommit;
	private int commitCount;

	public ListBackedBatchCommitter(final int threshold, final ModelService modelService,
									final FlushingStrategy flushingStrategy)
	{
		this(threshold, modelService, false, flushingStrategy);
	}

	public ListBackedBatchCommitter(final int threshold, final ModelService modelService,
									final boolean detachAfterCommit, final FlushingStrategy flushingStrategy)
	{
		this.commitCount = 0;
		this.commitThreshold = threshold;
		this.modelService = modelService;
		this.detachAfterCommit = detachAfterCommit;
		this.flushingStrategy = flushingStrategy;
		this.forCommit = Sets.newHashSet();
		LOG.debug("Created new BatchCommitter. Commit Threashold [{}]. Detach on commit [{}]", Integer.valueOf(this.commitThreshold), Boolean.valueOf(this.detachAfterCommit));
	}

	@Override
	public void reset()
	{
		if (!this.forCommit.isEmpty())
		{
			LOG.warn("Resetting BatchCommitter with [{}] items to commit. These items will be abandoned. The logged commitCount is currently [{}]; now resetting to zero.", Integer
					.valueOf(this.forCommit.size()), Integer.valueOf(this.commitCount));
		}

		this.forCommit.clear();
		this.commitCount = 0;
	}

	@Override
	public boolean addAll(final Collection<?> items)
	{
		Assert.isTrue(!items.contains((Object) null));
		final boolean success = this.forCommit.addAll(items);
		if (success)
		{
			LOG.debug("Added {} elements.  There are now {} items to commit.", Integer.valueOf(items.size()), Integer.valueOf(this.forCommit.size()));
		}
		else
		{
			LOG.debug("Added {} elements, but set of items was unchanged. There are {} items to commit.", Integer.valueOf(items.size()), Integer.valueOf(this.forCommit.size()));
		}

		return success;
	}

	@Override
	public boolean add(final Object... elements)
	{
		return this.addAll(Arrays.asList(elements));
	}

	@Override
	public boolean commitIfReady()
	{
		return this.commitInternal(false);
	}

	@Override
	public void commit()
	{
		this.commitInternal(true);
	}

	protected boolean commitInternal(final boolean force)
	{
		if (this.forCommit.size() < this.commitThreshold && !force)
		{
			return false;
		}
		else
		{
			this.commitCount += getFlushingStrategy().flush(this.forCommit, detachAfterCommit, this.modelService);

			this.forCommit.clear();
			LOG.info("[{}] items successfully committed to date.", Integer.valueOf(this.commitCount));
			return true;
		}
	}



	protected ModelService getModelService()
	{
		return this.modelService;
	}

	protected int getCommitThreshold()
	{
		return this.commitThreshold;
	}

	protected boolean isDetachAfterCommit()
	{
		return this.detachAfterCommit;
	}

	protected Set<Object> getForCommit()
	{
		return this.forCommit;
	}

	protected int getCommitCount()
	{
		return this.commitCount;
	}

	protected FlushingStrategy getFlushingStrategy()
	{
		return flushingStrategy;
	}
}
