package com.waters.hybris.integration.core.strategy.impl;

import com.waters.hybris.integration.core.strategy.DateConversionStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class XMLGregorianCalendarDateConversionStrategy implements DateConversionStrategy<XMLGregorianCalendar>
{

	private static final Logger LOG = LoggerFactory.getLogger(XMLGregorianCalendarDateConversionStrategy.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public XMLGregorianCalendar convert(final Date date)
	{
		return convert(date, null);
	}

	@Override
	public XMLGregorianCalendar convert(final Date date, final TimeZone timeZone)
	{
		if (date != null)
		{
			final GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			if (timeZone != null)
			{
				calendar.setTimeZone(timeZone);
			}
			try
			{
				return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
			}
			catch (final DatatypeConfigurationException e)
			{
				LOG.error("Error converting date [" + date + "]", e);
				throw new RuntimeException("Error converting date [" + date + "]", e);
			}
		}
		return null;
	}
}
