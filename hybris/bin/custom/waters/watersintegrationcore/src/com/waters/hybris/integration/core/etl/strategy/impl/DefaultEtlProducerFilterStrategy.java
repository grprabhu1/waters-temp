package com.waters.hybris.integration.core.etl.strategy.impl;

import com.waters.hybris.integration.core.etl.strategy.EtlProducerFilterStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

public class DefaultEtlProducerFilterStrategy<T> implements EtlProducerFilterStrategy<T>
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultEtlProducerFilterStrategy.class);

	@Override
	public Collection<T> filter(final Collection<T> results)
	{
		return results;
	}
}
