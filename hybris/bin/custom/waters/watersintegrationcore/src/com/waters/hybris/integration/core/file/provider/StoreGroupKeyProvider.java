package com.waters.hybris.integration.core.file.provider;

import com.waters.hybris.integration.core.file.parser.FileNameHashParser;
import org.springframework.util.Assert;

import java.io.File;

public class StoreGroupKeyProvider implements GroupKeyProvider
{
	private FileNameHashParser fileNameHashParser;

	@Override
	public String getGroupKey(final File file)
	{
		Assert.notNull(file, "file cannot be null");
		return getGroupKey(file.getName());
	}

	@Override
	public String getGroupKey(final String filename)
	{
		Assert.notNull(filename, "file cannot be null");
		return getFileNameHashParser().getHash(filename);
	}

	protected FileNameHashParser getFileNameHashParser()
	{
		return fileNameHashParser;
	}

	public void setFileNameHashParser(final FileNameHashParser fileNameHashParser)
	{
		this.fileNameHashParser = fileNameHashParser;
	}
}
