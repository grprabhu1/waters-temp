package com.waters.hybris.integration.core.etl.transformer;

import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import com.neoworks.hybris.etl.transformers.DataTransformer;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultDataTransformer<T, R> implements DataTransformer<T, R>
{
	private static final Logger LOG = LoggerFactory.getLogger(com.neoworks.hybris.etl.transformers.impl.DefaultDataTransformer.class);
	private EtlContext context;
	private Converter<T, R> converter;

	public void init(final EtlContext context) throws EtlValidationException
	{
		this.context = context;
	}

	public void end(final boolean success)
	{
		LOG.debug("DataTransformer finished");
	}

	public R transform(final T source)
	{
		return this.getConverter().convert(source);
	}

	public void start() throws EtlTransformationException
	{
		LOG.debug("DataTransformer start");
	}

	protected Converter<T, R> getConverter()
	{
		return this.converter;
	}

	public void setConverter(final Converter<T, R> converter)
	{
		this.converter = converter;
	}

	protected EtlContext getContext()
	{
		return this.context;
	}

	protected void setContext(final EtlContext context)
	{
		this.context = context;
	}
}
