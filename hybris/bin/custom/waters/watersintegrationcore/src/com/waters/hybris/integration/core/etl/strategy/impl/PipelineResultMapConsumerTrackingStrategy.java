package com.waters.hybris.integration.core.etl.strategy.impl;

import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.waters.hybris.integration.core.data.PipelineResultData;
import com.waters.hybris.integration.core.etl.data.PipelineResult;
import com.waters.hybris.integration.core.etl.strategy.ConsumerTrackingStrategy;
import com.waters.hybris.integration.core.etl.util.PipelineResultDataUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Map;

/**
 * Implementation of {@link ConsumerTrackingStrategy} which tracks the items in the context pipeline result map.
 */
public class PipelineResultMapConsumerTrackingStrategy implements ConsumerTrackingStrategy
{
	private String pipelineResultMapParamKey;
	private String currentItemKeyParamKey;

	@Override
	public void markAsConsumed(final EtlContext context) throws EtlTransformationException
	{
		final Map<Object, PipelineResultData> pipelineResultMap = (Map<Object, PipelineResultData>) context.getParameters().get(getPipelineResultMapParamKey());
		if (MapUtils.isNotEmpty(pipelineResultMap))
		{
			final Object itemKey = context.getParameters().get(getCurrentItemKeyParamKey());
			pipelineResultMap.put(itemKey, PipelineResultDataUtils.createPipelineResultData(PipelineResult.CONSUMED));
		}
		else
		{
			throw new EtlTransformationException("Result map must be provided in ETL context parameters");
		}
	}

	@Override
	public void markAsComplete(final EtlContext context) throws EtlTransformationException
	{
		final Map<Object, PipelineResultData> pipelineResultMap = (Map<Object, PipelineResultData>) context.getParameters().get(getPipelineResultMapParamKey());
		if (MapUtils.isNotEmpty(pipelineResultMap))
		{
			final Object itemKey = context.getParameters().get(getCurrentItemKeyParamKey());
			pipelineResultMap.put(itemKey, PipelineResultDataUtils.createPipelineResultData(PipelineResult.COMPLETE));
		}
		else
		{
			throw new EtlTransformationException("Result map must be provided in ETL context parameters");
		}
	}

	@Override
	public void markAsError(final EtlContext context, final String errorMessage) throws EtlTransformationException
	{
		final Map<Object, PipelineResultData> pipelineResultMap = (Map<Object, PipelineResultData>) context.getParameters().get(getPipelineResultMapParamKey());
		if (MapUtils.isNotEmpty(pipelineResultMap))
		{
			final Object itemKey = context.getParameters().get(getCurrentItemKeyParamKey());
			pipelineResultMap.put(itemKey, PipelineResultDataUtils.createPipelineResultData(PipelineResult.ERROR, errorMessage));
		}
		else
		{
			throw new EtlTransformationException("Result map must be provided in ETL context parameters");
		}
	}

	@Override
	public void markAllConsumedAsComplete(final EtlContext context) throws EtlTransformationException
	{
		final Map<Object, PipelineResultData> pipelineResultMap = (Map<Object, PipelineResultData>) context.getParameters().get(getPipelineResultMapParamKey());
		if (pipelineResultMap != null)
		{
			pipelineResultMap.entrySet()
					.stream()
					.filter(e -> PipelineResult.CONSUMED.equals(e.getValue().getPipelineResult()))
					.forEach(e -> e.setValue(PipelineResultDataUtils.createPipelineResultData(PipelineResult.COMPLETE)));
		}
		else
		{
			throw new EtlTransformationException("Result map must be provided in ETL context parameters");
		}
	}

	@Override
	public void markAllConsumedAsError(final EtlContext context, final String errorMessage) throws EtlTransformationException
	{
		final Map<Object, PipelineResultData> pipelineResultMap = (Map<Object, PipelineResultData>) context.getParameters().get(getPipelineResultMapParamKey());
		if (pipelineResultMap != null)
		{
			pipelineResultMap.entrySet()
					.stream()
					.filter(e -> PipelineResult.CONSUMED.equals(e.getValue().getPipelineResult()))
					.forEach(e -> e.setValue(PipelineResultDataUtils.createPipelineResultData(PipelineResult.ERROR, errorMessage)));
		}
		else
		{
			throw new EtlTransformationException("Result map must be provided in ETL context parameters");
		}
	}

	protected String getPipelineResultMapParamKey()
	{
		return pipelineResultMapParamKey;
	}

	@Required
	public void setPipelineResultMapParamKey(final String pipelineResultMapParamKey)
	{
		this.pipelineResultMapParamKey = pipelineResultMapParamKey;
	}

	protected String getCurrentItemKeyParamKey()
	{
		return currentItemKeyParamKey;
	}

	@Required
	public void setCurrentItemKeyParamKey(final String currentItemKeyParamKey)
	{
		this.currentItemKeyParamKey = currentItemKeyParamKey;
	}
}
