package com.waters.hybris.integration.core.splitter.impl;

import com.ctc.wstx.exc.WstxEOFException;
import com.ctc.wstx.stax.WstxInputFactory;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.security.AnyTypePermission;
import com.thoughtworks.xstream.security.NoTypePermission;
import com.waters.hybris.integration.core.context.ImportContext;
import com.waters.hybris.integration.core.iterator.CloseableIterator;
import com.waters.hybris.integration.core.splitter.Splitter;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.xml.stream.XMLInputFactory;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

@SuppressFBWarnings(value = {"RV_RETURN_VALUE_IGNORED", "REC_CATCH_EXCEPTION"}, justification = "I don't need to read hasNext() return value in next()")
public class XmlSplitter<T> implements Splitter<T>
{
	private static final Logger LOG = LoggerFactory.getLogger(XmlSplitter.class);

	private Class<T> dataType;
	private boolean ignoreUnknownElements = false;
	private boolean allowAllTypes = false;
	private Set<String> allowedTypes;

	@Override
	@SuppressWarnings("unchecked")
	public CloseableIterator<T> split(final InputStream stream, final ImportContext importContext) throws IOException
	{
		final StaxDriver staxDriver = new NoDTDStaxDriver();
		final XStream xstream = new XStream(staxDriver);
		if (isIgnoreUnknownElements())
		{
			xstream.ignoreUnknownElements();
		}

		initPermissions(xstream);

		xstream.processAnnotations(getDataType());

		final ObjectInputStream objectInputStream = xstream.createObjectInputStream(new InputStreamReader(stream, StandardCharsets.UTF_8));


		final Iterator<T> iterator = new Iterator<T>()
		{
			private T cachedObject = null;
			private boolean finished = false;

			@Override
			public boolean hasNext()
			{
				if (cachedObject != null)
				{
					return true;
				}
				else if (finished)
				{
					return false;
				}
				else
				{
					final T object = readNextObject();
					if (object == null)
					{
						finished = true;
						return false;
					}
					else
					{
						cachedObject = object;
						return true;
					}
				}

			}

			@Override
			public T next()
			{
				if (!hasNext())
				{
					throw new NoSuchElementException("File has been consumed");
				}
				final T currentObject = cachedObject;
				cachedObject = null;
				return currentObject;
			}

			private T readNextObject()
			{
				while (true)
				{
					try
					{
						return (T) objectInputStream.readObject();
					}
					catch (final Exception e)
					{
						// Woodstox XML parsing library throws this exception when it reaches EOF in a malformed XML
						if (e instanceof EOFException || e.getCause() instanceof WstxEOFException)
						{
							return null;
						}
						else
						{
							final String msg = "Error parsing XML [source: " + importContext.getSource() + "]";
							LOG.error(msg, e);
							importContext.getAuditContext().addMessage(msg, e);
							importContext.handleInputParsingException();
						}
					}
				}
			}

		};

		return new CloseableIterator<T>()
		{
			@Override
			public Closeable getCloseable()
			{
				return objectInputStream;
			}

			@Override
			public Iterator<T> getIterator()
			{
				return iterator;
			}
		};
	}

	protected void initPermissions(final XStream xstream)
	{
		xstream.addPermission(NoTypePermission.NONE);
		if (isAllowAllTypes())
		{
			LOG.warn("initPermissions: CVE-2013-7285 - Allowing ANY type means XStream can be used for Remote Code Execution");
			xstream.addPermission(AnyTypePermission.ANY);
		}
		else
		{
			if (CollectionUtils.isNotEmpty(allowedTypes))
			{
				xstream.allowTypesByWildcard(allowedTypes.toArray(new String[allowedTypes.size()]));
			}
			else
			{
				xstream.allowTypesByWildcard(new String[] {getDataType().getPackage().getName() + ".*"});
			}
		}
	}

	protected Class<T> getDataType()
	{
		return dataType;
	}

	@Required
	public void setDataType(final Class<T> dataType)
	{
		this.dataType = dataType;
	}

	protected boolean isIgnoreUnknownElements()
	{
		return ignoreUnknownElements;
	}

	public void setIgnoreUnknownElements(final boolean ignoreUnknownElements)
	{
		this.ignoreUnknownElements = ignoreUnknownElements;
	}

	protected boolean isAllowAllTypes()
	{
		return allowAllTypes;
	}

	protected void setAllowAllTypes(final boolean allowAllTypes)
	{
		this.allowAllTypes = allowAllTypes;
	}

	protected Set<String> getAllowedTypes()
	{
		return allowedTypes;
	}

	public void setAllowedTypes(final Set<String> allowedTypes)
	{
		this.allowedTypes = allowedTypes;
	}

	static class NoDTDStaxDriver extends StaxDriver
	{
		@Override
		protected XMLInputFactory createInputFactory()
		{
			final XMLInputFactory factory = new WstxInputFactory();    // Not using system property 'javax.xml.stream.XMLInputFactory' as we don't want to make 'WstxInputFactory' as default
			factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);

			return factory;
		}
	}
}