package com.waters.hybris.integration.core.etl.producer;

import com.google.common.collect.Lists;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.neoworks.hybris.etl.job.EtlJobContext;
import com.neoworks.hybris.etl.producers.impl.FlexibleSearchDataProducer;
import com.waters.hybris.integration.core.etl.strategy.EtlProducerFilterStrategy;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;

public class LastRunFlexibleSearchProducer extends FlexibleSearchDataProducer
{
	private static final Logger LOG = LoggerFactory.getLogger(LastRunFlexibleSearchProducer.class);

	private Iterator<PK> resultIterator;
	private ItemModel lastItem;
	private String incrementalQuery;
	private EtlProducerFilterStrategy<ItemModel> etlProducerFilterStrategy;

	@Override
	public void start() throws EtlTransformationException
	{
		final FlexibleSearchQuery searchQuery;
		final Date lastSuccessfulRunDate = ((EtlJobContext) getContext()).getEtlCronJob().getLastSuccessfulRunDate();
		if (lastSuccessfulRunDate != null)
		{
			if (getIncrementalQuery() == null)
			{
				searchQuery = new FlexibleSearchQuery(getQuery() + " WHERE {modifiedtime} > ?lastSuccessfulRunDate", this.getContext().getParameters());
			}
			else
			{
				searchQuery = new FlexibleSearchQuery(getIncrementalQuery(), this.getContext().getParameters());
			}
		}
		else
		{
			searchQuery = new FlexibleSearchQuery(getQuery(), this.getContext().getParameters());
		}

		searchQuery.setResultClassList(Arrays.asList(new Class[]{PK.class}));
		final SearchResult result = this.getFlexibleSearchService().search(searchQuery);
		resultIterator = result.getResult().iterator();
		LOG.debug("Data producer has been started with {} items.", Integer.valueOf(result.getTotalCount()));
	}

	@Override
	public ItemModel get()
	{
		if (this.lastItem != null)
		{
			this.getModelService().detach(this.lastItem);
			this.lastItem = null;
		}

		while (this.resultIterator.hasNext())
		{
			final PK pk = this.resultIterator.next();
			final ItemModel nextItem = filter(this.getModelService().get(pk));

			if (nextItem != null)
			{
				this.lastItem = nextItem;
				return nextItem;
			}
		}

		return null;
	}

	protected ItemModel filter(final ItemModel item)
	{
		if (getEtlProducerFilterStrategy() != null)
		{
			return CollectionUtils.isEmpty(getEtlProducerFilterStrategy().filter(Lists.newArrayList(item))) ? null : item;
		}
		return item;
	}

	public String getIncrementalQuery()
	{
		return incrementalQuery;
	}

	public void setIncrementalQuery(final String incrementalQuery)
	{
		this.incrementalQuery = incrementalQuery;
	}

	protected EtlProducerFilterStrategy<ItemModel> getEtlProducerFilterStrategy()
	{
		return etlProducerFilterStrategy;
	}

	public void setEtlProducerFilterStrategy(final EtlProducerFilterStrategy<ItemModel> etlProducerFilterStrategy)
	{
		this.etlProducerFilterStrategy = etlProducerFilterStrategy;
	}
}
