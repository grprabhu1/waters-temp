package com.waters.hybris.integration.core.file.filter;

import com.waters.hybris.integration.core.dao.FileImportRecordDao;
import com.waters.hybris.integration.core.enums.FileType;
import org.fest.util.Collections;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import java.io.File;

/**
 * @author Tom Greasley (tom@neoworks.com)
 */
public class FileNameFilter implements FileFilter
{
	private FileImportRecordDao fileImportRecordDao;
	private FileType fileType;

	@Override
	public boolean accept(final File file)
	{
		Assert.notNull(file, "file cannot be null");
		final String fileName = file.getName();
		return Collections.isEmpty(getFileImportRecordDao().findByName(getFileType(), fileName));
	}

	@Required
	protected FileImportRecordDao getFileImportRecordDao()
	{
		return fileImportRecordDao;
	}

	@Required
	public void setFileImportRecordDao(final FileImportRecordDao fileImportRecordDao)
	{
		this.fileImportRecordDao = fileImportRecordDao;
	}

	protected FileType getFileType()
	{
		return fileType;
	}

	@Required
	public void setFileType(final FileType fileType)
	{
		this.fileType = fileType;
	}
}
