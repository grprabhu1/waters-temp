package com.waters.hybris.integration.core.etl.util;

import com.waters.hybris.integration.core.data.PipelineResultData;
import com.waters.hybris.integration.core.etl.data.PipelineResult;

/**
 * @author ramana
 */
public class PipelineResultDataUtils
{
	public static PipelineResultData createPipelineResultData(final PipelineResult result)
	{
		return createPipelineResultData(result, null);
	}

	public static PipelineResultData createPipelineResultData(final PipelineResult result, final String message)
	{
		final PipelineResultData pipelineResultData = new PipelineResultData();
		pipelineResultData.setPipelineResult(result);
		pipelineResultData.setMessage(message);

		return pipelineResultData;
	}
}
