package com.waters.hybris.integration.core.etl.action;

import com.google.common.io.ByteSink;
import com.google.common.io.ByteSource;
import com.google.common.io.Files;
import com.neoworks.hybris.etl.action.EtlAction;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.waters.hybris.integration.core.etl.strategy.EtlNamingStrategy;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * {@link EtlAction} to copy file directly to a particular location.
 */
public class EtlCopyFileToDestinationAction implements EtlAction
{

	private String fileParamKey;
	private String destinationFilePath;
	private EtlNamingStrategy etlNamingStrategy;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void execute(final EtlContext context) throws EtlTransformationException
	{
		ensureDestination(getDestinationFilePath());

		try
		{
			final String sourceFileLocation = (String) context.getParameters().get(getFileParamKey());
			final File sourceFile = new File(sourceFileLocation);

			if (!sourceFile.exists())
			{
				throw new EtlTransformationException("Source file at location [" + sourceFileLocation + "] does not exist");
			}

			final ByteSource byteSource = Files.asByteSource(sourceFile);

			final File outputFile = new File(getFileName(getDestinationFilePath(), getEtlNamingStrategy().generateName(context)));
			final ByteSink byteSink = Files.asByteSink(outputFile);

			try (final InputStream inputStream = byteSource.openStream())
			{
				byteSink.writeFrom(inputStream);
			}
		}
		catch (final IOException e)
		{
			throw new EtlTransformationException("Error moving file to destination.", e);
		}
	}

	protected void ensureDestination(final String destinationPath) throws EtlTransformationException
	{
		final File file = new File(destinationPath);
		if (!(file.exists() || file.mkdirs()))
		{
			throw new EtlTransformationException("Unable to create output directory [" + destinationPath + "]");
		}
	}

	protected String getFileName(final String folder, final String fileName)
	{
		final String file = fileName.startsWith(File.separator) ? StringUtils.removeStart(fileName, File.separator) : fileName;
		return folder.endsWith(File.separator) ? folder + file : folder + File.separator + file;
	}

	protected String getFileParamKey()
	{
		return fileParamKey;
	}

	@Required
	public void setFileParamKey(final String fileParamKey)
	{
		this.fileParamKey = fileParamKey;
	}

	protected String getDestinationFilePath()
	{
		return destinationFilePath;
	}

	@Required
	public void setDestinationFilePath(final String destinationFilePath)
	{
		this.destinationFilePath = destinationFilePath;
	}

	protected EtlNamingStrategy getEtlNamingStrategy()
	{
		return etlNamingStrategy;
	}

	@Required
	public void setEtlNamingStrategy(final EtlNamingStrategy etlNamingStrategy)
	{
		this.etlNamingStrategy = etlNamingStrategy;
	}
}
