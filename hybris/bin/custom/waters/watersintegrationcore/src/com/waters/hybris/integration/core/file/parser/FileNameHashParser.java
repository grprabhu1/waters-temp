package com.waters.hybris.integration.core.file.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Tom Greasley (tom@neoworks.com)
 */
public class FileNameHashParser
{
	private static final Logger LOG = LoggerFactory.getLogger(FileNameHashParser.class);

	private Pattern extractHashPattern;

	public String getHash(final String fileName)
	{
		final Matcher matcher = getExtractHashPattern().matcher(fileName);
		if (matcher.matches())
		{
			return matcher.group(1);
		}
		else
		{
			LOG.error("File name [{}] does not match pattern [{}].", fileName, getExtractHashPattern());
		}

		return null;
	}

	protected Pattern getExtractHashPattern()
	{
		return extractHashPattern;
	}

	@Required
	public void setExtractHashPattern(final Pattern extractHashPattern)
	{
		this.extractHashPattern = extractHashPattern;
	}
}
