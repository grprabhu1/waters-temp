package com.waters.hybris.integration.core.dao;

import com.waters.hybris.integration.core.enums.FileType;
import com.waters.hybris.integration.core.model.record.FileImportRecordModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.Date;
import java.util.List;

public interface FileImportRecordDao extends GenericDao<FileImportRecordModel>
{
	List<FileImportRecordModel> findAllPendingOlderThan(FileType fileType, Date date);

	List<FileImportRecordModel> findAllPending(FileType fileType);

	Date findLastSuccess(FileType fileType);

	List<FileImportRecordModel> findByHash(FileType fileType, String fileHash);

	List<FileImportRecordModel> findByName(FileType fileType, String fileName);
}
