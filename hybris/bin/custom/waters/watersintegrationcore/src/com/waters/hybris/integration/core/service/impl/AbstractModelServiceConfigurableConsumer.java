package com.waters.hybris.integration.core.service.impl;

import com.waters.hybris.integration.core.context.ImportContext;
import com.waters.hybris.integration.core.strategy.ModelServiceSelectionStrategy;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;

public abstract class AbstractModelServiceConfigurableConsumer<T, C extends ImportContext> extends AbstractContextAwareConsumer<T, C>
{
	private ModelServiceSelectionStrategy<C> modelServiceSelectionStrategy;

	protected ModelService getModelService(final C context)
	{
		return getModelServiceSelectionStrategy().selectModelService(context);
	}

	protected ModelServiceSelectionStrategy<C> getModelServiceSelectionStrategy()
	{
		return modelServiceSelectionStrategy;
	}

	@Required
	public void setModelServiceSelectionStrategy(final ModelServiceSelectionStrategy<C> modelServiceSelectionStrategy)
	{
		this.modelServiceSelectionStrategy = modelServiceSelectionStrategy;
	}
}
