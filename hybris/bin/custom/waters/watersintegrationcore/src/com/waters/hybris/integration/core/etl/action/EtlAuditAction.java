package com.waters.hybris.integration.core.etl.action;

import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.etl.action.EtlAction;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public class EtlAuditAction implements EtlAction
{
	private static final Logger LOG = LoggerFactory.getLogger(EtlAuditAction.class);

	private String failingRecordsKey;
	private SystemArea systemArea = SystemArea.ETL;

	@Override
	public void execute(final EtlContext etlContext) throws EtlTransformationException
	{
		final Object param = etlContext.getParameters().get(getFailingRecordsKey());
		if (param instanceof List)
		{
			final List<Object> errors = (List<Object>) param;
			final AuditContext auditContext = etlContext.getAuditService().beginAudit(getSystemArea(), null, "Error processing {} items for {}", errors.size(), etlContext.getActivityDescription());
			errors.forEach(x ->
			{
				auditContext.addMessage("Error processing item {}", x);
				LOG.error("Error processing items " + x);
			});

			auditContext.endAudit(Status.FAILURE, null);
		}
		else
		{
			etlContext.getAuditService()
				.audit(getSystemArea(), Status.SUCCESS, "No failures during the " + getSystemArea() + " data extraction for {}", etlContext.getActivityDescription());
		}
	}

	protected String getFailingRecordsKey()
	{
		return failingRecordsKey;
	}

	@Required
	public void setFailingRecordsKey(final String failingRecordsKey)
	{
		this.failingRecordsKey = failingRecordsKey;
	}

	public SystemArea getSystemArea()
	{
		return systemArea;
	}

	public void setSystemArea(final SystemArea systemArea)
	{
		this.systemArea = systemArea;
	}
}
