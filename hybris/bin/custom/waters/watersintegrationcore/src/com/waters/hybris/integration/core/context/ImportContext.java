package com.waters.hybris.integration.core.context;

import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.util.BatchCommitter;
import com.waters.hybris.integration.core.model.record.FileImportResultModel;
import de.hybris.platform.core.model.ItemModel;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Not thread safe.
 */
public abstract class ImportContext<T> implements AutoCloseable
{
	private final T input;
	private final AuditContext auditContext;
	private final BatchCommitter batchCommitter;
	private final Map<String, ItemModel> processedItems = new HashMap<>();
	private int totalProcessed = 0;
	private int totalSuccess = 0;
	private int totalError = 0;
	private boolean success;
	private boolean absolute;
	private boolean useTransactionalModelService = false;
	private Date startTime;
	private JobContext jobContext;
	private FileImportResultModel lastFileImportResult;

	public ImportContext(final T input, final AuditContext auditContext, final BatchCommitter batchCommitter)
	{
		this.input = input;
		this.auditContext = auditContext;
		this.batchCommitter = batchCommitter;
		this.startTime = new Date();
	}

	public T getInput()
	{
		return input;
	}

	public AuditContext getAuditContext()
	{
		return auditContext;
	}

	public BatchCommitter getBatchCommitter()
	{
		return batchCommitter;
	}

	public void addToBatchAndRegister(final String key, final ItemModel item)
	{
		getBatchCommitter().add(item);
		registerAsProcessed(key, item);
		if (getBatchCommitter().commitIfReady())
		{
			resetProcessed();
		}
	}

	public Map<String, ItemModel> getProcessedItems()
	{
		return processedItems;
	}

	public void registerAsProcessed(final String id, final ItemModel item)
	{
		processedItems.put(id, item);
	}

	public boolean isProcessed(final String id)
	{
		return processedItems.containsKey(id);
	}

	public ItemModel getProcessed(final String id)
	{
		return processedItems.get(id);
	}

	public void resetProcessed()
	{
		processedItems.clear();
	}

	protected int getTotalProcessed()
	{
		return totalProcessed;
	}

	public int getTotalSuccess()
	{
		return totalSuccess;
	}

	public int getTotalError()
	{
		return totalError;
	}

	protected boolean isSuccess()
	{
		return success;
	}

	public boolean isAbsolute()
	{
		return absolute;
	}

	public void setIsAbsolute(final boolean absolute)
	{
		this.absolute = absolute;
	}

	public Date getStartTime()
	{
		return new Date(startTime.getTime());
	}

	public void setStartTime(final Date startTime)
	{
		this.startTime = new Date(startTime.getTime());
	}

	public boolean isUseTransactionalModelService()
	{
		return useTransactionalModelService;
	}

	public void setUseTransactionalModelService(final boolean useTransactionalModelService)
	{
		this.useTransactionalModelService = useTransactionalModelService;
	}

	public JobContext getJobContext()
	{
		return jobContext;
	}

	public void setJobContext(final JobContext jobContext)
	{
		this.jobContext = jobContext;
	}

	public boolean isAborted()
	{
		return getJobContext() != null && getJobContext().isAborted();
	}

	public FileImportResultModel getLastFileImportResult()
	{
		return lastFileImportResult;
	}

	public void setLastFileImportResult(final FileImportResultModel lastFileImportResult)
	{
		this.lastFileImportResult = lastFileImportResult;
	}

	public void incrementSuccess()
	{
		totalSuccess++;
		totalProcessed++;
	}

	public void incrementError()
	{
		totalError++;
		totalProcessed++;
	}

	public void incrementProcessed()
	{
		totalProcessed++;
	}

	public void markAsSuccessful()
	{
		success = true;
	}

	@Override
	public void close() throws Exception
	{
		flushBatchCommitter();
		endAudit();
	}

	public void flush()
	{
		flushBatchCommitter();
	}

	protected void endAudit()
	{
		if (getAuditContext() != null)
		{
			if (isSuccess())
			{
				getAuditContext().addMessage(generateSuccessMessage());
				getAuditContext().endAudit(Status.SUCCESS, null);
			}
			else
			{
				getAuditContext().addMessage(generateErrorMessage());
				getAuditContext().endAudit(Status.FAILURE, null);
			}
		}
	}

	protected void flushBatchCommitter()
	{
		if (getBatchCommitter() != null)
		{
			getBatchCommitter().commit();
		}
	}

	public void handleInputParsingException()
	{
		// Do nothing by default
	}

	public void handleNoDataToProcessError()
	{
		// Do nothing by default
	}

	protected abstract String generateSuccessMessage();

	protected abstract String generateErrorMessage();

	public abstract String getSource();
}
