package com.waters.hybris.integration.core.context;

import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang.BooleanUtils;

import java.util.Optional;

public class JobContext
{
	private final CronJobModel cronJob;
	private final ModelService modelService;
	private boolean alreadyAbortedFlag = false;

	public JobContext(final CronJobModel cronJob, final ModelService modelService)
	{
		this.cronJob = cronJob;
		this.modelService = modelService;
	}

	public boolean isAborted()
	{
		if (alreadyAbortedFlag)
		{
			return true;
		}
		else
		{
			if (cronJob != null)
			{
				modelService.refresh(cronJob);
				if (BooleanUtils.isTrue(cronJob.getRequestAbort()))
				{
					alreadyAbortedFlag = true;
					return true;
				}
			}

			return false;
		}
	}

	public Optional<String> getCronJobCode()
	{
		if (cronJob != null)
		{
			return Optional.ofNullable(cronJob.getCode());
		}
		return Optional.empty();
	}
}
