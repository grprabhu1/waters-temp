package com.waters.hybris.integration.core.store.dao;

import com.waters.hybris.integration.core.enums.ExternalDataSource;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

/**
 * DAO for accessing BaseStore data for product data import.
 */
public interface IntegrationBaseStoreDao
{
	/**
	 * Gets the BaseStores for the passed in external store ID and external data source.
	 *
	 * @param externalStoreId external store ID
	 * @param source          external data source
	 * @return List of BaseStoreModel objects that match the external ID and source
	 */
	List<BaseStoreModel> getBaseStoreForExternalIdAndSource(String externalStoreId, ExternalDataSource source);
}
