package com.waters.hybris.integration.core.service;

import com.waters.hybris.integration.core.context.ImportContext;

public interface ContextAwareConsumer<T, C extends ImportContext>
{
	void accept(T item, C context);

	default void postAccept(final C context)
	{
		// do nothing
	}
}
