package com.waters.hybris.integration.core.file.provider;

import java.io.File;

/**
 * @author Tom Greasley (tom@neoworks.com)
 */
public interface FileHashProvider
{
	String getFileHash(File file);

	String getFileHash(String filename);
}
