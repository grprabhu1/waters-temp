package com.waters.hybris.integration.core.job;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.annotation.Timed;
import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.audit.service.AuditService;
import com.waters.hybris.integration.core.context.JobContext;
import com.waters.hybris.integration.core.dao.FileImportRecordDao;
import com.waters.hybris.integration.core.enums.FileProcessingStatus;
import com.waters.hybris.integration.core.enums.FileType;
import com.waters.hybris.integration.core.model.record.FileImportRecordModel;
import com.waters.hybris.integration.core.service.impl.DefaultMediaProcessor;
import de.hybris.platform.core.Registry;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class FeedImporterJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(FeedImporterJob.class);

	private AuditService auditService;
	private SystemArea systemArea;

	private FileImportRecordDao fileImportRecordDao;
	private DefaultMediaProcessor mediaProcessor;
	private FileType fileType;
	private MetricRegistry metricRegistry;
	private ConfigurationService configurationService;
	private TimeService timeService;

	@Override
	@Timed(name = "integration.core.feedImport.FeedImporterJob", absolute = true)
	public PerformResult perform(final CronJobModel cronJob)
	{
		final AuditContext auditContext = getAuditService().beginAudit(getSystemArea(), null, "Processing Inbound File Records of [{}]", fileType.getCode());

		final PerformResult result = performInternal(cronJob, auditContext);

		auditContext.endAudit(result.getResult() != CronJobResult.SUCCESS ? Status.SUCCESS : Status.WARN, null);

		return result;
	}

	protected PerformResult performInternal(final CronJobModel cronJob, final AuditContext auditContext)
	{
		final JobContext jobContext = new JobContext(cronJob, getModelService());

		final Date lastSuccess = getFileImportRecordDao().findLastSuccess(getFileType());

		if (lastSuccess != null)
		{
			final String lastSuccessString = DateTimeFormatter.ISO_DATE_TIME.format(ZonedDateTime.ofInstant(lastSuccess.toInstant(), ZoneOffset.UTC));
			LOG.info("Processing files received after {}", lastSuccessString);
		}

		final List<FileImportRecordModel> records = computeDelayTime(jobContext)
				.map(date -> getFileImportRecordDao().findAllPendingOlderThan(getFileType(), date))
				.orElseGet(() -> getFileImportRecordDao().findAllPending(getFileType()));

		for (final FileImportRecordModel record : records)
		{
			LOG.debug("Processing file record [{}]", record.getPk());

			if (clearAbortRequestedIfNeeded(cronJob))
			{
				LOG.warn("Feed import job has been aborted before processing record [{}]!", record.getPk());
				return new PerformResult(CronJobResult.UNKNOWN, CronJobStatus.ABORTED);
			}

			if (acceptRecord(lastSuccess, record))
			{
				LOG.debug("Record [{}] has been accepted for processing", record.getPk());
				processRecord(record, jobContext);
			}
			else
			{
				LOG.info("Record [{}] has been skipped during processing", record.getPk());
				skipRecord(record);
			}
		}

		if (clearAbortRequestedIfNeeded(cronJob))
		{
			final String message = "Feed import job has been aborted!";
			LOG.warn(message);
			auditContext.endAudit(Status.WARN, message);
			return new PerformResult(CronJobResult.UNKNOWN, CronJobStatus.ABORTED);
		}
		else
		{
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
	}

	protected Optional<Date> computeDelayTime(final JobContext jobContext)
	{
		final String configKey = jobContext.getCronJobCode()
				.map(cronJobCode -> String.format("%s.%s.delayInMinutes", cronJobCode, fileType.name()))
				.orElse(String.format("%s.delayInMinutes", fileType.name()));

		final int delayInMinutes = getConfigurationService().getConfiguration().getInt(configKey, 0);
		if (delayInMinutes > 0)
		{
			final Date date = new DateTime(getTimeService().getCurrentTime()).minusMinutes(delayInMinutes).toDate();

			LOG.info("Adding [{}} min delay as per config [{}]. Processing only file records older than [{}].", delayInMinutes, configKey, date);
			return Optional.of(date);
		}

		LOG.debug("No valid delay was configured for key [{}]. File records will be processed without any delay.", configKey);
		return Optional.empty();
	}

	protected void processRecord(final FileImportRecordModel record, final JobContext jobContext)
	{
		LOG.debug("Processing file record [{}]", record.getPk());

		record.setStatus(FileProcessingStatus.PROCESSING);
		record.setProcessingDate(Calendar.getInstance().getTime());
		record.setHybrisNode(Registry.getClusterID());
		getModelService().save(record);

		final long start = System.currentTimeMillis();
		try
		{
			getMediaProcessor().process(record, jobContext);
			final long end = System.currentTimeMillis();
			record.setDuration(end - start);
			if (FileProcessingStatus.PROCESSING.equals(record.getStatus()))
			{
				record.setStatus(FileProcessingStatus.DONE);
			}
			getModelService().save(record);

		}
		catch (final Exception e)
		{
			final long end = System.currentTimeMillis();
			record.setDuration(end - start);
			record.setStatus(FileProcessingStatus.ERROR);
			getModelService().save(record);

			LOG.error("Error importing product feed for media [{}]", record.getMedia().getCode(), e);
		}
	}

	protected boolean acceptRecord(final Date lastSuccess, final FileImportRecordModel record)
	{
		if (lastSuccess == null)
		{
			LOG.warn("Unable to compare file import record dates for file [{}].  The last successful import date is not known.", record.getFileName());
		}
		else if (record.getFileDate() == null)
		{
			LOG.warn("Unable to compare file import record dates for file [{}].  The date of the file is unknown.", record.getFileName());
		}

		return !(lastSuccess != null && record.getFileDate() != null && lastSuccess.compareTo(record.getFileDate()) >= 0);
	}

	protected void skipRecord(final FileImportRecordModel record)
	{
		LOG.warn("Skipping file import record for file [{}], more recent files have been processed.", record.getFileName());

		record.setStatus(FileProcessingStatus.SKIPPED);
		record.setProcessingDate(Calendar.getInstance().getTime());
		record.setHybrisNode(Registry.getClusterID());
		getModelService().save(record);
	}


	@Override
	public boolean isAbortable()
	{
		return true;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	protected FileImportRecordDao getFileImportRecordDao()
	{
		return fileImportRecordDao;
	}

	@Required
	public void setFileImportRecordDao(final FileImportRecordDao fileImportRecordDao)
	{
		this.fileImportRecordDao = fileImportRecordDao;
	}

	protected FileType getFileType()
	{
		return fileType;
	}

	@Required
	public void setFileType(final FileType fileType)
	{
		this.fileType = fileType;
	}

	protected MetricRegistry getMetricRegistry()
	{
		return metricRegistry;
	}

	@Required
	public void setMetricRegistry(final MetricRegistry metricRegistry)
	{
		this.metricRegistry = metricRegistry;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public TimeService getTimeService()
	{
		return this.timeService;
	}

	@Required
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	protected DefaultMediaProcessor getMediaProcessor()
	{
		return mediaProcessor;
	}

	@Required
	public void setMediaProcessor(final DefaultMediaProcessor mediaProcessor)
	{
		this.mediaProcessor = mediaProcessor;
	}


	public AuditService getAuditService()
	{
		return auditService;
	}

	@Required
	public void setAuditService(final AuditService auditService)
	{
		this.auditService = auditService;
	}

	public SystemArea getSystemArea()
	{
		return systemArea;
	}

	@Required
	public void setSystemArea(final SystemArea systemArea)
	{
		this.systemArea = systemArea;
	}
}
