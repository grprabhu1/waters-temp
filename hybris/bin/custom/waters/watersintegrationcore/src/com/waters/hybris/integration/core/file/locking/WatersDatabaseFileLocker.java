package com.waters.hybris.integration.core.file.locking;

import com.neoworks.hybris.feature.FeatureService;
import com.waters.hybris.integration.core.file.locking.dao.FileLockDao;
import com.waters.hybris.integration.core.model.file.locking.FileLockModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.integration.file.locking.AbstractFileLockerFilter;
import org.springframework.messaging.MessagingException;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class WatersDatabaseFileLocker extends AbstractFileLockerFilter
{
	public static final String FEATURE_DBLOCKING = "dblocking.enabled";
	private static final Logger LOG = LoggerFactory.getLogger(WatersDatabaseFileLocker.class);
	private final ConcurrentMap<File, String> lockCache = new ConcurrentHashMap<>();

	private FeatureService featureService;
	private FileLockDao fileLockDao;
	private ModelService modelService;
	private int lockTtl;

	@Override
	public boolean lock(final File file)
	{
		if (isDbLockingEnabled())
		{
			Assert.notNull(file, "file must not be null");

			if (file.isFile())
			{
				final String canonicalPath = getCanonicalPath(file);
				LOG.info("Attempting to lock file [{}].", canonicalPath);
				String token = lockCache.get(file);
				if (token == null)
				{
					LOG.debug("No lock token found for file [{}].", canonicalPath);
					final String newToken = (getDbLock(file) != null) ? null : createDbLock(file);
					if (StringUtils.isNotBlank(newToken))
					{
						final String originalToken = lockCache.putIfAbsent(file, newToken);
						token = StringUtils.isNotBlank(originalToken) ? originalToken : newToken;
					}
				}
				else if (LOG.isDebugEnabled())
				{
					LOG.debug("Lock with token [{}] already held for for file [{}].", token, canonicalPath);
				}

				if (StringUtils.isNotBlank(token))
				{
					LOG.info("Acquired lock with token [{}] for file [{}].", token, canonicalPath);
					return true;
				}
				else
				{
					LOG.info("Failed to acquire lock for file [{}].", canonicalPath);
					return false;
				}
			}
			else
			{
				LOG.warn("Request to lock a file which is not a file [{}]", file);
				return false;
			}
		}
		else
		{
			LOG.debug("Lock: DB file Locking is disabled for file [{}]", file);
			return true;
		}
	}

	@Override
	public boolean isLockable(final File file)
	{
		if (isDbLockingEnabled())
		{
			Assert.notNull(file, "file must not be null");

			if (file.isFile())
			{
				// Return true if this VM already has the lock on the file OR it's unlocked in the DB.
				final boolean isLockable = lockCache.containsKey(file) || getDbLock(file) == null;
				LOG.debug("File [{}] is {}.", getCanonicalPath(file), isLockable ? "lockable" : "not lockable");
				return isLockable;
			}
			return false;
		}
		else
		{
			LOG.debug("isLockable: DB file Locking is disabled for file [{}]", file);
			return true;
		}
	}

	@Override
	public void unlock(final File file)
	{
		if (isDbLockingEnabled())
		{
			Assert.notNull(file, "file must not be null");

			if (file.isFile())
			{
				final String canonicalPath = getCanonicalPath(file);
				final String token = lockCache.remove(file);
				if (StringUtils.isNotBlank(token))
				{
					if (releaseDbLock(token))
					{
						LOG.info("Released lock with token [{}] for file [{}].", token, canonicalPath);
					}
					else
					{
						LOG.error("Unable to release lock with token [{}] for file [{}].", token, canonicalPath);
					}
				}
				else
				{
					LOG.error("Unable to release lock for file [{}]. Unable to find lock token.", canonicalPath);
				}
			}
			else
			{
				LOG.warn("Request to unlock a file which is not a file [{}]", file);
			}
		}
		else
		{
			LOG.debug("unlock: DB file Locking is disabled for file [{}]", file);
		}
	}

	protected boolean isDbLockingEnabled()
	{
		return getFeatureService().isFeatureOn(FEATURE_DBLOCKING);
	}

	protected FileLockModel getDbLock(final File file)
	{
		final String canonicalPath = getCanonicalPath(file);
		final FileLockModel fileLock = getFileLockDao().getFileLockForPath(canonicalPath);
		if (fileLock != null)
		{
			if (LOG.isDebugEnabled())
			{
				final ZonedDateTime lockDate = ZonedDateTime.ofInstant(fileLock.getCreationtime().toInstant(), ZoneOffset.UTC);
				final String lockDateString = DateTimeFormatter.ISO_DATE_TIME.format(lockDate);
				LOG.debug("File [{}] was DB locked by node [{}] at [{}] with token [{}].", canonicalPath, fileLock.getHybrisNode(), lockDateString, fileLock.getToken());
			}

			// Check to see if the lock has expired.  If it has, remove the lock record and return null to indicate that no lock is currently held.
			final ZonedDateTime expiryDate = ZonedDateTime.ofInstant(fileLock.getExpiryDate().toInstant(), ZoneOffset.UTC);
			if (ZonedDateTime.now(ZoneOffset.UTC).compareTo(expiryDate) > 0)
			{
				if (LOG.isWarnEnabled())
				{
					final String expiryDateString = DateTimeFormatter.ISO_DATE_TIME.format(expiryDate);
					LOG.warn("DB lock with token [{}] expired at [{}].", fileLock.getToken(), expiryDateString);
				}
				releaseDbLock(fileLock.getToken());
				return null;
			}
			else
			{
				return fileLock;
			}
		}
		else
		{
			LOG.debug("File [{}] is not DB locked.", canonicalPath);
			return null;
		}
	}

	protected String createDbLock(final File file)
	{
		final FileLockModel lock = createNewLock(file);
		try
		{
			getModelService().save(lock);
		}
		catch (final ModelSavingException ex)
		{
			LOG.debug("Unable to create DB lock for file [{}].", getCanonicalPath(file), ex);
			return null;
		}
		return lock.getToken();
	}

	protected boolean releaseDbLock(final String token)
	{
		final FileLockModel fileLock = getFileLockDao().getFileLockForToken(token);
		if (fileLock != null)
		{
			try
			{
				getModelService().remove(fileLock);
				LOG.debug("Released DB lock with token [{}].", token);
			}
			catch (final ModelRemovalException ex)
			{
				LOG.debug("Unable to release DB lock with token [{}].", token, ex);
			}
			return true;
		}
		LOG.warn("Unable to find DB lock for token [{}].", token);
		return false;
	}

	protected FileLockModel createNewLock(final File file)
	{
		final String canonicalPath = getCanonicalPath(file);
		final String token = createNewLockToken(file);
		final FileLockModel lock = getModelService().create(FileLockModel.class);
		final Date expiryDate = Date.from(ZonedDateTime.now(ZoneOffset.UTC).plus(getLockTtl(), ChronoUnit.MILLIS).toInstant());
		lock.setFilePath(canonicalPath);
		lock.setHybrisNode(Registry.getClusterID());
		lock.setToken(token);
		lock.setExpiryDate(expiryDate);
		return lock;
	}

	protected String createNewLockToken(@SuppressWarnings("UnusedParameters") final File file)
	{
		return UUID.randomUUID().toString();
	}

	protected String getCanonicalPath(final File fileToLock)
	{
		try
		{
			return fileToLock.getCanonicalPath();
		}
		catch (final IOException e)
		{
			LOG.error("Unable to get canonical path for file [{}].", fileToLock, e);
			throw new MessagingException("Unable to get canonical path for file [" + fileToLock + "].", e);
		}
	}

	protected FileLockDao getFileLockDao()
	{
		return fileLockDao;
	}

	@Required
	public void setFileLockDao(final FileLockDao fileLockDao)
	{
		this.fileLockDao = fileLockDao;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected int getLockTtl()
	{
		return lockTtl;
	}

	@Required
	public void setLockTtl(final int lockTtl)
	{
		this.lockTtl = lockTtl;
	}

	protected FeatureService getFeatureService()
	{
		return featureService;
	}

	@Required
	public void setFeatureService(final FeatureService featureService)
	{
		this.featureService = featureService;
	}
}
