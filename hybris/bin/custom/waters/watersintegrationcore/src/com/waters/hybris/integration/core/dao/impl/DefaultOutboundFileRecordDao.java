package com.waters.hybris.integration.core.dao.impl;

import com.waters.hybris.integration.core.dao.OutboundFileRecordDao;
import com.waters.hybris.integration.core.enums.FileProcessingStatus;
import com.waters.hybris.integration.core.enums.FileType;
import com.waters.hybris.integration.core.model.record.OutboundFileRecordModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultOutboundFileRecordDao extends DefaultGenericDao<OutboundFileRecordModel> implements OutboundFileRecordDao
{
	private static final String FIND_ALL_CREATED = "SELECT {" + OutboundFileRecordModel.PK + "} " +
			"FROM {" + OutboundFileRecordModel._TYPECODE + "} " +
			"WHERE {" + OutboundFileRecordModel.STATUS + "} = ?status " +
			"AND {" + OutboundFileRecordModel.FILETYPE + "} = ?fileType " +
			"ORDER BY {" + OutboundFileRecordModel.CREATIONTIME + "}";


	public DefaultOutboundFileRecordDao()
	{
		this(OutboundFileRecordModel._TYPECODE);
	}

	protected DefaultOutboundFileRecordDao(final String typecode)
	{
		super(typecode);
	}


	@Override
	public List<OutboundFileRecordModel> findAllCreated(final FileType fileType, final int batchSize)
	{
		Assert.notNull(fileType, "fileType cannot be null");

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ALL_CREATED);
		final Map<String, Object> params = new HashMap<>();
		params.put("status", FileProcessingStatus.PENDING);
		params.put("fileType", fileType);
		query.addQueryParameters(params);
		query.setCount(batchSize);

		final SearchResult<OutboundFileRecordModel> results = getFlexibleSearchService().search(query);

		return results.getResult();
	}
}
