package com.waters.hybris.integration.core.file.filter;

import com.waters.hybris.integration.core.dao.FileImportRecordDao;
import com.waters.hybris.integration.core.enums.FileType;
import com.waters.hybris.integration.core.file.provider.FileHashProvider;
import org.fest.util.Collections;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import java.io.File;

/**
 * @author Tom Greasley (tom@neoworks.com)
 *
 * A filter to reject files that we have already seen, based on a hash of the file contents.
 */
public class FileHashFilter implements FileFilter
{
	private FileImportRecordDao fileImportRecordDao;
	private FileHashProvider fileHashProvider;
	private FileType fileType;

	@Override
	public boolean accept(final File file)
	{
		Assert.notNull(file, "file cannot be null");
		final String fileHash = getFileHashProvider().getFileHash(file);
		return Collections.isEmpty(getFileImportRecordDao().findByHash(getFileType(), fileHash));
	}

	@Required
	protected FileImportRecordDao getFileImportRecordDao()
	{
		return fileImportRecordDao;
	}

	@Required
	public void setFileImportRecordDao(final FileImportRecordDao fileImportRecordDao)
	{
		this.fileImportRecordDao = fileImportRecordDao;
	}

	protected FileHashProvider getFileHashProvider()
	{
		return fileHashProvider;
	}

	@Required
	public void setFileHashProvider(final FileHashProvider fileHashProvider)
	{
		this.fileHashProvider = fileHashProvider;
	}

	protected FileType getFileType()
	{
		return fileType;
	}

	@Required
	public void setFileType(final FileType fileType)
	{
		this.fileType = fileType;
	}
}
