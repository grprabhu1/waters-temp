package com.waters.hybris.integration.core.dao.impl;

import com.waters.hybris.integration.core.dao.FileImportRecordDao;
import com.waters.hybris.integration.core.enums.FileProcessingStatus;
import com.waters.hybris.integration.core.enums.FileType;
import com.waters.hybris.integration.core.model.record.FileImportRecordModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultFileImportRecordDao extends DefaultGenericDao<FileImportRecordModel> implements FileImportRecordDao
{
	private static final String FIND_ALL_BY_STATUS = "SELECT {" + FileImportRecordModel.PK + "} " +
			"FROM {" + FileImportRecordModel._TYPECODE + "} " +
			"WHERE {" + FileImportRecordModel.STATUS + "} = ?status " +
			"AND {" + FileImportRecordModel.FILETYPE + "} = ?fileType " +
			"ORDER BY {" + FileImportRecordModel.FILEDATE + "} ASC";

	private static final String FIND_ALL_BY_STATUS_OLDER_THAN_DATE = "SELECT {" + FileImportRecordModel.PK + "} " +
			"FROM {" + FileImportRecordModel._TYPECODE + "} " +
			"WHERE {" + FileImportRecordModel.STATUS + "} = ?status " +
			"AND {" + FileImportRecordModel.FILETYPE + "} = ?fileType " +
			"AND {" + FileImportRecordModel.CREATIONTIME + "} <= ?creationtime " +
			"ORDER BY {" + FileImportRecordModel.FILEDATE + "} ASC";

	private static final String FIND_MOST_RECENT_BY_STATUS = "SELECT MAX({" + FileImportRecordModel.FILEDATE + "}) " +
			"FROM {" + FileImportRecordModel._TYPECODE + "} " +
			"WHERE {" + FileImportRecordModel.STATUS + "} = ?status " +
			"AND {" + FileImportRecordModel.FILETYPE + "} = ?fileType ";

	private static final String FIND_BY_HASH = "SELECT {" + FileImportRecordModel.PK + "} " +
			"FROM {" + FileImportRecordModel._TYPECODE + "} " +
			"WHERE {" + FileImportRecordModel.FILEHASH + "} = ?fileHash " +
			"AND {" + FileImportRecordModel.FILETYPE + "} = ?fileType " +
			"ORDER BY {" + FileImportRecordModel.FILEDATE + "} ASC";

	private static final String FIND_BY_NAME = "SELECT {" + FileImportRecordModel.PK + "} " +
			"FROM {" + FileImportRecordModel._TYPECODE + "} " +
			"WHERE {" + FileImportRecordModel.FILENAME + "} = ?fileName " +
			"AND {" + FileImportRecordModel.FILETYPE + "} = ?fileType " +
			"ORDER BY {" + FileImportRecordModel.FILEDATE + "} ASC";

	public DefaultFileImportRecordDao()
	{
		this(FileImportRecordModel._TYPECODE);
	}

	protected DefaultFileImportRecordDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<FileImportRecordModel> findAllPendingOlderThan(final FileType fileType, final Date date)
	{
		Assert.notNull(fileType, "File type can not be null");
		Assert.notNull(date, "Date can not be null");

		final Map<String, Object> params = new HashMap<>();
		params.put(FileImportRecordModel.STATUS, FileProcessingStatus.PENDING);
		params.put(FileImportRecordModel.FILETYPE, fileType);
		params.put(FileImportRecordModel.CREATIONTIME, date);

		final SearchResult<FileImportRecordModel> results = getFlexibleSearchService().search(FIND_ALL_BY_STATUS_OLDER_THAN_DATE, params);
		return results.getResult();
	}

	@Override
	public List<FileImportRecordModel> findAllPending(final FileType fileType)
	{
		Assert.notNull(fileType, "File type can not be null");
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ALL_BY_STATUS);
		final Map<String, Object> params = new HashMap<>();
		params.put("status", FileProcessingStatus.PENDING);
		params.put("fileType", fileType);
		query.addQueryParameters(params);
		final SearchResult<FileImportRecordModel> results = getFlexibleSearchService().search(query);
		return results.getResult();
	}

	@Override
	public Date findLastSuccess(final FileType fileType)
	{
		Assert.notNull(fileType, "File type can not be null");
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_MOST_RECENT_BY_STATUS);
		final Map<String, Object> params = new HashMap<>();
		params.put("status", FileProcessingStatus.DONE);
		params.put("fileType", fileType);
		query.addQueryParameters(params);
		query.setResultClassList(Collections.singletonList(Date.class));
		final SearchResult<Date> searchResult = getFlexibleSearchService().search(query);
		final List<Date> results = searchResult.getResult();
		return results.isEmpty() ? null : results.iterator().next();
	}

	@Override
	public List<FileImportRecordModel> findByHash(final FileType fileType, final String fileHash)
	{
		Assert.notNull(fileHash, "File hash can not be null");
		Assert.notNull(fileType, "File type can not be null");
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_BY_HASH);
		final Map<String, Object> params = new HashMap<>();
		params.put("fileHash", fileHash);
		params.put("fileType", fileType);
		query.addQueryParameters(params);
		final SearchResult<FileImportRecordModel> results = getFlexibleSearchService().search(query);
		return results.getResult();
	}

	@Override
	public List<FileImportRecordModel> findByName(final FileType fileType, final String fileName)
	{
		Assert.notNull(fileName, "fileName can not be null");
		Assert.notNull(fileType, "File type can not be null");
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_BY_NAME);
		final Map<String, Object> params = new HashMap<>();
		params.put("fileName", fileName);
		params.put("fileType", fileType);
		query.addQueryParameters(params);
		final SearchResult<FileImportRecordModel> results = getFlexibleSearchService().search(query);
		return results.getResult();
	}
}
