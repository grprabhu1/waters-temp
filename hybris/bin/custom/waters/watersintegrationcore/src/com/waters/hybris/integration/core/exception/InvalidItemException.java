package com.waters.hybris.integration.core.exception;

public class InvalidItemException extends RuntimeException
{
	public InvalidItemException(final String message)
	{
		super(message);
	}
}
