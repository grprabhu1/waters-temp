package com.waters.hybris.integration.core.context;

import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.util.BatchCommitter;
import com.waters.hybris.integration.core.enums.FileProcessingStatus;
import com.waters.hybris.integration.core.model.record.FileImportRecordModel;

/**
 * Not thread safe.
 */
public class MediaImportContext extends ImportContext<FileImportRecordModel>
{
	public MediaImportContext(final FileImportRecordModel importRecordModel, final AuditContext auditContext, final BatchCommitter batchCommitter)
	{
		super(importRecordModel, auditContext, batchCommitter);
	}

	public String getMediaCode()
	{
		return getInput() != null ? getInput().getMedia().getCode() : null;
	}

	@Override
	protected String generateSuccessMessage()
	{
		return String.format("Imported media with code [%s]. Processed: [%d]. Success: [%d]. Errors: [%d].", getMediaCode(), getTotalProcessed(), getTotalSuccess(), getTotalError());
	}

	@Override
	protected String generateErrorMessage()
	{
		return String.format("Failed to import media with code [%s]. Processed: [%d]. Success: [%d]. Errors: [%d].", getMediaCode(), getTotalProcessed(), getTotalSuccess(), getTotalError());
	}

	@Override
	public String getSource()
	{
		return getMediaCode();
	}

	@Override
	public void handleInputParsingException()
	{
		this.getInput().setStatus(FileProcessingStatus.WARNING);
	}

	@Override
	public void handleNoDataToProcessError()
	{
		this.getInput().setStatus(FileProcessingStatus.WARNING);
	}
}
