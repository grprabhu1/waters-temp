package com.waters.hybris.integration.core.etl.pipeline.impl;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.google.common.base.Stopwatch;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.neoworks.hybris.etl.pipeline.impl.DefaultEtlPipeline;
import org.springframework.beans.factory.annotation.Required;

public class WatersMetricsEtlPipeline<S,T> extends DefaultEtlPipeline<S,T>
{

	private MetricRegistry metricRegistry;
	private String jobMetricName;
	private String itemMetricName;

	@Override
	public void execute(final EtlContext context) throws EtlTransformationException
	{
		final Timer timer = getMetricRegistry().timer(getJobMetricName());
		try (final Timer.Context timerContext = timer.time())
		{
			super.execute(context);
		}
	}

	@Override
	protected boolean doPipelineTransformation(final Stopwatch totalT, final Stopwatch incT, final Stopwatch totalC, final Stopwatch incC, final S item, final long processedCount)
	{
		final Timer timer = getMetricRegistry().timer(getItemMetricName());
		try (final Timer.Context timerContext = timer.time())
		{
			return super.doPipelineTransformation(totalT, incT, totalC, incC, item, processedCount);
		}
	}

	public MetricRegistry getMetricRegistry()
	{
		return metricRegistry;
	}
	@Required
	public void setMetricRegistry(final MetricRegistry metricRegistry)
	{
		this.metricRegistry = metricRegistry;
	}

	protected String getJobMetricName()
	{
		return jobMetricName;
	}

	protected String getItemMetricName()
	{
		return itemMetricName;
	}

	@Required
	public void setMetricName(final String metricName)
	{
		this.itemMetricName = String.format("etl.%s.singleItemImport", metricName);
		this.jobMetricName = String.format("etl.%s", metricName);
	}
}
