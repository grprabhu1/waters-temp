package com.waters.hybris.integration.core.etl.consumer;

import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;

public class CollectionCsvDataConsumer<E> extends CsvDataConsumer<Collection<? extends E>>
{

	private static final Logger LOG = LoggerFactory.getLogger(CollectionCsvDataConsumer.class);

	@Override
	public void put(final Collection<? extends E> data)
	{
		try
		{
			for (final E line : data)
			{
				getWriter().write(line);
			}
			setAtLeastOneItem(true);
			getConsumerTrackingStrategy().markAsConsumed(getEtlContext());
		}
		catch (final IOException e)
		{
			LOG.error("Could not write data to file", e);
		}
		catch (final EtlTransformationException e)
		{
			LOG.error("Failed to mark item as consumed.", e);
		}
	}
}
