package com.waters.hybris.integration.core.maintenance.batch.strategy.impl;

import com.google.common.base.Stopwatch;
import com.waters.hybris.integration.core.maintenance.batch.strategy.FlushingStrategy;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

public class RemoveFlushingStrategy implements FlushingStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(RemoveFlushingStrategy.class);

	@Override
	public int flush(final Collection<Object> batch, final boolean detachAfterCommit, final ModelService modelService)
	{
		try
		{
			if (!batch.isEmpty())
			{
				final Stopwatch e = Stopwatch.createStarted();
				LOG.info("Attempting to commit [{}] items. Detaching: [{}]", Integer.valueOf(batch.size()), Boolean.valueOf(detachAfterCommit));
				modelService.removeAll(batch);

				if (detachAfterCommit)
				{
					detachAll(batch, modelService);
				}

				e.stop();
				LOG.info("Successfully committed [{}] items in [{}]ms.", Integer.valueOf(batch.size()), Long.valueOf(e.elapsed(TimeUnit.MILLISECONDS)));
				return batch.size();
			}
		}
		catch (final ModelRemovalException ex)
		{
			LOG.error("Error removing batch.", ex);
		}

		return 0;
	}

	protected void detachAll(final Collection<Object> toDetach, final ModelService modelService)
	{
		toDetach.stream().forEach(modelService::detach);
	}
}
