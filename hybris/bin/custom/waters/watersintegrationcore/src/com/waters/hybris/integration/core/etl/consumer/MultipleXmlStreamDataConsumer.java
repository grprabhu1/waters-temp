package com.waters.hybris.integration.core.etl.consumer;

import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class MultipleXmlStreamDataConsumer<T, K> extends AbstractXmlStreamDataConsumer<T, K>
{
	private static final Logger LOG = LoggerFactory.getLogger(MultipleXmlStreamDataConsumer.class);

	@Override
	public void init(final EtlContext etlContext) throws EtlValidationException
	{
		super.init(etlContext);
	}

	@Override
	public void start() throws EtlTransformationException
	{
		// Empty
	}

	@Override
	public void put(final T item)
	{
		Assert.notNull(item, "item cannot be null");
		try
		{
			final File file = File.createTempFile(getTemporaryFilePrefix(), getTemporaryFileSuffix());
			XMLStreamWriter xmlStreamWriter = null;

			try (final FileOutputStream fileOutputStream = FileUtils.openOutputStream(file, false))
			{
				xmlStreamWriter = createStreamWriter(fileOutputStream);

				xmlStreamWriter.writeStartDocument();

				// If necessary, add the DOCTYPE declaration here by calling xmlStreamWriter.writeDTD("<!DOCTYPE {...}>")
				if (StringUtils.isNotBlank(getRootElementName()))
				{
					xmlStreamWriter.writeStartElement(getRootElementName());
				}

				getMarshaller().marshal(item, xmlStreamWriter);

				if (StringUtils.isNotBlank(getRootElementName()))
				{
					xmlStreamWriter.writeEndDocument();
				}

				xmlStreamWriter.flush();
				xmlStreamWriter.close();
				getEtlContext().getParameters().put(getFileParamKey(), file.getAbsolutePath());

				getMediaActionList().execute(getEtlContext());
				getConsumerTrackingStrategy().markAsComplete(getEtlContext());
			}
			catch (final JAXBException | XMLStreamException e)
			{
				closeStream(xmlStreamWriter);
				LOG.error("Failed to consume item.", e);
				markAsError("Failed to consume item.", e);
			}
			catch (final EtlTransformationException e)
			{
				LOG.error("Failed to create media.", e);
				markAsError("Failed to create media.", e);
			}
		}
		catch (final IOException e)
		{
			LOG.error("Failed to consume item.", e);
			markAsError("Failed to consume item.", e);
		}
	}

	@Override
	public void end(final boolean b) throws EtlTransformationException
	{
		// Empty
	}

	protected void closeStream(final XMLStreamWriter xmlStreamWriter)
	{
		if (xmlStreamWriter != null)
		{
			try
			{
				xmlStreamWriter.close();
			}
			catch (final XMLStreamException e)
			{
				LOG.error("Could not close XML stream writer.", e);
			}
		}
	}
}
