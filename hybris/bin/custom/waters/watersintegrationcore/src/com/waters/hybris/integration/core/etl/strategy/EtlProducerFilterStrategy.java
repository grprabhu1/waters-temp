package com.waters.hybris.integration.core.etl.strategy;

import java.util.Collection;

public interface EtlProducerFilterStrategy<T>
{
	Collection<T> filter(Collection<T> results);
}
