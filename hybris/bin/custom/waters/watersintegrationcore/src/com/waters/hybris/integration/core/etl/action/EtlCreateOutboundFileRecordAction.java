package com.waters.hybris.integration.core.etl.action;

import com.neoworks.hybris.etl.action.EtlAction;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.waters.hybris.integration.core.enums.FileProcessingStatus;
import com.waters.hybris.integration.core.enums.FileType;
import com.waters.hybris.integration.core.model.record.OutboundFileRecordModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;

/**
 * @author Tom Greasley (tom@neoworks.com)
 */
public class EtlCreateOutboundFileRecordAction implements EtlAction
{

	private ModelService modelService;
	private FileType fileType;
	private String mediaParamKey;

	@Override
	public void execute(final EtlContext context) throws EtlTransformationException
	{
		final MediaModel media = (MediaModel) context.getParameters().get(getMediaParamKey());
		createOutboundFileRecord(media);
	}

	protected void createOutboundFileRecord(final MediaModel media)
	{
		final OutboundFileRecordModel outboundFileRecord = getModelService().create(OutboundFileRecordModel.class);
		outboundFileRecord.setMedia(media);
		outboundFileRecord.setStatus(FileProcessingStatus.PENDING);
		outboundFileRecord.setFileType(getFileType());
		outboundFileRecord.setFileName(media.getRealFileName());
		getModelService().save(outboundFileRecord);
	}

	public String getMediaParamKey()
	{
		return mediaParamKey;
	}

	@Required
	public void setMediaParamKey(final String mediaParamKey)
	{
		this.mediaParamKey = mediaParamKey;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected FileType getFileType()
	{
		return fileType;
	}

	@Required
	public void setFileType(final FileType fileType)
	{
		this.fileType = fileType;
	}
}
