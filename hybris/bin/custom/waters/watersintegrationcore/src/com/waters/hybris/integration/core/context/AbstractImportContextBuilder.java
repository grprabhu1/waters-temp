package com.waters.hybris.integration.core.context;

import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.audit.service.AuditService;
import com.waters.hybris.integration.core.strategy.ModelServiceSelectionStrategy;
import org.springframework.beans.factory.annotation.Required;

public abstract class AbstractImportContextBuilder
{
	private AuditService auditService;
	private ModelServiceSelectionStrategy<String> modelServiceSelectionStrategy;
	private SystemArea systemArea;
	private boolean useBatchCommitter = true;
	private int batchSize = 100;
	private String useTransactionalModelServiceConfigKey;

	protected void initializeModelService(final ImportContext<?> context)
	{
		context.setUseTransactionalModelService(getModelServiceSelectionStrategy().useTransactionalModelService(getUseTransactionalModelServiceConfigKey()));
	}

	protected AuditService getAuditService()
	{
		return auditService;
	}

	@Required
	public void setAuditService(final AuditService auditService)
	{
		this.auditService = auditService;
	}

	protected int getBatchSize()
	{
		return batchSize;
	}

	public void setBatchSize(final int batchSize)
	{
		this.batchSize = batchSize;
	}

	protected SystemArea getSystemArea()
	{
		return systemArea;
	}

	@Required
	public void setSystemArea(final SystemArea systemArea)
	{
		this.systemArea = systemArea;
	}

	protected boolean isUseBatchCommitter()
	{
		return useBatchCommitter;
	}

	public void setUseBatchCommitter(final boolean useBatchCommitter)
	{
		this.useBatchCommitter = useBatchCommitter;
	}

	protected String getUseTransactionalModelServiceConfigKey()
	{
		return useTransactionalModelServiceConfigKey;
	}

	@Required
	public void setUseTransactionalModelServiceConfigKey(final String useTransactionalModelServiceConfigKey)
	{
		this.useTransactionalModelServiceConfigKey = useTransactionalModelServiceConfigKey;
	}

	protected ModelServiceSelectionStrategy<String> getModelServiceSelectionStrategy()
	{
		return modelServiceSelectionStrategy;
	}

	@Required
	public void setModelServiceSelectionStrategy(final ModelServiceSelectionStrategy<String> modelServiceSelectionStrategy)
	{
		this.modelServiceSelectionStrategy = modelServiceSelectionStrategy;
	}

}
