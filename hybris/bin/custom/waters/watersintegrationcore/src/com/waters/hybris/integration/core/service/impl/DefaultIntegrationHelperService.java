package com.waters.hybris.integration.core.service.impl;

import com.waters.hybris.integration.core.service.IntegrationHelperService;
import com.waters.hybris.integration.core.store.dao.IntegrationBaseStoreDao;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.store.BaseStoreModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

/**
 * @author ramana
 */
public class DefaultIntegrationHelperService implements IntegrationHelperService
{
	protected static final String PRODUCT_CATALOG = "ProductCatalog";
	private static final Logger LOG = LoggerFactory.getLogger(DefaultIntegrationHelperService.class);
	private IntegrationBaseStoreDao integrationBaseStoreDao;
	private CatalogVersionService catalogVersionService;

	@Override
	public Collection<CatalogVersionModel> getStagedProductCatalogVersionForStore(final BaseStoreModel baseStore)
	{
		validateParameterNotNull(baseStore, "Parameter baseStore must not be null");

		final Set<CatalogVersionModel> catalogVersions = new HashSet<>();
		for (final CatalogModel catalog : baseStore.getCatalogs())
		{
			if (catalog.getId().contains(PRODUCT_CATALOG))
			{
				try
				{
					catalogVersions.add(
							getCatalogVersionService().getCatalogVersion(catalog
									.getId(), CatalogManager.OFFLINE_VERSION));
				}
				catch (final UnknownIdentifierException e)
				{
					LOG.error("Error getting catalog versions for base store [" + baseStore.getUid() + "]", e);
				}
			}
		}

		return catalogVersions;
	}

	protected IntegrationBaseStoreDao getIntegrationBaseStoreDao()
	{
		return integrationBaseStoreDao;
	}

	@Required
	public void setIntegrationBaseStoreDao(final IntegrationBaseStoreDao integrationBaseStoreDao)
	{
		this.integrationBaseStoreDao = integrationBaseStoreDao;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}
}
