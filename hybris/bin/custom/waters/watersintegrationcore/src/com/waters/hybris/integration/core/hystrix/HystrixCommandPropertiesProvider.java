package com.waters.hybris.integration.core.hystrix;

import com.netflix.hystrix.HystrixCommand;

public interface HystrixCommandPropertiesProvider
{
	HystrixCommand.Setter createCommandProperties(final String commandKey);
}
