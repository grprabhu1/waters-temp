package com.waters.hybris.integration.core.exception;

public class ImportAbortedException extends RuntimeException
{
	public ImportAbortedException(final String message)
	{
		super(message);
	}
}
