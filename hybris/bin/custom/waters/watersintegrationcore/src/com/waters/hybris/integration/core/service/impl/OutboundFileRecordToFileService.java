package com.waters.hybris.integration.core.service.impl;

import com.google.common.io.ByteSink;
import com.google.common.io.Files;
import com.waters.hybris.integration.core.model.record.OutboundFileRecordModel;
import com.waters.hybris.integration.core.service.OutboundFileRecordService;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class OutboundFileRecordToFileService implements OutboundFileRecordService
{
	@Override
	public void writeFromStream(final InputStream stream, final String destination, final OutboundFileRecordModel outboundFileRecord) throws IOException
	{
		Assert.notNull(stream, "stream cannot be null");
		Assert.notNull(destination, "destination cannot be null");
		Assert.notNull(outboundFileRecord, "outboundFileRecord cannot be null");

		ensureDestination(destination);

		final String mediaFileName = outboundFileRecord.getMedia().getRealFileName();
		Assert.notNull(mediaFileName, "mediaFileName cannot be null");

		final File outputFile = new File(getFileName(destination, mediaFileName));
		final ByteSink byteSink = Files.asByteSink(outputFile);
		byteSink.writeFrom(stream);
	}

	protected void ensureDestination(final String destinationPath)
	{
		final File file = new File(destinationPath);
		if (!(file.exists() || file.mkdirs()))
		{
			throw new RuntimeException("Unable to create output directory [" + destinationPath + "]");
		}
	}

	protected String getFileName(final String folder, final String fileName)
	{
		final String file = fileName.startsWith(File.separator) ? StringUtils.removeStart(fileName, File.separator) : fileName;
		return folder.endsWith(File.separator) ? folder + file : folder + File.separator + file;
	}
}
