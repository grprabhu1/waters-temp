package com.waters.hybris.integration.core.job;

import com.codahale.metrics.annotation.Timed;
import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.audit.service.AuditService;
import com.waters.hybris.integration.core.dao.OutboundFileRecordDao;
import com.waters.hybris.integration.core.enums.FileProcessingStatus;
import com.waters.hybris.integration.core.enums.FileType;
import com.waters.hybris.integration.core.model.job.OutboundFileRecordProcessCronJobModel;
import com.waters.hybris.integration.core.model.record.OutboundFileRecordModel;
import com.waters.hybris.integration.core.service.OutboundFileRecordService;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class OutboundFileRecordProcessJob extends AbstractJobPerformable<OutboundFileRecordProcessCronJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(OutboundFileRecordProcessJob.class);

	private MediaService mediaService;
	private AuditService auditService;
	private Map<FileType, OutboundFileRecordService> outboundFileRecordServiceMap;
	private OutboundFileRecordService defaultOutboundFileRecordService;
	private OutboundFileRecordDao outboundFileRecordDao;
	private SystemArea systemArea;
	private int batchSize = 100;

	@Override
	public boolean isAbortable()
	{
		return true;
	}

	@SuppressWarnings("squid:S1166")
	@SuppressFBWarnings(value = "REC_CATCH_EXCEPTION", justification = "We want to make sure that an error does not prevent further processing")
	@Override
	@Timed(name = "integration.core.outboundFeedCreate.OutboundFileRecordProcessJob", absolute = true)
	public PerformResult perform(final OutboundFileRecordProcessCronJobModel cronJob)
	{
		final FileType fileType = cronJob.getFileType();
		final AuditContext auditContext = getAuditService().beginAudit(getSystemArea(), null, "Processing Outbound File Records of type [{}]", fileType.getCode());

		List<OutboundFileRecordModel> recordsToProcess;
		int errorCount = 0;
		while (CollectionUtils.isNotEmpty(recordsToProcess = getOutboundFileRecordDao().findAllCreated(fileType, getBatchSize())))
		{
			if (clearAbortRequestedIfNeeded(cronJob))
			{
				final String message = "Outbound File Record processing has been aborted!";
				LOG.warn(message);
				auditContext.endAudit(Status.WARN, message);
				return new PerformResult(CronJobResult.UNKNOWN, CronJobStatus.ABORTED);
			}
			else
			{
				for (final OutboundFileRecordModel recordToProcess : recordsToProcess)
				{
					errorCount = processRecord(cronJob, fileType, auditContext, recordsToProcess, errorCount, recordToProcess);
				}
			}
		}

		auditContext.endAudit(errorCount <= 0 ? Status.SUCCESS : Status.WARN, null);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	@SuppressWarnings("squid:S1166")
	protected int processRecord(final OutboundFileRecordProcessCronJobModel cronJob, final FileType fileType, final AuditContext auditContext, final List<OutboundFileRecordModel> recordsToProcess, int errorCount, final OutboundFileRecordModel recordToProcess)
	{
		try
		{
			final boolean ret = processOutboundFileRecord(recordToProcess, auditContext, fileType, cronJob.getDestinationPath());
			if (!ret)
			{
				errorCount++;
			}
		}
		catch (final Exception ex)
		{
			LOG.warn("Failed to process file record [{}]", recordsToProcess);
		}
		return errorCount;
	}

	@SuppressFBWarnings(value = "REC_CATCH_EXCEPTION", justification = "We want to make sure that an error does not prevent further processing")
	protected boolean processOutboundFileRecord(final OutboundFileRecordModel recordToProcess, final AuditContext auditContext, final FileType fileType, final String destinationPath)
	{
		final Date startTime = Calendar.getInstance().getTime();
		final MediaModel media = recordToProcess.getMedia();
		if (media != null)
		{
			final String fileName = StringUtils.isNotBlank(media.getRealFileName()) ? media.getRealFileName() : media.getPk().toString();

			LOG.debug("Processing Outbound File Record: fileType [{}] PK [{}] file [{}]", fileType, recordToProcess.getPk(), fileName);
			recordToProcess.setStatus(FileProcessingStatus.PROCESSING);
			recordToProcess.setProcessingDate(startTime);
			getModelService().save(recordToProcess);

			try (final InputStream streamFromMedia = getMediaService().getStreamFromMedia(media))
			{
				getOutboundFileRecordService(fileType).writeFromStream(streamFromMedia, destinationPath, recordToProcess);
				recordToProcess.setStatus(FileProcessingStatus.DONE);
			}
			catch (final Exception ex)
			{
				final String errorMessage = String.format("Failed to process Outbound File Record: fileType [%s] PK [%s] file [%s]", fileType, recordToProcess.getPk(), fileName);
				LOG.error(errorMessage, ex);
				auditContext.addMessage(errorMessage);
				recordToProcess.setStatus(FileProcessingStatus.ERROR);
			}
		}
		else
		{
			final String errorMessage = String.format("Failed to process Outbound File Record - it has no media: fileType [%s] PK [%s]", fileType, recordToProcess.getPk());
			LOG.error(errorMessage);
			auditContext.addMessage(errorMessage);
			recordToProcess.setStatus(FileProcessingStatus.ERROR);
		}

		LOG.debug("Processed Outbound File Record: fileType [{}] PK [{}]", fileType, recordToProcess.getPk());
		recordToProcess.setDuration(Calendar.getInstance().getTime().getTime() - startTime.getTime());
		getModelService().save(recordToProcess);

		return !FileProcessingStatus.ERROR.equals(recordToProcess.getStatus());
	}

	protected OutboundFileRecordService getOutboundFileRecordService(final FileType fileType)
	{
		return getOutboundFileRecordServiceMap().getOrDefault(fileType, getDefaultOutboundFileRecordService());
	}

	public MediaService getMediaService()
	{
		return mediaService;
	}

	@Required
	public void setMediaService(final MediaService mediaService)
	{
		this.mediaService = mediaService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	protected AuditService getAuditService()
	{
		return auditService;
	}

	@Required
	public void setAuditService(final AuditService auditService)
	{
		this.auditService = auditService;
	}

	protected OutboundFileRecordDao getOutboundFileRecordDao()
	{
		return outboundFileRecordDao;
	}

	@Required
	public void setOutboundFileRecordDao(final OutboundFileRecordDao outboundFileRecordDao)
	{
		this.outboundFileRecordDao = outboundFileRecordDao;
	}

	protected SystemArea getSystemArea()
	{
		return systemArea;
	}

	@Required
	public void setSystemArea(final SystemArea systemArea)
	{
		this.systemArea = systemArea;
	}

	protected int getBatchSize()
	{
		return batchSize;
	}

	public void setBatchSize(final int batchSize)
	{
		this.batchSize = batchSize;
	}

	protected Map<FileType, OutboundFileRecordService> getOutboundFileRecordServiceMap()
	{
		return outboundFileRecordServiceMap;
	}

	@Required
	public void setOutboundFileRecordServiceMap(final Map<FileType, OutboundFileRecordService> outboundFileRecordServiceMap)
	{
		this.outboundFileRecordServiceMap = outboundFileRecordServiceMap;
	}

	protected OutboundFileRecordService getDefaultOutboundFileRecordService()
	{
		return defaultOutboundFileRecordService;
	}

	@Required
	public void setDefaultOutboundFileRecordService(final OutboundFileRecordService defaultOutboundFileRecordService)
	{
		this.defaultOutboundFileRecordService = defaultOutboundFileRecordService;
	}
}
