package com.waters.hybris.integration.core.strategy;

import java.util.Date;
import java.util.TimeZone;

public interface DateConversionStrategy<RESULT>
{

	/**
	 * Convert a {@link Date} into the RESULT type.
	 *
	 * @param date the date to convert
	 * @return the result
	 */
	RESULT convert(final Date date);

	/**
	 * Convert a {@link Date} into the RESULT type taking into account the time zone.
	 *
	 * @param date     the date to convert
	 * @param timeZone the timezone
	 * @return the result
	 */
	RESULT convert(final Date date, TimeZone timeZone);

}
