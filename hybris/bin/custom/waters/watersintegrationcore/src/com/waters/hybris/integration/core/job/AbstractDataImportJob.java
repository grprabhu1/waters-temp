package com.waters.hybris.integration.core.job;

import com.waters.hybris.integration.core.context.JobContext;
import com.waters.hybris.integration.core.exception.ImportAbortedException;
import com.waters.hybris.integration.core.service.impl.DefaultDataProcessor;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

@SuppressWarnings("PMD.LoggerIsNotStaticFinal")
public abstract class AbstractDataImportJob extends AbstractJobPerformable<CronJobModel>
{
	protected final Logger LOG = LoggerFactory.getLogger(getClass());

	private DefaultDataProcessor dataProcessor;

	@Override
	public boolean isAbortable()
	{
		return true;
	}

	@SuppressWarnings("squid:S1166")
	protected PerformResult performInternal(final CronJobModel cronJobModel)
	{
		final JobContext jobContext = new JobContext(cronJobModel, modelService);
		boolean success = false;
		try
		{
			getDataProcessor().process(null, jobContext);
			success = true;
		}
		catch (final ImportAbortedException e)
		{
			if (clearAbortRequestedIfNeeded(cronJobModel))
			{
				LOG.warn("Feed import job has been aborted!");
				return new PerformResult(CronJobResult.UNKNOWN, CronJobStatus.ABORTED);
			}
		}
		catch (final Exception e)
		{
			LOG.error("Failed to import data", e);
		}

		return new PerformResult(success ? CronJobResult.SUCCESS : CronJobResult.FAILURE, CronJobStatus.FINISHED);
	}

	protected DefaultDataProcessor getDataProcessor()
	{
		return dataProcessor;
	}

	@Required
	public void setDataProcessor(final DefaultDataProcessor dataProcessor)
	{
		this.dataProcessor = dataProcessor;
	}
}
