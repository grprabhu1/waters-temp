package com.waters.hybris.integration.core.etl.strategy.impl;

import com.neoworks.hybris.etl.context.EtlContext;
import com.waters.hybris.integration.core.strategy.ModelServiceSelectionStrategy;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;

public class DefaultEtlModelServiceSelectionStrategy implements ModelServiceSelectionStrategy<EtlContext>
{
	protected static final String DEFAULT_TRANSACTIONAL_MODEL_SERVICE_PARAMKEY = "useTransactionalModelService";
	protected static final boolean DEFAULT_TRANSACTIONAL_MODEL_SERVICE = false;

	private ModelService modelService;
	private ModelService transactionalModelService;
	private String transactionalModelServiceParamKey = DEFAULT_TRANSACTIONAL_MODEL_SERVICE_PARAMKEY;

	@Override
	public ModelService selectModelService(final EtlContext context)
	{
		if (useTransactionalModelService(context))
		{
			return getTransactionalModelService();
		}
		else
		{
			return getModelService();
		}
	}

	@Override
	public boolean useTransactionalModelService(final EtlContext etlContext)
	{
		return Boolean.parseBoolean(etlContext.getParameters().getOrDefault(getTransactionalModelServiceParamKey(), DEFAULT_TRANSACTIONAL_MODEL_SERVICE).toString());
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected ModelService getTransactionalModelService()
	{
		return transactionalModelService;
	}

	@Required
	public void setTransactionalModelService(final ModelService transactionalModelService)
	{
		this.transactionalModelService = transactionalModelService;
	}

	protected String getTransactionalModelServiceParamKey()
	{
		return transactionalModelServiceParamKey;
	}

	public void setTransactionalModelServiceParamKey(final String transactionalModelServiceParamKey)
	{
		this.transactionalModelServiceParamKey = transactionalModelServiceParamKey;
	}
}
