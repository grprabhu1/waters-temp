package com.waters.hybris.integration.core.etl.producer;

import com.google.common.primitives.Ints;
import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import com.neoworks.hybris.etl.producers.DataProducer;
import com.waters.hybris.integration.core.etl.strategy.EtlProducerFilterStrategy;
import com.waters.hybris.integration.core.strategy.ModelServiceSelectionStrategy;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @author Tom Greasley (tom@neoworks.com)
 */
@SuppressWarnings("unused")
public class BatchedFlexibleSearchDataProducer implements DataProducer<ItemModel>
{
	protected static final String DEFAULT_BATCH_SIZE_PARAMKEY = "batchSize";
	protected static final int DEFAULT_BATCH_SIZE = 100;
	protected static final String DEFAULT_DETACH_MODELS_PARAMKEY = "detachModels";
	protected static final boolean DEFAULT_DETACH_MODELS = true;
	protected static final String DEFAULT_KEYS_QUERY_PARAM = "keys";
	private static final Logger LOG = LoggerFactory.getLogger(BatchedFlexibleSearchDataProducer.class);
	private static final String MAXIMUM_BATCH_SIZE_PROPERTY = "watersintegrationcore.maximum.etl.batch.size";
	private static final int DEFAULT_MAX_BATCH_SIZE = 999;
	protected Iterator<PK> pkIterator;
	private String batchSizeParamkey = DEFAULT_BATCH_SIZE_PARAMKEY;
	private int batchSize = DEFAULT_BATCH_SIZE;
	private String keysQueryParam = DEFAULT_KEYS_QUERY_PARAM;
	private String detachModelsParamKey = DEFAULT_DETACH_MODELS_PARAMKEY;
	private boolean detachModels = DEFAULT_DETACH_MODELS;
	private FlexibleSearchService flexibleSearchService;
	private EtlProducerFilterStrategy<ItemModel> etlProducerFilterStrategy;
	private ModelService modelService;
	private ModelServiceSelectionStrategy<EtlContext> modelServiceSelectionStrategy;
	private ConfigurationService configurationService;
	private String findItemsQuery;
	private String loadItemsQuery;
	private EtlContext context;
	private BlockingQueue<ItemModel> itemQueue;
	private ItemModel lastItem;
	private SystemArea systemArea = SystemArea.ETL;

	@Override
	public void init(final EtlContext context) throws EtlValidationException
	{
		if (StringUtils.isBlank(getFindItemsQuery()))
		{
			throw new EtlValidationException("Find items query was not set ");
		}
		if (StringUtils.isBlank(getLoadItemsQuery()))
		{
			throw new EtlValidationException("Load items query was not set ");
		}
		this.context = context;
		this.detachModels = findDetachModels(context);
		this.batchSize = initializeBatchSize(context);
		this.itemQueue = new ArrayBlockingQueue<>(batchSize);
		this.modelService = getModelServiceSelectionStrategy().selectModelService(context);
	}

	@Override
	public void start() throws EtlTransformationException
	{
		final Collection<PK> keys = findItems();
		this.pkIterator = keys.iterator();
		LOG.debug("Data producer has been started with {} items.", keys.size());
	}

	@Override
	public void end(final boolean success) throws EtlTransformationException
	{
		if (lastItem != null && detachModels)
		{
			getModelService().detach(lastItem);
		}
		lastItem = null;
		LOG.debug("DataProducer finished");
	}

	@Override
	public ItemModel get()
	{
		if (lastItem != null && detachModels)
		{
			getModelService().detach(lastItem);
		}
		lastItem = null;

		while (itemQueue.isEmpty() && pkIterator != null && pkIterator.hasNext())
		{
			final int capacity = itemQueue.remainingCapacity();
			final Collection<PK> keys = new ArrayList<>(capacity);
			for (int x = 0; x < capacity && pkIterator.hasNext(); x++)
			{
				keys.add(pkIterator.next());
			}

			final Collection<ItemModel> itemModels = filter(loadItems(keys));
			if (!itemModels.isEmpty())
			{
				itemQueue.addAll(itemModels);
				LOG.debug("Data producer has loaded a batch of {} items.", itemModels.size());
			}
		}

		if (!itemQueue.isEmpty())
		{
			lastItem = itemQueue.poll();
			return lastItem;
		}

		return null;
	}

	protected Collection<ItemModel> filter(final List<ItemModel> itemModels)
	{
		if (getEtlProducerFilterStrategy() != null)
		{
			return getEtlProducerFilterStrategy().filter(itemModels);
		}
		return itemModels;
	}

	protected Collection<PK> findItems()
	{
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(getFindItemsQuery(), getContext().getParameters());
		searchQuery.setResultClassList(Collections.singletonList(PK.class));
		final SearchResult<PK> result = getFlexibleSearchService().search(searchQuery);
		getContext().getAuditService().audit(getSystemArea(), Status.SUCCESS, "Data producer has found {} items to produce for {}", result.getCount(), getContext().getActivityDescription());
		return result.getResult();
	}

	protected List<ItemModel> loadItems(final Collection<PK> keys)
	{
		if (keys.isEmpty())
		{
			return new ArrayList<>(0);
		}
		final FlexibleSearchQuery query = new FlexibleSearchQuery(getLoadItemsQuery(), Collections.singletonMap(getKeysQueryParam(), keys));
		final SearchResult<ItemModel> searchResult = getFlexibleSearchService().search(query);
		return searchResult.getResult();
	}

	protected int getBatchSize()
	{
		return batchSize;
	}

	protected int initializeBatchSize(final EtlContext etlContext)
	{
		final Integer size = Ints.tryParse(etlContext.getParameters().getOrDefault(getBatchSizeParamkey(), DEFAULT_BATCH_SIZE).toString());
		final int sizeParam = size != null ? size : DEFAULT_BATCH_SIZE;
		final int maxBatchSize = findMaxBatchSize();
		if (sizeParam > maxBatchSize)
		{
			LOG.warn("Batch size [{}] for ETL activity [{}] is greater than the maximum batch size [{}]. Maximum batch size will be used instead.",
					sizeParam, etlContext.getActivityDescription(), maxBatchSize);
			return maxBatchSize;
		}
		return sizeParam;

	}

	protected int findMaxBatchSize()
	{
		return getConfigurationService().getConfiguration().getInt(MAXIMUM_BATCH_SIZE_PROPERTY, DEFAULT_MAX_BATCH_SIZE);
	}

	protected boolean findDetachModels(final EtlContext etlContext)
	{
		return Boolean.parseBoolean(etlContext.getParameters().getOrDefault(getDetachModelsParamKey(), DEFAULT_DETACH_MODELS).toString());
	}

	protected EtlContext getContext()
	{
		return context;
	}

	protected FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	protected String getFindItemsQuery()
	{
		return findItemsQuery;
	}

	@Required
	public void setFindItemsQuery(final String findItemsQuery)
	{
		this.findItemsQuery = findItemsQuery;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	protected String getLoadItemsQuery()
	{
		return loadItemsQuery;
	}

	public void setLoadItemsQuery(final String loadItemsQuery)
	{
		this.loadItemsQuery = loadItemsQuery;
	}

	protected String getBatchSizeParamkey()
	{
		return batchSizeParamkey;
	}

	public void setBatchSizeParamkey(final String batchSizeParamkey)
	{
		this.batchSizeParamkey = batchSizeParamkey;
	}

	protected String getDetachModelsParamKey()
	{
		return detachModelsParamKey;
	}

	public void setDetachModelsParamKey(final String detatchModelsParamKey)
	{
		this.detachModelsParamKey = detatchModelsParamKey;
	}

	public String getKeysQueryParam()
	{
		return keysQueryParam;
	}

	public void setKeysQueryParam(final String keysQueryParam)
	{
		this.keysQueryParam = keysQueryParam;
	}

	public EtlProducerFilterStrategy<ItemModel> getEtlProducerFilterStrategy()
	{
		return etlProducerFilterStrategy;
	}

	public void setEtlProducerFilterStrategy(final EtlProducerFilterStrategy<ItemModel> etlProducerFilterStrategy)
	{
		this.etlProducerFilterStrategy = etlProducerFilterStrategy;
	}

	protected ModelServiceSelectionStrategy<EtlContext> getModelServiceSelectionStrategy()
	{
		return modelServiceSelectionStrategy;
	}

	@Required
	public void setModelServiceSelectionStrategy(final ModelServiceSelectionStrategy<EtlContext> modelServiceSelectionStrategy)
	{
		this.modelServiceSelectionStrategy = modelServiceSelectionStrategy;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public SystemArea getSystemArea()
	{
		return systemArea;
	}

	public void setSystemArea(final SystemArea systemArea)
	{
		this.systemArea = systemArea;
	}
}
