package com.waters.hybris.integration.core.etl.consumer;

import com.google.common.primitives.Ints;
import com.neoworks.hybris.etl.consumers.DataConsumer;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import com.neoworks.hybris.util.BatchCommitter;
import com.neoworks.hybris.util.ListBackedBatchCommitter;
import com.waters.hybris.integration.core.strategy.ModelServiceSelectionStrategy;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

/**
 * @author Tom Greasley (tom@neoworks.com)
 */
public class ModelSavingDataConsumer<T extends ItemModel> implements DataConsumer<T>
{
	protected static final String DEFAULT_BATCH_SIZE_PARAMKEY = "batchSize";
	protected static final int DEFAULT_BATCH_SIZE = 100;

	protected static final String DEFAULT_DETACH_MODELS_PARAMKEY = "detachModels";
	protected static final boolean DEFAULT_DETACH_MODELS = true;

	protected ModelServiceSelectionStrategy<EtlContext> modelServiceSelectionStrategy;
	protected ModelService modelService;
	protected BatchCommitter batchCommitter;
	protected EtlContext etlContext;
	private String batchSizeParamKey = DEFAULT_BATCH_SIZE_PARAMKEY;
	private String detachModelsParamKey = DEFAULT_DETACH_MODELS_PARAMKEY;


	@Override
	public void put(final T itemModel)
	{
		Assert.notNull(itemModel, "Item cannot be null.");

		if (etlContext.hasAuditContext() && etlContext.getAuditContext().isOpen())
		{
			etlContext.getAuditContext().addMessage("Saving model {}", itemModel.getPk()).setRelatedItem(itemModel);
		}
		batchCommitter.add(itemModel);
		batchCommitter.commitIfReady();
	}

	@Override
	public void init(final EtlContext etlContext) throws EtlValidationException
	{
		this.etlContext = etlContext;

		this.modelService = getModelServiceSelectionStrategy().selectModelService(etlContext);
		final int batchSize = getBatchSize(etlContext);
		final boolean detachModels = getDetachModels(etlContext);
		this.batchCommitter = new ListBackedBatchCommitter(batchSize, getModelService(), detachModels);
	}

	@Override
	public void start() throws EtlTransformationException
	{
		batchCommitter.reset();
	}

	@Override
	public void end(final boolean success) throws EtlTransformationException
	{
		batchCommitter.commit();
		batchCommitter.reset();
	}

	protected int getBatchSize(final EtlContext etlContext)
	{
		final Integer size = Ints.tryParse(etlContext.getParameters().getOrDefault(getBatchSizeParamKey(), DEFAULT_BATCH_SIZE).toString());
		return size != null ? size : DEFAULT_BATCH_SIZE;
	}

	protected boolean getDetachModels(final EtlContext etlContext)
	{
		return Boolean.parseBoolean(etlContext.getParameters().getOrDefault(getBatchSizeParamKey(), DEFAULT_DETACH_MODELS).toString());
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	protected String getBatchSizeParamKey()
	{
		return batchSizeParamKey;
	}

	public void setBatchSizeParamKey(final String batchSizeParamKey)
	{
		this.batchSizeParamKey = batchSizeParamKey;
	}

	protected String getDetachModelsParamKey()
	{
		return detachModelsParamKey;
	}

	public void setDetachModelsParamKey(final String detachModelsParamKey)
	{
		this.detachModelsParamKey = detachModelsParamKey;
	}

	protected ModelServiceSelectionStrategy<EtlContext> getModelServiceSelectionStrategy()
	{
		return modelServiceSelectionStrategy;
	}

	@Required
	public void setModelServiceSelectionStrategy(final ModelServiceSelectionStrategy<EtlContext> modelServiceSelectionStrategy)
	{
		this.modelServiceSelectionStrategy = modelServiceSelectionStrategy;
	}
}
