package com.waters.hybris.integration.core.context;

import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.util.BatchCommitter;
import com.neoworks.hybris.util.ListBackedBatchCommitter;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.lang.reflect.Constructor;

@SuppressFBWarnings(value = "REC_CATCH_EXCEPTION", justification = "We want to catch exceptions to so we can log them.")
public class DataImportContextBuilder extends AbstractImportContextBuilder
{
	private static final Logger LOG = LoggerFactory.getLogger(DataImportContextBuilder.class);
	private Class<? extends DataImportContext> cls;

	protected Class<? extends DataImportContext> getCls()
	{
		return cls;
	}

	@Required
	public void setCls(final Class<? extends DataImportContext> cls)
	{
		this.cls = cls;
	}

	public DataImportContext createDataImportContext(final String type)
	{
		final AuditContext auditContext = getAuditService().beginAudit(getSystemArea(), null, "Importing data [{}]", type);
		BatchCommitter batchCommitter = null;
		if (isUseBatchCommitter())
		{
			batchCommitter = new ListBackedBatchCommitter(getBatchSize(), getModelServiceSelectionStrategy().selectModelService(getUseTransactionalModelServiceConfigKey()), true);
		}

		try
		{
			final Constructor<?> ctor = getCls().getConstructor(String.class, AuditContext.class, BatchCommitter.class);
			final DataImportContext context = (DataImportContext) ctor.newInstance(type, auditContext, batchCommitter);
			initializeModelService(context);
			return context;
		}
		catch (final Exception e)
		{
			LOG.error("Failed to create DataImportContext class " + getCls(), e);
		}

		auditContext.endAudit(Status.FAILURE, null);
		return null;
	}
}
