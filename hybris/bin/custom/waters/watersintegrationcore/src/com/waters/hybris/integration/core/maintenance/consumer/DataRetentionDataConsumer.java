package com.waters.hybris.integration.core.maintenance.consumer;

import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import com.waters.hybris.integration.core.etl.consumer.ModelSavingDataConsumer;
import com.waters.hybris.integration.core.maintenance.batch.ListBackedBatchCommitter;
import com.waters.hybris.integration.core.maintenance.batch.strategy.FlushingStrategy;
import de.hybris.platform.core.model.ItemModel;
import org.springframework.beans.factory.annotation.Required;

public class DataRetentionDataConsumer<T extends ItemModel> extends ModelSavingDataConsumer<T>
{
	private FlushingStrategy flushingStrategy;

	@Override
	public void init(final EtlContext etlContext) throws EtlValidationException
	{
		this.etlContext = etlContext;

		this.modelService = getModelServiceSelectionStrategy().selectModelService(etlContext);
		final int batchSize = getBatchSize(etlContext);
		final boolean detachModels = getDetachModels(etlContext);
		this.batchCommitter = new ListBackedBatchCommitter(batchSize, getModelService(), detachModels, getFlushingStrategy());
	}

	protected FlushingStrategy getFlushingStrategy()
	{
		return flushingStrategy;
	}

	@Required
	public void setFlushingStrategy(final FlushingStrategy flushingStrategy)
	{
		this.flushingStrategy = flushingStrategy;
	}
}
