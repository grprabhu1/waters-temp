package com.waters.hybris.integration.core.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Collection;

/**
 * @author ramana
 */
public interface IntegrationHelperService
{
	Collection<CatalogVersionModel> getStagedProductCatalogVersionForStore(BaseStoreModel baseStore);
}
