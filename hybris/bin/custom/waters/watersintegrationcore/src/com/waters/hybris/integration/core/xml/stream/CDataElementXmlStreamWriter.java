package com.waters.hybris.integration.core.xml.stream;

import org.springframework.util.Assert;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.util.List;

/**
 * @author Tom Greasley (tom@neoworks.com)
 */
public class CDataElementXmlStreamWriter extends DelegatingXMLStreamWriter
{
	private final List<String> cdataFields;
	private boolean cdataRequired = false;

	public CDataElementXmlStreamWriter(final XMLStreamWriter streamWriter, final List<String> cdataFields)
	{
		super(streamWriter);

		Assert.notNull(cdataFields, "cdataFields must not be null");
		this.cdataFields = cdataFields;
	}

	@Override
	public void writeStartElement(final String localName) throws XMLStreamException
	{
		cdataRequired = requiresCData(localName);
		super.writeStartElement(localName);
	}

	@Override
	public void writeStartElement(final String namespaceURI, final String localName) throws XMLStreamException
	{
		cdataRequired = requiresCData(localName);
		super.writeStartElement(namespaceURI, localName);
	}

	@Override
	public void writeStartElement(final String prefix, final String localName, final String namespaceURI) throws XMLStreamException
	{
		cdataRequired = requiresCData(localName);
		super.writeStartElement(prefix, localName, namespaceURI);
	}

	@Override
	public void writeCharacters(final String text) throws XMLStreamException
	{
		if (cdataRequired)
		{
			writeCData(text);
		}
		else
		{
			super.writeCharacters(text);
		}
	}

	protected boolean requiresCData(final String localName)
	{
		return cdataFields.contains(localName);
	}

}
