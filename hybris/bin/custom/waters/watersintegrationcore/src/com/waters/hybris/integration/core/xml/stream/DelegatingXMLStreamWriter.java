package com.waters.hybris.integration.core.xml.stream;

import org.springframework.util.Assert;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 * @author Tom Greasley (tom@neoworks.com)
 */
public class DelegatingXMLStreamWriter implements XMLStreamWriter
{
	private final XMLStreamWriter xmlStreamWriter;

	public DelegatingXMLStreamWriter(final XMLStreamWriter xmlStreamWriter)
	{
		Assert.notNull(xmlStreamWriter, "xmlStreamWriter must not be null");
		this.xmlStreamWriter = xmlStreamWriter;
	}

	@Override
	public void writeStartElement(final String localName) throws XMLStreamException
	{
		xmlStreamWriter.writeStartElement(localName);
	}

	@Override
	public void writeStartElement(final String namespaceURI, final String localName) throws XMLStreamException
	{
		xmlStreamWriter.writeStartElement(namespaceURI, localName);
	}

	@Override
	public void writeStartElement(final String prefix, final String localName, final String namespaceURI) throws XMLStreamException
	{
		xmlStreamWriter.writeStartElement(prefix, localName, namespaceURI);
	}

	@Override
	public void writeEmptyElement(final String namespaceURI, final String localName) throws XMLStreamException
	{
		xmlStreamWriter.writeEmptyElement(namespaceURI, localName);
	}

	@Override
	public void writeEmptyElement(final String prefix, final String localName, final String namespaceURI) throws XMLStreamException
	{
		xmlStreamWriter.writeEmptyElement(prefix, localName, namespaceURI);
	}

	@Override
	public void writeEmptyElement(final String localName) throws XMLStreamException
	{
		xmlStreamWriter.writeEmptyElement(localName);
	}

	@Override
	public void writeEndElement() throws XMLStreamException
	{
		xmlStreamWriter.writeEndElement();
	}

	@Override
	public void writeEndDocument() throws XMLStreamException
	{
		xmlStreamWriter.writeEndDocument();
	}

	@Override
	public void close() throws XMLStreamException
	{
		xmlStreamWriter.close();
	}

	@Override
	public void flush() throws XMLStreamException
	{
		xmlStreamWriter.flush();
	}

	@Override
	public void writeAttribute(final String localName, final String value) throws XMLStreamException
	{
		xmlStreamWriter.writeAttribute(localName, value);
	}

	@Override
	public void writeAttribute(final String prefix, final String namespaceURI, final String localName, final String value) throws XMLStreamException
	{
		xmlStreamWriter.writeAttribute(prefix, namespaceURI, localName, value);
	}

	@Override
	public void writeAttribute(final String namespaceURI, final String localName, final String value) throws XMLStreamException
	{
		xmlStreamWriter.writeAttribute(namespaceURI, localName, value);
	}

	@Override
	public void writeNamespace(final String prefix, final String namespaceURI) throws XMLStreamException
	{
		xmlStreamWriter.writeNamespace(prefix, namespaceURI);
	}

	@Override
	public void writeDefaultNamespace(final String namespaceURI) throws XMLStreamException
	{
		xmlStreamWriter.writeDefaultNamespace(namespaceURI);
	}

	@Override
	public void writeComment(final String data) throws XMLStreamException
	{
		xmlStreamWriter.writeComment(data);
	}

	@Override
	public void writeProcessingInstruction(final String target) throws XMLStreamException
	{
		xmlStreamWriter.writeProcessingInstruction(target);
	}

	@Override
	public void writeProcessingInstruction(final String target, final String data) throws XMLStreamException
	{
		xmlStreamWriter.writeProcessingInstruction(target, data);
	}

	@Override
	public void writeCData(final String data) throws XMLStreamException
	{
		xmlStreamWriter.writeCData(data);
	}

	@Override
	public void writeDTD(final String dtd) throws XMLStreamException
	{
		xmlStreamWriter.writeDTD(dtd);
	}

	@Override
	public void writeEntityRef(final String name) throws XMLStreamException
	{
		xmlStreamWriter.writeEntityRef(name);
	}

	@Override
	public void writeStartDocument() throws XMLStreamException
	{
		xmlStreamWriter.writeStartDocument();
	}

	@Override
	public void writeStartDocument(final String version) throws XMLStreamException
	{
		xmlStreamWriter.writeStartDocument(version);
	}

	@Override
	public void writeStartDocument(final String encoding, final String version) throws XMLStreamException
	{
		xmlStreamWriter.writeStartDocument(encoding, version);
	}

	@Override
	public void writeCharacters(final String text) throws XMLStreamException
	{
		xmlStreamWriter.writeCharacters(text);
	}

	@Override
	public void writeCharacters(final char[] text, final int start, final int len) throws XMLStreamException
	{
		xmlStreamWriter.writeCharacters(text, start, len);
	}

	@Override
	public String getPrefix(final String uri) throws XMLStreamException
	{
		return xmlStreamWriter.getPrefix(uri);
	}

	@Override
	public void setPrefix(final String prefix, final String uri) throws XMLStreamException
	{
		xmlStreamWriter.setPrefix(prefix, uri);
	}

	@Override
	public void setDefaultNamespace(final String uri) throws XMLStreamException
	{
		xmlStreamWriter.setDefaultNamespace(uri);
	}

	@Override
	public NamespaceContext getNamespaceContext()
	{
		return xmlStreamWriter.getNamespaceContext();
	}

	@Override
	public void setNamespaceContext(final NamespaceContext context) throws XMLStreamException
	{
		xmlStreamWriter.setNamespaceContext(context);
	}

	@Override
	public Object getProperty(final String name) throws IllegalArgumentException
	{
		return xmlStreamWriter.getProperty(name);
	}
}
