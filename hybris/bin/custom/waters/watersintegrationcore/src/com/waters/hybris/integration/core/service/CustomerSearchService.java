package com.waters.hybris.integration.core.service;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;

/**
 * Created by david.minter on 16/02/2017.
 */
public interface CustomerSearchService
{
	List<CustomerModel> findCustomerByPartialDetails(String email, String firstName, final String lastName, String phone, String postcode, String address, int maximumRecordsLimit);
}
