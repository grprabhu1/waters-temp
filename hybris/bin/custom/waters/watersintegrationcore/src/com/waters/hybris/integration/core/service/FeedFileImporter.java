package com.waters.hybris.integration.core.service;

import java.io.File;

public interface FeedFileImporter
{
	void importFile(File file);
}
