package com.waters.hybris.integration.core.file.provider;

import java.io.File;

public interface GroupKeyProvider
{
	String getGroupKey(File file);

	String getGroupKey(String filename);
}
