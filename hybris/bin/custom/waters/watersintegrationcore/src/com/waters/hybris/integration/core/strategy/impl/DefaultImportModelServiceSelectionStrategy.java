package com.waters.hybris.integration.core.strategy.impl;

import com.waters.hybris.integration.core.context.ImportContext;
import com.waters.hybris.integration.core.strategy.ModelServiceSelectionStrategy;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;

public class DefaultImportModelServiceSelectionStrategy implements ModelServiceSelectionStrategy<ImportContext>
{
	private ModelService modelService;
	private ModelService transactionalModelService;

	@Override
	public ModelService selectModelService(final ImportContext context)
	{
		if (useTransactionalModelService(context))
		{
			return getTransactionalModelService();
		}
		else
		{
			return getModelService();
		}
	}

	@Override
	public boolean useTransactionalModelService(final ImportContext context)
	{
		return context.isUseTransactionalModelService();
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected ModelService getTransactionalModelService()
	{
		return transactionalModelService;
	}

	@Required
	public void setTransactionalModelService(final ModelService transactionalModelService)
	{
		this.transactionalModelService = transactionalModelService;
	}

}
