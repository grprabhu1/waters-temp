package com.waters.hybris.integration.core.etl.strategy;

import com.neoworks.hybris.etl.context.EtlContext;

public interface EtlItemFilterStrategy<T>
{
	boolean matches(EtlContext context, T item);
}
