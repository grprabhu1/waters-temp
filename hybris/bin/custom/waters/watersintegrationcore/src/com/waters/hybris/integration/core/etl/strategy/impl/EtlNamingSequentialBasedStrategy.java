package com.waters.hybris.integration.core.etl.strategy.impl;

import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.model.EtlCronJobParamModel;
import com.waters.hybris.integration.core.etl.strategy.EtlNamingStrategy;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EtlNamingSequentialBasedStrategy implements EtlNamingStrategy
{

	private final static Pattern sizeFinderPattern = Pattern.compile(".*\\%0(\\d)[d].*");
	private static final Logger LOG = LoggerFactory.getLogger(EtlNamingSequentialBasedStrategy.class);
	private String etlCronJobSequenceParamKey;
	private String fileNamePattern;
	private ModelService modelService;
	private EtlNamingStrategy previousStrategy;

	@Override
	public String generateName(final EtlContext context)
	{
		final String pattern = getFilePattern(context);
		final int nextSequentialNumber = incrementAndGetNextNumber(context, getMaxSequentialNumber(pattern));
		return String.format(pattern, nextSequentialNumber);
	}

	private int incrementAndGetNextNumber(final EtlContext context, final int maxSequentialNumber)
	{
		final Object etlCronJobParam = context.getParameters().get(getEtlCronJobSequenceParamKey());
		if (etlCronJobParam instanceof EtlCronJobParamModel)
		{
			final EtlCronJobParamModel etlCronJobParamModel = ((EtlCronJobParamModel) etlCronJobParam);
			if (etlCronJobParamModel.getValue() instanceof Integer)
			{
				final int nextSequentialNumber = incrementSequence((Integer) etlCronJobParamModel.getValue(), maxSequentialNumber);
				etlCronJobParamModel.setValue(nextSequentialNumber);
				modelService.save(etlCronJobParam);
				return nextSequentialNumber;
			}
		}
		throw new IllegalArgumentException("Expected property " + getEtlCronJobSequenceParamKey() + " of type EtlCronJobParamModel with value type Integer");
	}

	private String getFilePattern(final EtlContext context)
	{
		if (getPreviousStrategy() != null)
		{
			return getPreviousStrategy().generateName(context);
		}
		return getFileNamePattern();
	}

	private int incrementSequence(Integer currentSequentialNumber, final int maxSequentialNumber)
	{
		currentSequentialNumber++;
		if (currentSequentialNumber > maxSequentialNumber)
		{
			return 1;
		}
		return currentSequentialNumber;
	}

	public int getMaxSequentialNumber(final String pattern)
	{
		final Matcher result = sizeFinderPattern.matcher(pattern);
		if (result.matches() && StringUtils.isNotEmpty(result.group()))
		{
			try
			{
				return (int) Math.pow(10, Integer.parseInt(result.group(1))) - 1;
			}
			catch (NumberFormatException e)
			{
				//it should not happen if so we log it and we throw Illegal....
				LOG.error("Error converting to integer for: " + result.group(), e);
			}
		}
		throw new IllegalArgumentException("The file pattern must include the format %0{number}d. Not found in: " + pattern);
	}

	public String getFileNamePattern()
	{
		return fileNamePattern;
	}

	public void setFileNamePattern(final String fileNamePattern)
	{
		this.fileNamePattern = fileNamePattern;
	}

	public EtlNamingStrategy getPreviousStrategy()
	{
		return previousStrategy;
	}

	public void setPreviousStrategy(final EtlNamingStrategy previousStrategy)
	{
		this.previousStrategy = previousStrategy;
	}

	public String getEtlCronJobSequenceParamKey()
	{
		return etlCronJobSequenceParamKey;
	}

	@Required
	public void setEtlCronJobSequenceParamKey(final String etlCronJobSequenceParamKey)
	{
		this.etlCronJobSequenceParamKey = etlCronJobSequenceParamKey;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
