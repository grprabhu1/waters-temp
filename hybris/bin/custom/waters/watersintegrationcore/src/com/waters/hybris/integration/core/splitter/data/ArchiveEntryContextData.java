package com.waters.hybris.integration.core.splitter.data;

import java.io.InputStream;

public class ArchiveEntryContextData
{
	private final InputStream inputStream;
	private final String filename;

	public ArchiveEntryContextData(final InputStream inputStream, final String filename)
	{
		this.inputStream = inputStream;
		this.filename = filename;
	}

	public InputStream getInputStream()
	{
		return inputStream;
	}

	public String getFilename()
	{
		return filename;
	}

}
