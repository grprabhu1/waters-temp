package com.waters.hybris.integration.core.etl.action;

import com.neoworks.hybris.etl.action.EtlAction;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.waters.hybris.integration.core.data.PipelineResultData;
import org.springframework.beans.factory.annotation.Required;

import java.util.Map;

public abstract class AbstractPipelineResultProcessorAction<K> implements EtlAction
{
	private String pipelineResultMapParamKey;

	@Override
	@SuppressWarnings("unchecked")
	public void execute(final EtlContext etlContext) throws EtlTransformationException
	{
		final Map<K, PipelineResultData> pipelineResultMap = (Map<K, PipelineResultData>) etlContext.getParameters().get(getPipelineResultMapParamKey());

		if (pipelineResultMap != null)
		{
			processPipelineResultMap(pipelineResultMap);
		}
		else
		{
			throw new EtlTransformationException("Result map must be provided in ETL context parameters");
		}
	}

	protected abstract void processPipelineResultMap(final Map<K, PipelineResultData> pipelineResultMap);

	protected String getPipelineResultMapParamKey()
	{
		return pipelineResultMapParamKey;
	}

	@Required
	public void setPipelineResultMapParamKey(final String pipelineResultMapParamKey)
	{
		this.pipelineResultMapParamKey = pipelineResultMapParamKey;
	}
}
