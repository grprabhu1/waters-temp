package com.waters.hybris.integration.core.strategy;

import de.hybris.platform.servicelayer.model.ModelService;

public interface ModelServiceSelectionStrategy<CONTEXT>
{
	ModelService selectModelService(CONTEXT context);

	boolean useTransactionalModelService(CONTEXT context);
}
