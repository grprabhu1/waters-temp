package com.waters.hybris.integration.core.file.provider;

import com.waters.hybris.integration.core.file.parser.FileNameHashParser;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import java.io.File;

/**
 * @author Tom Greasley (tom@neoworks.com)
 */
public class FileHashFromNameProvider implements FileHashProvider
{
	private FileNameHashParser fileNameHashParser;

	@Override
	public String getFileHash(final File file)
	{
		Assert.notNull(file, "file cannot be null");
		Assert.notNull(file.getName(), "fileName can not be null");
		return getFileNameHashParser().getHash(file.getName());
	}

	@Override
	public String getFileHash(final String filename)
	{
		Assert.notNull(filename, "fileName can not be null");
		return getFileNameHashParser().getHash(filename);
	}

	protected FileNameHashParser getFileNameHashParser()
	{
		return fileNameHashParser;
	}

	@Required
	public void setFileNameHashParser(final FileNameHashParser fileNameHashParser)
	{
		this.fileNameHashParser = fileNameHashParser;
	}
}
