package com.waters.hybris.integration.core.service;

/**
 * @author ramana
 */
public interface DataProcessor<inputType, context>
{
	default void process(final inputType input) throws Exception
	{
		process(input, null);
	}

	void process(inputType input, context context) throws Exception;
}
