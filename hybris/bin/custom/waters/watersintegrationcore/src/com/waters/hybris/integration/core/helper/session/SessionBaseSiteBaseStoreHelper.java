package com.waters.hybris.integration.core.helper.session;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.storesession.StoreSessionService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import static org.slf4j.LoggerFactory.getLogger;

public class SessionBaseSiteBaseStoreHelper
{
	private static final Logger LOG = getLogger(SessionBaseSiteBaseStoreHelper.class);

	private BaseSiteService baseSiteService;
	private BaseStoreService baseStoreService;
	private SessionService sessionService;
	private StoreSessionService storeSessionService;

	public void setBaseSite(final BaseSiteModel site)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Updating the current base site to [{}] ", site.getName());
		}
		getBaseSiteService().setCurrentBaseSite(site, false);
	}

	public void setUserPriceGroup(final BaseStoreModel store)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Updating session UPG to {}", store.getUid());
		}
		//getSessionService().setAttribute(Europe1Constants.PARAMS.UPG, store.getUserPriceGroup());
	}

	public void setSessionAttributesForBaseSite(final BaseSiteModel site)
	{
		setBaseSite(site);
		final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();
		setUserPriceGroup(baseStore);

		final CurrencyModel defaultCurrency = baseStore.getDefaultCurrency();
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Updating session Currency to {}", defaultCurrency.getIsocode());
		}
		getStoreSessionService().setCurrentCurrency(defaultCurrency.getIsocode());

		final LanguageModel defaultLanguage = baseStore.getDefaultLanguage();
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Updating session Language to {}", defaultLanguage.getIsocode());
		}
		getStoreSessionService().setCurrentLanguage(defaultLanguage.getIsocode());
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected StoreSessionService getStoreSessionService()
	{
		return storeSessionService;
	}

	@Required
	public void setStoreSessionService(final StoreSessionService storeSessionService)
	{
		this.storeSessionService = storeSessionService;
	}
}
