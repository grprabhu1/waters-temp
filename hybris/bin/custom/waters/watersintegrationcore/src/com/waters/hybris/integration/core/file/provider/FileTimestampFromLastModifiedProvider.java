package com.waters.hybris.integration.core.file.provider;

import org.springframework.util.Assert;

import java.io.File;
import java.util.Date;

public class FileTimestampFromLastModifiedProvider implements FileTimestampProvider
{
	@Override
	public Date getFileTimestamp(final File file)
	{
		Assert.notNull(file, "file cannot be null");
		return new Date(file.lastModified());
	}

	@Override
	public Date getFileTimestamp(final String filename)
	{
		Assert.notNull(filename, "file cannot be null");
		return new Date();
	}
}
