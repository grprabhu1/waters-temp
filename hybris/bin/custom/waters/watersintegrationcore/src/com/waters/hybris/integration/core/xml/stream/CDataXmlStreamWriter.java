package com.waters.hybris.integration.core.xml.stream;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.util.regex.Pattern;

/**
 * @author Tom Greasley (tom@neoworks.com)
 */
public class CDataXmlStreamWriter extends DelegatingXMLStreamWriter
{
	private static final Pattern XML_CHARS = Pattern.compile("[&<>]");

	public CDataXmlStreamWriter(final XMLStreamWriter del)
	{
		super(del);
	}

	@Override
	public void writeCharacters(final String text) throws XMLStreamException
	{
		if (XML_CHARS.matcher(text).find())
		{
			writeCData(text);
		}
		else
		{
			super.writeCharacters(text);
		}
	}
}
