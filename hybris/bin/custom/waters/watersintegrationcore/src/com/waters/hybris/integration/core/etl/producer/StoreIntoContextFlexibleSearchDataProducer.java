package com.waters.hybris.integration.core.etl.producer;

import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import com.neoworks.hybris.etl.producers.impl.FlexibleSearchDataProducer;
import com.waters.hybris.integration.core.data.PipelineResultData;
import com.waters.hybris.integration.core.etl.data.PipelineResult;
import com.waters.hybris.integration.core.etl.strategy.EtlItemFilterStrategy;
import com.waters.hybris.integration.core.etl.util.PipelineResultDataUtils;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.ItemModel;
import org.springframework.beans.factory.annotation.Required;

import java.util.HashMap;
import java.util.Map;

public class StoreIntoContextFlexibleSearchDataProducer extends FlexibleSearchDataProducer
{
	private String pipelineResultMapParamKey;
	private String currentItemKeyParamKey;
	private EtlItemFilterStrategy<ItemModel> etlItemFilterStrategy = (EtlContext context, ItemModel item) -> true;

	@Override
	public void init(final EtlContext context) throws EtlValidationException
	{
		super.init(context);
		getContext().getParameters().put(getPipelineResultMapParamKey(), new HashMap<PK, PipelineResultData>());
	}

	@Override
	@SuppressWarnings("unchecked")
	public ItemModel get()
	{
		final ItemModel nextItem = super.get();
		if (nextItem == null)
		{
			return nextItem;
		}

		final EtlContext context = getContext();
		if (getEtlItemFilterStrategy().matches(context, nextItem))
		{
			final Map<PK, PipelineResultData> pipelineResultMap = (Map<PK, PipelineResultData>) context.getParameters().get(getPipelineResultMapParamKey());
			if (pipelineResultMap != null)
			{
				pipelineResultMap.put(nextItem.getPk(), PipelineResultDataUtils.createPipelineResultData(PipelineResult.PRODUCED));
				context.getParameters().put(getCurrentItemKeyParamKey(), nextItem.getPk());
			}
			return nextItem;
		}

		return get();
	}

	protected String getPipelineResultMapParamKey()
	{
		return pipelineResultMapParamKey;
	}

	@Required
	public void setPipelineResultMapParamKey(final String pipelineResultMapParamKey)
	{
		this.pipelineResultMapParamKey = pipelineResultMapParamKey;
	}

	protected String getCurrentItemKeyParamKey()
	{
		return currentItemKeyParamKey;
	}

	@Required
	public void setCurrentItemKeyParamKey(final String currentItemKeyParamKey)
	{
		this.currentItemKeyParamKey = currentItemKeyParamKey;
	}

	protected EtlItemFilterStrategy<ItemModel> getEtlItemFilterStrategy()
	{
		return etlItemFilterStrategy;
	}

	public void setEtlItemFilterStrategy(final EtlItemFilterStrategy<ItemModel> etlItemFilterStrategy)
	{
		this.etlItemFilterStrategy = etlItemFilterStrategy;
	}
}
