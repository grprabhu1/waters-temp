package com.waters.hybris.integration.core.splitter.impl;


import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.audit.service.AuditService;
import com.waters.hybris.integration.core.context.ImportContext;
import com.waters.hybris.integration.core.filter.ArchiveEntryFilter;
import com.waters.hybris.integration.core.iterator.CloseableIterator;
import com.waters.hybris.integration.core.splitter.Splitter;
import com.waters.hybris.integration.core.splitter.data.ArchiveEntryContextData;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArchiveSplitter implements Splitter<ArchiveEntryContextData>
{
	private static final Logger LOG = LoggerFactory.getLogger(ArchiveSplitter.class);
	private AuditService auditService;
	private ArchiveEntryFilter<TarArchiveEntry> filter;


	/**
	 * Split each entry of an archive into an Iterable resource. The custom iterator will create an ArchiveInputStream from the fileImportRecord
	 * as we cannot directly access the underlying file from the TarArchiveEntry we will need to access the entry via the archiveInputStream
	 *
	 * Note that wrapping the creation of the input stream in a try-with-resources will cause a NPE when we call getNextTarEntry() method
	 *
	 * @param stream        The file import record
	 * @param importContext context
	 * @return -
	 * @throws IOException
	 */
	@Override
	public CloseableIterator<ArchiveEntryContextData> split(final InputStream stream, final ImportContext importContext) throws IOException
	{
		final AuditContext auditContext = getAuditService().beginAudit(SystemArea.GENERAL, null, "Importing tar feed file");
		try
		{
			final TarArchiveInputStream tarArchiveInputStream = new TarArchiveInputStream(new GzipCompressorInputStream(stream));

			final Iterator<ArchiveEntryContextData> archiveIterator = new Iterator<ArchiveEntryContextData>()
			{
				private ArchiveEntryContextData cachedObject = null;
				private boolean finished = false;

				@Override
				public boolean hasNext()
				{
					if (cachedObject != null)
					{
						return true;
					}
					else if (finished)
					{
						return false;
					}
					else
					{
						final ArchiveEntryContextData nextStream = readNextEntry();
						if (nextStream == null)
						{
							finished = true;
							return false;
						}
						else
						{
							cachedObject = nextStream;
							return true;
						}
					}
				}

				@Override
				public ArchiveEntryContextData next()
				{
					if (!hasNext())
					{
						auditContext.endAudit(Status.FAILURE, "File has been consumed");
						throw new NoSuchElementException("File has been consumed");
					}

					final ArchiveEntryContextData currentObject = cachedObject;
					cachedObject = null;
					return currentObject;
				}

				/**
				 * read the next entry in the archive. As we will need the filename on creation of the ATG Product Record (for the hash) we need to put the
				 * required items (InputStream and TarFile) in a wrapper object
				 * @return -
				 */
				private ArchiveEntryContextData readNextEntry()
				{
					try
					{
						ArchiveEntryContextData result = null;
						boolean eos = false;
						while (null == result && !eos)
						{
							final TarArchiveEntry entry = tarArchiveInputStream.getNextTarEntry();
							if (entry == null)
							{
								eos = true;
							}
							else
							{
								final String entryName = entry.getName();
								if (getFilter().allow(entry))
								{
									LOG.debug("Processing new file [{}]", entryName);
									final InputStream entryInputStream = createInputStreamForRecord(tarArchiveInputStream, entry);
									if (entryInputStream == null)
									{
										LOG.error("Unable to create input stream for {}", entryName);
										auditContext.addMessage("Unable to create input stream for {}", entryName);
									}
									result = new ArchiveEntryContextData(entryInputStream, entryName);
								}
								else
								{
									LOG.debug("{} has already been processed", entryName);
									auditContext.addMessage("{} has already been processed", entryName);
								}
							}
						}
						return result;
					}
					catch (final IOException ex)
					{
						LOG.error("Unable to get next entry", ex);
						auditContext.endAudit(Status.FAILURE, "Exception processing file", ex);
						return null;
					}
				}
			};

			auditContext.endAudit(Status.SUCCESS, "Iterator created successfully");
			return new CloseableIterator<ArchiveEntryContextData>()
			{
				@Override
				public Closeable getCloseable()
				{
					return tarArchiveInputStream;
				}

				@Override
				public Iterator<ArchiveEntryContextData> getIterator()
				{
					return archiveIterator;
				}
			};
		}
		catch (final IOException e)
		{
			LOG.error("Unable to process tar file", e);
			auditContext.endAudit(Status.FAILURE, "Exception processing file", e);
		}

		return null;
	}

	/**
	 * Creates an input stream for a given Tar entry.
	 *
	 * We cannot access the File from the TarArchiveEntry, as getFile() will always return null
	 * Also, we cannot pass the tarArchiveInputStream into the SAXBuilder as the SAXBuilder will then close the Stream after processing the first entry
	 *
	 * @param tarArchiveInputStream tar input stream
	 * @param entry                 tar entry
	 * @return -
	 * @throws IOException
	 */
	protected InputStream createInputStreamForRecord(final TarArchiveInputStream tarArchiveInputStream, final TarArchiveEntry entry) throws IOException
	{
		final byte[] content = new byte[(int) entry.getSize()];
		final int bytesRead = tarArchiveInputStream.read(content, 0, content.length);
		return ((bytesRead != -1) ? new ByteArrayInputStream(content) : null);
	}

	protected AuditService getAuditService()
	{
		return auditService;
	}

	@Required
	public void setAuditService(final AuditService auditService)
	{
		this.auditService = auditService;
	}

	protected ArchiveEntryFilter<TarArchiveEntry> getFilter()
	{
		return filter;
	}

	@Required
	public void setFilter(final ArchiveEntryFilter<TarArchiveEntry> filter)
	{
		this.filter = filter;
	}
}
