package com.waters.hybris.integration.core.service;

import com.waters.hybris.integration.core.model.record.OutboundFileRecordModel;

import java.io.IOException;
import java.io.InputStream;

public interface OutboundFileRecordService
{
	void writeFromStream(InputStream stream, String destination, OutboundFileRecordModel outboundFileRecord) throws IOException;
}
