package com.waters.hybris.integration.core.file.filter;

import com.neoworks.hybris.audit.enums.SystemArea;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static javax.xml.XMLConstants.FEATURE_SECURE_PROCESSING;
import static org.slf4j.LoggerFactory.getLogger;

public class ValidateXmlFileFilter implements FileFilter
{
    private static final Logger LOG = LoggerFactory.getLogger(ValidateXmlFileFilter.class);
    private String xsdPath;
    private SystemArea systemArea;

    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value="UI_INHERITANCE_UNSAFE_GETRESOURCE", justification="I know what I'm doing")
    protected InputStream getResourceAsStream(final String resource )
    {
        final InputStream in = getClass().getResourceAsStream( resource );
        return in == null ? getClass().getResourceAsStream( resource ) : in;
    }

    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value="REC_CATCH_EXCEPTION", justification="I know what I'm doing")
    @Override
    public boolean accept(final File file)
    {
        final Source xmlFile = new StreamSource(file);

        try {
            final Source xsdSource = new StreamSource(getResourceAsStream(getXsdPath()));
            final SchemaFactory schemaFactory = SchemaFactory
                    .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            schemaFactory.setFeature(FEATURE_SECURE_PROCESSING, false); // switch off the size checks, no ddos here!
            final Schema schema = schemaFactory.newSchema(xsdSource);
            final Validator validator = schema.newValidator();
            validator.validate(xmlFile);
            getLogger(getSystemArea()).debug("{} is valid", xmlFile.getSystemId());
            return true;
        } catch (final SAXException | IOException e) {
            getLogger(getSystemArea()).error("{} is not valid, reason: {}", xmlFile.getSystemId(), e);
            throw new RuntimeException(e);
        }
    }

    @Required
    public void setXsdPath(final String xsdPath)
    {
        this.xsdPath = xsdPath;
    }

    public String getXsdPath() {
        return xsdPath;
    }

	protected Logger getLogger(final SystemArea systemArea)
	{
		if (systemArea != null)
		{
			return LoggerFactory.getLogger(systemArea.name());
		}
		return LOG;
	}

	public SystemArea getSystemArea()
	{
		return systemArea;
	}

	public void setSystemArea(final SystemArea systemArea)
	{
		this.systemArea = systemArea;
	}
}
