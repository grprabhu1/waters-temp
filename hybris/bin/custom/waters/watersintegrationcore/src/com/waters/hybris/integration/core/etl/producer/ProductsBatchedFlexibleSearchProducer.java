package com.waters.hybris.integration.core.etl.producer;

import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.waters.hybris.integration.core.etl.util.BatchedProcessor;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Producer to get products based on categories configured categories.
 */
public class ProductsBatchedFlexibleSearchProducer extends BatchedFlexibleSearchDataProducer
{

	private String catalogVersionKeyParam;
	private String webCategoryKeyParam;

	@Override
	public void start() throws EtlTransformationException
	{
		final CatalogVersionModel catalogVersion = getCatalogVersionFromContext();
		if (catalogVersion == null)
		{
			throw new EtlTransformationException("Catalog version must be provided in the etl context");
		}

		super.start();
	}

	@Override
	public Collection<PK> findItems()
	{
		final Object objectParam = getContext().getParameters().get(getWebCategoryKeyParam());

		final CategoryModel categoryModel = (CategoryModel) objectParam;
		final Collection<CategoryModel> allSubcategories = categoryModel.getAllSubcategories();
		final List<PK> keys = allSubcategories.stream()
				.map(CategoryModel::getPk)
				.collect(toList());

		return new BatchedProcessor<PK, PK>(getBatchSize(), this::loadProductInCategories).processBatchedData(keys, true);
	}

	protected List<PK> loadProductInCategories(final Collection<PK> keys)
	{
		if (keys.isEmpty())
		{
			return new ArrayList<>(0);
		}

		final String findItemsQuery = getFindItemsQuery();
		final FlexibleSearchQuery query = new FlexibleSearchQuery(findItemsQuery, Collections.singletonMap(getKeysQueryParam(), keys));
		query.setCatalogVersions(getCatalogVersionFromContext());
		query.setResultClassList(Collections.singletonList(PK.class));

		final SearchResult<PK> searchResult = getFlexibleSearchService().search(query);
		return searchResult.getResult();
	}

	protected CatalogVersionModel getCatalogVersionFromContext()
	{
		final Object param = getContext().getParameters().get(getCatalogVersionKeyParam());
		if (param instanceof CatalogVersionModel)
		{
			return (CatalogVersionModel) param;
		}
		return null;
	}

	protected String getCatalogVersionKeyParam()
	{
		return catalogVersionKeyParam;
	}

	@Required
	public void setCatalogVersionKeyParam(final String catalogVersionKeyParam)
	{
		this.catalogVersionKeyParam = catalogVersionKeyParam;
	}

	protected String getWebCategoryKeyParam()
	{
		return webCategoryKeyParam;
	}

	@Required
	public void setWebCategoryKeyParam(final String webCategoryKeyParam)
	{
		this.webCategoryKeyParam = webCategoryKeyParam;
	}
}
