package com.waters.hybris.integration.core.service;

import com.waters.hybris.integration.core.context.ImportContext;

import java.util.Iterator;

/**
 * @author ramana
 */
public interface ContextAwareSupplier<T, C extends ImportContext>
{
	Iterator<T> get(C context);
}
