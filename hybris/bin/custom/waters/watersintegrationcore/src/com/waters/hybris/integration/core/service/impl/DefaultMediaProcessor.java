package com.waters.hybris.integration.core.service.impl;

import com.waters.hybris.integration.core.context.JobContext;
import com.waters.hybris.integration.core.context.MediaImportContext;
import com.waters.hybris.integration.core.context.MediaImportContextBuilder;
import com.waters.hybris.integration.core.iterator.CloseableIterator;
import com.waters.hybris.integration.core.model.record.FileImportRecordModel;
import com.waters.hybris.integration.core.service.AbstractDataProcessor;
import com.waters.hybris.integration.core.splitter.Splitter;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import java.io.IOException;
import java.io.InputStream;

public class DefaultMediaProcessor<T> extends AbstractDataProcessor<FileImportRecordModel, T, MediaImportContext, JobContext>
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultMediaProcessor.class);

	private MediaImportContextBuilder mediaImportContextBuilder;
	private Splitter<T> splitter;
	private MediaService mediaService;

	@Override
	public void process(final FileImportRecordModel importRecordModel, final JobContext jobContext) throws Exception
	{
		super.process(importRecordModel, jobContext);
	}

	@Override
	protected MediaImportContext createContext(final FileImportRecordModel importRecordModel)
	{
		Assert.notNull(importRecordModel, "import record cannot be null");
		Assert.notNull(importRecordModel.getMedia(), "media cannot be null");

		return getMediaImportContextBuilder().createMediaImportContext(importRecordModel);
	}

	@Override
	protected CloseableIterator<T> retrieveData(final MediaImportContext context) throws IOException
	{
		if (context.getInput() != null)
		{
			final MediaModel media = context.getInput().getMedia();
			try
			{
				final InputStream inputStream = getMediaService().getStreamFromMedia(media);
				return getSplitter().split(inputStream, context);
			}
			catch (final IOException e)
			{
				LOG.error("Cannot read media [{}]", media.getCode(), e);
				return null;
			}
		}

		LOG.error("Input of unexpected type or null.");

		return null;
	}

	protected MediaImportContextBuilder getMediaImportContextBuilder()
	{
		return mediaImportContextBuilder;
	}

	@Required
	public void setMediaImportContextBuilder(final MediaImportContextBuilder mediaImportContextBuilder)
	{
		this.mediaImportContextBuilder = mediaImportContextBuilder;
	}

	protected Splitter<T> getSplitter()
	{
		return splitter;
	}

	@Required
	public void setSplitter(final Splitter<T> splitter)
	{
		this.splitter = splitter;
	}

	protected MediaService getMediaService()
	{
		return mediaService;
	}

	@Required
	public void setMediaService(final MediaService mediaService)
	{
		this.mediaService = mediaService;
	}
}
