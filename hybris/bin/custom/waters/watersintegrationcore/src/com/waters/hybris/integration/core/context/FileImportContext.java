package com.waters.hybris.integration.core.context;

import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.util.BatchCommitter;

import java.io.File;

/**
 * Not thread safe.
 */
public class FileImportContext extends ImportContext<File>
{
	public FileImportContext(final File file, final AuditContext auditContext, final BatchCommitter batchCommitter)
	{
		super(file, auditContext, batchCommitter);
	}

	public String getFileName()
	{
		return getInput() != null ? getInput().getName() : null;
	}

	@Override
	protected String generateSuccessMessage()
	{
		return String.format("Imported file [%s]. Processed: [%d]. Success: [%d]. Errors: [%d].", getFileName(), getTotalProcessed(), getTotalSuccess(), getTotalError());
	}

	@Override
	protected String generateErrorMessage()
	{
		return String.format("Failed to import file [%s]. Processed: [%d]. Success: [%d]. Errors: [%d].", getFileName(), getTotalProcessed(), getTotalSuccess(), getTotalError());
	}

	@Override
	public String getSource()
	{
		return getFileName();
	}
}
