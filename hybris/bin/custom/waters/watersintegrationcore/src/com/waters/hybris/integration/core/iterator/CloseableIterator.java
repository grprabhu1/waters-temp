package com.waters.hybris.integration.core.iterator;

import java.io.Closeable;
import java.util.Iterator;
import java.util.function.Consumer;

public interface CloseableIterator<T> extends Iterator<T>, AutoCloseable
{

	Closeable getCloseable();

	Iterator<T> getIterator();

	@Override
	default boolean hasNext()
	{
		return getIterator() != null && getIterator().hasNext();
	}

	@Override
	default T next()
	{
		return getIterator().next();
	}

	@Override
	default void remove()
	{
		getIterator().remove();
	}

	@Override
	default void forEachRemaining(final Consumer<? super T> action)
	{
		if (getIterator() != null)
		{
			getIterator().forEachRemaining(action);
		}
	}

	@Override
	default void close() throws Exception
	{
		if (getCloseable() != null)
		{
			getCloseable().close();
		}
	}

}
