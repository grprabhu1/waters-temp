package com.waters.hybris.integration.core.maintenance.consumer;

import com.waters.hybris.integration.core.model.record.OutboundFileRecordModel;
import org.springframework.util.Assert;

public class OutboundFileDataRetentionDataConsumer extends DataRetentionDataConsumer<OutboundFileRecordModel>
{
	@Override
	public void put(final OutboundFileRecordModel itemModel)
	{
		Assert.notNull(itemModel, "Item cannot be null.");

		if (etlContext.hasAuditContext() && etlContext.getAuditContext().isOpen())
		{
			etlContext.getAuditContext().addMessage("Saving model {}", itemModel.getPk()).setRelatedItem(itemModel);
		}

		if(itemModel.getMedia() != null)
		{
			batchCommitter.add(itemModel.getMedia());
		}
		batchCommitter.add(itemModel);
		batchCommitter.commitIfReady();
	}
}
