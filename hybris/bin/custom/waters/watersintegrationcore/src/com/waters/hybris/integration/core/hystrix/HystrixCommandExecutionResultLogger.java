package com.waters.hybris.integration.core.hystrix;

import com.netflix.hystrix.HystrixCommand;

public interface HystrixCommandExecutionResultLogger
{
	void logExecutionResult(final HystrixCommand<?> command, final String action, final String metricKeyPrefix, final String loggingServiceName);
}
