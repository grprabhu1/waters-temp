package com.waters.hybris.integration.core.service;

public interface FeedMessageImporter
{
	void importMessage(byte[] payload);
}
