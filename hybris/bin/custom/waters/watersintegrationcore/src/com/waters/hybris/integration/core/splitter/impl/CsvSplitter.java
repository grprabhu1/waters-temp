package com.waters.hybris.integration.core.splitter.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.waters.hybris.integration.core.context.ImportContext;
import com.waters.hybris.integration.core.iterator.CloseableIterator;
import com.waters.hybris.integration.core.splitter.Splitter;
import org.springframework.beans.factory.annotation.Required;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public class CsvSplitter<T> implements Splitter<T>
{
	private char columnSeparator;
	private boolean discardFirstRow;
	private Class<T> dataType;
	private boolean trimValues;

	protected char getColumnSeparator()
	{
		return columnSeparator;
	}

	@Required
	public void setColumnSeparator(final char columnSeparator)
	{
		this.columnSeparator = columnSeparator;
	}

	protected boolean isDiscardFirstRow()
	{
		return discardFirstRow;
	}

	@Required
	public void setDiscardFirstRow(final boolean discardFirstRow)
	{
		this.discardFirstRow = discardFirstRow;
	}

	protected Class<T> getDataType()
	{
		return dataType;
	}

	@Required
	public void setDataType(final Class<T> dataType)
	{
		this.dataType = dataType;
	}

	public boolean isTrimValues()
	{
		return trimValues;
	}

	public void setTrimValues(final boolean trimValues)
	{
		this.trimValues = trimValues;
	}

	@Override
	public CloseableIterator<T> split(final InputStream stream, final ImportContext importContext) throws IOException
	{
		final CsvMapper mapper = new CsvMapper();
		if (isTrimValues())
		{
			mapper.enable(CsvParser.Feature.TRIM_SPACES);
		}

		final CsvSchema schema = mapper.schemaFor(dataType).withSkipFirstDataRow(isDiscardFirstRow()).withColumnSeparator(getColumnSeparator());
		final ObjectReader objectReader = mapper.readerWithSchemaFor(dataType).with(schema)
				.with(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES);


		final MappingIterator<T> iterator = objectReader.readValues(stream);
		return new CloseableIterator<T>()
		{
			@Override
			public Closeable getCloseable()
			{
				return stream;
			}

			@Override
			public Iterator<T> getIterator()
			{
				return iterator;
			}
		};
	}
}
