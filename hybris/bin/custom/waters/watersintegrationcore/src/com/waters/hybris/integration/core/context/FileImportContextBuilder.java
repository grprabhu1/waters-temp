package com.waters.hybris.integration.core.context;

import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.util.BatchCommitter;
import com.neoworks.hybris.util.ListBackedBatchCommitter;

import java.io.File;

public class FileImportContextBuilder extends AbstractImportContextBuilder
{
	public FileImportContext createFileImportContext(final File file)
	{
		final AuditContext auditContext = getAuditService().beginAudit(getSystemArea(), null, "Importing file [{}]", file.getName());
		BatchCommitter batchCommitter = null;
		if (isUseBatchCommitter())
		{
			batchCommitter = new ListBackedBatchCommitter(getBatchSize(), getModelServiceSelectionStrategy().selectModelService(getUseTransactionalModelServiceConfigKey()), true);
		}

		final FileImportContext context = new FileImportContext(file, auditContext, batchCommitter);
		initializeModelService(context);
		return context;
	}
}
