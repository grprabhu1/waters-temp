package com.waters.hybris.integration.core.etl.transformer;

import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.waters.hybris.integration.core.etl.strategy.ConsumerTrackingStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Transformer that logs failed items to the pipeline result map.
 */
public class WatersPipelineResultTransformerSkipRecord<T,R> extends DefaultDataTransformer<T,R>
{

	private static final Logger LOG = LoggerFactory.getLogger(WatersPipelineResultTransformerSkipRecord.class);

	private ConsumerTrackingStrategy consumerTrackingStrategy;

	@Override
	public R transform(final T source)
	{
		try
		{
			return super.transform(source);
		}
		catch (final Exception e)
		{
			LOG.warn("Error while converting item [{}]", source, e);

			markCurrentItemAsError(e);
			return null;
		}
	}

	protected void markCurrentItemAsError(final Exception e)
	{
		try
		{
			getConsumerTrackingStrategy().markAsError(getContext(), "Error while converting item [" + e.getClass() + "]\n"
					+ Stream.of(e.getStackTrace()).map(x -> x.toString() + "\n").collect(Collectors.joining()));
		}
		catch (final EtlTransformationException e1)
		{
			LOG.error("Failed to mark current item as an error due to an exception.", e1);
		}
	}

	protected ConsumerTrackingStrategy getConsumerTrackingStrategy()
	{
		return consumerTrackingStrategy;
	}

	@Required
	public void setConsumerTrackingStrategy(final ConsumerTrackingStrategy consumerTrackingStrategy)
	{
		this.consumerTrackingStrategy = consumerTrackingStrategy;
	}
}
