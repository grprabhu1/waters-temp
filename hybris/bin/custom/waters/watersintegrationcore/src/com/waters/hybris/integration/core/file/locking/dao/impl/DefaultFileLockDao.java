package com.waters.hybris.integration.core.file.locking.dao.impl;

import com.waters.hybris.integration.core.file.locking.dao.FileLockDao;
import com.waters.hybris.integration.core.model.file.locking.FileLockModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Tom Greasley (tom@neoworks.com)
 */
public class DefaultFileLockDao extends DefaultGenericDao<FileLockModel> implements FileLockDao
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultFileLockDao.class);

	public DefaultFileLockDao()
	{
		this(FileLockModel._TYPECODE);
	}

	public DefaultFileLockDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public FileLockModel getFileLockForPath(final String path)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(FileLockModel.FILEPATH, path);
		final List<FileLockModel> results = find(params);

		if (LOG.isWarnEnabled() && results.size() > 1)
		{
			LOG.warn("More than one lock found for path [{}]. Returning the first.", path);
		}

		return results.isEmpty() ? null : results.get(0);
	}

	@Override
	public FileLockModel getFileLockForToken(final String token)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(FileLockModel.TOKEN, token);
		final List<FileLockModel> results = find(params);

		if (LOG.isWarnEnabled() && results.size() > 1)
		{
			LOG.warn("More than one lock found for token [{}]. Returning the first.", token);
		}

		return results.isEmpty() ? null : results.get(0);
	}

}
