package com.waters.hybris.integration.core.etl.strategy.impl;

import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.waters.hybris.integration.core.etl.strategy.ConsumerTrackingStrategy;

/**
 * Default implementation of {@link ConsumerTrackingStrategy}. Does nothing.
 */
public class DefaultConsumerTrackingStrategy implements ConsumerTrackingStrategy
{
	@Override
	public void markAsConsumed(final EtlContext context) throws EtlTransformationException
	{
		/* Do nothing. */
	}

	@Override
	public void markAsComplete(final EtlContext context) throws EtlTransformationException
	{
		/* Do nothing. */
	}

	@Override
	public void markAsError(final EtlContext context, final String errorMessage) throws EtlTransformationException
	{
		/* Do nothing. */
	}

	@Override
	public void markAllConsumedAsComplete(final EtlContext context) throws EtlTransformationException
	{
		/* Do nothing. */
	}

	@Override
	public void markAllConsumedAsError(final EtlContext context, final String errorMessage) throws EtlTransformationException
	{
		/* Do nothing. */
	}
}
