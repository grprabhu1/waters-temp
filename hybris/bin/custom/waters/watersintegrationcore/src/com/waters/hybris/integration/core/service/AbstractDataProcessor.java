package com.waters.hybris.integration.core.service;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.waters.hybris.integration.core.context.ImportContext;
import com.waters.hybris.integration.core.context.JobContext;
import com.waters.hybris.integration.core.exception.ImportAbortedException;
import com.waters.hybris.integration.core.iterator.CloseableIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

/**
 * @author ramana
 */
public abstract class AbstractDataProcessor<inputType, T, C extends ImportContext, context extends JobContext> implements DataProcessor<inputType, context>
{
	private static final Logger LOG = LoggerFactory.getLogger(AbstractDataProcessor.class);

	private ContextAwareConsumer<T, C> consumer;
	private boolean stopAfterItemProcessingFailure = false;
	private boolean noDataToProcessAllowed = true;
	private MetricRegistry metricRegistry;
	private String itemMetricName;
	private String processorMetricName;

	@Override
	public void process(final inputType input, final context jobContext) throws Exception
	{
		final Timer timer = getMetricRegistry().timer(getProcessorMetricName());
		try (final Timer.Context timerContext = timer.time())
		{
			processInternal(input, jobContext);
		}
	}

	public void processInternal(final inputType input, final context jobContext) throws Exception
	{
		try (final C context = createContext(input, jobContext))
		{
			try (final CloseableIterator<T> dataIterator = retrieveData(context))
			{
				if (dataIterator != null)
				{
					final boolean noData = !dataIterator.hasNext();

					while (dataIterator.hasNext())
					{
						if (context.isAborted())
						{
							throw new ImportAbortedException("Job aborted.");
						}

						try
						{
							final T next = dataIterator.next();
							if (next != null)
							{
								consumeItem(context, next);
							}
						}
						catch (final RuntimeException e)
						{
							if (isStopAfterItemProcessingFailure())
							{
								final String message = "An exception occurred while processing an item. Stopping import.";
								LOG.error(message, e);
								context.getAuditContext().addMessage(message, e);
								throw e;
							}
							else
							{
								final String message = "An exception occurred while processing an item. Skipping item import.";
								LOG.error(message, e);
								context.getAuditContext().addMessage(message, e);
								context.handleInputParsingException();
								context.incrementError();
							}
						}
					}

					if (noData && !isNoDataToProcessAllowed())
					{
						handleNoDataToProcessError(context);
					}

					context.flush();

					getConsumer().postAccept(context);
					context.markAsSuccessful();

				}
				else
				{
					handleNoDataToProcessError(context);
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("An exception occurred while importing the data [" + input + "]", e);
			throw e;
		}
	}

	private void consumeItem(final C context, final T next)
	{
		final Timer timer = getMetricRegistry().timer(getItemMetricName());
		try (final Timer.Context timerContext = timer.time())
		{
			getConsumer().accept(next, context);
		}
	}

	protected void handleNoDataToProcessError(final C context)
	{
		final String message = "No data to process.";
		LOG.error(message);
		context.getAuditContext().addMessage(message);
		context.handleNoDataToProcessError();
	}

	protected C createContext(final inputType input, final context jobContext)
	{
		final C context = createContext(input);
		context.setJobContext(jobContext);
		return context;
	}

	protected abstract C createContext(final inputType input);

	protected abstract CloseableIterator<T> retrieveData(C context) throws Exception;

	protected ContextAwareConsumer<T, C> getConsumer()
	{
		return consumer;
	}

	@Required
	public void setConsumer(final ContextAwareConsumer<T, C> consumer)
	{
		this.consumer = consumer;
	}

	protected boolean isStopAfterItemProcessingFailure()
	{
		return stopAfterItemProcessingFailure;
	}

	public void setStopAfterItemProcessingFailure(final boolean stopAfterItemProcessingFailure)
	{
		this.stopAfterItemProcessingFailure = stopAfterItemProcessingFailure;
	}

	protected boolean isNoDataToProcessAllowed()
	{
		return noDataToProcessAllowed;
	}

	public void setNoDataToProcessAllowed(final boolean noDataToProcessAllowed)
	{
		this.noDataToProcessAllowed = noDataToProcessAllowed;
	}

	@Required
	public MetricRegistry getMetricRegistry()
	{
		return metricRegistry;
	}

	public void setMetricRegistry(final MetricRegistry metricRegistry)
	{
		this.metricRegistry = metricRegistry;
	}

	protected String getItemMetricName()
	{
		return itemMetricName;
	}

	protected String getProcessorMetricName()
	{
		return processorMetricName;
	}

	@Required
	public void setMetricName(final String metricName){
		this.processorMetricName = String.format("processor.%s", metricName);
		this.itemMetricName = String.format("processor.%s.singleItemProcess", metricName);
	}


}
