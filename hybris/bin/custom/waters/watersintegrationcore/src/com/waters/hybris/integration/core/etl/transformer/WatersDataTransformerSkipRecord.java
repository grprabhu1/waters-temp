package com.waters.hybris.integration.core.etl.transformer;

import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import com.waters.hybris.integration.core.etl.strategy.EtlItemFilterStrategy;
import de.hybris.platform.jalo.c2l.LocalizableItem;
import de.hybris.platform.servicelayer.model.AbstractItemModel;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.List;

public class WatersDataTransformerSkipRecord<T,R> extends DefaultDataTransformer<T,R>
{
	private static final String NEW_LINE = System.lineSeparator();
	@SuppressWarnings("PMD.LoggerIsNotStaticFinal")
	protected final Logger LOG = LoggerFactory.getLogger(this.getClass());
	private String failingRecordListKey;
	private SessionService sessionService;
	private boolean languageAware;
	private EtlItemFilterStrategy<Object> etlItemFilterStrategy = (EtlContext context, Object item) -> true;

	@Override
	public void init(final EtlContext context) throws EtlValidationException
	{
		super.init(context);
		if (isLanguageAware())
		{
			getSessionService().setAttribute(LocalizableItem.LANGUAGE_FALLBACK_ENABLED, true);
			getSessionService().setAttribute(AbstractItemModel.LANGUAGE_FALLBACK_ENABLED_SERVICE_LAYER, true);
		}

	}

	@Override
	public R transform(final T source)
	{
		if (shouldTransform(source))
		{
			return applyTransformation(source);
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("source [{} - {}] did not match filterStrategy [{}]", source.getClass(), source, getEtlItemFilterStrategy());
		}
		return null;
	}

	protected boolean shouldTransform(final T source)
	{
		return getEtlItemFilterStrategy().matches(getContext(), source);
	}

	protected R applyTransformation(final T source)
	{
		try
		{
			return internalTransform(source);
		}
		catch (final Exception ex)
		{
			LOG.warn(String.format("Error while converting item [%s]", source), ex);
			recordError(source, ex);
			return null;
		}
	}

	protected R internalTransform(final T source)
	{
		return super.transform(source);
	}

	protected void recordError(final Object source, final Exception ex)
	{
		final Object fails = getContext().getParameters().get(getFailingRecordListKey());
		final List<Object> messages;
		if (fails instanceof List)
		{
			//noinspection unchecked
			messages = (List<Object>) fails;
		}
		else
		{
			messages = new ArrayList<>();
			getContext().getParameters().put(getFailingRecordListKey(), messages);
		}
		final StringBuilder messageBuilder = new StringBuilder("Error occurred during conversion, the record will be skipped.");

		messageBuilder.append(NEW_LINE)
				.append(ExceptionUtils.getRootCauseMessage(ex));

		if (source != null)
		{
			messageBuilder.append(NEW_LINE);
			messageBuilder.append("Source: ").append(source.toString());
		}

		messages.add(messageBuilder.toString());
	}

	protected String getFailingRecordListKey()
	{
		return failingRecordListKey;
	}

	@Required
	public void setFailingRecordListKey(final String failingRecordListKey)
	{
		this.failingRecordListKey = failingRecordListKey;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected boolean isLanguageAware()
	{
		return languageAware;
	}

	@Required
	public void setLanguageAware(final boolean languageAware)
	{
		this.languageAware = languageAware;
	}

	protected EtlItemFilterStrategy<Object> getEtlItemFilterStrategy()
	{
		return etlItemFilterStrategy;
	}

	public void setEtlItemFilterStrategy(final EtlItemFilterStrategy<Object> etlItemFilterStrategy)
	{
		this.etlItemFilterStrategy = etlItemFilterStrategy;
	}
}
