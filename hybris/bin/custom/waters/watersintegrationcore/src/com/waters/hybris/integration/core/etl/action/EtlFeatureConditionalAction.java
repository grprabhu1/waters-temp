package com.waters.hybris.integration.core.etl.action;

import com.neoworks.hybris.etl.action.EtlAction;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.neoworks.hybris.feature.FeatureService;
import org.springframework.beans.factory.annotation.Required;

/**
 * ETL action that chooses between two different ETL actions to perform based on the state of a feature service flag.
 */
public class EtlFeatureConditionalAction implements EtlAction
{

	private FeatureService featureService;
	private EtlAction featureActivatedAction;
	private EtlAction featureDeactivatedAction;

	private String feature;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void execute(final EtlContext context) throws EtlTransformationException
	{
		if (getFeatureService().isFeatureOn(getFeature()))
		{
			getFeatureActivatedAction().execute(context);
		}
		else
		{
			getFeatureDeactivatedAction().execute(context);
		}
	}

	protected FeatureService getFeatureService()
	{
		return featureService;
	}

	@Required
	public void setFeatureService(final FeatureService featureService)
	{
		this.featureService = featureService;
	}

	protected EtlAction getFeatureActivatedAction()
	{
		return featureActivatedAction;
	}

	@Required
	public void setFeatureActivatedAction(final EtlAction featureActivatedAction)
	{
		this.featureActivatedAction = featureActivatedAction;
	}

	protected EtlAction getFeatureDeactivatedAction()
	{
		return featureDeactivatedAction;
	}

	@Required
	public void setFeatureDeactivatedAction(final EtlAction featureDeactivatedAction)
	{
		this.featureDeactivatedAction = featureDeactivatedAction;
	}

	protected String getFeature()
	{
		return feature;
	}

	@Required
	public void setFeature(final String feature)
	{
		this.feature = feature;
	}
}
