package com.waters.hybris.integration.core.service.impl;

import com.waters.hybris.integration.core.context.FileImportContext;
import com.waters.hybris.integration.core.context.FileImportContextBuilder;
import com.waters.hybris.integration.core.context.JobContext;
import com.waters.hybris.integration.core.iterator.CloseableIterator;
import com.waters.hybris.integration.core.service.AbstractDataProcessor;
import com.waters.hybris.integration.core.splitter.Splitter;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class DefaultFileProcessor<T> extends AbstractDataProcessor<File, T, FileImportContext, JobContext>
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultFileProcessor.class);

	private FileImportContextBuilder fileImportContextBuilder;
	private Splitter<T> splitter;

	@Override
	//@Impersonate(disableRestrictions = true)
	public void process(final File file, final JobContext jobContext) throws Exception
	{
		super.process(file, jobContext);
	}

	@Override
	protected FileImportContext createContext(final File file)
	{
		Assert.notNull(file, "file cannot be null");

		return getFileImportContextBuilder().createFileImportContext(file);
	}

	@Override
	protected CloseableIterator<T> retrieveData(final FileImportContext context) throws IOException
	{
		if (context.getInput() != null)
		{
			final File file = (File) context.getInput();
			try
			{
				final FileInputStream inputStream = FileUtils.openInputStream(file);
				return getSplitter().split(inputStream, context);
			}
			catch (final IOException e)
			{
				LOG.error("Cannot read file [{}]", file.getName(), e);
				return null;
			}
		}

		return null;
	}

	protected FileImportContextBuilder getFileImportContextBuilder()
	{
		return fileImportContextBuilder;
	}

	@Required
	public void setFileImportContextBuilder(final FileImportContextBuilder fileImportContextBuilder)
	{
		this.fileImportContextBuilder = fileImportContextBuilder;
	}

	protected Splitter<T> getSplitter()
	{
		return splitter;
	}

	@Required
	public void setSplitter(final Splitter<T> splitter)
	{
		this.splitter = splitter;
	}

}
