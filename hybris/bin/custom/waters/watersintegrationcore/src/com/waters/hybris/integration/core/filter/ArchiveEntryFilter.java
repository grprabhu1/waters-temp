package com.waters.hybris.integration.core.filter;


public interface ArchiveEntryFilter<T>
{
	boolean allow(T entry);
}
