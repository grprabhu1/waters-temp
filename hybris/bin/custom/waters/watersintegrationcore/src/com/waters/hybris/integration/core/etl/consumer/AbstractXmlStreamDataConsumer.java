package com.waters.hybris.integration.core.etl.consumer;

import com.neoworks.hybris.etl.action.EtlAction;
import com.neoworks.hybris.etl.consumers.DataConsumer;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import com.waters.hybris.integration.core.etl.strategy.ConsumerTrackingStrategy;
import com.waters.hybris.integration.core.xml.stream.CDataElementXmlStreamWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileOutputStream;
import java.util.List;

public abstract class AbstractXmlStreamDataConsumer<T, K> implements DataConsumer<T>
{
	private static final Logger LOG = LoggerFactory.getLogger(AbstractXmlStreamDataConsumer.class);

	private Marshaller marshaller;
	private XMLOutputFactory outputFactory;
	private EtlContext etlContext;
	private ConsumerTrackingStrategy consumerTrackingStrategy;

	private String temporaryFileSuffix = ".xml";
	private String temporaryFilePrefix;
	private String rootElementName;
	private Class<T> dataType;
	private String fileParamKey;
	private EtlAction mediaActionList;
	private List<String> cdataElements;

	@Override
	public void init(final EtlContext etlContext) throws EtlValidationException
	{
		setContext(etlContext);

		try
		{
			outputFactory = XMLOutputFactory.newFactory();
			marshaller = JAXBContext.newInstance(getDataType()).createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
		}
		catch (final JAXBException e)
		{
			throw new EtlValidationException("Failed to initialize the consumer.", e);
		}
	}

	protected void setContext(final EtlContext etlContext)
	{
		this.etlContext = etlContext;
	}

	protected XMLStreamWriter createStreamWriter(final FileOutputStream fileOutputStream) throws XMLStreamException
	{
		final List<String> cdataElements = getCdataElements();
		final XMLStreamWriter xmlStreamWriter = getOutputFactory().createXMLStreamWriter(fileOutputStream);
		if (cdataElements != null)
		{
			return new CDataElementXmlStreamWriter(xmlStreamWriter, cdataElements);
		}
		else
		{
			return xmlStreamWriter;
		}
	}

	protected void markAsError(final String message, final Exception e)
	{
		try
		{
			getConsumerTrackingStrategy().markAsError(getEtlContext(), message + "[" + e.getMessage() + "]");
		}
		catch (final EtlTransformationException ex)
		{
			LOG.error("Failed to mark item as error.", ex);
		}
	}

	protected void markAllConsumedAsError(final String message, final Exception e)
	{
		try
		{
			getConsumerTrackingStrategy().markAllConsumedAsError(getEtlContext(), message + "[" + e.getMessage() + "]");
		}
		catch (final EtlTransformationException ex)
		{
			LOG.error("Failed to mark all consumed as error.", ex);
		}
	}

	protected Marshaller getMarshaller()
	{
		return marshaller;
	}

	protected XMLOutputFactory getOutputFactory()
	{
		return outputFactory;
	}

	protected EtlContext getEtlContext()
	{
		return etlContext;
	}

	protected String getTemporaryFilePrefix()
	{
		return temporaryFilePrefix;
	}

	@Required
	public void setTemporaryFilePrefix(final String temporaryFilePrefix)
	{
		this.temporaryFilePrefix = temporaryFilePrefix;
	}

	protected String getTemporaryFileSuffix()
	{
		return temporaryFileSuffix;
	}

	@Required
	public void setTemporaryFileSuffix(final String temporaryFileSuffix)
	{
		this.temporaryFileSuffix = temporaryFileSuffix;
	}

	protected String getRootElementName()
	{
		return rootElementName;
	}

	@Required
	public void setRootElementName(final String rootElementName)
	{
		this.rootElementName = rootElementName;
	}

	protected Class<T> getDataType()
	{
		return dataType;
	}

	@Required
	public void setDataType(final Class<T> dataType)
	{
		this.dataType = dataType;
	}

	protected String getFileParamKey()
	{
		return fileParamKey;
	}

	@Required
	public void setFileParamKey(final String fileParamKey)
	{
		this.fileParamKey = fileParamKey;
	}

	protected EtlAction getMediaActionList()
	{
		return mediaActionList;
	}

	@Required
	public void setMediaActionList(final EtlAction mediaActionList)
	{
		this.mediaActionList = mediaActionList;
	}

	public List<String> getCdataElements()
	{
		return cdataElements;
	}

	public void setCdataElements(final List<String> cdataElements)
	{
		this.cdataElements = cdataElements;
	}


	protected ConsumerTrackingStrategy getConsumerTrackingStrategy()
	{
		return consumerTrackingStrategy;
	}

	@Required
	public void setConsumerTrackingStrategy(final ConsumerTrackingStrategy consumerTrackingStrategy)
	{
		this.consumerTrackingStrategy = consumerTrackingStrategy;
	}
}
