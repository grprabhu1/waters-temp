package com.waters.hybris.integration.core.hystrix.impl;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixCommandProperties;
import com.waters.hybris.integration.core.hystrix.HystrixCommandPropertiesProvider;
import org.springframework.beans.factory.annotation.Required;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DefaultHystrixCommandPropertiesProvider implements HystrixCommandPropertiesProvider
{
	private final HystrixCommandGroupKey hystrixCommandGroupKey;
	private final ConcurrentHashMap<String, HystrixCommand.Setter> cachedCommandProperties = new ConcurrentHashMap<>();

	private int threadTimeoutMillis;
	private int circuitBreakerFailureLimit;
	private int circuitBreakerSleepWindow;
	private int metricsWindowMillis;
	private int metricsWindowBuckets;
	private boolean fallbackEnabled;

	public DefaultHystrixCommandPropertiesProvider(final String hystrixCommandGroupKey)
	{
		this.hystrixCommandGroupKey = HystrixCommandGroupKey.Factory.asKey(hystrixCommandGroupKey);
	}

	@Override
	public HystrixCommand.Setter createCommandProperties(final String commandKey)
	{
		return getCachedCommandProperties().computeIfAbsent(commandKey,
				(key) -> HystrixCommand.Setter.withGroupKey(hystrixCommandGroupKey)
						.andCommandKey(HystrixCommandKey.Factory.asKey(key))
						.andCommandPropertiesDefaults(getHystrixCommandPropertiesSetter())
		);
	}

	private HystrixCommandProperties.Setter getHystrixCommandPropertiesSetter()
	{
		return HystrixCommandProperties.Setter()
				.withFallbackEnabled(isFallbackEnabled())
				.withExecutionTimeoutInMilliseconds(getThreadTimeoutMillis())
				.withCircuitBreakerRequestVolumeThreshold(getCircuitBreakerFailureLimit())
				.withCircuitBreakerSleepWindowInMilliseconds(getCircuitBreakerSleepWindow())
				.withMetricsRollingStatisticalWindowInMilliseconds(getMetricsWindowMillis())
				.withMetricsRollingStatisticalWindowBuckets(getMetricsWindowBuckets());
	}

	protected int getThreadTimeoutMillis()
	{
		return threadTimeoutMillis;
	}

	@Required
	public void setThreadTimeoutMillis(final int threadTimeoutMillis)
	{
		this.threadTimeoutMillis = threadTimeoutMillis;
	}

	protected int getCircuitBreakerFailureLimit()
	{
		return circuitBreakerFailureLimit;
	}

	@Required
	public void setCircuitBreakerFailureLimit(final int circuitBreakerFailureLimit)
	{
		this.circuitBreakerFailureLimit = circuitBreakerFailureLimit;
	}

	protected int getCircuitBreakerSleepWindow()
	{
		return circuitBreakerSleepWindow;
	}

	@Required
	public void setCircuitBreakerSleepWindow(final int circuitBreakerSleepWindow)
	{
		this.circuitBreakerSleepWindow = circuitBreakerSleepWindow;
	}

	protected int getMetricsWindowMillis()
	{
		return metricsWindowMillis;
	}

	@Required
	public void setMetricsWindowMillis(final int metricsWindowMillis)
	{
		this.metricsWindowMillis = metricsWindowMillis;
	}

	protected int getMetricsWindowBuckets()
	{
		return metricsWindowBuckets;
	}

	@Required
	public void setMetricsWindowBuckets(final int metricsWindowBuckets)
	{
		this.metricsWindowBuckets = metricsWindowBuckets;
	}

	protected Map<String, HystrixCommand.Setter> getCachedCommandProperties()
	{
		return cachedCommandProperties;
	}

	protected boolean isFallbackEnabled()
	{
		return fallbackEnabled;
	}

	@Required
	public void setFallbackEnabled(final boolean fallbackEnabled)
	{
		this.fallbackEnabled = fallbackEnabled;
	}

}
