package com.waters.hybris.integration.core.etl.consumer;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.dataformat.csv.CsvGenerator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.neoworks.hybris.etl.action.EtlAction;
import com.neoworks.hybris.etl.consumers.DataConsumer;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import com.waters.hybris.integration.core.etl.strategy.ConsumerTrackingStrategy;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class CsvDataConsumer<T> implements DataConsumer<T>
{
	protected static final String TEMP_FILE_SUFFIX = ".csv";
	private static final Logger LOG = LoggerFactory.getLogger(CsvDataConsumer.class);
	private EtlContext etlContext;
	private File file;
	private SequenceWriter writer;
	private boolean appendToFile = false;
	private boolean atLeastOneItem = false;
	private boolean alwaysQuoteStrings = false;
	private boolean forceWriteFileWhenEmpty = false;

	private Class<T> dataType;
	private char delimiter;
	private String temporaryFilePrefix;
	private String fileParamKey;
	private ConsumerTrackingStrategy consumerTrackingStrategy;
	private EtlAction mediaActionList;

	@Override
	public void init(final EtlContext etlContext) throws EtlValidationException
	{
		setEtlContext(etlContext);

		try
		{
			file = File.createTempFile(getTemporaryFilePrefix(), TEMP_FILE_SUFFIX);
			writer = createWriter(getDataType(), file);
		}
		catch (final IOException e)
		{
			closeStreams();
			throw new EtlValidationException("Failed to initialize the consumer.", e);
		}
	}

	@Override
	public void put(final T data)
	{
		try
		{
			writer.write(data);
			atLeastOneItem = true;
			getConsumerTrackingStrategy().markAsConsumed(getEtlContext());
		}
		catch (final IOException e)
		{
			LOG.error("Could not write data to file", e);
		}
		catch (final EtlTransformationException e)
		{
			LOG.error("Failed to mark item as consumed.", e);
		}
	}

	@Override
	public void start() throws EtlTransformationException
	{
		// Nothing to do here
	}

	@Override
	public void end(final boolean b) throws EtlTransformationException
	{
		closeStreams();

		if (hasAtLeastOneItem() || isForceWriteFileWhenEmpty())
		{
			getEtlContext().getParameters().put(getFileParamKey(), file.getAbsolutePath());
			try
			{
				getMediaActionList().execute(getEtlContext());
			}
			catch (final EtlTransformationException e)
			{
				throw new EtlTransformationException("Failed to create media for the recipients file.", e);
			}

			getConsumerTrackingStrategy().markAllConsumedAsComplete(getEtlContext());
		}
	}

	protected CsvSchema createCsvSchema(final Class classType, final CsvMapper mapper) throws IOException
	{
		return mapper.schemaFor(classType).withHeader().withColumnSeparator(getDelimiter());
	}

	protected SequenceWriter createWriter(final Class classType, final File file) throws IOException
	{
		final CsvMapper mapper = new CsvMapper();
		final CsvSchema schema = createCsvSchema(classType, mapper);
		final ObjectWriter objectWriter = mapper.writer(schema);
		if(isAlwaysQuoteStrings())
		{
			mapper.configure(CsvGenerator.Feature.ALWAYS_QUOTE_STRINGS, true);
		}
		final FileOutputStream tempFileOutputStream = FileUtils.openOutputStream(file, isAppendToFile());
		final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(tempFileOutputStream, 1024);
		final OutputStreamWriter writerOutputStream = new OutputStreamWriter(bufferedOutputStream, "UTF-8");

		return objectWriter.writeValues(writerOutputStream);
	}

	protected void closeStreams()
	{
		try
		{
			if (writer != null)
			{
				writer.close();
			}
		}
		catch (final IOException e)
		{
			LOG.error("Could not close writer", e);
		}
	}

	protected EtlContext getEtlContext()
	{
		return etlContext;
	}

	protected void setEtlContext(final EtlContext etlContext)
	{
		this.etlContext = etlContext;
	}

	protected boolean hasAtLeastOneItem()
	{
		return atLeastOneItem;
	}

	protected void setAtLeastOneItem(final boolean atLeastOneItem)
	{
		this.atLeastOneItem = atLeastOneItem;
	}

	protected SequenceWriter getWriter()
	{
		return writer;
	}

	protected void setWriter(final SequenceWriter writer)
	{
		this.writer = writer;
	}

	protected File getFile()
	{
		return file;
	}

	protected void setFile(final File file)
	{
		this.file = file;
	}

	protected String getTemporaryFilePrefix()
	{
		return temporaryFilePrefix;
	}

	@Required
	public void setTemporaryFilePrefix(final String temporaryFilePrefix)
	{
		this.temporaryFilePrefix = temporaryFilePrefix;
	}

	protected Class<T> getDataType()
	{
		return dataType;
	}

	@Required
	public void setDataType(final Class<T> dataType)
	{
		this.dataType = dataType;
	}

	protected char getDelimiter()
	{
		return delimiter;
	}

	@Required
	public void setDelimiter(final char delimiter)
	{
		this.delimiter = delimiter;
	}

	protected ConsumerTrackingStrategy getConsumerTrackingStrategy()
	{
		return consumerTrackingStrategy;
	}

	@Required
	public void setConsumerTrackingStrategy(final ConsumerTrackingStrategy consumerTrackingStrategy)
	{
		this.consumerTrackingStrategy = consumerTrackingStrategy;
	}

	protected EtlAction getMediaActionList()
	{
		return mediaActionList;
	}

	@Required
	public void setMediaActionList(final EtlAction mediaActionList)
	{
		this.mediaActionList = mediaActionList;
	}

	protected String getFileParamKey()
	{
		return fileParamKey;
	}

	@Required
	public void setFileParamKey(final String fileParamKey)
	{
		this.fileParamKey = fileParamKey;
	}

	public boolean isAppendToFile()
	{
		return appendToFile;
	}

	public void setAppendToFile(final boolean appendToFile)
	{
		this.appendToFile = appendToFile;
	}

	public boolean isAlwaysQuoteStrings()
	{
		return alwaysQuoteStrings;
	}

	public void setAlwaysQuoteStrings(final boolean alwaysQuoteStrings)
	{
		this.alwaysQuoteStrings = alwaysQuoteStrings;
	}

	public boolean isForceWriteFileWhenEmpty()
	{
		return forceWriteFileWhenEmpty;
	}

	public void setForceWriteFileWhenEmpty(final boolean forceWriteFileWhenEmpty)
	{
		this.forceWriteFileWhenEmpty = forceWriteFileWhenEmpty;
	}
}
