package com.waters.hybris.integration.core.context;

import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.util.BatchCommitter;

/**
 * Not thread safe.
 */
public class DataImportContext extends ImportContext<String>
{
	public DataImportContext(final String type, final AuditContext auditContext, final BatchCommitter batchCommitter)
	{
		super(type, auditContext, batchCommitter);
	}

	protected String getDataType()
	{
		return getInput() != null ? getInput() : null;
	}

	@Override
	protected String generateSuccessMessage()
	{
		return String.format("Imported data [%s]. Processed: [%d]. Success: [%d]. Errors: [%d].", getDataType(), getTotalProcessed(), getTotalSuccess(), getTotalError());
	}

	@Override
	protected String generateErrorMessage()
	{
		return String.format("Failed to import data [%s]. Processed: [%d]. Success: [%d]. Errors: [%d].", getDataType(), getTotalProcessed(), getTotalSuccess(), getTotalError());
	}

	@Override
	public String getSource()
	{
		return null;
	}
}
