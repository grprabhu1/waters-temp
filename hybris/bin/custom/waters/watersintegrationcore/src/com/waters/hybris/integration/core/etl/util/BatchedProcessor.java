package com.waters.hybris.integration.core.etl.util;

import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.function.Function;

/**
 * Helper class to process a set of data in batches and then return the complete processed result list.
 *
 * @param <PT> Data type to process
 * @param <RT> Result data type
 */
public class BatchedProcessor<PT, RT>
{

	private final int batchSize;
	private final Function<List<PT>, Collection<RT>> processingFunction;

	/**
	 * Create a processor.
	 *
	 * @param batchSize          size of the batched to process
	 * @param processingFunction function to process batches.
	 * @throws IllegalArgumentException is {@code batchSize} is not greater than zero or {@code processingFunction} is null
	 */
	public BatchedProcessor(final int batchSize, final Function<List<PT>, Collection<RT>> processingFunction)
	{
		Assert.isTrue(batchSize > 0, "batchSize must be greater than zero");
		Assert.notNull(processingFunction, "processingFunction cannot be null");

		this.batchSize = batchSize;
		this.processingFunction = processingFunction;
	}

	/**
	 * Processes the passed in list of data in batches. This is done as a sliding window of {@code batchSize} items. Modifications to the
	 * batched lists passed to the processing function will be reflected in the passed in list of data.
	 *
	 * @param data            data to process in batches
	 * @param distinctResults whether the results of the processing should be distinct
	 * @return collection of processed data
	 */
	public Collection<RT> processBatchedData(final List<PT> data, final boolean distinctResults)
	{
		Assert.notNull(data, "data cannot be null");

		final Collection<RT> results;
		if (distinctResults)
		{
			results = new LinkedHashSet<>();
		}
		else
		{
			results = new ArrayList<>();
		}
		int remainingDataLength = data.size();
		int lastPosition = 0;
		while (remainingDataLength > 0)
		{
			final int currentBatchSize = Math.min(remainingDataLength, batchSize);
			results.addAll(processingFunction.apply(data.subList(lastPosition, lastPosition + currentBatchSize)));
			lastPosition += currentBatchSize;
			remainingDataLength -= currentBatchSize;
		}
		return results;
	}

}
