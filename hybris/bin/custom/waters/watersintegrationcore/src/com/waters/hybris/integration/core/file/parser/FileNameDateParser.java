package com.waters.hybris.integration.core.file.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Required;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileNameDateParser implements InitializingBean
{
	private static final Logger LOG = LoggerFactory.getLogger(FileNameDateParser.class);

	private Pattern extractDatePattern;
	private DateTimeFormatter formatter;

	private String extractDateRegex;
	private String dateTimePattern;

	public LocalDateTime getLocalDateTime(final String fileName)
	{
		final Matcher matcher = getExtractDatePattern().matcher(fileName);
		if (matcher.matches())
		{
			return LocalDateTime.parse(matcher.group(1), getFormatter());
		}
		LOG.error("File name [{}] does not match pattern [{}].", fileName, getExtractDatePattern());
		throw new RuntimeException("File name [" + fileName + "] does not match pattern [" + getExtractDatePattern() + "].");
	}

	public Date getDate(final String fileName)
	{
		final LocalDateTime localDateTime = getLocalDateTime(fileName);
		return localDateTime != null ? Date.from(localDateTime.atZone(ZoneOffset.UTC).toInstant()) : null;
	}

	@Override
	public void afterPropertiesSet() throws Exception
	{
		setFormatter(DateTimeFormatter.ofPattern(getDateTimePattern()).withResolverStyle(ResolverStyle.STRICT));
		setExtractDatePattern(Pattern.compile(getExtractDateRegex()));
	}

	protected String getExtractDateRegex()
	{
		return extractDateRegex;
	}

	@Required
	public void setExtractDateRegex(final String extractDateRegex)
	{
		this.extractDateRegex = extractDateRegex;
	}

	protected String getDateTimePattern()
	{
		return dateTimePattern;
	}

	@Required
	public void setDateTimePattern(final String dateTimePattern)
	{
		this.dateTimePattern = dateTimePattern;
	}

	protected DateTimeFormatter getFormatter()
	{
		return formatter;
	}

	public void setFormatter(final DateTimeFormatter formatter)
	{
		this.formatter = formatter;
	}

	protected Pattern getExtractDatePattern()
	{
		return extractDatePattern;
	}

	public void setExtractDatePattern(final Pattern extractDatePattern)
	{
		this.extractDatePattern = extractDatePattern;
	}
}
