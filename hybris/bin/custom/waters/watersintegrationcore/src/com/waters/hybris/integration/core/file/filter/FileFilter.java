package com.waters.hybris.integration.core.file.filter;

import java.io.File;

/**
 * @author Tom Greasley (tom@neoworks.com)
 */
public interface FileFilter
{
	boolean accept(final File file);
}
