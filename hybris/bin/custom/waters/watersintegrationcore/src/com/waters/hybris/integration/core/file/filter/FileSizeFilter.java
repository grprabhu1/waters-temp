package com.waters.hybris.integration.core.file.filter;

import com.neoworks.hybris.audit.enums.SystemArea;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import java.io.File;

public class FileSizeFilter implements FileFilter
{
	private static final Logger LOG = LoggerFactory.getLogger(FileSizeFilter.class);

	private ConfigurationService configurationService;
	private String feedSizeThresholdKey;
	private SystemArea systemArea;

	@Override
	public boolean accept(final File file)
	{
		Assert.notNull(file, "file cannot be null");

		final long threshold = getConfigurationService().getConfiguration().getLong(getFeedSizeThresholdKey(), 0);
		if (file.exists() && file.length() > threshold)
		{
			getLogger(getSystemArea()).debug("{} is valid", file.getName());
			return true;
		}
		else
		{
			final String errorMessage = String.format("%s is not valid, reason: file size is less than the threshold or empty", file.getName());
			getLogger(getSystemArea()).error(errorMessage, file.getName());
			throw new RuntimeException(errorMessage);
		}
	}

	protected Logger getLogger(final SystemArea systemArea)
	{
		return LoggerFactory.getLogger(systemArea.name());
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected String getFeedSizeThresholdKey()
	{
		return feedSizeThresholdKey;
	}

	@Required
	public void setFeedSizeThresholdKey(final String feedSizeThresholdKey)
	{
		this.feedSizeThresholdKey = feedSizeThresholdKey;
	}

	protected SystemArea getSystemArea()
	{
		return systemArea;
	}

	@Required
	public void setSystemArea(final SystemArea systemArea)
	{
		this.systemArea = systemArea;
	}
}
