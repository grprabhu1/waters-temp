package com.waters.hybris.integration.core.service.impl;

import com.waters.hybris.integration.core.context.ImportContext;
import com.waters.hybris.integration.core.exception.InvalidItemException;
import com.waters.hybris.integration.core.service.ContextAwareConsumer;

public abstract class AbstractContextAwareConsumer<T, C extends ImportContext> implements ContextAwareConsumer<T, C>
{
	@Override
	public void accept(final T item, final C context)
	{
		if (isValid(item, context))
		{
			internalProcess(item, context);
		}
		else
		{
			context.getAuditContext().addMessage("Invalid item [{}]", item);
			throw new InvalidItemException("Invalid item [" + item + "]");
		}
	}

	protected abstract boolean isValid(final T item, final C context);

	protected abstract void internalProcess(final T item, final C context);
}
