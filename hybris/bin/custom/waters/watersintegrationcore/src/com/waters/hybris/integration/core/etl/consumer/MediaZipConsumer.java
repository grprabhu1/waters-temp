package com.waters.hybris.integration.core.etl.consumer;

import com.neoworks.hybris.etl.action.EtlAction;
import com.neoworks.hybris.etl.consumers.DataConsumer;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import com.waters.hybris.integration.core.etl.strategy.ConsumerTrackingStrategy;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;

public class MediaZipConsumer implements DataConsumer<MediaModel>
{

	protected static final String TEMP_FILE_SUFFIX = ".zip";
	private static final Logger LOG = LoggerFactory.getLogger(MediaZipConsumer.class);
	private EtlContext etlContext;
	private File file;
	private ZipArchiveOutputStream writer;
	private boolean atLeastOneItem;

	private String temporaryFilePrefix;
	private String fileParamKey;
	private ConsumerTrackingStrategy consumerTrackingStrategy;
	private EtlAction mediaActionList;
	private MediaService mediaService;

	@Override
	public void init(final EtlContext etlContext) throws EtlValidationException
	{
		setEtlContext(etlContext);

		try
		{
			file = File.createTempFile(getTemporaryFilePrefix(), TEMP_FILE_SUFFIX);

			final FileOutputStream fos = new FileOutputStream(file);
			final BufferedOutputStream bos = new BufferedOutputStream(fos);
			writer = new ZipArchiveOutputStream(bos);
		}
		catch (final IOException e)
		{
			closeStreams();
			throw new EtlValidationException("Failed to initialize the consumer.", e);
		}
	}
	@Override
	public void put(final MediaModel mediaModel)
	{
		try
		{
			writeFilesToZipStream(mediaModel);
			atLeastOneItem = true;
			getConsumerTrackingStrategy().markAsConsumed(getEtlContext());
		}
		catch (final IOException e)
		{
			LOG.error("Could not write data to file", e);
		}
		catch (final EtlTransformationException e)
		{
			LOG.error("Failed to mark item as consumed.", e);
		}
	}

	protected void writeFilesToZipStream(final MediaModel mediaModel) throws IOException
	{
			final ZipArchiveEntry entry = new ZipArchiveEntry(mediaModel.getCode());
			entry.setMethod(ZipEntry.DEFLATED);

			try
			{
				writer.putArchiveEntry(entry);
				try (InputStream fileInputStream = getMediaService().getStreamFromMedia(mediaModel);
					 BufferedInputStream bis = new BufferedInputStream(fileInputStream))
				{
					IOUtils.copy(bis, writer);
					writer.closeArchiveEntry();
				}
			}
			catch (final IOException e)
			{
				// Log error and go to next entry
				LOG.error("Unable to add zip entry for the media [{}]. Skip to next media", mediaModel.getCode());
				throw e;
			}
	}

	@Override
	public void start() throws EtlTransformationException
	{
		//nothing
	}

	@Override
	public void end(final boolean b) throws EtlTransformationException
	{
		closeStreams();

		if (hasAtLeastOneItem())
		{
			getEtlContext().getParameters().put(getFileParamKey(), file.getAbsolutePath());
			try
			{
				getMediaActionList().execute(getEtlContext());
			}
			catch (final EtlTransformationException e)
			{
				throw new EtlTransformationException("Failed to create zip media for the recipients file.", e);
			}

			getConsumerTrackingStrategy().markAllConsumedAsComplete(getEtlContext());
		}
	}

	protected void closeStreams()
	{
		try
		{
			if (writer != null)
			{
				writer.close();
			}
		}
		catch (final IOException e)
		{
			LOG.error("Could not close writer", e);
		}
	}

	protected EtlContext getEtlContext()
	{
		return etlContext;
	}

	protected void setEtlContext(final EtlContext etlContext)
	{
		this.etlContext = etlContext;
	}

	protected File getFile()
	{
		return file;
	}

	protected void setFile(final File file)
	{
		this.file = file;
	}

	protected String getTemporaryFilePrefix()
	{
		return temporaryFilePrefix;
	}

	@Required
	public void setTemporaryFilePrefix(final String temporaryFilePrefix)
	{
		this.temporaryFilePrefix = temporaryFilePrefix;
	}

	protected boolean hasAtLeastOneItem()
	{
		return atLeastOneItem;
	}

	protected void setAtLeastOneItem(final boolean atLeastOneItem)
	{
		this.atLeastOneItem = atLeastOneItem;
	}

	protected ConsumerTrackingStrategy getConsumerTrackingStrategy()
	{
		return consumerTrackingStrategy;
	}

	@Required
	public void setConsumerTrackingStrategy(final ConsumerTrackingStrategy consumerTrackingStrategy)
	{
		this.consumerTrackingStrategy = consumerTrackingStrategy;
	}

	protected EtlAction getMediaActionList()
	{
		return mediaActionList;
	}

	@Required
	public void setMediaActionList(final EtlAction mediaActionList)
	{
		this.mediaActionList = mediaActionList;
	}

	protected String getFileParamKey()
	{
		return fileParamKey;
	}

	@Required
	public void setFileParamKey(final String fileParamKey)
	{
		this.fileParamKey = fileParamKey;
	}

	public MediaService getMediaService()
	{
		return mediaService;
	}

	@Required
	public void setMediaService(final MediaService mediaService)
	{
		this.mediaService = mediaService;
	}
}
