package com.waters.hybris.integration.core.file.provider;

import java.io.File;
import java.util.Date;

public interface FileTimestampProvider
{
	Date getFileTimestamp(File file);

	Date getFileTimestamp(String filename);
}
