package com.waters.hybris.integration.core.service.impl;

import com.waters.hybris.integration.core.context.DataImportContext;
import com.waters.hybris.integration.core.context.DataImportContextBuilder;
import com.waters.hybris.integration.core.context.JobContext;
import com.waters.hybris.integration.core.iterator.CloseableIterator;
import com.waters.hybris.integration.core.service.AbstractDataProcessor;
import com.waters.hybris.integration.core.service.ContextAwareSupplier;
import org.springframework.beans.factory.annotation.Required;

import java.io.Closeable;
import java.util.Iterator;

/**
 * @author ramana
 */
public class DefaultDataProcessor<T, C extends DataImportContext> extends AbstractDataProcessor<String, T, C, JobContext>
{
	private DataImportContextBuilder dataImportContextBuilder;
	private ContextAwareSupplier<T, C> supplier;
	private String dataType;

	@Override
	public void process(final String input, final JobContext jobContext) throws Exception
	{
		super.process(input, jobContext);
	}

	@Override
	protected C createContext(final String input)
	{
		return (C) getDataImportContextBuilder().createDataImportContext(getDataType());
	}

	@Override
	protected CloseableIterator<T> retrieveData(final C context)
	{
		final Iterator<T> iterator = getSupplier().get(context);
		return new CloseableIterator<T>()
		{
			@Override
			public Closeable getCloseable()
			{
				return null;
			}

			@Override
			public Iterator<T> getIterator()
			{
				return iterator;
			}
		};
	}

	protected DataImportContextBuilder getDataImportContextBuilder()
	{
		return dataImportContextBuilder;
	}

	@Required
	public void setDataImportContextBuilder(final DataImportContextBuilder dataImportContextBuilder)
	{
		this.dataImportContextBuilder = dataImportContextBuilder;
	}

	protected ContextAwareSupplier<T, C> getSupplier()
	{
		return supplier;
	}

	@Required
	public void setSupplier(final ContextAwareSupplier<T, C> supplier)
	{
		this.supplier = supplier;
	}

	protected String getDataType()
	{
		return dataType;
	}

	@Required
	public void setDataType(final String dataType)
	{
		this.dataType = dataType;
	}
}
