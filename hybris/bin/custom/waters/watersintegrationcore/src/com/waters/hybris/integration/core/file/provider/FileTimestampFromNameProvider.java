package com.waters.hybris.integration.core.file.provider;

import com.waters.hybris.integration.core.file.parser.FileNameDateParser;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import java.io.File;
import java.util.Date;

public class FileTimestampFromNameProvider implements FileTimestampProvider
{
	private FileNameDateParser fileNameDateParser;

	@Override
	public Date getFileTimestamp(final File file)
	{
		Assert.notNull(file, "file cannot be null");
		return getFileTimestamp(file.getName());
	}

	@Override
	public Date getFileTimestamp(final String filename)
	{
		Assert.notNull(filename, "file cannot be null");
		final Date dateFromName = getFileNameDateParser().getDate(filename);
		return dateFromName != null ? dateFromName : new Date();
	}

	protected FileNameDateParser getFileNameDateParser()
	{
		return fileNameDateParser;
	}

	@Required
	public void setFileNameDateParser(final FileNameDateParser fileNameDateParser)
	{
		this.fileNameDateParser = fileNameDateParser;
	}
}
