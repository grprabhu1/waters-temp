package com.waters.hybris.integration.core.store.dao.impl;

import com.waters.hybris.integration.core.enums.ExternalDataSource;
import com.waters.hybris.integration.core.store.dao.IntegrationBaseStoreDao;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Default product import base store DAO implementation.
 */
public class DefaultIntegrationBaseStoreDao extends DefaultGenericDao<BaseStoreModel> implements IntegrationBaseStoreDao
{
	public DefaultIntegrationBaseStoreDao()
	{
		this(BaseStoreModel._TYPECODE);
	}

	public DefaultIntegrationBaseStoreDao(final String typecode)
	{
		super(typecode);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<BaseStoreModel> getBaseStoreForExternalIdAndSource(final String externalStoreId, final ExternalDataSource source)
	{
		// TODO: to update as per Waters data model and process
		final String queryString = "";

		//"SELECT {esid:" + ExternalStoreIDModel.BASESTORE + "} FROM {"
		//+ ExternalStoreIDModel._TYPECODE + " AS esid} WHERE {esid:"
		//+ ExternalStoreIDModel.ID + "}=?id AND {esid:" + ExternalStoreIDModel.SOURCE + "}=?source ";

		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter("id", externalStoreId);
		query.addQueryParameter("source", source);

		List<BaseStoreModel> baseStoreModels = getFlexibleSearchService().<BaseStoreModel>search(query).getResult();
		if (baseStoreModels == null)
		{
			baseStoreModels = Collections.emptyList();
		}
		else
		{
			baseStoreModels = baseStoreModels.stream().filter(store -> store != null).collect(Collectors.toList());
		}
		return baseStoreModels;
	}
}
