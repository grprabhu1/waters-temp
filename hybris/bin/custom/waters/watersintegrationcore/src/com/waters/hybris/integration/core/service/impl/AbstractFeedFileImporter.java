package com.waters.hybris.integration.core.service.impl;

import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.audit.service.AuditContext;
import com.waters.hybris.integration.core.file.filter.FileFilter;
import com.waters.hybris.integration.core.file.provider.FileHashProvider;
import com.waters.hybris.integration.core.file.provider.FileTimestampProvider;
import com.waters.hybris.integration.core.file.provider.GroupKeyProvider;
import com.waters.hybris.integration.core.model.record.FileImportRecordModel;
import com.waters.hybris.integration.core.service.FeedFileImporter;
import de.hybris.platform.core.model.media.MediaModel;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.io.FileUtils;
import org.fest.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.integration.file.FileLocker;
import org.springframework.util.Assert;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;

public class AbstractFeedFileImporter extends AbstractFeedImporter implements FeedFileImporter
{
	private static final Logger LOG = LoggerFactory.getLogger(AbstractFeedFileImporter.class);

	private FileTimestampProvider fileTimestampProvider;
	private FileHashProvider fileHashProvider;
	private GroupKeyProvider groupKeyProvider;
	private Collection<FileFilter> fileFilters;
	private FileLocker fileLocker;

	@Override
	@SuppressFBWarnings(value = "REC_CATCH_EXCEPTION", justification = "We want to make sure that an error is processed to the correct file and doens't prevent further processing of the file.")
	public void importFile(final File file)
	{
		Assert.notNull(file, "file must not be null");
		final AuditContext auditContext = getAuditService().beginAudit(getSystemArea(), null, "importing [{}]", file.getName());
		try
		{
			if (accept(file))
			{
				getLogger(getSystemArea()).info("File [{}] accepted by filter set [{}]", file.getName(), getFileFilters());
				auditContext.addMessage("File [{}] accepted by filter set [{}]", file.getName(), getFileFilters());
				final FileImportRecordModel record = getModelService().create(FileImportRecordModel.class);
				final MediaModel media = createMedia(file, getCatalogId(), getMediaFolderCode());
				updateFileImportRecord(
					record,
					media,
					file.getName(),
					getFileTimestampProvider().getFileTimestamp(file),
					getFileHashProvider().getFileHash(file),
					getGroupKeyProvider() != null ? getGroupKeyProvider().getGroupKey(file) : null,
					null);

				getLogger(getSystemArea()).info("File import record created for file [{}]", file.getName());
				auditContext.addMessage("File import record created for file [{}]", file.getName());
			}
			else
			{
				getLogger(getSystemArea()).info("File [{}] rejected by filter set [{}]", file.getName(), getFileFilters());
				auditContext.addMessage("File [{}] rejected by filter set [{}]", file.getName(), getFileFilters());
			}
		}
		finally
		{
			if (getFileLocker() != null)
			{
				try
				{
					getFileLocker().unlock(file);
				}
				catch (final RuntimeException ex)
				{
					getLogger(getSystemArea()).error("Unable to unlock file.", ex);
					auditContext.endAudit(Status.WARN, "Unable to unlock " + file.getName());
				}
			}
			auditContext.endAudit(Status.SUCCESS, null);
		}
	}

	protected boolean accept(final File file)
	{
		boolean accept = true;
		if (!Collections.isEmpty(getFileFilters()))
		{
			for (final FileFilter fileFilter : getFileFilters())
			{
				accept &= fileFilter.accept(file);
			}
		}
		return accept;
	}

	protected MediaModel createMedia(final File file, final String catalogId, final String mediaFolderCode)
	{
		Assert.notNull(file, "file cannot be null.");
		Assert.hasLength(catalogId, "catalogId stream cannot be empty");
		Assert.hasLength(mediaFolderCode, "mediaFolderCode stream cannot be empty");

		final AuditContext auditContext = getAuditService().beginAudit(getSystemArea(), null, "Importing feed file [{}]", file.getName());

		try (final FileInputStream inputStream = FileUtils.openInputStream(file))
		{
			final MediaModel media = createMediaFromStream(inputStream, catalogId, mediaFolderCode, file.getName(), file.getName());
			auditContext.addMessage("Created media [{}] for file", media.getCode()).endAudit(Status.SUCCESS, null);
			return media;
		}
		catch (final IOException e)
		{
			getLogger(getSystemArea()).error("Cannot read file [{}]", file.getName(), e);
			auditContext.addMessage("Error when reading a file");
			auditContext.endAudit(Status.FAILURE, null, e);
			return null;
		}
	}

	protected FileTimestampProvider getFileTimestampProvider()
	{
		return fileTimestampProvider;
	}

	@Required
	public void setFileTimestampProvider(final FileTimestampProvider fileTimestampProvider)
	{
		this.fileTimestampProvider = fileTimestampProvider;
	}

	protected FileHashProvider getFileHashProvider()
	{
		return fileHashProvider;
	}

	@Required
	public void setFileHashProvider(final FileHashProvider fileHashProvider)
	{
		this.fileHashProvider = fileHashProvider;
	}

	protected Collection<FileFilter> getFileFilters()
	{
		return fileFilters;
	}

	public void setFileFilters(final Collection<FileFilter> fileFilters)
	{
		this.fileFilters = fileFilters;
	}

	protected FileLocker getFileLocker()
	{
		return fileLocker;
	}

	public void setFileLocker(final FileLocker fileLocker)
	{
		this.fileLocker = fileLocker;
	}

	protected GroupKeyProvider getGroupKeyProvider()
	{
		return groupKeyProvider;
	}

	public void setGroupKeyProvider(final GroupKeyProvider groupKeyProvider)
	{
		this.groupKeyProvider = groupKeyProvider;
	}


	public Logger getLogger(final SystemArea systemArea)
	{
		if (systemArea != null)
		{
			return LoggerFactory.getLogger(systemArea.name());
		}
		return LOG;

	}
}
