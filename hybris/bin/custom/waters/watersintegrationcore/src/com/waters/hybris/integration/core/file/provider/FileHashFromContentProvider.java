package com.waters.hybris.integration.core.file.provider;

import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import com.google.common.io.Files;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author Tom Greasley (tom@neoworks.com)
 */
public class FileHashFromContentProvider implements FileHashProvider
{
	@Override
	public String getFileHash(final File file)
	{
		Assert.notNull(file, "file cannot be null");
		try
		{
			return BaseEncoding.base64().encode(Files.hash(file, Hashing.md5()).asBytes());
		}
		catch (final IOException e)
		{
			throw new RuntimeException("Unable to hash file contents.", e);
		}
	}

	@Override
	public String getFileHash(final String name)
	{
		return BaseEncoding.base64().encode(name.getBytes(StandardCharsets.UTF_8));
	}
}
