package com.waters.hybris.integration.core.job;

import de.hybris.platform.catalog.job.CompositeJobPerformable;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.jalo.Job;
import de.hybris.platform.cronjob.jalo.TriggerableJob;
import de.hybris.platform.cronjob.model.CompositeCronJobModel;
import de.hybris.platform.cronjob.model.CompositeEntryModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.CronJobFactory;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.internal.model.ServicelayerJobModel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


public class FailFastCompositeJobPerformable extends CompositeJobPerformable
{

	private static final Logger LOG = Logger.getLogger(FailFastCompositeJobPerformable.class);

	private static final int DEFAULT_WAIT = 1000; // 1 sec.

	private int wait = DEFAULT_WAIT;

	private CronJobService cronJobService;

	//logic from CompositeJobPerformable

	@Override
	public PerformResult perform(final CompositeCronJobModel cronJob)
	{

		for (final CompositeEntryModel entry : cronJob.getCompositeEntries())
		{
			try
			{
				final CronJobModel executedCronJob = executeCompositeEntry(entry);

				while (getCronJobService().isRunning(getRefreshedCronJobModel(executedCronJob)))
				{
					// Wait a little bit and try again
					try
					{
						Thread.sleep(getWait());
					}
					catch (final InterruptedException e)
					{
						Thread.currentThread().interrupt();
					}
				}

				if(!CronJobResult.SUCCESS.equals(executedCronJob.getResult())){
					LOG.error("Terminating composite cronjob as the entry "+entry.getCode()+" has NOT been completed successfully");
					return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
				}
			}
			catch (final SystemException e)
			{
				LOG.error("Error while performing the composite job entry " + entry + " :" + e.getMessage()
					+ ", for details set debug log level ");
				if (LOG.isDebugEnabled())
				{
					LOG.debug(e.getMessage(), e);
				}
				return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
			}
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	protected CronJobModel getRefreshedCronJobModel(final CronJobModel cronJobModel)
	{
		modelService.refresh(cronJobModel);
		return cronJobModel;
	}


	protected CronJobModel executeCompositeEntry(final CompositeEntryModel compositeEntryModel)
	{
		CronJobModel executableCronJob = compositeEntryModel.getExecutableCronJob();

		if (executableCronJob != null)
		{
			//got the cronjob -> execute it
			getCronJobService().performCronJob(executableCronJob, true);
		}
		else if (compositeEntryModel.getTriggerableJob() != null) //do we have a triggerable
		{
			final Job job = modelService.getSource(compositeEntryModel.getTriggerableJob());

			if (job instanceof TriggerableJob) //old way
			{
				executableCronJob = modelService.get(((TriggerableJob) job).newExecution());
			}
			else if (compositeEntryModel.getTriggerableJob() instanceof ServicelayerJobModel)
			{
				final ServicelayerJobModel serviceLayerTriggerable = (ServicelayerJobModel) compositeEntryModel.getTriggerableJob();
				final CronJobFactory cronJobFactory = getCronJobService().getCronJobFactory(serviceLayerTriggerable);
				executableCronJob = cronJobFactory.createCronJob(serviceLayerTriggerable);
			}
			else
			{
				throw new UnsupportedOperationException("Neither a CronJobModel or TriggerableJob instance was assigned!");
			}
			getCronJobService().performCronJob(executableCronJob, true);
		}
		return executableCronJob;
	}

	public void setWait(final int wait)
	{
		this.wait = wait;
	}

	@Required
	public void setCronJobService(final CronJobService cronJobService)
	{
		this.cronJobService = cronJobService;
	}


	protected int getWait()
	{
		return wait;
	}

	protected CronJobService getCronJobService()
	{
		return cronJobService;
	}
}
