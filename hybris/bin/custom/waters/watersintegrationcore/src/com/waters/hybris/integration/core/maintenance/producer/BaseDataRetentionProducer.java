package com.waters.hybris.integration.core.maintenance.producer;

import com.waters.hybris.integration.core.etl.producer.BatchedFlexibleSearchDataProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class BaseDataRetentionProducer extends BatchedFlexibleSearchDataProducer
{
	private static final String DB_DATE_PATTERN = "YYYY-MM-DD";
	private static final int DEFAULT_THRESHOLD_DAYS = 30;

	private String typecode;
	private String thresholdKey;

	private static final Logger LOG = LoggerFactory.getLogger(BaseDataRetentionProducer.class);

	@Override
	public String getFindItemsQuery()
	{
		final String parameterizedQuery = super.getFindItemsQuery();
		final String query = String.format(parameterizedQuery, getTypecode(), calculateThresholdDate(), DB_DATE_PATTERN);
		LOG.debug("formatting String {} into query [{}]", parameterizedQuery, query);
		return query;
	}

	@Override
	protected String getLoadItemsQuery()
	{
		final String parameterizedQuery = super.getLoadItemsQuery();
		final String query = String.format(parameterizedQuery, getTypecode());
		LOG.debug("formatting String {} into query [{}]", parameterizedQuery, query);
		return query;
	}


	/**
	 * As we want to load updates to the threshold on demand we pass the key into the config service to get the latest value.
	 * Loading the value into the bean will require a server restart to pick up the latest value
	 * @return
	 */
	protected String calculateThresholdDate()
	{
		final int thresholdInDays = getConfigurationService().getConfiguration().getInt(getThresholdKey(), DEFAULT_THRESHOLD_DAYS);

		final LocalDate now = LocalDate.now();
		final LocalDate thresholdDate = now.minusDays(thresholdInDays);
		return thresholdDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
	}

	protected String getTypecode()
	{
		return typecode;
	}

	@Required
	public void setTypecode(final String typecode)
	{
		this.typecode = typecode;
	}

	protected String getThresholdKey()
	{
		return thresholdKey;
	}

	public void setThresholdKey(final String thresholdKey)
	{
		this.thresholdKey = thresholdKey;
	}

}

