package com.waters.hybris.integration.core.etl.strategy;

import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;

/**
 * Strategy for tracking Consumed items.
 */
public interface ConsumerTrackingStrategy
{
	void markAsConsumed(EtlContext context) throws EtlTransformationException;

	void markAsComplete(EtlContext context) throws EtlTransformationException;

	void markAsError(EtlContext context, String errorMessage) throws EtlTransformationException;

	void markAllConsumedAsComplete(EtlContext context) throws EtlTransformationException;

	void markAllConsumedAsError(EtlContext context, String errorMessage) throws EtlTransformationException;
}
