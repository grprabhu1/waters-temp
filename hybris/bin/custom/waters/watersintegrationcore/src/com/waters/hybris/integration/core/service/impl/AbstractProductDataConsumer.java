package com.waters.hybris.integration.core.service.impl;

import com.neoworks.hybris.util.HybrisCollections;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.integration.core.constants.WatersintegrationcoreConstants;
import com.waters.hybris.integration.core.context.ImportContext;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.variants.model.VariantTypeModel;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Optional;

public abstract class AbstractProductDataConsumer<T, C extends ImportContext> extends AbstractModelServiceConfigurableConsumer<T, C>
{
	private ProductService productService;
	private CatalogVersionService catalogVersionService;
	private ClassificationService classificationService;
	private TypeService typeService;
	private ClassificationSystemService classificationSystemService;
	private Boolean featureListChanged = false;

	protected ProductModel getExistingProductForCode(final String code)
	{
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(WatersintegrationcoreConstants.CATALOG_NAME, WatersintegrationcoreConstants.CATALOG_VERSION_NAME);

		return getExistingProductForCodeInCatelogVersion(code, catalogVersion);
	}

	protected ProductModel getExistingProductForCodeInCatelogVersion(final String code, final CatalogVersionModel catalogVersion)
	{
		try
		{
			return getProductService().getProductForCode(catalogVersion, code);
		}
		catch (final UnknownIdentifierException ex)
		{
			// do nothing
		}

		return null;
	}

	protected <T extends ProductModel> T createNewProduct(final Class<? extends ProductModel> productModelClass,
	                                                      final String productCode,
	                                                      final Optional<Class> variantTypeClass,
	                                                      final C context)
	{
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(WatersintegrationcoreConstants.CATALOG_NAME, WatersintegrationcoreConstants.CATALOG_VERSION_NAME);

		final ModelService modelService = getModelService(context);
		final T product = modelService.create(productModelClass);
		product.setCode(productCode);
		product.setCatalogVersion(catalogVersion);
		variantTypeClass.ifPresent(clazz -> product.setVariantType((VariantTypeModel) getTypeService().getComposedTypeForClass(clazz)));

		product.setApprovalStatus(ArticleApprovalStatus.CHECK);
		getModelService(context).save(product);

		return product;
	}

	protected void ensureProductClassifiedAs(final WatersProductModel watersProduct, final String classificationClassCode, final C context)
	{
		final ClassificationClassModel classificationClass = getClassificationClass(classificationClassCode);
		if (classificationClass != null)
		{
			if (!watersProduct.getSupercategories().contains(classificationClass))
			{
				watersProduct.setSupercategories(HybrisCollections.addUnique(watersProduct.getSupercategories(), classificationClass));
				getModelService(context).save(watersProduct);
			}
		}
	}

	protected ClassificationClassModel getClassificationClass(final String classificationClassCode)
	{
		final ClassificationSystemVersionModel systemVersion = getClassificationSystemService().getSystemVersion(WatersintegrationcoreConstants.CLASSIFICATION_SYSTEM, WatersintegrationcoreConstants.CLASSIFICATION_SYSTEM_VERSION);
		return getClassificationSystemService().getClassForCode(systemVersion, classificationClassCode);
	}

	protected String generateFeatureCode(final String classificationClass, final String feature)
	{
		return WatersintegrationcoreConstants.CLASSIFICATION_SYSTEM + '/' + WatersintegrationcoreConstants.CLASSIFICATION_SYSTEM_VERSION + '/' + classificationClass + '.' + feature;
	}

	protected void populateFeatureValue(final FeatureList featureList, final String featureCode, final String featureValue)
	{
		final Feature feature = featureList.getFeatureByCode(featureCode);
		if (feature == null)
		{
			return;
		}
		final FeatureValue oldValue = feature.getValue();
		if (oldValue == null && featureValue != null)
		{
			feature.addValue(new FeatureValue(featureValue));
			setFeatureListChanged(true);
		}
		else if (oldValue != null && featureValue == null)
		{
			feature.removeValue(oldValue);
			setFeatureListChanged(true);
		}
		else if (oldValue != null && featureValue != null && !StringUtils.equals(featureValue, (String) oldValue.getValue()))
		{
			oldValue.setValue(featureValue);
			setFeatureListChanged(true);
		}
	}

	protected boolean populateFeatureValue(final FeatureList featureList, final String featureCode, final boolean featureValue)
	{
		setFeatureListChanged(false);
		boolean success = false;
		final Feature feature = featureList.getFeatureByCode(featureCode);
		if (feature != null)
		{
			final FeatureValue oldValue = feature.getValue();
			if (oldValue != null)
			{
				if (oldValue.getValue() == null || featureValue != (boolean) oldValue.getValue())
				{
					oldValue.setValue(featureValue);
					setFeatureListChanged(true);
				}
			}
			else
			{
				feature.addValue(new FeatureValue(featureValue));
				setFeatureListChanged(true);
			}
			success = true;
		}

		return success;
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected ClassificationService getClassificationService()
	{
		return classificationService;
	}

	@Required
	public void setClassificationService(final ClassificationService classificationService)
	{
		this.classificationService = classificationService;
	}

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	protected ClassificationSystemService getClassificationSystemService()
	{
		return classificationSystemService;
	}

	@Required
	public void setClassificationSystemService(final ClassificationSystemService classificationSystemService)
	{
		this.classificationSystemService = classificationSystemService;
	}

	public Boolean isFeatureListChanged()
	{
		return featureListChanged;
	}

	public void setFeatureListChanged(final Boolean featureListChanged)
	{
		this.featureListChanged = featureListChanged;
	}
}
