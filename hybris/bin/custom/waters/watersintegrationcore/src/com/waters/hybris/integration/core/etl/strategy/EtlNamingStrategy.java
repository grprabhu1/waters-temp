package com.waters.hybris.integration.core.etl.strategy;

import com.neoworks.hybris.etl.context.EtlContext;

public interface EtlNamingStrategy
{
	String generateName(EtlContext context);
}
