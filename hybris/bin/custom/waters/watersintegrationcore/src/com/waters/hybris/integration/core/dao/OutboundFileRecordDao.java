package com.waters.hybris.integration.core.dao;

import com.waters.hybris.integration.core.enums.FileType;
import com.waters.hybris.integration.core.model.record.OutboundFileRecordModel;

import java.util.List;

/**
 * @author ramana
 */
public interface OutboundFileRecordDao
{
	List<OutboundFileRecordModel> findAllCreated(FileType fileType, int batchSize);
}
