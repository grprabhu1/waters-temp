package com.waters.hybris.integration.core.service.impl;

import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.audit.service.AuditService;
import com.waters.hybris.integration.core.enums.FileProcessingStatus;
import com.waters.hybris.integration.core.enums.FileType;
import com.waters.hybris.integration.core.model.record.FileImportRecordModel;
import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;

import java.io.InputStream;
import java.util.Date;
import java.util.UUID;

public class AbstractFeedImporter
{
	private AuditService auditService;
	private ModelService modelService;
	private MediaService mediaService;
	private CatalogService catalogService;
	private String catalogId;
	private String mediaFolderCode;
	private String mimeType;
	private FileType fileType;
	private SystemArea systemArea;

	protected CatalogVersionModel getCatalogVersion(final String catalogId)
	{
		final CatalogModel catalog = getCatalogService().getCatalogForId(catalogId);
		return catalog.getActiveCatalogVersion();
	}

	protected void updateFileImportRecord(final FileImportRecordModel record,
	                                      final MediaModel media,
	                                      final String fileName,
	                                      final Date fileDate,
	                                      final String fileHash,
	                                      final String groupKey,
	                                      final Integer hybrisNode)
	{
		record.setMedia(media);
		record.setStatus(FileProcessingStatus.PENDING);
		record.setFileType(getFileType());
		record.setFileName(fileName);
		record.setFileDate(fileDate);
		record.setFileHash(fileHash);
		record.setGroupKey(groupKey);
		record.setHybrisNode(hybrisNode);
		getModelService().save(record);
	}

	protected MediaModel createMediaFromStream(final InputStream inputStream, final String catalogId, final String mediaFolderCode, final String mediaCodePrefix, final String mediaRealFileName)
	{
		final MediaModel media = getModelService().create(MediaModel.class);
		final String identifier = mediaCodePrefix + "_" + UUID.randomUUID();
		media.setCode(identifier);
		media.setCatalogVersion(getCatalogVersion(catalogId));
		media.setRealFileName(mediaRealFileName);
		getModelService().save(media);

		final MediaFolderModel mediaFolder = getMediaService().getFolder(mediaFolderCode);
		getMediaService().setStreamForMedia(media, inputStream, identifier, getMimeType(), mediaFolder);

		return media;
	}

	protected AuditService getAuditService()
	{
		return auditService;
	}

	@Required
	public void setAuditService(final AuditService auditService)
	{
		this.auditService = auditService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected MediaService getMediaService()
	{
		return mediaService;
	}

	@Required
	public void setMediaService(final MediaService mediaService)
	{
		this.mediaService = mediaService;
	}

	protected CatalogService getCatalogService()
	{
		return catalogService;
	}

	@Required
	public void setCatalogService(final CatalogService catalogService)
	{
		this.catalogService = catalogService;
	}

	protected String getCatalogId()
	{
		return catalogId;
	}

	@Required
	public void setCatalogId(final String catalogId)
	{
		this.catalogId = catalogId;
	}

	protected String getMediaFolderCode()
	{
		return mediaFolderCode;
	}

	@Required
	public void setMediaFolderCode(final String mediaFolderCode)
	{
		this.mediaFolderCode = mediaFolderCode;
	}

	protected String getMimeType()
	{
		return mimeType;
	}

	@Required
	public void setMimeType(final String mimeType)
	{
		this.mimeType = mimeType;
	}

	protected FileType getFileType()
	{
		return fileType;
	}

	@Required
	public void setFileType(final FileType fileType)
	{
		this.fileType = fileType;
	}

	protected SystemArea getSystemArea()
	{
		return systemArea;
	}

	@Required
	public void setSystemArea(final SystemArea systemArea)
	{
		this.systemArea = systemArea;
	}
}
