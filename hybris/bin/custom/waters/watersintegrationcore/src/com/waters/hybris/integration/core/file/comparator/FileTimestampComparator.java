package com.waters.hybris.integration.core.file.comparator;

import com.waters.hybris.integration.core.file.provider.FileTimestampProvider;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.springframework.beans.factory.annotation.Required;

import java.io.File;
import java.util.Comparator;
import java.util.Date;

@SuppressFBWarnings(value = "SE_COMPARATOR_SHOULD_BE_SERIALIZABLE", justification = "This comparator doesn't need to be serializable")
public class FileTimestampComparator implements Comparator<File>
{
	private FileTimestampProvider fileTimestampProvider;

	@Override
	public int compare(final File file1, final File file2)
	{
		if (file1 == null || file2 == null)
		{
			return 0;
		}

		final Date dateFile1 = getFileTimestampProvider().getFileTimestamp(file1);
		final Date dateFile2 = getFileTimestampProvider().getFileTimestamp(file2);

		if (dateFile1 == null || dateFile2 == null)
		{
			// this will never happen because the providers always default to the file last modified time
			return 0;
		}

		return dateFile1.compareTo(dateFile2);
	}

	protected FileTimestampProvider getFileTimestampProvider()
	{
		return fileTimestampProvider;
	}

	@Required
	public void setFileTimestampProvider(final FileTimestampProvider fileTimestampProvider)
	{
		this.fileTimestampProvider = fileTimestampProvider;
	}
}
