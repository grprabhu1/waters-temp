package com.waters.hybris.integration.core.dao;

import de.hybris.platform.core.Registry;
import org.junit.Assert;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;


public class IntegrationsDatabaseTest {

    // TODO disabling this test, pending to clarify what we do with Automated SIT tests
    public void testDBConnection() {
        final JdbcTemplate jdbcTemplate = (JdbcTemplate) Registry.getApplicationContext()
                .getBean("watersIntegrationJdbcTemplate");
        List<Map<String, Object>> queryForList = jdbcTemplate.queryForList("select systimestamp from dual");
        Assert.assertNotNull(((Map) queryForList.get(0)).get("systimestamp"));
    }
}