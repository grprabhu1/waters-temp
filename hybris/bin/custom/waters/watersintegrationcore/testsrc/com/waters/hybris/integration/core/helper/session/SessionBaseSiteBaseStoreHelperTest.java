package com.waters.hybris.integration.core.helper.session;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.storesession.StoreSessionService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.europe1.constants.Europe1Constants;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SessionBaseSiteBaseStoreHelperTest
{
	public static final String GBP = "GBP";
	public static final String EN = "en";
	@InjectMocks
	private SessionBaseSiteBaseStoreHelper helper;

	@Mock
	private BaseSiteService baseSiteService;

	@Mock
	private BaseStoreService baseStoreService;

	@Mock
	private SessionService sessionService;

	@Mock
	private StoreSessionService storeSessionService;

	@Mock
	private BaseSiteModel baseSite;

	@Mock
	private BaseStoreModel baseStore;

	@Mock
	private UserPriceGroup userPriceGroup;

	@Mock
	private CurrencyModel currency;

	@Mock
	private LanguageModel language;

	@Before
	public void setUp()
	{
		//given(baseStore.getUserPriceGroup()).willReturn(userPriceGroup);
		given(baseStore.getDefaultCurrency()).willReturn(currency);
		given(baseStore.getDefaultLanguage()).willReturn(language);

		given(currency.getIsocode()).willReturn(GBP);
		given(language.getIsocode()).willReturn(EN);
	}

	@Test
	public void setBaseSiteShouldSetCurrentBaseSite()
	{
		helper.setBaseSite(baseSite);

		verify(baseSiteService).setCurrentBaseSite(baseSite, false);
	}

	@Test
	public void setUserPriceGroupShouldSetSessionUpgToThatOnGivenStore()
	{
		helper.setUserPriceGroup(baseStore);

		//verify(sessionService).setAttribute(Europe1Constants.PARAMS.UPG, userPriceGroup);
	}

	@Test
	public void setSessionAttributesForBaseSiteShouldSetBaseSiteUpgCurrencyAndLanguage()
	{
		given(baseStoreService.getCurrentBaseStore()).willReturn(baseStore);

		helper.setSessionAttributesForBaseSite(baseSite);

		InOrder orderOf = inOrder(baseSiteService, baseStoreService, sessionService, storeSessionService);

		orderOf.verify(baseSiteService).setCurrentBaseSite(baseSite, false);
		orderOf.verify(baseStoreService).getCurrentBaseStore();
		//orderOf.verify(sessionService).setAttribute(Europe1Constants.PARAMS.UPG, userPriceGroup);
		orderOf.verify(storeSessionService).setCurrentCurrency(GBP);
		orderOf.verify(storeSessionService).setCurrentLanguage(EN);
	}

}