package com.waters.hybris.stepdefs.integration.helper.impl;

import com.waters.hybris.core.enums.SalesStatus;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.integration.core.enums.FileType;
import com.waters.hybris.integration.core.model.record.OutboundFileRecordModel;
import com.waters.hybris.stepdefs.integration.helper.IntegrationHelper;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.waters.hybris.integration.core.enums.FileProcessingStatus.PENDING;

public class DefaultIntegrationHelper implements IntegrationHelper
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultIntegrationHelper.class);

	public static final String QUERY_PUBLISHED_ACTIVE_PRODUCTS = "SELECT {p.pk} FROM {WatersProduct as p}, {CatalogVersion as cv}" +
		" WHERE {p.catalogVersion} = {cv.pk}" +
		" AND {cv.active} = ?active" +
		" AND {p.approvalStatus} = ?approvalStatus" +
		" AND {p.salesStatus} = ?salesStatus";

	public static final String QUERY_PENDING_OUTBOUND_RECORDS = "SELECT {o.pk} FROM {OutboundFileRecord as o}" +
		" WHERE {o.status} = ?status" +
		" AND {o.fileName} = ?fileName" +
		" AND {o.fileType} = ?fileType" +
		" ORDER BY {o.creationTime}";

	private FlexibleSearchService flexibleSearchService;
	private ConfigurationService configurationService;

	@Override
	public List<WatersProductModel> getPublishedActiveProducts()
	{
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(QUERY_PUBLISHED_ACTIVE_PRODUCTS);
		flexibleSearchQuery.addQueryParameter(CatalogVersionModel.ACTIVE, Boolean.TRUE);
		flexibleSearchQuery.addQueryParameter(WatersProductModel.APPROVALSTATUS, ArticleApprovalStatus.APPROVED);
		flexibleSearchQuery.addQueryParameter(WatersProductModel.SALESSTATUS, SalesStatus.ACTIVE);

		final SearchResult<WatersProductModel> publishedActiveProducts = getFlexibleSearchService().search(flexibleSearchQuery);
		return publishedActiveProducts.getResult();
	}

	@Override
	public List<OutboundFileRecordModel> getPendingOutboundRecords(final String fileName, final FileType fileType)
	{
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(QUERY_PENDING_OUTBOUND_RECORDS);
		flexibleSearchQuery.addQueryParameter(OutboundFileRecordModel.FILETYPE, fileType);
		flexibleSearchQuery.addQueryParameter(OutboundFileRecordModel.FILENAME, fileName);
		flexibleSearchQuery.addQueryParameter(OutboundFileRecordModel.STATUS, PENDING);

		final SearchResult<OutboundFileRecordModel> publishedActiveProducts = getFlexibleSearchService().search(flexibleSearchQuery);
		return publishedActiveProducts.getResult();
	}

	@Override
	public List<File> getFeedFiles(final String fileName, final String pathConfigurationKey)
	{
		final String path = getConfigurationService().getConfiguration().getString(pathConfigurationKey);
		if (StringUtils.isNotBlank(path))
		{
			try
			{
				return Files.list(Paths.get(path))
					.map(Path::toFile)
					.filter(f -> f.isFile() && f.getName().equalsIgnoreCase(fileName))
					.sorted(Comparator.comparing(File::lastModified))
					.collect(Collectors.toList());
			}
			catch (final IOException e)
			{
				LOG.error("Failed to get generated feed files", e);
			}
		}

		return null;
	}

	protected FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
