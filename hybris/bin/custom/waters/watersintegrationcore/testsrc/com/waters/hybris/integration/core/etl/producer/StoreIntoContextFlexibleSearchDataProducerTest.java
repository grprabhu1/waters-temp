package com.waters.hybris.integration.core.etl.producer;

import com.google.common.collect.Lists;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlTransformationException;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import com.waters.hybris.integration.core.data.PipelineResultData;
import com.waters.hybris.integration.core.etl.data.PipelineResult;
import com.waters.hybris.integration.core.etl.strategy.EtlItemFilterStrategy;
import com.waters.hybris.integration.core.etl.util.PipelineResultDataUtils;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.isA;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StoreIntoContextFlexibleSearchDataProducerTest
{
	private static final String RESULT_KEY = "resultKey";
	private static final String CURRENT_ITEM_KEY = "currentItemKey";
	private static final String FLEXIBLE_SEARCH_QUERY = "flexibleSearchQuery";
	private final PK item1Pk = PK.fromLong(1);
	private final PK item2Pk = PK.fromLong(2);
	@InjectMocks
	private StoreIntoContextFlexibleSearchDataProducer producer = new StoreIntoContextFlexibleSearchDataProducer();
	@Mock
	private FlexibleSearchService flexibleSearchService;
	@Mock
	private ModelService modelService;
	@Mock
	private SearchResult<PK> searchResult;
	@Mock
	private EtlItemFilterStrategy<ItemModel> etlItemFilterStrategy;
	@Mock
	private ItemModel item1;
	@Mock
	private ItemModel item2;
	@Spy
	private EtlContext context = new EtlContext();
	private List<PK> results = Lists.newArrayList();

	@Before
	public void setUp() throws EtlValidationException, EtlTransformationException
	{
		results.add(item1Pk);
		results.add(item2Pk);

		final Map<PK, PipelineResultData> pipelineResultMap = new HashMap();
		final Map<String, Object> parameters = new HashMap<>();
		parameters.put(RESULT_KEY, pipelineResultMap);
		context.setParameters(parameters);

		given(flexibleSearchService.<PK>search(isA(FlexibleSearchQuery.class))).willReturn(searchResult);
		given(searchResult.getResult()).willReturn(results);
		given(modelService.get(item1Pk)).willReturn(item1);
		given(modelService.get(item2Pk)).willReturn(item2);
		given(item1.getPk()).willReturn(item1Pk);
		given(item2.getPk()).willReturn(item2Pk);

		producer.setPipelineResultMapParamKey(RESULT_KEY);
		producer.setCurrentItemKeyParamKey(CURRENT_ITEM_KEY);
		producer.setQuery(FLEXIBLE_SEARCH_QUERY);

		producer.init(context);
		producer.start();
	}

	@Test
	public void getShouldReturnItemWhenIsApplicable()
	{
		given(etlItemFilterStrategy.matches(context, item1)).willReturn(true);

		assertThat(producer.get()).isEqualTo(item1);
		assertThat(getPipelineResultMap().get(item1Pk)).isEqualToComparingFieldByField(PipelineResultDataUtils.createPipelineResultData(PipelineResult.PRODUCED));
		assertThat(context.getParameters().get(CURRENT_ITEM_KEY)).isEqualTo(item1Pk);
	}

	@Test
	public void getShouldReturnReturnFollowingItemWhenNextIsNotApplicable()
	{
		given(etlItemFilterStrategy.matches(context, item1)).willReturn(false);
		given(etlItemFilterStrategy.matches(context, item2)).willReturn(true);

		assertThat(producer.get()).isEqualTo(item2);
		assertThat(getPipelineResultMap().get(item2Pk)).isEqualToComparingFieldByField(PipelineResultDataUtils.createPipelineResultData(PipelineResult.PRODUCED));
		assertThat(context.getParameters().get(CURRENT_ITEM_KEY)).isEqualTo(item2Pk);
	}

	@Test
	public void getShouldReturnReturnNullWhenNextIsNotApplicableAndNoFurtherItemAvailable()
	{
		given(etlItemFilterStrategy.matches(context, item1)).willReturn(false);
		given(etlItemFilterStrategy.matches(context, item2)).willReturn(false);

		assertThat(producer.get()).isNull();
		assertThat(getPipelineResultMap()).isEmpty();
		assertThat(context.getParameters()).doesNotContainKey(CURRENT_ITEM_KEY);
	}

	private Map<PK, PipelineResultData> getPipelineResultMap()
	{
		return (Map<PK, PipelineResultData>) context.getParameters().get(RESULT_KEY);
	}

}