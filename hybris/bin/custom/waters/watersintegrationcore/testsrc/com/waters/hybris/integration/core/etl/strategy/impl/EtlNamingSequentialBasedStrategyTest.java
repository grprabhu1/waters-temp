package com.waters.hybris.integration.core.etl.strategy.impl;


import com.google.common.collect.Maps;
import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.model.EtlCronJobParamModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EtlNamingSequentialBasedStrategyTest
{
	private final static String ETL_CRON_JOB_PARAM_NAME = "etlCronJobParamName";
	@InjectMocks
	private EtlNamingSequentialBasedStrategy etlNamingSequentialBasedStrategy = new EtlNamingSequentialBasedStrategy();
	private EtlNamingTimeBasedStrategy etlNamingTimeBasedStrategy = new EtlNamingTimeBasedStrategy();
	@Mock
	private ModelService modelService;
	@Mock
	private EtlCronJobParamModel etlCronJobParamModel;
	private EtlContext etlContext = new EtlContext();

	@Before
	public void setUp()
	{
		etlContext.setParameters(Maps.newHashMap());
		etlNamingSequentialBasedStrategy.setEtlCronJobSequenceParamKey(ETL_CRON_JOB_PARAM_NAME);
		etlContext.getParameters().put(ETL_CRON_JOB_PARAM_NAME, etlCronJobParamModel);
	}

	@Test
	public void testStrategyGeneratesNextNumber()
	{
		//GIVEN
		etlNamingSequentialBasedStrategy.setFileNamePattern("test_%04d_test.xml");
		when(etlCronJobParamModel.getValue()).thenReturn(11);

		//WHEN
		String result = etlNamingSequentialBasedStrategy.generateName(etlContext);

		//THEN
		Assert.assertEquals("test_0012_test.xml", result);
		verify(etlCronJobParamModel).setValue(12);
		verify(modelService).save(etlCronJobParamModel);
	}

	@Test
	public void testStrategyResetsNumberWhenMaximumReached()
	{
		//GIVEN
		etlNamingSequentialBasedStrategy.setFileNamePattern("test_%04d_test.xml");
		when(etlCronJobParamModel.getValue()).thenReturn(9999);

		//WHEN
		String result = etlNamingSequentialBasedStrategy.generateName(etlContext);

		//THEN
		Assert.assertEquals("test_0001_test.xml", result);
		verify(etlCronJobParamModel).setValue(1);
		verify(modelService).save(etlCronJobParamModel);
	}

	@Test
	public void testStrategyResetsNumberWhenMaximumIsAboutToBeReached()
	{
		//GIVEN
		etlNamingSequentialBasedStrategy.setFileNamePattern("test_%04d_test.xml");
		when(etlCronJobParamModel.getValue()).thenReturn(9998);

		//WHEN
		String result = etlNamingSequentialBasedStrategy.generateName(etlContext);

		//THEN
		Assert.assertEquals("test_9999_test.xml", result);
		verify(etlCronJobParamModel).setValue(9999);
		verify(modelService).save(etlCronJobParamModel);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testStrategyThrowsIllegalExceptionWhenNoParameterProvided()
	{
		//GIVEN
		etlNamingSequentialBasedStrategy.setFileNamePattern("test_%04d_test.xml");
		etlContext.getParameters().put(ETL_CRON_JOB_PARAM_NAME, null);

		//WHEN
		etlNamingSequentialBasedStrategy.generateName(etlContext);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testStrategyThrowsIllegalExceptionWhenWrongType()
	{
		//GIVEN
		etlNamingSequentialBasedStrategy.setFileNamePattern("test_%04d_test.xml");
		etlContext.getParameters().put(ETL_CRON_JOB_PARAM_NAME, new CronJobModel());

		//WHEN
		etlNamingSequentialBasedStrategy.generateName(etlContext);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testStrategyThrowsIllegalExceptionWhenFormatNotFound()
	{
		//GIVEN
		etlNamingSequentialBasedStrategy.setFileNamePattern("test_%4d_test.xml");
		when(etlCronJobParamModel.getValue()).thenReturn("1");

		//WHEN
		etlNamingSequentialBasedStrategy.generateName(etlContext);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testStrategyThrowsIllegalExceptionWhenFormatNotFound2()
	{
		//GIVEN
		etlNamingSequentialBasedStrategy.setFileNamePattern("test_01d_test.xml");
		when(etlCronJobParamModel.getValue()).thenReturn("1");

		//WHEN
		etlNamingSequentialBasedStrategy.generateName(etlContext);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testStrategyThrowsIllegalExceptionWhenValueTypeInvalid()
	{
		//GIVEN
		etlNamingSequentialBasedStrategy.setFileNamePattern("test_%04d_test.xml");
		when(etlCronJobParamModel.getValue()).thenReturn("1");

		//WHEN
		etlNamingSequentialBasedStrategy.generateName(etlContext);
	}

	@Test
	public void testStrategyGeneratesComposedFilePattern()
	{

		//GIVEN
		etlNamingTimeBasedStrategy.setFileNamePatternParamKey("anything");
		etlNamingTimeBasedStrategy.setFileNamePattern("'test_'yyyyMMdd_HH_mm_ss'_%04d.xml'");

		etlNamingTimeBasedStrategy.setTimeStampParamKey("timestamp");
		ZonedDateTime frozenDate = ZonedDateTime.now();
		etlContext.getParameters().put("timestamp", frozenDate);

		when(etlCronJobParamModel.getValue()).thenReturn(111);
		etlNamingSequentialBasedStrategy.setPreviousStrategy(etlNamingTimeBasedStrategy);

		//WHEN
		String result = etlNamingSequentialBasedStrategy.generateName(etlContext);

		//THEN
		Assert.assertEquals("test_" + DateTimeFormatter.ofPattern("yyyyMMdd_HH_mm_ss").format(frozenDate) + "_0112.xml", result);
		verify(etlCronJobParamModel).setValue(112);
		verify(modelService).save(etlCronJobParamModel);
	}
}
