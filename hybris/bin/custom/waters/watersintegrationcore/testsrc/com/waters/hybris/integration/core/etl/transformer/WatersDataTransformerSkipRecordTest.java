package com.waters.hybris.integration.core.etl.transformer;

import com.google.common.collect.Maps;
import com.neoworks.hybris.etl.context.EtlContext;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.c2l.LocalizableItem;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.AbstractItemModel;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WatersDataTransformerSkipRecordTest
{
	private static final String FAILING_RECORD_LIST_KEY = "failingRecordListKey";

	@InjectMocks
	private WatersDataTransformerSkipRecord transformer;

	@Mock
	private SessionService sessionService;

	@Mock
	private Converter<Object, Object> converter;

	private EtlContext context = new EtlContext();

	private Object source = new Object();

	@Before
	public void setUp()
	{
		context.setParameters(Maps.newHashMap());

		transformer.setFailingRecordListKey(FAILING_RECORD_LIST_KEY);
	}

	@Test
	public void initDoesNotSetLanguageFallbackOnSessionWhenIsNotLanguageAware() throws Exception
	{
		transformer.setLanguageAware(false);

		transformer.init(context);

		verify(sessionService, never()).setAttribute(LocalizableItem.LANGUAGE_FALLBACK_ENABLED, true);
		verify(sessionService, never()).setAttribute(AbstractItemModel.LANGUAGE_FALLBACK_ENABLED_SERVICE_LAYER, true);
	}

	@Test
	public void initSetsLanguageFallbackOnSessionWhenIsLanguageAware() throws Exception
	{
		transformer.setLanguageAware(true);

		transformer.init(context);

		verify(sessionService).setAttribute(LocalizableItem.LANGUAGE_FALLBACK_ENABLED, true);
		verify(sessionService).setAttribute(AbstractItemModel.LANGUAGE_FALLBACK_ENABLED_SERVICE_LAYER, true);
	}

	@Test
	public void transformReturnConvertedObjectWhenSuccessful() throws Exception
	{
		transformer.init(context);

		final Object converted = new Object();
		given(converter.convert(source)).willReturn(converted);

		assertThat(transformer.transform(source)).isEqualTo(converted);
	}

	@Test
	public void transformReturnsNullWhenFailsToTransform() throws Exception
	{
		transformer.init(context);

		final RuntimeException ex = new RuntimeException("Runtime error message");
		given(converter.convert(source)).willThrow(ex);

		assertThat(transformer.transform(source)).isNull();
	}

	@Test
	public void transformReturnsLogsErrorToMessageListWhenFailsToTransform() throws Exception
	{
		transformer.init(context);

		final RuntimeException ex = new RuntimeException("Runtime error message");
		given(converter.convert(source)).willThrow(ex);

		transformer.transform(source);

		assertThat((List) context.getParameters().get(FAILING_RECORD_LIST_KEY)).containsExactly(
				"Error occurred during conversion, the record will be skipped." + System.lineSeparator() + //
						ExceptionUtils.getRootCauseMessage(ex) + System.lineSeparator() + //
						"Source: " + source.toString());
	}


	@Test
	public void transformReturnsNullAndDoesNotTryToConvertWhenItemDoesNotMatchFilterStrategy() throws Exception
	{
		transformer.setEtlItemFilterStrategy((a, b) -> false);

		transformer.init(context);

		assertThat(transformer.transform(source)).isNull();

		verify(converter, never()).convert(source);
	}

}