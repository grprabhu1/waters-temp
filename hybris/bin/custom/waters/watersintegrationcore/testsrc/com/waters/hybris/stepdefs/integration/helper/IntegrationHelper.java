package com.waters.hybris.stepdefs.integration.helper;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.integration.core.enums.FileType;
import com.waters.hybris.integration.core.model.record.OutboundFileRecordModel;

import java.io.File;
import java.util.List;

public interface IntegrationHelper
{
	List<WatersProductModel> getPublishedActiveProducts();

	List<OutboundFileRecordModel> getPendingOutboundRecords(String fileName, FileType fileType);

	List<File> getFeedFiles(String fileName, String pathConfigurationKey);
}
