package com.waters.hybris.stepdefs;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import de.hybris.platform.servicelayer.session.SessionService;

public class SampleCucumberTest {
	private static final Logger LOG = Logger.getLogger(SampleCucumberTest.class);
	private SessionService sessionService;

	@Given("^Initial setup data$")
	public void given_test_data() {
		LOG.info("Given Test data called:" + getSessionService().getCurrentSession().getSessionId());
	}

	@When("^Perform operation$")
	public void perform_operation() {
		LOG.info("Perform Operation called:" + getSessionService().getCurrentSession().getSessionId());
	}

	@Then("^Assert output$")
	public void then_assert_output() {
		LOG.info("Then assert output called:" + getSessionService().getCurrentSession().getSessionId());
	}

	protected SessionService getSessionService() {
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService) {
		this.sessionService = sessionService;
	}
}
