package com.waters.hybris.integration.core.etl.util;

import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * UnitTest class for {@link BatchedProcessor}
 */
@UnitTest
public class BatchedProcessorTest
{

	private static final int BATCH_SIZE = 2;
	private static final String KEY_ONE = "Key1";
	private static final String KEY_TWO = "Key2";
	private static final String KEY_THREE = "Key3";
	private static final List<String> KEY_ONE_DATA = Arrays.asList("1", "2", "3", "4");
	private static final List<String> KEY_TWO_DATA = Arrays.asList("a", "b", "c", "d");
	private static final List<String> KEY_THREE_DATA = Arrays.asList("one", "two", "three");


	private BatchedProcessor<String, String> loader;
	private Map<String, List<String>> lookupData;

	@Before
	public void setUp()
	{
		lookupData = new HashMap<>();
		lookupData.put(KEY_ONE, KEY_ONE_DATA);
		lookupData.put(KEY_TWO, KEY_TWO_DATA);
		lookupData.put(KEY_THREE, KEY_THREE_DATA);

		loader = new BatchedProcessor<String, String>(BATCH_SIZE,
				(data) -> data.stream().map(key -> lookupData.get(key)).filter(Objects::nonNull).flatMap(List::stream).collect(Collectors.toList()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstruct_BatchSizeLessThenOne_ShouldThrowException()
	{
		new BatchedProcessor<String, String>(0, lookupData::get);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstruct_NullLoadingFunction_ShouldThrowException()
	{
		new BatchedProcessor<String, String>(BATCH_SIZE, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testLoadBatchedData_NullData_ShouldThrowException()
	{
		loader.processBatchedData(null, false);
	}

	@Test
	public void testLoadBatchedData_NonDistinct_EmptyData_ShouldReturnEmptyList()
	{
		final Collection<String> result = loader.processBatchedData(Collections.emptyList(), false);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testLoadBatchedData_NonDistinct_LoadAllData_ShouldReturnListOfAllData()
	{
		final Collection<String> result = loader.processBatchedData(Arrays.asList(KEY_ONE, KEY_TWO, KEY_THREE), false);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertResults(result, KEY_ONE_DATA, KEY_TWO_DATA, KEY_THREE_DATA);
	}

	@Test
	public void testLoadBatchedData_Distinct_LoadAllData_ShouldReturnListOfAllData()
	{
		final Collection<String> result = loader.processBatchedData(Arrays.asList(KEY_ONE, KEY_TWO, KEY_THREE), true);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertResults(result, KEY_ONE_DATA, KEY_TWO_DATA, KEY_THREE_DATA);
	}

	@Test
	public void testLoadBatchedData_Distinct_LoadDuplicateData_ShouldReturnDistinctListProcessedData()
	{
		final Collection<String> result = loader.processBatchedData(Arrays.asList(KEY_ONE, KEY_TWO, KEY_ONE), true);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertResults(result, KEY_ONE_DATA, KEY_TWO_DATA);
	}

	@Test
	public void testLoadBatchedData_NonDistinct_LoadDuplicateData_ShouldReturnListOfAllData()
	{
		final Collection<String> result = loader.processBatchedData(Arrays.asList(KEY_ONE, KEY_TWO, KEY_ONE), false);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertResults(result, KEY_ONE_DATA, KEY_TWO_DATA, KEY_ONE_DATA);
	}

	@SafeVarargs
	private final void assertResults(final Collection<String> results, final List<String>... expected)
	{
		assertTrue(CollectionUtils.isEqualCollection(buildExpectedResultsList(expected), results));
	}

	@SafeVarargs
	private final Collection<String> buildExpectedResultsList(final List<String>... data)
	{
		final List<String> expectedResults = new ArrayList<>();
		for (final List<String> list : data)
		{
			expectedResults.addAll(list);
		}
		return expectedResults;
	}

}