package com.waters.hybris.integration.core.splitter.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("person")
public class Person
{
	@XStreamAlias("name")
	private String name;
	@XStreamAlias("surname")
	private String surname;
	@XStreamImplicit(itemFieldName = "address")
	private List<Address> addresses;

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public String getSurname()
	{
		return surname;
	}

	public void setSurname(final String surname)
	{
		this.surname = surname;
	}

	public List<Address> getAddresses()
	{
		return addresses;
	}

	public void setAddresses(final List<Address> addresses)
	{
		this.addresses = addresses;
	}

	@Override
	public String toString()
	{
		return "Person{" +
				"name='" + name + '\'' +
				", surname='" + surname + '\'' +
				", addresses=" + addresses +
				'}';
	}
}