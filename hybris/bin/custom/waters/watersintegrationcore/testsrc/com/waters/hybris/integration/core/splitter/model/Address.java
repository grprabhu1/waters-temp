package com.waters.hybris.integration.core.splitter.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("address")
public class Address
{
	@XStreamAlias("firstLine")
	private String firstLine;
	@XStreamAlias("secondLOine")
	private String secondLine;
	@XStreamAlias("postCode")
	private String postCode;

	public String getFirstLine()
	{
		return firstLine;
	}

	public void setFirstLine(final String firstLine)
	{
		this.firstLine = firstLine;
	}

	public String getSecondLine()
	{
		return secondLine;
	}

	public void setSecondLine(final String secondLine)
	{
		this.secondLine = secondLine;
	}

	public String getPostCode()
	{
		return postCode;
	}

	public void setPostCode(final String postCode)
	{
		this.postCode = postCode;
	}

	@Override
	public String toString()
	{
		return "Address{" +
				"firstLine='" + firstLine + '\'' +
				", secondLine='" + secondLine + '\'' +
				", postCode='" + postCode + '\'' +
				'}';
	}
}