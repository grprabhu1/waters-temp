package com.waters.hybris.stepdefs.integration;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.stepdefs.integration.helper.IntegrationHelper;
import cucumber.api.java.en.Given;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public class IntegrationCoreStepDefs
{
	private IntegrationHelper integrationHelper;

	@Given("^there are published and active products in PIM$")
	public void given_there_are_published_active_products_in_pim()
	{
		final List<WatersProductModel> publishedActiveProducts = getIntegrationHelper().getPublishedActiveProducts();
		Assert.assertTrue(CollectionUtils.isNotEmpty(publishedActiveProducts));
	}

	protected IntegrationHelper getIntegrationHelper()
	{
		return integrationHelper;
	}

	@Required
	public void setIntegrationHelper(final IntegrationHelper integrationHelper)
	{
		this.integrationHelper = integrationHelper;
	}
}
