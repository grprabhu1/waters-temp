package com.waters.hybris.integration.core.splitter.impl;

import com.neoworks.hybris.audit.service.AuditContext;
import com.waters.hybris.integration.core.context.FileImportContext;
import com.waters.hybris.integration.core.splitter.model.Address;
import com.waters.hybris.integration.core.splitter.model.Person;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.FileInputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class XmlSplitterTest
{
	private final XmlSplitter<Person> splitter = new XmlSplitter<>();

	@Mock
	private FileImportContext fileImportContext;
	@Mock
	private AuditContext auditContext;

	@Before
	public void init()
	{
		when(fileImportContext.getAuditContext()).thenReturn(auditContext);

		splitter.setDataType(Person.class);
	}


	@Ignore
	@Test
	public void test_valid_xml() throws Exception
	{
		final URL resource = getClass().getClassLoader().getResource("watersintegrationcore/test/xml/test.xml");
		assertNotNull(resource);
		checkTwoPeopleAndTwoAddresses(resource);

	}

	@Ignore
	@Test(expected = NoSuchElementException.class)
	public void test_valid_xml_too_many_calls() throws Exception
	{
		final URL resource = getClass().getClassLoader().getResource("watersintegrationcore/test/xml/test.xml");
		assertNotNull(resource);
		final File file = new File(resource.toURI());
		try (final FileInputStream inputStream = FileUtils.openInputStream(file))
		{
			final Iterator<Person> it = splitter.split(inputStream, fileImportContext);
			it.next();
			it.next();
			it.next();
		}
	}

	@Ignore
	@Test
	public void test_malformed_xml_two_people_expected() throws Exception
	{
		final URL resource = getClass().getClassLoader().getResource("watersintegrationcore/test/xml/test_malformed.xml");
		assertNotNull(resource);
		final List<Person> people = checkTwoPeopleAndTwoAddresses(resource);
		final Person p = people.get(1);
		assertEquals("Mario", p.getName());
		assertEquals("Rossi", p.getSurname());
		for (int i = 0; i < p.getAddresses().size(); i++)
		{
			final Address address = p.getAddresses().get(i);
			assertEquals("Street" + i, address.getFirstLine());
			assertEquals("Street" + i, address.getSecondLine());
			assertEquals("Postcode" + i, address.getPostCode());

		}

	}

	protected List<Person> checkTwoPeopleAndTwoAddresses(final URL resource) throws java.io.IOException, URISyntaxException
	{
		final File file = new File(resource.toURI());
		try (final FileInputStream inputStream = FileUtils.openInputStream(file))
		{
			final Iterator<Person> it = splitter.split(inputStream, fileImportContext);


			final List<Person> people = new ArrayList<>();
			int n = 0;
			while (it.hasNext())
			{
				final Person person = it.next();
				assertNotNull("Person should not be null", person);
				assertNotNull("Name should not be null", person.getName());
				assertNotNull("Surname should not be null", person.getSurname());
				assertNotNull("Addresses should not be null", person.getAddresses());
				assertEquals("There should be 2 addresses", 2, person.getAddresses().size());
				for (final Address address : person.getAddresses())
				{
					assertNotNull("First line should not be null", address.getFirstLine());
					assertNotNull("Second line should not be null", address.getSecondLine());
					assertNotNull("Postcode should not be null", address.getPostCode());
				}
				people.add(person);
				n++;
			}

			assertEquals("2 person objects should be parsed", n, 2);

			return people;
		}
	}

}