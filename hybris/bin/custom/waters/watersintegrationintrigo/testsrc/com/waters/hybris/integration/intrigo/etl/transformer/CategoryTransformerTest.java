package com.waters.hybris.integration.intrigo.etl.transformer;

import com.neoworks.hybris.etl.context.EtlContext;
import com.waters.hybris.integration.intrigo.schema.CategoryData;
import com.waters.hybris.integration.intrigo.strategy.ProductFacetsInCategoryStrategy;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Locale;
import java.util.stream.Stream;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;

@RunWith(MockitoJUnitRunner.class)
public class CategoryTransformerTest
{

	private static final String CATALOG_PARAM_NAME = "ProductCatalogVersion";

    @InjectMocks
    private final CategoryTransformer transformer = new CategoryTransformer();

    @Mock
    private Converter<CategoryModel, CategoryData> converter;

    @Mock
	private EtlContext etlContext;

    @Mock
	private CatalogVersionModel catalogVersionModel;

    @Mock
	private ProductFacetsInCategoryStrategy productFacetsInCategoryStrategy;

    @Mock
    private CategoryModel root1;

    @Mock
    private CategoryModel level1;
    @Mock
    private CategoryModel level12;
    @Mock
    private CategoryModel level2;


    private final CategoryData rootData1 = new CategoryData();

    private final CategoryData levelData1 = new CategoryData();
    private final CategoryData levelData12 = new CategoryData();
    private final CategoryData levelData2 = new CategoryData();


    @Before
    public void setUp() {
        given(converter.convert(root1)).willReturn(rootData1);
        given(converter.convert(level1)).willReturn(levelData1);
        given(converter.convert(level12)).willReturn(levelData12);
        given(converter.convert(level2)).willReturn(levelData2);

		rootData1.setNavigationId( "root1");
		levelData1.setNavigationId( "level1");
		levelData12.setNavigationId( "level12");
		levelData2.setNavigationId( "level2");

		given(etlContext.getParameters()).willReturn(Collections.singletonMap(CATALOG_PARAM_NAME, catalogVersionModel));

		given(productFacetsInCategoryStrategy.calculateFacets(levelData1, catalogVersionModel)).willReturn(Stream.empty());
		given(productFacetsInCategoryStrategy.calculateFacets(levelData12, catalogVersionModel)).willReturn(Stream.empty());
		given(productFacetsInCategoryStrategy.calculateFacets(levelData2, catalogVersionModel)).willReturn(Stream.empty());

    }



	@Test
    public void test_transformer_populatesDoesNotPopulateRoot(){
        final Collection<CategoryData> result = transformer.transform(root1);

        Assert.assertEquals(Collections.emptyList(), result);

    }

    @Test
    public void test_transformer_populatesAllLevels(){
        given(root1.getCategories()).willReturn(Arrays.asList(level1, level12));
        given(level12.getCategories()).willReturn(Collections.singletonList(level2));

        final Collection<CategoryData> result = transformer.transform(root1);

        Assert.assertEquals(Arrays.asList(
                createIntrigoCategory( "level12","0"),
                createIntrigoCategory("level2", "1"),
                createIntrigoCategory("level1","0")), result);

    }


	@Test
	public void test_transformer_populatesFacets(){
		given(root1.getCategories()).willReturn(Arrays.asList(level1, level12));
		given(level12.getCategories()).willReturn(Collections.singletonList(level2));
		given(productFacetsInCategoryStrategy.calculateFacets(levelData2, catalogVersionModel)).willReturn(Stream.of(createIntrigoCategory("facet1", "10")));

		final Collection<CategoryData> result = transformer.transform(root1);

		Assert.assertEquals(Arrays.asList(
			createIntrigoCategory( "level12","0"),
			createIntrigoCategory("level2", "1"),
			createIntrigoCategory("level1","0"),
			createIntrigoCategory("facet1", "10")), result);

	}

    private CategoryData createIntrigoCategory(String code, final String level){
        final CategoryData categoryData = new CategoryData();
        categoryData.setNavigationId(code);
        categoryData.setNavLevel(level);
        return categoryData;
    }



}
