package com.waters.hybris.integration.intrigo.etl.transformer;

import com.google.common.collect.Lists;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.integration.intrigo.schema.FeatureData;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import org.assertj.core.util.Sets;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class ProductFeatureTransformerTest
{

	@InjectMocks
	ProductFeatureTransformer transformer = new ProductFeatureTransformer();

	@Mock
	private ClassificationService classificationService;

	@Mock
	private FeatureList featureList;

	@Mock
	private Feature feature;

	@Mock
	private FeatureValue featureValue;

	@Mock
	private FeatureValue featureValue2;

	@Mock
	private ClassificationAttributeValueModel classificationAttributeValueModel;

	@Mock
	private ClassificationAttributeValueModel classificationAttributeValueModel2;

	@Mock
	private ClassificationAttributeModel classificationAttributeModel;

	@Mock
	private ClassAttributeAssignmentModel classAttributeAssignmentModel;

	@Mock
	private ClassificationAttributeUnitModel classificationAttributeUnitModel;

	@Mock
	private WatersProductModel productModel;


	@Before
	public void setUp(){
		given(productModel.getCode()).willReturn("code");

		given(classificationService.getFeatures(productModel)).willReturn(featureList);
		given(featureList.getFeatures()).willReturn(Collections.singletonList(feature));
		given(feature.getClassAttributeAssignment()).willReturn(classAttributeAssignmentModel);
		given(classAttributeAssignmentModel.getClassificationAttribute()).willReturn(classificationAttributeModel);
		given(classificationAttributeModel.getName(Locale.ENGLISH)).willReturn("name");
		given(classAttributeAssignmentModel.isInternalOnly()).willReturn(false);
		given(classificationAttributeUnitModel.getSymbol()).willReturn("%");

		given(feature.getValue()).willReturn(featureValue);
		given(feature.getValues()).willReturn(Collections.singletonList(featureValue));
		given(featureValue.getValue()).willReturn("value");
	}

	@Test
	public void test_transform_PopulatesValue_WhenString(){
		final Collection<FeatureData> result = transformer.transform(productModel);

		Assert.assertEquals(createFeatureDataWithHazardous("value"), result);
	}

	@Test
	public void test_transform_PopulatesValue_WithNoDuplicates(){
		given(featureList.getFeatures()).willReturn(Lists.newArrayList(feature, feature));

		final Collection<FeatureData> result = transformer.transform(productModel);

		Assert.assertEquals(createFeatureDataWithHazardous("value"), result);
	}

	@Test
	public void test_transform_PopulatesValue_WhenClassificationAttributeValue(){

		given(featureValue.getValue()).willReturn(classificationAttributeValueModel);
		given(classificationAttributeValueModel.getName(Locale.ENGLISH)).willReturn("value1");

		final Collection<FeatureData> result = transformer.transform(productModel);

		Assert.assertEquals(createFeatureDataWithHazardous("value1"), result);
	}

	@Test
	public void test_transform_PopulatesValue_WhenClassificationAttributeValues(){

		given(feature.getValues()).willReturn(Arrays.asList(featureValue, featureValue2));
		given(featureValue.getValue()).willReturn(classificationAttributeValueModel);
		given(featureValue2.getValue()).willReturn(classificationAttributeValueModel2);
		given(classificationAttributeValueModel.getName(Locale.ENGLISH)).willReturn("value1");
		given(classificationAttributeValueModel2.getName(Locale.ENGLISH)).willReturn("value2");

		final Collection<FeatureData> result = transformer.transform(productModel);

		Assert.assertEquals(new HashSet(Arrays.asList(
			createHazardousFeatureData("N"),
			createFeatureData("value1"),
			createFeatureData("value2"))), result);
	}

	@Test
	public void test_transform_PopulatesOnlyHazardousFlag_WhenNoFeatureValue(){
		given(feature.getValue()).willReturn(null);
		given(feature.getValues()).willReturn(null);

		final Collection<FeatureData> result = transformer.transform(productModel);

		Assert.assertEquals(Collections.singleton(createHazardousFeatureData("N")), result);
	}

	@Test
	public void test_transform_PopulatesOnlyHazardousFlagAsTrue(){

		given(feature.getValues()).willReturn(null);
		given(productModel.isHazardous()).willReturn(true);

		final Collection<FeatureData> result = transformer.transform(productModel);

		Assert.assertEquals(Collections.singleton(createHazardousFeatureData("Y")), result);
	}

	@Test
	public void test_transform_PopulatesHazardous_WhenNoClassAttributeAssignment(){
		given(feature.getClassAttributeAssignment()).willReturn(null);

		final Collection<FeatureData> result = transformer.transform(productModel);

		Assert.assertEquals(Collections.singleton(createHazardousFeatureData("N")), result);
	}

	@Test
	public void test_transform_PopulatesHazardous_WhenNoClassificationAttribute(){
		given(classAttributeAssignmentModel.getClassificationAttribute()).willReturn(null);

		final Collection<FeatureData> result = transformer.transform(productModel);

		Assert.assertEquals(Collections.singleton(createHazardousFeatureData("N")), result);
	}

	@Test
	public void test_transform_DoesNOTPopulateFields_MarkedAsInternalOnly(){
		given(classAttributeAssignmentModel.isInternalOnly()).willReturn(true);

		final Collection<FeatureData> result = transformer.transform(productModel);

		Assert.assertEquals(Collections.singleton(createHazardousFeatureData("N")), result);
	}

	@Test
	public void test_transform_PopulatesValueAndUOM_WhenClassificationAttributeValueIsUOM(){

		given(featureValue.getValue()).willReturn(classificationAttributeValueModel);
		given(classificationAttributeValueModel.getName(Locale.ENGLISH)).willReturn("value1");
		given(featureValue.getUnit()).willReturn(classificationAttributeUnitModel);

		final Collection<FeatureData> result = transformer.transform(productModel);

		Assert.assertEquals(createFeatureDataWithHazardous("value1 %"), result);
	}

	@Test
	public void test_transform_PopulatesValueAndUOM_WhenFreeTextFielHasUOM(){

		given(featureValue.getUnit()).willReturn(classificationAttributeUnitModel);

		final Collection<FeatureData> result = transformer.transform(productModel);

		Assert.assertEquals(createFeatureDataWithHazardous("value %"), result);
	}


	private FeatureData createFeatureData(final String value){
		return new FeatureData("code","name", value);
	}

	private FeatureData createHazardousFeatureData(final String value)
	{
		return new FeatureData("code", "Hazardous", value);
	}

	private Set<FeatureData> createFeatureDataWithHazardous(final String value){
		return Sets.newLinkedHashSet(new FeatureData("code","name", value), new FeatureData("code", "Hazardous", "N"));
	}


}
