package com.waters.hybris.integration.intrigo.etl.populators;

import com.google.common.collect.ImmutableMap;
import com.waters.hybris.core.model.model.ExternalMediaModel;
import com.waters.hybris.core.strategies.ProductImageResolutionStrategy;
import com.waters.hybris.integration.intrigo.schema.CategoryData;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Locale;
import java.util.Optional;

import static com.waters.hybris.integration.intrigo.constants.WatersintegrationintrigoConstants.DEFAULT_INTRIGO_IMAGE_THUMBNAIL;
import static com.waters.hybris.integration.intrigo.constants.WatersintegrationintrigoConstants.INTRIGO_IMAGE_RENDITION;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class CategoryPopulatorTest
{
	private static final String CODE = "code";
	private static final String NAME = "name";
	private static final String DESCRIPTION = "description";
	private static final String DEFAULT_PARENT_ID = "0";

	@InjectMocks
	private CategoryPopulator populator = new CategoryPopulator();

	@Mock
	private ProductImageResolutionStrategy productImageResolutionStrategy;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private Configuration configuration;

	@Mock
	private CategoryModel category;

	@Mock
	private CategoryModel parent;

	@Mock
	private ExternalMediaModel externalMediaModel;

	private CategoryData categoryData = new CategoryData();

	@Before
	public void setUp() throws Exception
	{
		populator.setIntrigoParentIdMapping(ImmutableMap.of("Intrigo", "0"));
		given(category.getCode()).willReturn(CODE);
		given(category.getName(Locale.ENGLISH)).willReturn(NAME);
		given(category.getDescription(Locale.ENGLISH)).willReturn(DESCRIPTION);
		given(category.getOrder()).willReturn(10);

		given(productImageResolutionStrategy.resolveImageUrl(externalMediaModel, Locale.ENGLISH, DEFAULT_INTRIGO_IMAGE_THUMBNAIL)).willReturn("http://localhost");
		given(externalMediaModel.getAlternativeText(Locale.ENGLISH)).willReturn("alternative");

		given(configurationService.getConfiguration()).willReturn(configuration);
		given(configuration.getString(INTRIGO_IMAGE_RENDITION, DEFAULT_INTRIGO_IMAGE_THUMBNAIL)).willReturn(DEFAULT_INTRIGO_IMAGE_THUMBNAIL);

		categoryData = new CategoryData();
	}

	@Test
	public void test_Populate_PopulatesDefaultParentId(){
		populator.populate(category, categoryData);

		Assert.assertEquals(createIntrigoCategory(), categoryData);
	}

	@Test
	public void test_Populate_PopulatesParentCategoryCode_WhenAvailable(){
		given(category.getSupercategories()).willReturn(Collections.singletonList(parent));
		given(parent.getCode()).willReturn("parent1");

		populator.populate(category, categoryData);

		Assert.assertEquals(createIntrigoCategory(Optional.of("parent1")), categoryData);
	}

	@Test
	public void test_Populate_PopulatesMappedCodeForParentCategoryCode_WhenAvailable(){
		given(category.getSupercategories()).willReturn(Collections.singletonList(parent));
		given(parent.getCode()).willReturn("Intrigo");

		populator.populate(category, categoryData);

		Assert.assertEquals(createIntrigoCategory(Optional.of("0")), categoryData);
	}

	@Test
	public void test_Populate_PopulatesImageData_WhenAvailable(){
		given(category.getPrimaryImage()).willReturn(externalMediaModel);


		populator.populate(category, categoryData);

		Assert.assertEquals(createCategoryWithMedia("http://localhost", "alternative"), categoryData);
	}

	private CategoryData createIntrigoCategory(){
		return createIntrigoCategory(Optional.empty());
	}

	private CategoryData createCategoryWithMedia(String url, String text){
		final CategoryData category = createIntrigoCategory();
		category.setImageUrl(url);
		category.setImageAltText(text);
		return category;
	}

	private CategoryData createIntrigoCategory(final Optional<String> parent){
		final CategoryData category = new CategoryData();
		category.setTitle(NAME);
		category.setDescription(DESCRIPTION);
		category.setNavigationId(CODE);
		category.setNavigationType("P");
		category.setParentId(DEFAULT_PARENT_ID);
		category.setSortOrder("10");
		parent.ifPresent(category::setParentId);
		return category;
	}

}
