package com.waters.hybris.integration.intrigo.etl.transformer;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.waters.hybris.core.model.model.PunchoutCategoryModel;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.integration.intrigo.schema.PartToNavigationData;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;

import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class PartToNavigationTransformerTest
{
	private static final String PRODUCT_CODE = "code1";
	private static final String CATEGORY_CODE = "cat1";
	private static final String PARENT_CATEGORY_CODE = "p_cat1";

	@InjectMocks
	private final PartToNavigationTransformer partToNavigationTransformer = new PartToNavigationTransformer();

	@Mock
	private ClassificationService classificationService;

	@Mock
	private PunchoutCategoryModel punchoutCategoryModel;

	@Mock
	private PunchoutCategoryModel parentPunchoutCategoryModel;

	@Mock
	private PunchoutCategoryModel rootCategory;

	@Mock
	private CategoryModel categoryModel;

	@Mock
	private WatersProductModel watersProductModel;

	@Mock
	private ClassAttributeAssignmentModel classAttributeAssignment0;
	@Mock
	private ClassAttributeAssignmentModel classAttributeAssignment1;
	@Mock
	private ClassAttributeAssignmentModel classAttributeAssignment2;
	@Mock
	private ClassAttributeAssignmentModel classAttributeAssignment3;
	@Mock
	private ClassificationClassModel classificationClass1;
	@Mock
	private ClassificationClassModel classificationClass2;
	@Mock
	private ClassificationAttributeModel classificationAttribute1;
	@Mock
	private ClassificationAttributeModel classificationAttribute2;

	@Mock
	private FeatureList featureList;
	@Mock
	private Feature feature0;
	@Mock
	private Feature feature1;
	@Mock
	private Feature feature2;
	@Mock
	private Feature feature3;
	@Mock
	private FeatureValue featureValue1;
	@Mock
	private FeatureValue featureValue2;
	@Mock
	private FeatureValue featureValue3;
	@Mock
	private ClassificationAttributeValueModel classificationAttributeValue1;
	@Mock
	private ClassificationAttributeValueModel classificationAttributeValue2;
	@Mock
	private ClassificationAttributeValueModel classificationAttributeValue3;

	@Before
	public void setUp()
	{
		given(classificationService.getFeatures(watersProductModel)).willReturn(featureList);
		given(watersProductModel.getCode()).willReturn(PRODUCT_CODE);
		given(punchoutCategoryModel.getCode()).willReturn(CATEGORY_CODE);
		given(punchoutCategoryModel.getSupercategories()).willReturn(Collections.singletonList(parentPunchoutCategoryModel));
		given(parentPunchoutCategoryModel.getCode()).willReturn(PARENT_CATEGORY_CODE);
		given(parentPunchoutCategoryModel.getSupercategories()).willReturn(Collections.singletonList(rootCategory));
		given(watersProductModel.getSupercategories()).willReturn(Arrays.asList(punchoutCategoryModel, categoryModel));


		given(feature0.getClassAttributeAssignment()).willReturn(classAttributeAssignment0);
		given(feature1.getClassAttributeAssignment()).willReturn(classAttributeAssignment1);
		given(feature2.getClassAttributeAssignment()).willReturn(classAttributeAssignment2);
		given(feature3.getClassAttributeAssignment()).willReturn(classAttributeAssignment3);
		given(classAttributeAssignment0.isFacet()).willReturn(Boolean.FALSE);
		given(classAttributeAssignment1.isFacet()).willReturn(Boolean.TRUE);
		given(classAttributeAssignment2.isFacet()).willReturn(Boolean.TRUE);
		given(classAttributeAssignment3.isFacet()).willReturn(Boolean.TRUE);
		given(classAttributeAssignment1.getClassificationClass()).willReturn(classificationClass1);
		given(classAttributeAssignment2.getClassificationClass()).willReturn(classificationClass2);
		given(classAttributeAssignment3.getClassificationClass()).willReturn(classificationClass2);
		given(classAttributeAssignment1.getClassificationAttribute()).willReturn(classificationAttribute1);
		given(classAttributeAssignment2.getClassificationAttribute()).willReturn(classificationAttribute2);
		given(classAttributeAssignment3.getClassificationAttribute()).willReturn(classificationAttribute2);
		given(classificationClass1.getCode()).willReturn("cc1");
		given(classificationClass2.getCode()).willReturn("cc2");
		given(classificationAttribute1.getCode()).willReturn("ca1");
		given(classificationAttribute2.getCode()).willReturn("ca2");
	}

	@Test
	public void test_transform_ReturnsOnlyIntrigoCategories()
	{
		final PartToNavigationData partToNavigationData = new PartToNavigationData(CATEGORY_CODE, PRODUCT_CODE);

		Assert.assertEquals(Collections.singleton(partToNavigationData), partToNavigationTransformer.transform(watersProductModel));
	}

	@Test
	public void test_transform_ReturnsAllIntrigo_Categories()
	{
		final PartToNavigationData partToNavigationData = new PartToNavigationData(CATEGORY_CODE, PRODUCT_CODE);
		final PartToNavigationData partToNavigationData1 = new PartToNavigationData(PARENT_CATEGORY_CODE, PRODUCT_CODE);

		given(punchoutCategoryModel.getAllSupercategories()).willReturn(Collections.singletonList(parentPunchoutCategoryModel));

		Assert.assertEquals(Sets.newHashSet(partToNavigationData1, partToNavigationData), partToNavigationTransformer.transform(watersProductModel));
	}

	@Test
	public void test_transform_ReturnsOnlyNonRootIntrigoCategories()
	{
		final PartToNavigationData partToNavigationData = new PartToNavigationData(CATEGORY_CODE, PRODUCT_CODE);
		given(punchoutCategoryModel.getAllSupercategories()).willReturn(Collections.singletonList(rootCategory));

		Assert.assertEquals(Collections.singleton(partToNavigationData), partToNavigationTransformer.transform(watersProductModel));
	}

	@Test
	public void test_transform_ReturnIntrigoCategoriesAndFacetCategories()
	{
		final PartToNavigationData partToNavigationData = new PartToNavigationData(CATEGORY_CODE, PRODUCT_CODE);
		final PartToNavigationData data1 = new PartToNavigationData(CATEGORY_CODE + "-cc1.ca1", PRODUCT_CODE);
		final PartToNavigationData data2 = new PartToNavigationData(CATEGORY_CODE + "-cc2.ca2", PRODUCT_CODE);
		final PartToNavigationData data3 = new PartToNavigationData("cat1-cc1.ca1:1", PRODUCT_CODE);
		final PartToNavigationData data4 = new PartToNavigationData("cat1-cc2.ca2:2", PRODUCT_CODE);
		final PartToNavigationData data5 = new PartToNavigationData("cat1-cc2.ca2:3", PRODUCT_CODE);
		given(punchoutCategoryModel.getAllSupercategories()).willReturn(Collections.singletonList(rootCategory));

		given(feature1.getValues()).willReturn(Collections.singletonList(featureValue1));
		given(feature2.getValues()).willReturn(Collections.singletonList(featureValue2));
		given(feature3.getValues()).willReturn(Collections.singletonList(featureValue3));
		given(featureValue1.getValue()).willReturn(classificationAttributeValue1);
		given(featureValue2.getValue()).willReturn(classificationAttributeValue2);
		given(featureValue3.getValue()).willReturn(classificationAttributeValue3);
		given(classificationAttributeValue1.getName(Locale.ENGLISH)).willReturn("1");
		given(classificationAttributeValue2.getName(Locale.ENGLISH)).willReturn("2");
		given(classificationAttributeValue3.getName(Locale.ENGLISH)).willReturn("3");
		given(classificationService.getFeatures(watersProductModel)).willReturn(featureList);
		given(featureList.getFeatures()).willReturn(Arrays.asList(feature0, feature1, feature2, feature3));

		Assert.assertEquals(Sets.newHashSet(partToNavigationData, data1, data2, data3, data4, data5), partToNavigationTransformer.transform(watersProductModel));
	}

	@Test
	public void test_transform_ReturnsAllIntrigo_WithNoDuplicates_Categories()
	{
		final PartToNavigationData partToNavigationData = new PartToNavigationData(CATEGORY_CODE, PRODUCT_CODE);
		final PartToNavigationData partToNavigationData1 = new PartToNavigationData(PARENT_CATEGORY_CODE, PRODUCT_CODE);

		given(punchoutCategoryModel.getAllSupercategories()).willReturn(Lists.newArrayList(punchoutCategoryModel, parentPunchoutCategoryModel));

		Assert.assertEquals(Sets.newHashSet(partToNavigationData1, partToNavigationData), partToNavigationTransformer.transform(watersProductModel));
	}

	@Test
	public void test_transform_ReturnsEmpty_WhenNoIntrigoCategories()
	{
		given(watersProductModel.getSupercategories()).willReturn(Collections.singletonList(categoryModel));
		Assert.assertEquals(Collections.emptySet(), partToNavigationTransformer.transform(watersProductModel));
	}

	@Test
	public void test_transform_ReturnsEmpty_WhenNoSuperCategories()
	{
		given(watersProductModel.getSupercategories()).willReturn(null);
		Assert.assertEquals(Collections.emptySet(), partToNavigationTransformer.transform(watersProductModel));
	}

}
