package com.waters.hybris.integration.intrigo.etl.populators;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.model.model.ExternalMediaModel;
import com.waters.hybris.core.strategies.ProductImageResolutionStrategy;
import com.waters.hybris.integration.intrigo.constants.WatersintegrationintrigoConstants;
import com.waters.hybris.integration.intrigo.schema.PartData;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Locale;

import static com.waters.hybris.integration.intrigo.constants.WatersintegrationintrigoConstants.DEFAULT_IMAGE_PROPERTY;
import static com.waters.hybris.integration.intrigo.constants.WatersintegrationintrigoConstants.DEFAULT_INTRIGO_IMAGE_THUMBNAIL;
import static com.waters.hybris.integration.intrigo.constants.WatersintegrationintrigoConstants.INTRIGO_IMAGE_RENDITION;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class PartPopulatorTest
{
	@InjectMocks
	private final PartPopulator populator = new PartPopulator();

	@Mock
	private ProductImageResolutionStrategy productImageResolutionStrategy;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private Configuration configuration;

	@Mock
	private WatersProductModel productModel;

	@Mock
	private ExternalMediaModel externalMediaModel;

	@Mock
	private ProductFeatureModel productFeatureModel;

	private final PartData partData = new PartData();

	@Before
	public void setUp(){
		given(configurationService.getConfiguration()).willReturn(configuration);
		given(configuration.getString(DEFAULT_IMAGE_PROPERTY)).willReturn("default.png");
		given(configuration.getString(INTRIGO_IMAGE_RENDITION, DEFAULT_INTRIGO_IMAGE_THUMBNAIL)).willReturn(DEFAULT_INTRIGO_IMAGE_THUMBNAIL);

		given(productModel.getPrimaryImage()).willReturn(externalMediaModel);
		given(productImageResolutionStrategy.resolveImageUrl(externalMediaModel, Locale.ENGLISH, DEFAULT_INTRIGO_IMAGE_THUMBNAIL)).willReturn("image.png");
		given(productModel.getCode()).willReturn("1");
		given(productModel.getDynamicName()).willReturn("title");
		given(productModel.getSummary(Locale.ENGLISH)).willReturn("short description");

		given(productModel.getFeatures()).willReturn(Collections.singletonList(productFeatureModel));
		given(productFeatureModel.getQualifier()).willReturn(WatersintegrationintrigoConstants.UNSPSC_QUALIFIER_NAME);
		given(productFeatureModel.getValue()).willReturn("un1");
	}

	@Test
	public void test_populate_PopulatesData_whenAvailable(){

		populator.populate(productModel, partData);

		Assert.assertEquals(createPartData("image.png", "un1"), partData);
	}

	@Test
	public void test_populate_PopulatesDefaultImage_WhenMissingPrimaryImage(){

		given(productModel.getPrimaryImage()).willReturn(null);

		populator.populate(productModel, partData);

		Assert.assertEquals(createPartData("default.png", "un1"), partData);
	}

	@Test
	public void test_populate_PopulatesNoUNSPSC_WhenMissing(){

		given(productFeatureModel.getQualifier()).willReturn("a");

		populator.populate(productModel, partData);

		Assert.assertEquals(createPartData("image.png", null), partData);
	}

	@Test
	public void test_populate_PopulatesNoUNSPSC_WhenMissingFeatures(){

		given(productModel.getFeatures()).willReturn(null);

		populator.populate(productModel, partData);

		Assert.assertEquals(createPartData("image.png", null), partData);
	}

	private PartData createPartData(final String image, final String unspsc){
		final PartData partData = new PartData();
		partData.setImage(image);
		partData.setUnspsc(unspsc);
		partData.setTitle("title");
		partData.setDescription("short description");
		partData.setPartNumber("1");
		partData.setId("1");
		return partData;
	}
}
