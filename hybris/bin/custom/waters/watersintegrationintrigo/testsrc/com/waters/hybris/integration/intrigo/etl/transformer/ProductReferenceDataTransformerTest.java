package com.waters.hybris.integration.intrigo.etl.transformer;

import com.waters.hybris.core.enums.SalesStatus;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.integration.intrigo.schema.RelatedPartData;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Collections;

import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class ProductReferenceDataTransformerTest
{
	private final ProductReferenceDataTransformer transformer = new ProductReferenceDataTransformer();

	@Mock
	private WatersProductModel productModel;

	@Mock
	private ProductReferenceModel productReferenceModel;

	@Mock
	private WatersProductModel referencedProduct;

	@Before
	public void setUp(){

		given(productModel.getProductReferences()).willReturn(Collections.singletonList(productReferenceModel));
		given(productReferenceModel.getTarget()).willReturn(referencedProduct);
		given(productReferenceModel.getActive()).willReturn(Boolean.TRUE);
		given(productModel.getCode()).willReturn("code1");
		given(referencedProduct.getCode()).willReturn("refer1");
	}

	@Test
	public void test_transform_PopulatesReferenceCodes(){
		given(referencedProduct.isTerminated()).willReturn(false);
		given(referencedProduct.getSalesStatus()).willReturn(SalesStatus.ACTIVE);
		final Collection<RelatedPartData> result = transformer.transform(productModel);
		Assert.assertEquals(Collections.singleton(createData()), result);
	}

	@Test
	public void test_transform_DoesntPopulateTerminatedReference(){
		given(referencedProduct.isTerminated()).willReturn(true);
		given(referencedProduct.getSalesStatus()).willReturn(SalesStatus.ACTIVE);
		final Collection<RelatedPartData> result = transformer.transform(productModel);
		Assert.assertTrue(result.isEmpty());
	}

	@Test
	public void test_transform_DoesntPopulateObsoleteReference(){
		given(referencedProduct.isTerminated()).willReturn(false);
		given(referencedProduct.getSalesStatus()).willReturn(SalesStatus.OBSOLETENOREPLACEMENT);
		final Collection<RelatedPartData> result = transformer.transform(productModel);
		Assert.assertTrue(result.isEmpty());
	}

	@Test
	public void test_transform_PopulatesEmpty_WhenNoTarget(){
		given(productReferenceModel.getTarget()).willReturn(null);

		final Collection<RelatedPartData> result = transformer.transform(productModel);

		Assert.assertEquals(Collections.emptySet(), result);
	}

	@Test
	public void test_transform_PopulatesEmpty_WhenNoReferences(){
		given(productModel.getProductReferences()).willReturn(null);

		final Collection<RelatedPartData> result = transformer.transform(productModel);

		Assert.assertEquals(Collections.emptySet(), result);
	}

	@Test
	public void test_transform_PopulatesEmpty_WhenReferenceIsInactive(){
		given(productReferenceModel.getActive()).willReturn(null);

		final Collection<RelatedPartData> result = transformer.transform(productModel);

		Assert.assertEquals(Collections.emptySet(), result);
	}

	private RelatedPartData createData(){
		return new RelatedPartData("code1", "refer1");
	}
}
