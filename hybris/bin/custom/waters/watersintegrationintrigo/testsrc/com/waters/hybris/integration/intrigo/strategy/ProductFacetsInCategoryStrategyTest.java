package com.waters.hybris.integration.intrigo.strategy;

import com.google.common.collect.Lists;
import com.waters.hybris.integration.intrigo.schema.CategoryData;
import com.waters.hybris.integration.intrigo.strategy.impl.WatersProductFacetsInCategoryStrategy;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.stream.Collectors;

import static com.waters.hybris.core.util.WatersAssertUtils.assertCollectionContainsSameElements;
import static java.util.Arrays.asList;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class ProductFacetsInCategoryStrategyTest
{
	private static final String CLASS_NAME = "Standards";
	private static final String QUALIFIER_1 = "c1-Standards.field_code1";
	private static final String QUALIFIER_2 = "c1-Standards.field_code2";
	private static final String CATEGORY_CODE = "c1";
	private static final String FIELD_1 = "field_code1";
	private static final String FIELD_2 = "field_code2";
	private static final String NAME_1 = "field_name1";
	private static final String NAME_2 = "field_name1";
	private static final String VALUE_1 = "value1";
	private static final String VALUE_2 = "value2";


	private Comparator<CategoryData> CATEGORY_DATA_COMPARATOR = (a,b) -> a.getNavigationId().compareTo(b.getNavigationId());

	@InjectMocks
	private ProductFacetsInCategoryStrategy strategy = new WatersProductFacetsInCategoryStrategy();

	@Mock
	private ClassificationService classificationService;

	@Mock
	private CategoryService categoryService;

	@Mock
	private FeatureList featureList;

	@Mock
	private CatalogVersionModel catalogVersion;

	@Mock
	private CategoryModel category;

	@Mock
	private ProductModel product1;

	@Mock
	private ProductModel product2;

	@Mock
	private Feature feature1;

	@Mock
	private Feature feature2;

	@Mock
	private FeatureValue featureValue1;

	@Mock
	private FeatureValue featureValue2;

	@Mock
	private ClassAttributeAssignmentModel classAttributeAssignment1;

	@Mock
	private ClassificationAttributeModel classificationAttribute1;

	@Mock
	private ClassificationAttributeValueModel classificationAttributeValue1;

	@Mock
	private ClassificationAttributeValueModel classificationAttributeValue2;

	@Mock
	private ClassificationClassModel classificationClass;

	private CategoryData categoryData = new CategoryData();

	@Before
	public void setUp()
	{
		given(classificationService.getFeatures(product1)).willReturn(featureList);
		given(featureList.getFeatures()).willReturn(Collections.singletonList(feature1));
		given(feature1.getClassAttributeAssignment()).willReturn(classAttributeAssignment1);
		given(classAttributeAssignment1.getClassificationAttribute()).willReturn(classificationAttribute1);
		given(classAttributeAssignment1.isFacet()).willReturn(true);
		given(classAttributeAssignment1.getClassificationClass()).willReturn(classificationClass);
		given(classificationClass.getCode()).willReturn(CLASS_NAME);
		given(classificationAttribute1.getCode()).willReturn(FIELD_1);
		given(classificationAttribute1.getName(Locale.ENGLISH)).willReturn(NAME_1);
		given(feature1.getValues()).willReturn(Collections.singletonList(featureValue1));
		given(featureValue1.getValue()).willReturn(classificationAttributeValue1);
		given(classificationAttributeValue1.getName(Locale.ENGLISH)).willReturn(VALUE_1);
		given(classAttributeAssignment1.getPosition()).willReturn(1);


		given(categoryService.getCategoryForCode(catalogVersion, CATEGORY_CODE)).willReturn(category);
		given(category.getProducts()).willReturn(Lists.newArrayList(product1));

		categoryData.setNavigationId(CATEGORY_CODE);
		categoryData.setNavLevel("1");
	}

	@Test
	public void test_calculateFacets_getFeatures()
	{

		final Collection<CategoryData> result = strategy.calculateFacets(categoryData, catalogVersion).collect(Collectors.toList());

		assertCollectionContainsSameElements(asList(
			createIntrigoCategory(CATEGORY_CODE, QUALIFIER_1, "10", NAME_1),
			createIntrigoCategory(QUALIFIER_1, QUALIFIER_1 + ":" + VALUE_1, "100", VALUE_1)), result, CATEGORY_DATA_COMPARATOR);

	}

	@Test
	public void test_calculateFacets_getFeaturesWithUnitOfMeasure()
	{

		final ClassificationAttributeUnitModel classificationAttributeUnitModel = mock(ClassificationAttributeUnitModel.class);
		given(featureValue1.getUnit()).willReturn(classificationAttributeUnitModel);
		given(classificationAttributeUnitModel.getSymbol()).willReturn("mg");

		final Collection<CategoryData> result = strategy.calculateFacets(categoryData, catalogVersion).collect(Collectors.toList());

		assertCollectionContainsSameElements(asList(
			createIntrigoCategory(CATEGORY_CODE, QUALIFIER_1, "10", NAME_1),
			createIntrigoCategory(QUALIFIER_1, QUALIFIER_1 + ":" + VALUE_1 + "_mg", "100", VALUE_1 + " mg")), result, CATEGORY_DATA_COMPARATOR);

	}


	@Test
	public void test_calculateFacets_getFeatures_ReturnsEmpty_WhenNull()
	{
		given(category.getProducts()).willReturn(null);

		final Collection<CategoryData> result = strategy.calculateFacets(categoryData, catalogVersion).collect(Collectors.toList());

		assertCollectionContainsSameElements(Collections.emptyList(), result, CATEGORY_DATA_COMPARATOR);

	}

	@Test
	public void test_calculateFacets_getFeatures_ReturnsEmpty_WhenFieldIsNOTFacet()
	{
		given(classAttributeAssignment1.isFacet()).willReturn(false);

		final Collection<CategoryData> result = strategy.calculateFacets(categoryData, catalogVersion).collect(Collectors.toList());

		assertCollectionContainsSameElements(Collections.emptyList(), result, CATEGORY_DATA_COMPARATOR);

	}

	@Test
	public void test_calculateFacets_getFeatures_ReturnsEmpty_WhenEmptyCollection()
	{
		given(category.getProducts()).willReturn(Collections.emptyList());

		final Collection<CategoryData> result = strategy.calculateFacets(categoryData, catalogVersion).collect(Collectors.toList());

		assertCollectionContainsSameElements(Collections.emptyList(), result, CATEGORY_DATA_COMPARATOR);

	}

	@Test
	public void test_calculateFacets_getFeatures_ReturnsEmpty_WhenEmptyValuesInFeature()
	{
		given(category.getProducts()).willReturn(Collections.singletonList(product1));
		given(feature1.getValues()).willReturn(Collections.emptyList());

		final Collection<CategoryData> result = strategy.calculateFacets(categoryData, catalogVersion).collect(Collectors.toList());

		assertCollectionContainsSameElements(Collections.emptyList(), result, CATEGORY_DATA_COMPARATOR);

	}

	@Test
	public void test_calculateFacets_getFeatures_ReturnsEmpty_WhenAProductHasNoFeatures()
	{
		given(category.getProducts()).willReturn(asList(product1));
		given(featureList.getFeatures()).willReturn(Collections.emptyList());

		final Collection<CategoryData> result = strategy.calculateFacets(categoryData, catalogVersion).collect(Collectors.toList());

		assertCollectionContainsSameElements(Collections.emptyList(), result, CATEGORY_DATA_COMPARATOR);

	}

	@Test
	public void test_calculateFacets_getFeatures_GetPosition_WhenNull()
	{
		given(classAttributeAssignment1.getPosition()).willReturn(null);

		final Collection<CategoryData> result = strategy.calculateFacets(categoryData, catalogVersion).collect(Collectors.toList());

		CategoryData parent = createIntrigoCategory(CATEGORY_CODE, QUALIFIER_1, "10", NAME_1);
		CategoryData child = createIntrigoCategory(QUALIFIER_1, QUALIFIER_1 + ":" + VALUE_1, "100", VALUE_1);
		parent.setSortOrder("0");
		child.setSortOrder("0");

		assertCollectionContainsSameElements(asList(parent, child), result, CATEGORY_DATA_COMPARATOR);

	}

	@Test
	public void test_calculateFacets_getFeaturesWhenMoreThanOne()
	{

		given(category.getProducts()).willReturn(Lists.newArrayList(product1, product2));
		mockClassificationAttribute2();

		final Collection<CategoryData> result = strategy.calculateFacets(categoryData, catalogVersion).collect(Collectors.toList());

		assertCollectionContainsSameElements(asList(
			createIntrigoCategory(CATEGORY_CODE, QUALIFIER_1, "10", NAME_1),
			createIntrigoCategory(CATEGORY_CODE, QUALIFIER_2, "10", NAME_2),
			createIntrigoCategory(QUALIFIER_1, QUALIFIER_1 + ":" + VALUE_1, "100", VALUE_1),
			createIntrigoCategory(QUALIFIER_2, QUALIFIER_2 + ":" + VALUE_2, "100", VALUE_2)), result, CATEGORY_DATA_COMPARATOR);

	}


	@Test
	public void test_calculateFacets_getOnlyOneFacetName_AndMultipleValues()
	{

		given(category.getProducts()).willReturn(Lists.newArrayList(product1, product2));
		given(classificationAttribute1.getCode()).willReturn(FIELD_2);
		mockClassificationAttribute2();

		final Collection<CategoryData> result = strategy.calculateFacets(categoryData, catalogVersion).collect(Collectors.toList());

		assertCollectionContainsSameElements(asList(
			createIntrigoCategory(CATEGORY_CODE, QUALIFIER_2, "10", NAME_2),
			createIntrigoCategory(QUALIFIER_2, QUALIFIER_2 + ":" + VALUE_2, "100", VALUE_2),
			createIntrigoCategory(QUALIFIER_2, QUALIFIER_2 + ":" + VALUE_1, "100", VALUE_1)), result, CATEGORY_DATA_COMPARATOR);

	}

	@Test
	public void test_calculateFacets_getOnlyOneFacetName_AndOnlyOneDifferentValue()
	{

		given(category.getProducts()).willReturn(Lists.newArrayList(product1, product2));
		given(classificationAttribute1.getCode()).willReturn(FIELD_2);
		given(feature1.getValues()).willReturn(Collections.singletonList(featureValue2));

		mockClassificationAttribute2();
		given(featureValue2.getValue()).willReturn(classificationAttributeValue2);
		given(classificationAttributeValue2.getName(Locale.ENGLISH)).willReturn(VALUE_1);

		final Collection<CategoryData> result = strategy.calculateFacets(categoryData, catalogVersion).collect(Collectors.toList());

		assertCollectionContainsSameElements(asList(
			createIntrigoCategory(CATEGORY_CODE, QUALIFIER_2, "10", NAME_2),
			createIntrigoCategory(QUALIFIER_2, QUALIFIER_2 + ":" + VALUE_1, "100", VALUE_1)), result, CATEGORY_DATA_COMPARATOR);

	}

	@Test
	public void test_calculateFacets_getEmpty_WhenAssignmentClassificationClassIsNull()
	{
		given(classAttributeAssignment1.getClassificationClass()).willReturn(null);

		final Collection<CategoryData> result = strategy.calculateFacets(categoryData, catalogVersion).collect(Collectors.toList());

		assertCollectionContainsSameElements(Collections.emptyList(), result, CATEGORY_DATA_COMPARATOR);

	}

	@Test
	public void test_calculateFacets_getEmpty_WhenAssignmentClassificationAttributeIsNull()
	{
		given(classAttributeAssignment1.getClassificationAttribute()).willReturn(null);

		final Collection<CategoryData> result = strategy.calculateFacets(categoryData, catalogVersion).collect(Collectors.toList());

		assertCollectionContainsSameElements(Collections.emptyList(), result, CATEGORY_DATA_COMPARATOR);

	}

	@Test
	public void test_calculateFacets_getEmpty_WhenAssignmentIsNull()
	{
		given(feature1.getClassAttributeAssignment()).willReturn(null);

		final Collection<CategoryData> result = strategy.calculateFacets(categoryData, catalogVersion).collect(Collectors.toList());

		assertCollectionContainsSameElements(Collections.emptyList(), result, CATEGORY_DATA_COMPARATOR);

	}

	private CategoryData createIntrigoCategory(String parent, String code, final String level, final String title)
	{
		final CategoryData categoryData = new CategoryData();
		categoryData.setParentId(parent);
		categoryData.setNavigationId(code);
		categoryData.setNavLevel(level);
		categoryData.setTitle(title);
		categoryData.setSortOrder("1");
		categoryData.setNavigationType("S");
		return categoryData;
	}

	private void mockClassificationAttribute2()
	{
		final FeatureList featureList2 = mock(FeatureList.class);
		given(featureList2.getFeatures()).willReturn(Collections.singletonList(feature2));
		given(classificationService.getFeatures(product2)).willReturn(featureList2);

		final ClassAttributeAssignmentModel classAttributeAssignment2 = mock(ClassAttributeAssignmentModel.class);
		final ClassificationAttributeModel classificationAttribute2 = mock(ClassificationAttributeModel.class);
		final ClassificationAttributeValueModel classificationAttributeValue2 = mock(ClassificationAttributeValueModel.class);

		given(feature2.getClassAttributeAssignment()).willReturn(classAttributeAssignment2);
		given(classAttributeAssignment2.getClassificationAttribute()).willReturn(classificationAttribute2);
		given(classAttributeAssignment2.isFacet()).willReturn(Boolean.TRUE);
		given(classificationAttribute2.getName(Locale.ENGLISH)).willReturn(NAME_2);
		given(classificationAttribute2.getCode()).willReturn(FIELD_2);
		given(classAttributeAssignment2.getClassificationClass()).willReturn(classificationClass);
		given(feature2.getValues()).willReturn(Collections.singletonList(featureValue2));
		given(featureValue2.getValue()).willReturn(classificationAttributeValue2);
		given(classificationAttributeValue2.getName(Locale.ENGLISH)).willReturn(VALUE_2);
		given(classAttributeAssignment2.getPosition()).willReturn(1);
	}

}
