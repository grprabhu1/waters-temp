package com.waters.hybris.stepdefs.intrigo.intrigo;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.waters.hybris.core.enums.SalesStatus;
import com.waters.hybris.core.model.model.PunchoutCategoryModel;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.util.StreamUtil;
import com.waters.hybris.integration.core.model.record.OutboundFileRecordModel;
import com.waters.hybris.integration.intrigo.schema.CategoryData;
import com.waters.hybris.integration.intrigo.schema.FeatureData;
import com.waters.hybris.integration.intrigo.schema.PartData;
import com.waters.hybris.integration.intrigo.schema.PartToNavigationData;
import com.waters.hybris.integration.intrigo.schema.RelatedPartData;
import com.waters.hybris.integration.intrigo.util.IntrigoHelper;
import com.waters.hybris.stepdefs.integration.helper.IntegrationHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.time.TimeService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.waters.hybris.integration.core.enums.FileType.WATERS_INTRIGO;
import static com.waters.hybris.integration.intrigo.constants.WatersintegrationintrigoConstants.UNSPSC_QUALIFIER_NAME;
import static org.assertj.core.api.Assertions.assertThat;

public class IntrigoStepDefs
{
	private static final Logger LOG = LoggerFactory.getLogger(IntrigoStepDefs.class);

	protected static final char DELIMITER = ',';

	private IntegrationHelper integrationHelper;
	private TimeService timeService;

	@Then("^outbound file record for \"(.*?)\" is created$")
	public void outbound_file_record_for(final String feedType)
	{
		final List<OutboundFileRecordModel> outboundRecords = getIntegrationHelper().getPendingOutboundRecords(feedType + ".csv", WATERS_INTRIGO);
		Assert.assertTrue("Outbound file record did not get created", CollectionUtils.isNotEmpty(outboundRecords));
		Assert.assertEquals("Outbound file record is not recent enough", getTimeService().getCurrentTime().getTime(), outboundRecords.get(0).getCreationtime().getTime(), 5000);
	}

	@Then("^Feed file for parts generated with expected data$")
	public void feed_file_for_parts()
	{
		final File feedFile = getGeneratedFeed("parts");

		final List<PartData> feedData = extractFeedData(feedFile, PartData.class);

		final List<WatersProductModel> publishedActiveProducts = getIntegrationHelper().getPublishedActiveProducts();

		validatePartsFeedData(publishedActiveProducts, feedData);
	}

	@Then("^Feed file for related parts generated with expected data$")
	public void feed_file_for_relatedParts()
	{
		final File feedFile = getGeneratedFeed("related_parts");

		final List<RelatedPartData> feedData = extractFeedData(feedFile, RelatedPartData.class);

		final List<WatersProductModel> publishedActiveProducts = getIntegrationHelper().getPublishedActiveProducts();

		validateRelatedPartsFeedData(publishedActiveProducts, feedData);
	}

	@Then("^Feed file for parts to navigation generated with expected data$")
	public void feed_file_for_partToNavigation()
	{
		final File feedFile = getGeneratedFeed("parts_to_navigation");

		final List<PartToNavigationData> feedData = extractFeedData(feedFile, PartToNavigationData.class);

		final List<WatersProductModel> publishedActiveProducts = getIntegrationHelper().getPublishedActiveProducts();

		validatePartToNavigationDataFeedData(publishedActiveProducts, feedData);
	}

	@And("^Feed file for part to navigation generated with the following data$")
	public void feed_file_for_part_to_navigation(final List<PartToNavigationData> expectedData)
	{
		final File feedFile = getGeneratedFeed("parts_to_navigation");

		final List<PartToNavigationData> feedData = extractFeedData(feedFile, PartToNavigationData.class);

		validateFeedDataWithExpectedData(expectedData, feedData);
	}

	@Then("^Feed file for features generated with expected data$")
	public void feed_file_for_features()
	{
		final File feedFile = getGeneratedFeed("features");

		final List<FeatureData> feedData = extractFeedData(feedFile, FeatureData.class);

		final List<WatersProductModel> publishedActiveProducts = getIntegrationHelper().getPublishedActiveProducts();

		validateFeaturesFeedData(publishedActiveProducts, feedData);
	}

	@Then("^Feed file for category navigation generated with the following data$")
	public void feed_file_for_category_navigation(final List<CategoryData> expectedData)
	{
		final File feedFile = getGeneratedFeed("category_navigation");

		final List<CategoryData> feedData = extractFeedData(feedFile, CategoryData.class);

		validateFeedDataWithExpectedData(expectedData, feedData);
	}

	protected File getGeneratedFeed(final String feedType)
	{
		final List<File> feedFiles = getIntegrationHelper().getFeedFiles(feedType + ".csv", "watersintegrationintrigo.intrigoFeed.destinationpath");
		Assert.assertTrue("Feed file did not get created", CollectionUtils.isNotEmpty(feedFiles));

		final File feedFile = feedFiles.get(0);
		Assert.assertEquals("Feed file is not recent enough", getTimeService().getCurrentTime().getTime(), feedFile.lastModified(), 5000);

		return feedFile;
	}

	protected static <T> List<T> extractFeedData(final File feedFile, final Class<T> dataType)
	{
		final List<T> partData = extractDataFromCvsFeed(feedFile, dataType);
		Assert.assertNotNull("Failed to extract data from feed ", partData);
		Assert.assertTrue("Feed contained no data", !partData.isEmpty());

		return partData;
	}

	protected static <T> List<T> extractDataFromCvsFeed(final File file, final Class<T> dataType)
	{
		final CsvMapper mapper = new CsvMapper();

		final CsvSchema schema = mapper
			.schemaFor(dataType)
			.withSkipFirstDataRow(true)
			.withColumnSeparator(DELIMITER);

		final ObjectReader objectReader = mapper
			.readerWithSchemaFor(dataType)
			.with(schema)
			.with(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES);

		try
		{
			final MappingIterator<T> iterator = objectReader.readValues(file);
			final List<T> result = new ArrayList<>();
			while(iterator.hasNext())
			{
				result.add(iterator.next());
			}

			return result;
		}
		catch (final IOException e)
		{
			LOG.error("Failed to read feed file", e);
		}

		return null;
	}

	protected void validatePartsFeedData(final List<WatersProductModel> publishedActiveProducts, final List<PartData> feedData)
	{
		Assert.assertEquals("size mismatch between feed and system", publishedActiveProducts.size(), feedData.size());

		publishedActiveProducts.forEach(p -> {
			final Optional<PartData> data = feedData.stream()
				.filter(f -> f.getPartNumber().equalsIgnoreCase(p.getCode()))
				.findFirst();

			Assert.assertTrue("Product details missing for product " + p.getCode(), data.isPresent());

			validateData("Product title mismatch for product " + p.getCode(), p.getDynamicName(), data.get().getTitle());
			validateData("Product short description mismatch for product " + p.getCode(), p.getSummary(Locale.ENGLISH), data.get().getDescription());
			Assert.assertNotNull("Product image url missing for product " + p.getCode(), data.get().getImage());
			final String productUnspsc = p.getFeatures().stream()
				.filter(f -> UNSPSC_QUALIFIER_NAME.equals(f.getQualifier()))
				.map(ProductFeatureModel::getValue)
				.map(Object::toString)
				.findFirst()
				.orElse(Strings.EMPTY);
			validateData("Product Unspsc mismatch for product " + p.getCode(), productUnspsc, data.get().getUnspsc());
		});
	}

	protected void validateRelatedPartsFeedData(final List<WatersProductModel> publishedActiveProducts, final List<RelatedPartData> feedData)
	{
		final Set<WatersProductModel> productWithReferences = publishedActiveProducts.stream()
			.filter(p -> CollectionUtils.isNotEmpty(p.getProductReferences()))
			.collect(Collectors.toSet());

		productWithReferences.forEach(p -> {
			final Set<String> relatedPartsInFeed = feedData.stream()
				.filter(f -> f.getNavigationId().equalsIgnoreCase(p.getCode()))
				.map(RelatedPartData::getPartNumber)
				.collect(Collectors.toSet());

			final Set<String> relatedProductsInSystem = p.getProductReferences().stream()
				.filter(ProductReferenceModel::getActive)
				.map(ProductReferenceModel::getTarget)
				.filter(WatersProductModel.class::isInstance)
				.map(WatersProductModel.class::cast)
				.filter(wp -> !wp.isTerminated())
				.filter(wp -> SalesStatus.ACTIVE == wp.getSalesStatus())
				.map(ProductModel::getCode)
				.collect(Collectors.toSet());

			Assert.assertEquals("related parts size mismatch between feed and the system for product " + p.getCode(), relatedProductsInSystem.size(), relatedPartsInFeed.size());
			Assert.assertTrue("related parts mismatch for product " + p.getCode(), relatedProductsInSystem.containsAll(relatedPartsInFeed));
		});
	}

	protected void validatePartToNavigationDataFeedData(final List<WatersProductModel> publishedActiveProducts, final List<PartToNavigationData> feedData)
	{
		publishedActiveProducts.forEach(p -> {
			final Set<String> navigationsForProductInFeed = feedData.stream()
				.filter(f -> f.getPartNumber().equalsIgnoreCase(p.getCode()))
				.map(PartToNavigationData::getNavigationId)
				.collect(Collectors.toSet());

			final Set<String> productPunchoutCategories = p.getSupercategories().stream()
				.filter(PunchoutCategoryModel.class::isInstance)
				.map(PunchoutCategoryModel.class::cast)
				.map(PunchoutCategoryModel::getCode)
				.collect(Collectors.toSet());

			Assert.assertTrue("part to navigation (product categories) mismatch for product " + p.getCode(), navigationsForProductInFeed.containsAll(productPunchoutCategories));

			final Set<String> productParentPunchoutCategories = p.getSupercategories().stream()
				.filter(PunchoutCategoryModel.class::isInstance)
				.map(CategoryModel::getAllSupercategories)
				.flatMap(Collection::stream)
				.distinct()
				.filter(c -> CollectionUtils.isNotEmpty(c.getSupercategories()))
				.filter(PunchoutCategoryModel.class::isInstance)
				.map(PunchoutCategoryModel.class::cast)
				.map(PunchoutCategoryModel::getCode)
				.collect(Collectors.toSet());

			Assert.assertTrue("part to navigation (product parent categories) mismatch for product " + p.getCode(), navigationsForProductInFeed.containsAll(productParentPunchoutCategories));

			if (CollectionUtils.isNotEmpty(productPunchoutCategories))
			{
				productPunchoutCategories.stream()
					.forEach(c -> {
						final Set<String> facetNamesInSystem = StreamUtil.nullSafe(p.getFeatures())
							.filter(pf -> pf.getClassificationAttributeAssignment().isFacet())
							.map(pf -> IntrigoHelper.BUILD_FACET_NAVIGATION_ID.apply(c, pf.getClassificationAttributeAssignment()))
							.collect(Collectors.toSet());

						Assert.assertTrue("part to navigation (facet name) mismatch for product " + p.getCode(), navigationsForProductInFeed.containsAll(facetNamesInSystem));
					});
			}
		});
	}

	protected void validateFeaturesFeedData(final List<WatersProductModel> publishedActiveProducts, final List<FeatureData> feedData)
	{
		publishedActiveProducts.forEach(p -> {
			final Set<ProductFeatureModel> productFeatures = p.getFeatures().stream()
				.filter(pf -> !pf.getClassificationAttributeAssignment().isInternalOnly())
				.collect(Collectors.toSet());

			productFeatures.stream()
				.forEach(pf -> {
					final String featureName = pf.getClassificationAttributeAssignment().getClassificationAttribute().getName(Locale.ENGLISH);
					final String featureValue = pf.getValue() instanceof String ? (String) pf.getValue() : ((ClassificationAttributeValueModel) pf.getValue()).getName(Locale.ENGLISH);

					final Optional<FeatureData> featureData = feedData.stream()
						.filter(f -> f.getPartNumber().equalsIgnoreCase(p.getCode()) && f.getFeatureTitle().equalsIgnoreCase(featureName) && f.getValue().startsWith(featureValue))
						.findFirst();

					Assert.assertTrue("Feature not found in the feed [" + featureName + ":" + featureValue + "] for product " + p.getCode(), featureData.isPresent());
				});

			// spl case - hazardous feature value
			final String hazardousFeatureValue = p.isHazardous() ? "Y" : "N";
			final Optional<FeatureData> featureData = feedData.stream()
				.filter(f -> f.getPartNumber().equalsIgnoreCase(p.getCode()) && f.getFeatureTitle().equalsIgnoreCase("Hazardous") && f.getValue().equalsIgnoreCase(hazardousFeatureValue))
				.findFirst();
			Assert.assertTrue("Feature not found in the feed [Hazardous:" + hazardousFeatureValue + "] for product " + p.getCode(), featureData.isPresent());
		});
	}

	protected <T> void validateFeedDataWithExpectedData(final List<T> expectedData, final List<T> feedData)
	{
		Assert.assertTrue("Not all expected entries are present in the feed", feedData.containsAll(expectedData));
	}

	protected void validateData(final String errorMessage, final String expected, final String actual)
	{
		assertThat(actual)
			.as(errorMessage)
			.isIn(expected, expected == null ? "" : expected);
	}

	protected IntegrationHelper getIntegrationHelper()
	{
		return integrationHelper;
	}

	@Required
	public void setIntegrationHelper(final IntegrationHelper integrationHelper)
	{
		this.integrationHelper = integrationHelper;
	}

	protected TimeService getTimeService()
	{
		return timeService;
	}

	@Required
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}
}
