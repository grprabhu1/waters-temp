package com.waters.hybris.integration.intrigo.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

//PART_NUMBER,RELATED_PART_NUMBER
@JsonPropertyOrder({
		"PART_NUMBER",
		"RELATED_PART_NUMBER"
})
public class RelatedPartData
{
	private String navigationId;
	private String partNumber;

	public RelatedPartData()
	{
		// empty
	}

	public RelatedPartData(final String navigationId, final String partNumber)
	{
		this.navigationId = navigationId;
		this.partNumber = partNumber;
	}

	@JsonProperty(value = "PART_NUMBER")
	public String getNavigationId()
	{
		return navigationId;
	}

	public void setNavigationId(final String navigationId)
	{
		this.navigationId = navigationId;
	}

	@JsonProperty(value = "RELATED_PART_NUMBER")
	public String getPartNumber()
	{
		return partNumber;
	}

	public void setPartNumber(final String partNumber)
	{
		this.partNumber = partNumber;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final RelatedPartData that = (RelatedPartData) o;

		if (getNavigationId() != null ? !getNavigationId().equals(that.getNavigationId()) : that.getNavigationId() != null)
		{
			return false;
		}
		return getPartNumber() != null ? getPartNumber().equals(that.getPartNumber()) : that.getPartNumber() == null;
	}

	@Override
	public int hashCode()
	{
		int result = getNavigationId() != null ? getNavigationId().hashCode() : 0;
		result = 31 * result + (getPartNumber() != null ? getPartNumber().hashCode() : 0);
		return result;
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder("RelatedPartData{");
		sb.append("navigationId='").append(navigationId).append('\'');
		sb.append(", partNumber='").append(partNumber).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
