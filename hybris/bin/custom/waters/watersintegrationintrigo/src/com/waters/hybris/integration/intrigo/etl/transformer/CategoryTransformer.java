package com.waters.hybris.integration.intrigo.etl.transformer;

import com.neoworks.hybris.etl.context.EtlContext;
import com.neoworks.hybris.etl.exceptions.EtlValidationException;
import com.waters.hybris.integration.core.etl.transformer.WatersDataTransformerSkipRecord;
import com.waters.hybris.integration.intrigo.schema.CategoryData;
import com.waters.hybris.integration.intrigo.strategy.ProductFacetsInCategoryStrategy;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CategoryTransformer extends WatersDataTransformerSkipRecord<CategoryModel, Collection<CategoryData>>
{

	private static final Logger LOG = LoggerFactory.getLogger(CategoryTransformer.class);

	private Converter<CategoryModel, CategoryData> categoryConverter;
	private ProductFacetsInCategoryStrategy productFacetsInCategoryStrategy;
	private CatalogVersionModel catalogVersionModel;
	private String catalogVersionParamKey;

	@Override
	public void init(final EtlContext context) throws EtlValidationException
	{
		super.init(context);
		final Object catalog = getContext().getParameters().get(getCatalogVersionParamKey());
		if (catalog instanceof CatalogVersionModel)
		{
			this.catalogVersionModel = (CatalogVersionModel) catalog;
		}
		else
		{
			throw new EtlValidationException("Missing catalog version");
		}
	}

	@Override
	public Collection<CategoryData> internalTransform(final CategoryModel categoryModel)
	{
		return getSubCategoriesForRoot(categoryModel).collect(Collectors.toList());
	}

	private Stream<CategoryData> getSubCategoriesForRoot(final CategoryModel categoryModel)
	{
		if (CollectionUtils.isNotEmpty(categoryModel.getCategories()))
		{
			final Set<CategoryData> categories = categoryModel.getCategories().stream().flatMap(c -> getSubCategories(c, 0)).collect(Collectors.toSet());
			final Stream<CategoryData> features = categories.stream().flatMap(this::calculateFeatures);
			return Stream.concat(categories.stream(), features);
		}
		return Stream.empty();
	}

	private Stream<CategoryData> calculateFeatures(final CategoryData categoryData)
	{
		return getProductFacetsInCategoryStrategy().calculateFacets(categoryData, getCatalogVersionModel());
	}

	protected Stream<CategoryData> getSubCategories(final CategoryModel category, final int level)
	{
		if (CollectionUtils.isNotEmpty(category.getCategories()))
		{
			return Stream.concat(Stream.of(convertIntrigoCategory(category, level)),
				category.getCategories().stream()
					.flatMap(c -> getSubCategories(c, level + 1)));
		}
		return Stream.of(convertIntrigoCategory(category, level));
	}

	private CategoryData convertIntrigoCategory(final CategoryModel category, final int level)
	{
		final CategoryData categoryData = getCategoryConverter().convert(category);
		categoryData.setNavLevel(String.valueOf(level));
		return categoryData;
	}

	public Converter<CategoryModel, CategoryData> getCategoryConverter()
	{
		return categoryConverter;
	}

	@Required
	public void setCategoryConverter(final Converter<CategoryModel, CategoryData> categoryConverter)
	{
		this.categoryConverter = categoryConverter;
	}

	public ProductFacetsInCategoryStrategy getProductFacetsInCategoryStrategy()
	{
		return productFacetsInCategoryStrategy;
	}

	@Required
	public void setProductFacetsInCategoryStrategy(final ProductFacetsInCategoryStrategy productFacetsInCategoryStrategy)
	{
		this.productFacetsInCategoryStrategy = productFacetsInCategoryStrategy;
	}

	public CatalogVersionModel getCatalogVersionModel()
	{
		return catalogVersionModel;
	}

	public String getCatalogVersionParamKey()
	{
		return catalogVersionParamKey;
	}

	@Required
	public void setCatalogVersionParamKey(final String catalogVersionParamKey)
	{
		this.catalogVersionParamKey = catalogVersionParamKey;
	}
}
