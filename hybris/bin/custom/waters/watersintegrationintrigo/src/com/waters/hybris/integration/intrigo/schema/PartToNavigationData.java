package com.waters.hybris.integration.intrigo.schema;

//NAVIGATION_ID,PART_NUMBER

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonPropertyOrder({
		"NAVIGATION_ID",
		"PART_NUMBER"
})
public class PartToNavigationData
{
	private String navigationId;
	private String partNumber;

	public PartToNavigationData()
	{
		// empty
	}

	public PartToNavigationData(final String navigationId, final String partNumber)
	{
		this.navigationId = navigationId;
		this.partNumber = partNumber;
	}

	@JsonProperty(value = "NAVIGATION_ID")
	public String getNavigationId()
	{
		return navigationId;
	}

	public void setNavigationId(final String navigationId)
	{
		this.navigationId = navigationId;
	}

	@JsonProperty(value = "PART_NUMBER")
	public String getPartNumber()
	{
		return partNumber;
	}

	public void setPartNumber(final String partNumber)
	{
		this.partNumber = partNumber;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		final PartToNavigationData that = (PartToNavigationData) o;
		return Objects.equals(getNavigationId(), that.getNavigationId()) &&
			Objects.equals(getPartNumber(), that.getPartNumber());
	}

	@Override
	public int hashCode()
	{

		return Objects.hash(getNavigationId(), getPartNumber());
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder("PartToNavigationData{");
		sb.append("navigationId='").append(navigationId).append('\'');
		sb.append(", partNumber='").append(partNumber).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
