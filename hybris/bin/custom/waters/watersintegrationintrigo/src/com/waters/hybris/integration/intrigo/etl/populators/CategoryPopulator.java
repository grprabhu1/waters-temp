package com.waters.hybris.integration.intrigo.etl.populators;

import com.waters.hybris.core.strategies.ProductImageResolutionStrategy;
import com.waters.hybris.integration.intrigo.schema.CategoryData;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Locale;
import java.util.Map;

import static com.waters.hybris.integration.intrigo.constants.WatersintegrationintrigoConstants.DEFAULT_INTRIGO_IMAGE_THUMBNAIL;
import static com.waters.hybris.integration.intrigo.constants.WatersintegrationintrigoConstants.INTRIGO_IMAGE_RENDITION;

public class CategoryPopulator implements Populator<CategoryModel, CategoryData>
{

	private static final String DEFAULT_PARENT_ID = "0";
	private static final String NAVIGATION_TYPE_FACET = "S";
	private static final String NAVIGATION_TYPE_CATEGORY = "P";
	private static final Locale localeEn = Locale.ENGLISH;
	private ProductImageResolutionStrategy productImageResolutionStrategy;
	private ConfigurationService configurationService;
	private Map<String, String> intrigoParentIdMapping;

	@Override
	public void populate(final CategoryModel source, final CategoryData target) throws ConversionException
	{
		target.setDescription(source.getDescription(localeEn));
		target.setNavigationId(source.getCode());
		target.setTitle(source.getName(localeEn));
		if(source.getOrder() != null)
		{
			target.setSortOrder(String.valueOf(source.getOrder()));
		}
		if (CollectionUtils.isNotEmpty(source.getSupercategories()))
		{
			final String parentId = source.getSupercategories().get(0).getCode();
			target.setParentId(getIntrigoParentIdMapping().getOrDefault(parentId, parentId));
		}
		else
		{
			target.setParentId(DEFAULT_PARENT_ID);
		}

		if (source.getPrimaryImage() != null)
		{
			final String primaryImageUrl = getProductImageResolutionStrategy().resolveImageUrl(source.getPrimaryImage(), localeEn, getRendition());
			target.setImageUrl(primaryImageUrl);
			target.setImageAltText(source.getPrimaryImage().getAlternativeText(localeEn));
		}
		if (source instanceof VariantCategoryModel || source instanceof VariantValueCategoryModel)
		{
			target.setNavigationType(NAVIGATION_TYPE_FACET);
		}
		else
		{
			target.setNavigationType(NAVIGATION_TYPE_CATEGORY);
		}
	}

	private String getRendition()
	{
		return getConfigurationService().getConfiguration().getString(INTRIGO_IMAGE_RENDITION, DEFAULT_INTRIGO_IMAGE_THUMBNAIL);
	}

	public ProductImageResolutionStrategy getProductImageResolutionStrategy()
	{
		return productImageResolutionStrategy;
	}

	@Required
	public void setProductImageResolutionStrategy(final ProductImageResolutionStrategy productImageResolutionStrategy)
	{
		this.productImageResolutionStrategy = productImageResolutionStrategy;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}
	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected Map<String, String> getIntrigoParentIdMapping()
	{
		return intrigoParentIdMapping;
	}

	@Required
	public void setIntrigoParentIdMapping(final Map<String, String> intrigoParentIdMapping)
	{
		this.intrigoParentIdMapping = intrigoParentIdMapping;
	}
}
