package com.waters.hybris.integration.intrigo.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

//ID,PARTNUMBER,TITLE,IMAGE,DESCRIPTION,UNSPSC
@JsonPropertyOrder({
		"ID",
		"PARTNUMBER",
		"TITLE",
		"IMAGE",
		"DESCRIPTION",
		"UNSPSC"
})
public class PartData
{
	private String id;
	private String partNumber;
	private String title;
	private String image;
	private String description;
	private String unspsc;

	@JsonProperty(value = "ID")
	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	@JsonProperty(value = "PARTNUMBER")
	public String getPartNumber()
	{
		return partNumber;
	}

	public void setPartNumber(final String partNumber)
	{
		this.partNumber = partNumber;
	}

	@JsonProperty(value = "TITLE")
	public String getTitle()
	{
		return title;
	}

	public void setTitle(final String title)
	{
		this.title = title;
	}

	@JsonProperty(value = "IMAGE")
	public String getImage()
	{
		return image;
	}

	public void setImage(final String image)
	{
		this.image = image;
	}

	@JsonProperty(value = "DESCRIPTION")
	public String getDescription()
	{
		return description;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	@JsonProperty(value = "UNSPSC")
	public String getUnspsc()
	{
		return unspsc;
	}

	public void setUnspsc(final String unspsc)
	{
		this.unspsc = unspsc;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final PartData partData = (PartData) o;

		if (getId() != null ? !getId().equals(partData.getId()) : partData.getId() != null)
		{
			return false;
		}
		if (getPartNumber() != null ? !getPartNumber().equals(partData.getPartNumber()) : partData.getPartNumber() != null)
		{
			return false;
		}
		if (getTitle() != null ? !getTitle().equals(partData.getTitle()) : partData.getTitle() != null)
		{
			return false;
		}
		if (getImage() != null ? !getImage().equals(partData.getImage()) : partData.getImage() != null)
		{
			return false;
		}
		if (getDescription() != null ? !getDescription().equals(partData.getDescription()) : partData.getDescription() != null)
		{
			return false;
		}
		return getUnspsc() != null ? getUnspsc().equals(partData.getUnspsc()) : partData.getUnspsc() == null;
	}

	@Override
	public int hashCode()
	{
		int result = getId() != null ? getId().hashCode() : 0;
		result = 31 * result + (getPartNumber() != null ? getPartNumber().hashCode() : 0);
		result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
		result = 31 * result + (getImage() != null ? getImage().hashCode() : 0);
		result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
		result = 31 * result + (getUnspsc() != null ? getUnspsc().hashCode() : 0);
		return result;
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder("PartData{");
		sb.append("id='").append(id).append('\'');
		sb.append(", partNumber='").append(partNumber).append('\'');
		sb.append(", title='").append(title).append('\'');
		sb.append(", image='").append(image).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", unspsc='").append(unspsc).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
