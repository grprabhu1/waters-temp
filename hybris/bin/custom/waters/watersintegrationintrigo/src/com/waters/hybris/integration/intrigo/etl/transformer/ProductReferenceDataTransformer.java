package com.waters.hybris.integration.intrigo.etl.transformer;

import com.waters.hybris.core.enums.SalesStatus;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.util.StreamUtil;
import com.waters.hybris.integration.core.etl.transformer.WatersDataTransformerSkipRecord;
import com.waters.hybris.integration.intrigo.schema.RelatedPartData;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.lang.BooleanUtils;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductReferenceDataTransformer extends WatersDataTransformerSkipRecord<WatersProductModel, Collection<RelatedPartData>>
{
	@Override
	protected Collection<RelatedPartData> internalTransform(final WatersProductModel source)
	{
		final Function<String, RelatedPartData> createData = target -> new RelatedPartData(source.getCode(), target);

		return StreamUtil.nullSafe(source.getProductReferences())
				.filter(r -> BooleanUtils.isTrue(r.getActive()))
				.map(ProductReferenceModel::getTarget)
				.filter(Objects::nonNull)
				.filter(WatersProductModel.class::isInstance)
				.map(WatersProductModel.class::cast)
				.filter(p -> !p.isTerminated())
				.filter(p -> SalesStatus.ACTIVE == p.getSalesStatus())
				.map(ProductModel::getCode)
				.map(createData)
				.collect(Collectors.toSet());
	}


}
