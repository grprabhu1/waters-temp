package com.waters.hybris.integration.intrigo.util;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.classification.features.FeatureValue;
import org.apache.commons.lang3.StringUtils;

import java.util.Locale;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

public final class IntrigoHelper
{
	public static final BiFunction<String, ClassAttributeAssignmentModel, String> BUILD_FACET_NAVIGATION_ID =
		(c, caa) -> String.format("%s-%s.%s", c, caa.getClassificationClass().getCode(), caa.getClassificationAttribute().getCode());

	public static final BiFunction<String, String, String> BUILD_FACET_VALUE_NAVIGATION_ID =
		(fnid, fv) -> StringUtils.stripAccents(String.format("%s:%s", fnid, fv).replace(' ', '_'));

	public static final Function<Object, String> EXTRACT_ATTRIBUTE_VALUE = p -> Optional.of(p)
		.filter(ClassificationAttributeValueModel.class::isInstance)
		.map(ClassificationAttributeValueModel.class::cast)
		.map(v -> v.getName(Locale.ENGLISH))
		.orElse(p.toString());

	public static String extractFeatureValue(final FeatureValue featureValue)
	{
		final String value = EXTRACT_ATTRIBUTE_VALUE.apply(featureValue.getValue());
		if (featureValue.getUnit() != null)
		{
			return String.format("%s %s", value, featureValue.getUnit().getSymbol());
		}
		return value;
	}
}
