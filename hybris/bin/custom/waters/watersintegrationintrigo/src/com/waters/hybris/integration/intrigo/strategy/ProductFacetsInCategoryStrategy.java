package com.waters.hybris.integration.intrigo.strategy;

import com.waters.hybris.integration.intrigo.schema.CategoryData;
import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.stream.Stream;

public interface ProductFacetsInCategoryStrategy
{
	Stream<CategoryData> calculateFacets(final CategoryData categoryData, final CatalogVersionModel catalogVersionModel);
}
