/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.integration.intrigo.constants;

/**
 * Global class for all Watersintegrationintrigo constants. You can add global constants for your extension into this class.
 */
public final class WatersintegrationintrigoConstants extends GeneratedWatersintegrationintrigoConstants
{
	public static final String EXTENSIONNAME = "watersintegrationintrigo";

	private WatersintegrationintrigoConstants()
	{
	}

	public static final String DEFAULT_IMAGE_PROPERTY = "watersintegrationintrigo.default.image";
	public static final String UNSPSC_QUALIFIER_NAME = "WatersClassification/1.0/Products.unspsc";
	public static final String INTRIGO_IMAGE_RENDITION = "watersintegrationintrigo.image.rendition";
	public static final String DEFAULT_INTRIGO_IMAGE_THUMBNAIL = "thumbnail";
}
