package com.waters.hybris.integration.intrigo.etl.transformer;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.integration.core.etl.transformer.WatersDataTransformerSkipRecord;
import com.waters.hybris.integration.intrigo.schema.FeatureData;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.waters.hybris.integration.intrigo.util.IntrigoHelper.extractFeatureValue;

public class ProductFeatureTransformer extends WatersDataTransformerSkipRecord<WatersProductModel, Collection<FeatureData>>
{

	private static final Locale localeEn = Locale.ENGLISH;
	private static final String HAZARDOUS_FIELD_NAME = "Hazardous";

	private static final Predicate<Feature> IS_INTERNAL = p -> Optional.ofNullable(p)
		.map(Feature::getClassAttributeAssignment)
		.map(ClassAttributeAssignmentModel::isInternalOnly)
		.orElse(Boolean.FALSE).booleanValue();

	private ClassificationService classificationService;

	@Override
	public Collection<FeatureData> internalTransform(final WatersProductModel productModel)
	{
		final FeatureList features = getClassificationService().getFeatures(productModel);
		final Collection<FeatureData> result = features.getFeatures().stream()
			.filter(IS_INTERNAL.negate())
			.flatMap(f -> this.buildFeature(f, productModel)).collect(Collectors.toSet());

		result.add(createHazardousFeature(productModel));

		return result;
	}

	protected FeatureData createHazardousFeature(final WatersProductModel productModel)
	{
		return buildFeatureData(productModel.getCode(), HAZARDOUS_FIELD_NAME, productModel.isHazardous() ? "Y" : "N");
	}

	protected Stream<FeatureData> buildFeature(final Feature feature, final WatersProductModel product)
	{
		final Optional<String> title = Optional.ofNullable(feature).map(Feature::getClassAttributeAssignment).map(ClassAttributeAssignmentModel::getClassificationAttribute)
			.map(c -> c.getName(localeEn));

		if (title.isPresent() && CollectionUtils.isNotEmpty(feature.getValues()))
		{
			return feature.getValues().stream()
				.map(fv -> buildFeatureData(product.getCode(), title.get(), extractFeatureValue(fv)));
		}

		return Stream.empty();
	}

	protected FeatureData buildFeatureData(final String partNumber, final String featureName, final String featureValue)
	{
		return new FeatureData(partNumber, featureName, featureValue);
	}

	protected ClassificationService getClassificationService()
	{
		return classificationService;
	}

	@Required
	public void setClassificationService(final ClassificationService classificationService)
	{
		this.classificationService = classificationService;
	}
}
