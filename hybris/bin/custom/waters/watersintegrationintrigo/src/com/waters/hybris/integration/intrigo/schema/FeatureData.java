package com.waters.hybris.integration.intrigo.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

//PART_NUMBER,FEATURE_TITLE,VALUE,
@JsonPropertyOrder({
		"PART_NUMBER",
		"FEATURE_TITLE",
		"VALUE"
})
public class FeatureData
{
	private String partNumber;
	private String featureTitle;
	private String value;

	public FeatureData()
	{
		// empty
	}

	public FeatureData(final String partNumber, final String featureTitle, final String value)
	{
		this.partNumber = partNumber;
		this.featureTitle = featureTitle;
		this.value = value;
	}

	@JsonProperty(value = "PART_NUMBER")
	public String getPartNumber()
	{
		return partNumber;
	}

	public void setPartNumber(final String partNumber)
	{
		this.partNumber = partNumber;
	}

	@JsonProperty(value = "FEATURE_TITLE")
	public String getFeatureTitle()
	{
		return featureTitle;
	}

	public void setFeatureTitle(final String featureTitle)
	{
		this.featureTitle = featureTitle;
	}

	@JsonProperty(value = "VALUE")
	public String getValue()
	{
		return value;
	}

	public void setValue(final String value)
	{
		this.value = value;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final FeatureData that = (FeatureData) o;

		if (getPartNumber() != null ? !getPartNumber().equals(that.getPartNumber()) : that.getPartNumber() != null)
		{
			return false;
		}
		if (getFeatureTitle() != null ? !getFeatureTitle().equals(that.getFeatureTitle()) : that.getFeatureTitle() != null)
		{
			return false;
		}
		return getValue() != null ? getValue().equals(that.getValue()) : that.getValue() == null;
	}

	@Override
	public int hashCode()
	{
		int result = getPartNumber() != null ? getPartNumber().hashCode() : 0;
		result = 31 * result + (getFeatureTitle() != null ? getFeatureTitle().hashCode() : 0);
		result = 31 * result + (getValue() != null ? getValue().hashCode() : 0);
		return result;
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder("FeatureData{");
		sb.append("partNumber='").append(partNumber).append('\'');
		sb.append(", featureTitle='").append(featureTitle).append('\'');
		sb.append(", value='").append(value).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
