package com.waters.hybris.integration.intrigo.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "PARENT_ID",
        "NAVIGATION_ID",
        "IMAGE_URL",
        "IMAGE_ALT_TEXT",
        "SORT_ORDER",
        "NAVIGATION_TYPE",
        "TITLE",
        "DESCRIPTION",
        "NAVLEVEL"
})
public class CategoryData
{
    private String parentId;
    private String navigationId;
    private String imageUrl;
    private String imageAltText;
    private String sortOrder;
    private String navigationType;
    private String title;
    private String description;
    private String navLevel;

    @JsonProperty(value = "PARENT_ID")
    public String getParentId() {
        return parentId;
    }

    public void setParentId(final String parentId) {
        this.parentId = parentId;
    }

    @JsonProperty(value = "NAVIGATION_ID")
    public String getNavigationId() {
        return navigationId;
    }

    public void setNavigationId(final String navigationId) {
        this.navigationId = navigationId;
    }

    @JsonProperty(value = "IMAGE_URL")
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @JsonProperty(value = "IMAGE_ALT_TEXT")
    public String getImageAltText() {
        return imageAltText;
    }

    public void setImageAltText(final String imageAltText) {
        this.imageAltText = imageAltText;
    }

    @JsonProperty(value = "SORT_ORDER")
    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(final String sortOrder) {
        this.sortOrder = sortOrder;
    }

    @JsonProperty(value = "NAVIGATION_TYPE")
    public String getNavigationType() {
        return navigationType;
    }

    public void setNavigationType(final String navigationType) {
        this.navigationType = navigationType;
    }

    @JsonProperty(value = "TITLE")
    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    @JsonProperty(value = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @JsonProperty(value = "NAVLEVEL")
    public String getNavLevel() {
        return navLevel;
    }

    public void setNavLevel(final String navLevel) {
        this.navLevel = navLevel;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final CategoryData that = (CategoryData) o;

        if (getParentId() != null ? !getParentId().equals(that.getParentId()) : that.getParentId() != null)
            return false;
        if (getNavigationId() != null ? !getNavigationId().equals(that.getNavigationId()) : that.getNavigationId() != null)
            return false;
        if (getImageUrl() != null ? !getImageUrl().equals(that.getImageUrl()) : that.getImageUrl() != null)
            return false;
        if (getImageAltText() != null ? !getImageAltText().equals(that.getImageAltText()) : that.getImageAltText() != null)
            return false;
        if (getSortOrder() != null ? !getSortOrder().equals(that.getSortOrder()) : that.getSortOrder() != null)
            return false;
        if (getNavigationType() != null ? !getNavigationType().equals(that.getNavigationType()) : that.getNavigationType() != null)
            return false;
        if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) return false;
        if (getDescription() != null ? !getDescription().equals(that.getDescription()) : that.getDescription() != null)
            return false;
        return getNavLevel() != null ? getNavLevel().equals(that.getNavLevel()) : that.getNavLevel() == null;
    }

    @Override
    public int hashCode() {
        int result = getParentId() != null ? getParentId().hashCode() : 0;
        result = 31 * result + (getNavigationId() != null ? getNavigationId().hashCode() : 0);
        result = 31 * result + (getImageUrl() != null ? getImageUrl().hashCode() : 0);
        result = 31 * result + (getImageAltText() != null ? getImageAltText().hashCode() : 0);
        result = 31 * result + (getSortOrder() != null ? getSortOrder().hashCode() : 0);
        result = 31 * result + (getNavigationType() != null ? getNavigationType().hashCode() : 0);
        result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getNavLevel() != null ? getNavLevel().hashCode() : 0);
        return result;
    }

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder("CategoryData{");
		sb.append("parentId='").append(parentId).append('\'');
		sb.append(", navigationId='").append(navigationId).append('\'');
		sb.append(", imageUrl='").append(imageUrl).append('\'');
		sb.append(", imageAltText='").append(imageAltText).append('\'');
		sb.append(", sortOrder='").append(sortOrder).append('\'');
		sb.append(", navigationType='").append(navigationType).append('\'');
		sb.append(", title='").append(title).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", navLevel='").append(navLevel).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
