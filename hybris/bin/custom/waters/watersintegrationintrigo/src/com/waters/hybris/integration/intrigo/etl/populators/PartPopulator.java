package com.waters.hybris.integration.intrigo.etl.populators;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.strategies.ProductImageResolutionStrategy;
import com.waters.hybris.core.util.StreamUtil;
import com.waters.hybris.integration.intrigo.schema.PartData;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;

import java.util.Locale;
import java.util.Optional;

import static com.waters.hybris.integration.intrigo.constants.WatersintegrationintrigoConstants.DEFAULT_IMAGE_PROPERTY;
import static com.waters.hybris.integration.intrigo.constants.WatersintegrationintrigoConstants.DEFAULT_INTRIGO_IMAGE_THUMBNAIL;
import static com.waters.hybris.integration.intrigo.constants.WatersintegrationintrigoConstants.INTRIGO_IMAGE_RENDITION;
import static com.waters.hybris.integration.intrigo.constants.WatersintegrationintrigoConstants.UNSPSC_QUALIFIER_NAME;

public class PartPopulator implements Populator<WatersProductModel, PartData>
{
	private static final Locale localeEn = Locale.ENGLISH;
	private ConfigurationService configurationService;
	private ProductImageResolutionStrategy productImageResolutionStrategy;

	@Override
	public void populate(final WatersProductModel productModel, final PartData partData) throws ConversionException
	{
		partData.setId(productModel.getCode());
		partData.setPartNumber(productModel.getCode());
		partData.setDescription(productModel.getSummary(localeEn));
		partData.setTitle(productModel.getDynamicName());

		final String imageUrl = Optional.ofNullable(productModel.getPrimaryImage())
				.map(e -> getProductImageResolutionStrategy().resolveImageUrl(e, localeEn, getRendition()))
				.orElseGet(() -> getConfigurationService().getConfiguration().getString(DEFAULT_IMAGE_PROPERTY));

		partData.setImage(imageUrl);

		StreamUtil.nullSafe(productModel.getFeatures())
				.filter(f -> UNSPSC_QUALIFIER_NAME.equals(f.getQualifier())).findFirst()
				.map(ProductFeatureModel::getValue)
				.map(Object::toString)
				.ifPresent(partData::setUnspsc);
	}

	private String getRendition()
	{
		return getConfigurationService().getConfiguration().getString(INTRIGO_IMAGE_RENDITION, DEFAULT_INTRIGO_IMAGE_THUMBNAIL);
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public ProductImageResolutionStrategy getProductImageResolutionStrategy()
	{
		return productImageResolutionStrategy;
	}

	@Required
	public void setProductImageResolutionStrategy(final ProductImageResolutionStrategy productImageResolutionStrategy)
	{
		this.productImageResolutionStrategy = productImageResolutionStrategy;
	}
}
