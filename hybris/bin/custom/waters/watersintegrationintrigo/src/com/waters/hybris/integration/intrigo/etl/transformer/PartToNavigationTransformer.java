package com.waters.hybris.integration.intrigo.etl.transformer;

import com.waters.hybris.core.model.model.PunchoutCategoryModel;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.util.StreamUtil;
import com.waters.hybris.integration.core.etl.transformer.WatersDataTransformerSkipRecord;
import com.waters.hybris.integration.intrigo.schema.PartToNavigationData;
import com.waters.hybris.integration.intrigo.util.IntrigoHelper;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PartToNavigationTransformer extends WatersDataTransformerSkipRecord<WatersProductModel, Collection<PartToNavigationData>>
{
	private ClassificationService classificationService;

	@Override
	public Collection<PartToNavigationData> internalTransform(final WatersProductModel product)
	{
		return Stream.concat(getCategoryNavigationStream(product), getFacetNavigationStream(product))
			.collect(Collectors.toSet());
	}

	protected Stream<PartToNavigationData> getCategoryNavigationStream(final WatersProductModel product)
	{
		return StreamUtil.nullSafe(product.getSupercategories())
			.filter(PunchoutCategoryModel.class::isInstance)
			.flatMap(this::getSuperCategories)
			.distinct()
			.filter(this::isNotRootCategory)
			.map(CategoryModel::getCode)
			.map(navigationId -> createPartToNavigation(navigationId, product.getCode()));
	}

	protected Stream<PartToNavigationData> getFacetNavigationStream(final WatersProductModel product)
	{
		return StreamUtil.nullSafe(getClassificationService().getFeatures(product).getFeatures())
			.filter(feature -> feature.getClassAttributeAssignment().isFacet())
			.map(feature -> getFacetNavigationIdsForProduct(feature, product))
			.flatMap(Collection::stream)
			.distinct()
			.map(navigationId -> createPartToNavigation(navigationId, product.getCode()));
	}

	protected Stream<CategoryModel> getSuperCategories(final CategoryModel category)
	{
		return Stream.concat(Stream.of(category), StreamUtil.nullSafe(category.getAllSupercategories()));
	}

	protected boolean isNotRootCategory(final CategoryModel category)
	{
		return CollectionUtils.isNotEmpty(category.getSupercategories());
	}

	protected PartToNavigationData createPartToNavigation(final String navigationId, final String partNumber)
	{
		return new PartToNavigationData(navigationId, partNumber);
	}

	protected Set<String> getFacetNavigationIdsForProduct(final Feature feature, final ProductModel product)
	{
		return StreamUtil.nullSafe(product.getSupercategories())
			.filter(PunchoutCategoryModel.class::isInstance)
			.map(category -> getFacetNavigationIdsForCategory(feature, category))
			.flatMap(Collection::stream)
			.collect(Collectors.toSet());
	}

	protected Set<String> getFacetNavigationIdsForCategory(final Feature feature, final CategoryModel category)
	{
		final String facetNavigationId = IntrigoHelper.BUILD_FACET_NAVIGATION_ID.apply(category.getCode(), feature.getClassAttributeAssignment());

		final Set<String> facetValueNavigationIds = StreamUtil.nullSafe(feature.getValues())
			.filter(fv -> fv != null && fv.getValue() != null)
			.map(fv -> IntrigoHelper.BUILD_FACET_VALUE_NAVIGATION_ID.apply(facetNavigationId, IntrigoHelper.extractFeatureValue(fv)))
			.collect(Collectors.toSet());

		if (CollectionUtils.isNotEmpty(facetValueNavigationIds))
		{
			// include facet only if there is value assigned
			facetValueNavigationIds.add(facetNavigationId);
		}

		return facetValueNavigationIds;
	}

	protected ClassificationService getClassificationService()
	{
		return classificationService;
	}

	@Required
	public void setClassificationService(final ClassificationService classificationService)
	{
		this.classificationService = classificationService;
	}
}
