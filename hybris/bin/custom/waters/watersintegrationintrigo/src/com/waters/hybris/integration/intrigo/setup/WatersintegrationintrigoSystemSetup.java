/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.integration.intrigo.setup;

import com.waters.hybris.integration.intrigo.constants.WatersintegrationintrigoConstants;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;

import java.util.List;


@SystemSetup(extension = WatersintegrationintrigoConstants.EXTENSIONNAME)
public class WatersintegrationintrigoSystemSetup extends AbstractSystemSetup
{

	@Override
	public List<SystemSetupParameter> getInitializationOptions()
	{
		return null;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData(final SystemSetupContext context)
	{
		importImpexFile(context, "/watersintegrationintrigo/import/essential-data.impex");
		importImpexFile(context, "/watersintegrationintrigo/import/cron-jobs.impex");
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.PROJECT)
	public void createProjectData(final SystemSetupContext context)
	{
		importImpexFile(context, "/watersintegrationintrigo/import/cron-jobs-triggers.impex");
	}
}
