package com.waters.hybris.integration.intrigo.strategy.impl;

import com.waters.hybris.core.util.StreamUtil;
import com.waters.hybris.integration.intrigo.schema.CategoryData;
import com.waters.hybris.integration.intrigo.strategy.ProductFacetsInCategoryStrategy;
import com.waters.hybris.integration.intrigo.util.IntrigoHelper;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.waters.hybris.integration.intrigo.util.IntrigoHelper.extractFeatureValue;

public class WatersProductFacetsInCategoryStrategy implements ProductFacetsInCategoryStrategy
{
	private static final Predicate<Feature> IS_EXTRACTABLE = p -> Optional.of(p)
		.map(Feature::getClassAttributeAssignment)
		.filter(ClassAttributeAssignmentModel::isFacet)
		.filter(f -> f.getClassificationClass() != null && f.getClassificationAttribute() != null)
		.isPresent();

	private static final Function<Optional<ClassAttributeAssignmentModel>, String> CODE_BUILDER = classAsignment -> classAsignment
		.map(f -> String.format("%s.%s", f.getClassificationClass().getCode(), f.getClassificationAttribute().getCode()))
		.orElse(StringUtils.EMPTY);

	private static final Function<Feature, String> EXTRACT_FEATURE_CODE =
		feature -> CODE_BUILDER.apply(Optional.of(feature)
			.map(Feature::getClassAttributeAssignment));

	private static final String NAVIGATION_TYPE = "S";
	private static final String DEFAULT_ORDER = "0";
	private static final String ONE_ZERO = "0";

	private CategoryService categoryService;
	private ClassificationService classificationService;

	@Override
	public Stream<CategoryData> calculateFacets(final CategoryData categoryData, final CatalogVersionModel catalogVersionModel)
	{
		final CategoryModel category = getCategoryService().getCategoryForCode(catalogVersionModel, categoryData.getNavigationId());

		final Map<String, Set<Feature>> features = StreamUtil.nullSafe(category.getProducts())
			.flatMap(p -> getClassificationService().getFeatures(p).getFeatures().stream())
			.filter(IS_EXTRACTABLE)
			.collect(Collectors.groupingBy(EXTRACT_FEATURE_CODE, Collectors.toCollection(LinkedHashSet::new)));

		final Set<CategoryData> featuresCategoryData = features.entrySet().stream()
			.filter(e -> StringUtils.isNotBlank(e.getKey()))
			.flatMap(e -> createPseudoCategoriesForFeatures(categoryData, e.getValue()))
			.collect(Collectors.toCollection(LinkedHashSet::new));

		return featuresCategoryData.stream();
	}

	private Stream<CategoryData> createPseudoCategoriesForFeatures(final CategoryData parentCategory, final Set<Feature> features)
	{
		final Optional<ClassAttributeAssignmentModel> classAttributeAssignmentModel = features.stream()
			.map(Feature::getClassAttributeAssignment)
			.findFirst();

		if (classAttributeAssignmentModel.isPresent())
		{
			final CategoryData categoryData = new CategoryData();
			categoryData.setParentId(parentCategory.getNavigationId());
			final String facetNavigationId = IntrigoHelper.BUILD_FACET_NAVIGATION_ID.apply(parentCategory.getNavigationId(), classAttributeAssignmentModel.get());
			categoryData.setNavigationId(facetNavigationId);

			categoryData.setSortOrder(classAttributeAssignmentModel
				.map(ClassAttributeAssignmentModel::getPosition)
				.map(Object::toString)
				.orElse(DEFAULT_ORDER));

			categoryData.setNavigationType(NAVIGATION_TYPE);

			classAttributeAssignmentModel
				.map(ClassAttributeAssignmentModel::getClassificationAttribute)
				.map(c -> c.getName(Locale.ENGLISH)).ifPresent(categoryData::setTitle);

			categoryData.setNavLevel(parentCategory.getNavLevel() + ONE_ZERO);

			final Set<CategoryData> values = createPseudoCategoriesForFeatureValues(categoryData, features);
			if (!values.isEmpty())
			{
				return Stream.concat(Stream.of(categoryData), values.stream());
			}
		}

		return Stream.empty();
	}

	private Set<CategoryData> createPseudoCategoriesForFeatureValues(final CategoryData parentCategory, final Set<Feature> features)
	{
		final Set<String> processedValues = new HashSet<>();

		return features.stream()
			.filter(f -> CollectionUtils.isNotEmpty(f.getValues()))
			.flatMap(f -> createFeatureValuesData(parentCategory, f))
			.filter(f -> !processedValues.contains(f.getTitle()))
			.peek(f -> processedValues.add(f.getTitle()))
			.map(f -> createPseudoCategoryForFeatureValue(parentCategory, f))
			.collect(Collectors.toCollection(LinkedHashSet::new));
	}

	private Stream<FeatureValueData> createFeatureValuesData(final CategoryData parentCategory, final Feature feature)
	{
		return StreamUtil.nullSafe(feature.getValues())
			.filter(featureValue -> featureValue != null && featureValue.getValue() != null)
			.map(featureValue -> createFeatureValueData(parentCategory, feature, featureValue));
	}

	private FeatureValueData createFeatureValueData(final CategoryData parentCategory, final Feature feature, final FeatureValue featureValue)
	{
		final String position = Optional.ofNullable(feature.getClassAttributeAssignment())
			.map(ClassAttributeAssignmentModel::getPosition)
			.map(Object::toString)
			.orElse(DEFAULT_ORDER);

		final String value = extractFeatureValue(featureValue);
		final String facetValueNavigationId = IntrigoHelper.BUILD_FACET_VALUE_NAVIGATION_ID.apply(parentCategory.getNavigationId(), value);

		return new FeatureValueData(facetValueNavigationId, value, position);
	}

	private CategoryData createPseudoCategoryForFeatureValue(final CategoryData parentCategory, final FeatureValueData featureValueData)
	{
		final CategoryData categoryData = new CategoryData();
		categoryData.setParentId(parentCategory.getNavigationId());
		categoryData.setNavigationId(featureValueData.getId());
		categoryData.setSortOrder(featureValueData.getPosition());
		categoryData.setNavigationType(NAVIGATION_TYPE);
		categoryData.setTitle(featureValueData.getTitle());
		categoryData.setNavLevel(parentCategory.getNavLevel() + ONE_ZERO);

		return categoryData;
	}

	public CategoryService getCategoryService()
	{
		return categoryService;
	}

	@Required
	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	public ClassificationService getClassificationService()
	{
		return classificationService;
	}

	@Required
	public void setClassificationService(final ClassificationService classificationService)
	{
		this.classificationService = classificationService;
	}

	private static class FeatureValueData
	{
		private final String id;
		private final String title;
		private final String position;

		public FeatureValueData(final String id, final String title, final String position)
		{
			this.id = id;
			this.title = title;
			this.position = position;
		}

		public String getId()
		{
			return id;
		}

		public String getTitle()
		{
			return title;
		}

		public String getPosition()
		{
			return position;
		}
	}
}
