@IntrigoFeeds @Transaction
Feature: Intrigo Feeds

	@AdminUser
	Scenario: Intrigo Parts
		Given there are published and active products in PIM
		When the "intrigoPartsFeedJob" has been run
		And the job completed successfully
		Then outbound file record for "parts" is created
		When the "intrigoOutboundProductFeedGenerator" has been run
		And the job completed successfully
		Then Feed file for parts generated with expected data


	@AdminUser
	Scenario: Intrigo Related Parts
		Given there are published and active products in PIM
		When the "intrigoRelatedPartsFeedJob" has been run
		And the job completed successfully
		Then outbound file record for "related_parts" is created
		When the "intrigoOutboundProductFeedGenerator" has been run
		And the job completed successfully
		Then Feed file for related parts generated with expected data


	@AdminUser
	Scenario: Intrigo Parts To Navigation
		Given there are published and active products in PIM
		When the "intrigoPartsToNavigationFeedJob" has been run
		And the job completed successfully
		Then outbound file record for "parts_to_navigation" is created
		When the "intrigoOutboundProductFeedGenerator" has been run
		And the job completed successfully
		Then Feed file for parts to navigation generated with expected data
		And Feed file for part to navigation generated with the following data
			| navigationId                                                         | partNumber |
			| Intrigo_Sub2umUPLCColumns-Products.Brand                             | 176000863  |
			| Intrigo_Sub2umUPLCColumns                                            | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnUSPClassification:L1          | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnLength:50_mm                  | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnParticleSubstrate:Hybrid      | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnParticleTechnology            | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnFormat:Column                 | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnSystem:UPLC                   | 176000863  |
			| Intrigo_Columns                                                      | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnSeparationMode                | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnChemistryType:C18             | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnSeparationMode:Reversed-Phase | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnParticleTechnology:BEH        | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnPoreSize                      | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Products.Brand:ACQUITY_UPLC                | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnFormat                        | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnSystem:UHPLC                  | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnSystem                        | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnParticleSize:1.7_µm           | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnParticleSize                  | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnUSPClassification             | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnParticleSubstrate             | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnChemistryType                 | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnPoreSize:130_A                | 176000863  |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnLength                        | 176000863  |
			| Intrigo_AqueousFilters-Filters.FilterSampleVolume                    | 186005596  |
			| Intrigo_AqueousFilters-Filters.FilterFormat                          | 186005596  |
			| Intrigo_AqueousFilters-Filters.FilterHoldupVolume                    | 186005596  |
			| Intrigo_Filters                                                      | 186005596  |
			| Intrigo_AqueousFilters-Products.Brand                                | 186005596  |
			| Intrigo_AqueousFilters-Filters.FilterType                            | 186005596  |
			| Intrigo_AqueousFilters-Filters.FilterFormat:Syringe_Filter           | 186005596  |
			| Intrigo_AqueousFilters-Filters.FilterDiameter:25_mm                  | 186005596  |
			| Intrigo_AqueousFilters-Filters.FilterSampleVolume:<_100_mL           | 186005596  |
			| Intrigo_AqueousFilters-Filters.FilterSeparationMode:Aqueous          | 186005596  |
			| Intrigo_AqueousFilters                                               | 186005596  |
			| Intrigo_AqueousFilters-Filters.FilterDiameter                        | 186005596  |
			| Intrigo_AqueousFilters-Filters.FilterHoldupVolume:<_100_µL           | 186005596  |
			| Intrigo_AqueousFilters-Filters.FilterPoreSize                        | 186005596  |
			| Intrigo_AqueousFilters-Products.Brand:Acrodisc                       | 186005596  |
			| Intrigo_AqueousFilters-Filters.FilterSeparationMode                  | 186005596  |
			| Intrigo_AqueousFilters-Filters.FilterType:GHP                        | 186005596  |
			| Intrigo_AqueousFilters-Filters.FilterPoreSize:0.2_µm                 | 186005596  |

	@AdminUser
	Scenario: Intrigo Features
		Given there are published and active products in PIM
		When the "intrigoFeatureFeedJob" has been run
		And the job completed successfully
		Then outbound file record for "features" is created
		When the "intrigoOutboundProductFeedGenerator" has been run
		And the job completed successfully
		Then Feed file for features generated with expected data


	@AdminUser
	Scenario: Intrigo Category Navigation
		Given there are published and active products in PIM
		When the "intrigoCategoryFeedJob" has been run
		And the job completed successfully
		Then outbound file record for "category_navigation" is created
		When the "intrigoOutboundProductFeedGenerator" has been run
		And the job completed successfully
		Then Feed file for category navigation generated with the following data
			| parentId                                                  | navigationId                                                                    | imageUrl | imageAltText | sortOrder | navigationType | title                               | description      | navLevel |
			| Intrigo_Sub2umUPLCColumns                                 | Intrigo_Sub2umUPLCColumns-Column.ColumnPoreSize                                 |          |              | 360       | S              | Column Pore Size                    |                  | 10       |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnPoreSize           | Intrigo_Sub2umUPLCColumns-Column.ColumnPoreSize:130_A                           |          |              | 360       | S              | 130 Å                               |                  | 100      |
			| Intrigo_Sub2umUPLCColumns                                 | Intrigo_Sub2umUPLCColumns-Column.ColumnSeparationMode                           |          |              | 310       | S              | Column Separation Mode              |                  | 10       |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnSeparationMode     | Intrigo_Sub2umUPLCColumns-Column.ColumnSeparationMode:Reversed-Phase            |          |              | 310       | S              | Reversed-Phase                      |                  | 100      |
			| Intrigo_Sub2umUPLCColumns                                 | Intrigo_Sub2umUPLCColumns-Column.ColumnFormat                                   |          |              | 260       | S              | Column Format                       |                  | 10       |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnFormat             | Intrigo_Sub2umUPLCColumns-Column.ColumnFormat:Column                            |          |              | 260       | S              | Column                              |                  | 100      |
			| Intrigo_Sub2umUPLCColumns                                 | Intrigo_Sub2umUPLCColumns-Products.Brand                                        |          |              | 30        | S              | Brand                               |                  | 10       |
			| Intrigo_Sub2umUPLCColumns-Products.Brand                  | Intrigo_Sub2umUPLCColumns-Products.Brand:ACQUITY_UPLC                           |          |              | 30        | S              | ACQUITY UPLC                        |                  | 100      |
			| Intrigo_Sub2umUPLCColumns                                 | Intrigo_Sub2umUPLCColumns-Column.ColumnBondingTechnology                        |          |              | 220       | S              | Column Bonding Technology           |                  | 10       |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnBondingTechnology  | Intrigo_Sub2umUPLCColumns-Column.ColumnBondingTechnology:Shield_RP18            |          |              | 220       | S              | Shield RP18                         |                  | 100      |
			| Intrigo_Sub2umUPLCColumns                                 | Intrigo_Sub2umUPLCColumns-Column.ColumnUSPClassification                        |          |              | 440       | S              | Column USP Classification           |                  | 10       |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnUSPClassification  | Intrigo_Sub2umUPLCColumns-Column.ColumnUSPClassification:L1                     |          |              | 440       | S              | L1                                  |                  | 100      |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnUSPClassification  | Intrigo_Sub2umUPLCColumns-Column.ColumnUSPClassification:L7                     |          |              | 440       | S              | L7                                  |                  | 100      |
			| Intrigo_Sub2umUPLCColumns                                 | Intrigo_Sub2umUPLCColumns-Column.ColumnLength                                   |          |              | 290       | S              | Column Length                       |                  | 10       |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnLength             | Intrigo_Sub2umUPLCColumns-Column.ColumnLength:100_mm                            |          |              | 290       | S              | 100 mm                              |                  | 100      |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnLength             | Intrigo_Sub2umUPLCColumns-Column.ColumnLength:50_mm                             |          |              | 290       | S              | 50 mm                               |                  | 100      |
			| Intrigo_Sub2umUPLCColumns                                 | Intrigo_Sub2umUPLCColumns-Column.ColumnParticleTechnology                       |          |              | 430       | S              | Column Particle Technology          |                  | 10       |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnParticleTechnology | Intrigo_Sub2umUPLCColumns-Column.ColumnParticleTechnology:BEH                   |          |              | 430       | S              | BEH                                 |                  | 100      |
			| Intrigo_Sub2umUPLCColumns                                 | Intrigo_Sub2umUPLCColumns-Column.ColumnSystem                                   |          |              | 420       | S              | Column System                       |                  | 10       |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnSystem             | Intrigo_Sub2umUPLCColumns-Column.ColumnSystem:UPLC                              |          |              | 420       | S              | UPLC                                |                  | 100      |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnSystem             | Intrigo_Sub2umUPLCColumns-Column.ColumnSystem:UHPLC                             |          |              | 420       | S              | UHPLC                               |                  | 100      |
			| Intrigo_Sub2umUPLCColumns                                 | Intrigo_Sub2umUPLCColumns-Column.ColumnChemistryType                            |          |              | 230       | S              | Column Chemistry Type               |                  | 10       |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnChemistryType      | Intrigo_Sub2umUPLCColumns-Column.ColumnChemistryType:C18                        |          |              | 230       | S              | C18                                 |                  | 100      |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnChemistryType      | Intrigo_Sub2umUPLCColumns-Column.ColumnChemistryType:C8                         |          |              | 230       | S              | C8                                  |                  | 100      |
			| Intrigo_Sub2umUPLCColumns                                 | Intrigo_Sub2umUPLCColumns-Column.ColumnParticleSubstrate                        |          |              | 350       | S              | Column Particle Substrate           |                  | 10       |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnParticleSubstrate  | Intrigo_Sub2umUPLCColumns-Column.ColumnParticleSubstrate:Hybrid                 |          |              | 350       | S              | Hybrid                              |                  | 100      |
			| Intrigo_Sub2umUPLCColumns                                 | Intrigo_Sub2umUPLCColumns-Column.ColumnParticleSize                             |          |              | 340       | S              | Column Particle Size                |                  | 10       |
			| Intrigo_Sub2umUPLCColumns-Column.ColumnParticleSize       | Intrigo_Sub2umUPLCColumns-Column.ColumnParticleSize:1.7_µm                      |          |              | 340       | S              | 1.7 µm                              |                  | 100      |
			| Intrigo_AqueousFilters                                    | Intrigo_AqueousFilters-Filters.FilterSeparationMode                             |          |              | 1570      | S              | Filter Separation Mode              |                  | 10       |
			| Intrigo_AqueousFilters-Filters.FilterSeparationMode       | Intrigo_AqueousFilters-Filters.FilterSeparationMode:Aqueous                     |          |              | 1570      | S              | Aqueous                             |                  | 100      |
			| Intrigo_AqueousFilters                                    | Intrigo_AqueousFilters-Filters.FilterType                                       |          |              | 1580      | S              | Filter Type                         |                  | 10       |
			| Intrigo_AqueousFilters-Filters.FilterType                 | Intrigo_AqueousFilters-Filters.FilterType:GHP                                   |          |              | 1580      | S              | GHP                                 |                  | 100      |
			| Intrigo_AqueousFilters                                    | Intrigo_AqueousFilters-Filters.FilterDiameter                                   |          |              | 1510      | S              | Filter Diameter                     |                  | 10       |
			| Intrigo_AqueousFilters-Filters.FilterDiameter             | Intrigo_AqueousFilters-Filters.FilterDiameter:13_mm                             |          |              | 1510      | S              | 13 mm                               |                  | 100      |
			| Intrigo_AqueousFilters-Filters.FilterDiameter             | Intrigo_AqueousFilters-Filters.FilterDiameter:25_mm                             |          |              | 1510      | S              | 25 mm                               |                  | 100      |
			| Intrigo_AqueousFilters                                    | Intrigo_AqueousFilters-Filters.FilterPoreSize                                   |          |              | 1550      | S              | Filter Pore Size                    |                  | 10       |
			| Intrigo_AqueousFilters-Filters.FilterPoreSize             | Intrigo_AqueousFilters-Filters.FilterPoreSize:0.2_µm                            |          |              | 1550      | S              | 0.2 µm                              |                  | 100      |
			| Intrigo_AqueousFilters-Filters.FilterPoreSize             | Intrigo_AqueousFilters-Filters.FilterPoreSize:0.45_µm                           |          |              | 1550      | S              | 0.45 µm                             |                  | 100      |
			| Intrigo_AqueousFilters                                    | Intrigo_AqueousFilters-Filters.FilterSampleVolume                               |          |              | 1560      | S              | Filter Sample Volume                |                  | 10       |
			| Intrigo_AqueousFilters-Filters.FilterSampleVolume         | Intrigo_AqueousFilters-Filters.FilterSampleVolume:<_10_mL                       |          |              | 1560      | S              | < 10 mL                             |                  | 100      |
			| Intrigo_AqueousFilters-Filters.FilterSampleVolume         | Intrigo_AqueousFilters-Filters.FilterSampleVolume:<_100_mL                      |          |              | 1560      | S              | < 100 mL                            |                  | 100      |
			| Intrigo_AqueousFilters                                    | Intrigo_AqueousFilters-Filters.FilterHoldupVolume                               |          |              | 1530      | S              | Filter Hold-up Volume               |                  | 10       |
			| Intrigo_AqueousFilters-Filters.FilterHoldupVolume         | Intrigo_AqueousFilters-Filters.FilterHoldupVolume:<_14_µL                       |          |              | 1530      | S              | < 14 µL                             |                  | 100      |
			| Intrigo_AqueousFilters-Filters.FilterHoldupVolume         | Intrigo_AqueousFilters-Filters.FilterHoldupVolume:<_100_µL                      |          |              | 1530      | S              | < 100 µL                            |                  | 100      |
			| Intrigo_AqueousFilters                                    | Intrigo_AqueousFilters-Filters.FilterFormat                                     |          |              | 1520      | S              | Filter Format                       |                  | 10       |
			| Intrigo_AqueousFilters-Filters.FilterFormat               | Intrigo_AqueousFilters-Filters.FilterFormat:Minispike_Syringe_Filter            |          |              | 1520      | S              | Minispike Syringe Filter            |                  | 100      |
			| Intrigo_AqueousFilters-Filters.FilterFormat               | Intrigo_AqueousFilters-Filters.FilterFormat:Syringe_Filter                      |          |              | 1520      | S              | Syringe Filter                      |                  | 100      |
			| Intrigo_AqueousFilters-Filters.FilterFormat               | Intrigo_AqueousFilters-Filters.FilterFormat:Syringe_Filter_with_Glass_PreFilter |          |              | 1520      | S              | Syringe Filter with Glass PreFilter |                  | 100      |
			| Intrigo_AqueousFilters                                    | Intrigo_AqueousFilters-Products.Brand                                           |          |              | 30        | S              | Brand                               |                  | 10       |
			| Intrigo_AqueousFilters-Products.Brand                     | Intrigo_AqueousFilters-Products.Brand:Acrodisc                                  |          |              | 30        | S              | Acrodisc                            |                  | 100      |
			| 0                                                         | Intrigo_Columns                                                                 |          |              | 0         | P              | Columns                             | Columns          | 0        |
			| 0                                                         | Intrigo_ApplicationKits                                                         |          |              | 23        | P              | Application Kits                    | Application Kits | 0        |
