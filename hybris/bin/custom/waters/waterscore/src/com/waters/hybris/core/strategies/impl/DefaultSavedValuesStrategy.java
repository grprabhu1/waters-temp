package com.waters.hybris.core.strategies.impl;

import com.waters.hybris.core.strategies.SavedValuesStrategy;
import de.hybris.platform.core.enums.SavedValueEntryType;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.hmc.model.SavedValuesModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.servicelayer.type.TypeService;
import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Optional;

public class DefaultSavedValuesStrategy implements SavedValuesStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultSavedValuesStrategy.class);
	private ModelService modelService;
	private TypeService typeService;
	private TimeService timeService;

	@Override
	public Optional<SavedValuesModel> create(@NotNull final ItemModel modifiedItemModel,
	                                         final UserModel userModel, final Object newValue, final Object oldValue, final Boolean force)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Creating SavedValuesModelEntry for the attribute change. New value: " + newValue + ", old value: " + oldValue);
		}
		if (userModel == null)
		{
			LOG.error("userModel is null, audit data not created for model with pk " + modifiedItemModel.getPk());
			return Optional.empty();
		}
		if (Objects.equals(newValue, oldValue) && BooleanUtils.isNotTrue(force))
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("SavedValuesModelEntry will not be created as the attributes are the same. Value: " + newValue);
			}
			return Optional.empty();
		}
		final SavedValuesModel savedValuesModel;
		savedValuesModel = getModelService().create(SavedValuesModel.class);
		savedValuesModel.setTimestamp(getTimeService().getCurrentTime());
		savedValuesModel.setModificationType(getModelService().isNew(modifiedItemModel) ? SavedValueEntryType.CREATED : SavedValueEntryType.CHANGED);
		savedValuesModel.setModifiedItemType(getTypeService().getComposedTypeForClass(modifiedItemModel.getClass()));
		savedValuesModel.setModifiedItem(modifiedItemModel);
		savedValuesModel.setModifiedItemDisplayString(modifiedItemModel.getPk() != null ?
			modifiedItemModel.getItemtype() + " (" + modifiedItemModel.getPk() + ")" : modifiedItemModel.getItemtype() + " created");
		savedValuesModel.setUser(userModel);
		return Optional.of(savedValuesModel);
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public TypeService getTypeService()
	{
		return typeService;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	public TimeService getTimeService()
	{
		return timeService;
	}

	@Required
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}
}
