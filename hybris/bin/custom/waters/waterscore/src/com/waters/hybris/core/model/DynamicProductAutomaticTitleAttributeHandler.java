package com.waters.hybris.core.model;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.services.ProductNamingRuleCalculationService;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

public class DynamicProductAutomaticTitleAttributeHandler extends AbstractDynamicAttributeHandler<String, WatersProductModel>
{
    private static final Logger LOG = LoggerFactory.getLogger(DynamicProductAutomaticTitleAttributeHandler.class);

    private ProductNamingRuleCalculationService productNamingRuleCalculationService;

    @Override
    public String get(final WatersProductModel model) {
        try {
            return getProductNamingRuleCalculationService().calculateAutomaticName(model);
        } catch (final Exception e) {
            logError(model, e, "Error in resolving naming rule for product ");
            return StringUtils.EMPTY;
        }
    }

    public ProductNamingRuleCalculationService getProductNamingRuleCalculationService() {
        return productNamingRuleCalculationService;
    }

    @Required
    public void setProductNamingRuleCalculationService(final ProductNamingRuleCalculationService productNamingRuleCalculationService) {
        this.productNamingRuleCalculationService = productNamingRuleCalculationService;
    }

    protected void logError(final WatersProductModel model, final Exception e, final String errorMessage) {
        LOG.error(errorMessage + model.getCode(), e);
    }
}
