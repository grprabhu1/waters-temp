package com.waters.hybris.core.util;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamUtil {

	@SafeVarargs
	public static <F, T> Optional<T> first(final F obj, final Function<T, Boolean> check, final Function<F, T>... functions) {
		return Stream.of(functions)
			.map(f -> f.apply(obj))
			.filter(check::apply)
			.findFirst();
	}
	
	public static <T> Stream<T> nullSafe(final Collection<T> collection) {
		return collection == null ? Stream.empty() : collection.stream();
	}

	public static <S, R> Stream<Map.Entry<S, R>> nullSafe(final Map<S, R> map) {
		return MapUtils.isEmpty(map) ? Stream.empty() : map.entrySet().stream();
	}

	public static <T> Stream<Pair<Integer, T>> zipStream(final Collection<T> collection) {
		final Iterator<T> iterator = collection.iterator();
		return IntStream.range(0, collection.size()).mapToObj(i -> new ImmutablePair<>(i, iterator.next()));
	}

	public static <S, R> Stream<R> nullSafe(final Optional<S> object, final Function<S, Collection<R>> extractor) {
		return object.map(extractor).map(Collection::stream).orElse(Stream.empty());
	}
}
