package com.waters.hybris.core.url;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;


public interface ProductUrlValidator
{
	/**
	 * Check whether the URL for the base product, of the given product, is valid
	 * @param product
	 * @return true if at all of the {@link CategoryModel}s used within it's URL have their name set for the current locale
	 */
	boolean hasValidUrl(final ProductModel product);
}
