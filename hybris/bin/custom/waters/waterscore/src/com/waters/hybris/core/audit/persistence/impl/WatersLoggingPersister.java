package com.waters.hybris.core.audit.persistence.impl;

import com.neoworks.hybris.audit.data.AuditRecordData;
import com.neoworks.hybris.audit.data.AuditRecordEntryData;
import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.audit.persistence.Persister;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class WatersLoggingPersister implements Persister
{
	private ConfigurationService configurationService;
	private static final String DD_MMM_YYYY_HH_MM_SS = "dd-MMM-yyyy HH:mm:ss";
	private static final String AUDIT_ENTRY_MESSAGE_SEPARATOR = "; ";
	private static final Logger LOG = LoggerFactory.getLogger(WatersLoggingPersister.class);

	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DD_MMM_YYYY_HH_MM_SS, Locale.UK)
			.withZone(ZoneId.of("UTC"));


	public Logger getLogger(final SystemArea systemArea)
	{
		if (systemArea != null)
		{
			return LoggerFactory.getLogger(systemArea.name());
		}
		else
		{
			return LOG;
		}
	}

	public void persist(final AuditRecordData auditRecord)
	{
		final Status severityLevel = auditRecord.getStatus();
		final LocalDateTime localDateTime =
				LocalDateTime.ofInstant(Instant.ofEpochMilli(auditRecord.getStartTime()), ZoneId.systemDefault());

		final String preamble = this.createLogPreamble(auditRecord, localDateTime.format(DATE_TIME_FORMATTER));
		final StringBuilder msg = new StringBuilder();
		final List<AuditRecordEntryData> entries = auditRecord.getEntries();

		msg.append(preamble).append(' ').append(auditRecord.getMessage());

		final Logger logger = getLogger(auditRecord.getSystemArea());

		if (!entries.isEmpty())
		{
			if (entries.size() < 150)
			{
				msg.append(AUDIT_ENTRY_MESSAGE_SEPARATOR);
				msg.append(entries.stream()
						.map(AuditRecordEntryData::getMessage)
						.collect(Collectors.joining(AUDIT_ENTRY_MESSAGE_SEPARATOR)));
			}
			else
			{
				for (int i = 0; i < entries.size(); ++i)
				{
					msg.append(AUDIT_ENTRY_MESSAGE_SEPARATOR).append((entries.get(i)).getMessage());
					if (i % 100 == 0)
					{
						if (Status.SUCCESS.equals(severityLevel))
						{
							logger.info(msg.toString());
						}
						else
						{
							logger.error(msg.toString());
						}

						msg.setLength(0);
					}
				}
			}
		}

		if (StringUtils.isNotBlank(auditRecord.getReturnedValue()))
		{
			msg.append(" Result [").append(auditRecord.getReturnedValue()).append(']');
		}

		if (StringUtils.isNotBlank(auditRecord.getExceptionMessage()))
		{
			msg.append(" Error [").append(auditRecord.getExceptionMessage()).append(']');
		}

		msg.append(" (").append(auditRecord.getRecordId()).append(')');
		if (Status.SUCCESS.equals(severityLevel))
		{
			logger.info(msg.toString());
		}
		else if (Status.WARN.equals(severityLevel))
		{
			logger.warn(msg.toString());
		}
		else if (auditRecord.getException() != null)
		{
			logger.error(msg.toString(), auditRecord.getException());
		}
		else
		{
			logger.error(msg.toString());
		}

	}

	protected String createLogPreamble(final AuditRecordData auditRecordData, final String date)
	{
		return "[" + auditRecordData.getNodeId() + ':' + auditRecordData.getSystemArea().name() + ':' + date + ':' + auditRecordData.getJaloSessionId() + ']';
	}

	public boolean canPersist(final AuditRecordData auditRecord)
	{
		return this.getConfigurationService().getConfiguration().getBoolean("watersintegrationprices.audit.logging.persist", false);
	}

	protected ConfigurationService getConfigurationService()
	{
		return this.configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
