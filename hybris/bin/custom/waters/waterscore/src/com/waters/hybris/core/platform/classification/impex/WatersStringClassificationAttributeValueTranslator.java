package com.waters.hybris.core.platform.classification.impex;

import de.hybris.platform.catalog.constants.GeneratedCatalogConstants.Enumerations.ClassificationAttributeTypeEnum;
import de.hybris.platform.impex.jalo.translators.SingleValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;

public class WatersStringClassificationAttributeValueTranslator extends SingleValueTranslator
{
	private final String type;

	public WatersStringClassificationAttributeValueTranslator(final String type)
	{
		this.type = type;
	}

	@Override
	protected Object convertToJalo(final String string, final Item item)
	{

		final String[] splits = string.split(":");
		final String value = splits.length > 1 ? splits[0] : string;
		if (ClassificationAttributeTypeEnum.NUMBER.equalsIgnoreCase(type))
		{
			try
			{
				return Integer.valueOf(value);

			}
			catch (final NumberFormatException e)
			{
				throw new JaloInvalidParameterException("Classification attribute value " + string + " cannot be converted to Integer: " + e.getMessage(), 0);
			}
		}
		return value;
	}

	@Override
	protected String convertToString(final Object o)
	{
		return o.toString();
	}
}


