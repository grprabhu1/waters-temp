package com.waters.hybris.core.model;

import com.waters.hybris.core.model.model.WatersProductModel;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

import static com.waters.hybris.core.constants.WatersCoreConstants.HAZARD_MATERIAL_NUMBER_QUALIFIER;

public class DynamicProductHazardousAttributeHandler extends AbstractDynamicAttributeHandler<Boolean, WatersProductModel>
{
	@Override
	public Boolean get(final WatersProductModel productModel)
	{
		return Boolean.valueOf(productModel.getFeatures().stream()
			.filter(f -> HAZARD_MATERIAL_NUMBER_QUALIFIER.equalsIgnoreCase(f.getQualifier()))
			.map(ProductFeatureModel::getValue)
			.filter(Objects::nonNull)
			.map(Object::toString)
			.anyMatch(StringUtils::isNotBlank));
	}
}

