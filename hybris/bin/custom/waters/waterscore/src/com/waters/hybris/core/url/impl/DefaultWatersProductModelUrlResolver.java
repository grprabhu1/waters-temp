package com.waters.hybris.core.url.impl;

import com.waters.hybris.core.url.ProductUrlValidator;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.helper.ProductAndCategoryHelper;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.commerceservices.url.impl.*;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.slf4j.LoggerFactory.getLogger;

public class DefaultWatersProductModelUrlResolver extends DefaultProductModelUrlResolver implements ProductUrlValidator
{
	private static final Logger LOG = getLogger(DefaultWatersProductModelUrlResolver.class);

	@Autowired
	private ProductAndCategoryHelper productAndCategoryHelper;


	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasValidUrl(final ProductModel source)
	{
		final ProductModel baseProduct = productAndCategoryHelper.getBaseProduct(source);
		final List<CategoryModel> categoryPath = getCategoryPath(baseProduct);
		final List<CategoryModel> filteredPath = filterCategoriesForUrl(categoryPath);

		return CollectionUtils.isEmpty(filteredPath)
				// Guards against NPE thrown in buildPathString when category.getName returns null as not set for locale
				|| filteredPath.stream().map(CategoryModel::getName).allMatch(Objects::nonNull);
	}

	@Override
	protected CategoryModel getPrimaryCategoryForProduct(final ProductModel product)
	{
		// TODO: implement as per Waters requirements
		// final CategoryModel primaryCategory = getProductService().getPrimaryCategory(product);
		//return primaryCategory != null ? primaryCategory : super.getPrimaryCategoryForProduct(product);
		return null;
	}

	@Override
	protected String buildPathString(final List<CategoryModel> unfilteredPaths)
	{
		final List<CategoryModel> path = filterCategoriesForUrl(unfilteredPaths);

		LOG.debug("category paths. unfiltered [{}]\r\nfiltered [{}]", unfilteredPaths, path);

		if (path == null || path.isEmpty())
		{
			return "c"; // Default category part of path when missing category
		}

		final StringBuilder result = new StringBuilder();

		for (int i = 0; i < path.size(); i++)
		{
			if (i != 0)
			{
				result.append('/');
			}
			final CategoryModel category = path.get(i);
			if (LOG.isDebugEnabled())
			{
				LOG.debug("processing category [{}] with name [{}]", category, Optional.ofNullable(category).map(CategoryModel::getName).orElse(null));
			}
			result.append(urlSafe(category.getName().toLowerCase()));
		}

		return result.toString();
	}

	@Override
	protected String resolveInternal(final ProductModel source)
	{
		final ProductModel baseProduct = productAndCategoryHelper.getBaseProduct(source);

		final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();

		String url = getPattern();

		if (currentBaseSite != null && url.contains("{baseSite-uid}"))
		{
			url = url.replace("{baseSite-uid}", currentBaseSite.getUid());
		}
		if (url.contains("{category-path}"))
		{
			url = url.replace("{category-path}", buildPathString(getCategoryPath(baseProduct)));
		}
		if (url.contains("{product-name}"))
		{
			//TODO: replace this as per Waters process
			// url = url.replace("{product-name}", urlSafe(getProductService().getProductName(source).toLowerCase()));
			url = url.replace("{product-name}", urlSafe("productName"));
		}
		if (url.contains("{product-code}"))
		{
			url = url.replace("{product-code}", source.getCode());
		}

		return url;
	}


	protected List<CategoryModel> filterCategoriesForUrl(final List<CategoryModel> categories)
	{
		return categories.stream()
				.filter(DefaultWatersProductModelUrlResolver::shouldShowInUrl)
				.collect(Collectors.toList());
	}


	protected static boolean shouldShowInUrl(final CategoryModel categoryModel)
	{
		// TODO: to implement as per Waters requirements
		return false;
		//return (categoryModel instanceof WebsiteCategoryModel) &&
		//		!(HideCategoryOptionsEnum.URL.equals(((WebsiteCategoryModel) categoryModel).getHideFrom()) ||
		//				HideCategoryOptionsEnum.URLANDBREADCRUMB.equals(((WebsiteCategoryModel) categoryModel).getHideFrom()));
	}

}
