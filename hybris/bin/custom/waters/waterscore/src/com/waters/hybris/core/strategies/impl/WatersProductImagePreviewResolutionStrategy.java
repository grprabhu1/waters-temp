package com.waters.hybris.core.strategies.impl;

import com.waters.hybris.core.model.model.ExternalMediaModel;
import com.waters.hybris.core.strategies.ProductImageResolutionStrategy;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Locale;

public class WatersProductImagePreviewResolutionStrategy implements ProductImageResolutionStrategy {
	private static final String EXTERNAL_IMAGE_RENDITION = "product.externalimage.rendition.";
	private static final String EXTERNAL_IMAGE_HOST = "product.externalimage.host";
	private static final String PRODUCT_DEFAULT_IMAGE = "product.externalimage.defaultimage";

	private ProductImageResolutionStrategy productImageResolutionStrategy;

	private ConfigurationService configurationService;

	@Override
	public String resolveImageUrl(final ExternalMediaModel externalMediaModel, final Locale locale, final String rendition) {
		final String imageHost = getImageHost();
		if (externalMediaModel == null || StringUtils.isEmpty(externalMediaModel.getExternalUrl(locale))) {
			return imageHost + getDefaultImage();
		}
		final String externalImageUrl = externalMediaModel.getExternalUrl(locale);
		if (StringUtils.isEmpty(rendition)) {
			return imageHost + externalImageUrl;
		}
		final String renditionUrl = getRenditionUrl(rendition);
		return imageHost + externalImageUrl + renditionUrl;
	}

	@Override
	public ImageData populateImageData(final ExternalMediaModel externalMediaModel, final ImageDataType imageDataType)
	{
		final ImageData imageData = new ImageData();
		imageData.setImageType(imageDataType);
		final String imageUrl = getProductImageResolutionStrategy().resolveImageUrl(externalMediaModel, Locale.ENGLISH);
		imageData.setUrl(imageUrl);
		return imageData;
	}

	public String getDefaultImage() {
		return getConfigurationService().getConfiguration().getString(PRODUCT_DEFAULT_IMAGE, StringUtils.EMPTY);
	}

	public String getRenditionUrl(final String rendition) {
		return getConfigurationService().getConfiguration().getString(EXTERNAL_IMAGE_RENDITION + rendition, StringUtils.EMPTY);
	}


	public String getImageHost() {
		return getConfigurationService().getConfiguration().getString(EXTERNAL_IMAGE_HOST);
	}

	@Override
	public String resolveImageUrl(final ExternalMediaModel externalMediaModel, final Locale locale) {
		return resolveImageUrl(externalMediaModel, locale, null);
	}

	public ProductImageResolutionStrategy getProductImageResolutionStrategy()
	{
		return productImageResolutionStrategy;
	}

	public void setProductImageResolutionStrategy(final ProductImageResolutionStrategy productImageResolutionStrategy)
	{
		this.productImageResolutionStrategy = productImageResolutionStrategy;
	}

	public ConfigurationService getConfigurationService() {
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}
}
