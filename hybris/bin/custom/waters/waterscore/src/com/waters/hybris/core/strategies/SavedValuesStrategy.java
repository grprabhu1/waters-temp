package com.waters.hybris.core.strategies;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.hmc.model.SavedValuesModel;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public interface SavedValuesStrategy
{
	Optional<SavedValuesModel> create(@NotNull final ItemModel model,
	                                  @NotNull final UserModel userModel, final Object newValue, final Object oldValue, final Boolean force);
}
