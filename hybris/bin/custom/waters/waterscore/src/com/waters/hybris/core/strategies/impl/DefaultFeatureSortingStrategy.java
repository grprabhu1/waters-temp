package com.waters.hybris.core.strategies.impl;

import com.waters.hybris.core.strategies.FeatureSortingStrategy;
import com.waters.hybris.core.util.StreamUtil;
import de.hybris.platform.classification.features.Feature;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DefaultFeatureSortingStrategy implements FeatureSortingStrategy
{
	@Override
	public List<Feature> sortByPositionAndName(final List<Feature> features)
	{
		final Comparator<Feature> featureComparatorByPosition = Comparator.comparing(f -> f.getClassAttributeAssignment().getPosition(), Comparator.nullsLast(Comparator.naturalOrder()));
		final Comparator<Feature> featureComparatorByPositionAndName = featureComparatorByPosition.thenComparing(Feature::getName);
		return StreamUtil.nullSafe(features).sorted(featureComparatorByPositionAndName).collect(Collectors.toList());
	}
}
