package com.waters.hybris.core.category.interceptor;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class WatersCategoryValidateInterceptor implements ValidateInterceptor<CategoryModel>
{
	@Override
	public void onValidate(final CategoryModel category, final InterceptorContext ctx) throws InterceptorException
	{
		final Map<String, Set<Locale>> modifiedMap = ctx.getDirtyAttributes(category);

		if (modifiedMap.containsKey(CategoryModel.CATEGORIES))
		{
			validateCategoryHierarchy(category, category.getCategories());
		}

		if (modifiedMap.containsKey(CategoryModel.SUPERCATEGORIES))
		{
			validateCategoryHierarchy(category, category.getSupercategories());
		}
	}

	protected void validateCategoryHierarchy(final CategoryModel category, final Collection<CategoryModel> existingCategories) throws InterceptorException
	{
		final Class<? extends CategoryModel> cls = category.getClass();

		final Set<String> invalidCategories = existingCategories.stream()
			.filter(c -> cls != c.getClass())
			.map(CategoryModel::getCode)
			.collect(Collectors.toSet());

		if (CollectionUtils.isNotEmpty(invalidCategories))
		{
			throw new InterceptorException(invalidCategories + " are not of valid categories for this category " + category.getCode() + ", only " + cls.getSimpleName() + " type categories are valid");
		}
	}
}
