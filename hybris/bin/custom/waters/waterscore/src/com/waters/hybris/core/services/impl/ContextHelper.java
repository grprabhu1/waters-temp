package com.waters.hybris.core.services.impl;

import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;

public class ContextHelper {

    private final Map<String, String> featureValuesMap;
    private final Map<String, String> featureUnitsMap;
            
    public ContextHelper(final Map<String, String> featureValuesMap, final Map<String, String> featureUnitsMap)
    {
        this.featureUnitsMap = featureUnitsMap;
        this.featureValuesMap = featureValuesMap;
    }

    private Optional<String> internalFeature(final String feature)
    {
        return ofNullable(getFeatureValuesMap().get(feature));
    }

    private Optional<String> internalUnit(final String unit) {
        return ofNullable(getFeatureUnitsMap().get(unit));
    }

    public String feature(final String feature)
    {
        return internalFeature(feature)
                .orElse(null);
    }

    public String unit(final String unit)
    {
        return internalUnit(unit)
                .orElse(null);
    }

    protected Map<String, String> getFeatureUnitsMap()
    {
        return featureUnitsMap;
    }

    protected Map<String, String> getFeatureValuesMap()
    {
        return featureValuesMap;
    }
}
