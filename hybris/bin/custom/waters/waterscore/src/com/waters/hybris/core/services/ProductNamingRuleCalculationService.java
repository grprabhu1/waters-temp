package com.waters.hybris.core.services;


import com.waters.hybris.core.model.model.WatersProductModel;

public interface ProductNamingRuleCalculationService {
    String calculateAutomaticName(WatersProductModel model);
}
