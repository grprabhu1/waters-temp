package com.waters.hybris.core.util;

import java.util.Collections;
import java.util.Map;

public class MapUtils {
    public static <S,R> Map<S, R> safeMap(final Map<S,R> map)
    {
        return org.apache.commons.collections.MapUtils.isEmpty(map) ? Collections.emptyMap() : map ;
    }
}
