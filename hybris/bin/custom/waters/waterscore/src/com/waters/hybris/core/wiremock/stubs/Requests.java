package com.waters.hybris.core.wiremock.stubs;


import net.minidev.json.JSONObject;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Requests
{
	private Response response;

    private String id;

    private String wasMatched;

    private Request request;

    private StubMapping stubMapping;

    private JSONObject responseDefinition;

    public Response getResponse()
    {
        return response;
    }

    public void setResponse(final Response response)
    {
        this.response = response;
    }

    public String getId()
    {
        return id;
    }

    public void setId(final String id)
    {
        this.id = id;
    }

    public String getWasMatched()
    {
        return wasMatched;
    }

    public void setWasMatched(final String wasMatched)
    {
        this.wasMatched = wasMatched;
    }

    public Request getRequest()
    {
        return request;
    }

    public void setRequest(final Request request)
    {
        this.request = request;
    }

    public StubMapping getStubMapping()
    {
        return stubMapping;
    }

    public void setStubMapping(final StubMapping stubMapping)
    {
        this.stubMapping = stubMapping;
    }

    public JSONObject getResponseDefinition()
    {
        return responseDefinition;
    }

    public void setResponseDefinition(final JSONObject responseDefinition)
    {
        this.responseDefinition = responseDefinition;
    }

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
	}
}
