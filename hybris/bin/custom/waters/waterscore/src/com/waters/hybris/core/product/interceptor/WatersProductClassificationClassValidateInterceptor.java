package com.waters.hybris.core.product.interceptor;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.util.StreamUtil;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class WatersProductClassificationClassValidateInterceptor implements ValidateInterceptor<WatersProductModel>
{
	private CategoryService categoryService;

	private static final Comparator<Pair<CategoryModel, Collection<CategoryModel>>> CATEGORY_BRANCH_LENGTH = Comparator.comparingInt(pair -> pair.getValue().size());

	@Override
	public void onValidate(final WatersProductModel product, final InterceptorContext ctx) throws InterceptorException
	{
		final Map<String, Set<Locale>> modifiedMap = ctx.getDirtyAttributes(product);

		if (modifiedMap.containsKey(WatersProductModel.SUPERCATEGORIES))
		{
			validateClassificationHierarchy(product);
		}
	}

	protected void validateClassificationHierarchy(final WatersProductModel product) throws InterceptorException
	{
		final List<CategoryModel> classificationClasses = StreamUtil.nullSafe(product.getSupercategories())
			.filter(ClassificationClassModel.class::isInstance)
			.collect(Collectors.toList());

		if (classificationClasses.size() > 1)
		{
			final Optional<Pair<CategoryModel, Collection<CategoryModel>>> longestBranch = classificationClasses.stream()
				.map(this::getCategoryBranch)
				.filter(pair -> CollectionUtils.isNotEmpty(pair.getValue()))
				.max(CATEGORY_BRANCH_LENGTH);

			if (longestBranch.isPresent())
			{
				validateCategories(classificationClasses, longestBranch.get());
			}
		}
	}

	protected Pair<CategoryModel, Collection<CategoryModel>> getCategoryBranch(final CategoryModel categoryModel)
	{
		return new ImmutablePair<>(categoryModel, getCategoryService().getAllSupercategoriesForCategory(categoryModel));
	}

	protected void validateCategories(final List<CategoryModel> classificationClasses, final Pair<CategoryModel, Collection<CategoryModel>> longestBranch) throws InterceptorException
	{
		for (final CategoryModel classificationClass : classificationClasses)
		{
			if (!longestBranch.getKey().equals(classificationClass) && !longestBranch.getValue().contains(classificationClass))
			{
				throw new InterceptorException("The product can NOT be assigned to two different classification class branches: " + classificationClass.getCode() + " and " + longestBranch.getKey().getCode());
			}
		}
	}

	protected CategoryService getCategoryService()
	{
		return categoryService;
	}

	@Required
	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}
}
