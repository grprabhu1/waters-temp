package com.waters.hybris.core.wiremock.stubs;


import net.minidev.json.JSONObject;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class StubRequest
{
	private String method;

	private String urlPath;

	private JSONObject headers;

	public String getMethod()
	{
		return method;
	}

	public void setMethod(final String method)
	{
		this.method = method;
	}

	public String getUrlPath()
	{
		return urlPath;
	}

	public void setUrlPath(final String urlPath)
	{
		this.urlPath = urlPath;
	}

	public JSONObject getHeaders()
	{
		return headers;
	}

	public void setHeaders(final JSONObject headers)
	{
		this.headers = headers;
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
	}
}
