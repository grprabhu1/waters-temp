package com.waters.hybris.core.wiremock.stubs;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class StubMapping
{
	private StubResponse response;

    private String id;

    private StubRequest request;

    private String uuid;

    public StubResponse getResponse()
    {
        return response;
    }

    public void setResponse(final StubResponse response)
    {
        this.response = response;
    }

    public String getId()
    {
        return id;
    }

    public void setId(final String id)
    {
        this.id = id;
    }

    public StubRequest getRequest()
    {
        return request;
    }

    public void setRequest(final StubRequest request)
    {
        this.request = request;
    }

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(final String uuid)
    {
        this.uuid = uuid;
    }

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
	}
}
