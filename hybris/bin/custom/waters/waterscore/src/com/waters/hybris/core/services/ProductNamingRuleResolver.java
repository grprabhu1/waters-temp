package com.waters.hybris.core.services;

import com.waters.hybris.core.model.model.WatersProductModel;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * Abstract class to resolve the product naming rule.
 */
public abstract class ProductNamingRuleResolver {
    private static final Logger LOG = LoggerFactory.getLogger(ProductNamingRuleResolver.class);

    /**
     * Method to be overridden by implementation resolvers to generate the display name.
     * @param watersProductModel
     * @param namingField
     * @return
     */
    public abstract String resolve(WatersProductModel watersProductModel, String namingField);

    public String resolveNamingField(final WatersProductModel watersProductModel, final String namingField) {
        final String[] namingFieldValues = namingField.split(":");
        if (namingFieldValues.length == 2) {
            return resolve(watersProductModel, namingFieldValues[1]);
        }
        return StringUtils.EMPTY;
    }

    /**
     * Resolves the classification attribute value.
     *
     * @param watersProductModel
     * @param namingField
     * @return
     */
    public String resolveAttributeValue(final WatersProductModel watersProductModel, final String namingField) {
        final Optional<ProductFeatureModel> productFeatureOptional = watersProductModel.getFeatures().stream().filter(feature ->
                namingField.equals(feature.getClassificationAttributeAssignment()
                        .getClassificationAttribute().getCode())).findFirst();
        if (!productFeatureOptional.isPresent()) {
            LOG.error("Product feature not found for naming field: " + namingField + " for product: " + watersProductModel.getCode());
            return StringUtils.EMPTY;
        }
        final ProductFeatureModel productFeatureModel = productFeatureOptional.get();
        // If naming field has unit associated but UOM is not selected then excluding the naming field.
        if (productFeatureModel.getClassificationAttributeAssignment().getUnit() != null && StringUtils.isEmpty(productFeatureModel.getClassificationAttributeAssignment().getUnit().getSymbol())) {
            return StringUtils.EMPTY;
        }
        // If classification attribute is an ENUM type then get the enum name otherwise get the feature value
        if (productFeatureModel.getValue() instanceof ClassificationAttributeValueModel) {
            final ClassificationAttributeValueModel classificationAttributeValueModel = (ClassificationAttributeValueModel) productFeatureModel.getValue();
            return classificationAttributeValueModel.getName();
        }
        return productFeatureModel.getValue().toString();
    }
}
