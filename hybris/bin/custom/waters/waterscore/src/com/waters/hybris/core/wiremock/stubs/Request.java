package com.waters.hybris.core.wiremock.stubs;

import com.fasterxml.jackson.databind.JsonNode;
import net.minidev.json.JSONObject;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Request
{
	private JSONObject headers;

    private JsonNode body;

    private JsonNode queryParams;

    private String loggedDateString;

    private String browserProxyRequest;

    private String method;

    private String bodyAsBase64;

    private String clientIp;

    private JsonNode cookies;

    private String absoluteUrl;

    private String url;

    private String loggedDate;

    public JSONObject getHeaders()
    {
        return headers;
    }

    public void setHeaders(final JSONObject headers)
    {
        this.headers = headers;
    }

    public String getBody()
    {
        return body == null ? null : body.asText();
    }

    public void setBody(final JsonNode body)
    {
        this.body = body;
    }

    public JsonNode getQueryParams()
    {
        return queryParams;
    }

    public void setQueryParams(final JsonNode queryParams)
    {
        this.queryParams = queryParams;
    }

    public String getLoggedDateString()
    {
        return loggedDateString;
    }

    public void setLoggedDateString(final String loggedDateString)
    {
        this.loggedDateString = loggedDateString;
    }

    public String getBrowserProxyRequest()
    {
        return browserProxyRequest;
    }

    public void setBrowserProxyRequest(final String browserProxyRequest)
    {
        this.browserProxyRequest = browserProxyRequest;
    }

    public String getMethod()
    {
        return method;
    }

    public void setMethod (final String method)
    {
        this.method = method;
    }

    public String getBodyAsBase64()
    {
        return bodyAsBase64;
    }

    public void setBodyAsBase64(final String bodyAsBase64)
    {
        this.bodyAsBase64 = bodyAsBase64;
    }

    public String getClientIp()
    {
        return clientIp;
    }

    public void setClientIp(final String clientIp)
    {
        this.clientIp = clientIp;
    }

    public JsonNode getCookies()
    {
        return cookies;
    }

    public void setCookies(final JsonNode cookies)
    {
        this.cookies = cookies;
    }

    public String getAbsoluteUrl()
    {
        return absoluteUrl;
    }

    public void setAbsoluteUrl(final String absoluteUrl)
    {
        this.absoluteUrl = absoluteUrl;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(final String url)
    {
        this.url = url;
    }

    public String getLoggedDate()
    {
        return loggedDate;
    }

    public void setLoggedDate(final String loggedDate)
    {
        this.loggedDate = loggedDate;
    }

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
	}
}
