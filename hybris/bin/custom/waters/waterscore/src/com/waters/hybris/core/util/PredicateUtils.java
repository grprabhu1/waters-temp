package com.waters.hybris.core.util;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import static java.util.Optional.empty;
import static org.springframework.util.StringUtils.isEmpty;

public class PredicateUtils {
    public static <T> Optional<Predicate<T>> composeAnd(final List<Predicate<T>> predicateList) {
        if (isEmpty(predicateList)) {
            return empty();
        }

        return predicateList.stream()
                .filter(Objects::nonNull)
                .reduce(Predicate::and);
    }

}
