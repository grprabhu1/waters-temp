package com.waters.hybris.core.services;

import com.waters.hybris.core.services.data.ContentEntry;

import java.io.ByteArrayOutputStream;
import java.util.List;

public interface ContentPackagingService
{
	ByteArrayOutputStream createZip(final List<ContentEntry> entries);
}
