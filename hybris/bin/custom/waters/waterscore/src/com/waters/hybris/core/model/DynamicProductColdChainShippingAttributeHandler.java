package com.waters.hybris.core.model;

import com.waters.hybris.core.model.model.WatersProductModel;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import static com.waters.hybris.core.constants.WatersCoreConstants.COLD_CHAIN_SHIPPING_QUALIFIER;

public class DynamicProductColdChainShippingAttributeHandler extends AbstractDynamicAttributeHandler<Boolean, WatersProductModel> {

    @Override
    public Boolean get(final WatersProductModel productModel) {

        ProductFeatureModel coldChainShipping = productModel.getFeatures().stream()
                .filter(f -> COLD_CHAIN_SHIPPING_QUALIFIER.equalsIgnoreCase(f.getQualifier()))
                .findFirst().orElse(null);

        return coldChainShipping == null ? Boolean.FALSE : Boolean.valueOf(String.valueOf(coldChainShipping.getValue()));

    }
}

