package com.waters.hybris.core.wiremock.connector;


import com.waters.hybris.core.wiremock.stubs.WireMockRequests;
import com.waters.hybris.core.wiremock.stubs.WiremockFindRequests;

public interface WireMockGateway
{
	WireMockRequests getAllRequestData();

	WiremockFindRequests findRequestsForUrl(String url);

	void resetRequestCount();
}
