package com.waters.hybris.core.services.impl;

import com.waters.hybris.core.services.ContentPackagingService;
import com.waters.hybris.core.services.data.ContentEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipContentPackagingService implements ContentPackagingService
{

	private Charset encoding = StandardCharsets.UTF_8;
	private static final Logger LOG = LoggerFactory.getLogger(ZipContentPackagingService.class);

	@Override
	public ByteArrayOutputStream createZip(final List<ContentEntry> entries)
	{
		// header

		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try (ZipOutputStream zos = new ZipOutputStream(byteArrayOutputStream))
		{

			entries
				.forEach(entry -> {
					try
					{
						zos.putNextEntry(new ZipEntry(entry.getName()));
						zos.write(entry.getContent().getBytes(getEncoding()));
						zos.closeEntry();
					}
					catch (final IOException e)
					{
						LOG.error("Error creating zip for entry :"+ entry.getName(), e);
					}
				});
		}
		catch (final IOException e)
		{
			LOG.error("Error creating zip for entries :"+entries.stream().map(ContentEntry::getName).collect(Collectors.joining(",")), e);
		}

		return byteArrayOutputStream;
	}

	public Charset getEncoding()
	{
		return encoding;
	}

	public void setEncoding(final Charset encoding)
	{
		this.encoding = encoding;
	}
}
