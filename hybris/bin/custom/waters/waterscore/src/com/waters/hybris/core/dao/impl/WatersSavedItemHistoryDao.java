package com.waters.hybris.core.dao.impl;

import com.waters.hybris.core.dao.SavedItemHistoryDao;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.hmc.model.SavedValuesModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.Optional;

public class WatersSavedItemHistoryDao implements SavedItemHistoryDao {

    private FlexibleSearchService flexibleSearchService;

    @Override
    public Optional<SavedValuesModel> getLastSavedValue(final ItemModel item)
    {
        final String query = "SELECT {" + SavedValuesModel.PK + "} FROM {"+ SavedValuesModel._TYPECODE + "} WHERE {" + SavedValuesModel.MODIFIEDITEM + "} = ?pk ORDER BY {" + SavedValuesModel.MODIFIEDTIME +" } DESC";
        final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
        flexibleSearchQuery.addQueryParameter("pk", item.getPk());
        
        flexibleSearchQuery.setCount(1);
        flexibleSearchQuery.setNeedTotal(true);
        flexibleSearchQuery.setStart(0);
        final SearchResult<SavedValuesModel> searchResult = getFlexibleSearchService().search(flexibleSearchQuery);

        final List<SavedValuesModel> result = searchResult.getResult();
        if (!CollectionUtils.isEmpty(result)) {
            return Optional.of(result.get(0));
        }

        return Optional.empty();
    }

    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }
}

