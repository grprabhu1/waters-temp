package com.waters.hybris.core.services;

public interface AfterSaveListenerService {
    boolean canProcess();
}
