package com.waters.hybris.core.strategies.impl;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.waters.hybris.core.strategies.ConfigurationPropertyStrategy;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.stream.Collectors;

public class DefaultConfigurationPropertyStrategy implements ConfigurationPropertyStrategy
{
	private ConfigurationService configurationService;

	@Override
	public BiMap<String, String> getStringMap(final String propertyName)
	{
		final String value = getConfigurationService().getConfiguration().getString(propertyName);

		if (StringUtils.isNotBlank(value))
		{
			return Arrays.stream(value.split(",")).filter(StringUtils::isNotBlank)
				.map(s -> s.split(":"))
				.filter(v -> v.length == 2)
				.collect(Collectors.toMap(v -> v[0], v -> v[1], (a,b) -> b, HashBiMap::create));
		}
		return HashBiMap.create(0);
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

}
