package com.waters.hybris.core.wiremock.connector.impl;


import com.waters.hybris.core.wiremock.connector.WireMockGateway;
import com.waters.hybris.core.wiremock.stubs.WireMockRequests;
import com.waters.hybris.core.wiremock.stubs.WiremockFindRequests;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

public class DefaultWireMockGateway implements WireMockGateway
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultWireMockGateway.class);
	private static final String HTTP = "http";

	private static final int DEFAULT_WIREMOCK_PORT = 7000;
	private static final String ALL_REQUESTS = "__admin/requests";
	private static final String FIND_REQUESTS = "__admin/requests/find";
	private static final String LOCALHOST = "localhost";

	private RestTemplate restTemplate;

	private ConfigurationService configurationService;

	@Override
	public WireMockRequests getAllRequestData()
	{
		final String requestURL = getFullRequestUrl(ALL_REQUESTS);

		LOG.debug("Calling {} to get wiremock data ", requestURL);

		return getRestTemplate().getForObject(requestURL, WireMockRequests.class);
	}

	@Override
	public WiremockFindRequests findRequestsForUrl(final String url)
	{
		Assert.hasText(url, "URL cannot be null.");

		final String findRequestsUrl = getFullRequestUrl(FIND_REQUESTS);
		LOG.debug("Calling {} to get wiremock data ", findRequestsUrl);
		return getRestTemplate().postForObject(findRequestsUrl, Collections.singletonMap("url", url), WiremockFindRequests.class);
	}

	@Override
	public void resetRequestCount()
	{
		final String requestURL = getFullRequestUrl(ALL_REQUESTS);
		LOG.debug("Issuing DELETE to {} to reset wiremock request count", requestURL);
		getRestTemplate().delete(requestURL);
	}

	private String getFullRequestUrl(final String path)
	{
		final int port = getConfigurationService().getConfiguration().getInt("wiremockserver.port", DEFAULT_WIREMOCK_PORT);
		final String host = getConfigurationService().getConfiguration().getString("wiremockserver.host", LOCALHOST);

		return getRequestURL(host, port, path);
	}

	protected String getRequestURL(final String host, final int port, final String path)
	{
		return UriComponentsBuilder.newInstance()
			.scheme(HTTP)
			.host(host)
			.port(port)
			.path(path)
			.build(true)
			.toUriString();
	}

	protected RestTemplate getRestTemplate()
	{
		return restTemplate;
	}

	@Required
	public void setRestTemplate(final RestTemplate restTemplate)
	{
		this.restTemplate = restTemplate;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
