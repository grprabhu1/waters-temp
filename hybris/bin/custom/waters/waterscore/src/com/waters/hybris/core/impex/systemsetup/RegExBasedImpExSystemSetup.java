package com.waters.hybris.core.impex.systemsetup;

import de.hybris.bootstrap.config.ConfigUtil;
import de.hybris.bootstrap.config.ExtensionInfo;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.impex.jalo.ImpExManager;
import de.hybris.platform.impex.systemsetup.ImpExSystemSetup;
import de.hybris.platform.util.CSVConstants;
import de.hybris.platform.util.Config;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

// NOTE: Copied most of the code from ImpExSystemSetup

@SystemSetup(extension = SystemSetup.ALL_EXTENSIONS)
public class RegExBasedImpExSystemSetup implements InitializingBean
{
	private static final Logger LOG = Logger.getLogger(RegExBasedImpExSystemSetup.class);

	private static final String ROOT_DIRECTORY = "resources";

	private static final String PARAMETER_ESSENTIAL = "essentialdata-impex-regex-pattern";
	private static final String PARAMETER_PROJECT = "projectdata-impex-regex-pattern";

	@Override
	public void afterPropertiesSet() throws Exception
	{
		//NOOP
	}

	@SystemSetup(type = SystemSetup.Type.ESSENTIAL, process = SystemSetup.Process.ALL)
	public void createAutoImpexEssentialData(final SystemSetupContext context)
	{
		String patternCfg = Config.getParameter(context.getExtensionName() + "." + PARAMETER_ESSENTIAL);
		if (StringUtils.isNotEmpty(patternCfg))
		{
			if (!patternCfg.toLowerCase().startsWith(ROOT_DIRECTORY))
			{
				patternCfg = ROOT_DIRECTORY + "/" + patternCfg;
			}

			LOG.info("AutoImpEx for extension '" + context.getExtensionName() + "' will use userdefined regex filepattern '" + patternCfg
				+ "' for creating the essential data...");

			importData(context.getExtensionName(), ROOT_DIRECTORY, patternCfg);
		}
	}

	@SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
	public void createAutoImpexProjectData(final SystemSetupContext context)
	{
		final String path = ROOT_DIRECTORY;

		String patternCfg = Config.getParameter(context.getExtensionName() + "." + PARAMETER_PROJECT);
		if (StringUtils.isNotEmpty(patternCfg))
		{
			if (!patternCfg.toLowerCase().startsWith(ROOT_DIRECTORY))
			{
				patternCfg = ROOT_DIRECTORY + "/" + patternCfg;
			}

			if (Boolean.parseBoolean(System.getProperty("glassfish", "false")))
			{
				patternCfg = patternCfg.replace(path, context.getExtensionName());
			}

			LOG.info("AutoImpEx for extension '" + context.getExtensionName() + "' will use userdefined regex filepattern '" + patternCfg
				+ "' for creating the project data...");

			importData(context.getExtensionName(), path, patternCfg);
		}
	}

	protected void importData(final String extensionName, final String path, final String expression)
	{
		// get extension
		final ExtensionInfo extension = ConfigUtil.getPlatformConfig(ImpExSystemSetup.class).getExtensionInfo(extensionName);

		// get the impex files
		for (final String file : getFiles(extension, path, expression))
		{
			importCSVFromResources(file);
		}
	}

	protected void importCSVFromResources(final String csv)
	{
		/**
		 * just for security reason, the resources folder has to be written down in the pattern. here we exclude it again
		 * to match the classpath
		 */
		final String csvWithoutResourceFolder = csv.replace("resources/", "/");
		LOG.info("importing resource : " + csvWithoutResourceFolder);

		final String csvWithoutResourceFolderWithSlash = (csvWithoutResourceFolder.charAt(0) == '/') ? csvWithoutResourceFolder
			: "/" + csvWithoutResourceFolder;

		InputStream inputstream = null;
		try
		{
			inputstream = ImpExSystemSetup.class.getResourceAsStream(csvWithoutResourceFolderWithSlash);
			final ImpExManager impexManager = ImpExManager.getInstance();
			impexManager.importData(inputstream, CSVConstants.HYBRIS_ENCODING, CSVConstants.HYBRIS_FIELD_SEPARATOR,
				CSVConstants.HYBRIS_QUOTE_CHARACTER, true);
		}
		finally
		{
			IOUtils.closeQuietly(inputstream);
		}
	}

	protected List<String> getFiles(final ExtensionInfo extension, final String path, final String regex)
	{
		if (Boolean.parseBoolean(System.getProperty("glassfish", "false")))
		{
			return getResourcesPathsForExtension(extension.getName(), regex);
		}
		else
		{
			return scanDir(extension, path, regex);
		}
	}

	protected List<String> getResourcesPathsForExtension(final String extensionName, final String regex)
	{
		final List<String> foundFiles = new ArrayList<>();

		InputStream input = null;
		BufferedReader bufferedReader = null;

		try
		{
			input = this.getClass().getClassLoader().getResourceAsStream(extensionName + ".index.txt");
			bufferedReader = new BufferedReader(new InputStreamReader(input, "UTF-8"));
			String fileName;

			while ((fileName = bufferedReader.readLine()) != null)
			{
				final boolean accept = filePathMatches(fileName, regex.replace("resources/", ""));
				if (accept)
				{
					foundFiles.add(fileName);
				}
			}
		}
		catch (final IOException e)
		{
			LOG.error("Error while loading resources list for extension: " + extensionName + ".", e);
		}
		finally
		{
			IOUtils.closeQuietly(bufferedReader);
			IOUtils.closeQuietly(input);
		}
		return foundFiles;
	}

	protected List<String> scanDir(final ExtensionInfo extension, final String path, final String regex)
	{
		final List<String> foundFiles = new ArrayList<>();
		final File extensionDirectory = new File(extension.getExtensionDirectory(), "");
		final File directory = new File(extensionDirectory, path);
		if (directory.isDirectory())
		{
			final File[] files = directory.listFiles();
			if (files != null)
			{
				java.util.Arrays.sort(files);

				for (final File file : files)
				{
					final String filePath = createPathString(file, path);

					if (file.isDirectory())
					{
						foundFiles.addAll(getFiles(extension, filePath, regex));
					}
					else if (filePathMatches(filePath, regex))
					{
						foundFiles.add(filePath);
					}
				}
			}
		}

		return foundFiles;
	}


	protected boolean filePathMatches(final String filePath, final String regex)
	{
		return Pattern.matches(regex, filePath.replace("\\", "/"));
	}

	protected String createPathString(final File file, final String path)
	{
		if (StringUtils.isNotEmpty(path))
		{
			return path + "/" + file.getName();
		}
		else
		{
			return file.getName();
		}
	}
}
