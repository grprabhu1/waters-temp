package com.waters.hybris.core.strategies;

import java.util.Map;

public interface ConfigurationPropertyStrategy
{
	Map<String, String> getStringMap(final String propertyName);
}
