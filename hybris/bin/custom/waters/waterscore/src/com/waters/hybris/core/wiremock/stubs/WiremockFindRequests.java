package com.waters.hybris.core.wiremock.stubs;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@SuppressFBWarnings
public class WiremockFindRequests
{
	private Request[] requests;

	private String requestJournalDisabled;

	public Request[] getRequests()
	{
		return requests;
	}

	public void setRequests(final Request[] requests)
	{
		this.requests = requests;
	}

	public String getRequestJournalDisabled()
	{
		return requestJournalDisabled;
	}

	public void setRequestJournalDisabled(final String requestJournalDisabled)
	{
		this.requestJournalDisabled = requestJournalDisabled;
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
	}
}
