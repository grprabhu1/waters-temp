package com.waters.hybris.core.interceptors;

import com.waters.hybris.core.strategies.AfterSaveTypeStrategy;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.core.TenantListener;
import de.hybris.platform.jalo.type.TypeManager;
import de.hybris.platform.tx.AfterSaveEvent;
import de.hybris.platform.tx.AfterSaveListener;

import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.waters.hybris.core.util.StreamUtil.nullSafe;
import static java.util.Optional.ofNullable;

public class DefaultAfterSaveListener implements AfterSaveListener, TenantListener {

    private Map<String, AfterSaveTypeStrategy> strategyMap;
    private Map<Integer, AfterSaveTypeStrategy> processedStrategyMap;
    
    public DefaultAfterSaveListener()
    {
        Registry.registerTenantListener(this);
    }

    @Override
    public void afterSave(final Collection<AfterSaveEvent> collection) {
        nullSafe(collection)
            .forEach(event -> {
                final Integer type = Integer.valueOf(event.getPk().getTypeCode());
                getStrategyForType(type).ifPresent(strategy -> strategy.process(event));
            });
    }
    
    public Optional<AfterSaveTypeStrategy> getStrategyForType(final Integer type)
    {
        return ofNullable(getProcessedStrategyMap().get(type));
    }

    protected int getTypeCode(final String typecode)
    {
        return TypeManager.getInstance().getComposedType(typecode).getItemTypeCode();
    }

    @Required
    public void setStrategyMap(final Map<String, AfterSaveTypeStrategy> strategyMap) {
        this.strategyMap = strategyMap;
    }

    protected Map<Integer, AfterSaveTypeStrategy> getProcessedStrategyMap() {
        return ofNullable(processedStrategyMap).orElse(Collections.emptyMap());
    }



    @Override
    public void afterTenantStartUp(final Tenant tenant) {
        this.processedStrategyMap = nullSafe(strategyMap).collect(
                Collectors.toMap(entry -> getTypeCode(entry.getKey()), Map.Entry::getValue, (a, b) -> b));
    }

    @Override
    public void beforeTenantShutDown(final Tenant tenant) {
        // empty
    }

    @Override
    public void afterSetActivateSession(final Tenant tenant) {
        // empty
    }

    @Override
    public void beforeUnsetActivateSession(final Tenant tenant) {
        // empty
    }
}
