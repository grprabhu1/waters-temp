package com.waters.hybris.core.wiremock.stubs;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Meta
{
	private String total;

	public String getTotal()
	{
		return total;
	}

	public void setTotal(final String total)
	{
		this.total = total;
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
	}
}
