package com.waters.hybris.core.services.impl;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.services.ProductNamingRuleCalculationService;
import com.waters.hybris.core.util.StreamUtil;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Implementation class to calculate product naming rule.
 */
public class DefaultProductNamingRuleCalculationService implements ProductNamingRuleCalculationService
{

	private static final long MILLION = 1000000l;
	private static final int THOUSAND = 1000;

	private static final char MILLION_SUFFIX = 'M';
	private static final char THOUSAND_SUFFIX = 'K';

	private static final String PRODUCT_NAMINGRULES_METRIC_FIELDS = "product.namingrules.metricFields";
	private static final String PRODUCT_NAMINGRULES = "product.namingrules.";

	private static final Logger LOG = getLogger(DefaultProductNamingRuleCalculationService.class);

	protected static final String CLASSIFICATION_ATTRIBUTE_ASSIGNMENT_IS_NULL_FOR_PRODUCT_FEATURE = "ClassificationAttributeAssignment is null for product {} feature {}";
	protected static final String CLASSIFICATION_ATTRIBUTE_IS_NULL_FOR_PRODUCT_FEATURE_AND_CLASSIFICATION_ATTRIBUTE_ASSIGNMENT = "ClassificationAttribute is null for product {} feature {} and classification attribute assignment {}";

	private ConfigurationService configurationService;

	@Override
	public String calculateAutomaticName(final WatersProductModel watersProductModel)
	{
		final List<String> classificationHierarchy = getProductClassificationHierarchy(watersProductModel);
		if (CollectionUtils.isEmpty(classificationHierarchy))
		{
			logInfo("Product {} is not assigned to classification hierarchy", watersProductModel.getCode());
			return StringUtils.EMPTY;
		}
		if (classificationHierarchy.size() > 1)
		{
			logError("Product {} is assigned to more than 1 classification hierarchy", watersProductModel.getCode());
			return StringUtils.EMPTY;
		}
		final String productNamingRule = getProductNamingRule(classificationHierarchy.get(0));
		if (StringUtils.isEmpty(productNamingRule))
		{
			return StringUtils.EMPTY;
		}
		final ExpressionParser parser = new SpelExpressionParser();
		final StandardEvaluationContext context = new StandardEvaluationContext();


		final Map<String, String> featureValuesMap = new HashMap<>();
		final Map<String, String> featureUnitsMap = new HashMap<>();

		collateProductFeatures(watersProductModel, featureValuesMap, featureUnitsMap);

		context.setRootObject(new ContextHelper(featureValuesMap, featureUnitsMap));

		return parser.parseExpression(productNamingRule).getValue(context, String.class);
	}

	protected List<String> getProductClassificationHierarchy(final WatersProductModel watersProductModel)
	{
		return StreamUtil.nullSafe(watersProductModel.getClassificationClasses())
			.filter(classificationClass -> classificationClass.getAllSubcategories().isEmpty())
			.map(classificationClassModel -> classificationClassModel.getCode())
			.collect(Collectors.toList());
	}

	protected void logInfo(final String message, final String productCode)
	{
		LOG.info(message, productCode);
	}

	protected void logError(final String message, final String productCode)
	{
		LOG.error(message, productCode);
	}

	protected void logError(final String message, final String productCode, final String featureCode)
	{
		LOG.error(message, productCode, featureCode);
	}

	protected void logError(final String message, final String productCode, final String featureCode, final String attribute)
	{
		LOG.error(message, productCode, featureCode, attribute);
	}


	private void collateProductFeatures(final WatersProductModel watersProductModel, final Map<String, String> featureValuesMap,
	                                    final Map<String, String> featureUnitsMap)
	{
		watersProductModel.getFeatures().forEach(feature -> {
			final ClassAttributeAssignmentModel classificationAttributeAssignment = feature.getClassificationAttributeAssignment();

			if (classificationAttributeAssignment == null)
			{
				logError(CLASSIFICATION_ATTRIBUTE_ASSIGNMENT_IS_NULL_FOR_PRODUCT_FEATURE, watersProductModel.getCode(), feature.getQualifier());
			}
			else if (classificationAttributeAssignment.getClassificationAttribute() == null)
			{
				logError(CLASSIFICATION_ATTRIBUTE_IS_NULL_FOR_PRODUCT_FEATURE_AND_CLASSIFICATION_ATTRIBUTE_ASSIGNMENT, watersProductModel.getCode(), feature.getQualifier(), classificationAttributeAssignment.getPk().toString());
			}
			else
			{
				processFeature(featureValuesMap, featureUnitsMap, feature, classificationAttributeAssignment);
			}
		});
	}

	protected void processFeature(final Map<String, String> featureValuesMap, final Map<String, String> featureUnitsMap, final ProductFeatureModel feature, final ClassAttributeAssignmentModel classificationAttributeAssignment)
	{
		final String featureCode = classificationAttributeAssignment.getClassificationAttribute().getCode();
		if (feature.getValue() instanceof ClassificationAttributeValueModel)
		{
			final ClassificationAttributeValueModel classificationAttributeValueModel = (ClassificationAttributeValueModel) feature.getValue();
			collateProductFeature(featureValuesMap, featureUnitsMap, feature, featureCode, classificationAttributeValueModel.getName(Locale.ENGLISH));
		}
		else
		{
			collateProductFeature(featureValuesMap, featureUnitsMap, feature, featureCode, feature.getValue().toString());
		}
	}

	protected void collateProductFeature(final Map<String, String> featureValuesMap, final Map<String, String> featureUnitsMap,
	                                     final ProductFeatureModel feature, final String featureCode, final String featureValue)
	{
		if (StringUtils.isNotEmpty(featureValue))
		{
			// If field is a metric field then resolve it differently (Format = T for thousand and M for million)
			if (Arrays.asList(getMetricFields().split(",")).contains(featureCode))
			{
				if (Long.parseLong(featureValue) >= MILLION)
				{
					featureValuesMap.put(featureCode, String.valueOf(Long.parseLong(featureValue) / MILLION) + MILLION_SUFFIX);
				}
				else if (Long.parseLong(featureValue) >= THOUSAND)
				{
					featureValuesMap.put(featureCode, String.valueOf(Long.parseLong(featureValue) / THOUSAND) + THOUSAND_SUFFIX);
				}
				else
				{
					featureValuesMap.put(featureCode, featureValue);
				}
			}
			else
			{
				featureValuesMap.put(featureCode, featureValue);
			}
			if (feature.getUnit() != null)
			{
				featureUnitsMap.put(featureCode, feature.getUnit().getSymbol());
			}
		}
	}

	protected String getMetricFields()
	{
		return getConfigurationService().getConfiguration().getString(PRODUCT_NAMINGRULES_METRIC_FIELDS);
	}

	protected String getProductNamingRule(final String classificationClass)
	{
		return getConfigurationService().getConfiguration().getString(PRODUCT_NAMINGRULES + classificationClass);
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
