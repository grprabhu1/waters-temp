package com.waters.hybris.core.services.data;

public class ContentEntry
{
	private String name;
	private String content;

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(final String content)
	{
		this.content = content;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final ContentEntry that = (ContentEntry) o;

		if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null)
		{
			return false;
		}
		return getContent() != null ? getContent().equals(that.getContent()) : that.getContent() == null;
	}

	@Override
	public int hashCode()
	{
		int result = getName() != null ? getName().hashCode() : 0;
		result = 31 * result + (getContent() != null ? getContent().hashCode() : 0);
		return result;
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder("ContentEntry{");
		sb.append("name='").append(name).append('\'');
		sb.append(", content='").append(content).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
