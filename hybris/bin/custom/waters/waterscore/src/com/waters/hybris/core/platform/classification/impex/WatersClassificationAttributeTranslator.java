package com.waters.hybris.core.platform.classification.impex;

import de.hybris.platform.catalog.constants.GeneratedCatalogConstants.Enumerations.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.jalo.classification.ClassAttributeAssignment;
import de.hybris.platform.catalog.jalo.classification.ClassificationAttribute;
import de.hybris.platform.catalog.jalo.classification.ClassificationSystemVersion;
import de.hybris.platform.catalog.jalo.classification.impex.ClassificationAttributeTranslator;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.c2l.Language;

public class WatersClassificationAttributeTranslator extends ClassificationAttributeTranslator
{

	public WatersClassificationAttributeTranslator()
	{
		super();
	}

	public WatersClassificationAttributeTranslator(final ClassificationSystemVersion sysVer, final ClassificationAttribute attr, final char delimiter, final Language lang)
	{
		super(sysVer, attr, delimiter, lang);
	}

	@Override
	protected AbstractValueTranslator getSingleCellValueTranslator(final ClassAttributeAssignment assignment) throws HeaderValidationException {
		final String type = assignment.getAttributeType().getCode();
		if (ClassificationAttributeTypeEnum.ENUM.equalsIgnoreCase(type))
		{
			return new WatersEnumClassificationAttributeValueTranslator(assignment);
		}
		else if (shouldBeTranslatedWithString(type, assignment))
		{
			return new WatersStringClassificationAttributeValueTranslator(type);
		}

		return super.getSingleCellValueTranslator(assignment);
	}

	protected boolean shouldBeTranslatedWithString(final String type, final ClassAttributeAssignment assignment)
	{
		if (ClassificationAttributeTypeEnum.NUMBER.equalsIgnoreCase(type) || ClassificationAttributeTypeEnum.STRING.equalsIgnoreCase(type))
		{
			return assignment.getUnit() != null;
		}
		return false;
	}
}
