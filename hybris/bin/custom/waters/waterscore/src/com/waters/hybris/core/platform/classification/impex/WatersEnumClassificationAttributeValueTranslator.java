package com.waters.hybris.core.platform.classification.impex;

import de.hybris.platform.catalog.jalo.classification.ClassAttributeAssignment;
import de.hybris.platform.catalog.jalo.classification.ClassificationAttributeValue;
import de.hybris.platform.impex.jalo.translators.SingleValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;

import java.util.Collection;
import java.util.Iterator;

public class WatersEnumClassificationAttributeValueTranslator extends SingleValueTranslator
{
    private final ClassAttributeAssignment assignment;

    public WatersEnumClassificationAttributeValueTranslator(final ClassAttributeAssignment assignment)
    {
        this.assignment = assignment;
    }

    protected Object convertToJalo(final String string, final Item item)
    {
        final  Collection<ClassificationAttributeValue> values = this.assignment.getClassificationClass().getAttributeValues(this.assignment.getClassificationAttribute());

        final String[] splits = string.split(":");
        final String comp = splits.length > 1 ? splits[0] : string;

        final Iterator<ClassificationAttributeValue> valuesIterator = values.iterator();
        while(valuesIterator.hasNext())
        {
            final ClassificationAttributeValue value = valuesIterator.next();
            if (comp.equalsIgnoreCase(value.getCode()))
            {
                return value;
            }

            final Iterator<String> namesIterator = value.getAllName().values().iterator();
            while(namesIterator.hasNext())
            {
                final String name = namesIterator.next();
                if (comp.equalsIgnoreCase(name))
                {
                    return value;
                }
            }
        }

        throw new JaloInvalidParameterException("Classification attribute value " + string + " not found ", 0);
    }

    protected String convertToString(final Object obj)
    {
        final ClassificationAttributeValue val = (ClassificationAttributeValue)obj;
        return val.getCode();
    }
}


