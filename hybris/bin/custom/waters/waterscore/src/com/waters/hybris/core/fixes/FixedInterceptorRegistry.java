package com.waters.hybris.core.fixes;

import de.hybris.platform.jalo.type.TypeManager;
import de.hybris.platform.servicelayer.interceptor.impl.DefaultInterceptorRegistry;

import org.apache.log4j.Logger;

/**
 * DefaultInterceptorRegistry caches the TypeManager but this causes issues during initialisation.
 * This bean is only loaded when running standalone.
 *
 * @author Nicko Cadell
 */
public class FixedInterceptorRegistry extends DefaultInterceptorRegistry
{
	private static final Logger LOG = Logger.getLogger(FixedInterceptorRegistry.class);

	private boolean loggedOnce = false;

	@Override
	protected TypeManager getJaloTypeManager()
	{
		final TypeManager cachedTypeManager = super.getJaloTypeManager();
		final TypeManager currentTypeManager = TypeManager.getInstance();

		if (!loggedOnce && cachedTypeManager != null && cachedTypeManager != currentTypeManager)
		{
			LOG.error("+++++++++++++++++++++++++++++ InterceptorRegistry has seen a different TypeManager instance +++++++++++++++++++++++++++++");
			loggedOnce = true;
		}

		return currentTypeManager;
	}
}
