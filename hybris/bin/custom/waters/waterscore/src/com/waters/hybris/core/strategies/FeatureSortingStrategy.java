package com.waters.hybris.core.strategies;

import de.hybris.platform.classification.features.Feature;

import java.util.List;

public interface FeatureSortingStrategy
{
	/**
	 * Sort the features by position, if the position is null sort the feature by name
	 * @param features
	 * @return sorted list of Feature
	 */
	List<Feature> sortByPositionAndName(List<Feature> features);
}
