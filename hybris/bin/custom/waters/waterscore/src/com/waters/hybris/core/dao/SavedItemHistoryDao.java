package com.waters.hybris.core.dao;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.hmc.model.SavedValuesModel;

import java.util.Optional;

public interface SavedItemHistoryDao {
    Optional<SavedValuesModel> getLastSavedValue(ItemModel item);
}
