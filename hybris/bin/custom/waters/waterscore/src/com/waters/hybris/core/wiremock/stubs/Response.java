package com.waters.hybris.core.wiremock.stubs;

import com.fasterxml.jackson.databind.JsonNode;
import net.minidev.json.JSONObject;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Response
{
	private JSONObject headers;

    private JsonNode body;

    private String status;

    private String bodyAsBase64;

    public JSONObject getHeaders()
    {
        return headers;
    }

    public void setHeaders(final JSONObject headers)
    {
        this.headers = headers;
    }

    public String getBody()
    {
        return body == null ? null : body.asText();
    }

    public void setBody(final JsonNode body)
    {
        this.body = body;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(final String status)
    {
        this.status = status;
    }

    public String getBodyAsBase64()
    {
        return bodyAsBase64;
    }

    public void setBodyAsBase64(final String bodyAsBase64)
    {
        this.bodyAsBase64 = bodyAsBase64;
    }

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
	}
}
