package com.waters.hybris.core.services.impl;

import com.waters.hybris.core.services.AfterSaveListenerService;
import de.hybris.platform.core.Registry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collections;
import java.util.Set;

import static java.util.Optional.ofNullable;

public class WatersAfterSaveListenerService implements AfterSaveListenerService
{
    private static final Logger LOG = LoggerFactory.getLogger(WatersAfterSaveListenerService.class);
    private Set<String> tenantBlacklist;

    @Override
    public boolean canProcess()
    {
        final String tenantID = getTenantID();
        if (getTenantBlacklist().contains(tenantID))
        {
            LOG.info("Tenant [{}] in black list for AfterSaveListener. Skipping thread pool executor creation.", tenantID);
            return false;
        }

        return true;
    }

    protected String getTenantID() {
        return Registry.getCurrentTenant().getTenantID();
    }


    protected Set<String> getTenantBlacklist() {
        return ofNullable(tenantBlacklist).orElse(Collections.emptySet());
    }


    @Required
    public void setTenantBlacklist(final Set<String> tenantBlacklist)
    {
        this.tenantBlacklist = tenantBlacklist;
    }


}
