package com.waters.hybris.core.jalo.model;

import com.waters.hybris.core.constants.WatersCoreConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

@SuppressWarnings("PMD")
public class WatersCoreManager extends GeneratedWatersCoreManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( WatersCoreManager.class.getName() );
	
	public static final WatersCoreManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (WatersCoreManager) em.getExtension(WatersCoreConstants.EXTENSIONNAME);
	}
	
}
