package com.waters.hybris.core.strategies;


import com.waters.hybris.core.model.model.ExternalMediaModel;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;

import java.util.Locale;

public interface ProductImageResolutionStrategy
{
	String resolveImageUrl(ExternalMediaModel externalMediaModel, Locale locale, String rendition);

	String resolveImageUrl(ExternalMediaModel externalMediaModel, Locale locale);

	ImageData populateImageData(final ExternalMediaModel externalMediaModel, final ImageDataType imageDataType);
}
