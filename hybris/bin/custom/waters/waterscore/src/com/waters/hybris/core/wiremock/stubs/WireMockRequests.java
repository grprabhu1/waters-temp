package com.waters.hybris.core.wiremock.stubs;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Arrays;

public class WireMockRequests
{
	private Requests[] requests;

    private String requestJournalDisabled;

    private Meta meta;

    public Requests[] getRequests()
    {
        return Arrays.copyOf(requests, requests.length);
    }

    public void setRequests(final Requests[] requests)
    {
        this.requests =  Arrays.copyOf(requests, requests.length);
    }

    public String getRequestJournalDisabled()
    {
        return requestJournalDisabled;
    }

    public void setRequestJournalDisabled(final String requestJournalDisabled)
    {
        this.requestJournalDisabled = requestJournalDisabled;
    }

    public Meta getMeta()
    {
        return meta;
    }

    public void setMeta(final Meta meta)
    {
        this.meta = meta;
    }

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
	}
}
