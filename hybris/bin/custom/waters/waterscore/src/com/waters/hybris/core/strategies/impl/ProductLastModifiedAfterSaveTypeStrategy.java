package com.waters.hybris.core.strategies.impl;


import com.waters.hybris.core.dao.SavedItemHistoryDao;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.services.AfterSaveListenerService;
import com.waters.hybris.core.strategies.AfterSaveTypeStrategy;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.AfterSaveEvent;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.Objects;

import static org.slf4j.LoggerFactory.getLogger;

public class ProductLastModifiedAfterSaveTypeStrategy implements AfterSaveTypeStrategy {

    private static final Logger LOG = getLogger(ProductLastModifiedAfterSaveTypeStrategy.class);
    private ModelService modelService;
    private AfterSaveListenerService afterSaveListenerService;
    private SavedItemHistoryDao savedItemHistoryDao;

    @Override
    public void process(final AfterSaveEvent afterSaveEvent) {
        if (!getAfterSaveListenerService().canProcess() || afterSaveEvent == null || afterSaveEvent.getType() == AfterSaveEvent.REMOVE)
        {
            return;
        }

        final ItemModel itemModel = getModelService().get(afterSaveEvent.getPk());

        if (itemModel instanceof WatersProductModel) {
            final WatersProductModel productModel = ((WatersProductModel) itemModel);

            getSavedItemHistoryDao().getLastSavedValue(itemModel).ifPresent(savedValuesModel -> {
                    final UserModel userModel = savedValuesModel.getUser();

                    LOG.debug("lastEditedBy {}, userModel name {}", productModel.getLastEditedBy(), userModel.getName());
                    if (!Objects.equals(productModel.getLastEditedBy(), userModel.getName())) {
                        productModel.setLastEditedBy(userModel.getName());
                        getModelService().save(productModel);
                    }
            });
        }
    }


    public ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    protected AfterSaveListenerService getAfterSaveListenerService() {
        return afterSaveListenerService;
    }

    @Required
    public void setAfterSaveListenerService(final AfterSaveListenerService afterSaveListenerService) {
        this.afterSaveListenerService = afterSaveListenerService;
    }

    public SavedItemHistoryDao getSavedItemHistoryDao() {
        return savedItemHistoryDao;
    }

    @Required
    public void setSavedItemHistoryDao(final SavedItemHistoryDao savedItemHistoryDao) {
        this.savedItemHistoryDao = savedItemHistoryDao;
    }

}
