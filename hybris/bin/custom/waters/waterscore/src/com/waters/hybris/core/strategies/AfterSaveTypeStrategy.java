package com.waters.hybris.core.strategies;

import de.hybris.platform.tx.AfterSaveEvent;

public interface AfterSaveTypeStrategy {
    void process(AfterSaveEvent afterSaveEvent);
}
