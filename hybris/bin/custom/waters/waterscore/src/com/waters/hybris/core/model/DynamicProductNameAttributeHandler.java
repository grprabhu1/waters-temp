package com.waters.hybris.core.model;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.util.StreamUtil;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

public class DynamicProductNameAttributeHandler extends AbstractDynamicAttributeHandler<String, WatersProductModel>
{
	@Override
	public String get(final WatersProductModel model)
	{
		final Optional<String> name = StreamUtil.first(model, StringUtils::isNotBlank, WatersProductModel::getManualProductTitle, WatersProductModel::getAutomaticProductTitle, WatersProductModel::getSapProductTitle);
		return name.orElse(null);
	}
}

