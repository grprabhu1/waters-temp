package com.waters.hybris.core.services.impl;

import com.waters.hybris.core.strategies.impl.DefaultSavedValuesStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.SavedValueEntryType;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.hmc.model.SavedValuesModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.servicelayer.type.TypeService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class DefaultSavedValuesStrategyTest
{
	private final String newValue = "newValue";

	private final String oldValue = "oldValue";
	@Mock
	private ModelService modelService;

	@Mock
	private TypeService typeService;

	@Mock
	private TimeService timeService;

	@Mock
	private ItemModel itemModel;

	@Mock
	private UserModel userModel;

	@InjectMocks
	@Spy
	private DefaultSavedValuesStrategy testObj = new DefaultSavedValuesStrategy();

	@Before
	public void setup(){
		when(modelService.create(SavedValuesModel.class)).thenReturn(new SavedValuesModel());
	}

	@Test
	public void shouldReturnEmptyIfTheObjectsAreTheSame(){
		Optional<SavedValuesModel> result = testObj.create(itemModel, userModel, oldValue, oldValue, false);
		Assert.assertFalse(result.isPresent());
	}
	@Test
	public void shouldReturnEmptyIfTheObjectsAreNull(){
		Optional<SavedValuesModel> result = testObj.create(itemModel, userModel, null, null, false);
		Assert.assertFalse(result.isPresent());
	}

	@Test
	public void shouldReturnSavedValuesModeIfTheObjectsAreDifferent(){
		Optional<SavedValuesModel> result = testObj.create(itemModel, userModel, newValue, oldValue, false);
		SavedValuesModel savedValuesModel = result.get();
		Assert.assertNotNull(result);
		Assert.assertEquals(userModel, savedValuesModel.getUser());
		Assert.assertEquals(SavedValueEntryType.CHANGED, savedValuesModel.getModificationType());
		Assert.assertEquals(itemModel, savedValuesModel.getModifiedItem());
	}

	@Test
	public void shouldReturnSavedValuesModeIfTheObjectsTheSameAndForceIsTrue(){
		Optional<SavedValuesModel> result = testObj.create(itemModel, userModel, oldValue, oldValue, true);
		SavedValuesModel savedValuesModel = result.get();
		Assert.assertNotNull(result);
		Assert.assertEquals(userModel, savedValuesModel.getUser());
		Assert.assertEquals(SavedValueEntryType.CHANGED, savedValuesModel.getModificationType());
		Assert.assertEquals(itemModel, savedValuesModel.getModifiedItem());
	}
}
