package com.waters.hybris.core.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static java.util.Collections.singleton;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WatersAfterSaveListenerServiceTest {

    private static final String MASTER = "master";

    @InjectMocks
    @Spy
    private WatersAfterSaveListenerService testObj = new WatersAfterSaveListenerService();

    @Before
    public void setup()
    {
        doReturn(MASTER).when(testObj).getTenantID();

        testObj.setTenantBlacklist(singleton(MASTER));
    }

    @Test
    public void givenANullTenantList_thenReturnTrue()
    {
        testObj.setTenantBlacklist(null);
        final boolean result = testObj.canProcess();
        assertTrue(result);
    }

    @Test
    public void givenAnEmptyTenantList_thenReturnTrue()
    {
        testObj.setTenantBlacklist(Collections.emptySet());
        final boolean result = testObj.canProcess();
        assertTrue(result);
    }

    @Test
    public void givenAMatchingTenant_thenReturnFalse()
    {
        final boolean result = testObj.canProcess();
        assertFalse(result);
    }
}
