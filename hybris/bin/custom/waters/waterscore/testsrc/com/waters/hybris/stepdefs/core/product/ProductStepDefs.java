package com.waters.hybris.stepdefs.core.product;

import com.waters.hybris.core.enums.SalesStatus;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.util.StreamUtil;
import com.waters.hybris.stepdefs.core.product.dto.ProductDto;
import com.waters.hybris.stepdefs.core.product.dto.ProductDtoWithSapData;
import cucumber.api.java.en.Then;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.lang.StringUtils;

import java.util.List;

import static java.lang.Boolean.TRUE;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by ramana on 10/04/2018.
 */
public class ProductStepDefs extends AbstractProductStepDefs<ProductDto>
{
	public static final String WATERS_CLASSIFICATION_1_0_CONSUMABLES_AND_SPARES_COLDCHAINSHIPPING = "WatersClassification/1.0/ConsumablesAndSpares.coldchainshipping";
	public static final String WATERS_CLASSIFICATION_1_0_PRODUCTS_SAPHIERARCHY = "WatersClassification/1.0/Products.saphierarchy";
	public static final String WATERS_CLASSIFICATION_1_0_PRODUCTS_BATCHMANAGED = "WatersClassification/1.0/Products.batchmanaged";
	public static final String WATERS_CLASSIFICATION_1_0_PRODUCTS_REMAININGSHELFLIFE = "WatersClassification/1.0/Products.remainingshelflife";
	public static final String WATERS_CLASSIFICATION_1_0_PRODUCTS_CONTAINERREQUIREMENT = "WatersClassification/1.0/Products.containerrequirement";
	public static final String WATERS_CLASSIFICATION_1_0_PRODUCTS_HAZARDMATERIALNUMBER = "WatersClassification/1.0/Products.hazardmaterialnumber";
	public static final String WATERS_CLASSIFICATION_1_0_PRODUCTS_HAZARDMATERIALTEXT = "WatersClassification/1.0/Products.hazardmaterialtext";
	public static final String WATERS_CLASSIFICATION_1_0_PRODUCTS_WEIGHT = "WatersClassification/1.0/Products.weight";
	public static final String WATERS_CLASSIFICATION_1_0_PRODUCTS_WEIGHTUNIT = "WatersClassification/1.0/Products.weightunit";
	public static final String WATERS_CLASSIFICATION_1_0_PRODUCTS_HARMONIZATIONCODE = "WatersClassification/1.0/Products.harmonizationcode";
	public static final String WATERS_CLASSIFICATION_1_0_PRODUCTS_DGTECHNAME = "WatersClassification/1.0/Products.dgtechname";

	@Then("^the following products are not available$")
	public void the_following_products_do_not_exist(final List<ProductDto> products)
	{
		forEachProduct(products, (p, catalogVersion) -> {
			final ProductModel product = getProduct(catalogVersion, p.getId());
			assertThat(product)
					.as(String.format("Product [%s] exists, when it should not, in catalog [%s:%s]", p.getId(), p.getCatalog(), p.getCatalogVersion()))
					.isNull();
		});
	}

	@Then("^the following products are available$")
	public void the_following_products_are_available(final List<ProductDto> products)
	{
		forEachProduct(products, (p, catalogVersion) -> {
			final ProductModel product = getProduct(catalogVersion, p.getId());
			assertThat(product)
					.as(String.format("Product [%s] does not exist, when it should be, in catalog [%s:%s]", p.getId(), p.getCatalog(), p.getCatalogVersion()))
					.isNotNull();
		});
	}

	@Then("^the following products are available with sap data$")
	public void the_following_products_are_available_with_sap_data(final List<ProductDtoWithSapData> products)
	{
		products.forEach(p -> {
			final ProductModel product = getProduct(getCatalogVersion(p), p.getId());
			assertThat(product)
					.as(String.format("Product [%s] does not exist, when it should be, in catalog [%s:%s]", p.getId(), p.getCatalog(), p.getCatalogVersion()))
					.isNotNull();
			assertThat(product instanceof WatersProductModel)
				.as(String.format("Product [%s] is not waters product, when it should be", p.getId()))
				.isTrue();
			final WatersProductModel watersProduct = (WatersProductModel) product;

			validateSapData(p.getDescription(), watersProduct.getSapProductTitle(), "product title");
			validateSapData(p.getHierarchy(), getFeatureStringValue(watersProduct, WATERS_CLASSIFICATION_1_0_PRODUCTS_SAPHIERARCHY), "product hierarchy");
			validateSapData(p.getBatchManaged(), getFeatureStringValue(watersProduct, WATERS_CLASSIFICATION_1_0_PRODUCTS_BATCHMANAGED), "bach managed");
			validateSapData(p.getRemainingSelfLife(), getFeatureStringValue(watersProduct, WATERS_CLASSIFICATION_1_0_PRODUCTS_REMAININGSHELFLIFE), "remaining self life");
			validateSapData(p.getContainerRequirement(), getFeatureStringValue(watersProduct, WATERS_CLASSIFICATION_1_0_PRODUCTS_CONTAINERREQUIREMENT), "container requirement");
			validateSapData(p.getHazardousMaterialNumber(), getFeatureStringValue(watersProduct, WATERS_CLASSIFICATION_1_0_PRODUCTS_HAZARDMATERIALNUMBER), "hazardous material number");
			validateSapData(p.getHazardousMaterialText(), getFeatureStringValue(watersProduct, WATERS_CLASSIFICATION_1_0_PRODUCTS_HAZARDMATERIALTEXT), "hazardous material text");
			validateSapData(p.getWeight(), getFeatureStringValue(watersProduct, WATERS_CLASSIFICATION_1_0_PRODUCTS_WEIGHT), "weight");
			validateSapData(p.getWeightUom(), getFeatureStringValue(watersProduct, WATERS_CLASSIFICATION_1_0_PRODUCTS_WEIGHTUNIT), "weight unit");
			validateSapData(p.getHarmonizationCode(), getFeatureStringValue(watersProduct, WATERS_CLASSIFICATION_1_0_PRODUCTS_HARMONIZATIONCODE), "harmonization code");
			validateSapData(p.getDgTechName(), getFeatureStringValue(watersProduct, WATERS_CLASSIFICATION_1_0_PRODUCTS_DGTECHNAME), "dg tech name");
		});
	}

	protected String getFeatureStringValue(final WatersProductModel watersProduct, final String qualifier)
	{
		return StreamUtil.nullSafe(watersProduct.getFeatures())
			.filter(feature -> qualifier.equals(feature.getQualifier()))
			.map(feature -> feature.getValue().toString())
			.findFirst()
			.orElse(null);
	}

	protected void validateSapData(final String expected, final String actual, final String errorField)
	{
		assertThat(actual)
			.as(String.format("Product sap [%s] should be [%s], which is not", errorField, expected))
			.isIn(expected, StringUtils.isBlank(expected) ? null : expected);
	}

	@Then("^the following products are proprietary$")
	public void the_following_products_are_proprietary(final List<ProductDto> products)
	{
		forEachProduct(products, (p, catalogVersion) -> {
			final ProductModel product = getProduct(catalogVersion, p.getId());
			assertThat(product instanceof WatersProductModel)
				.as(String.format("Product [%s] is not waters product, when it should be", p.getId()))
				.isTrue();
			assertThat(((WatersProductModel) product).isProprietary())
				.as(String.format("Product [%s] is not proprietary product, when it should be", p.getId()))
				.isTrue();
		});
	}

	@Then("^the following products are not proprietary$")
	public void the_following_products_are_not_proprietary(final List<ProductDto> products)
	{
		forEachProduct(products, (p, catalogVersion) -> {
			final ProductModel product = getProduct(catalogVersion, p.getId());
			assertThat(product instanceof WatersProductModel)
				.as(String.format("Product [%s] is not waters product, when it should be", p.getId()))
				.isTrue();
			assertThat(((WatersProductModel) product).isProprietary())
				.as(String.format("Product [%s] is proprietary product, when it should not be", p.getId()))
				.isFalse();
		});
	}

	@Then("^the following products are cold chain$")
	public void the_following_products_are_cold_chain(final List<ProductDto> products)
	{
		forEachProduct(products, (p, catalogVersion) -> {
			final ProductModel product = getProduct(catalogVersion, p.getId());
			assertThat(product instanceof WatersProductModel)
				.as(String.format("Product [%s] is not waters product, when it should be", p.getId()))
				.isTrue();
			assertThat(getFeatureValue(((WatersProductModel) product), WATERS_CLASSIFICATION_1_0_CONSUMABLES_AND_SPARES_COLDCHAINSHIPPING))
				.as(String.format("Product [%s] is not cold chain product, when it should be", p.getId()))
				.isTrue();
		});
	}

	@Then("^the following products are not cold chain$")
	public void the_following_products_are_not_cold_chain(final List<ProductDto> products)
	{
		forEachProduct(products, (p, catalogVersion) -> {
			final ProductModel product = getProduct(catalogVersion, p.getId());
			assertThat(product instanceof WatersProductModel)
				.as(String.format("Product [%s] is not waters product, when it should be", p.getId()))
				.isTrue();
			assertThat(getFeatureValue(((WatersProductModel) product), WATERS_CLASSIFICATION_1_0_CONSUMABLES_AND_SPARES_COLDCHAINSHIPPING))
				.as(String.format("Product [%s] is cold chain product, when it should not be", p.getId()))
				.isNotEqualTo(TRUE);
		});
	}

	protected Boolean getFeatureValue(final WatersProductModel watersProduct, final String qualifier)
	{
		return StreamUtil.nullSafe(watersProduct.getFeatures())
			.filter(feature -> qualifier.equals(feature.getQualifier()))
			.map(feature -> (Boolean) feature.getValue())
			.findFirst()
			.orElse(null);
	}

	@Then("^the following products are discontinued$")
	public void the_following_products_are_discontinued(final List<ProductDto> products)
	{
		forEachProduct(products, (p, catalogVersion) -> {
			final ProductModel product = getProduct(catalogVersion, p.getId());
			assertThat(product instanceof WatersProductModel)
				.as(String.format("Product [%s] is not waters product, when it should be", p.getId()))
				.isTrue();
			assertThat(((WatersProductModel) product).getSalesStatus())
				.as(String.format("Product [%s] is not discontinued product, when it should be", p.getId()))
				.isEqualTo(SalesStatus.DISCONTINUENOREPLACEMENT);
			assertThat(((WatersProductModel) product).getApprovalStatus())
				.as(String.format("Product [%s] is not archived / unapproved product, when it should be", p.getId()))
				.isEqualTo(ArticleApprovalStatus.UNAPPROVED);
		});
	}

	@Then("^the following products are discontinued with replacement$")
	public void the_following_products_are_discontinued_with_replacement(final List<ProductDto> products)
	{
		forEachProduct(products, (p, catalogVersion) -> {
			final ProductModel product = getProduct(catalogVersion, p.getId());
			assertThat(product instanceof WatersProductModel)
				.as(String.format("Product [%s] is not waters product, when it should be", p.getId()))
				.isTrue();
			assertThat(((WatersProductModel) product).getSalesStatus())
				.as(String.format("Product [%s] is not discontinued with replacement product, when it should be", p.getId()))
				.isEqualTo(SalesStatus.DISCONTINUEWITHREPLACEMENT);
		});
	}

	@Then("^the following products are not discontinued$")
	public void the_following_products_are_not_discontinued(final List<ProductDto> products)
	{
		forEachProduct(products, (p, catalogVersion) -> {
			final ProductModel product = getProduct(catalogVersion, p.getId());
			assertThat(product instanceof WatersProductModel)
				.as(String.format("Product [%s] is not waters product, when it should be", p.getId()))
				.isTrue();
			assertThat(((WatersProductModel) product).getSalesStatus())
				.as(String.format("Product [%s] is discontinued product, when it should not be", p.getId()))
				.isNotIn(SalesStatus.DISCONTINUENOREPLACEMENT, SalesStatus.DISCONTINUEWITHREPLACEMENT);
		});
	}

	@Then("^the following products are obsolete$")
	public void the_following_products_are_obsolete(final List<ProductDto> products)
	{
		forEachProduct(products, (p, catalogVersion) -> {
			final ProductModel product = getProduct(catalogVersion, p.getId());
			assertThat(product instanceof WatersProductModel)
				.as(String.format("Product [%s] is not waters product, when it should be", p.getId()))
				.isTrue();
			assertThat(((WatersProductModel) product).getSalesStatus())
				.as(String.format("Product [%s] is not obsolete product, when it should be", p.getId()))
				.isEqualTo(SalesStatus.OBSOLETENOREPLACEMENT);
			assertThat(((WatersProductModel) product).getApprovalStatus())
				.as(String.format("Product [%s] is not archived / unapproved product, when it should be", p.getId()))
				.isEqualTo(ArticleApprovalStatus.UNAPPROVED);
		});
	}

	@Then("^the following products are obsolete with replacement$")
	public void the_following_products_are_obsolete_with_replacement(final List<ProductDto> products)
	{
		forEachProduct(products, (p, catalogVersion) -> {
			final ProductModel product = getProduct(catalogVersion, p.getId());
			assertThat(product instanceof WatersProductModel)
				.as(String.format("Product [%s] is not waters product, when it should be", p.getId()))
				.isTrue();
			assertThat(((WatersProductModel) product).getSalesStatus())
				.as(String.format("Product [%s] is not obsolete with replacement product, when it should be", p.getId()))
				.isEqualTo(SalesStatus.OBSOLETEWITHREPLACEMENT);
		});
	}

	@Then("^the following products are not obsolete$")
	public void the_following_products_are_not_obsolete(final List<ProductDto> products)
	{
		forEachProduct(products, (p, catalogVersion) -> {
			final ProductModel product = getProduct(catalogVersion, p.getId());
			assertThat(product instanceof WatersProductModel)
				.as(String.format("Product [%s] is not waters product, when it should be", p.getId()))
				.isTrue();
			assertThat(((WatersProductModel) product).getSalesStatus())
				.as(String.format("Product [%s] is obsolete product, when it should not be", p.getId()))
				.isNotIn(SalesStatus.OBSOLETENOREPLACEMENT, SalesStatus.OBSOLETEWITHREPLACEMENT);
		});
	}
}
