package com.waters.hybris.core.util;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;

import java.util.Optional;
import java.util.function.Predicate;

import static com.waters.hybris.core.util.PredicateUtils.composeAnd;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static org.fest.assertions.Assertions.assertThat;

@UnitTest
public class PredicateUtilsTest {


    private Predicate<Object> predicate = (o) -> o != null ;
    private Predicate<Object> predicate2 = (o) -> o instanceof PredicateUtilsTest ;

    @Test
    public void givenANullCollection_thenReturnEmpty()
    {
        final Optional<Predicate<Object>> result = composeAnd(null);
        assertThat(result).isEqualTo(empty());
    }

    @Test
    public void givenAnEmptyList_thenReturnEmpty()
    {
        final Optional<Predicate<Object>> result = composeAnd(emptyList());
        assertThat(result).isEqualTo(empty());
    }

    @Test
    public void givenAList_withANull_thenReturnEmpty()
    {
        final Optional<Predicate<Object>> result = composeAnd(singletonList(null));
        assertThat(result).isEqualTo(empty());
    }

    @Test
    public void givenAList_withAPredicate_thenReturnAPredicate_thatTestsTrue()
    {
        final Optional<Predicate<Object>> result = composeAnd(singletonList(predicate));
        assertThat(result).isNotEqualTo(empty());

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().test(new Object())).isTrue();
    }

    @Test
    public void givenAList_withAPredicate_andANull_thenReturnAPredicate_thatTestsTrue()
    {
        final Optional<Predicate<Object>> result = composeAnd(asList(predicate, null));
        assertThat(result).isNotEqualTo(empty());

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().test(new Object())).isTrue();
    }

    @Test
    public void givenAList_withTwoPredicate_thenReturnAPredicate_thatTestsTrue()
    {
        final Optional<Predicate<Object>> result = composeAnd(asList(predicate, predicate2));
        assertThat(result).isNotEqualTo(empty());

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().test(new PredicateUtilsTest())).isTrue();
    }

    @Test
    public void givenAList_withTwoPredicate_andTheOrderIsDiffernt_thenReturnAPredicate_thatTestsTrue()
    {
        final Optional<Predicate<Object>> result = composeAnd(asList(predicate2, predicate));
        assertThat(result).isNotEqualTo(empty());

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().test(new PredicateUtilsTest())).isTrue();
    }
}