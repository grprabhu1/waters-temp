package com.waters.hybris.stepdefs.core.product;

import com.waters.hybris.stepdefs.core.product.dto.ProductDto;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.function.BiConsumer;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class AbstractProductStepDefs<T extends ProductDto>
{
	private ProductService productService;
	private CatalogVersionService catalogVersionService;

	protected void forEachProduct(final List<T> products, final BiConsumer<T, CatalogVersionModel> action)
	{
		products.forEach(p -> action.accept(p, getCatalogVersion(p)));
	}

	protected void forEachExistingProduct(final List<T> products, final BiConsumer<T, ProductModel> action)
	{
		products.forEach(p -> {
			final ProductModel product = getProduct(p);
			assertThat(product)
					.as(String.format("Product [%s] not found", p.getId()))
					.isNotNull();

			action.accept(p, product);
		});
	}

	protected CatalogVersionModel getCatalogVersion(final ProductDto p)
	{
		return getCatalogVersionService().getCatalogVersion(p.getCatalog(), p.getCatalogVersion());
	}

	protected ProductModel getProduct(final ProductDto p)
	{
		final CatalogVersionModel catalogVersion = getCatalogVersionService().getCatalogVersion(p.getCatalog(), p.getCatalogVersion());
		assertThat(catalogVersion)
				.as(String.format("Catalog [%s] Version [%s] not found", p.getCatalog(), p.getCatalogVersion()))
				.isNotNull();

		return getProduct(catalogVersion, p.getId());
	}

	protected ProductModel getProduct(final CatalogVersionModel catalogVersion, final String id)
	{
		try
		{
			return getProductService().getProductForCode(catalogVersion, id);
		}
		catch (final UnknownIdentifierException e)
		{
			return null;
		}
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}
}
