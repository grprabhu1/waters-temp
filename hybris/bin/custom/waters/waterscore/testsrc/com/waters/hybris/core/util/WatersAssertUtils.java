package com.waters.hybris.core.util;

import org.junit.Assert;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class WatersAssertUtils
{
	public static <T> void assertCollectionContainsSameElements(final Collection<T> expected, final Collection<T> actual)
	{
		final List<T> concurrentList = new CopyOnWriteArrayList<>(expected);
		for (final T data : actual)
		{
			if (concurrentList.isEmpty())
			{
				Assert.fail("Missing expected data. Actual: " + data.toString());
			}
			if (concurrentList.contains(data))
			{
				concurrentList.remove(data);
			}
			else
			{
				Assert.fail("Missing actual data -> " + data.toString() + " in expected: " + expected.toString());
			}
		}
		if (!concurrentList.isEmpty())
		{
			Assert.fail("Actual data missing, Expected pending to be matched: " + concurrentList.toString());
		}
	}

	public static <T> void assertCollectionContainsSameElements(final Collection<T> expected, final Collection<T> actual, final Comparator<? super T> comparator)
	{
		if (expected.size() != actual.size())
		{
			assertCollectionContainsSameElements(expected, actual);
		}
		else
		{
			final List<T> expectedSorted = expected.stream().sorted(comparator).collect(Collectors.toList());
			final List<T> actualSorted = actual.stream().sorted(comparator).collect(Collectors.toList());
			for (int i = 0; i < expectedSorted.size(); i++)
			{
				Assert.assertEquals(expectedSorted.get(i), actualSorted.get(i));
			}
		}
	}
}
