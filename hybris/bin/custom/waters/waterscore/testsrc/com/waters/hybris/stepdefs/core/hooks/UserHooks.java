package com.waters.hybris.stepdefs.core.hooks;

import com.waters.hybris.stepdefs.core.helper.SessionCreatedModelsHelper;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;

public class UserHooks
{
	private UserService userService;
	private ModelService modelService;
	private SessionCreatedModelsHelper sessionCreatedModelsHelper;

	@Before(value= "@AdminUser", order = 1 )
	public void setUp() {
		getUserService().setCurrentUser(getUserService().getUserForUID("admin"));
	}

	@After(value = "@RemoveOrders", order = 101)
	public void removeOrders()
	{
		final UserModel currentUser = getUserService().getCurrentUser();
		if (currentUser instanceof CustomerModel)
		{
			final Collection<OrderModel> orders = currentUser.getOrders();
			getModelService().removeAll(orders);
		}
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

		protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected SessionCreatedModelsHelper getSessionCreatedModelsHelper()
	{
		return sessionCreatedModelsHelper;
	}

	@Required
	public void setSessionCreatedModelsHelper(final SessionCreatedModelsHelper sessionCreatedModelsHelper)
	{
		this.sessionCreatedModelsHelper = sessionCreatedModelsHelper;
	}
}
