package com.waters.hybris.core.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.UnlocalizedFeature;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultFeatureSortingStrategyTest
{
	@InjectMocks
	private DefaultFeatureSortingStrategy featureSortingStrategy;

	@Test
	public void testSort(){
		ClassAttributeAssignmentModel class1 = new ClassAttributeAssignmentModel();
		class1.setPosition(3);
		Feature feature1 = new UnlocalizedFeature(class1);
		ClassAttributeAssignmentModel class2 = new ClassAttributeAssignmentModel();
		class2.setPosition(1);
		Feature feature2 = new UnlocalizedFeature(class2);
		List<Feature> unorderedFeatures = Arrays.asList(feature1, feature2);


		final List<Feature> sortedFeatures = featureSortingStrategy.sortByPositionAndName(unorderedFeatures);
		Assert.assertEquals(2, sortedFeatures.size());
		Assert.assertEquals(new Integer(1), sortedFeatures.get(0).getClassAttributeAssignment().getPosition());
		Assert.assertEquals(new Integer(3), sortedFeatures.get(1).getClassAttributeAssignment().getPosition());
	}

	@Test
	public void testNullFeatureList(){
		List<Feature> unorderedFeatures = null;
		final List<Feature> sortedFeatures = featureSortingStrategy.sortByPositionAndName(unorderedFeatures);
		Assert.assertEquals(0, sortedFeatures.size());
	}

}
