package com.waters.hybris.core.model;

import com.waters.hybris.core.model.model.WatersProductModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static com.waters.hybris.core.constants.WatersCoreConstants.HAZARD_MATERIAL_NUMBER_QUALIFIER;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class DynamicProductHazardousAttributeHandlerTest
{

	@InjectMocks
	private final DynamicAttributeHandler<Boolean, WatersProductModel> testObj = new DynamicProductHazardousAttributeHandler();

	@Mock
	private WatersProductModel productModel;

	@Mock
	private ProductFeatureModel productFeatureModel;

	@Before
	public void setUp()
	{
		given(productModel.getFeatures()).willReturn(Collections.singletonList(productFeatureModel));
		given(productFeatureModel.getQualifier()).willReturn(HAZARD_MATERIAL_NUMBER_QUALIFIER);
		given(productFeatureModel.getValue()).willReturn("1");
	}

	@Test
	public void testGet_ReturnsTrue_whenMaterialNumberIsNOTEmpty(){
		Assert.assertTrue(testObj.get(productModel).booleanValue());
	}

	@Test
	public void testGet_ReturnsTrue_whenMaterialNumberIsAValueModel(){

		final ClassificationAttributeValueModel classificationAttributeValueModel = mock(ClassificationAttributeValueModel.class);
		given(productFeatureModel.getValue()).willReturn(classificationAttributeValueModel);

		Assert.assertTrue(testObj.get(productModel).booleanValue());
	}

	@Test
	public void testGet_ReturnsFalse_whenMaterialNumberAttributeIsNOTFound(){

		given(productFeatureModel.getQualifier()).willReturn("otherQualifier");

		Assert.assertFalse(testObj.get(productModel).booleanValue());
	}

	@Test
	public void testGet_ReturnsFalse_whenNoClassificationAttributes(){

		given(productModel.getFeatures()).willReturn(Collections.emptyList());

		Assert.assertFalse(testObj.get(productModel).booleanValue());
	}

	@Test
	public void testGet_ReturnsFalse_whenMaterialNumberIsNull(){

		given(productFeatureModel.getValue()).willReturn(null);

		Assert.assertFalse(testObj.get(productModel).booleanValue());
	}

	@Test
	public void testGet_ReturnsFalse_whenMaterialNumberIsEmpty(){

		given(productFeatureModel.getValue()).willReturn(StringUtils.EMPTY);

		Assert.assertFalse(testObj.get(productModel).booleanValue());
	}

	@Test
	public void testGet_ReturnsFalse_whenMaterialNumberAreSpaces(){

		given(productFeatureModel.getValue()).willReturn("      ");

		Assert.assertFalse(testObj.get(productModel).booleanValue());
	}
}
