package com.waters.hybris.stepdefs.core.hooks;

import com.waters.hybris.stepdefs.core.helper.SessionCreatedModelsHelper;
import cucumber.api.java.After;
import de.hybris.platform.core.PK;
import de.hybris.platform.servicelayer.model.ModelService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static org.slf4j.LoggerFactory.getLogger;

public class DeleteCreatedModelsHook
{
	private static final Logger LOG = getLogger(DeleteCreatedModelsHook.class);

	private SessionCreatedModelsHelper sessionCreatedModelsHelper;
	private ModelService modelService;

	@After(order = 100)
	public void teardown()
	{
		final List<Long> pks = new LinkedList<>(getSessionCreatedModelsHelper().getPks());
		Collections.reverse(pks);
		pks.stream()
				.filter(Objects::nonNull)
				.peek(m -> LOG.debug("Removing [{}]", m))
				.map(PK::fromLong)
				.forEach(this::attemptRemove);
	}

	private void attemptRemove(final PK pk)
	{
		try
		{
			getModelService().remove(pk);
		}
		catch (final RuntimeException ex)
		{
			LOG.warn("Unable to remove model for PK '{}'", pk);
		}
	}

	protected SessionCreatedModelsHelper getSessionCreatedModelsHelper()
	{
		return sessionCreatedModelsHelper;
	}

	@Required
	public void setSessionCreatedModelsHelper(final SessionCreatedModelsHelper sessionCreatedModelsHelper)
	{
		this.sessionCreatedModelsHelper = sessionCreatedModelsHelper;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
