package com.waters.hybris.core.model;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.services.impl.DefaultProductNamingRuleCalculationService;
import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class DynamicProductAutomaticTitleAttributeHandlerTest {

    public static final String CALCULATED_NAME_A = "calculatedNameA";

    @Spy
    @InjectMocks
    private final DynamicProductAutomaticTitleAttributeHandler testObj = new DynamicProductAutomaticTitleAttributeHandler();

    @Mock
    private final DefaultProductNamingRuleCalculationService productNamingRuleCalculationService = new DefaultProductNamingRuleCalculationService();

    @Test
    public void testGet() {
        when(productNamingRuleCalculationService.calculateAutomaticName(Mockito.any())).thenReturn(CALCULATED_NAME_A);
        assertEquals(testObj.get(Mockito.any()), "calculatedNameA");
    }

    @Test
    public void testGetWithException() {
        doNothing().when(testObj).logError(Mockito.any(), Mockito.any(), Mockito.any());
        when(productNamingRuleCalculationService.calculateAutomaticName(Mockito.any())).thenThrow(new RuntimeException());
        assertEquals(testObj.get(Mockito.any()), StringUtils.EMPTY);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSet() {
        testObj.set(new WatersProductModel(), StringUtils.EMPTY);
    }
}
