package com.waters.hybris.stepdefs.core.helper;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.ItemModel;

public class SessionModelHelper extends SessionValueHelper
{
	public <T> T get(Class<T> type) {
		final Long pk = get(getKey(type));
		return pk == null ? null : getModelService().get(PK.fromLong(pk));
	}

	public void save(final ItemModel item) {
		set(getKey(item.getClass()), item.getPk().getLong());
	}
}
