package com.waters.hybris.stepdefs.core.helper;

import de.hybris.platform.core.model.ItemModel;
import org.slf4j.Logger;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import static org.slf4j.LoggerFactory.getLogger;

public class SessionCreatedModelsHelper extends SessionValueHelper
{
	private static final Logger LOG = getLogger(SessionCreatedModelsHelper.class);
	private static final String KEY = SessionCreatedModelsHelper.class.getName();

	public List<Long> getPks()
	{
		return getExistingOrDefault(Collections::emptyList);
	}

	public void save(final ItemModel model)
	{
		final List<Long> currentUnmodifiable = getExistingOrDefault(LinkedList::new);
		if (currentUnmodifiable.contains(model.getPk().getLong()))
		{
			LOG.debug("Already have saved reference to {}", model);
		}
		else
		{
			final List<Long> newList = new LinkedList<>(currentUnmodifiable);
			newList.add(model.getPk().getLong());
			set(KEY, newList);
			LOG.debug("saved reference to {}", model);
		}
	}

	private List<Long> getExistingOrDefault(final Supplier<List<Long>> supplier)
	{
		return Optional.<List<Long>>ofNullable(get(KEY)).orElseGet(supplier);
	}
}
