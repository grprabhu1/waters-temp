package com.waters.hybris.stepdefs.core.product.dto;

/**
 * Created by ramana on 10/04/2018.
 */
public class ProductDto
{
	private String id;
	private String catalog;
	private String catalogVersion;

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public String getCatalog()
	{
		return catalog;
	}

	public void setCatalog(final String catalog)
	{
		this.catalog = catalog;
	}

	public String getCatalogVersion()
	{
		return catalogVersion;
	}

	public void setCatalogVersion(final String catalogVersion)
	{
		this.catalogVersion = catalogVersion;
	}
}
