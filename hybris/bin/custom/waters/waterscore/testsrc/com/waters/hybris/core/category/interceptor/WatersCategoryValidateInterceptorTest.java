package com.waters.hybris.core.category.interceptor;

import com.waters.hybris.core.model.model.PunchoutCategoryModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WatersCategoryValidateInterceptorTest
{
	@InjectMocks
	private WatersCategoryValidateInterceptor interceptor = new WatersCategoryValidateInterceptor();

	private CategoryModel cat1 = new CategoryModel();

	private PunchoutCategoryModel pcat1 = new PunchoutCategoryModel();

	@Mock
	private InterceptorContext ctx;

	@Before
	public void setUp()
	{
	}

	@Test
	public void testSettingCategoryAsSubCategory()
	{
		final Map<String, Set<Locale>> modMap = createMap(Arrays.asList("categories"));
		when(ctx.getDirtyAttributes(cat1)).thenReturn(modMap);

		final CategoryModel category = new CategoryModel();
		final List<CategoryModel> subcategories = Stream.of(category).collect(Collectors.toCollection(ArrayList::new));
		cat1.setCategories(subcategories);

		try
		{
			interceptor.onValidate(cat1, ctx);
		}
		catch (final InterceptorException e)
		{
			fail("Exception should not be thrown when a normal category is set as sub category");
		}
	}

	@Test
	public void testSettingCategoryAsSuperCategory()
	{
		final Map<String, Set<Locale>> modMap = createMap(Arrays.asList("supercategories"));
		when(ctx.getDirtyAttributes(cat1)).thenReturn(modMap);

		final CategoryModel category = new CategoryModel();
		final List<CategoryModel> supercategories = Stream.of(category).collect(Collectors.toCollection(ArrayList::new));
		cat1.setSupercategories(supercategories);

		try
		{
			interceptor.onValidate(cat1, ctx);
		}
		catch (final InterceptorException e)
		{
			fail("Exception should not be thrown when a normal category is set as super category");
		}
	}

	@Test
	public void testSettingPunchoutCategoryAsSubCategory()
	{
		final Map<String, Set<Locale>> modMap = createMap(Arrays.asList("categories"));
		when(ctx.getDirtyAttributes(pcat1)).thenReturn(modMap);

		final PunchoutCategoryModel punchoutCategory = new PunchoutCategoryModel();
		final List<CategoryModel> subCategories = Stream.of(punchoutCategory).collect(Collectors.toCollection(ArrayList::new));
		pcat1.setCategories(subCategories);

		try
		{
			interceptor.onValidate(pcat1, ctx);
		}
		catch (final InterceptorException e)
		{
			fail("Exception should not be thrown when a punchout category is set as sub category");
		}
	}

	@Test
	public void testSettingPunchoutCategoryAsSuperCategory()
	{
		final Map<String, Set<Locale>> modMap = createMap(Arrays.asList("supercategories"));
		when(ctx.getDirtyAttributes(pcat1)).thenReturn(modMap);

		final PunchoutCategoryModel punchoutCategory = new PunchoutCategoryModel();
		final ArrayList<CategoryModel> superCategories = Stream.of(punchoutCategory).collect(Collectors.toCollection(ArrayList::new));
		pcat1.setSupercategories(superCategories);

		try
		{
			interceptor.onValidate(pcat1, ctx);
		}
		catch (final InterceptorException e)
		{
			fail("Exception should not be thrown when a normal category is set as super category");
		}
	}

	@Test
	public void testSettingPunchoutCategoryAsSubCategoryForNormalCategory()
	{
		final Map<String, Set<Locale>> modMap = createMap(Arrays.asList("categories"));
		when(ctx.getDirtyAttributes(cat1)).thenReturn(modMap);

		final PunchoutCategoryModel punchoutCategory = new PunchoutCategoryModel();
		final List<CategoryModel> subCategories = Stream.of(punchoutCategory).collect(Collectors.toCollection(ArrayList::new));
		cat1.setCategories(subCategories);

		try
		{
			interceptor.onValidate(cat1, ctx);
			fail("Exception should be thrown when sub category is not a normal category");
		}
		catch (final InterceptorException e)
		{
			// expected
		}
	}

	@Test
	public void testSettingPunchoutCategoryAsSuperCategoryForNormalCategory()
	{
		final Map<String, Set<Locale>> modMap = createMap(Arrays.asList("supercategories"));
		when(ctx.getDirtyAttributes(cat1)).thenReturn(modMap);

		final PunchoutCategoryModel punchoutCategory = new PunchoutCategoryModel();
		final List<CategoryModel> superCategories = Stream.of(punchoutCategory).collect(Collectors.toCollection(ArrayList::new));
		cat1.setSupercategories(superCategories);

		try
		{
			interceptor.onValidate(cat1, ctx);
			fail("Exception should be thrown when super category is not a normal category");
		}
		catch (final InterceptorException e)
		{
			// expected
		}
	}

	@Test
	public void testSettingCategoryAsSubCategoryForPunchoutCategory()
	{
		final Map<String, Set<Locale>> modMap = createMap(Arrays.asList("categories"));
		when(ctx.getDirtyAttributes(pcat1)).thenReturn(modMap);

		final CategoryModel category = new CategoryModel();
		final List<CategoryModel> subCategories = Stream.of(category).collect(Collectors.toCollection(ArrayList::new));
		pcat1.setCategories(subCategories);

		try
		{
			interceptor.onValidate(pcat1, ctx);
			fail("Exception should be thrown when sub category is not a punchout category");
		}
		catch (final InterceptorException e)
		{
			// expected
		}
	}

	@Test
	public void testSettingCategoryAsSuperCategoryForPunchoutCategory()
	{
		final Map<String, Set<Locale>> modMap = createMap(Arrays.asList("supercategories"));
		when(ctx.getDirtyAttributes(pcat1)).thenReturn(modMap);

		final CategoryModel category = new CategoryModel();
		final List<CategoryModel> superCategories = Stream.of(category).collect(Collectors.toCollection(ArrayList::new));
		pcat1.setSupercategories(superCategories);

		try
		{
			interceptor.onValidate(pcat1, ctx);
			fail("Exception should be thrown when super category is not a punchout category");
		}
		catch (final InterceptorException e)
		{
			// expected
		}
	}

	protected Map<String, Set<Locale>> createMap(final List<String> modifiedAttrs)
	{
		final Set<Locale> locales = new HashSet<>(Arrays.asList(Locale.ENGLISH, Locale.FRENCH));

		return modifiedAttrs.stream()
			.collect(Collectors.toMap(Function.identity(), e -> locales));
	}
}
