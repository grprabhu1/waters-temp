package com.waters.hybris.stepdefs.core.impex;

import cucumber.api.java.en.Given;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.impex.ImportResult;
import de.hybris.platform.servicelayer.impex.ImportService;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.IOException;
import java.io.InputStream;

public class ImpexStepDefs
{

	private static final Logger LOG = LoggerFactory.getLogger(ImpexStepDefs.class);
	private static final String fileEncoding = "UTF-8";
	private ImportService importService;

	@Given("^That the Impex \"([^\"]*)\" to \"([^\"]*)\" has been executed$")
	public void import_impex_file(final String impexName, final String ignore)
	{
		if(!importImpexFile(impexName)){
			throw new IllegalArgumentException("The impex "+impexName+" couldn't be imported");
		}
	}

	protected boolean importImpexFile(final String impexName)
	{
		try
		{
			try (final InputStream fis = this.getClass().getResourceAsStream("/test/" + impexName);)
			{
				final ImportConfig importConfig = new ImportConfig();
				importConfig.setScript(new StreamBasedImpExResource(fis, fileEncoding));
				importConfig.setLegacyMode(false);
				importConfig.setMaxThreads(2);

				final ImportResult result = getImportService().importData(importConfig);
				return result.isSuccessful();
			}


		}
		catch (final IOException ioEx)
		{
			LOG.error("IO Error while importing [" + impexName + "]", ioEx);
			return false;
		}
	}

	public ImportService getImportService()
	{
		return importService;
	}

	@Required
	public void setImportService(final ImportService importService)
	{
		this.importService = importService;
	}
}
