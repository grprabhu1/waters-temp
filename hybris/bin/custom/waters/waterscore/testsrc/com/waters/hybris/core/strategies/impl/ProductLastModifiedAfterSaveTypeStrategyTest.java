package com.waters.hybris.core.strategies.impl;

import com.waters.hybris.core.dao.SavedItemHistoryDao;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.services.AfterSaveListenerService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.hmc.model.SavedValuesModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.AfterSaveEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class ProductLastModifiedAfterSaveTypeStrategyTest {

    private final static String RUPRECHT = "Ruprecht";
    private final static String NOT_MOTHER = "Not Mother";

    private final static int PRODUCT_TYPE = 1;
    private PK pk = PK.createFixedCounterPK(PRODUCT_TYPE,1000);

    @Mock
    private ModelService modelService;

    @Mock
    private SavedItemHistoryDao savedItemHistoryDao;

    @Mock
    private AfterSaveListenerService afterSaveListenerService;

    @Mock
    private ItemModel item;

    @Mock
    private UserModel user;

    @Mock
    private ProductModel product;

    @Mock
    private WatersProductModel watersProduct;

    @Mock
    private SavedValuesModel savedValues;

    @Mock
    private AfterSaveEvent event;

    @Spy
    @InjectMocks
    private ProductLastModifiedAfterSaveTypeStrategy testObj = new ProductLastModifiedAfterSaveTypeStrategy();

    @Before
    public void setup() {
        doReturn(AfterSaveEvent.CREATE).when(event).getType();
        doReturn(pk).when(event).getPk();
        doReturn(item).when(modelService).get(pk);
        doReturn(Optional.of(savedValues)).when(savedItemHistoryDao).getLastSavedValue(watersProduct);
        doReturn(user).when(savedValues).getUser();
        doReturn(RUPRECHT).when(user).getName();
        doReturn(RUPRECHT).when(watersProduct).getLastEditedBy();
        doReturn(true).when(afterSaveListenerService).canProcess();
    }

    @Test
    public void givenANullPk_thenDoNothing() {
        testObj.process(null);

        verify(watersProduct, times(0)).setLastEditedBy(anyString());
        verify(modelService, times(0)).save(watersProduct);
    }


    @Test
    public void givenACanProcessFalse_thenDoNothing() {
        doReturn(true).when(afterSaveListenerService).canProcess();
        testObj.process(null);

        verify(watersProduct, times(0)).setLastEditedBy(anyString());
        verify(modelService, times(0)).save(watersProduct);
    }


    @Test
    public void givenARemoveEvent_thenDoNothing() {
        doReturn(AfterSaveEvent.REMOVE).when(event).getType();
        testObj.process(event);

        verify(modelService, times(0)).get(pk);
        verify(watersProduct, times(0)).setLastEditedBy(anyString());
        verify(modelService, times(0)).save(watersProduct);
    }

    @Test
    public void givenAnInvalidPk_thenDoNothing() {
        doReturn(null).when(modelService).get(pk);
        testObj.process(event);

        verify(watersProduct, times(0)).setLastEditedBy(anyString());
        verify(modelService, times(0)).save(watersProduct);
    }

    @Test
    public void givenAPkThatResolvesToProductModel_thenDoNothing() {


        doReturn(product).when(modelService).get(pk);
        testObj.process(event);

        verify(watersProduct, times(0)).setLastEditedBy(anyString());
        verify(modelService, times(0)).save(watersProduct);
    }

    @Test
    public void givenAPkThatResolvesToWatersProductModel_whenHistoryIsEmpty_thenDoNothing() {
        doReturn(Optional.empty()).when(savedItemHistoryDao).getLastSavedValue(watersProduct);
        doReturn(watersProduct).when(modelService).get(pk);
        testObj.process(event);

        verify(watersProduct, times(0)).setLastEditedBy(anyString());
        verify(modelService, times(0)).save(watersProduct);
    }


    @Test
    public void givenAPkThatResolvesToWatersProductModel_whenHistoryIsPopulated_andUserMatches_doNothing() {


        //productModel.getLastEditedBy(), userModel.getName()
        //         doReturn(RUPRECHT).when(user).getDisplayName();
        //        doReturn(RUPRECHT).when(watersProduct).getLastEditedBy();
        //        doReturn(pk).when(event).getPk();
        //          final UserModel userModel = savedValuesModel.getUser();
        //          doReturn(user).when(savedValues).getUser();

        doReturn(watersProduct).when(modelService).get(pk);
        testObj.process(event);
        verify(watersProduct, times(0)).setLastEditedBy(anyString());
        verify(modelService, times(0)).save(watersProduct);
    }

    @Test
    public void givenAPkThatResolvesToWatersProductModel_whenHistoryIsPopulated_andUserDoesntMatch_updateProduct() {
        doReturn(watersProduct).when(modelService).get(pk);
        doReturn(NOT_MOTHER).when(user).getName();
        testObj.process(event);
        verify(watersProduct, times(1)).setLastEditedBy(NOT_MOTHER);
        verify(modelService, times(1)).save(watersProduct);
    }


}
