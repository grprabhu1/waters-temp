package com.waters.hybris.stepdefs.core.helper;

import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


public class SessionCronjobHelper extends SessionValueHelper
{
	private static final String ATTRIBUTE = "tstCronjob";

	private CronJobService cronJobService;

	public CronJobModel getJob()
	{
		final String code = get(ATTRIBUTE);
		return getCronJobService().getCronJob(code);
	}

	public void save(final String code)
	{
		Assert.notNull(code, "Cronjob code cannot be null");
		set(ATTRIBUTE, code);
	}

	protected CronJobService getCronJobService() {
		return cronJobService;
	}

	@Required
	public void setCronJobService(final CronJobService cronJobService)
	{
		this.cronJobService = cronJobService;
	}
}
