package com.waters.hybris.core.services.impl;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.services.ProductNamingRuleResolver;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.core.PK;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import java.util.*;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.slf4j.LoggerFactory.getLogger;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class FiltersProductNamingRuleCalculationServiceTest {
    private static final String FILTERS_NAMING_RULE_TO_TEST = "(feature('Brand') != null ? feature('Brand') : '')+(feature('FilterFormat') != null ? (feature('Brand') != null ? ', ' : '')+feature('FilterFormat') : '')+(feature('FilterHousingMaterial') != null ? (feature('Brand') != null || feature('FilterFormat') != null ? ', ' : '')+feature('FilterHousingMaterial') : '')+(feature('FilterDiameter') != null ? ((feature('Brand') != null || feature('FilterFormat') != null || feature('FilterHousingMaterial') != null ? ', ' : '') + feature('FilterDiameter') + ' ' + unit('FilterDiameter')) : '')+(feature('FilterPoreSize') != null ? ((feature('Brand') != null || feature('FilterFormat') != null || feature('FilterHousingMaterial') != null || feature('FilterDiameter') != null ? ', ' : '') + feature('FilterPoreSize') + ' ' + unit('FilterPoreSize')) : '')+(feature('FilterSeparationMode') != null ? ((feature('Brand') != null || feature('FilterFormat') != null || feature('FilterHousingMaterial') != null || feature('FilterDiameter') != null || feature('FilterPoreSize') != null ? ', ' : '') + feature('FilterSeparationMode')) : '')+(feature('UnitsPerPackage') != null ? ((feature('Brand') != null || feature('FilterFormat') != null || feature('FilterHousingMaterial') != null || feature('FilterDiameter') != null || feature('FilterPoreSize') != null || feature('FilterSeparationMode') != null ? ', ' : '') + feature('UnitsPerPackage') + '/' + unit('UnitsPerPackage')) : '')";
    private static final String METRIC_FIELDS = "ColumnMolecularWeightRangeMin,ColumnMolecularWeightRangeMax";
    private static final String FILTERS_COLUMN = "Filters";
    public static final String CODE = "CODE";
    public static final String BROKEN_FEATURE = "BROKEN FEATURE";
    public static final PK BROKEN_ATTRIBUTE_ASSIGNMENT_PK = PK.NULL_PK;

    @Mock
    private final WatersProductModel watersProductModel = new WatersProductModel();
    @Mock
    private final ClassificationClassModel classificationClassModel = new ClassificationClassModel();

    @Mock
    private final ProductFeatureModel brand = new ProductFeatureModel();
    @Mock
    private final ClassificationAttributeModel brandAttributeModel = new ClassificationAttributeModel();
    @Mock
    private final ClassAttributeAssignmentModel brandAttributeAssignmentModel = new ClassAttributeAssignmentModel();

    @Mock
    private final ProductFeatureModel filterFormat = new ProductFeatureModel();
    @Mock
    private final ClassificationAttributeModel filterFormatAttributeModel = new ClassificationAttributeModel();
    @Mock
    private final ClassAttributeAssignmentModel filterFormatAttributeAssignmentModel = new ClassAttributeAssignmentModel();

    @Mock
    private final ProductFeatureModel filterHousingMaterial = new ProductFeatureModel();
    @Mock
    private final ClassificationAttributeModel filterHousingMaterialAttributeModel = new ClassificationAttributeModel();
    @Mock
    private final ClassAttributeAssignmentModel filterHousingMaterialAttributeAssignmentModel = new ClassAttributeAssignmentModel();

    @Mock
    private final ProductFeatureModel filterDiameter = new ProductFeatureModel();
    @Mock
    private final ClassificationAttributeModel filterDiameterAttributeModel = new ClassificationAttributeModel();
    @Mock
    private final ClassAttributeAssignmentModel filterDiameterAttributeAssignmentModel = new ClassAttributeAssignmentModel();
    @Mock
    private final ClassificationAttributeUnitModel filterDiameterUnitModel = new ClassificationAttributeUnitModel();

    @Mock
    private final ProductFeatureModel poreSize = new ProductFeatureModel();
    @Mock
    private final ClassificationAttributeModel poreSizeAttributeModel = new ClassificationAttributeModel();
    @Mock
    private final ClassificationAttributeUnitModel poreSizeUnitModel = new ClassificationAttributeUnitModel();
    @Mock
    private final ClassAttributeAssignmentModel poreSizeAttributeAssignmentModel = new ClassAttributeAssignmentModel();

    @Mock
    private final ProductFeatureModel filterSeperationMode = new ProductFeatureModel();
    @Mock
    private final ClassificationAttributeModel filterSeperationModeAttributeModel = new ClassificationAttributeModel();
    @Mock
    private final ClassAttributeAssignmentModel filterSeperationModeAttributeAssignmentModel = new ClassAttributeAssignmentModel();

    @Mock
    private final ProductFeatureModel unitsPerPackage = new ProductFeatureModel();
    @Mock
    private final ClassificationAttributeModel unitsPerPackageModel = new ClassificationAttributeModel();
    @Mock
    private final ClassAttributeAssignmentModel unitsPerPackageAttributeAssignmentModel = new ClassAttributeAssignmentModel();
    @Mock
    private final ClassificationAttributeUnitModel unitsPerPackageUnitModel = new ClassificationAttributeUnitModel();

    @Mock
    private ProductFeatureModel brokenFeature;

    @Mock
    private ClassAttributeAssignmentModel brokenClassAttributeAssignmentModel;

    @Spy
    @InjectMocks
    private final DefaultProductNamingRuleCalculationService testObj = new DefaultProductNamingRuleCalculationService();

    private final List<ProductFeatureModel> productFeaturesDataSetWithAllFeatures = new ArrayList<>();
    private final List<ProductFeatureModel> productFeaturesDataSetWithUomNotSet = new ArrayList<>();
    private final List<ProductFeatureModel> productFeaturesDataSetWithOneFeatureMissing = new ArrayList<>();


    private final Map<String, ProductNamingRuleResolver> productNamingRuleResolverMap = new HashMap<>();

    private static final Logger LOG = getLogger(DefaultProductNamingRuleCalculationService.class);



    @Before
    public void setup() {

        when(classificationClassModel.getCode()).thenReturn(FILTERS_COLUMN);
        final List<ClassificationClassModel> classificationClasses = new ArrayList<>();
        classificationClasses.add(classificationClassModel);

        when(watersProductModel.getClassificationClasses()).thenReturn(classificationClasses);

        setupProductFeatures();
        doReturn(METRIC_FIELDS).when(testObj).getMetricFields();
        doReturn(FILTERS_NAMING_RULE_TO_TEST).when(testObj).getProductNamingRule(Mockito.any());

        doReturn(CODE).when(watersProductModel).getCode();

        doReturn(BROKEN_FEATURE).when(brokenFeature).getQualifier();
        doReturn(BROKEN_ATTRIBUTE_ASSIGNMENT_PK).when(brokenClassAttributeAssignmentModel).getPk();
    }

    /**
     * Test calculated automatic name when all features are set.
     */
    @Test
    public void checkAutoNameWhenAllFeaturesSet() {
        when(watersProductModel.getFeatures()).thenReturn(productFeaturesDataSetWithAllFeatures);
        final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
        final String expectedAutoName = "Acrodisc, Minispike Syringe Filter, Nylon, 13 mm, 0.45 µm, Aqueous, 300/case";
        assertEquals(calculatedAutoName, expectedAutoName);
    }

    /**
     * Test calculated automatic name when one feature having unit is set with feature value only and no value for it's unit.
     * Check that such feature is excluded from the calculation due to missing unit.
     */
    @Test
    public void checkAutoNameWhenUomNotSet() {
        when(watersProductModel.getFeatures()).thenReturn(productFeaturesDataSetWithUomNotSet);
        final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
        final String expectedAutoName = "Acrodisc, Minispike Syringe Filter, Nylon, 0.45 µm, Aqueous, 300/case";
        assertEquals(calculatedAutoName, expectedAutoName);
    }

    /**
     * Test calculated automatic name when one of the feature involved in calculation is not set.
     * Check that such feature is excluded from the calculation and double commas (,,) do not appear due to missing feature value.
     */
    @Test
    public void checkAutoNameWhenOneFeatureNotSet() {
        when(watersProductModel.getFeatures()).thenReturn(productFeaturesDataSetWithOneFeatureMissing);
        final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
        final String expectedAutoName = "Acrodisc, Nylon, 13 mm, 0.45 µm, Aqueous, 300/case";
        assertEquals(calculatedAutoName, expectedAutoName);
    }

    @Test
    public void givenAFeature_thatHasAClassificationAssignment_AndClassificationAttributeIsNull_thenLogError() {

        doReturn(singletonList(brokenFeature)).when(watersProductModel).getFeatures();
        doReturn(brokenClassAttributeAssignmentModel).when(brokenFeature).getClassificationAttributeAssignment();

        final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
        final String expectedAutoName = "";
        assertEquals(calculatedAutoName, expectedAutoName);
        verify(testObj, times(1))
                .logError(DefaultProductNamingRuleCalculationService.CLASSIFICATION_ATTRIBUTE_IS_NULL_FOR_PRODUCT_FEATURE_AND_CLASSIFICATION_ATTRIBUTE_ASSIGNMENT, CODE, BROKEN_FEATURE, BROKEN_ATTRIBUTE_ASSIGNMENT_PK.toString());
    }

    @Test
    public void givenAFeature_thatHasNoClassificationAssignment_thenLogError() {

        when(watersProductModel.getFeatures()).thenReturn(singletonList(brokenFeature));
        final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
        final String expectedAutoName = "";
        assertEquals(calculatedAutoName, expectedAutoName);
        verify(testObj, times(1))
                .logError(DefaultProductNamingRuleCalculationService.CLASSIFICATION_ATTRIBUTE_ASSIGNMENT_IS_NULL_FOR_PRODUCT_FEATURE, CODE, BROKEN_FEATURE);
    }

    /**
     * Set up product features with values and units.
     */
    private void setupProductFeatures() {
        setupProductFeature(brand, brandAttributeAssignmentModel, brandAttributeModel, "Brand", "Acrodisc");
        productFeaturesDataSetWithAllFeatures.add(brand);
        productFeaturesDataSetWithUomNotSet.add(brand);
        productFeaturesDataSetWithOneFeatureMissing.add(brand);

        setupProductFeature(filterFormat, filterFormatAttributeAssignmentModel, filterFormatAttributeModel, "FilterFormat", "Minispike Syringe Filter");
        productFeaturesDataSetWithAllFeatures.add(filterFormat);
        productFeaturesDataSetWithUomNotSet.add(filterFormat);

        setupProductFeature(filterHousingMaterial, filterHousingMaterialAttributeAssignmentModel, filterHousingMaterialAttributeModel, "FilterHousingMaterial", "Nylon");
        productFeaturesDataSetWithAllFeatures.add(filterHousingMaterial);
        productFeaturesDataSetWithUomNotSet.add(filterHousingMaterial);
        productFeaturesDataSetWithOneFeatureMissing.add(filterHousingMaterial);

        setupProductFeatureWithUnit(filterDiameter, filterDiameterAttributeAssignmentModel, filterDiameterAttributeModel, filterDiameterUnitModel, "mm", "FilterDiameter", "13");
        productFeaturesDataSetWithAllFeatures.add(filterDiameter);
        productFeaturesDataSetWithOneFeatureMissing.add(filterDiameter);

        setupProductFeatureWithUnit(poreSize, poreSizeAttributeAssignmentModel, poreSizeAttributeModel, poreSizeUnitModel, "µm", "FilterPoreSize", "0.45");
        productFeaturesDataSetWithAllFeatures.add(poreSize);
        productFeaturesDataSetWithUomNotSet.add(poreSize);
        productFeaturesDataSetWithOneFeatureMissing.add(poreSize);

        setupProductFeature(filterSeperationMode, filterSeperationModeAttributeAssignmentModel, filterSeperationModeAttributeModel, "FilterSeparationMode", "Aqueous");
        productFeaturesDataSetWithAllFeatures.add(filterSeperationMode);
        productFeaturesDataSetWithUomNotSet.add(filterSeperationMode);
        productFeaturesDataSetWithOneFeatureMissing.add(filterSeperationMode);

        setupProductFeatureWithUnit(unitsPerPackage, unitsPerPackageAttributeAssignmentModel, unitsPerPackageModel, unitsPerPackageUnitModel, "case", "UnitsPerPackage", "300");
        productFeaturesDataSetWithAllFeatures.add(unitsPerPackage);
        productFeaturesDataSetWithUomNotSet.add(unitsPerPackage);
        productFeaturesDataSetWithOneFeatureMissing.add(unitsPerPackage);
    }

    private void setupProductFeature(final ProductFeatureModel productFeatureModel,
                                     final ClassAttributeAssignmentModel classAttributeAssignmentModel,
                                     final ClassificationAttributeModel classificationAttributeModel,
                                     final String featureCode, final String featureValue) {
        when(productFeatureModel.getClassificationAttributeAssignment()).thenReturn(classAttributeAssignmentModel);
        when(classAttributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
        when(classificationAttributeModel.getCode()).thenReturn(featureCode);
        when(productFeatureModel.getValue()).thenReturn(featureValue);
    }

    private void setupProductFeatureWithUnit(final ProductFeatureModel productFeatureModel,
                                             final ClassAttributeAssignmentModel classAttributeAssignmentModel,
                                             final ClassificationAttributeModel classificationAttributeModel,
                                             final ClassificationAttributeUnitModel classificationAttributeUnitModel,
                                             final String unitSymbol,
                                             final String featureCode, final String featureValue) {
        when(productFeatureModel.getClassificationAttributeAssignment()).thenReturn(classAttributeAssignmentModel);
        when(classificationAttributeUnitModel.getSymbol()).thenReturn(unitSymbol);
        when(classAttributeAssignmentModel.getUnit()).thenReturn(classificationAttributeUnitModel);
        when(classAttributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
        when(productFeatureModel.getUnit()).thenReturn(classificationAttributeUnitModel);
        when(classificationAttributeModel.getCode()).thenReturn(featureCode);
        when(productFeatureModel.getValue()).thenReturn(featureValue);
    }
}
