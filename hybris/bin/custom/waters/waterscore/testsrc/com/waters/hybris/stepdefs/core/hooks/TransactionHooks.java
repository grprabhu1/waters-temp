package com.waters.hybris.stepdefs.core.hooks;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class TransactionHooks
{
	private PlatformTransactionManager platformTransactionManager;
	private TransactionStatus txnStatus;

	@Before(value = "@Transaction", order = 1)
	public void setup()
	{
		txnStatus = getPlatformTransactionManager().getTransaction(new DefaultTransactionDefinition());
	}

	@After(value = "@Transaction", order = 1)
	public void teardown()
	{
		getPlatformTransactionManager().rollback(txnStatus);
	}

	protected PlatformTransactionManager getPlatformTransactionManager()
	{
		return platformTransactionManager;
	}

	@Required
	public void setPlatformTransactionManager(final PlatformTransactionManager platformTransactionManager)
	{
		this.platformTransactionManager = platformTransactionManager;
	}
}
