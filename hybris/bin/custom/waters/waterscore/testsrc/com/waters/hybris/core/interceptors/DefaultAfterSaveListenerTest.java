package com.waters.hybris.core.interceptors;

import com.waters.hybris.core.strategies.AfterSaveTypeStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.tx.AfterSaveEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class DefaultAfterSaveListenerTest {

    private static final Object NON_MATCHING_TYPE = 0;
    private static final String PRODUCT_TYPE_CODE = "Product";
    private static final Integer PRODUCT_TYPE = 1;

    @Mock
    private Tenant tenant;



    @Mock
    private AfterSaveEvent event;

    @Mock
    private AfterSaveTypeStrategy afterSaveTypeStrategy;

    private final PK pk = PK.createFixedCounterPK(DefaultAfterSaveListenerTest.PRODUCT_TYPE,1000);

    @InjectMocks
    @Spy
    final DefaultAfterSaveListener testObj = new DefaultAfterSaveListener();

    @Before
    public void setup()
    {
        final Map<String, AfterSaveTypeStrategy> strategyMap = new HashMap<>();

        strategyMap.put(DefaultAfterSaveListenerTest.PRODUCT_TYPE_CODE, afterSaveTypeStrategy);
        testObj.setStrategyMap(strategyMap);


        doReturn(DefaultAfterSaveListenerTest.PRODUCT_TYPE).when(testObj).getTypeCode(DefaultAfterSaveListenerTest.PRODUCT_TYPE_CODE);

        // process strategy
        testObj.afterTenantStartUp(tenant);
    }

    @Test
    public void givenNoConfiguration_thenAfterSave_doesNothing() {
        testObj.setStrategyMap(null);

        // wipe strategy map
        testObj.afterTenantStartUp(tenant);
        testObj.afterSave(null);


    }


    @Test
    public void givenTheStrategyMapIsPopulated_thenTheProcessedStrategyMapShouldBePopulated() {

        final Map<Integer, AfterSaveTypeStrategy> processedStrategyMap = testObj.getProcessedStrategyMap();

        assertThat(processedStrategyMap).hasSize(1);
        assertThat(processedStrategyMap.get(DefaultAfterSaveListenerTest.PRODUCT_TYPE)).isEqualTo(afterSaveTypeStrategy);
    }

    @Test
    public void givenAnEmptyCollection_thenAfterSave_doesNothing() {
        testObj.afterSave(Collections.emptyList());
    }

    @Test
    public void givenAnEventThatIsNotMatched_thenAfterSave_doesNothing() {
        doReturn(pk).when(event).getPk();
        doReturn(DefaultAfterSaveListenerTest.NON_MATCHING_TYPE).when(event).getType();
        testObj.afterSave(asList(event));
    }

    @Test
    public void givenAnEventThatMatched_thenAfterSaveInvokesStrategy() {

        doReturn(pk).when(event).getPk();
        doReturn(DefaultAfterSaveListenerTest.PRODUCT_TYPE).when(event).getType();
        testObj.afterSave(asList(event));

        verify(afterSaveTypeStrategy).process(event);
    }
}