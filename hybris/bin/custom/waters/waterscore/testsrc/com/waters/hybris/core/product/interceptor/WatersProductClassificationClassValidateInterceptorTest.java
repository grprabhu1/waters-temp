package com.waters.hybris.core.product.interceptor;

import com.waters.hybris.core.model.model.WatersProductModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WatersProductClassificationClassValidateInterceptorTest
{

	@InjectMocks
	private final ValidateInterceptor<WatersProductModel> interceptor = new WatersProductClassificationClassValidateInterceptor();

	@Mock
	private CategoryService categoryService;

	@Mock
	private WatersProductModel watersProductModel;

	@Mock
	private InterceptorContext interceptorContext;

	@Mock
	private ClassificationClassModel root;

	@Mock
	private ClassificationClassModel parent;

	@Mock
	private ClassificationClassModel leaf;


	@Before
	public void setUp()
	{
		given(interceptorContext.getDirtyAttributes(watersProductModel)).willReturn(Collections.singletonMap(WatersProductModel.SUPERCATEGORIES, Collections.emptySet()));

		given(categoryService.getAllSupercategoriesForCategory(leaf)).willReturn(Lists.newArrayList(parent, root));
		given(categoryService.getAllSupercategoriesForCategory(parent)).willReturn(Collections.singletonList(root));
		given(categoryService.getAllSupercategoriesForCategory(root)).willReturn(Collections.emptyList());

		given(root.getCode()).willReturn("Root");
		given(parent.getCode()).willReturn("Parent");
		given(leaf.getCode()).willReturn("leaf");
	}


	@Test
	public void test_ValidateDoesNOTFail_WhenOnlyRootCategory() throws InterceptorException
	{
		given(watersProductModel.getSupercategories()).willReturn(Collections.singletonList(root));
		interceptor.onValidate(watersProductModel, interceptorContext);
	}

	@Test
	public void test_ValidateDoesNOTFail_WhenOnlyMidCategory() throws InterceptorException
	{
		given(watersProductModel.getSupercategories()).willReturn(Collections.singletonList(parent));
		interceptor.onValidate(watersProductModel, interceptorContext);
	}

	@Test
	public void test_ValidateDoesNOTFail_WhenOnlyLeafCategory() throws InterceptorException
	{
		given(watersProductModel.getSupercategories()).willReturn(Collections.singletonList(leaf));
		interceptor.onValidate(watersProductModel, interceptorContext);
	}

	@Test
	public void test_ValidateDoesNOTFail_WhenAllTheCategoriesBelongToSameBranch() throws InterceptorException
	{
		given(watersProductModel.getSupercategories()).willReturn(Lists.newArrayList(root, parent, leaf));
		interceptor.onValidate(watersProductModel, interceptorContext);
	}

	@Test
	public void test_ValidateFails_WhenTwoCategoriesInDifferentBranches() throws InterceptorException
	{

		final ClassificationClassModel leaf2 = mock(ClassificationClassModel.class);
		given(leaf2.getCode()).willReturn("leaf2");
		given(leaf2.getSupercategories()).willReturn(Collections.singletonList(parent));

		given(watersProductModel.getSupercategories()).willReturn(Lists.newArrayList(root, parent, leaf, leaf2));
		try
		{
			interceptor.onValidate(watersProductModel, interceptorContext);
			Assert.fail("InterceptorException expected");
		}
		catch (final InterceptorException e)
		{
			Assert.assertEquals("[null]:The product can NOT be assigned to two different classification class branches: leaf2 and leaf", e.getMessage());
		}
	}

	@Test
	public void test_ValidateDoesNOTFail_WhenOtherTypesOfCategories() throws InterceptorException
	{

		final CategoryModel leaf2 = mock(CategoryModel.class);
		given(leaf2.getSupercategories()).willReturn(Collections.singletonList(parent));

		given(watersProductModel.getSupercategories()).willReturn(Lists.newArrayList(root, parent, leaf, leaf2));
		interceptor.onValidate(watersProductModel, interceptorContext);
	}

	@Test
	public void test_ValidateDoesNOTFail_WhenAllTheCategoriesBelongToSameBranch_AndRootReturnsNull() throws InterceptorException
	{
		given(categoryService.getAllSupercategoriesForCategory(root)).willReturn(null);

		given(watersProductModel.getSupercategories()).willReturn(Lists.newArrayList(root, parent, leaf));
		interceptor.onValidate(watersProductModel, interceptorContext);
	}
}
