package com.waters.hybris.core.services.impl;

import com.waters.hybris.core.model.model.WatersProductModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class ColumnProductNamingRuleCalculationServiceTest
{
	private static final String COLUMN_NAMING_RULE_TO_TEST = "(feature('Brand') != null ? feature('Brand') : '')+(feature('ColumnQCTested') != null ? ' '+feature('ColumnQCTested') : '')+(feature('ColumnParticleTechnology') != null ? ' '+feature('ColumnParticleTechnology') : '')+(feature('ColumnBondingTechnology') != null ? (' ' + feature('ColumnBondingTechnology')) : (feature('ColumnChemistry') != null ? (' ' + feature('ColumnChemistry')) : ''))+(feature('ColumnFormat') != null ? ' ' + feature('ColumnFormat') : '') +(feature('ColumnPoreSize') != null ? (', ' + feature('ColumnPoreSize') + unit('ColumnPoreSize')) : '')+(feature('ColumnParticleSize') != null ? (', ' + feature('ColumnParticleSize') + ' ' + unit('ColumnParticleSize')) : '')+(feature('ColumnInnerDiameter') != null ? (', ' + feature('ColumnInnerDiameter') + ' ' + unit('ColumnInnerDiameter')) : '')+(' X ')+(feature('ColumnLength') != null ? (feature('ColumnLength') + ' ' + unit('ColumnLength')) : '')+(feature('ColumnMolecularWeightRangeMin') != null ? (', ' + feature('ColumnMolecularWeightRangeMin')) : '')+(feature('ColumnMolecularWeightRangeMax') != null ? (' - ' + feature('ColumnMolecularWeightRangeMax')) : '')+(feature('UnitsPerPackage') != null ? (', ' + feature('UnitsPerPackage') + '/' + unit('UnitsPerPackage')) : '')";
	private static final String METRIC_FIELDS = "ColumnMolecularWeightRangeMin,ColumnMolecularWeightRangeMax";
	private static final String CATEGORY_COLUMN = "Column";
	private static final String CATEGORY_VIAL = "Vial";
	private static final String CATEGORY_CONSUMABLES_AND_SPARES = "ConsumablesAndSpares";

	@Mock
	private final WatersProductModel watersProductModel = new WatersProductModel();
	@Mock
	private final ClassificationClassModel classificationClassModel = new ClassificationClassModel();
	@Mock
	private final ClassificationClassModel classificationClassModel2 = new ClassificationClassModel();
	@Mock
	private final ClassificationClassModel classificationClassModel3 = new ClassificationClassModel();

	@Mock
	private final ProductFeatureModel brand = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel brandAttributeModel = new ClassificationAttributeModel();
	@Mock
	private final ClassAttributeAssignmentModel brandAttributeAssignmentModel = new ClassAttributeAssignmentModel();
	@Mock
	private final ClassificationAttributeValueModel brandAttributeValueModel = new ClassificationAttributeValueModel();

	@Mock
	private final ProductFeatureModel qcTested = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel qcTestedAttributeModel = new ClassificationAttributeModel();
	@Mock
	private final ClassAttributeAssignmentModel qcTestedAttributeAssignmentModel = new ClassAttributeAssignmentModel();

	@Mock
	private final ProductFeatureModel particleTechnology = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel particleTechnologyAttributeModel = new ClassificationAttributeModel();
	@Mock
	private final ClassAttributeAssignmentModel particleTechnologyAttributeAssignmentModel = new ClassAttributeAssignmentModel();

	@Mock
	private final ProductFeatureModel bondingTechnology = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel bondingTechnologyAttributeModel = new ClassificationAttributeModel();
	@Mock
	private final ClassAttributeAssignmentModel bondingTechnologyAttributeAssignmentModel = new ClassAttributeAssignmentModel();

	@Mock
	private final ProductFeatureModel chemistry = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel chemistryAttributeModel = new ClassificationAttributeModel();
	@Mock
	private final ClassAttributeAssignmentModel chemistryAttributeAssignmentModel = new ClassAttributeAssignmentModel();

	@Mock
	private final ProductFeatureModel format = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel formatAttributeModel = new ClassificationAttributeModel();
	@Mock
	private final ClassAttributeAssignmentModel formatAttributeAssignmentModel = new ClassAttributeAssignmentModel();

	@Mock
	private final ProductFeatureModel poreSize = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel poreSizeAttributeModel = new ClassificationAttributeModel();
	@Mock
	private final ClassificationAttributeUnitModel poreSizeUnitModel = new ClassificationAttributeUnitModel();
	@Mock
	private final ClassAttributeAssignmentModel poreSizeAttributeAssignmentModel = new ClassAttributeAssignmentModel();

	@Mock
	private final ProductFeatureModel particleSize = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel particleSizeAttributeModel = new ClassificationAttributeModel();
	@Mock
	private final ClassificationAttributeUnitModel particleSizeUnitModel = new ClassificationAttributeUnitModel();
	@Mock
	private final ClassAttributeAssignmentModel particleSizeAttributeAssignmentModel = new ClassAttributeAssignmentModel();

	@Mock
	private final ProductFeatureModel innerDiameter = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel innerDiameterAttributeModel = new ClassificationAttributeModel();
	@Mock
	private final ClassAttributeAssignmentModel innerDiameterAttributeAssignmentModel = new ClassAttributeAssignmentModel();
	@Mock
	private final ClassificationAttributeUnitModel innerDiameterUnitModel = new ClassificationAttributeUnitModel();

	@Mock
	private final ProductFeatureModel length = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel lengthAttributeModel = new ClassificationAttributeModel();
	@Mock
	private final ClassAttributeAssignmentModel lengthAttributeAssignmentModel = new ClassAttributeAssignmentModel();
	@Mock
	private final ClassificationAttributeUnitModel lengthUnitModel = new ClassificationAttributeUnitModel();

	@Mock
	private final ProductFeatureModel molecularWeightRangeMin = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel molecularWeightRangeMinModel = new ClassificationAttributeModel();
	@Mock
	private final ClassAttributeAssignmentModel molecularWeightRangeMinAttributeAssignmentModel = new ClassAttributeAssignmentModel();

	@Mock
	private final ProductFeatureModel molecularWeightRangeMax = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel molecularWeightRangeMaxModel = new ClassificationAttributeModel();
	@Mock
	private final ClassAttributeAssignmentModel molecularWeightRangeMaxAttributeAssignmentModel = new ClassAttributeAssignmentModel();

	@Mock
	private final ProductFeatureModel molecularWeightRangeAboveThousandMax = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel molecularWeightRangeAboveThousandMaxModel = new ClassificationAttributeModel();
	@Mock
	private final ClassAttributeAssignmentModel molecularWeightRangeAboveThousandMaxAttributeAssignmentModel = new ClassAttributeAssignmentModel();

	@Mock
	private final ProductFeatureModel molecularWeightRangeAboveMillionMax = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel molecularWeightRangeAboveMillionMaxModel = new ClassificationAttributeModel();
	@Mock
	private final ClassAttributeAssignmentModel molecularWeightRangeAboveMillionMaxAttributeAssignmentModel = new ClassAttributeAssignmentModel();

	@Mock
	private final ProductFeatureModel unitsPerPackage = new ProductFeatureModel();
	@Mock
	private final ClassificationAttributeModel unitsPerPackageModel = new ClassificationAttributeModel();
	@Mock
	private final ClassAttributeAssignmentModel unitsPerPackageAttributeAssignmentModel = new ClassAttributeAssignmentModel();
	@Mock
	private final ClassificationAttributeUnitModel unitsPerPackageUnitModel = new ClassificationAttributeUnitModel();

	@Spy
	@InjectMocks
	private final DefaultProductNamingRuleCalculationService testObj = new DefaultProductNamingRuleCalculationService();

	private final List<ProductFeatureModel> productFeaturesDataSetWithAllFeatures = new ArrayList<>();
	private final List<ProductFeatureModel> productFeaturesDataSetWithUomNotSet = new ArrayList<>();
	private final List<ProductFeatureModel> productFeaturesDataSetWithOneFeatureMissing = new ArrayList<>();
	private final List<ProductFeatureModel> productFeaturesDataSetWithSecondPriorityField = new ArrayList<>();
	private final List<ProductFeatureModel> productFeaturesDataSetWithPriorityFieldsMissing = new ArrayList<>();
	private final List<ProductFeatureModel> productFeaturesDataSetWithMetricAboveThousand = new ArrayList<>();
	private final List<ProductFeatureModel> productFeaturesDataSetWithMetricAboveMillion = new ArrayList<>();
	private final List<ProductFeatureModel> productFeaturesDataSetWithMetricNotSet = new ArrayList<>();

	@Before
	public void setup()
	{
		when(classificationClassModel.getCode()).thenReturn(CATEGORY_COLUMN);
		when(classificationClassModel2.getCode()).thenReturn(CATEGORY_VIAL);
		when(classificationClassModel3.getCode()).thenReturn(CATEGORY_CONSUMABLES_AND_SPARES);

		final List<ClassificationClassModel> classificationClasses = new ArrayList<>();
		classificationClasses.add(classificationClassModel);
		when(watersProductModel.getClassificationClasses()).thenReturn(classificationClasses);
		setupProductFeatures();

		doReturn(METRIC_FIELDS).when(testObj).getMetricFields();
	}

	/**
	 * Test calculated automatic name when product is unclassified.
	 */
	@Test
	public void checkAutoNameWhenProductIsUnclassified()
	{
		doReturn(COLUMN_NAMING_RULE_TO_TEST).when(testObj).getProductNamingRule(Mockito.any());
		doReturn(Collections.EMPTY_LIST).when(testObj).getProductClassificationHierarchy(Mockito.any());
		when(watersProductModel.getFeatures()).thenReturn(new ArrayList<>());
		final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
		final String expectedAutoName = StringUtils.EMPTY;
		assertEquals(calculatedAutoName, expectedAutoName);
	}

	/**
	 * Test calculated automatic name when naming rule is not configured.
	 */
	@Test
	public void checkAutoNameWhenNoNamingRule()
	{
		doReturn(StringUtils.EMPTY).when(testObj).getProductNamingRule(Mockito.any());
		when(watersProductModel.getFeatures()).thenReturn(productFeaturesDataSetWithAllFeatures);
		final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
		final String expectedAutoName = StringUtils.EMPTY;
		assertEquals(calculatedAutoName, expectedAutoName);
	}

	/**
	 * Test calculated automatic name when product belongs to multiple classification hierarchy.
	 */
	@Test
	public void checkAutoNameWhenProductContainsMultipleClassificationHierarchy()
	{
		when(watersProductModel.getClassificationClasses()).thenReturn(Arrays.asList(classificationClassModel, classificationClassModel2));
		final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
		final String expectedAutoName = StringUtils.EMPTY;
		assertEquals(calculatedAutoName, expectedAutoName);
	}

	@Test
	public void checkClassificationHierarchyWhenContainsMultipleCategoriesOfSameHierarchy()
	{
		when(classificationClassModel.getAllSubcategories()).thenReturn(Arrays.asList(classificationClassModel3));
		when(watersProductModel.getClassificationClasses()).thenReturn(Arrays.asList(classificationClassModel, classificationClassModel3));
		final List<String> productClassificationHierarchy = testObj.getProductClassificationHierarchy(watersProductModel);
		assertEquals(productClassificationHierarchy, Arrays.asList(classificationClassModel3.getCode()));
	}

	@Test
	public void checkClassificationHierarchyWhenContainsMultipleCategoriesOfDifferentHierarchy()
	{
		when(classificationClassModel.getAllSubcategories()).thenReturn(Arrays.asList(classificationClassModel3, classificationClassModel2));
		when(watersProductModel.getClassificationClasses()).thenReturn(Arrays.asList(classificationClassModel, classificationClassModel3, classificationClassModel2));
		final List<String> productClassificationHierarchy = testObj.getProductClassificationHierarchy(watersProductModel);
		assertEquals(productClassificationHierarchy, Arrays.asList(classificationClassModel3.getCode(), classificationClassModel2.getCode()));
	}

	/**
	 * Test calculated automatic name when all features are set.
	 */
	@Test
	public void checkAutoNameWhenAllFeaturesSet()
	{
		doReturn(COLUMN_NAMING_RULE_TO_TEST).when(testObj).getProductNamingRule(Mockito.any());
		when(watersProductModel.getFeatures()).thenReturn(productFeaturesDataSetWithAllFeatures);
		final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
		final String expectedAutoName = "BrandA QcTestedA ParticleTechnologyA BondingTechnologyA ColumnFormatA, PoreSizeAÅ, 200 mm, 100 mm X 50 mm, 50 - 500, 2/pk";
		assertEquals(calculatedAutoName, expectedAutoName);
	}

	/**
	 * Test calculated automatic name when one feature having unit is set with feature value only and no value for it's unit.
	 * Check that such feature is excluded from the calculation due to missing unit.
	 */
	@Test
	public void checkAutoNameWhenUomNotSet()
	{
		doReturn(COLUMN_NAMING_RULE_TO_TEST).when(testObj).getProductNamingRule(Mockito.any());
		when(watersProductModel.getFeatures()).thenReturn(productFeaturesDataSetWithUomNotSet);
		final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
		final String expectedAutoName = "BrandA QcTestedA ParticleTechnologyA BondingTechnologyA ColumnFormatA, 200 mm, 100 mm X 50 mm, 50 - 500, 2/pk";
		assertEquals(calculatedAutoName, expectedAutoName);
	}

	/**
	 * Test calculated automatic name when one of the feature involved in calculation is not set.
	 * Check that such feature is excluded from the calculation and double commas (,,) do not appear due to missing feature value.
	 */
	@Test
	public void checkAutoNameWhenOneFeatureNotSet()
	{
		doReturn(COLUMN_NAMING_RULE_TO_TEST).when(testObj).getProductNamingRule(Mockito.any());
		when(watersProductModel.getFeatures()).thenReturn(productFeaturesDataSetWithOneFeatureMissing);
		final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
		final String expectedAutoName = "BrandA QcTestedA ParticleTechnologyA BondingTechnologyA ColumnFormatA, 200 mm, 100 mm X 50 mm, 50 - 500, 2/pk";
		assertEquals(calculatedAutoName, expectedAutoName);
	}

	/**
	 * Test calculated automatic name when first priority field is not set and second priority field is set.
	 */
	@Test
	public void checkAutoNameForPriorityFields()
	{
		doReturn(COLUMN_NAMING_RULE_TO_TEST).when(testObj).getProductNamingRule(Mockito.any());
		when(watersProductModel.getFeatures()).thenReturn(productFeaturesDataSetWithSecondPriorityField);
		final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
		final String expectedAutoName = "BrandA QcTestedA ParticleTechnologyA ChemistryA ColumnFormatA, PoreSizeAÅ, 200 mm, 100 mm X 50 mm, 50 - 500, 2/pk";
		assertEquals(calculatedAutoName, expectedAutoName);
	}

	/**
	 * Test calculated automatic name when both priority fields are not set.
	 */
	@Test
	public void checkAutoNameWhenPriorityFieldsNotSet()
	{
		doReturn(COLUMN_NAMING_RULE_TO_TEST).when(testObj).getProductNamingRule(Mockito.any());
		when(watersProductModel.getFeatures()).thenReturn(productFeaturesDataSetWithPriorityFieldsMissing);
		final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
		final String expectedAutoName = "BrandA QcTestedA ParticleTechnologyA ColumnFormatA, PoreSizeAÅ, 200 mm, 100 mm X 50 mm, 50 - 500, 2/pk";
		assertEquals(calculatedAutoName, expectedAutoName);
	}

	/**
	 * Test calculated automatic name when molecular weight range value is greater than thousand but less than million.
	 * Check that molecular weight range is displayed using k.
	 */
	@Test
	public void checkAutoNameWhenMetricAboveThousand()
	{
		doReturn(COLUMN_NAMING_RULE_TO_TEST).when(testObj).getProductNamingRule(Mockito.any());
		when(watersProductModel.getFeatures()).thenReturn(productFeaturesDataSetWithMetricAboveThousand);
		final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
		final String expectedAutoName = "BrandA QcTestedA ParticleTechnologyA BondingTechnologyA ColumnFormatA, PoreSizeAÅ, 200 mm, 100 mm X 50 mm, 50 - 5K, 2/pk";
		assertEquals(calculatedAutoName, expectedAutoName);
	}

	/**
	 * Test calculated automatic name when molecular weight range value is greater than million.
	 * Check that molecular weight range is displayed using k.
	 */
	@Test
	public void checkAutoNameWhenMetricAboveMillion()
	{
		doReturn(COLUMN_NAMING_RULE_TO_TEST).when(testObj).getProductNamingRule(Mockito.any());
		when(watersProductModel.getFeatures()).thenReturn(productFeaturesDataSetWithMetricAboveMillion);
		final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
		final String expectedAutoName = "BrandA QcTestedA ParticleTechnologyA BondingTechnologyA ColumnFormatA, PoreSizeAÅ, 200 mm, 100 mm X 50 mm, 50 - 5M, 2/pk";
		assertEquals(calculatedAutoName, expectedAutoName);
	}

	@Test
	public void checkAutoNameWhenMetricNotSet()
	{
		doReturn(COLUMN_NAMING_RULE_TO_TEST).when(testObj).getProductNamingRule(Mockito.any());
		when(watersProductModel.getFeatures()).thenReturn(productFeaturesDataSetWithMetricNotSet);
		final String calculatedAutoName = testObj.calculateAutomaticName(watersProductModel);
		final String expectedAutoName = "BrandA QcTestedA ParticleTechnologyA BondingTechnologyA ColumnFormatA, PoreSizeAÅ, 200 mm, 100 mm X 50 mm, 2/pk";
		assertEquals(calculatedAutoName, expectedAutoName);
	}

	/**
	 * Set up product features with values and units.
	 */
	private void setupProductFeatures()
	{
		setupProductFeatureWithEnum(brand, brandAttributeAssignmentModel, brandAttributeModel, brandAttributeValueModel, "Brand", "BrandA");
		productFeaturesDataSetWithAllFeatures.add(brand);
		productFeaturesDataSetWithUomNotSet.add(brand);
		productFeaturesDataSetWithOneFeatureMissing.add(brand);
		productFeaturesDataSetWithSecondPriorityField.add(brand);
		productFeaturesDataSetWithPriorityFieldsMissing.add(brand);
		productFeaturesDataSetWithMetricAboveThousand.add(brand);
		productFeaturesDataSetWithMetricAboveMillion.add(brand);
		productFeaturesDataSetWithMetricNotSet.add(brand);

		setupProductFeature(qcTested, qcTestedAttributeAssignmentModel, qcTestedAttributeModel, "ColumnQCTested", "QcTestedA");
		productFeaturesDataSetWithAllFeatures.add(qcTested);
		productFeaturesDataSetWithUomNotSet.add(qcTested);
		productFeaturesDataSetWithOneFeatureMissing.add(qcTested);
		productFeaturesDataSetWithSecondPriorityField.add(qcTested);
		productFeaturesDataSetWithPriorityFieldsMissing.add(qcTested);
		productFeaturesDataSetWithMetricAboveThousand.add(qcTested);
		productFeaturesDataSetWithMetricAboveMillion.add(qcTested);
		productFeaturesDataSetWithMetricNotSet.add(qcTested);

		setupProductFeature(particleTechnology, particleTechnologyAttributeAssignmentModel, particleTechnologyAttributeModel, "ColumnParticleTechnology", "ParticleTechnologyA");
		productFeaturesDataSetWithAllFeatures.add(particleTechnology);
		productFeaturesDataSetWithUomNotSet.add(particleTechnology);
		productFeaturesDataSetWithOneFeatureMissing.add(particleTechnology);
		productFeaturesDataSetWithSecondPriorityField.add(particleTechnology);
		productFeaturesDataSetWithPriorityFieldsMissing.add(particleTechnology);
		productFeaturesDataSetWithMetricAboveThousand.add(particleTechnology);
		productFeaturesDataSetWithMetricAboveMillion.add(particleTechnology);
		productFeaturesDataSetWithMetricNotSet.add(particleTechnology);

		setupProductFeature(bondingTechnology, bondingTechnologyAttributeAssignmentModel, bondingTechnologyAttributeModel, "ColumnBondingTechnology", "BondingTechnologyA");
		productFeaturesDataSetWithAllFeatures.add(bondingTechnology);
		productFeaturesDataSetWithUomNotSet.add(bondingTechnology);
		productFeaturesDataSetWithOneFeatureMissing.add(bondingTechnology);
		productFeaturesDataSetWithMetricAboveThousand.add(bondingTechnology);
		productFeaturesDataSetWithMetricAboveMillion.add(bondingTechnology);
		productFeaturesDataSetWithMetricNotSet.add(bondingTechnology);

		setupProductFeature(chemistry, chemistryAttributeAssignmentModel, chemistryAttributeModel, "ColumnChemistry", "ChemistryA");
		productFeaturesDataSetWithAllFeatures.add(chemistry);
		productFeaturesDataSetWithUomNotSet.add(chemistry);
		productFeaturesDataSetWithOneFeatureMissing.add(chemistry);
		productFeaturesDataSetWithSecondPriorityField.add(chemistry);
		productFeaturesDataSetWithMetricAboveThousand.add(chemistry);
		productFeaturesDataSetWithMetricAboveMillion.add(chemistry);
		productFeaturesDataSetWithMetricNotSet.add(chemistry);

		setupProductFeature(format, formatAttributeAssignmentModel, formatAttributeModel, "ColumnFormat", "ColumnFormatA");
		productFeaturesDataSetWithAllFeatures.add(format);
		productFeaturesDataSetWithUomNotSet.add(format);
		productFeaturesDataSetWithOneFeatureMissing.add(format);
		productFeaturesDataSetWithSecondPriorityField.add(format);
		productFeaturesDataSetWithPriorityFieldsMissing.add(format);
		productFeaturesDataSetWithMetricAboveThousand.add(format);
		productFeaturesDataSetWithMetricAboveMillion.add(format);
		productFeaturesDataSetWithMetricNotSet.add(format);

		setupProductFeatureWithUnit(poreSize, poreSizeAttributeAssignmentModel, poreSizeAttributeModel, poreSizeUnitModel, "Å", "ColumnPoreSize", "PoreSizeA");
		productFeaturesDataSetWithAllFeatures.add(poreSize);
		productFeaturesDataSetWithSecondPriorityField.add(poreSize);
		productFeaturesDataSetWithPriorityFieldsMissing.add(poreSize);
		productFeaturesDataSetWithMetricAboveThousand.add(poreSize);
		productFeaturesDataSetWithMetricAboveMillion.add(poreSize);
		productFeaturesDataSetWithMetricNotSet.add(poreSize);

		setupProductFeatureWithUnit(particleSize, particleSizeAttributeAssignmentModel, particleSizeAttributeModel, particleSizeUnitModel, "mm", "ColumnParticleSize", "200");
		productFeaturesDataSetWithAllFeatures.add(particleSize);
		productFeaturesDataSetWithUomNotSet.add(particleSize);
		productFeaturesDataSetWithOneFeatureMissing.add(particleSize);
		productFeaturesDataSetWithSecondPriorityField.add(particleSize);
		productFeaturesDataSetWithPriorityFieldsMissing.add(particleSize);
		productFeaturesDataSetWithMetricAboveThousand.add(particleSize);
		productFeaturesDataSetWithMetricAboveMillion.add(particleSize);
		productFeaturesDataSetWithMetricNotSet.add(particleSize);

		setupProductFeatureWithUnit(innerDiameter, innerDiameterAttributeAssignmentModel, innerDiameterAttributeModel, innerDiameterUnitModel, "mm", "ColumnInnerDiameter", "100");
		productFeaturesDataSetWithAllFeatures.add(innerDiameter);
		productFeaturesDataSetWithUomNotSet.add(innerDiameter);
		productFeaturesDataSetWithOneFeatureMissing.add(innerDiameter);
		productFeaturesDataSetWithSecondPriorityField.add(innerDiameter);
		productFeaturesDataSetWithPriorityFieldsMissing.add(innerDiameter);
		productFeaturesDataSetWithMetricAboveThousand.add(innerDiameter);
		productFeaturesDataSetWithMetricAboveMillion.add(innerDiameter);
		productFeaturesDataSetWithMetricNotSet.add(innerDiameter);

		setupProductFeatureWithUnit(length, lengthAttributeAssignmentModel, lengthAttributeModel, lengthUnitModel, "mm", "ColumnLength", "50");
		productFeaturesDataSetWithAllFeatures.add(length);
		productFeaturesDataSetWithUomNotSet.add(length);
		productFeaturesDataSetWithOneFeatureMissing.add(length);
		productFeaturesDataSetWithSecondPriorityField.add(length);
		productFeaturesDataSetWithPriorityFieldsMissing.add(length);
		productFeaturesDataSetWithMetricAboveThousand.add(length);
		productFeaturesDataSetWithMetricAboveMillion.add(length);
		productFeaturesDataSetWithMetricNotSet.add(length);

		setupProductFeature(molecularWeightRangeMin, molecularWeightRangeMinAttributeAssignmentModel, molecularWeightRangeMinModel, "ColumnMolecularWeightRangeMin", "50");
		productFeaturesDataSetWithAllFeatures.add(molecularWeightRangeMin);
		productFeaturesDataSetWithUomNotSet.add(molecularWeightRangeMin);
		productFeaturesDataSetWithOneFeatureMissing.add(molecularWeightRangeMin);
		productFeaturesDataSetWithSecondPriorityField.add(molecularWeightRangeMin);
		productFeaturesDataSetWithPriorityFieldsMissing.add(molecularWeightRangeMin);
		productFeaturesDataSetWithMetricAboveThousand.add(molecularWeightRangeMin);
		productFeaturesDataSetWithMetricAboveMillion.add(molecularWeightRangeMin);

		setupProductFeature(molecularWeightRangeMax, molecularWeightRangeMaxAttributeAssignmentModel, molecularWeightRangeMaxModel, "ColumnMolecularWeightRangeMax", "500");
		productFeaturesDataSetWithAllFeatures.add(molecularWeightRangeMax);
		productFeaturesDataSetWithUomNotSet.add(molecularWeightRangeMax);
		productFeaturesDataSetWithOneFeatureMissing.add(molecularWeightRangeMax);
		productFeaturesDataSetWithPriorityFieldsMissing.add(molecularWeightRangeMax);
		productFeaturesDataSetWithSecondPriorityField.add(molecularWeightRangeMax);

		setupProductFeature(molecularWeightRangeAboveThousandMax, molecularWeightRangeAboveThousandMaxAttributeAssignmentModel, molecularWeightRangeAboveThousandMaxModel, "ColumnMolecularWeightRangeMax", "5000");
		productFeaturesDataSetWithMetricAboveThousand.add(molecularWeightRangeAboveThousandMax);

		setupProductFeature(molecularWeightRangeAboveMillionMax, molecularWeightRangeAboveMillionMaxAttributeAssignmentModel, molecularWeightRangeAboveMillionMaxModel, "ColumnMolecularWeightRangeMax", "5000000");
		productFeaturesDataSetWithMetricAboveMillion.add(molecularWeightRangeAboveMillionMax);

		setupProductFeatureWithUnit(unitsPerPackage, unitsPerPackageAttributeAssignmentModel, unitsPerPackageModel, unitsPerPackageUnitModel, "pk", "UnitsPerPackage", "2");
		productFeaturesDataSetWithAllFeatures.add(unitsPerPackage);
		productFeaturesDataSetWithUomNotSet.add(unitsPerPackage);
		productFeaturesDataSetWithOneFeatureMissing.add(unitsPerPackage);
		productFeaturesDataSetWithSecondPriorityField.add(unitsPerPackage);
		productFeaturesDataSetWithPriorityFieldsMissing.add(unitsPerPackage);
		productFeaturesDataSetWithMetricAboveThousand.add(unitsPerPackage);
		productFeaturesDataSetWithMetricAboveMillion.add(unitsPerPackage);
		productFeaturesDataSetWithMetricNotSet.add(unitsPerPackage);
	}

	private void setupProductFeature(final ProductFeatureModel productFeatureModel,
	                                 final ClassAttributeAssignmentModel classAttributeAssignmentModel,
	                                 final ClassificationAttributeModel classificationAttributeModel,
	                                 final String featureCode, final String featureValue)
	{
		when(productFeatureModel.getClassificationAttributeAssignment()).thenReturn(classAttributeAssignmentModel);
		when(classAttributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
		when(classificationAttributeModel.getCode()).thenReturn(featureCode);
		when(productFeatureModel.getValue()).thenReturn(featureValue);
	}

	private void setupProductFeatureWithEnum(final ProductFeatureModel productFeatureModel,
	                                         final ClassAttributeAssignmentModel classAttributeAssignmentModel,
	                                         final ClassificationAttributeModel classificationAttributeModel,
	                                         final ClassificationAttributeValueModel classificationAttributeValueModel,
	                                         final String featureCode, final String featureValue)
	{
		when(productFeatureModel.getClassificationAttributeAssignment()).thenReturn(classAttributeAssignmentModel);
		when(classAttributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
		when(classificationAttributeModel.getCode()).thenReturn(featureCode);
		when(classificationAttributeValueModel.getName(Locale.ENGLISH)).thenReturn(featureValue);
		when(productFeatureModel.getValue()).thenReturn(classificationAttributeValueModel);
	}

	private void setupProductFeatureWithUnit(final ProductFeatureModel productFeatureModel,
	                                         final ClassAttributeAssignmentModel classAttributeAssignmentModel,
	                                         final ClassificationAttributeModel classificationAttributeModel,
	                                         final ClassificationAttributeUnitModel classificationAttributeUnitModel,
	                                         final String unitSymbol,
	                                         final String featureCode, final String featureValue)
	{
		when(productFeatureModel.getClassificationAttributeAssignment()).thenReturn(classAttributeAssignmentModel);
		when(classificationAttributeUnitModel.getSymbol()).thenReturn(unitSymbol);
		when(classAttributeAssignmentModel.getUnit()).thenReturn(classificationAttributeUnitModel);
		when(productFeatureModel.getUnit()).thenReturn(classificationAttributeUnitModel);
		when(classAttributeAssignmentModel.getClassificationAttribute()).thenReturn(classificationAttributeModel);
		when(classificationAttributeModel.getCode()).thenReturn(featureCode);
		when(productFeatureModel.getValue()).thenReturn(featureValue);
	}
}
