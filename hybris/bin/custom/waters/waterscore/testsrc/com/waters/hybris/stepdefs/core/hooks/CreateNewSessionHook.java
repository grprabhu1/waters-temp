package com.waters.hybris.stepdefs.core.hooks;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import de.hybris.platform.servicelayer.session.SessionService;
import org.springframework.beans.factory.annotation.Required;

public class CreateNewSessionHook
{
	private SessionService sessionService;

	@Before(order = 0)
	public void setUp()
	{
		getSessionService().createNewSession();
	}

	@After(order = 0)
	public void teardown()
	{
		getSessionService().closeCurrentSession();
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
