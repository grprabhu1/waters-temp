package com.waters.hybris.stepdefs.core.helper;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;

public class SessionValueHelper
{
	private SessionService sessionService;
	private ModelService modelService;

	public <T> T get(final Class<T> clazz)
	{
		return get(getKey(clazz));
	}

	public <T> T get(final String key)
	{
		@SuppressWarnings("unchecked")
		final T item = getSessionService().getAttribute(key);
		if (item instanceof ItemModel)
		{
			getModelService().refresh(item);
		}
		else if (item instanceof Collection)
		{
			for (final Object collectionItem : (Collection) item)
			{
				if (collectionItem instanceof ItemModel)
				{
					getModelService().refresh(collectionItem);
				}
			}
		}
		return item;
	}

	public void set(final Object item)
	{
		set(getKey(item.getClass()), item);
	}

	public void set(final String key, final Object item)
	{
		getSessionService().setAttribute(key, item);
	}

	protected String getKey(Class clazz)
	{
		return clazz.getName();
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
