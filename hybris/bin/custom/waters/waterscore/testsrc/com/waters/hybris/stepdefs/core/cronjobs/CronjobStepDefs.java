package com.waters.hybris.stepdefs.core.cronjobs;

import com.waters.hybris.stepdefs.core.helper.SessionCronjobHelper;
import cucumber.api.java.en.Given;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.concurrent.ConcurrentHashMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.slf4j.LoggerFactory.getLogger;

public class CronjobStepDefs
{
	private static final Logger LOG = getLogger(CronjobStepDefs.class);
	private CronJobService cronJobService;
	private SessionCronjobHelper sessionCronjobHelper;
	private ConcurrentHashMap<String, Object> syncObjects = new ConcurrentHashMap<>();

	@Given("^the \"(.*?)\" has been run$")
	public void the_has_been_run(final String code)
	{
		final CronJobModel cronJob = getCronJobService().getCronJob(code);
		assertThat(cronJob).isNotNull();
		final Object lock = syncObjects.computeIfAbsent(cronJob.getCode(), (key) -> new Object());
		synchronized (lock)
		{
			LOG.debug("Starting cronjob [{}]", cronJob.getCode());
			getCronJobService().performCronJob(cronJob, true);
			LOG.debug("Cronjob [{}] finished", cronJob.getCode());
		}
		getSessionCronjobHelper().save(code);

	}

	@Given("^the job completed successfully$")
	public void the_job_completed_successfully()
	{
		final CronJobModel job = getSessionCronjobHelper().getJob();
		assertThat(job.getStatus()).isEqualTo(CronJobStatus.FINISHED);
		assertThat(job.getResult()).isEqualTo(CronJobResult.SUCCESS);
	}

	protected CronJobService getCronJobService()
	{
		return cronJobService;
	}

	@Required
	public void setCronJobService(final CronJobService cronJobService)
	{
		this.cronJobService = cronJobService;
	}

	protected SessionCronjobHelper getSessionCronjobHelper()
	{
		return sessionCronjobHelper;
	}

	@Required
	public void setSessionCronjobHelper(final SessionCronjobHelper sessionCronjobHelper)
	{
		this.sessionCronjobHelper = sessionCronjobHelper;
	}
}
