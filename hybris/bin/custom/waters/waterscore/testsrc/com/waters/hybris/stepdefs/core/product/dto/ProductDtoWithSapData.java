package com.waters.hybris.stepdefs.core.product.dto;

public class ProductDtoWithSapData extends ProductDto
{
	private String description;
	private String hierarchy;
	private String batchManaged;
	private String remainingSelfLife;
	private String containerRequirement;
	private String hazardousMaterialNumber;
	private String hazardousMaterialText;
	private String weight;
	private String weightUom;
	private String harmonizationCode;
	private String dgTechName;

	public String getDescription()
	{
		return description;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	public String getHierarchy()
	{
		return hierarchy;
	}

	public void setHierarchy(final String hierarchy)
	{
		this.hierarchy = hierarchy;
	}

	public String getBatchManaged()
	{
		return batchManaged;
	}

	public void setBatchManaged(final String batchManaged)
	{
		this.batchManaged = batchManaged;
	}

	public String getRemainingSelfLife()
	{
		return remainingSelfLife;
	}

	public void setRemainingSelfLife(final String remainingSelfLife)
	{
		this.remainingSelfLife = remainingSelfLife;
	}

	public String getContainerRequirement()
	{
		return containerRequirement;
	}

	public void setContainerRequirement(final String containerRequirement)
	{
		this.containerRequirement = containerRequirement;
	}

	public String getHazardousMaterialNumber()
	{
		return hazardousMaterialNumber;
	}

	public void setHazardousMaterialNumber(final String hazardousMaterialNumber)
	{
		this.hazardousMaterialNumber = hazardousMaterialNumber;
	}

	public String getHazardousMaterialText()
	{
		return hazardousMaterialText;
	}

	public void setHazardousMaterialText(final String hazardousMaterialText)
	{
		this.hazardousMaterialText = hazardousMaterialText;
	}

	public String getWeight()
	{
		return weight;
	}

	public void setWeight(final String weight)
	{
		this.weight = weight;
	}

	public String getWeightUom()
	{
		return weightUom;
	}

	public void setWeightUom(final String weightUom)
	{
		this.weightUom = weightUom;
	}

	public String getHarmonizationCode()
	{
		return harmonizationCode;
	}

	public void setHarmonizationCode(final String harmonizationCode)
	{
		this.harmonizationCode = harmonizationCode;
	}

	public String getDgTechName()
	{
		return dgTechName;
	}

	public void setDgTechName(final String dgTechName)
	{
		this.dgTechName = dgTechName;
	}
}
