package com.waters.hybris.core.strategies.impl;


import com.waters.hybris.core.model.model.ExternalMediaModel;
import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WatersProductImagePreviewResolutionStrategyTest {

    private static final String EXTERNAL_IMAGE_URL = "/content/dam/waters/waters/photography/products/skus/columns-and-consumables/filters/glass-base-tabulated-cap.png";
    private static final String RENDITION_IMAGE_URL = "_jcr_content/renditions/cq5dam.thumbnail.319.319.png";

    private static final String IMAGE_HOST = "https://test-www.waters.com";
    private static final String PRODUCT_DEFAULT_IMAGE = "/default-image.png";
    private static final String THUMBNAIL = "thumbnail";

    @Mock
    final ExternalMediaModel externalMediaModel = new ExternalMediaModel();

    @Spy
    @InjectMocks
    private final WatersProductImagePreviewResolutionStrategy testObj = new WatersProductImagePreviewResolutionStrategy();

    @Test
    public void testResolveImageUrlWhenExternalImageUrlNotSet() {
        doReturn(IMAGE_HOST).when(testObj).getImageHost();
        doReturn(PRODUCT_DEFAULT_IMAGE).when(testObj).getDefaultImage();
        when(externalMediaModel.getExternalUrl(Mockito.anyObject())).thenReturn(StringUtils.EMPTY);
        final String imageUrl = testObj.resolveImageUrl(null, Locale.ENGLISH);
        assertEquals(imageUrl, IMAGE_HOST + PRODUCT_DEFAULT_IMAGE);
    }

    @Test
    public void testResolveImageUrlWhenExternalImageNotSet() {
        doReturn(IMAGE_HOST).when(testObj).getImageHost();
        doReturn(PRODUCT_DEFAULT_IMAGE).when(testObj).getDefaultImage();
        final String imageUrl = testObj.resolveImageUrl(externalMediaModel, Locale.ENGLISH);
        assertEquals(imageUrl, IMAGE_HOST + PRODUCT_DEFAULT_IMAGE);
    }

    @Test
    public void testResolveImageUrlWhenExternalImageSet() {
        doReturn(IMAGE_HOST).when(testObj).getImageHost();
        when(externalMediaModel.getExternalUrl(Mockito.anyObject())).thenReturn(EXTERNAL_IMAGE_URL);
        final String imageUrl = testObj.resolveImageUrl(externalMediaModel, Locale.ENGLISH);
        assertEquals(imageUrl, IMAGE_HOST + EXTERNAL_IMAGE_URL);
    }

    @Test
    public void testResolveImageUrlWhenExternalImageSetWithRendition() {
        doReturn(IMAGE_HOST).when(testObj).getImageHost();
        doReturn(RENDITION_IMAGE_URL).when(testObj).getRenditionUrl(Mockito.anyObject());
        when(externalMediaModel.getExternalUrl(Mockito.anyObject())).thenReturn(EXTERNAL_IMAGE_URL);
        final String imageUrl = testObj.resolveImageUrl(externalMediaModel, Locale.ENGLISH, THUMBNAIL);
        assertEquals(imageUrl, IMAGE_HOST + EXTERNAL_IMAGE_URL + RENDITION_IMAGE_URL);
    }
}
