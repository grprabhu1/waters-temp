package com.waters.hybris.core.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultConfigurationPropertyStrategyTest
{

	public static final String PROPERTY_KEY = "propertyKey";

	@InjectMocks
	private DefaultConfigurationPropertyStrategy propertyMapStrategy;

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private ConfigurationService configurationService;

	@Test
	public void testGetMap(){
		when(propertyMapStrategy.getConfigurationService().getConfiguration().getString(anyString())).thenReturn("GBP:GFP,USD:U3D");
		Map<String, String> result = propertyMapStrategy.getStringMap(PROPERTY_KEY);
		Assert.assertEquals(2, result.size());
		Assert.assertEquals("GFP", result.get("GBP"));
		Assert.assertEquals("U3D", result.get("USD"));
	}

	@Test
	public void testGetMapWithEmptyValue(){
		when(propertyMapStrategy.getConfigurationService().getConfiguration().getString(anyString())).thenReturn("");
		Map<String, String> result = propertyMapStrategy.getStringMap(PROPERTY_KEY);
		Assert.assertEquals(0, result.size());
	}
	@Test
	public void testGetMapWithNullValue(){
		when(propertyMapStrategy.getConfigurationService().getConfiguration().getString(anyString())).thenReturn(null);
		Map<String, String> result = propertyMapStrategy.getStringMap(PROPERTY_KEY);
		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testMalformedPropertyValue(){
		when(propertyMapStrategy.getConfigurationService().getConfiguration().getString(anyString())).thenReturn("GBPGFP,USD:U3D");
		Map<String, String> result = propertyMapStrategy.getStringMap(PROPERTY_KEY);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("U3D", result.get("USD"));
	}
}
