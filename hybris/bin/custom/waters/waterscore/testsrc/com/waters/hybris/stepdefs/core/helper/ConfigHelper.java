package com.waters.hybris.stepdefs.core.helper;

import com.google.common.base.Joiner;
import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import java.util.Optional;

import static org.slf4j.LoggerFactory.getLogger;

public class ConfigHelper
{
	private static final ConfigurationService CONFIG = (ConfigurationService) Registry.getApplicationContext().getBean("configurationService");
	private static final Logger LOG = getLogger(ConfigHelper.class);
	public static final Joiner JOINER = Joiner.on(".").skipNulls();

	private ConfigHelper()
	{

	}

	public static double getFirstConfigDouble(final double defaultValue, final String key, final String... suffixes)
	{
		return Optional.ofNullable(getConfig(key, suffixes))
				.map(Double::valueOf)
				.orElseGet(() -> Double.valueOf(defaultValue))
				.doubleValue();
	}

	public static int getFirstConfigInt(final int defaultValue, final String key, final String... suffixes)
	{
		return Optional.ofNullable(getConfig(key, suffixes))
				.map(Integer::valueOf)
				.orElseGet(() -> Integer.valueOf(defaultValue))
				.intValue();
	}

	private static String getConfig(final String key, final String... suffixes)
	{
		final Configuration configuration = CONFIG.getConfiguration();
		for (final String suffix : suffixes)
		{
			final String configKey = JOINER.join(key, suffix);
			final String val = configuration.getString(configKey);
			if (StringUtils.isNotBlank(val))
			{
				LOG.debug("Found config for key [{}] using suffix [{}].  Value was [{}]", key, suffix, val);
				return val;
			}
		}

		final String val = configuration.getString(key);
		if (StringUtils.isNotBlank(val))
		{
			LOG.debug("Found config for key [{}]. Value was [{}]", key, val);
			return val;
		}

		LOG.debug("No config found for key [{}] with suffixes [{}]", key, suffixes);
		return null;
	}
}
