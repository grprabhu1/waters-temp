package com.waters.hybris.backoffice.services;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.hybris.backoffice.excel.data.SelectedAttribute;
import com.hybris.backoffice.excel.export.wizard.ExcelExportWizardForm;
import com.hybris.backoffice.excel.translators.ExcelTranslatorRegistry;
import com.hybris.backoffice.excel.translators.ExcelValueTranslator;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.services.ContentPackagingService;
import com.waters.hybris.core.services.data.ContentEntry;
import com.waters.hybris.core.util.WatersAssertUtils;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WatersExcelExportHandlerTest {

	public static final String EN_ISOCODE = "en";
	public static final String QUALIFIER = "qualifier";
	@InjectMocks
	private final WatersExcelExportHandler testObj = new WatersExcelExportHandler();

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private ContentPackagingService contentPackagingService;

	@Mock
	final ExcelExportWizardForm form = new ExcelExportWizardForm();

	@Mock
	private WatersProductModel watersProductModel1;

	@Mock
	private WatersProductModel watersProductModel2;

	@Mock
	private ClassificationClassModel classificationClassModel1;

	@Mock
	private ClassificationClassModel classificationClassModel2;

	@Mock
	private SelectedAttribute selectedAttribute1;

	@Mock
	private AttributeDescriptorModel attributeDescriptorModel;

	@Mock
	private ProductFeatureModel productFeatureModel1;

	@Mock
	private ProductFeatureModel productFeatureModel2;

	@Mock
	private ClassAttributeAssignmentModel classAttributeAssignmentModel1;

	@Mock
	private ClassAttributeAssignmentModel classAttributeAssignmentModel2;

	@Mock
	private ClassificationAttributeModel classificationAttributeModel1;

	@Mock
	private ClassificationAttributeModel classificationAttributeModel2;

	@Mock
	private ExcelTranslatorRegistry excelTranslatorRegistry;

	@Mock
	private ExcelValueTranslator excelValueTranslator;

	@Mock
	private LanguageModel en;

	@Mock
	private LanguageModel de;

	private final ContentEntry contentEntry = new ContentEntry();

	private final ContentEntry contentEntry2 = new ContentEntry();

	final List<ClassificationClassModel> classificationCategories1 = new ArrayList<>();
	final List<ClassificationClassModel> classificationCategories2 = new ArrayList<>();

	@Before
	public void setup() throws IOException {
		final List<SelectedAttribute> selectedAttributes = Collections.singletonList(selectedAttribute1);
		when(form.getAttributes()).thenReturn(selectedAttributes);

		classificationCategories1.add(classificationClassModel1);
		classificationCategories2.add(classificationClassModel2);

		when(classificationClassModel1.getCode()).thenReturn("Column");
		when(classificationClassModel2.getCode()).thenReturn("Vial");
		when(classificationClassModel1.getAllClassificationAttributeAssignments()).thenReturn(Collections.singletonList(classAttributeAssignmentModel1));
		when(classificationClassModel2.getAllClassificationAttributeAssignments()).thenReturn(Collections.singletonList(classAttributeAssignmentModel2));

		when(watersProductModel1.getClassificationClasses()).thenReturn(classificationCategories1);
		when(watersProductModel2.getClassificationClasses()).thenReturn(classificationCategories2);

		when(excelTranslatorRegistry.getTranslator(attributeDescriptorModel)).thenReturn(Optional.empty());

		setupProductFeatures();

		given(commonI18NService.getLocaleForIsoCode(EN_ISOCODE)).willReturn(Locale.ENGLISH);

		given(selectedAttribute1.getName()).willReturn("Code");
		given(selectedAttribute1.isLocalized()).willReturn(false);
		given(selectedAttribute1.getIsoCode()).willReturn(EN_ISOCODE);
		given(selectedAttribute1.getAttributeDescriptor()).willReturn(attributeDescriptorModel);
		given(attributeDescriptorModel.getQualifier()).willReturn(QUALIFIER);

		given(watersProductModel1.getProperty(QUALIFIER)).willReturn("111");
		given(watersProductModel2.getProperty(QUALIFIER)).willReturn("222");
		given(watersProductModel1.getProperty(QUALIFIER, Locale.ENGLISH)).willReturn("111_en");
		given(watersProductModel2.getProperty(QUALIFIER, Locale.ENGLISH)).willReturn("222_en");

		given(contentPackagingService.createZip(anyList())).willReturn(new ByteArrayOutputStream());
		contentEntry.setName("Column.csv");
		contentEntry.setContent("Code;AttributeA_name\n111;testValue1");

		contentEntry2.setName("Vial.csv");
		contentEntry2.setContent("Code;AttributeB_name\n222;testValue2");

		given(en.getIsocode()).willReturn("en");
		given(de.getIsocode()).willReturn("de");

		given(commonI18NService.getAllLanguages()).willReturn(Lists.newArrayList(en, de));

	}

	@Test
	public void should_CreateTwoDifferentClassifications() {
		testObj.getWatersProductsByteStream(form, Lists.newArrayList(watersProductModel1, watersProductModel2));

		assertOutputStream(Sets.newHashSet(contentEntry, contentEntry2));
	}

	@Test
	public void should_EscapeEspecialCharactersFromValues() {
		when(productFeatureModel1.getValue()).thenReturn("test\n;Value1");
		given(watersProductModel1.getProperty(QUALIFIER)).willReturn("11;1");

		contentEntry.setContent("Code;AttributeA_name\n\"11;1\";\"test;Value1\"");

		testObj.getWatersProductsByteStream(form, Lists.newArrayList(watersProductModel1, watersProductModel2));

		assertOutputStream(Sets.newHashSet(contentEntry, contentEntry2));
	}

	@Test
	public void should_CreateUnClassified_Classification() {
		given(watersProductModel2.getClassificationClasses()).willReturn(Collections.emptyList());

		testObj.getWatersProductsByteStream(form, Lists.newArrayList(watersProductModel1, watersProductModel2));

		contentEntry2.setName("Unclassified.csv");
		contentEntry2.setContent("Code\n222");
		assertOutputStream(Sets.newHashSet(contentEntry, contentEntry2));
	}

	@Test
	public void should_AddLanguageISOCodeWhenLocalizedField() {
		setUpBothProductsToSameClassification();
		given(selectedAttribute1.isLocalized()).willReturn(true);
		contentEntry.setContent("Code[en];AttributeA_name\n111_en;testValue1\n222_en;testValue2");

		testObj.getWatersProductsByteStream(form, Lists.newArrayList(watersProductModel1, watersProductModel2));

		assertOutputStream(Sets.newHashSet(contentEntry));
	}

	@Test
	public void should_AddLanguageISOCodeWhenIsStringAndLocalizedClassificationAttribute() {
		setUpBothProductsToSameClassification();
		given(classAttributeAssignmentModel1.getLocalized()).willReturn(Boolean.TRUE);
		given(classAttributeAssignmentModel1.getAttributeType()).willReturn(ClassificationAttributeTypeEnum.STRING);
		contentEntry.setContent("Code;AttributeA_name[en];AttributeA_name[de]\n111;testValue1_en;testValue1_de\n222;testValue2;");

		final ProductFeatureModel productFeatureModelDE = mock(ProductFeatureModel.class);
		when(productFeatureModelDE.getClassificationAttributeAssignment()).thenReturn(classAttributeAssignmentModel1);
		when(productFeatureModelDE.getValue()).thenReturn("testValue1_de");
		when(productFeatureModel1.getValue()).thenReturn("testValue1_en");
		when(productFeatureModelDE.getLanguage()).thenReturn(de);
		when(watersProductModel1.getFeatures()).thenReturn(Lists.newArrayList(productFeatureModel1, productFeatureModelDE));


		testObj.getWatersProductsByteStream(form, Lists.newArrayList(watersProductModel1, watersProductModel2));

		assertOutputStream(Sets.newHashSet(contentEntry));
	}

	@Test
	public void should_AddLanguageToHeader_ButEmptyValuesForMissingLanguages() {
		setUpBothProductsToSameClassification();
		given(classAttributeAssignmentModel1.getLocalized()).willReturn(Boolean.TRUE);
		given(selectedAttribute1.isLocalized()).willReturn(Boolean.TRUE);
		given(classAttributeAssignmentModel1.getAttributeType()).willReturn(ClassificationAttributeTypeEnum.STRING);
		contentEntry.setContent("Code[en];AttributeA_name[en];AttributeA_name[de];AttributeA_name[es]\n111_en;testValue1_en;;\n222_en;testValue2;;");

		when(productFeatureModel1.getValue()).thenReturn("testValue1_en");
		final LanguageModel es = mock(LanguageModel.class);
		when(es.getIsocode()).thenReturn("es");
		given(commonI18NService.getAllLanguages()).willReturn(Lists.newArrayList(en, de, es));

		testObj.getWatersProductsByteStream(form, Lists.newArrayList(watersProductModel1, watersProductModel2));

		assertOutputStream(Sets.newHashSet(contentEntry));
	}

	@Test
	public void should_CreateOneClassification() {
		setUpBothProductsToSameClassification();
		contentEntry.setContent("Code;AttributeA_name\n111;testValue1\n222;testValue2");

		testObj.getWatersProductsByteStream(form, Lists.newArrayList(watersProductModel1, watersProductModel2));

		assertOutputStream(Sets.newHashSet(contentEntry));
	}

	@Test
	public void should_ReturnsMultiValueFields_SeparatedByComma() {
		setUpBothProductsToSameClassification();
		final ProductFeatureModel productFeatureModel = mock(ProductFeatureModel.class);
		given(productFeatureModel.getClassificationAttributeAssignment()).willReturn(classAttributeAssignmentModel1);
		given(productFeatureModel.getValue()).willReturn("test2");
		given(watersProductModel1.getFeatures()).willReturn(Lists.newArrayList(productFeatureModel1, productFeatureModel));
		contentEntry.setContent("Code;AttributeA_name\n111;testValue1,test2\n222;testValue2");

		testObj.getWatersProductsByteStream(form, Lists.newArrayList(watersProductModel1, watersProductModel2));

		assertOutputStream(Sets.newHashSet(contentEntry));
	}

	@Test
	public void should_PopulateCatalogVersionAsString() {
		final CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);
		given(excelTranslatorRegistry.getTranslator(attributeDescriptorModel)).willReturn(Optional.of(excelValueTranslator));
		given(excelValueTranslator.exportData(catalogVersionModel)).willReturn(Optional.of("Product:Online"));

		setUpBothProductsToSameClassification();
		when(watersProductModel1.getProperty(QUALIFIER)).thenReturn(catalogVersionModel);
		when(watersProductModel2.getProperty(QUALIFIER)).thenReturn(catalogVersionModel);
		contentEntry.setContent("Code;AttributeA_name\nProduct:Online;testValue1\nProduct:Online;testValue2");

		testObj.getWatersProductsByteStream(form, Lists.newArrayList(watersProductModel1, watersProductModel2));

		assertOutputStream(Sets.newHashSet(contentEntry));
	}

	@Test
	public void should_PopulateClassificationClassCode() {
		given(excelTranslatorRegistry.getTranslator(attributeDescriptorModel)).willReturn(Optional.of(excelValueTranslator));
		given(excelValueTranslator.exportData(Collections.singletonList(classificationClassModel1))).willReturn(Optional.of("Column:WatersClassification:1.0"));

		setUpBothProductsToSameClassification();
		when(watersProductModel1.getProperty(QUALIFIER)).thenReturn(Collections.singletonList(classificationClassModel1));
		when(watersProductModel2.getProperty(QUALIFIER)).thenReturn(Collections.singletonList(classificationClassModel1));
		contentEntry.setContent("Code;AttributeA_name\nColumn:WatersClassification:1.0;testValue1\nColumn:WatersClassification:1.0;testValue2");

		testObj.getWatersProductsByteStream(form, Lists.newArrayList(watersProductModel1, watersProductModel2));

		assertOutputStream(Sets.newHashSet(contentEntry));
	}

	@Test
	public void should_ExportEmptyWhenNullAttribute() {
		setUpBothProductsToSameClassification();
		contentEntry.setContent("Code;AttributeA_name\n;testValue1\n222;testValue2");
		given(watersProductModel1.getProperty(QUALIFIER)).willReturn(null);

		testObj.getWatersProductsByteStream(form, Lists.newArrayList(watersProductModel1, watersProductModel2));

		assertOutputStream(Sets.newHashSet(contentEntry));
	}

	@Test
	public void should_ExportEmptyWhenNullFeature() {
		setUpBothProductsToSameClassification();
		contentEntry.setContent("Code;AttributeA_name\n111;\n222;testValue2");
		when(productFeatureModel1.getValue()).thenReturn(null);

		testObj.getWatersProductsByteStream(form, Lists.newArrayList(watersProductModel1, watersProductModel2));

		assertOutputStream(Sets.newHashSet(contentEntry));
	}

	private void assertOutputStream(final Set<ContentEntry> expected) {
		final ArgumentCaptor<List> captor = ArgumentCaptor.forClass(List.class);
		verify(contentPackagingService).createZip(captor.capture());
		final List<ContentEntry> entries = captor.getValue();
		WatersAssertUtils.assertCollectionContainsSameElements(expected, entries);
	}

	private void setUpBothProductsToSameClassification() {
		when(watersProductModel2.getClassificationClasses()).thenReturn(classificationCategories1);
		when(productFeatureModel2.getClassificationAttributeAssignment()).thenReturn(classAttributeAssignmentModel1);
	}

	private void setupProductFeatures() {
		when(productFeatureModel1.getClassificationAttributeAssignment()).thenReturn(classAttributeAssignmentModel1);
		when(productFeatureModel2.getClassificationAttributeAssignment()).thenReturn(classAttributeAssignmentModel2);
		when(productFeatureModel1.getLanguage()).thenReturn(en);
		when(productFeatureModel2.getLanguage()).thenReturn(en);

		when(classAttributeAssignmentModel1.getClassificationAttribute()).thenReturn(classificationAttributeModel1);
		when(classAttributeAssignmentModel2.getClassificationAttribute()).thenReturn(classificationAttributeModel2);

		when(classificationAttributeModel1.getCode()).thenReturn("AttributeA_code");
		when(classificationAttributeModel2.getCode()).thenReturn("AttributeB_code");

		when(classificationAttributeModel1.getName()).thenReturn("AttributeA_name");
		when(classificationAttributeModel2.getName()).thenReturn("AttributeB_name");

		when(productFeatureModel1.getValue()).thenReturn("testValue1");
		when(productFeatureModel2.getValue()).thenReturn("testValue2");

		final List<ProductFeatureModel> productFeatures1 = new ArrayList<>();
		final List<ProductFeatureModel> productFeatures2 = new ArrayList<>();

		productFeatures1.add(productFeatureModel1);
		productFeatures2.add(productFeatureModel2);

		when(watersProductModel1.getFeatures()).thenReturn(productFeatures1);
		when(watersProductModel2.getFeatures()).thenReturn(productFeatures2);
	}
}
