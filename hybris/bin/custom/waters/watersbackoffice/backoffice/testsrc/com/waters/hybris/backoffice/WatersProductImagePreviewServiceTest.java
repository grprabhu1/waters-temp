package com.waters.hybris.backoffice;

import com.hybris.cockpitng.core.config.impl.jaxb.hybris.Base;
import com.hybris.cockpitng.core.config.impl.jaxb.hybris.Preview;
import com.hybris.cockpitng.core.context.impl.DefaultCockpitContext;
import com.hybris.cockpitng.services.media.ObjectPreview;
import com.waters.hybris.backoffice.services.WatersProductImagePreviewService;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.model.model.ExternalMediaModel;
import com.waters.hybris.core.strategies.impl.WatersProductImagePreviewResolutionStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;
import java.util.Properties;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WatersProductImagePreviewServiceTest {
    private static final String TEST_URL = "https://test-www.waters.com/testImage.png";
    private static final String MIME_TYPE = "image/png";
    public static final String THUMBNAIL = "thumbnail";

    @Mock
    private final WatersProductImagePreviewResolutionStrategy watersProductImagePreviewResolutionStrategy = new WatersProductImagePreviewResolutionStrategy();

    @Spy
    @InjectMocks
    private final WatersProductImagePreviewService testObj = new WatersProductImagePreviewService();

    @Mock
    private final Properties extensionsToMime = new Properties();

    @Mock
    private final WatersProductModel watersProductModel = new WatersProductModel();

    private final Base configuration = new Base();

    private final DefaultCockpitContext context = new DefaultCockpitContext();

    @Before
    public void setup() {
        doReturn(Locale.ENGLISH).when(testObj).getCurrentLocale();
        when(extensionsToMime.getProperty(Mockito.anyString())).thenReturn(MIME_TYPE);
        final Preview preview = new Preview();
        preview.setUrlQualifier(THUMBNAIL);
        configuration.setPreview(preview);
    }

    @Test
    public void testProductImagePreviewWhenExternalImageUrlConfigured() {
        when(watersProductImagePreviewResolutionStrategy.resolveImageUrl(Mockito.anyObject(), Mockito.anyObject(), Mockito.anyObject()))
                .thenReturn(TEST_URL);
        when(watersProductModel.getPrimaryImage()).thenReturn(new ExternalMediaModel());
        final ObjectPreview productImagePreview = testObj.getPreview(watersProductModel, configuration, context);
        assertNotNull(productImagePreview);
        assertEquals(productImagePreview.getUrl(), TEST_URL);
        assertEquals(productImagePreview.getMime(), MIME_TYPE);
    }

    @Test
    public void testExternalImagePreview() {
        when(watersProductImagePreviewResolutionStrategy.resolveImageUrl(Mockito.anyObject(), Mockito.anyObject(), Mockito.anyObject()))
                .thenReturn(TEST_URL);
        when(watersProductModel.getPrimaryImage()).thenReturn(new ExternalMediaModel());
        final ObjectPreview productImagePreview = testObj.getPreview(new ExternalMediaModel(), configuration, context);
        assertNotNull(productImagePreview);
        assertEquals(productImagePreview.getUrl(), TEST_URL);
        assertEquals(productImagePreview.getMime(), MIME_TYPE);
    }

    @Test
    public void testProductImagePreviewWhenNoExternalImageConfigured() {
        when(watersProductImagePreviewResolutionStrategy.resolveImageUrl(Mockito.anyObject(), Mockito.anyObject(), Mockito.anyObject()))
                .thenReturn(StringUtils.EMPTY);
        final ObjectPreview productImagePreview = testObj.getPreview(watersProductModel, configuration, context);
        assertNull(productImagePreview);
    }
}
