package com.waters.hybris.backoffice.services;

import com.google.common.collect.Streams;
import com.hybris.backoffice.excel.data.SelectedAttribute;
import com.hybris.backoffice.excel.export.wizard.ExcelExportHandler;
import com.hybris.backoffice.excel.export.wizard.ExcelExportWizardForm;
import com.hybris.backoffice.excel.translators.ExcelTranslatorRegistry;
import com.hybris.cockpitng.config.jaxb.wizard.CustomType;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.widgets.configurableflow.FlowActionHandlerAdapter;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.services.ContentPackagingService;
import com.waters.hybris.core.services.data.ContentEntry;
import com.waters.hybris.core.util.StreamUtil;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.Config;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zul.Filedownload;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.waters.hybris.core.util.StreamUtil.nullSafe;

public class WatersExcelExportHandler extends ExcelExportHandler
{
	private static final Logger LOG = LoggerFactory.getLogger(WatersExcelExportHandler.class);
	private static final String SPACE = " ";
	private static final String SEPARATOR_CSV = ";";
	private static final String CSV = "csv";
	private static final String UNCLASSIFIED = "Unclassified.csv";

	private CommonI18NService commonI18NService;
	private ConfigurationService configurationService;
	private ContentPackagingService contentPackagingService;
	private ExcelTranslatorRegistry excelTranslatorRegistry;

	private static final Predicate<ClassAttributeAssignmentModel> IS_MULTI_LANGUAGE =
		caa -> Objects.nonNull(caa) && BooleanUtils.isTrue(caa.getLocalized())
			&& ClassificationAttributeTypeEnum.STRING.equals(caa.getAttributeType());

	private static final BiFunction<String, String, String> BUILD_MULTI_LANGUAGE_CODE = (a,b) -> String.format("%s[%s]", a, b);

	private static final Function<ProductFeatureModel, String> EXTRACT_ATTRIBUTE_NAME = c -> Optional.of(c)
		.map(ProductFeatureModel::getClassificationAttributeAssignment)
		.map(ClassAttributeAssignmentModel::getClassificationAttribute)
		.map(ClassificationAttributeModel::getName)
		.orElse(StringUtils.EMPTY);

	private static final Function<ProductFeatureModel, String> EXTRACT_ATTRIBUTE_NAME_LANGUAGE = c -> Optional.of(c)
		.filter(f -> Objects.nonNull(f.getLanguage()))
		.map(ProductFeatureModel::getClassificationAttributeAssignment)
		.filter(IS_MULTI_LANGUAGE)
		.map(ClassAttributeAssignmentModel::getClassificationAttribute)
		.map(ca -> BUILD_MULTI_LANGUAGE_CODE.apply(ca.getName(), c.getLanguage().getIsocode()))
		.orElse(EXTRACT_ATTRIBUTE_NAME.apply(c));

	private static final Function<WatersProductModel, Optional<ClassificationClassModel>> CLASSIFICATION_CLASS_EXTRACTOR = (wpm) ->
		nullSafe(wpm.getClassificationClasses())
			.findFirst();

	@Override
	public void perform(final CustomType customType, final FlowActionHandlerAdapter adapter, final Map<String, String> parameters)
	{

		final WidgetInstanceManager wim = adapter.getWidgetInstanceManager();
		final ExcelExportWizardForm form = wim.getModel().getValue("excelForm", ExcelExportWizardForm.class);
		final List<ItemModel> itemsToExport = form.getPageable().getAllResults();
		exportWatersProducts(form, itemsToExport);
	}


	private String getOutputZipFileName()
	{
		return getConfigurationService().getConfiguration().getString("backoffice.products.export.zipfilename");
	}

	protected void exportWatersProducts(final ExcelExportWizardForm form, final List<ItemModel> itemsToExport)
	{
		final ByteArrayOutputStream byteArrayOutputStream = getWatersProductsByteStream(form, itemsToExport);
		Filedownload.save(byteArrayOutputStream.toByteArray(), Config.getParameter("mediatype.by.fileextension.zip"), getOutputZipFileName());
	}

	protected String getHeader(final ExcelExportWizardForm form, final List<String> features)
	{
		final Stream<String> attributes = form.getAttributes().stream().map(this::getAttributeName);
		return Stream.concat(attributes, features.stream())
			.collect(Collectors.joining(WatersExcelExportHandler.SEPARATOR_CSV));
	}

	private String getAttributeName(final SelectedAttribute selectedAttribute)
	{
		return selectedAttribute.isLocalized() ? BUILD_MULTI_LANGUAGE_CODE.apply(selectedAttribute.getName(), selectedAttribute.getIsoCode()) : selectedAttribute.getName();
	}


	protected ByteArrayOutputStream getWatersProductsByteStream(final ExcelExportWizardForm form, final List<ItemModel> itemsToExport)
	{
		final List<ContentEntry> contentEntries = groupProductsByCategory(itemsToExport).entrySet().stream()
			.flatMap(e -> createContentEntry(form, e))
			.collect(Collectors.toList());

		return getContentPackagingService().createZip(contentEntries);
	}

	private String getFileName(final Optional<ClassificationClassModel> classificationClassModel)
	{
		return classificationClassModel
			.map(ClassificationClassModel::getCode)
			.map(c -> String.format("%s.%s", c, CSV))
			.orElse(UNCLASSIFIED);
	}

	private Stream<ContentEntry> createContentEntry(final ExcelExportWizardForm form, final Map.Entry<Optional<ClassificationClassModel>, List<WatersProductModel>> entry)
	{
		final List<WatersProductModel> products = entry.getValue();

		final List<String> features = StreamUtil
			.nullSafe(entry.getKey(), ClassificationClassModel::getAllClassificationAttributeAssignments)
			.flatMap(this::getClassificationAttributeName)
			.collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(products))
		{
			final ContentEntry contentEntry = new ContentEntry();
			contentEntry.setName(getFileName(entry.getKey()));
			final String content = Stream.concat(Stream.of(getHeader(form, features)),
				products.stream().map(p -> getRowData(form, features, p)))
				.collect(Collectors.joining("\n"));

			contentEntry.setContent(content);

			return Stream.of(contentEntry);
		}
		return Stream.empty();
	}

	private Stream<String> getClassificationAttributeName(final ClassAttributeAssignmentModel classAttributeAssignmentModel)
	{
		if (IS_MULTI_LANGUAGE.test(classAttributeAssignmentModel))
		{
			return Stream.of(classAttributeAssignmentModel)
				.map(ClassAttributeAssignmentModel::getClassificationAttribute)
				.map(ClassificationAttributeModel::getName)
				.flatMap(name -> getCommonI18NService().getAllLanguages()
					.stream()
					.map(l -> BUILD_MULTI_LANGUAGE_CODE.apply(name, l.getIsocode())));
		}

		return Stream.of(classAttributeAssignmentModel.getClassificationAttribute())
			.map(ClassificationAttributeModel::getName);
	}

	protected String getRowData(final ExcelExportWizardForm form, final List<String> features, final WatersProductModel product)
	{
		final Stream<String> attributes = form.getAttributes().stream()
			.map(attribute -> exportField(attribute, product));

		final Map<String, List<ProductFeatureModel>> productFeatures = product.getFeatures()
			.stream().collect(Collectors.groupingBy(EXTRACT_ATTRIBUTE_NAME_LANGUAGE));

		final Stream<String> featureValues = features.stream().map(f -> getFeatureValue(f, productFeatures));

		return Streams.concat(attributes, featureValues).collect(Collectors.joining(WatersExcelExportHandler.SEPARATOR_CSV));
	}

	protected Object getItemAttribute(final ItemModel item, final SelectedAttribute selectedAttribute)
	{

		final String qualifier = selectedAttribute.getAttributeDescriptor().getQualifier();
		return selectedAttribute.isLocalized() ? item
			.getProperty(qualifier, getCommonI18NService().getLocaleForIsoCode(selectedAttribute.getIsoCode())) : item.getProperty(qualifier);
	}

	private String exportField(final SelectedAttribute selectedAttribute, final WatersProductModel item)
	{
		final Object itemAttribute = getItemAttribute(item, selectedAttribute);

		if (itemAttribute != null)
		{
			final Optional<String> result = getExcelTranslatorRegistry().getTranslator(selectedAttribute.getAttributeDescriptor())
				.flatMap(t -> t.exportData(itemAttribute))
				.map(Object::toString);

			return formatCSVValue(result.orElse(itemAttribute.toString()));
		}
		return StringUtils.EMPTY;
	}

	protected Map<Optional<ClassificationClassModel>, List<WatersProductModel>> groupProductsByCategory(final List<ItemModel> itemsToExport)
	{
		// we assume that there will be only one ClassificationClass for each product
		return itemsToExport.stream()
			.map(WatersProductModel.class::cast)
			.collect(Collectors.groupingBy(CLASSIFICATION_CLASS_EXTRACTOR));
	}

	protected String formatCSVValue(final String value)
	{
		String result = value;
		if (StringUtils.contains(value, "\n"))
		{
			result = result.replaceAll("\n", StringUtils.EMPTY);
		}
		if (StringUtils.contains(result, ";"))
		{
			result = String.format("\"%s\"", result);
		}
		return result;
	}

	protected String getFeatureValue(final String code, final Map<String, List<ProductFeatureModel>> productFeatures)
	{
		return Optional.ofNullable(productFeatures.get(code))
			.map(this::getFeatureValue)
			.map(this::formatCSVValue)
			.orElse(StringUtils.EMPTY);
	}

	protected String getFeatureValue(final List<ProductFeatureModel> features)
	{
		return features.stream().map(this::getFeatureValue).collect(Collectors.joining(","));
	}

	protected String getFeatureValue(final ProductFeatureModel feature)
	{
		final String featureValue;
		if (feature.getValue() instanceof ClassificationAttributeValueModel)
		{
			final ClassificationAttributeValueModel classificationAttributeValueModel = (ClassificationAttributeValueModel) feature.getValue();
			featureValue = classificationAttributeValueModel.getName();
		}
		else
		{
			featureValue = feature.getValue() == null ? "" : feature.getValue().toString();
		}

		if (feature.getUnit() != null)
		{
			return featureValue + WatersExcelExportHandler.SPACE + feature.getUnit().getSymbol();
		}

		return featureValue;
	}


	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public ContentPackagingService getContentPackagingService()
	{
		return contentPackagingService;
	}

	@Required
	public void setContentPackagingService(final ContentPackagingService contentPackagingService)
	{
		this.contentPackagingService = contentPackagingService;
	}

	public ExcelTranslatorRegistry getExcelTranslatorRegistry()
	{
		return excelTranslatorRegistry;
	}

	@Required
	public void setExcelTranslatorRegistry(final ExcelTranslatorRegistry excelTranslatorRegistry)
	{
		this.excelTranslatorRegistry = excelTranslatorRegistry;
	}
}
