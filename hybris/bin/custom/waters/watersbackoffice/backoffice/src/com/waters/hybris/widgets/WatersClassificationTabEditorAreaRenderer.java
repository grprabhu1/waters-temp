//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.waters.hybris.widgets;

import com.hybris.cockpitng.components.Editor;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.util.YTestTools;
import com.waters.hybris.core.strategies.FeatureSortingStrategy;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.platformbackoffice.classification.ClassificationTabEditorAreaRenderer;
import de.hybris.platform.platformbackoffice.classification.util.BackofficeClassificationUtils;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Groupbox;

import java.util.List;
import java.util.Locale;
import java.util.Set;

public class WatersClassificationTabEditorAreaRenderer extends ClassificationTabEditorAreaRenderer {
	private I18NService i18NService;
	private FeatureSortingStrategy featureSortingStrategy;

	@Override
	protected Editor createEditor(final Feature feature, final WidgetInstanceManager widgetInstanceManager,
								  final boolean canWriteFeature) {
		final ProductModel productModel = this.lookupCurrentProduct(widgetInstanceManager);
		final Editor editor = new Editor();
		editor.setReadableLocales(this.getReadableLocales(productModel));
		editor.setWritableLocales(this.getWritableLocales(productModel));
		editor.setWidgetInstanceManager(widgetInstanceManager);
		editor.setType("Feature");
		editor.setOptional(BooleanUtils.isNotTrue(feature.getClassAttributeAssignment().getMandatory()));

		// If attribute is configured as read-only then set it as read-only
		// Otherwise use the default implementation of canWriteFeature
		if (feature.getClassAttributeAssignment().isReadOnly()) {
			editor.setReadOnly(true);
		} else {
			editor.setReadOnly(!canWriteFeature);
		}
//		editor.setClearValueSupported(this.isClearValueSupported(feature));
		editor.setInitialValue(BackofficeClassificationUtils.convertFeatureToClassificationInfo(feature));
		editor.setAtomic(true);
		final String featureQualifier = BackofficeClassificationUtils
			.getFeatureQualifier(feature.getClassAttributeAssignment());
		final String encodeFeatureQualifier = BackofficeClassificationUtils
			.getFeatureQualifierEncoded(featureQualifier);
		final String classificationQualifier = "currentObject['" + encodeFeatureQualifier + "']";
		editor.setProperty(classificationQualifier);
		editor.addParameter("editedPropertyQualifier", encodeFeatureQualifier);
		YTestTools.modifyYTestId(editor, "editor_" + featureQualifier);
		editor.afterCompose();
		return editor;
	}

	protected void renderSection(final ClassificationClassModel classificationClassModel, final List<Feature> features, final Component parent, final WidgetInstanceManager widgetInstanceManager) {
		if (!features.isEmpty()) {
			final Groupbox sectionGroupBox = new Groupbox();
			sectionGroupBox.setParent(parent);
			final String sectionName = String.format("%s - %s: %s - %s", this.getCatalogueName(classificationClassModel), classificationClassModel.getCatalogVersion().getVersion(), this.getClassificationClassName(classificationClassModel), classificationClassModel.getCode());
			final Caption caption = new Caption(sectionName);
			YTestTools.modifyYTestId(caption, sectionName + "_caption");
			caption.setSclass("yw-editorarea-tabbox-tabpanels-tabpanel-groupbox-caption");
			sectionGroupBox.appendChild(caption);
			sectionGroupBox.setSclass("yw-editorarea-tabbox-tabpanels-tabpanel-groupbox");
			YTestTools.modifyYTestId(sectionGroupBox, sectionName);
			final List<Feature> sortedFeatures = getFeatureSortingStrategy().sortByPositionAndName(features);
			this.renderAttributes(sectionGroupBox, sortedFeatures, widgetInstanceManager);
		}
	}

	private String getClassificationClassName(final ClassificationClassModel classificationClassModel) {
		final String classificationClassName = classificationClassModel.getName(this.getI18NService().getCurrentLocale());
		if (StringUtils.isNotEmpty(classificationClassName)) {
			return classificationClassName;
		}
		return classificationClassModel.getName(Locale.ENGLISH);
	}

	private String getCatalogueName(final ClassificationClassModel classificationClassModel) {
		final String catalogueName = classificationClassModel.getCatalogVersion().getCatalog().getName(this.getI18NService().getCurrentLocale());
		if (StringUtils.isNotEmpty(catalogueName)) {
			return catalogueName;
		}
		return classificationClassModel.getCatalogVersion().getCatalog().getName(Locale.ENGLISH);
	}

	private ProductModel lookupCurrentProduct(final WidgetInstanceManager widgetInstanceManager) {
		return widgetInstanceManager.getModel().getValue("currentProduct", ProductModel.class);
	}

	private Set<Locale> getReadableLocales(final ProductModel productModel) {
		return getPermissionFacade().getReadableLocalesForInstance(productModel);
	}

	private Set<Locale> getWritableLocales(final ProductModel productModel) {
		return getPermissionFacade().getWritableLocalesForInstance(productModel);
	}

	private boolean isClearValueSupported(final Feature feature) {
		return "date".equals(feature.getClassAttributeAssignment().getAttributeType().getCode());
	}

	public I18NService getI18NService() {
		return this.i18NService;
	}

	@Required
	public void setI18NService(final I18NService i18NService) {
		this.i18NService = i18NService;
	}

	public FeatureSortingStrategy getFeatureSortingStrategy()
	{
		return featureSortingStrategy;
	}

	public void setFeatureSortingStrategy(final FeatureSortingStrategy featureSortingStrategy)
	{
		this.featureSortingStrategy = featureSortingStrategy;
	}
}
