//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.waters.hybris.widgets;

import com.hybris.cockpitng.core.config.impl.jaxb.listview.ListColumn;
import com.hybris.cockpitng.dataaccess.facades.type.DataType;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.util.UITools;
import com.hybris.cockpitng.widgets.collectionbrowser.mold.impl.listview.renderer.DefaultListCellRenderer;
import com.hybris.cockpitng.widgets.util.QualifierLabel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;

public class WatersListCellRenderer extends DefaultListCellRenderer {

    private ConfigurationService configurationService;

    @Override
    public void render(final Listcell parent, final ListColumn columnConfiguration, final Object object, final DataType dataType, final WidgetInstanceManager widgetInstanceManger) {
        renderInternal(parent, columnConfiguration, object, dataType, widgetInstanceManger);
    }

    protected void renderInternal(final Listcell parent, final ListColumn columnConfiguration, final Object object, final DataType dataType, final WidgetInstanceManager widgetInstanceManger) {
        String labelText;
        if (columnConfiguration.isAutoExtract()) {
            labelText = ObjectUtils.toString(object);
        } else {
            final String qualifier = columnConfiguration.getQualifier();
            final QualifierLabel label = getWidgetRenderingUtils().getAttributeLabel(object, dataType, qualifier);
            labelText = label.getLabel();
            if (!label.isSuccessful()) {
                UITools.modifySClass(parent, "yw-listview-cell-restricted", true);
            }
        }
        final int maxCharsInCell = widgetInstanceManger.getWidgetSettings().getInt("maxCharsInCell");
        labelText = UITools.truncateText(labelText, getConfigurationService().getConfiguration()
                .getInt("backoffice.field.maxlength." + columnConfiguration.getQualifier(), maxCharsInCell));
        final Label label = new Label(labelText);
        UITools.modifySClass(label, "yw-listview-cell-label", true);
        label.setAttribute("hyperlink-candidate", Boolean.TRUE);
        parent.appendChild(label);
        fireComponentRendered(label, parent, columnConfiguration, object);
        fireComponentRendered(parent, columnConfiguration, object);
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}
