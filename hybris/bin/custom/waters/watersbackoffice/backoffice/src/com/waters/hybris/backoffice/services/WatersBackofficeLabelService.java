package com.waters.hybris.backoffice.services;

import com.hybris.backoffice.labels.impl.BackofficeLabelService;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.util.StreamUtil;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Arrays;
import java.util.Locale;
import java.util.stream.Collectors;

public class WatersBackofficeLabelService extends BackofficeLabelService
{
	protected static final String BRAND_QUALIFIER = "WatersClassification/1.0/Products.brand";
	protected static final String PRODUCT_DELIMITER = ", ";
	protected static final String PRODUCT_SEPARATOR = " - ";
	protected static final String PRODUCT_REFERENCE_SEPARATOR = " -> ";

	private ModelService modelService;

	@Override
	public String getObjectLabel(final Object object)
	{
		if (!hasReadPermission(object))
		{
			return getNoReadAccessMessage(object);
		}
		if (object instanceof WatersProductModel)
		{
			final WatersProductModel watersProductModel = (WatersProductModel) object;
			return getProductLabel(watersProductModel);
		}
		if (object instanceof ProductReferenceModel && ((ProductReferenceModel) object).getTarget() instanceof WatersProductModel)
		{
			final ProductReferenceModel productReference = (ProductReferenceModel) object;
			return getProductReferenceLabel(productReference);
		}
		return super.getObjectLabel(object);
	}

	protected String getProductLabel(final WatersProductModel watersProduct)
	{
		return StringUtils.join(Arrays.asList(watersProduct.getCode(), getProductType(watersProduct), getBrand(watersProduct)), PRODUCT_SEPARATOR);
	}

	protected String getProductReferenceLabel(final ProductReferenceModel productReference)
	{
		final String referenceType = productReference.getReferenceType() != null ? productReference.getReferenceType().getCode() : StringUtils.EMPTY;
		return StringUtils.join(Arrays.asList(referenceType, getProductLabel((WatersProductModel) productReference.getTarget())), PRODUCT_REFERENCE_SEPARATOR);
	}

	protected boolean hasReadPermission(final Object object)
	{
		return getPermissionFacade().canReadInstance(object);
	}

	protected String getBrand(final WatersProductModel watersProduct)
	{
		return StreamUtil.nullSafe(watersProduct.getFeatures())
			.filter(feature -> !getModelService().isRemoved(feature) && BRAND_QUALIFIER.equals(feature.getQualifier()))
			.map(feature -> ((ClassificationAttributeValueModel) feature.getValue()).getName(Locale.ENGLISH))
			.findFirst()
			.orElse(StringUtils.EMPTY);
	}

	protected String getProductType(final WatersProductModel watersProduct)
	{
		return StreamUtil.nullSafe(watersProduct.getClassificationClasses())
			.filter(classificationClass -> CollectionUtils.isEmpty(classificationClass.getAllSubcategories()))
			.map(CategoryModel::getCode)
			.collect(Collectors.joining(PRODUCT_DELIMITER));
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
