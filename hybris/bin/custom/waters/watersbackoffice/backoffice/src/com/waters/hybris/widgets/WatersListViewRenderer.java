//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.waters.hybris.widgets;

import com.hybris.cockpitng.core.config.impl.jaxb.listview.ListColumn;
import com.hybris.cockpitng.dataaccess.facades.type.DataType;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.widgets.collectionbrowser.mold.impl.listview.renderer.DefaultListViewRenderer;
import com.hybris.cockpitng.widgets.common.WidgetComponentRenderer;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zul.Listcell;

public class WatersListViewRenderer extends DefaultListViewRenderer {
    private WatersListCellRenderer watersListCellRenderer;

    @Override
    protected WidgetComponentRenderer<Listcell, ListColumn, Object> getColumnCellRenderer(final ListColumn column, final WidgetInstanceManager widgetInstanceManager, final DataType dataType) {
        return super.getColumnCellRenderer(column, widgetInstanceManager, dataType);
    }

    public WatersListCellRenderer getWatersListCellRenderer() {
        return watersListCellRenderer;
    }

    @Required
    public void setWatersListCellRenderer(final WatersListCellRenderer watersListCellRenderer) {
        this.watersListCellRenderer = watersListCellRenderer;
    }
}
