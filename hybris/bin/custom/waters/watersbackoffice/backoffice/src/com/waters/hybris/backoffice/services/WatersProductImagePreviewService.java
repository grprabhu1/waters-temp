package com.waters.hybris.backoffice.services;

import com.hybris.cockpitng.core.config.impl.jaxb.hybris.Base;
import com.hybris.cockpitng.core.context.CockpitContext;
import com.hybris.cockpitng.services.media.ObjectPreview;
import com.hybris.cockpitng.services.media.impl.DefaultObjectPreviewService;
import com.hybris.cockpitng.util.UITools;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.model.model.ExternalMediaModel;
import com.waters.hybris.core.strategies.ProductImageResolutionStrategy;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Locale;
import java.util.Properties;

public class WatersProductImagePreviewService extends DefaultObjectPreviewService {

    private ProductImageResolutionStrategy productImageResolutionStrategy;
    private I18NService i18NService;
    private Properties extensionsToMime;

    @Override
    public ObjectPreview getPreview(final Object target, final Base configuration, final CockpitContext context) {
        if (target instanceof WatersProductModel || target instanceof ExternalMediaModel) {
            return getObjectPreview(target, configuration);
        }
            return super.getPreview(target, configuration, context);
        }

    private ObjectPreview getObjectPreview(final Object target, final Base configuration) {
        final ExternalMediaModel externalMediaModel = getExternalMediaModel(target);
        final String previewQualifier = configuration.getPreview() != null ? configuration.getPreview().getUrlQualifier() : StringUtils.EMPTY;
        final String imageUrl = getProductImageResolutionStrategy().resolveImageUrl(externalMediaModel, getCurrentLocale(), previewQualifier);
        final String imageMime = resolveMimeByExtension(imageUrl);
        if (StringUtils.isNotEmpty(imageMime)) {
            return new ObjectPreview(imageUrl, imageMime, false);
        }
        return null;
    }

    public ExternalMediaModel getExternalMediaModel(final Object target) {
        if (target instanceof ExternalMediaModel) {
            return (ExternalMediaModel) target;
        } else {
            final WatersProductModel watersProductModel = (WatersProductModel) target;
            return watersProductModel.getPrimaryImage();
        }
    }

    public Locale getCurrentLocale() {
        return getI18NService().getCurrentLocale();
    }

    public String resolveMimeByExtension(final String url) {
        final String extension = UITools.extractExtension(url);
        if (StringUtils.isNotEmpty(extension) && StringUtils.isNotEmpty(getExtensionsToMime().getProperty(extension))) {
            return getExtensionsToMime().getProperty(extension);
        }
        return StringUtils.EMPTY;
    }

    public ProductImageResolutionStrategy getProductImageResolutionStrategy() {
        return productImageResolutionStrategy;
    }

    @Required
    public void setProductImageResolutionStrategy(final ProductImageResolutionStrategy productImageResolutionStrategy) {
        this.productImageResolutionStrategy = productImageResolutionStrategy;
    }

    public I18NService getI18NService() {
        return i18NService;
    }

    @Required
    public void setI18NService(final I18NService i18NService) {
        this.i18NService = i18NService;
    }

    public Properties getExtensionsToMime() {
        return extensionsToMime;
    }

    @Required
    public void setExtensionsToMime(final Properties extensionsToMime) {
        this.extensionsToMime = extensionsToMime;
    }
}
