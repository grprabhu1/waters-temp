package com.waters.hybris.backoffice.services;

import com.hybris.backoffice.cockpitng.dataaccess.facades.object.savedvalues.DefaultItemModificationHistoryService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.hmc.model.SavedValuesModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.springframework.beans.factory.annotation.Required;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WatersItemModificationHistoryService extends DefaultItemModificationHistoryService {

	public static final String FIND_SAVED_VALUES_ORDER_BY_MODIFIED_TIME = "SELECT {" + SavedValuesModel.PK + "} FROM {" +  SavedValuesModel._TYPECODE + "} where {" + SavedValuesModel.MODIFIEDITEM + "} = ?modifiedItem ORDER BY {"+ SavedValuesModel.MODIFIEDTIME +"} DESC";

	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<SavedValuesModel> getSavedValues(final ItemModel item) {
		final Map<String, Long> args = new HashMap<>();
		args.put(SavedValuesModel.MODIFIEDITEM, item.getPk().getLongValue());

		final SearchResult<SavedValuesModel> searchResult = getFlexibleSearchService().search(FIND_SAVED_VALUES_ORDER_BY_MODIFIED_TIME, args);
		return searchResult.getResult();
	}

	public FlexibleSearchService getFlexibleSearchService() {
		return flexibleSearchService;
	}

	@Required
	@Override
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
		this.flexibleSearchService = flexibleSearchService;
	}
}
