package com.waters.hybris.backoffice.solrsearch.resolvers;

import com.hybris.backoffice.solrsearch.resolvers.EnumValueResolver;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.servicelayer.type.TypeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Locale;

public class WatersEnumValueResolver extends EnumValueResolver
{
	private TypeService typeService;

	@Override
	protected String getEnumValue(final HybrisEnumValue hybrisEnumValue)
	{
		final EnumerationValueModel enumerationValue = getTypeService().getEnumerationValue(hybrisEnumValue);
		final String value = enumerationValue != null ? enumerationValue.getName(Locale.ENGLISH) : StringUtils.EMPTY;
		return StringUtils.isNotBlank(value) ? value : hybrisEnumValue.getCode();
	}

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}
}
