package com.waters.hybris.backoffice.setup;

import com.waters.hybris.backoffice.constants.WatersbackofficeConstants;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;

import java.util.List;

@SystemSetup(extension = WatersbackofficeConstants.EXTENSIONNAME)
public class WatersbackofficeSystemSetup extends AbstractSystemSetup
{
	@Override
	public List<SystemSetupParameter> getInitializationOptions()
	{
		return null;
	}

	/**
	 * This method will be called during the system initialization.
	 *
	 * @param context the context provides the selected parameters and values
	 */
	@SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.INIT)
	public void createProjectData(final SystemSetupContext context) {
		importImpexFile(context, "/watersbackoffice/import/solr-backoffice.impex");
		importImpexFile(context, "/watersbackoffice/import/backoffice-scripts.impex");
	}
}
