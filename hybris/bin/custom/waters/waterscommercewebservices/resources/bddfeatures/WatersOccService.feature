@OccService @Transaction
Feature: Waters OCC Service

	Background: Access Waters Site
		Given accessing the "waters" site


	Scenario: Get Catalogs
		Then get Catalogs return the following
			| id                   | name                   | url                   |
			| watersProductCatalog | Waters Product Catalog | /watersProductCatalog |

	Scenario: Get Specific Catalog
		Then get "watersProductCatalog" catalog return the following catalog versions
			| Staged | /watersProductCatalog/Staged |
			| Online | /watersProductCatalog/Online |

	Scenario: Get Online Catalog Version
		Then get "watersProductCatalog" catalog "Online" version return the following categories
			| code | subcategories |
			| Web    | Columns,SamplePreparationProducts,VialsPlatesFiltersandContainers,AnalyticalStandardsReagents,CGAccessories,SpareParts,Primers,Software,Filters_w,ApplicationKits,StandardsandReagents,SamplePreparation,VialsPlatesandContainers,ObsoleteDiscontinued  |

	Scenario: Get Stage Catalog Version
		Then get "watersProductCatalog" catalog "Staged" version returns empty

	Scenario: Get Specific Category
		Then get "watersProductCatalog" catalog "Online" version "Columns" category return the following
			| code | subcategories |
			| Columns    | µBondapakBondapak,µPorasilPorasilColumns,ACQUITYAPCColumns,ACQUITYUPLCColumns,NanoMicoflowLCMSColumns,AdvancedPurificationGlassColumns,AtlantisColumns,CORTECSColumns,DeltaPakColumns,EnzymateOnlineDigestionColumns,GPCSECColumns,NovaPakColumns,ResolveColumns,SFCColumns,SunFireColumns,SymmetryColumns,VanGuardColumnProtection,WatersInsulinHMWPColumn,WatersPAHColumns,SpherisorbColumns,XBridgeBEHXPColumns,XBridgeColumns,XSelectColumns,XSelectHSSandCSHXPColumns,XTerraColumns,Bioseparations,Columnparticletechnologies  |

	Scenario: Export Products
		Then export products from "watersProductCatalog" catalog "Online" version return the following
			| 176001740  | Carbamate Analysis Kit |
			| 176001836  | HPLC Therapeutic Peptide Method Development Kit |

	Scenario: Get Specific Product Details
		Then get the product "176001740" return the following details
			| code      | name                   | url                        | description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | purchasable | salesStatus |
			| 176001740 | Carbamate Analysis Kit | /waters/products/176001740 | Waters Carbamate Analysis Kit includes a Waters Carbamate column, Oasis HLB cartridges, vials and Reference Standards. Optimized for use with EPA Method 531.2, these products simplify your analysis while increasing your confidence in the results. Carbamate Column – designed and tested for high sensitivity and excellent Resolution, specified as the primary column for EPA Method 531.2; Oasis HLB Cartridges – for sample cleanup and concentration to ensure maximum sensitivity; Vials – certified to be clean and free of background contamination in order to provide optimized analysis; Carbamate Reference Standards – for both drinking water and wastewater, these reference materials increase the level of confidence in your results. | true        | Active      |
