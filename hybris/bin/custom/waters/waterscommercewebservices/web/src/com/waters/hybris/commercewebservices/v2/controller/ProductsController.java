/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.commercewebservices.v2.controller;

import com.waters.hybris.commercewebservices.constants.YcommercewebservicesConstants;
import com.waters.hybris.commercewebservices.formatters.WsDateFormatter;
import com.waters.hybris.commercewebservices.swagger.ApiBaseSiteIdParam;
import com.waters.hybris.commercewebservices.v2.helper.ProductsHelper;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commercewebservicescommons.dto.product.ProductWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.search.facetdata.ProductSearchPageWsDTO;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.log4j.Logger;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.Set;


/**
 * Web Services Controller to expose the functionality of the
 * {@link de.hybris.platform.commercefacades.product.ProductFacade} and SearchFacade.
 */

@Controller
@Api(tags = "Products")
@RequestMapping(value = "/{baseSiteId}/products")
@SuppressFBWarnings
public class ProductsController extends BaseController {
	private static final String BASIC_OPTION = "BASIC";
	private static final Set<ProductOption> OPTIONS;
	private static final String MAX_INTEGER = "2147483647";
	private static final int CATALOG_ID_POS = 0;
	private static final int CATALOG_VERSION_POS = 1;
	private static final Logger LOG = Logger.getLogger(ProductsController.class);
	private static final String PRODUCT_OPTIONS;
	@Resource(name = "cwsProductFacade")
	private ProductFacade productFacade;
	@Resource(name = "wsDateFormatter")
	private WsDateFormatter wsDateFormatter;
	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;
	@Resource(name = "productsHelper")
	private ProductsHelper productsHelper;

	static {
		String productOptions = "";

		for (final ProductOption option : ProductOption.values()) {
			productOptions = productOptions + option.toString() + " ";
		}
		productOptions = productOptions.trim().replace(" ", YcommercewebservicesConstants.OPTIONS_SEPARATOR);

		PRODUCT_OPTIONS = productOptions;
		OPTIONS = extractOptions(productOptions);
	}

	protected static Set<ProductOption> extractOptions(final String options) {
		final String[] optionsStrings = options.split(YcommercewebservicesConstants.OPTIONS_SEPARATOR);

		final Set<ProductOption> opts = new HashSet<ProductOption>();
		for (final String option : optionsStrings) {
			opts.add(ProductOption.valueOf(option));
		}
		return opts;
	}


	@RequestMapping(value = "/search", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(value = "Get a list of products and additional data", notes = "Returns a list of products and additional data such as: available facets, available sorting and pagination options."
		+ "It can include spelling suggestions. To make spelling suggestions work you need to: 1. Make sure enableSpellCheck on the SearchQuery is set to true. By default it should be already set to true. 2. Have indexed properties configured to be used for spellchecking.")
	@ApiBaseSiteIdParam
	public ProductSearchPageWsDTO searchProducts(
		@ApiParam(value = "Serialized query, free text search, facets. The format of a serialized query: freeTextSearch:sort:facetKey1:facetValue1:facetKey2:facetValue2") @RequestParam(required = false) final String query,
		@ApiParam(value = "The current result page requested.") @RequestParam(required = false, defaultValue = DEFAULT_CURRENT_PAGE) final int currentPage,
		@ApiParam(value = "The number of results returned per page.") @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) final int pageSize,
		@ApiParam(value = "Response configuration (list of fields, which should be returned in response)", allowableValues = "BASIC, DEFAULT, FULL") @RequestParam(required = false) final String sort,
		@ApiParam(value = "The context to be used in the search query.") @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields,
		@RequestParam(required = false) final String searchQueryContext, final HttpServletResponse response) {
		final ProductSearchPageWsDTO result = productsHelper.searchProducts(query, currentPage, pageSize, sort,
			addPaginationField(fields), searchQueryContext);
		// X-Total-Count header
		setTotalCountHeader(response, result.getPagination());
		return result;
	}


	@RequestMapping(value = "/search", method = RequestMethod.HEAD)
	@ApiOperation(value = "Get a header with total number of products", notes = "Returns X-Total-Count header with total number of products satisfying a query. It doesn't return HTTP body.")
	@ApiBaseSiteIdParam
	public void countSearchProducts(
		@ApiParam(value = "Serialized query, free text search, facets. The format of a serialized query: freeTextSearch:sort:facetKey1:facetValue1:facetKey2:facetValue2") @RequestParam(required = false) final String query,
		final HttpServletResponse response) {
		final ProductSearchPageData<SearchStateData, ProductData> result = productsHelper.searchProducts(query, 0, 1, null);
		setTotalCountHeader(response, result.getPagination());
	}


	@RequestMapping(value = "/{productCode}", method = RequestMethod.GET)
	@CacheControl(directive = CacheControlDirective.PRIVATE, maxAge = 120)
	@Cacheable(value = "productCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(true,true,#productCode,#fields)")
	@ResponseBody
	@ApiOperation(value = "Get product details", notes = "Returns details of a single product according to a product code.")
	@ApiBaseSiteIdParam
	public ProductWsDTO getProductByCode(
		@ApiParam(value = "Product identifier", required = true) @PathVariable final String productCode,
		@ApiParam(value = "Response configuration (list of fields, which should be returned in response)", allowableValues = "BASIC, DEFAULT, FULL") @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("getProductByCode: code=" + sanitize(productCode) + " | options=" + PRODUCT_OPTIONS);
		}

		final ProductData product = productFacade.getProductForCodeAndOptions(productCode, OPTIONS);
		return getDataMapper().map(product, ProductWsDTO.class, fields);
	}


	protected PageableData createPageableData(final int currentPage, final int pageSize, final String sort) {
		final PageableData pageable = new PageableData();

		pageable.setCurrentPage(currentPage);
		pageable.setPageSize(pageSize);
		pageable.setSort(sort);
		return pageable;
	}
}
