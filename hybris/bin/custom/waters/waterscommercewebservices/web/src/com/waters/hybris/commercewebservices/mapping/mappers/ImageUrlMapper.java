package com.waters.hybris.commercewebservices.mapping.mappers;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercewebservicescommons.dto.product.ImageWsDTO;
import de.hybris.platform.webservicescommons.mapping.mappers.AbstractCustomMapper;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import ma.glasnost.orika.MappingContext;

@SuppressWarnings("PMD")
@SuppressFBWarnings
public class ImageUrlMapper extends AbstractCustomMapper<ImageData, ImageWsDTO> {
	@Override
	public void mapAtoB(final ImageData imageData, final ImageWsDTO imageWsDto, final MappingContext context) {
		// other fields are mapped automatically

		context.beginMappingField("url", getAType(), imageData, "url", getBType(), imageWsDto);
		try {
			if (shouldMap(imageData, imageWsDto, context)) {
				imageWsDto.setUrl(imageData.getUrl());
			}
		} finally {
			context.endMappingField();
		}
	}
}
