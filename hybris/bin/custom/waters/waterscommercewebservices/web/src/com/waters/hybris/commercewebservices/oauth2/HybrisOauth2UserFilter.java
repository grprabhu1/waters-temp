/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.commercewebservices.oauth2;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.annotation.Resource;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * Filter sets current user by userService depending on current principal. <br>
 * This should happen only when there is a customer context. Anonymous credentials are also applicable, because special
 * 'anonymous' user is available for that purpose. Customer context is not available during client credential flow.
 */
@SuppressWarnings("PMD")
@SuppressFBWarnings
public class HybrisOauth2UserFilter implements Filter {
	private static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
	private static final String ROLE_CUSTOMERGROUP = "ROLE_CUSTOMERGROUP";
	private static final String ROLE_CUSTOMERMANAGERGROUP = "ROLE_CUSTOMERMANAGERGROUP";

	@Resource(name = "userService")
	private UserService userService;


	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
		throws IOException, ServletException {
		final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (HybrisOauth2UserFilter.containsRole(auth, HybrisOauth2UserFilter.ROLE_ANONYMOUS) || HybrisOauth2UserFilter.containsRole(auth, HybrisOauth2UserFilter.ROLE_CUSTOMERGROUP)
			|| HybrisOauth2UserFilter.containsRole(auth, HybrisOauth2UserFilter.ROLE_CUSTOMERMANAGERGROUP)) {
			final UserModel userModel = this.userService.getUserForUID((String) auth.getPrincipal());
			this.userService.setCurrentUser(userModel);
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		// YTODO Auto-generated method stub
	}

	@Override
	public void init(final FilterConfig arg0) throws ServletException {
		// YTODO Auto-generated method stub
	}

	protected static boolean containsRole(final Authentication auth, final String role) {
		for (final GrantedAuthority ga : auth.getAuthorities()) {
			if (ga.getAuthority().equals(role)) {
				return true;
			}
		}
		return false;
	}
}
