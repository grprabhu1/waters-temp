/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.commercewebservices.conv;

import com.waters.hybris.commercewebservices.formatters.WsDateFormatter;

import java.util.Date;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Converter for a specific date format.
 */
@SuppressFBWarnings
public class CustomDateConverter implements SingleValueConverter
{
	private WsDateFormatter wsDateFormatter;

	public void setWsDateFormatter(final WsDateFormatter wsDateFormatter)
	{
		this.wsDateFormatter = wsDateFormatter;
	}

	@Override
	public boolean canConvert(final Class type)
	{
		return type == Date.class;
	}

	@Override
	public String toString(final Object obj)
	{
		return wsDateFormatter.toString((Date) obj);

	}

	@Override
	public Object fromString(final String str)
	{
		return wsDateFormatter.toDate(str);
	}
}
