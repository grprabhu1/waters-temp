package com.waters.hybris.commercewebservices.populator;

import com.waters.hybris.core.util.StreamUtil;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.catalog.CatalogOption;
import de.hybris.platform.commercefacades.catalog.PageOption;
import de.hybris.platform.commercefacades.catalog.converters.populator.CatalogVersionPopulator;
import de.hybris.platform.commercefacades.catalog.data.CatalogVersionData;
import de.hybris.platform.commercefacades.catalog.data.CategoryHierarchyData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

public class WatersCatalogVersionPopulator extends CatalogVersionPopulator
{
	@Override
	public void populate(final CatalogVersionModel source, final CatalogVersionData target,
	                     final Collection<CatalogOption> options) throws ConversionException
	{
		target.setId(source.getVersion());
		target.setLastModified(source.getModifiedtime());
		target.setName(source.getCategorySystemName());
		target.setCategoriesHierarchyData(Collections.emptyList());

		if (options.contains(CatalogOption.CATEGORIES))
		{
			target.setCategoriesHierarchyData(StreamUtil.nullSafe(getCategoryService().getRootCategoriesForCatalogVersion(source))
				.filter(this::isWebCategory)
				.map(category -> createCategoryHierarchyData(category, target, options)).collect(Collectors.toList()));
		}
	}

	private CategoryHierarchyData createCategoryHierarchyData(final CategoryModel category, final CatalogVersionData target, final Collection<CatalogOption> options)
	{
		final String catUrl = target.getUrl() + getCategoriesUrl();
		final CategoryHierarchyData categoryData = new CategoryHierarchyData();
		categoryData.setUrl(catUrl);
		getCategoryHierarchyPopulator().populate(category, categoryData, options, PageOption.createWithoutLimits());
		return categoryData;
	}

	private boolean isWebCategory(final CategoryModel source)
	{
		return CategoryModel._TYPECODE.equals(source.getItemtype());
	}
}
