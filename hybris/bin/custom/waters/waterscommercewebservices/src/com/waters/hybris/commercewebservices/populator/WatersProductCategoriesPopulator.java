package com.waters.hybris.commercewebservices.populator;

import com.waters.hybris.core.util.StreamUtil;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.ProductCategoriesPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;
import java.util.stream.Collectors;

public class WatersProductCategoriesPopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends ProductCategoriesPopulator<SOURCE, TARGET>
{
	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		final List<CategoryModel> categories = StreamUtil.nullSafe(getCommerceProductService().getSuperCategoriesExceptClassificationClassesForProduct(productModel))
			.filter(categoryModel -> categoryModel.getItemtype().equals(CategoryModel._TYPECODE))
			.collect(Collectors.toList());

		productData.setCategories(getCategoryConverter().convertAll(categories));
	}
}
