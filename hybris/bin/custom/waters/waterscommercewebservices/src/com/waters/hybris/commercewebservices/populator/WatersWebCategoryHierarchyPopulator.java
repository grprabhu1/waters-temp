package com.waters.hybris.commercewebservices.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.catalog.CatalogOption;
import de.hybris.platform.commercefacades.catalog.PageOption;
import de.hybris.platform.commercefacades.catalog.converters.populator.CategoryHierarchyPopulator;
import de.hybris.platform.commercefacades.catalog.data.CategoryHierarchyData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.Collections;

public class WatersWebCategoryHierarchyPopulator extends CategoryHierarchyPopulator
{
	@Override
	public void populate(final CategoryModel source, final CategoryHierarchyData target,
	                     final Collection<? extends CatalogOption> options, final PageOption page) throws ConversionException
	{
		if (isWebCategory(source))
		{
			super.populate(source, target, options, page);
		}
		else
		{
			target.setUrl(StringUtils.EMPTY);
			target.setSubcategories(Collections.EMPTY_LIST);
		}
	}

	private boolean isWebCategory(final CategoryModel source)
	{
		return source.getItemtype().equals(CategoryModel._TYPECODE);
	}
}
