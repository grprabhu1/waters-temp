package com.waters.hybris.commercewebservices.populator;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.waters.hybris.core.model.model.PunchoutCategoryModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.catalog.CatalogOption;
import de.hybris.platform.commercefacades.catalog.converters.populator.CategoryHierarchyPopulator;
import de.hybris.platform.commercefacades.catalog.data.CatalogVersionData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WatersCatalogVersionPopulatorTest
{
	private final WatersCatalogVersionPopulator watersCatalogVersionPopulator = new WatersCatalogVersionPopulator();

	@Mock
	private CategoryService mockCategoryService;
	@Mock
	private CategoryHierarchyPopulator mockCategoryHierarchyPopulator;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		watersCatalogVersionPopulator.setCategoriesUrl("/categories/");
		watersCatalogVersionPopulator.setCategoryHierarchyPopulator(mockCategoryHierarchyPopulator);
		watersCatalogVersionPopulator.setCategoryService(mockCategoryService);
	}

	@Test
	public void testWithBasicOption()
	{
		final Date lastModifiedDate = new Date();

		final CatalogVersionModel catalogVersionModel = Mockito.mock(CatalogVersionModel.class);
		BDDMockito.when(catalogVersionModel.getVersion()).thenReturn("Online");
		BDDMockito.when(catalogVersionModel.getCategorySystemName()).thenReturn("Online");
		BDDMockito.when(catalogVersionModel.getModifiedtime()).thenReturn(lastModifiedDate);

		final CatalogVersionData catalogVersionData = new CatalogVersionData();
		catalogVersionData.setUrl("/hwcatalog/Online");
		final Collection<CatalogOption> options = Sets.newHashSet(CatalogOption.BASIC);

		watersCatalogVersionPopulator.populate(catalogVersionModel, catalogVersionData, options);

		assertThat(catalogVersionData.getId()).isEqualTo("Online");
		assertThat(catalogVersionData.getName()).isEqualTo("Online");
		assertThat(catalogVersionData.getLastModified()).isEqualTo(lastModifiedDate);
		assertThat(catalogVersionData.getCategoriesHierarchyData()).isEmpty();
		assertThat(catalogVersionData.getUrl()).isEqualTo("/hwcatalog/Online");
	}

	@Test
	public void testWithCategoriesOption()
	{
		final Date lastModifiedDate = new Date();

		final CatalogVersionModel catalogVersionModel = Mockito.mock(CatalogVersionModel.class);
		BDDMockito.when(catalogVersionModel.getVersion()).thenReturn("Online");
		BDDMockito.when(catalogVersionModel.getCategorySystemName()).thenReturn("Online");
		BDDMockito.when(catalogVersionModel.getModifiedtime()).thenReturn(lastModifiedDate);

		final CatalogVersionData catalogVersionData = new CatalogVersionData();
		catalogVersionData.setUrl("/hwcatalog/Online");
		final Collection<CatalogOption> options = Sets.newHashSet(CatalogOption.CATEGORIES);

		final CategoryModel mockRootCategory = Mockito.mock(CategoryModel.class);
		BDDMockito.when(mockRootCategory.getCode()).thenReturn("HW-1000");
		BDDMockito.when(mockRootCategory.getItemtype()).thenReturn(CategoryModel._TYPECODE);
		BDDMockito.when(mockCategoryService.getRootCategoriesForCatalogVersion(catalogVersionModel)).thenReturn(
			Lists.newArrayList(mockRootCategory));

		watersCatalogVersionPopulator.populate(catalogVersionModel, catalogVersionData, options);

		assertThat(catalogVersionData.getId()).isEqualTo("Online");
		assertThat(catalogVersionData.getName()).isEqualTo("Online");
		assertThat(catalogVersionData.getLastModified()).isEqualTo(lastModifiedDate);
		assertThat(catalogVersionData.getCategoriesHierarchyData()).hasSize(1);

		assertThat(catalogVersionData.getUrl()).isEqualTo("/hwcatalog/Online");
	}

	@Test
	public void testWithMultipleCategoriesOption()
	{
		final Date lastModifiedDate = new Date();

		final CatalogVersionModel catalogVersionModel = Mockito.mock(CatalogVersionModel.class);
		BDDMockito.when(catalogVersionModel.getVersion()).thenReturn("Online");
		BDDMockito.when(catalogVersionModel.getCategorySystemName()).thenReturn("Online");
		BDDMockito.when(catalogVersionModel.getModifiedtime()).thenReturn(lastModifiedDate);

		final CatalogVersionData catalogVersionData = new CatalogVersionData();
		catalogVersionData.setUrl("/hwcatalog/Online");
		final Collection<CatalogOption> options = Sets.newHashSet(CatalogOption.CATEGORIES);

		final CategoryModel mockRootCategory = Mockito.mock(CategoryModel.class);
		BDDMockito.when(mockRootCategory.getItemtype()).thenReturn(CategoryModel._TYPECODE);
		final CategoryModel mockRootCategory2 = Mockito.mock(CategoryModel.class);
		BDDMockito.when(mockRootCategory2.getItemtype()).thenReturn(CategoryModel._TYPECODE);

		BDDMockito.when(mockRootCategory.getCode()).thenReturn("HW-1000");
		BDDMockito.when(mockRootCategory2.getCode()).thenReturn("mockRootCategory2");
		BDDMockito.when(mockCategoryService.getRootCategoriesForCatalogVersion(catalogVersionModel)).thenReturn(
			Lists.newArrayList(mockRootCategory, mockRootCategory2));

		watersCatalogVersionPopulator.populate(catalogVersionModel, catalogVersionData, options);

		assertThat(catalogVersionData.getId()).isEqualTo("Online");
		assertThat(catalogVersionData.getName()).isEqualTo("Online");
		assertThat(catalogVersionData.getLastModified()).isEqualTo(lastModifiedDate);
		assertThat(catalogVersionData.getCategoriesHierarchyData()).hasSize(2);
		assertThat(catalogVersionData.getUrl()).isEqualTo("/hwcatalog/Online");
	}

	@Test
	public void testWithWebAndPunchoutCategoryOption()
	{
		final Date lastModifiedDate = new Date();

		final CatalogVersionModel catalogVersionModel = Mockito.mock(CatalogVersionModel.class);
		BDDMockito.when(catalogVersionModel.getVersion()).thenReturn("Online");
		BDDMockito.when(catalogVersionModel.getCategorySystemName()).thenReturn("Online");
		BDDMockito.when(catalogVersionModel.getModifiedtime()).thenReturn(lastModifiedDate);

		final CatalogVersionData catalogVersionData = new CatalogVersionData();
		catalogVersionData.setUrl("/hwcatalog/Online");
		final Collection<CatalogOption> options = Sets.newHashSet(CatalogOption.CATEGORIES);

		final CategoryModel mockRootCategory = Mockito.mock(CategoryModel.class);
		BDDMockito.when(mockRootCategory.getItemtype()).thenReturn(CategoryModel._TYPECODE);
		final PunchoutCategoryModel mockPunchoutCategory = Mockito.mock(PunchoutCategoryModel.class);
		BDDMockito.when(mockPunchoutCategory.getItemtype()).thenReturn(PunchoutCategoryModel._TYPECODE);

		BDDMockito.when(mockRootCategory.getCode()).thenReturn("HW-1000");
		BDDMockito.when(mockPunchoutCategory.getCode()).thenReturn("punchoutCategory");
		BDDMockito.when(mockCategoryService.getRootCategoriesForCatalogVersion(catalogVersionModel)).thenReturn(
			Lists.newArrayList(mockRootCategory, mockPunchoutCategory));

		watersCatalogVersionPopulator.populate(catalogVersionModel, catalogVersionData, options);

		assertThat(catalogVersionData.getId()).isEqualTo("Online");
		assertThat(catalogVersionData.getName()).isEqualTo("Online");
		assertThat(catalogVersionData.getLastModified()).isEqualTo(lastModifiedDate);
		assertThat(catalogVersionData.getCategoriesHierarchyData()).hasSize(1);
		assertThat(catalogVersionData.getUrl()).isEqualTo("/hwcatalog/Online");
	}

	@Test
	public void testWithCategoriesAndProductsOptions()
	{
		final Date lastModifiedDate = new Date();

		final CatalogVersionModel catalogVersionModel = Mockito.mock(CatalogVersionModel.class);
		BDDMockito.when(catalogVersionModel.getVersion()).thenReturn("Online");
		BDDMockito.when(catalogVersionModel.getCategorySystemName()).thenReturn("Online");
		BDDMockito.when(catalogVersionModel.getModifiedtime()).thenReturn(lastModifiedDate);

		final CatalogVersionData catalogVersionData = new CatalogVersionData();
		catalogVersionData.setUrl("/hwcatalog/Online");
		final Collection<CatalogOption> options = Sets.newHashSet(CatalogOption.CATEGORIES, CatalogOption.PRODUCTS);

		final CategoryModel mockRootCategory = Mockito.mock(CategoryModel.class);
		BDDMockito.when(mockRootCategory.getItemtype()).thenReturn(CategoryModel._TYPECODE);
		BDDMockito.when(mockRootCategory.getCode()).thenReturn("HW-1000");
		BDDMockito.when(mockCategoryService.getRootCategoriesForCatalogVersion(catalogVersionModel)).thenReturn(
			Lists.newArrayList(mockRootCategory));

		watersCatalogVersionPopulator.populate(catalogVersionModel, catalogVersionData, options);

		assertThat(catalogVersionData.getId()).isEqualTo("Online");
		assertThat(catalogVersionData.getName()).isEqualTo("Online");
		assertThat(catalogVersionData.getLastModified()).isEqualTo(lastModifiedDate);
		assertThat(catalogVersionData.getCategoriesHierarchyData()).hasSize(1);
		assertThat(catalogVersionData.getUrl()).isEqualTo("/hwcatalog/Online");
	}
}
