package com.waters.hybris.stepdefs.commercewebservices;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.catalog.CatalogFacade;
import de.hybris.platform.commercefacades.catalog.CatalogOption;
import de.hybris.platform.commercefacades.catalog.PageOption;
import de.hybris.platform.commercefacades.catalog.data.CatalogData;
import de.hybris.platform.commercefacades.catalog.data.CatalogVersionData;
import de.hybris.platform.commercefacades.catalog.data.CategoryHierarchyData;
import de.hybris.platform.commercefacades.product.ProductExportFacade;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductResultData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import gherkin.formatter.model.DataTableRow;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Required;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class WatersOccServiceStepDefs
{
	private BaseSiteService baseSiteService;
	private UserService userService;
	private CatalogFacade watersCatalogFacade;
	private ProductExportFacade watersProductExportFacade;
	private ProductFacade cwsProductFacade;

	@Given("^accessing the \"([^\"]*)\" site$")
	public void accessing_the_site(final String siteId)
	{
		setUserSiteSession(siteId, getUserService().getAnonymousUser());
	}

	@Then("^get Catalogs return the following$")
	public void getCatalogs(final DataTable data)
	{
		final List<CatalogData> productCatalogs = getWatersCatalogFacade().getAllProductCatalogsForCurrentSite(getCatalogOptions());
		Assert.assertEquals(1, productCatalogs.size());

		final DataTableRow dataTableRow = data.getGherkinRows().get(1);
		Assert.assertEquals(dataTableRow.getCells().get(0), productCatalogs.get(0).getId());
		Assert.assertEquals(dataTableRow.getCells().get(1), productCatalogs.get(0).getName());
		Assert.assertEquals(dataTableRow.getCells().get(2), productCatalogs.get(0).getUrl());
	}

	@Then("^get \"([^\"]*)\" catalog return the following catalog versions$")
	public void getSpecificCatalog(final String catalogId, final DataTable data)
	{
		final CatalogData catalogData = getWatersCatalogFacade().getProductCatalogForCurrentSite(catalogId, getCatalogOptions());

		data.asMap(String.class, String.class).forEach((id, url) ->
		{
			if (catalogData.getCatalogVersions().stream()
				.noneMatch(v -> id.equals(v.getId()) && url.equals(v.getUrl())))
			{
				Assert.fail("Not found: " + id);
			}
		});
	}

	@Then("^get \"([^\"]*)\" catalog \"([^\"]*)\" version return the following categories$")
	public void getSpecificCatalogVersion(final String catalogId, final String versionId, final DataTable data)
	{
		final CatalogVersionData catalogVersionData = getWatersCatalogFacade().getProductCatalogVersionForTheCurrentSite(catalogId, versionId, getCatalogOptions());

		final CategoryHierarchyData categoryHierarchyData = catalogVersionData.getCategoriesHierarchyData().iterator().next();

		validateCatalogVersionData(data, categoryHierarchyData);
	}

	@Then("^get \"([^\"]*)\" catalog \"([^\"]*)\" version returns empty$")
	public void getStagedCatalogVersionReturnEmpty(final String catalogId, final String versionId)
	{
		final CatalogVersionData catalogVersionData = getWatersCatalogFacade().getProductCatalogVersionForTheCurrentSite(catalogId, versionId, getCatalogOptions());

		Assert.assertNotNull(catalogVersionData);

		Assert.assertEquals(0, catalogVersionData.getCategoriesHierarchyData().size());
	}

	@Then("^get \"([^\"]*)\" catalog \"([^\"]*)\" version \"([^\"]*)\" category return the following$")
	public void getSpecificCatalogVersion(final String catalogId, final String versionId, final String categoryId, final DataTable data)
	{
		final CategoryHierarchyData categoryHierarchyData = getWatersCatalogFacade().getCategoryById(catalogId, versionId, categoryId, getPageOption(), getCatalogOptions());

		validateCatalogVersionData(data, categoryHierarchyData);
	}

	@Then("^export products from \"([^\"]*)\" catalog \"([^\"]*)\" version return the following$")
	public void exportProducts(final String catalogId, final String versionId, final DataTable data)
	{
		final ProductResultData productsData = getWatersProductExportFacade().getAllProductsForOptions(catalogId, versionId, getProductOptions(), 0, 20);

		for (final DataTableRow dataTableRow : data.getGherkinRows())
		{
			final String id = dataTableRow.getCells().get(0);
			final String name = dataTableRow.getCells().get(1);
			final Optional<ProductData> product = productsData.getProducts().stream().filter(p -> id.equals(p.getCode())).findFirst();
			if (!product.isPresent() || !name.equals(product.get().getName()))
			{
				Assert.fail("Product not found: " + id + " name: " + name);
			}
		}
	}
	@Then("^get the product \"([^\"]*)\" return the following details$")
	public void getProductDetails(final String productCode, final DataTable data)
	{
		final ProductData product = getCwsProductFacade().getProductForCodeAndOptions(productCode, getProductOptions());
		Assert.assertNotNull(product);

		final DataTableRow dataTableRow = data.getGherkinRows().get(1);

		Assert.assertEquals(dataTableRow.getCells().get(0), product.getCode());
		Assert.assertEquals(dataTableRow.getCells().get(1), product.getName());
		Assert.assertEquals(dataTableRow.getCells().get(2), product.getUrl());
		Assert.assertEquals(dataTableRow.getCells().get(3), product.getDescription());
	}

	private void setUserSiteSession(final String siteId, final CustomerModel customer)
	{
		final BaseSiteModel baseSite = getBaseSiteService().getAllBaseSites().stream()
			.filter(b -> siteId.equals(b.getUid()))
			.findFirst()
			.orElseThrow(() -> new IllegalArgumentException("No site found with id: " + siteId));
		getUserService().setCurrentUser(customer);
		getBaseSiteService().setCurrentBaseSite(baseSite, true);
	}

	private Set<CatalogOption> getCatalogOptions()
	{
		return new HashSet(Arrays.asList(CatalogOption.BASIC, CatalogOption.CATEGORIES, CatalogOption.SUBCATEGORIES));
	}

	private PageOption getPageOption()
	{
		return PageOption.createForPageNumberAndPageSize(0, 10);
	}

	private Set<ProductOption> getProductOptions()
	{
		return new HashSet(Arrays.asList(ProductOption.values()));
	}

	private void validateCatalogVersionData(final DataTable data, final CategoryHierarchyData categoryHierarchyData)
	{

		Assert.assertEquals(data.getGherkinRows().get(1).getCells().get(0), categoryHierarchyData.getId());

		final List<String> subCategories = categoryHierarchyData.getSubcategories().stream()
			.map(CategoryHierarchyData::getId)
			.collect(Collectors.toList());

		final List<String> expectedSubCategories = Arrays.stream(data.getGherkinRows().get(1).getCells().get(1).split(","))
			.collect(Collectors.toList());

		Assert.assertEquals(expectedSubCategories, subCategories);
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected CatalogFacade getWatersCatalogFacade()
	{
		return watersCatalogFacade;
	}

	@Required
	public void setWatersCatalogFacade(final CatalogFacade watersCatalogFacade)
	{
		this.watersCatalogFacade = watersCatalogFacade;
	}

	protected ProductExportFacade getWatersProductExportFacade()
	{
		return watersProductExportFacade;
	}

	@Required
	public void setWatersProductExportFacade(final ProductExportFacade watersProductExportFacade)
	{
		this.watersProductExportFacade = watersProductExportFacade;
	}

	protected ProductFacade getCwsProductFacade()
	{
		return cwsProductFacade;
	}

	@Required
	public void setCwsProductFacade(final ProductFacade cwsProductFacade)
	{
		this.cwsProductFacade = cwsProductFacade;
	}
}
