/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.test.setup;

import com.neoworks.hybris.systemsetup.NeoworksAbstractSystemSetup;
import com.waters.hybris.test.constants.WatersTestConstants;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;


/**
 * This class provides hooks into the system's initialization and update processes.
 */
@SystemSetup(extension = WatersTestConstants.EXTENSIONNAME)
public class TestDataSystemSetup extends NeoworksAbstractSystemSetup
{
	private static final org.slf4j.Logger LOG = getLogger(TestDataSystemSetup.class);

	private static final String CREATE_TEST_DATA = "createTestData";
	private static final String PRODUCT_CATALOG_NAME = "waters";
	private static final String CONTENT_CATALOG_NAME = "waters";

	private CronJobService cronJobService;

	/**
	 * Generates the Dropdown and Multi-select boxes for the projectdata import
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<>();
		params.add(createBooleanSystemSetupParameter(CREATE_TEST_DATA, "Create Test Data", false));
		return params;
	}

	/**
	 * Implement this method to create initial objects. This method will be called by system creator during
	 * initialization and system update. Be sure that this method can be called repeatedly.
	 *
	 * @param context the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
	public void createEssentialData(final SystemSetupContext context)
	{
		// No essential data to import for the sample data extension
	}

	/**
	 * Implement this method to create data that is used in your project. This method will be called during the system
	 * initialization.
	 *
	 * @param context the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		if (getBooleanSystemSetupParameter(context, CREATE_TEST_DATA))
		{
			final List<String> testDataFiles  = Arrays.asList(
				"applicationkits.impex",
				"bulk.impex",
				"column.impex",
				"containers.impex",
				"filters.impex",
				"labequipment.impex",
				"plates.impex",
				"primers.impex",
				"quechers.impex",
				"smallcomponents.impex",
				"specartridges.impex",
				"speplates.impex",
				"software.impex",
				"standards.impex",
				"subassemblies.impex",
				"vialandplateaccessories.impex",
				"vials.impex",
				"related-products.impex",
				"images_Columns.impex",
				"images_Filters.impex",
				"images_Reagents.impex",
				"images_Sample_Prep.impex",
				"images_Vials.impex",
				"intrego-product-categories.impex",
				"aem-product-catagories.impex",
				"waters-product-manual-titles.impex",
				"product-descriptions.impex",
				"product-short-descriptions.impex",
				"prices.impex",
				"stocks.impex",
				"test-employees.impex",
				"approve-products.impex");

			testDataFiles.stream()
				.forEach(dataFile ->
				{
					LOG.info("importing file [{}]", dataFile);
					importImpexFile(context, "/waterstest/import/sampledata/" + dataFile, false);
				});

			importImpexFile(context, "/waterstest/import/sampledata/test-employees.impex");

			synchronizeProductCatalog(context, PRODUCT_CATALOG_NAME);

			synchronizeContentCatalog(context, CONTENT_CATALOG_NAME);
		}
	}

	public boolean synchronizeContentCatalog(final SystemSetupContext context, final String catalogName)
	{
		logInfo(context, String.format("Begin synchronizing Content Catalog [%s]", catalogName));

		final PerformResult syncCronJobResult = getSetupSyncJobService().executeCatalogSyncJob(
			String.format("%sContentCatalog", catalogName));
		if (isSyncRerunNeeded(syncCronJobResult))
		{
			logInfo(context, String.format("Content Catalog [%s] sync has issues.", catalogName));
			return false;
		}

		return true;
	}


	protected boolean synchronizeProductCatalog(final SystemSetupContext context, final String catalogName)
	{
		logInfo(context, "Begin synchronizing Product Catalog [" + catalogName + "] - ");

		boolean result = true;

		final PerformResult syncCronJobResult = executeCatalogSyncJob(context, catalogName + "ProductCatalog");
		if (isSyncRerunNeeded(syncCronJobResult))
		{
			logInfo(context, "Product catalog [" + catalogName + "] sync has issues.");
			result = false;
		}
		logInfo(context, "Done synchronizing Product Catalog [" + catalogName + "]");
		return result;
	}

	@Override
	public boolean getBooleanSystemSetupParameter(final SystemSetupContext context, final String key)
	{
		final String parameterKey = context.getExtensionName() + "_" + key;
		final String parameterValue = context.getParameter(parameterKey);
		if (parameterValue != null)
		{
			if (BOOLEAN_TRUE.equals(parameterValue))
			{
				return true;
			}
			else if (BOOLEAN_FALSE.equals(parameterValue))
			{
				return false;
			}
		}

		// next - check the local.properties
		LOG.debug("Looking for setup key [" + parameterKey + "] from configuration service.");

		final Boolean fromConfig = getConfigurationService().getConfiguration().getBoolean(parameterKey, null);
		if (fromConfig != null)
		{
			LOG.debug("Loading value for setup parameter [" + key + "] from configuration service. value was [" + fromConfig.booleanValue() + "]");
			return fromConfig.booleanValue();
		}

		// Have not been able to determine value from context, fallback to default value
		final boolean defaultValue = getDefaultValueForBooleanSystemSetupParameter(key);
		LOG.debug("Missing setup parameter for key [" + key + "], falling back to defined default [" + defaultValue + "]");
		return defaultValue;
	}

	protected CronJobService getCronJobService()
	{
		return cronJobService;
	}

	public void setCronJobService(final CronJobService cronJobService)
	{
		this.cronJobService = cronJobService;
	}
}
