package com.waters.hybris.service.impl;

import com.waters.hybris.constants.Environment;
import com.waters.hybris.model.Patch;
import com.waters.hybris.service.PatchRunnerService;
import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class DefaultPatchRunnerService implements PatchRunnerService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPatchRunnerService.class);
	private static final String ENVIRONMENT_CONFIG_PROPERTY = "configsrc.env";

	private ConfigurationService configurationService;
	private SetupImpexService setupImpexService;

	@Override
	public void runPatches(final List<Patch> patches)
	{
		final Optional<StackTraceElement> callingMethodInfo = Stream.of(Thread.currentThread().getStackTrace()).filter(s -> s.getClassName().startsWith("com.waters.hybris.setup")).findFirst();

		callingMethodInfo.ifPresent(e -> LOG.info("{}#{}(): starting patch execution...",
			e.getClassName(), e.getMethodName()));
		final long start = System.currentTimeMillis();

		final String currentEnv = getConfigurationService().getConfiguration().getString(ENVIRONMENT_CONFIG_PROPERTY);

		if (currentEnv == null)
		{
			LOG.error("Current environment property not set, skipping patch import");
			return;
		}

		patches.stream()
			.filter(p -> p.getEnvironments().contains(currentEnv) || p.getEnvironments().contains(Environment.ALL))
			.forEach(p -> p.getImpexList().forEach(impexFile ->
				{
					LOG.info("importing file [{}]", impexFile);
					getSetupImpexService().importImpexFile("/waterspatches/releases/" + p.getRelease() + "/" + impexFile, true);
				})
			);

		callingMethodInfo.ifPresent(e -> LOG.info("{}#{}(): patch execution finished in {} ms",
			e.getClassName(), e.getMethodName(), System.currentTimeMillis() - start));
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public SetupImpexService getSetupImpexService()
	{
		return setupImpexService;
	}

	@Required
	public void setSetupImpexService(final SetupImpexService setupImpexService)
	{
		this.setupImpexService = setupImpexService;
	}
}
