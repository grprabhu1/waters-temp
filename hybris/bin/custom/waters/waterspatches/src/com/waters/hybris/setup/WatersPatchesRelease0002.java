/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.setup;

import com.waters.hybris.constants.Environment;
import com.waters.hybris.constants.WaterspatchesConstants;
import com.waters.hybris.model.Patch;
import com.waters.hybris.service.PatchRunnerService;
import de.hybris.platform.core.initialization.SystemSetup;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.waters.hybris.constants.Release.R0002.IMPEX_DIRECTORY;
import static com.waters.hybris.constants.Release.R0002.PATCH_001;
import static com.waters.hybris.constants.Release.R0002.PATCH_002;
import static com.waters.hybris.constants.Release.R0002.PATCH_003;

/**
 * Executes patches during the system update
 * Patches are sorted by name and applied in that order.
 */
@SystemSetup(extension = WaterspatchesConstants.EXTENSIONNAME, process = SystemSetup.Process.UPDATE, patch = true)
public class WatersPatchesRelease0002
{
	private PatchRunnerService patchRunnerService;

	@SystemSetup(required = false, name = PATCH_001)
	public void patch001()
	{
		final List<String> impexes = Arrays.asList("r02_01-WAT-1074-sap-user.impex");
		final Patch patch = new Patch(Collections.singletonList(Environment.ALL), impexes, IMPEX_DIRECTORY);

		getPatchRunnerService().runPatches(Collections.singletonList(patch));
	}

	@SystemSetup(required = false, name = PATCH_002)
	public void patch002()
	{
		final List<String> impexes = Arrays.asList("r02_02-product-unspsc.impex");
		final Patch patch = new Patch(Collections.singletonList(Environment.ALL), impexes, IMPEX_DIRECTORY);

		getPatchRunnerService().runPatches(Collections.singletonList(patch));
	}

	@SystemSetup(required = false, name = PATCH_003)
	public void patch003()
	{
		final List<String> impexes = Arrays.asList("r02_03-product-syncjob-trigger.impex");
		final Patch patch = new Patch(Collections.singletonList(Environment.ALL), impexes, IMPEX_DIRECTORY);

		getPatchRunnerService().runPatches(Collections.singletonList(patch));
	}

	public PatchRunnerService getPatchRunnerService()
	{
		return patchRunnerService;
	}

	public void setPatchRunnerService(final PatchRunnerService patchRunnerService)
	{
		this.patchRunnerService = patchRunnerService;
	}

}
