package com.waters.hybris.constants;

public class Release
{
	public static class R0001
	{
		public static final String IMPEX_DIRECTORY = "release-0001";
		public static final String PATCH_001 = "R0001-P001";
		public static final String PATCH_002 = "R0001-P002";
		public static final String PATCH_003 = "R0001-P003";
		public static final String PATCH_004 = "R0001-P004";
		public static final String PATCH_005 = "R0001-P005";
	}

	public static class R0002
	{
		public static final String IMPEX_DIRECTORY = "release-0002";
		public static final String PATCH_001 = "R0002-P001";
		public static final String PATCH_002 = "R0002-P002";
		public static final String PATCH_003 = "R0002-P003";
	}

	public static class R0003
	{
		public static final String IMPEX_DIRECTORY = "release-0003";
		public static final String PATCH_001 = "R0003-P001";
	}
}
