package com.waters.hybris.model;

import java.util.List;

public class Patch
{

	public List<String> environments;

	public List<String> impexList;

	public String release;

	public Patch(final List<String> environments, final List<String> impexList, final String release)
	{
		this.environments = environments;
		this.impexList = impexList;
		this.release = release;
	}

	public List<String> getEnvironments()
	{
		return environments;
	}

	public void setEnvironments(final List<String> environments)
	{
		this.environments = environments;
	}

	public List<String> getImpexList()
	{
		return impexList;
	}

	public void setImpexList(final List<String> impexList)
	{
		this.impexList = impexList;
	}

	public String getRelease()
	{
		return release;
	}

	public void setRelease(final String release)
	{
		this.release = release;
	}
}
