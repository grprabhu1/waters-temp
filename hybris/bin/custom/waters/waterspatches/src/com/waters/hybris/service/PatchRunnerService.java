package com.waters.hybris.service;

import com.waters.hybris.model.Patch;

import java.util.List;

public interface PatchRunnerService
{

	void runPatches(final List<Patch> patches);
}
