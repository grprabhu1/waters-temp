/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.setup;

import com.waters.hybris.constants.Environment;
import com.waters.hybris.constants.WaterspatchesConstants;
import com.waters.hybris.model.Patch;
import com.waters.hybris.service.PatchRunnerService;
import de.hybris.platform.core.initialization.SystemSetup;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.waters.hybris.constants.Release.R0001.IMPEX_DIRECTORY;
import static com.waters.hybris.constants.Release.R0001.PATCH_001;
import static com.waters.hybris.constants.Release.R0001.PATCH_002;
import static com.waters.hybris.constants.Release.R0001.PATCH_003;
import static com.waters.hybris.constants.Release.R0001.PATCH_004;
import static com.waters.hybris.constants.Release.R0001.PATCH_005;


/**
 * Executes patches during the system update
 * Patches are sorted by name and applied in that order.
 */
@SystemSetup(extension = WaterspatchesConstants.EXTENSIONNAME, process = SystemSetup.Process.UPDATE, patch = true)
public class WatersPatchesRelease0001
{
	private PatchRunnerService patchRunnerService;

	@SystemSetup(required = false, name = PATCH_001)
	public void patch001()
	{
		final List<String> impexes = Arrays.asList(
			"r01_00-discontinued-obsolete-products.impex",
			"r01_00-manual-product-titles-remove-column.impex",
			"r01_00-manual-product-titles-remove-filters.impex",
			"r01_00-manual-product-titles-remove-specartridges.impex",
			"r01_00-manual-product-titles-remove-speplates.impex",
			"r01_00-smallComponentsInnerDiameter.impex",
			"r01_00-WAT-1138.impex",
			"r01_00-WAT-1168.impex",
			"r01_00-WAT-1170.impex");
		final Patch patch01Dev = new Patch(Collections.singletonList(Environment.ALL), impexes, IMPEX_DIRECTORY);

		getPatchRunnerService().runPatches(Collections.singletonList(patch01Dev));
	}

	@SystemSetup(required = false, name = PATCH_002)
	public void patch002()
	{
		final List<String> impexes = Arrays.asList(
			"r01_00-WAT-1171.impex",
			"r01_00_010-WAT-1156.impex");
		final Patch patch01Dev = new Patch(Collections.singletonList(Environment.ALL), impexes, IMPEX_DIRECTORY);

		getPatchRunnerService().runPatches(Collections.singletonList(patch01Dev));
	}

	@SystemSetup(required = false, name = PATCH_003)
	public void patch003()
	{
		final List<String> impexes = Arrays.asList(
			"r01_00-backoffice-scripts.impex");
		final Patch patch01Dev = new Patch(Collections.singletonList(Environment.ALL), impexes, IMPEX_DIRECTORY);

		getPatchRunnerService().runPatches(Collections.singletonList(patch01Dev));
	}

	@SystemSetup(required = false, name = PATCH_004)
	public void patch004()
	{
		final List<String> impexes = Arrays.asList(
			"r01_04-WAT-268.impex");
		final Patch patch04All = new Patch(Collections.singletonList(Environment.ALL), impexes, IMPEX_DIRECTORY);

		getPatchRunnerService().runPatches(Collections.singletonList(patch04All));
	}

	@SystemSetup(required = false, name = PATCH_005)
	public void patch005()
	{
		final List<String> impexes = Arrays.asList(
			"r01_05-WAT-1230.impex");
		final Patch patch05All = new Patch(Collections.singletonList(Environment.ALL), impexes, IMPEX_DIRECTORY);

		getPatchRunnerService().runPatches(Collections.singletonList(patch05All));
	}

	public PatchRunnerService getPatchRunnerService()
	{
		return patchRunnerService;
	}

	public void setPatchRunnerService(final PatchRunnerService patchRunnerService)
	{
		this.patchRunnerService = patchRunnerService;
	}

}
