/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.setup;



import static com.waters.hybris.constants.Release.R0003.IMPEX_DIRECTORY;
import static com.waters.hybris.constants.Release.R0003.PATCH_001;

import com.waters.hybris.constants.Environment;
import com.waters.hybris.constants.WaterspatchesConstants;
import com.waters.hybris.model.Patch;
import com.waters.hybris.service.PatchRunnerService;
import de.hybris.platform.core.initialization.SystemSetup;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Executes patches during the system update
 * Patches are sorted by name and applied in that order.
 */
@SystemSetup(extension = WaterspatchesConstants.EXTENSIONNAME, process = SystemSetup.Process.UPDATE, patch = true)
public class WatersPatchesRelease0003
{
	private PatchRunnerService patchRunnerService;

	@SystemSetup(required = true, name = PATCH_001)
	public void patch001()
	{
		final List<String> impexes = Arrays.asList("solrUpdateFixes.impex");
		final Patch patch = new Patch(Collections.singletonList(Environment.ALL), impexes, IMPEX_DIRECTORY);

		getPatchRunnerService().runPatches(Collections.singletonList(patch));
	}


	public PatchRunnerService getPatchRunnerService()
	{
		return patchRunnerService;
	}

	public void setPatchRunnerService(final PatchRunnerService patchRunnerService)
	{
		this.patchRunnerService = patchRunnerService;
	}

}
