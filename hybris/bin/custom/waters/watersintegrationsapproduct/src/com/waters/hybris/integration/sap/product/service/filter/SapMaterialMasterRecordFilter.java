package com.waters.hybris.integration.sap.product.service.filter;

import com.waters.hybris.integration.core.filter.ValueFilter;
import com.waters.hybris.integration.sap.product.data.SapMaterialGeneralData;
import com.waters.hybris.integration.sap.product.data.SapMaterialMasterRecord;
import org.apache.commons.collections.CollectionUtils;

/**
 * Created by ramana on 04/04/2018.
 */
public class SapMaterialMasterRecordFilter implements ValueFilter<SapMaterialMasterRecord>
{
	public static final String CROSS_PLANT_STATUS_PP = "PP";
	public static final String CROSS_PLANT_STATUS_RG = "RG";
	public static final String CHAIN_SPECIFIC_STATUS_A = "A";
	public static final String ATTRIBUTE_10_X = "X";

	@Override
	public boolean matches(final SapMaterialMasterRecord sapMaterialMasterRecord)
	{
		return sapMaterialMasterRecord.getGeneralData() != null && isSalesDataMatches(sapMaterialMasterRecord) && isCrossPlantStatusMatches(sapMaterialMasterRecord.getGeneralData());
	}

	protected boolean isCrossPlantStatusMatches(final SapMaterialGeneralData sapGeneralData)
	{
		return CROSS_PLANT_STATUS_PP.equalsIgnoreCase(sapGeneralData.getCrossPlantStatus()) || CROSS_PLANT_STATUS_RG.equalsIgnoreCase(sapGeneralData.getCrossPlantStatus());
	}

	protected boolean isSalesDataMatches(final SapMaterialMasterRecord sapMaterialMasterRecord)
	{
		return CollectionUtils.isNotEmpty(sapMaterialMasterRecord.getGeneralData().getSalesData()) &&
				sapMaterialMasterRecord.getGeneralData().getSalesData().stream()
						.anyMatch(salesData -> CHAIN_SPECIFIC_STATUS_A.equalsIgnoreCase(salesData.getChainSpecificStatus()) && ATTRIBUTE_10_X.equalsIgnoreCase(salesData.getAttribute10()));
	}
}
