package com.waters.hybris.integration.sap.product.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ZHYBEXT")
public class SapMaterialHybExtData
{
	@XStreamAlias("ZTECHNAME")
	private String dgTechName;
	@XStreamAlias("STOFT")
	private String hazardMaterialText;

	public String getDgTechName()
	{
		return dgTechName;
	}

	public String getHazardMaterialText()
	{
		return hazardMaterialText;
	}

	@Override
	public String toString()
	{
		return "SapMaterialHybExtData{" +
			"dgTechName='" + dgTechName + '\'' +
			", hazardMaterialText='" + hazardMaterialText + '\'' +
			"}";
	}
}
