package com.waters.hybris.integration.sap.product.jalo;

import com.waters.hybris.integration.sap.product.constants.WatersIntegrationSapProductConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

@SuppressWarnings("PMD")
public class WatersIntegrationSapProductManager extends GeneratedWatersIntegrationSapProductManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(WatersIntegrationSapProductManager.class.getName());

	public static final WatersIntegrationSapProductManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (WatersIntegrationSapProductManager) em.getExtension(WatersIntegrationSapProductConstants.EXTENSIONNAME);
	}

}
