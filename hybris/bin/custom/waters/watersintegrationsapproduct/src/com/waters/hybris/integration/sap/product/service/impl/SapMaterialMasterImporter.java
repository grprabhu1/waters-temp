package com.waters.hybris.integration.sap.product.service.impl;

import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.strategies.SavedValuesStrategy;
import com.waters.hybris.integration.core.constants.WatersintegrationcoreConstants;
import com.waters.hybris.integration.core.context.ImportContext;
import com.waters.hybris.integration.core.filter.ValueFilter;
import com.waters.hybris.integration.core.service.impl.AbstractProductDataConsumer;
import com.waters.hybris.integration.sap.product.constants.WatersIntegrationSapProductConstants;
import com.waters.hybris.integration.sap.product.data.SapMaterialAdditionalData;
import com.waters.hybris.integration.sap.product.data.SapMaterialDescriptionData;
import com.waters.hybris.integration.sap.product.data.SapMaterialGeneralData;
import com.waters.hybris.integration.sap.product.data.SapMaterialMasterRecord;
import com.waters.hybris.integration.sap.product.data.SapMaterialPlantData;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.hmc.model.SavedValuesModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Objects;
import java.util.Optional;

/**
 * Created by ramana on 04/04/2018.
 */
public class SapMaterialMasterImporter extends AbstractProductDataConsumer<SapMaterialMasterRecord, ImportContext>
{
	private static final Logger LOG = LoggerFactory.getLogger(SapMaterialMasterImporter.class);

	public static final String LANGUAGE_EN = "EN";

	private ValueFilter<SapMaterialMasterRecord> materialMasterDataValueFilter;
	private UserService userService;
	private SavedValuesStrategy savedValuesStrategy;
	private Optional<SavedValuesModel> savedValuesModel = Optional.empty();

	@Override
	protected boolean isValid(final SapMaterialMasterRecord item, final ImportContext context)
	{
		return item != null && item.getGeneralData() != null;
	}

	@Override
	protected void internalProcess(final SapMaterialMasterRecord item, final ImportContext context)
	{
		importSapMaterialMasterRecord(item, context);
	}

	protected void importSapMaterialMasterRecord(final SapMaterialMasterRecord item, final ImportContext context)
	{
		if (getMaterialMasterDataValueFilter().matches(item))
		{
			createOrUpdateWatersProduct(context, item);
			context.getBatchCommitter().commitIfReady();
			context.incrementSuccess();
		}
		else
		{
			if (LOG.isInfoEnabled())
			{
				LOG.info("Item did not match filter requirements, it will not be imported. [{}]", item);
			}
			context.incrementProcessed();
		}
	}

	protected void createOrUpdateWatersProduct(final ImportContext context, final SapMaterialMasterRecord item)
	{
		final ProductModel product = getExistingProductForCode(item.getGeneralData().getMaterialNumber());
		final boolean newProduct = product == null;
		final WatersProductModel watersProduct;
		if (!newProduct)
		{
			watersProduct = (WatersProductModel) product;
		}
		else
		{
			watersProduct = createNewProduct(WatersProductModel.class, item.getGeneralData().getMaterialNumber(), Optional.empty(), context);
		}

		updateWatersProductWithImportData(watersProduct, item, context);
		context.getBatchCommitter().add(watersProduct);
		logSuccessfulProductImport(watersProduct, newProduct);
	}

	protected void updateWatersProductWithImportData(final WatersProductModel watersProduct, final SapMaterialMasterRecord item, final ImportContext context)
	{
		final UserModel sapUserModel = getSapUser();

		// update product attributes
		savedValuesModel = getSavedValuesStrategy().create(watersProduct, sapUserModel, item.getGeneralData().getMaterialNumber(), watersProduct.getName(), false);
		watersProduct.setName(item.getGeneralData().getMaterialNumber());

		getSapProductTitle(item).ifPresent(title -> {
			savedValuesModel = getSavedValuesStrategy().create(watersProduct, sapUserModel, title, watersProduct.getSapProductTitle(), false);
			watersProduct.setSapProductTitle(title);
		});

		// update product classification attributes
		updateProductFeatures(watersProduct, item, context);

		if(savedValuesModel!=null && savedValuesModel.isPresent())
		{
			getModelService(context).save(savedValuesModel.get());
		}
	}

	protected void updateProductFeatures(final WatersProductModel watersProduct, final SapMaterialMasterRecord item, final ImportContext context)
	{
		ensureProductClassifiedAsProductsClass(watersProduct, context);
		setFeatureListChanged(false);
		final FeatureList featureList = getClassificationService().getFeatures(watersProduct);
		if (featureList != null)
		{
			final SapMaterialGeneralData generalData = item.getGeneralData();

			populateFeatureValue(featureList, generateProductsFeatureCode(WatersIntegrationSapProductConstants.CLASSIFICATION_ATTRIBUTE_SAP_HIERARCHY), generalData.getProductHierarchy());
			populateFeatureValue(featureList, generateProductsFeatureCode(WatersIntegrationSapProductConstants.CLASSIFICATION_ATTRIBUTE_BATCH_MANAGED), generalData.getBatchManaged());
			populateFeatureValue(featureList, generateProductsFeatureCode(WatersIntegrationSapProductConstants.CLASSIFICATION_ATTRIBUTE_REMAINING_SHELF_LIFE), generalData.getRemainingShelfLife());
			populateFeatureValue(featureList, generateProductsFeatureCode(WatersIntegrationSapProductConstants.CLASSIFICATION_ATTRIBUTE_CONTAINER_REQUIREMENT), generalData.getContainerRequirement());
			populateFeatureValue(featureList, generateProductsFeatureCode(WatersIntegrationSapProductConstants.CLASSIFICATION_ATTRIBUTE_HAZARD_MATERIAL_NUMBER), generalData.getHazardMaterialNumber());

			populateFeatureValue(featureList, generateProductsFeatureCode(WatersIntegrationSapProductConstants.CLASSIFICATION_ATTRIBUTE_WEIGHT), generalData.getWeight());
			populateFeatureValue(featureList, generateProductsFeatureCode(WatersIntegrationSapProductConstants.CLASSIFICATION_ATTRIBUTE_WEIGHT_UNIT), generalData.getWeightUnit());

			getHarmonizationCode(generalData).ifPresent(s -> populateFeatureValue(featureList, generateProductsFeatureCode(WatersIntegrationSapProductConstants.CLASSIFICATION_ATTRIBUTE_HARMONIZATION_CODE), s));

			final SapMaterialAdditionalData additionalData = generalData.getAdditionalData();
			if (additionalData != null && additionalData.getHybExtData() != null)
			{
				populateFeatureValue(featureList, generateProductsFeatureCode(WatersIntegrationSapProductConstants.CLASSIFICATION_ATTRIBUTE_DG_TECH_NAME), additionalData.getHybExtData().getDgTechName());
				populateFeatureValue(featureList, generateProductsFeatureCode(WatersIntegrationSapProductConstants.CLASSIFICATION_ATTRIBUTE_HAZARD_MATERIAL_TEXT), additionalData.getHybExtData().getHazardMaterialText());
			}
			if(isFeatureListChanged())
			{
				savedValuesModel = getSavedValuesStrategy().create(watersProduct, getSapUser(), featureList, watersProduct.getFeatures(), true);
				getClassificationService().replaceFeatures(watersProduct, featureList);
			}
		}
		else
		{
			LOG.error("Product {} has no feature list, Not yet Classified?", watersProduct.getCode());
		}
	}

	protected String generateProductsFeatureCode(final String feature)
	{
		return generateFeatureCode(WatersIntegrationSapProductConstants.CLASSIFICATION_CLASS_PRODUCTS, feature);
	}

	protected Optional<String> getSapProductTitle(final SapMaterialMasterRecord item)
	{
		if (CollectionUtils.isNotEmpty(item.getGeneralData().getDescriptionData()))
		{
			return item.getGeneralData().getDescriptionData().stream()
					.filter(descriptionData -> LANGUAGE_EN.equalsIgnoreCase(descriptionData.getLanguageIso()))
					.map(SapMaterialDescriptionData::getDescription)
					.filter(Objects::nonNull)
					.findFirst();
		}

		return Optional.empty();
	}

	protected Optional<String> getHarmonizationCode(final SapMaterialGeneralData generalData)
	{
		if (CollectionUtils.isNotEmpty(generalData.getPlantData()))
		{
			return generalData.getPlantData().stream()
					.map(SapMaterialPlantData::getHarmonizationCode)
					.filter(Objects::nonNull)
					.findFirst();
		}

		return Optional.empty();
	}

	protected void ensureProductClassifiedAsProductsClass(final WatersProductModel watersProduct, final ImportContext context)
	{
		ensureProductClassifiedAs(watersProduct, WatersIntegrationSapProductConstants.CLASSIFICATION_CLASS_PRODUCTS, context);
	}

	protected UserModel getSapUser()
	{
		try
		{
			return getUserService().getUserForUID(WatersintegrationcoreConstants.SAP_USER_UID);
		}
		catch (final Throwable t)
		{
			LOG.warn("Failed to get User with id " + WatersintegrationcoreConstants.SAP_USER_UID);
		}

		return null;
	}

	protected void logSuccessfulProductImport(final ProductModel productModel, final boolean newProduct)
	{
		if (LOG.isInfoEnabled())
		{
			LOG.info("SapMaterialMasterImporter {} product: {}", newProduct ? "created" : "updated", productModel.getCode());
		}
	}

	protected ValueFilter<SapMaterialMasterRecord> getMaterialMasterDataValueFilter()
	{
		return materialMasterDataValueFilter;
	}

	@Required
	public void setMaterialMasterDataValueFilter(final ValueFilter<SapMaterialMasterRecord> materialMasterDataValueFilter)
	{
		this.materialMasterDataValueFilter = materialMasterDataValueFilter;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public SavedValuesStrategy getSavedValuesStrategy()
	{
		return savedValuesStrategy;
	}

	public void setSavedValuesStrategy(final SavedValuesStrategy savedValuesStrategy)
	{
		this.savedValuesStrategy = savedValuesStrategy;
	}
}
