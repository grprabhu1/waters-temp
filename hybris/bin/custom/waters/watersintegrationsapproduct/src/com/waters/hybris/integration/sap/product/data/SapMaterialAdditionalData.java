package com.waters.hybris.integration.sap.product.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by ramana on 04/04/2018.
 *
 * SAP table MARA1 - Additional fields
 */
@XStreamAlias("E1MARA1")
public class SapMaterialAdditionalData
{
	@XStreamAlias("ZHYBEXT")
	private SapMaterialHybExtData hybExtData;

	public SapMaterialHybExtData getHybExtData()
	{
		return hybExtData;
	}

	@Override
	public String toString()
	{
		return "SapMaterialAdditionalData{" +
			"hybExtData='" + hybExtData + '\'' +
			"}";
	}
}
