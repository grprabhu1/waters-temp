package com.waters.hybris.integration.sap.product.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by ramana on 04/04/2018.
 */
@XStreamAlias("IDOC")
public class SapMaterialMasterRecord
{
	@XStreamAlias("E1MARAM")
	private SapMaterialGeneralData generalData;

	public SapMaterialGeneralData getGeneralData()
	{
		return generalData;
	}

	@Override
	public String toString()
	{
		return "SapMaterialMasterRecord{" +
				"generalData='" + generalData + '\'' +
				"}";
	}
}

