package com.waters.hybris.integration.sap.product.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by ramana on 04/04/2018.
 *
 * SAP table MARC - Plant Data for Material
 */
@XStreamAlias("E1MARCM")
public class SapMaterialPlantData
{
	@XStreamAlias("STAWN")
	private String harmonizationCode;

	public String getHarmonizationCode()
	{
		return harmonizationCode;
	}

	@Override
	public String toString()
	{
		return "SapMaterialPlantData{" +
				"harmonizationCode='" + harmonizationCode + '\'' +
				"}";
	}
}
