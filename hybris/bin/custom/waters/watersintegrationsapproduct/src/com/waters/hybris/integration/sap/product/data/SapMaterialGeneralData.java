package com.waters.hybris.integration.sap.product.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created by ramana on 04/04/2018.
 *
 * SAP table MARA - General Material Data
 */
@XStreamAlias("E1MARAM")
public class SapMaterialGeneralData
{
	@XStreamAlias("MATNR")
	private String materialNumber;
	@XStreamAlias("PRDHA")
	private String productHierarchy;
	@XStreamAlias("XCHPF")
	private String batchManaged;
	@XStreamAlias("MHDRZ")
	private String remainingShelfLife;
	@XStreamAlias("BEHVO")
	private String containerRequirement;
	@XStreamAlias("STOFF")
	private String hazardMaterialNumber;
	@XStreamAlias("BRGEW")
	private String weight;
	@XStreamAlias("GEWEI")
	private String weightUnit;
	@XStreamAlias("MSTAE")
	private String crossPlantStatus;
	@XStreamImplicit(itemFieldName = "E1MAKTM")
	private List<SapMaterialDescriptionData> descriptionData;
	@XStreamImplicit(itemFieldName = "E1MVKEM")
	private List<SapMaterialSalesData> salesData;
	@XStreamImplicit(itemFieldName = "E1MARCM")
	private List<SapMaterialPlantData> plantData;
	@XStreamAlias("E1MARA1")
	private SapMaterialAdditionalData additionalData;

	public String getMaterialNumber()
	{
		return materialNumber;
	}

	public String getProductHierarchy()
	{
		return productHierarchy;
	}

	public String getBatchManaged()
	{
		return batchManaged;
	}

	public String getRemainingShelfLife()
	{
		return remainingShelfLife;
	}

	public String getContainerRequirement()
	{
		return containerRequirement;
	}

	public String getHazardMaterialNumber()
	{
		return hazardMaterialNumber;
	}

	public String getWeight()
	{
		return weight;
	}

	public String getWeightUnit()
	{
		return weightUnit;
	}

	public String getCrossPlantStatus()
	{
		return crossPlantStatus;
	}

	public List<SapMaterialDescriptionData> getDescriptionData()
	{
		return descriptionData;
	}

	public List<SapMaterialSalesData> getSalesData()
	{
		return salesData;
	}

	public List<SapMaterialPlantData> getPlantData()
	{
		return plantData;
	}

	public SapMaterialAdditionalData getAdditionalData()
	{
		return additionalData;
	}

	@Override
	public String toString()
	{
		return "SapMaterialGeneralData{" +
				"materialNumber='" + materialNumber + '\'' +
				", productHierarchy='" + productHierarchy + '\'' +
				", batchManaged='" + batchManaged + '\'' +
				", remainingShelfLife='" + remainingShelfLife + '\'' +
				", containerRequirement='" + containerRequirement + '\'' +
				", hazardMaterialNumber='" + hazardMaterialNumber + '\'' +
				", weight='" + weight + '\'' +
				", weightUnit='" + weightUnit + '\'' +
				", crossPlantStatus='" + crossPlantStatus + '\'' +
				", descriptionData='" + descriptionData + '\'' +
				", salesData='" + salesData + '\'' +
				", plantData='" + plantData + '\'' +
				", additionalData='" + additionalData + '\'' +
				"}";
	}
}
