package com.waters.hybris.integration.sap.product.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by ramana on 04/04/2018.
 *
 * SAP table MAKT - Material Descriptions
 */
@XStreamAlias("E1MAKTM")
public class SapMaterialDescriptionData
{
	@XStreamAlias("SPRAS_ISO")
	private String languageIso;
	@XStreamAlias("MAKTX")
	private String description;

	public String getLanguageIso()
	{
		return languageIso;
	}

	public String getDescription()
	{
		return description;
	}

	@Override
	public String toString()
	{
		return "SapMaterialDescriptionData{" +
				"languageIso='" + languageIso + '\'' +
				", description='" + description + '\'' +
				"}";
	}
}
