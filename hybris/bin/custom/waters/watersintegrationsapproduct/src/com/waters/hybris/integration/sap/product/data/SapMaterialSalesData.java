package com.waters.hybris.integration.sap.product.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by ramana on 04/04/2018.
 *
 * SAP table MVKE - Sales Data for Material
 */
@XStreamAlias("E1MVKEM")
public class SapMaterialSalesData
{
	@XStreamAlias("VMSTA")
	String chainSpecificStatus;
	@XStreamAlias("PRATA")
	String attribute10;

	public String getChainSpecificStatus()
	{
		return chainSpecificStatus;
	}

	public String getAttribute10()
	{
		return attribute10;
	}

	@Override
	public String toString()
	{
		return "SapMaterialSalesData{" +
				"chainSpecificStatus='" + chainSpecificStatus + '\'' +
				", attribute10='" + attribute10 + '\'' +
				"}";
	}
}
