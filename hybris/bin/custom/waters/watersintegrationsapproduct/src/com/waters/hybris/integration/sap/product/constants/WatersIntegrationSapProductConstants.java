/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.waters.hybris.integration.sap.product.constants;

/**
 * Global class for all Watersintegrationsapproduct constants. You can add global constants for your extension into this class.
 */
public final class WatersIntegrationSapProductConstants extends GeneratedWatersIntegrationSapProductConstants
{
	public static final String EXTENSIONNAME = "watersintegrationsapproduct";

	public static final String SAP_MATERIAL_MASTER_DATA_IMPORT_METRICS_BASE_NAME = "sap.materialMasterDataImport.";

	public static final String CLASSIFICATION_CLASS_PRODUCTS = "Products";
	public static final String CLASSIFICATION_ATTRIBUTE_SAP_HIERARCHY = "saphierarchy";
	public static final String CLASSIFICATION_ATTRIBUTE_BATCH_MANAGED = "batchmanaged";
	public static final String CLASSIFICATION_ATTRIBUTE_REMAINING_SHELF_LIFE = "remainingshelflife";
	public static final String CLASSIFICATION_ATTRIBUTE_CONTAINER_REQUIREMENT = "containerrequirement";
	public static final String CLASSIFICATION_ATTRIBUTE_HAZARD_MATERIAL_NUMBER = "hazardmaterialnumber";
	public static final String CLASSIFICATION_ATTRIBUTE_HAZARD_MATERIAL_TEXT = "hazardmaterialtext";
	public static final String CLASSIFICATION_ATTRIBUTE_WEIGHT = "weight";
	public static final String CLASSIFICATION_ATTRIBUTE_WEIGHT_UNIT = "weightunit";
	public static final String CLASSIFICATION_ATTRIBUTE_HARMONIZATION_CODE = "harmonizationcode";
	public static final String CLASSIFICATION_ATTRIBUTE_DG_TECH_NAME = "dgtechname";

	public static final String PLATFORM_LOGO_CODE = "watersintegrationsapproductPlatformLogo";

	// implement here constants used by this extension

	private WatersIntegrationSapProductConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
