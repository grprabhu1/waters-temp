@SapMaterialMasterFeed @Transaction
Feature: Sap Material Master Feed

  @AdminUser
  Scenario: Sap Material Master Feed
    Given Sap material master feed record setup
    Given That the Impex "createTestMaterialMasterFileImportRecords.impex" to "Setup SAP material master feed record" has been executed
    When the "materialMasterFeedImportJob" has been run
    And the job completed successfully
    Then the following products are available
        | id | catalog | catalogVersion |
        | WAT073109-TEST | watersProductCatalog | Staged |
    And the following products are available with sap data
        | id | catalog | catalogVersion | description | hierarchy| batchManaged | remainingSelfLife | containerRequirement | hazardousMaterialNumber | hazardousMaterialText | weight | weightUom | harmonizationCode | dgTechName |
        | WAT073109-TEST | watersProductCatalog | Staged | SYRINGE FOR 710B WISP 250U | 034861100 | | 0      |                   |                   |                       | 442.000 | GRM      | 25K               |            |
        | 176001235 | watersProductCatalog | Staged | AccQ-Tag Ultra Chemistry Kit | 176001235-hierarchy | Yes | 100      |  176001235-containerRequirement                 |  176001235-hazardousNumber  | 176001235-hazardMaterialText | 100.00 | KG      | 100K               | 176001235-dgTechName           |
    And the following products are not available
        | id | catalog | catalogVersion |
        | 186003064-TEST | watersProductCatalog | Staged |
        | WAT094293-TEST | watersProductCatalog | Staged |
        | 700004045-TEST | watersProductCatalog | Staged |

