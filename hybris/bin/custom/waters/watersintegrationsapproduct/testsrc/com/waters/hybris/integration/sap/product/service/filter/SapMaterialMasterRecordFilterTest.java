package com.waters.hybris.integration.sap.product.service.filter;

import com.waters.hybris.integration.sap.product.data.SapMaterialGeneralData;
import com.waters.hybris.integration.sap.product.data.SapMaterialMasterRecord;
import com.waters.hybris.integration.sap.product.data.SapMaterialSalesData;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

/**
 * Created by ramana on 06/04/2018.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SapMaterialMasterRecordFilterTest
{
	private SapMaterialMasterRecordFilter filter = new SapMaterialMasterRecordFilter();

	@Mock
	private SapMaterialMasterRecord sapMaterialMasterRecord;
	@Mock
	private SapMaterialGeneralData sapMaterialGeneralData;
	@Mock
	private SapMaterialSalesData sapMaterialSalesData;

	@Before
	public void setUp()
	{
		given(sapMaterialMasterRecord.getGeneralData()).willReturn(sapMaterialGeneralData);
		given(sapMaterialGeneralData.getSalesData()).willReturn(Collections.singletonList(sapMaterialSalesData));
	}

	@Test
	public void shouldReturnFalse_WhenCrossPlantStatusDoesNotMatch()
	{
		given(sapMaterialGeneralData.getCrossPlantStatus()).willReturn("NO");
		assertThat(filter.matches(sapMaterialMasterRecord)).isFalse();
	}

	@Test
	public void shouldReturnFalse_WhenSalesChainSpecificStatusDoesNotMatch()
	{
		given(sapMaterialSalesData.getChainSpecificStatus()).willReturn("N");
		assertThat(filter.matches(sapMaterialMasterRecord)).isFalse();
	}

	@Test
	public void shouldReturnFalse_WhenAttribute10DoesNotMatch()
	{
		given(sapMaterialSalesData.getAttribute10()).willReturn("NULL");
		assertThat(filter.matches(sapMaterialMasterRecord)).isFalse();
	}

	@Test
	public void shouldReturnTrue_WhenAllMatch_PP()
	{
		given(sapMaterialGeneralData.getCrossPlantStatus()).willReturn(SapMaterialMasterRecordFilter.CROSS_PLANT_STATUS_PP);
		given(sapMaterialSalesData.getChainSpecificStatus()).willReturn(SapMaterialMasterRecordFilter.CHAIN_SPECIFIC_STATUS_A);
		given(sapMaterialSalesData.getAttribute10()).willReturn(SapMaterialMasterRecordFilter.ATTRIBUTE_10_X);
		assertThat(filter.matches(sapMaterialMasterRecord)).isTrue();
	}

	@Test
	public void shouldReturnTrue_WhenAllMatch_RG()
	{
		given(sapMaterialGeneralData.getCrossPlantStatus()).willReturn(SapMaterialMasterRecordFilter.CROSS_PLANT_STATUS_RG);
		given(sapMaterialSalesData.getChainSpecificStatus()).willReturn(SapMaterialMasterRecordFilter.CHAIN_SPECIFIC_STATUS_A);
		given(sapMaterialSalesData.getAttribute10()).willReturn(SapMaterialMasterRecordFilter.ATTRIBUTE_10_X);
		assertThat(filter.matches(sapMaterialMasterRecord)).isTrue();
	}
}
