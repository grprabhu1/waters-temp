package com.waters.hybris.integration.sap.product.service.impl;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.neoworks.hybris.util.BatchCommitter;
import com.waters.hybris.core.model.model.WatersProductModel;
import com.waters.hybris.core.strategies.SavedValuesStrategy;
import com.waters.hybris.integration.core.context.ImportContext;
import com.waters.hybris.integration.core.filter.ValueFilter;
import com.waters.hybris.integration.core.strategy.ModelServiceSelectionStrategy;
import com.waters.hybris.integration.sap.product.data.SapMaterialAdditionalData;
import com.waters.hybris.integration.sap.product.data.SapMaterialDescriptionData;
import com.waters.hybris.integration.sap.product.data.SapMaterialGeneralData;
import com.waters.hybris.integration.sap.product.data.SapMaterialHybExtData;
import com.waters.hybris.integration.sap.product.data.SapMaterialMasterRecord;
import com.waters.hybris.integration.sap.product.data.SapMaterialPlantData;
import com.waters.hybris.integration.sap.product.data.SapMaterialSalesData;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.user.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by ramana on 06/04/2018.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SapMaterialMasterImporterTest
{
	// data
	private static final String DE_DESCRIPTION = "DE_DESCRIPTION";
	private static final String EN_DESCRIPTION = "EN_DESCRIPTION";
	private static final String FR_DESCRIPTION = "FR_DESCRIPTION";
	private static final String WEIGHT = "123";
	private static final String WEIGHT_UNIT_KG = "KG";
	private static final String BATCH_MANAGED = "BATCH_MANAGED";
	private static final String CONTAINER_REQUIREMENT = "CONTAINER_REQUIREMENT";
	private static final String PRODUCT_HIERARCHY = "PRODUCT_HIERARCHY";
	private static final String REMAINING_SHELF_LIFE = "25";
	private static final String DG_TECH_NAME = "DG_TECH_NAME";
	private static final String HAZARD_TEXT = "HAZARD_TEXT";
	private static final String HARMONIZATION_CODE = "HARMONIZATION_CODE";
	private static final String HAZARD_MATERIAL_NUMBER = "HazardMaterialNumber";

	private static final String NEW_PROD_CODE = "NEW";

	private static final String OLD_PROD_CODE = "OLD";
	private static final String WEIGHT_NEW = "124";

	@InjectMocks
	private SapMaterialMasterImporter importer = new SapMaterialMasterImporter();

	@Mock
	private ValueFilter<SapMaterialMasterRecord> materialMasterDataValueFilter;
	@Mock
	private ProductService productService;
	@Mock
	private CatalogVersionService catalogVersionService;
	@Mock
	private ClassificationService classificationService;
	@Mock
	private TypeService typeService;
	@Mock
	private ClassificationSystemService classificationSystemService;
	@Mock
	private ModelServiceSelectionStrategy modelServiceSelectionStrategy;
	@Mock
	private ModelService modelService;
	@Mock
	private MetricRegistry metricRegistry;
	@Mock
	private Timer timer;
	@Mock
	private ImportContext context;
	@Mock
	private BatchCommitter batchCommitter;
	@Mock
	private CatalogVersionModel catalogVersion;
	@Mock
	private WatersProductModel newProduct;
	@Mock
	private WatersProductModel oldProduct;
	@Mock
	private UserService userService;
	@Mock
	private SavedValuesStrategy savedValuesStrategy;

	@Mock
	private SapMaterialMasterRecord sapMaterialMasterRecord;
	@Mock
	private SapMaterialGeneralData sapMaterialGeneralData;
	@Mock
	private SapMaterialSalesData sapMaterialSalesData;
	@Mock
	private SapMaterialPlantData sapMaterialPlantData;
	@Mock
	private SapMaterialDescriptionData sapMaterialDescriptionData1;
	@Mock
	private SapMaterialDescriptionData sapMaterialDescriptionData2;
	@Mock
	private SapMaterialDescriptionData sapMaterialDescriptionData3;
	@Mock
	private SapMaterialAdditionalData sapMaterialAdditionalData;
	@Mock
	private SapMaterialHybExtData sapMaterialHybExtData;

	@Mock
	private FeatureList newFeatureList;
	@Mock
	private Feature newFeatureHazardNumber;
	@Mock
	private Feature newFeatureHazardText;
	@Mock
	private Feature newFeatureWeight;
	@Mock
	private Feature newFeatureWeightUnit;
	@Mock
	private Feature newFeatureSapHierarchy;
	@Mock
	private Feature newFeatureBatchManaged;
	@Mock
	private Feature newFeatureRemainingShelfLife;
	@Mock
	private Feature newFeatureContainerRequirement;
	@Mock
	private Feature newFeatureHarmonizationCode;
	@Mock
	private Feature newFeatureDGTechName;

	@Mock
	private FeatureList oldFeatureList;
	@Mock
	private Feature oldFeatureHazardNumber;
	@Mock
	private FeatureValue oldFeatureHazardNumberValue;
	@Mock
	private Feature oldFeatureHazardText;
	@Mock
	private FeatureValue oldFeatureHazardTextValue;
	@Mock
	private Feature oldFeatureWeight;
	@Mock
	private FeatureValue oldFeatureWeightValue;
	@Mock
	private Feature oldFeatureWeightUnit;
	@Mock
	private FeatureValue oldFeatureWeightUnitValue;
	@Mock
	private Feature oldFeatureSapHierarchy;
	@Mock
	private FeatureValue oldFeatureSapHierarchyValue;
	@Mock
	private Feature oldFeatureBatchManaged;
	@Mock
	private FeatureValue oldFeatureBatchManagedValue;
	@Mock
	private Feature oldFeatureRemainingShelfLife;
	@Mock
	private FeatureValue oldFeatureRemainingShelfLifeValue;
	@Mock
	private Feature oldFeatureContainerRequirement;
	@Mock
	private FeatureValue oldFeatureContainerRequirementValue;
	@Mock
	private Feature oldFeatureHarmonizationCode;
	@Mock
	private FeatureValue oldFeatureHarmonizationCodeValue;
	@Mock
	private Feature oldFeatureDGTechName;
	@Mock
	private FeatureValue oldFeatureDGTechNameValue;

	@Before
	public void setUp()
	{
		given(context.getBatchCommitter()).willReturn(batchCommitter);
		given(metricRegistry.timer(any())).willReturn(timer);
		given(modelServiceSelectionStrategy.selectModelService(context)).willReturn(modelService);
		given(catalogVersionService.getCatalogVersion(any(), any())).willReturn(catalogVersion);

		setupMaterialMasterData();

		// new product
		given(modelService.create(WatersProductModel.class)).willReturn(newProduct);
		setupNewProduct();

		// old product
		given(productService.getProductForCode(eq(catalogVersion), eq(OLD_PROD_CODE))).willReturn(oldProduct);
		setupOldProduct();
	}

	@Test
	public void shouldDoNothing_WhenNotForSystem()
	{
		given(materialMasterDataValueFilter.matches(sapMaterialMasterRecord)).willReturn(false);

		importer.internalProcess(sapMaterialMasterRecord, context);
		verify(productService, never()).getProductForCode(any(), any());
	}

	@Test
	public void shouldCreateNewProduct_WhenNotExistsInSystem()
	{
		given(materialMasterDataValueFilter.matches(sapMaterialMasterRecord)).willReturn(true);

		when(sapMaterialGeneralData.getMaterialNumber()).thenReturn(NEW_PROD_CODE);

		importer.internalProcess(sapMaterialMasterRecord, context);

		verify(productService, times(1)).getProductForCode(any(), any());
		verify(modelService, times(1)).create(WatersProductModel.class);

		verify(newProduct, times(1)).setName(NEW_PROD_CODE);
		verify(newProduct, times(1)).setSapProductTitle(EN_DESCRIPTION);

		verifyFeatureValueAdded(newFeatureSapHierarchy, PRODUCT_HIERARCHY);
		verifyFeatureValueAdded(newFeatureBatchManaged, BATCH_MANAGED);
		verifyFeatureValueAdded(newFeatureRemainingShelfLife, REMAINING_SHELF_LIFE);
		verifyFeatureValueAdded(newFeatureContainerRequirement, CONTAINER_REQUIREMENT);
		verifyFeatureValueAdded(newFeatureWeight, WEIGHT);
		verifyFeatureValueAdded(newFeatureWeightUnit, WEIGHT_UNIT_KG);
		verifyFeatureValueAdded(newFeatureHarmonizationCode, HARMONIZATION_CODE);
		verifyFeatureValueAdded(newFeatureHazardNumber, HAZARD_MATERIAL_NUMBER);
		verifyFeatureValueAdded(newFeatureHazardText, HAZARD_TEXT);
		verifyFeatureValueAdded(newFeatureDGTechName, DG_TECH_NAME);
	}

	@Test
	public void shouldUpdateOldProduct_WhenExistsInSystem()
	{
		given(materialMasterDataValueFilter.matches(sapMaterialMasterRecord)).willReturn(true);

		when(sapMaterialGeneralData.getMaterialNumber()).thenReturn(OLD_PROD_CODE);

		importer.internalProcess(sapMaterialMasterRecord, context);

		verify(productService, times(1)).getProductForCode(any(), any());
		verify(modelService, never()).create(WatersProductModel.class);
		verify(oldProduct, times(1)).setName(OLD_PROD_CODE);
		verify(oldProduct, times(1)).setSapProductTitle(EN_DESCRIPTION);

		verifyFeatureValueModified(oldFeatureWeightValue, WEIGHT);
		verifyFeatureValueModified(oldFeatureWeightUnitValue, WEIGHT_UNIT_KG);
		verifyFeatureValueModified(oldFeatureHazardTextValue, HAZARD_TEXT);

		verifyFeatureValueAdded(oldFeatureSapHierarchy, PRODUCT_HIERARCHY);
		verifyFeatureValueAdded(oldFeatureBatchManaged, BATCH_MANAGED);
		verifyFeatureValueAdded(oldFeatureRemainingShelfLife, REMAINING_SHELF_LIFE);
		verifyFeatureValueAdded(oldFeatureContainerRequirement, CONTAINER_REQUIREMENT);
		verifyFeatureValueAdded(oldFeatureHarmonizationCode, HARMONIZATION_CODE);
		verifyFeatureValueAdded(oldFeatureHazardNumber, HAZARD_MATERIAL_NUMBER);
		verifyFeatureValueAdded(oldFeatureDGTechName, DG_TECH_NAME);
	}

	@Test
	public void shouldNotUpdateOldProduct_WhenExistsInSystemAndFeaturesAreNotChanged()
	{
		setupOldProductValues();
		given(materialMasterDataValueFilter.matches(sapMaterialMasterRecord)).willReturn(true);

		when(sapMaterialGeneralData.getMaterialNumber()).thenReturn(OLD_PROD_CODE);


		importer.internalProcess(sapMaterialMasterRecord, context);

		verify(productService, times(1)).getProductForCode(any(), any());
		verify(modelService, never()).create(WatersProductModel.class);
		verify(oldProduct, times(1)).setName(OLD_PROD_CODE);
		verify(oldProduct, times(1)).setSapProductTitle(EN_DESCRIPTION);

		verify(classificationService, never()).replaceFeatures(oldProduct, oldFeatureList);
	}

	@Test
	public void shouldUpdateOldProduct_WhenExistsInSystemAndFeaturesAreChanged()
	{
		setupOldProductValues();
		given(materialMasterDataValueFilter.matches(sapMaterialMasterRecord)).willReturn(true);

		when(sapMaterialGeneralData.getMaterialNumber()).thenReturn(OLD_PROD_CODE);

		given(oldFeatureSapHierarchy.getValue().getValue()).willReturn(PRODUCT_HIERARCHY);
		given(oldFeatureBatchManaged.getValue().getValue()).willReturn(BATCH_MANAGED);
		given(oldFeatureRemainingShelfLife.getValue().getValue()).willReturn(REMAINING_SHELF_LIFE);
		given(oldFeatureContainerRequirement.getValue().getValue()).willReturn(CONTAINER_REQUIREMENT);
		given(oldFeatureHarmonizationCode.getValue().getValue()).willReturn(HARMONIZATION_CODE);
		given(oldFeatureHazardNumber.getValue().getValue()).willReturn(HAZARD_MATERIAL_NUMBER);
		given(oldFeatureHazardText.getValue().getValue()).willReturn(HAZARD_TEXT);
		given(oldFeatureDGTechName.getValue().getValue()).willReturn(DG_TECH_NAME);
		given(oldFeatureWeight.getValue().getValue()).willReturn(WEIGHT_NEW);
		given(oldFeatureWeightUnit.getValue().getValue()).willReturn(WEIGHT_UNIT_KG);

		importer.internalProcess(sapMaterialMasterRecord, context);

		verify(productService, times(1)).getProductForCode(any(), any());
		verify(modelService, never()).create(WatersProductModel.class);
		verify(oldProduct, times(1)).setName(OLD_PROD_CODE);
		verify(oldProduct, times(1)).setSapProductTitle(EN_DESCRIPTION);

		verify(classificationService, times(1)).replaceFeatures(oldProduct, oldFeatureList);
	}


	@Test
	public void shouldNotAddAnyFeatures_WhenProductNotClassified()
	{
		given(materialMasterDataValueFilter.matches(sapMaterialMasterRecord)).willReturn(true);

		when(sapMaterialGeneralData.getMaterialNumber()).thenReturn(NEW_PROD_CODE);
		given(classificationService.getFeatures(newProduct)).willReturn(null);

		importer.internalProcess(sapMaterialMasterRecord, context);

		verify(newFeatureList, never()).getFeatureByCode(any());

		verify(newFeatureSapHierarchy, never()).addValue(any());
		verify(newFeatureBatchManaged, never()).addValue(any());
		verify(newFeatureRemainingShelfLife, never()).addValue(any());
		verify(newFeatureContainerRequirement, never()).addValue(any());
		verify(newFeatureWeight, never()).addValue(any());
		verify(newFeatureWeightUnit, never()).addValue(any());
		verify(newFeatureHarmonizationCode, never()).addValue(any());
		verify(newFeatureHazardNumber, never()).addValue(any());
		verify(newFeatureHazardText, never()).addValue(any());
		verify(newFeatureDGTechName, never()).addValue(any());

		verify(classificationService, never()).replaceFeatures(newProduct, newFeatureList);
	}

	@Test
	public void shouldNotAddFeature_WhenNoValuePresent()
	{
		given(materialMasterDataValueFilter.matches(sapMaterialMasterRecord)).willReturn(true);

		when(sapMaterialGeneralData.getMaterialNumber()).thenReturn(NEW_PROD_CODE);
		when(sapMaterialHybExtData.getDgTechName()).thenReturn(null);  // no data received for DG Tech Name

		importer.internalProcess(sapMaterialMasterRecord, context);

		verifyFeatureValueAdded(newFeatureSapHierarchy, PRODUCT_HIERARCHY);
		verifyFeatureValueAdded(newFeatureBatchManaged, BATCH_MANAGED);
		verifyFeatureValueAdded(newFeatureRemainingShelfLife, REMAINING_SHELF_LIFE);
		verifyFeatureValueAdded(newFeatureContainerRequirement, CONTAINER_REQUIREMENT);
		verifyFeatureValueAdded(newFeatureWeight, WEIGHT);
		verifyFeatureValueAdded(newFeatureWeightUnit, WEIGHT_UNIT_KG);
		verifyFeatureValueAdded(newFeatureHarmonizationCode, HARMONIZATION_CODE);
		verifyFeatureValueAdded(newFeatureHazardNumber, HAZARD_MATERIAL_NUMBER);
		verifyFeatureValueAdded(newFeatureHazardText, HAZARD_TEXT);

		verify(newFeatureDGTechName, never()).addValue(any());
	}

	@Test
	public void shouldRemoveExistingFeatureValue_WhenNoValuePresent()
	{
		given(materialMasterDataValueFilter.matches(sapMaterialMasterRecord)).willReturn(true);

		when(sapMaterialGeneralData.getMaterialNumber()).thenReturn(OLD_PROD_CODE);
		when(sapMaterialGeneralData.getWeight()).thenReturn(null);
		when(sapMaterialGeneralData.getWeightUnit()).thenReturn(null);

		importer.internalProcess(sapMaterialMasterRecord, context);

		verify(oldFeatureWeight, times(1)).removeValue(oldFeatureWeightValue);
		verify(oldFeatureWeightUnit, times(1)).removeValue(oldFeatureWeightUnitValue);

		verifyFeatureValueModified(oldFeatureHazardTextValue, HAZARD_TEXT);

		verifyFeatureValueAdded(oldFeatureSapHierarchy, PRODUCT_HIERARCHY);
		verifyFeatureValueAdded(oldFeatureBatchManaged, BATCH_MANAGED);
		verifyFeatureValueAdded(oldFeatureRemainingShelfLife, REMAINING_SHELF_LIFE);
		verifyFeatureValueAdded(oldFeatureContainerRequirement, CONTAINER_REQUIREMENT);
		verifyFeatureValueAdded(oldFeatureHarmonizationCode, HARMONIZATION_CODE);
		verifyFeatureValueAdded(oldFeatureHazardNumber, HAZARD_MATERIAL_NUMBER);
		verifyFeatureValueAdded(oldFeatureDGTechName, DG_TECH_NAME);
	}

	protected void verifyFeatureValueModified(final FeatureValue featureValue, final String value)
	{
		verify(featureValue, times(1)).setValue(value);
	}

	protected void verifyFeatureValueAdded(final Feature feature, final String value)
	{
		ArgumentCaptor<FeatureValue> argument = ArgumentCaptor.forClass(FeatureValue.class);
		verify(feature).addValue(argument.capture());

		assertEquals(value, argument.getValue().getValue());
	}

	protected void setupMaterialMasterData()
	{
		given(sapMaterialMasterRecord.getGeneralData()).willReturn(sapMaterialGeneralData);
		given(sapMaterialGeneralData.getSalesData()).willReturn(Collections.singletonList(sapMaterialSalesData));
		given(sapMaterialGeneralData.getPlantData()).willReturn(Collections.singletonList(sapMaterialPlantData));
		given(sapMaterialGeneralData.getDescriptionData()).willReturn(Arrays.asList(sapMaterialDescriptionData1, sapMaterialDescriptionData2, sapMaterialDescriptionData3));
		given(sapMaterialGeneralData.getAdditionalData()).willReturn(sapMaterialAdditionalData);
		given(sapMaterialAdditionalData.getHybExtData()).willReturn(sapMaterialHybExtData);
		given(sapMaterialDescriptionData1.getLanguageIso()).willReturn("DE");
		given(sapMaterialDescriptionData2.getLanguageIso()).willReturn(SapMaterialMasterImporter.LANGUAGE_EN);
		given(sapMaterialDescriptionData3.getLanguageIso()).willReturn("FR");
		given(sapMaterialDescriptionData1.getDescription()).willReturn(DE_DESCRIPTION);
		given(sapMaterialDescriptionData2.getDescription()).willReturn(EN_DESCRIPTION);
		given(sapMaterialDescriptionData3.getDescription()).willReturn(FR_DESCRIPTION);
		given(sapMaterialGeneralData.getHazardMaterialNumber()).willReturn(HAZARD_MATERIAL_NUMBER);
		given(sapMaterialGeneralData.getWeight()).willReturn(WEIGHT);
		given(sapMaterialGeneralData.getWeightUnit()).willReturn(WEIGHT_UNIT_KG);
		given(sapMaterialGeneralData.getBatchManaged()).willReturn(BATCH_MANAGED);
		given(sapMaterialGeneralData.getContainerRequirement()).willReturn(CONTAINER_REQUIREMENT);
		given(sapMaterialGeneralData.getProductHierarchy()).willReturn(PRODUCT_HIERARCHY);
		given(sapMaterialGeneralData.getRemainingShelfLife()).willReturn(REMAINING_SHELF_LIFE);
		given(sapMaterialHybExtData.getDgTechName()).willReturn(DG_TECH_NAME);
		given(sapMaterialHybExtData.getHazardMaterialText()).willReturn(HAZARD_TEXT);
		given(sapMaterialPlantData.getHarmonizationCode()).willReturn(HARMONIZATION_CODE);
	}

	protected void setupNewProduct()
	{
		given(classificationService.getFeatures(newProduct)).willReturn(newFeatureList);
		given(newFeatureList.getFeatureByCode("WatersClassification/1.0/Products.hazardmaterialnumber")).willReturn(newFeatureHazardNumber);
		given(newFeatureList.getFeatureByCode("WatersClassification/1.0/Products.hazardmaterialtext")).willReturn(newFeatureHazardText);
		given(newFeatureList.getFeatureByCode("WatersClassification/1.0/Products.weight")).willReturn(newFeatureWeight);
		given(newFeatureList.getFeatureByCode("WatersClassification/1.0/Products.weightunit")).willReturn(newFeatureWeightUnit);
		given(newFeatureList.getFeatureByCode("WatersClassification/1.0/Products.saphierarchy")).willReturn(newFeatureSapHierarchy);
		given(newFeatureList.getFeatureByCode("WatersClassification/1.0/Products.batchmanaged")).willReturn(newFeatureBatchManaged);
		given(newFeatureList.getFeatureByCode("WatersClassification/1.0/Products.remainingshelflife")).willReturn(newFeatureRemainingShelfLife);
		given(newFeatureList.getFeatureByCode("WatersClassification/1.0/Products.containerrequirement")).willReturn(newFeatureContainerRequirement);
		given(newFeatureList.getFeatureByCode("WatersClassification/1.0/Products.harmonizationcode")).willReturn(newFeatureHarmonizationCode);
		given(newFeatureList.getFeatureByCode("WatersClassification/1.0/Products.dgtechname")).willReturn(newFeatureDGTechName);
	}

	protected void setupOldProduct()
	{
		given(classificationService.getFeatures(oldProduct)).willReturn(oldFeatureList);
		given(oldFeatureList.getFeatureByCode("WatersClassification/1.0/Products.hazardmaterialnumber")).willReturn(oldFeatureHazardNumber);
		given(oldFeatureList.getFeatureByCode("WatersClassification/1.0/Products.hazardmaterialtext")).willReturn(oldFeatureHazardText);
		given(oldFeatureHazardText.getValue()).willReturn(oldFeatureHazardTextValue);
		given(oldFeatureList.getFeatureByCode("WatersClassification/1.0/Products.weight")).willReturn(oldFeatureWeight);
		given(oldFeatureWeight.getValue()).willReturn(oldFeatureWeightValue);
		given(oldFeatureList.getFeatureByCode("WatersClassification/1.0/Products.weightunit")).willReturn(oldFeatureWeightUnit);
		given(oldFeatureWeightUnit.getValue()).willReturn(oldFeatureWeightUnitValue);
		given(oldFeatureList.getFeatureByCode("WatersClassification/1.0/Products.saphierarchy")).willReturn(oldFeatureSapHierarchy);
		given(oldFeatureList.getFeatureByCode("WatersClassification/1.0/Products.batchmanaged")).willReturn(oldFeatureBatchManaged);
		given(oldFeatureList.getFeatureByCode("WatersClassification/1.0/Products.remainingshelflife")).willReturn(oldFeatureRemainingShelfLife);
		given(oldFeatureList.getFeatureByCode("WatersClassification/1.0/Products.containerrequirement")).willReturn(oldFeatureContainerRequirement);
		given(oldFeatureList.getFeatureByCode("WatersClassification/1.0/Products.harmonizationcode")).willReturn(oldFeatureHarmonizationCode);
		given(oldFeatureList.getFeatureByCode("WatersClassification/1.0/Products.dgtechname")).willReturn(oldFeatureDGTechName);
	}

	protected void setupOldProductValues()
	{
		given(oldFeatureSapHierarchy.getValue()).willReturn(oldFeatureSapHierarchyValue);
		given(oldFeatureBatchManaged.getValue()).willReturn(oldFeatureBatchManagedValue);
		given(oldFeatureRemainingShelfLife.getValue()).willReturn(oldFeatureRemainingShelfLifeValue);
		given(oldFeatureContainerRequirement.getValue()).willReturn(oldFeatureContainerRequirementValue);
		given(oldFeatureHarmonizationCode.getValue()).willReturn(oldFeatureHarmonizationCodeValue);
		given(oldFeatureHazardNumber.getValue()).willReturn(oldFeatureHazardNumberValue);
		given(oldFeatureHazardText.getValue()).willReturn(oldFeatureHazardTextValue);
		given(oldFeatureDGTechName.getValue()).willReturn(oldFeatureDGTechNameValue);
		given(oldFeatureWeight.getValue()).willReturn(oldFeatureWeightValue);
		given(oldFeatureWeightUnit.getValue()).willReturn(oldFeatureWeightUnitValue);

		given(oldFeatureSapHierarchyValue.getValue()).willReturn(PRODUCT_HIERARCHY);
		given(oldFeatureBatchManagedValue.getValue()).willReturn(BATCH_MANAGED);
		given(oldFeatureRemainingShelfLifeValue.getValue()).willReturn(REMAINING_SHELF_LIFE);
		given(oldFeatureContainerRequirementValue.getValue()).willReturn(CONTAINER_REQUIREMENT);
		given(oldFeatureHarmonizationCodeValue.getValue()).willReturn(HARMONIZATION_CODE);
		given(oldFeatureHazardNumberValue.getValue()).willReturn(HAZARD_MATERIAL_NUMBER);
		given(oldFeatureHazardTextValue.getValue()).willReturn(HAZARD_TEXT);
		given(oldFeatureDGTechNameValue.getValue()).willReturn(DG_TECH_NAME);
		given(oldFeatureWeightValue.getValue()).willReturn(WEIGHT);
		given(oldFeatureWeightUnitValue.getValue()).willReturn(WEIGHT_UNIT_KG);
	}
}
