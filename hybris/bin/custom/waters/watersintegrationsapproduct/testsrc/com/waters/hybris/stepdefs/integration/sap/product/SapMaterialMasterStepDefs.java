package com.waters.hybris.stepdefs.integration.sap.product;

import cucumber.api.java.en.Given;
import org.apache.log4j.Logger;

/**
 * Created by ramana on 09/04/2018.
 */
public class SapMaterialMasterStepDefs
{
	private static final Logger LOG = Logger.getLogger(SapMaterialMasterStepDefs.class);

	@Given("^Sap material master feed record setup$")
	public void givenSapMaterialMasterFeedSetup()
	{
		LOG.info("Sap material master feed setup");
	}
}
