<%@ page import="de.hybris.platform.hmc.attribute.AbstractAttributeEditorChip" %>
<%@ page import="de.hybris.platform.hmc.webchips.AbstractChip" %>
<%@ page import="com.neoworks.hybris.hmc.attribute.DynamicTypeValueEditorChip" %>
<%@include file="../../head.inc"%>
<%--
  ~ Copyright (c) 2014-2018 Salmon Limited
  ~ Copyright (c) 2011-2014 Neoworks Limited
  ~ All rights reserved.
  --%>

<%
	final DynamicTypeValueEditorChip theChip = (DynamicTypeValueEditorChip) request.getAttribute(AbstractChip.CHIP_KEY);
	final AbstractAttributeEditorChip embeddedEditor = theChip.getEmbeddedEditor();
	if( embeddedEditor != null )
	{
		embeddedEditor.render( pageContext );
	}
	else
	{
%>
		<%= localized("notdefined") %>
<%
	}
%>
