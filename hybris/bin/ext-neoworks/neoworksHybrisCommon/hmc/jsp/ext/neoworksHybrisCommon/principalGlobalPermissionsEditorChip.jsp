<%@ include file="../../head.inc" %>
<%@ page import="de.hybris.platform.hmc.webchips.AbstractChip" %>
<%@ page import="java.util.List" %>
<%@ page import="com.neoworks.hybris.hmc.permissions.PrincipalGlobalPermissionsEditorChip" %>
<%--
  ~ Copyright (c) 2014-2018 Salmon Limited
  ~ Copyright (c) 2011-2014 Neoworks Limited
  ~ All rights reserved.
  --%>

<%
	final PrincipalGlobalPermissionsEditorChip theChip = (PrincipalGlobalPermissionsEditorChip) request.getAttribute(AbstractChip.CHIP_KEY);

	final String eventPermission = theChip.getEventID(PrincipalGlobalPermissionsEditorChip.PERMISSION_EVENT);
	final String eventRight = theChip.getEventID(PrincipalGlobalPermissionsEditorChip.RIGHT_EVENT);
	final String eventChangePermission = theChip.getEventID(PrincipalGlobalPermissionsEditorChip.CHANGE_PERMISSION_EVENT);

	final boolean disabled = !theChip.canChangePermissions();
	final String style = disabled ? "color: silver;" : "";

	final List<PrincipalGlobalPermissionsEditorChip.RightPermissionValue> permissionList = theChip.getPermissionList();
%>

<%!
	public String getQualifierLine(final PrincipalGlobalPermissionsEditorChip chip, final PrincipalGlobalPermissionsEditorChip.RightPermissionValue permissionValue, final boolean disabled)
	{
		if (disabled)
		{
			return "<img src='images/icons/checkbox_disabled.gif' />";
		}
		else
		{
			final String userRight = permissionValue.getUserRight().getCode();
			final String permission = permissionValue.getDeclaredPermissionValue().name();
			final String effectivePermission;
			if (PrincipalGlobalPermissionsEditorChip.PermissionValueEnum.none.equals(permissionValue.getDeclaredPermissionValue()) && !PrincipalGlobalPermissionsEditorChip.PermissionValueEnum.none.equals(permissionValue.getInheritedPermissionValue()))
			{
				effectivePermission = "super_" + permissionValue.getInheritedPermissionValue().name();
			}
			else
			{
				effectivePermission = permissionValue.getDeclaredPermissionValue().name();
			}
			return "<a href=\"#\" onclick=\"setEntityAndPermission('" + userRight + "', '" + permission + "'); setScrollAndSubmit(); return false;\">" +
					"<img id=\"" + userRight + "_" + permission + "\" src='images/icons/checkbox_" + effectivePermission + ".gif'/>" +
					"</a>";
		}
	}
%>

<script language="JavaScript1.2">
	function setEntityAndPermission(right, permission)
	{
		document.editorForm.elements['<%= eventPermission %>'].value = permission;
		document.editorForm.elements['<%= eventRight %>'].value = right;
		document.editorForm.elements['<%= eventChangePermission %>'].value = 'true';
	}
</script>

<table class="table.principalGlobalPermissionsEditorChipHeader">
	<tr>
		<td>&nbsp;</td>
		<td class="sectionheader">
			<div class="sh"><%=localized("principal.globalpermissions.section")%>&nbsp;<%=theChip.isChanged() ? "*" : ""%>
			</div>
		</td>
	</tr>
</table>

<table class="principalAccessRightsEditorChip">
	<tr>
		<td class="spacer" colspan="4">
			<div>&nbsp;</div>
		</td>
	</tr>
	<tr>
		<td class="arrowButton">&nbsp;</td>
		<td class="description" colspan="3">
			<div><%=localized("principal.globalpermissions.description")%>
			</div>
		</td>
	</tr>
	<tr>
		<td class="spacer" colspan="4">
			<div>&nbsp;</div>
		</td>
	</tr>

	<tr>
		<td class="arrowButton">&nbsp;</td>
		<td class="attrLabel">
			<div><%= localized("principal.globalpermissions.section") %>:</div>
		</td>
		<td style="vertical-align:top;">

			<table cellspacing="0" cellpadding="0" style="width:500px;">
				<%
					if (!theChip.hasItem())
					{
				%>
				<tr>
					<td class="disabled">
						<%= localized("listisempty") %>
					</td>
				</tr>
				<%
				}
				else
				{
				%>
				<tr>
					<td>
						<%-- BEGIN CONTENT --%>


						<input type="hidden" name="<%= eventPermission %>"/>
						<input type="hidden" name="<%= eventRight %>"/>
						<input type="hidden" name="<%= eventChangePermission %>"/>
						<table width="100%" class="listtable">
							<tr>
								<th style="width: 110px;">
									<div style="padding-left:2px;">
										<%= localized("principal.globalpermissions.userright") %>
									</div>
								</th>
								<th style="width: 30px;">
									<div style="padding-left:2px;">
										<%= localized("principal.globalpermissions.permission") %>
									</div>
								</th>
							</tr>
							<%
								for (final PrincipalGlobalPermissionsEditorChip.RightPermissionValue permissionValue : permissionList)
								{
							%>
							<tr>
								<td style="border-left: 1px solid #bbbbbb; width:110px; white-space:nowrap;<%= style %>">
									<div style="white-space:nowrap; overflow:hidden; padding-left:2px;">
										<%= permissionValue.getUserRight().getCode() %>
									</div>
								</td>
								<td style="text-align:center; width: 30px; border-right: 1px solid #bbbbbb;">
									<div style="padding-left:2px;">
										<%= getQualifierLine(theChip, permissionValue, disabled) %>
									</div>
								</td>
							</tr>
							<%
								}
							%>
						</table>


						<%-- END CONTENT --%>
					</td>
				</tr>
				<tr>
					<td>
						<%@include file="../../emptyFooter.inc" %>
					</td>
				</tr>
				<%
					}
				%>
			</table>

		</td>
	</tr>
	<tr>
		<td class="spacer" colspan="4">
			<div>&nbsp;</div>
		</td>
	</tr>
</table>
