/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.web;

import de.hybris.bootstrap.annotations.UnitTest;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;

import com.google.common.collect.Sets;
import com.neoworks.hybris.feature.SessionFeatureService;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Rick Hobbs (rick@neoworks.com)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FeatureServiceRequestParamFilterTest
{
	@InjectMocks
	private final FeatureServiceRequestParamFilter featureServiceRequestParamFilter = new FeatureServiceRequestParamFilter();

	@Mock
	private SessionFeatureService sessionFeatureService;

	@Mock
	private FilterChain filterChain;

	@Before
	public void setUp()
	{
	}

	@Test
	public void test_FilterNoRestrictionAndEnabled() throws Exception
	{
		when(sessionFeatureService.isSessionFeaturesEnabled()).thenReturn(true);
		featureServiceRequestParamFilter.setRestrictedFeaturesToSetSpecified(false);

		final ServletRequest request = buildRequestWithParam(null, "feature.foo.bar", "true");
		featureServiceRequestParamFilter.doFilter(request, null, filterChain);

		verify(sessionFeatureService, times(1)).isSessionFeaturesEnabled();
		verify(sessionFeatureService, times(1)).setSessionFeatureState(eq("feature.foo.bar"), eq(Boolean.TRUE));
	}

	@Test
	public void test_FilterNoRestrictionAndEnabled_BadBoolean() throws Exception
	{
		when(sessionFeatureService.isSessionFeaturesEnabled()).thenReturn(true);
		featureServiceRequestParamFilter.setRestrictedFeaturesToSetSpecified(false);

		final ServletRequest request = buildRequestWithParam(null, "feature.foo.bar", "tru");
		featureServiceRequestParamFilter.doFilter(request, null, filterChain);

		verify(sessionFeatureService, times(1)).isSessionFeaturesEnabled();
		verify(sessionFeatureService, times(1)).setSessionFeatureState(eq("feature.foo.bar"), eq(null));
	}

	@Test
	public void test_FilterNoRestrictionAndNotEnabled() throws Exception
	{
		when(sessionFeatureService.isSessionFeaturesEnabled()).thenReturn(false);
		featureServiceRequestParamFilter.setRestrictedFeaturesToSetSpecified(false);

		final ServletRequest request = buildRequestWithParam(null, "feature.foo.bar", "true");
		featureServiceRequestParamFilter.doFilter(request, null, filterChain);

		verify(sessionFeatureService, times(1)).isSessionFeaturesEnabled();
		verify(sessionFeatureService, never()).setSessionFeatureState(any(), any());
	}

	@Test
	public void test_FilterWithRestrictionButFailsAndEnabled() throws Exception
	{
		when(sessionFeatureService.isSessionFeaturesEnabled()).thenReturn(true);
		featureServiceRequestParamFilter.setRestrictedFeaturesToSetSpecified(true);

		featureServiceRequestParamFilter.setPermittedFeatures(Sets.newHashSet("feature.foo.NOTbar"));

		final ServletRequest request = buildRequestWithParam(null, "feature.foo.bar", "true");
		featureServiceRequestParamFilter.doFilter(request, null, filterChain);

		verify(sessionFeatureService, times(1)).isSessionFeaturesEnabled();
		verify(sessionFeatureService, never()).setSessionFeatureState(any(), any());
	}

	@Test
	public void test_FilterRestrictionButAllowedAndEnabled() throws Exception
	{
		when(sessionFeatureService.isSessionFeaturesEnabled()).thenReturn(true);
		featureServiceRequestParamFilter.setRestrictedFeaturesToSetSpecified(true);

		featureServiceRequestParamFilter.setPermittedFeatures(Sets.newHashSet("feature.foo.bar"));

		final ServletRequest request = buildRequestWithParam(null, "feature.foo.bar", "true");
		featureServiceRequestParamFilter.doFilter(request, null, filterChain);

		verify(sessionFeatureService, times(1)).isSessionFeaturesEnabled();
		verify(sessionFeatureService, times(1)).setSessionFeatureState(eq("feature.foo.bar"), eq(Boolean.TRUE));
	}

	@Test
	public void test_FilterRestrictionButAllowedMultipleAndEnabled() throws Exception
	{
		when(sessionFeatureService.isSessionFeaturesEnabled()).thenReturn(true);
		featureServiceRequestParamFilter.setRestrictedFeaturesToSetSpecified(true);

		featureServiceRequestParamFilter.setPermittedFeatures(Sets.newHashSet("feature.foo.bar", "feature.foo.baz"));

		final MockHttpServletRequest request = buildRequestWithParam(null, "feature.foo.bar", "true");
		buildRequestWithParam(request, "feature.foo.baz", "false");
		buildRequestWithParam(request, "feature.foo.NOT", "false");

		featureServiceRequestParamFilter.doFilter(request, null, filterChain);

		verify(sessionFeatureService, times(1)).isSessionFeaturesEnabled();
		verify(sessionFeatureService, times(1)).setSessionFeatureState(eq("feature.foo.bar"), eq(Boolean.TRUE));
		verify(sessionFeatureService, times(1)).setSessionFeatureState(eq("feature.foo.baz"), eq(Boolean.FALSE));
		verify(sessionFeatureService, never()).setSessionFeatureState(eq("feature.foo.NOT"), any());
	}

	protected MockHttpServletRequest buildRequestWithParam(final MockHttpServletRequest request, final String paramName, final String paramValue)
	{
		final MockHttpServletRequest responseRequest;
		if (request != null)
		{
			responseRequest = request;
		}
		else
		{
			responseRequest = new MockHttpServletRequest();
		}

		responseRequest.setParameter(paramName, paramValue);

		return responseRequest;
	}
}
