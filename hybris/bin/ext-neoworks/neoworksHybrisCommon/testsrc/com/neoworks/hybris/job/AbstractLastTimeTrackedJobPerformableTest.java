/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.job;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.neoworks.hybris.model.cronjob.LastTimeTrackedCronJobModel;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractLastTimeTrackedJobPerformableTest
{
	@Spy
	@InjectMocks
	private final AbstractLastTimeTrackedJobPerformableImpl abstractLastTimeTrackedJobPerformable = new AbstractLastTimeTrackedJobPerformableImpl();

	@Mock
	private TimeService timeService;
	@Mock
	private ModelService modelService;
	@Mock
	private SessionService sessionService;
	@Mock
	private FlexibleSearchService flexibleSearchService;

	private LastTimeTrackedCronJobModel lastTimeTrackedCronJobModel;
	private Date lastRunTime;
	private Date currentTime;

	@Before
	public void setUp()
	{
		final Calendar calendar = Calendar.getInstance();
		currentTime = calendar.getTime();
		when(timeService.getCurrentTime()).thenReturn(currentTime);
		calendar.add(Calendar.DATE,-1);
		lastRunTime = calendar.getTime();
		lastTimeTrackedCronJobModel = new LastTimeTrackedCronJobModel();
		lastTimeTrackedCronJobModel.setLastRunDate(lastRunTime);
	}

	@Test
	public void test_JobFailure()
	{
		when(abstractLastTimeTrackedJobPerformable.performInternal(lastTimeTrackedCronJobModel)).thenReturn(new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED));

		final PerformResult result = abstractLastTimeTrackedJobPerformable.perform(lastTimeTrackedCronJobModel);
		assertEquals("Incorrect result returned by job", CronJobResult.FAILURE, result.getResult());
		assertEquals("Incorrect status returned by job", CronJobStatus.FINISHED, result.getStatus());
		assertEquals("Incorrect last run time",lastRunTime,lastTimeTrackedCronJobModel.getLastRunDate());
		verify(modelService, never()).save(lastTimeTrackedCronJobModel);
	}

	@Test
	public void test_JobSuccess()
	{
		final ArgumentCaptor<LastTimeTrackedCronJobModel> cronJobCapture = ArgumentCaptor.forClass(LastTimeTrackedCronJobModel.class);
		when(abstractLastTimeTrackedJobPerformable.performInternal(lastTimeTrackedCronJobModel)).thenReturn(new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED));

		final PerformResult result = abstractLastTimeTrackedJobPerformable.perform(lastTimeTrackedCronJobModel);
		assertEquals("Incorrect result returned by job", CronJobResult.SUCCESS, result.getResult());
		assertEquals("Incorrect status returned by job",CronJobStatus.FINISHED,result.getStatus());
		assertEquals("Job has incorrect last run time",currentTime,lastTimeTrackedCronJobModel.getLastRunDate());
		verify(modelService).save(cronJobCapture.capture());
		assertEquals("Incorrect job saved",lastTimeTrackedCronJobModel, cronJobCapture.getValue());
		assertEquals("Incorrect last run time saved in the model service",currentTime,cronJobCapture.getValue().getLastRunDate());
	}

	@Test(expected=IllegalArgumentException.class)
	public void test_NullJob()
	{
		when(abstractLastTimeTrackedJobPerformable.performInternal(lastTimeTrackedCronJobModel)).thenReturn(new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED));
		abstractLastTimeTrackedJobPerformable.perform(null);
	}

	/**
	 * A dummy implementation of the abstract class, so that we can mock out its abstract method when we make it a spy
	 */
	public static class AbstractLastTimeTrackedJobPerformableImpl extends AbstractLastTimeTrackedJobPerformable<LastTimeTrackedCronJobModel>
	{
		@Override
		protected PerformResult performInternal(final LastTimeTrackedCronJobModel cronJob)
		{
			return null;
		}
	}
}
