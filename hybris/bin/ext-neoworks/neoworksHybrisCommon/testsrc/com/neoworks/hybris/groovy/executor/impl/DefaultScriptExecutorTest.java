/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.groovy.executor.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import com.neoworks.hybris.groovy.data.ScriptResultsData;
import com.neoworks.hybris.groovy.shell.ScriptingShell;

import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * @author Thomas O'Hagan (thomas.ohagan@neoworks.com)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultScriptExecutorTest
{
	@InjectMocks
	private final DefaultScriptExecutor scriptExecutor = new DefaultScriptExecutor();

	@Mock
	private ScriptingShell scriptingShell;
	@Captor
	private ArgumentCaptor<Map<String,Object>> mapArgumentCaptor;

	private static final String TEST_SCRIPT = "testScript";
	private static final String SCRIPT_RESULT = "scriptResult";

	@Test
	public void test_ExecuteEmptyScript()
	{
		final ScriptResultsData result = scriptExecutor.executeScript("      ", true);
		verify(scriptingShell, never()).runScript(anyString(), Matchers.<Map<String,Object>>any());
		assertNotNull("Result cannot be null", result);
		assertNull("Output must be null", result.getOutput());
		assertNull("Script result must be null",result.getResult());
		assertNull("Error message must be null",result.getErrorMessage());
		assertFalse("Error flag must be false", result.isError());
	}

	@Test
	public void test_SuccessfulExecution()
	{
		when(scriptingShell.runScript(eq(TEST_SCRIPT),Matchers.<Map<String,Object>>any())).thenReturn(SCRIPT_RESULT);
		final ScriptResultsData result = scriptExecutor.executeScript(TEST_SCRIPT,true);
		verify(scriptingShell).runScript(eq(TEST_SCRIPT), mapArgumentCaptor.capture());
		final Map<String,Object> context = mapArgumentCaptor.getValue();
		assertTrue("Incorrect context size",context.size() == 2);
		assertTrue("Incorrect context contents",context.get("out") instanceof PrintStream);
		assertTrue("Incorrect context contents",context.get("ctx") instanceof ApplicationContext);
		assertNotNull("Result cannot be null", result);
		assertFalse("Error flag must be false", result.isError());
		assertNull("Error message must be null", result.getErrorMessage());
		assertTrue("Output must be blank",StringUtils.isBlank(result.getOutput()));
		assertNotNull("Script result must not be null",result.getResult());
		assertEquals("Incorrect script result",SCRIPT_RESULT,result.getResult());
}
	
	@Test
	public void test_ExecutionWithError()
	{
		final RuntimeException ex = new RuntimeException("message");
		when(scriptingShell.runScript(eq(TEST_SCRIPT), Matchers.<Map<String, Object>>any())).thenThrow(ex);
		final ScriptResultsData result = scriptExecutor.executeScript(TEST_SCRIPT,false);
		verify(scriptingShell).runScript(eq(TEST_SCRIPT),Matchers.<Map<String,Object>>any());
		assertNotNull("Result cannot be null", result);
		assertTrue("Error flag must be true", result.isError());
		assertNotNull("Error message must not be null", result.getErrorMessage());
		assertEquals("Incorrect error message","message",result.getErrorMessage());
		assertTrue("Output must be blank",StringUtils.isBlank(result.getOutput()));
		assertNull("Script result must be null", result.getResult());
	}

	@Test
	public void test_ExecuteWithAdditionalContextParameters()
	{
		when(scriptingShell.runScript(eq(TEST_SCRIPT),Matchers.<Map<String,Object>>any())).thenReturn(SCRIPT_RESULT);
		final Map<String,Object> additionalContext = new HashMap<>();
		additionalContext.put("test", Integer.valueOf(100));
		final ScriptResultsData result = scriptExecutor.executeScript(TEST_SCRIPT,false,additionalContext);
		verify(scriptingShell).runScript(eq(TEST_SCRIPT), mapArgumentCaptor.capture());
		final Map<String,Object> context = mapArgumentCaptor.getValue();
		assertTrue("Incorrect context size",context.size() == 3);
		assertTrue("Incorrect context contents",context.get("out") instanceof PrintStream);
		assertTrue("Incorrect context contents",context.get("ctx") instanceof ApplicationContext);
		assertTrue("Incorrect context contents",context.get("test") instanceof Integer);
		assertEquals("Incorrect context contents", context.get("test"), 100);
		assertNotNull("Result cannot be null", result);
		assertFalse("Error flag must be false", result.isError());
		assertNull("Error message must be null", result.getErrorMessage());
		assertTrue("Output must be blank",StringUtils.isBlank(result.getOutput()));
		assertNotNull("Script result must not be null",result.getResult());
		assertEquals("Incorrect script result",SCRIPT_RESULT,result.getResult());
	}

}
