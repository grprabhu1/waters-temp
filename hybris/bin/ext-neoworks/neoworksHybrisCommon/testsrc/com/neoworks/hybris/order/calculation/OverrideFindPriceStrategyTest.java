/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.order.calculation;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.FindPriceStrategy;
import de.hybris.platform.util.PriceValue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import junit.framework.Assert;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * @author Nicko Cadell
 */
@UnitTest
public class OverrideFindPriceStrategyTest
{
	@InjectMocks
	private final OverrideFindPriceStrategy overrideFindPriceStrategy = new OverrideFindPriceStrategy();

	@Mock
	private FindPriceStrategy defaultFindPriceStrategy;
	@Mock
	private OrderModel orderModel;
	@Mock
	private AbstractOrderEntryModel normalOrderEntryModel;
	@Mock
	private AbstractOrderEntryModel overrideOrderEntryModel;
	@Mock
	private CurrencyModel currencyModel;

	private final PriceValue defaultPrice = new PriceValue("GBP", 99.99, false);
	private final BigDecimal overridePrice = new BigDecimal(24.99);

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		overrideFindPriceStrategy.setDefaultFindPriceStrategy(defaultFindPriceStrategy);

		when(currencyModel.getIsocode()).thenReturn("GBP");
		when(currencyModel.getDigits()).thenReturn(2);
		when(orderModel.getNet()).thenReturn(Boolean.FALSE);
		when(orderModel.getCurrency()).thenReturn(currencyModel);
		when(normalOrderEntryModel.getOrder()).thenReturn(orderModel);
		when(overrideOrderEntryModel.getOrder()).thenReturn(orderModel);
		when(defaultFindPriceStrategy.findBasePrice(any(AbstractOrderEntryModel.class))).thenReturn(defaultPrice);
		when(overrideOrderEntryModel.getOverrideBasePrice()).thenReturn(overridePrice);
	}

	@Test
	public void testNormalPrice() throws CalculationException
	{
		final PriceValue basePrice = overrideFindPriceStrategy.findBasePrice(normalOrderEntryModel);
		Assert.assertNotNull(basePrice);
		Assert.assertEquals(defaultPrice, basePrice);
	}

	@Test
	public void testOverridePrice() throws CalculationException
	{
		final PriceValue basePrice = overrideFindPriceStrategy.findBasePrice(overrideOrderEntryModel);
		Assert.assertNotNull(basePrice);
		Assert.assertEquals("GBP", basePrice.getCurrencyIso());
		Assert.assertEquals(24.99, basePrice.getValue());
		Assert.assertEquals(false, basePrice.isNet());
	}
}
