/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.security.encoder;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author Rick Hobbs (rick@neoworks.com)
 */
@RunWith(MockitoJUnitRunner.class)
public class NeoworksSaltedMD5PasswordEncoderTest
{
	private static final Logger LOG = LoggerFactory.getLogger(NeoworksSaltedMD5PasswordEncoderTest.class);

	@InjectMocks
	private final NeoworksSaltedMD5PasswordEncoder passwordEncoder = new NeoworksSaltedMD5PasswordEncoder();

	@Mock
	private UserService userService;

	private UserModel user;

	@Before
	public void setUp()
	{
		final String uid = "uid";
		user = new UserModel();
		user.setUid(uid);

		when(userService.getUserForUID(uid)).thenReturn(user);
	}

	@Test
	public void testGenerateUserSpecificSaltForEmptySalt()
	{
		assertEquals(user.getUid(), passwordEncoder.generateUserSpecificSalt(user.getUid()));
	}

	@Test
	public void testGenerateUserSpecificSaltWhenSaltPresent()
	{
		final String salt = "salt";
		user.setPasswordSalt(salt);

		assertEquals(salt, passwordEncoder.generateUserSpecificSalt(user.getUid()));
	}

	@Test
	public void testGenerateUserIdIsSalt()
	{
		final String salt = "salt";
		user.setPasswordSalt(salt);
		user.setUid(NeoworksSaltedMD5PasswordEncoder.SALT_PREFIX + "fooobarrr");

		assertEquals("fooobarrr", passwordEncoder.generateUserSpecificSalt(user.getUid()));
	}


}
