/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.util;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for DateUtils.parseDuration
 */
@UnitTest
public class DateUtilsTest
{
	@Test(expected = NullPointerException.class)
	public void testNullDuration()
	{
		DateUtils.parseDuration(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyDuration()
	{
		DateUtils.parseDuration("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBlankDuration()
	{
		DateUtils.parseDuration(" ");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidDuration1()
	{
		DateUtils.parseDuration("abc");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidDuration2()
	{
		DateUtils.parseDuration("1234 x");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidDuration3()
	{
		DateUtils.parseDuration("100 ms");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidDuration4()
	{
		DateUtils.parseDuration("abc");
	}

	@Test
	public void testParsingMillis()
	{
		Assert.assertEquals(0L, DateUtils.parseDuration("0"));
		Assert.assertEquals(0L, DateUtils.parseDuration("0000"));
		Assert.assertEquals(0L, DateUtils.parseDuration("000000000"));

		Assert.assertEquals(1L, DateUtils.parseDuration("1"));
		Assert.assertEquals(100L, DateUtils.parseDuration("100"));
		Assert.assertEquals(10000L, DateUtils.parseDuration("10000"));

		Assert.assertEquals(1L, DateUtils.parseDuration("01"));
		Assert.assertEquals(100L, DateUtils.parseDuration("00100"));
		Assert.assertEquals(10000L, DateUtils.parseDuration("00010000"));
	}

	@Test
	public void testParsingDurationsWithSingleUnit()
	{
		Assert.assertEquals(1 * 1000, DateUtils.parseDuration("1s"));
		Assert.assertEquals(15 * 1000, DateUtils.parseDuration("15s"));
		Assert.assertEquals(1 * 60 * 1000, DateUtils.parseDuration("1m"));
		Assert.assertEquals(15 * 60 * 1000, DateUtils.parseDuration("15m"));
		Assert.assertEquals(1 * 60 * 60 * 1000, DateUtils.parseDuration("1h"));
		Assert.assertEquals(15 * 60 * 60 * 1000, DateUtils.parseDuration("15h"));

		Assert.assertEquals(1 * 1000, DateUtils.parseDuration("01s"));
		Assert.assertEquals(15 * 1000, DateUtils.parseDuration("015s"));
		Assert.assertEquals(1 * 60 * 1000, DateUtils.parseDuration("01m"));
		Assert.assertEquals(15 * 60 * 1000, DateUtils.parseDuration("015m"));
		Assert.assertEquals(1 * 60 * 60 * 1000, DateUtils.parseDuration("01h"));
		Assert.assertEquals(15 * 60 * 60 * 1000, DateUtils.parseDuration("015h"));

		Assert.assertEquals(1 * 1000, DateUtils.parseDuration("1 s"));
		Assert.assertEquals(15 * 1000, DateUtils.parseDuration("15 s"));
		Assert.assertEquals(1 * 60 * 1000, DateUtils.parseDuration("1 m"));
		Assert.assertEquals(15 * 60 * 1000, DateUtils.parseDuration("15 m"));
		Assert.assertEquals(1 * 60 * 60 * 1000, DateUtils.parseDuration("1 h"));
		Assert.assertEquals(15 * 60 * 60 * 1000, DateUtils.parseDuration("15 h"));
	}

	@Test
	public void testParsingDurationsWithFractions()
	{
		Assert.assertEquals((long) (1.5 * 1000), DateUtils.parseDuration("1.5s"));
		Assert.assertEquals((long) (1.5 * 60 * 1000), DateUtils.parseDuration("1.5m"));
		Assert.assertEquals((long) (1.5 * 60 * 60 * 1000), DateUtils.parseDuration("1.5h"));
	}

	@Test
	public void testParsingDurationsWithMultipleUnits()
	{
		Assert.assertEquals((1 * 60 * 1000) + (1 * 1000), DateUtils.parseDuration("1m 1s"));
		Assert.assertEquals((1 * 60 * 60 * 1000) + (1 * 60 * 1000) + (1 * 1000), DateUtils.parseDuration("1h 1m 1s"));
		Assert.assertEquals((10 * 60 * 60 * 1000) + (10 * 60 * 1000) + (10 * 1000), DateUtils.parseDuration("10h 10m 10s"));
	}

	@Test
	public void testParsingDurationsWithMultipleUnitsAndFractions()
	{
		Assert.assertEquals((long) (10.5 * 60 * 60 * 1000) + (long) (10.5 * 60 * 1000) + (long) (10.5 * 1000), DateUtils.parseDuration("10.5h 10.5m 10.5s"));
	}
}
