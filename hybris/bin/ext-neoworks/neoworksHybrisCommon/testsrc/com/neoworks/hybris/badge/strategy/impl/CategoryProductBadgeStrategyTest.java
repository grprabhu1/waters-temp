/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.badge.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Lists;
import com.neoworks.hybris.badge.strategy.impl.CategoryProductBadgeStrategy;
import com.neoworks.hybris.model.badge.BadgeCategoryModel;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;


/**
 * @author Thomas O'Hagan (thomas.ohagan@neoworks.com)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CategoryProductBadgeStrategyTest
{
	private final CategoryProductBadgeStrategy categoryProductBadgeStrategy = new CategoryProductBadgeStrategy();

	private ProductModel productModel1;
	private VariantProductModel variantProductModel1;
	private VariantProductModel variantProductModel2;
	private CategoryModel categoryModel1;
	private BadgeCategoryModel badgeCategoryModel1;
	private BadgeCategoryModel badgeCategoryModel2;
	private BaseStoreModel baseStoreModel1;
	private static final String BADGE_NAME_1 = "badgeName1";
	private static final String BADGE_NAME_2 = "badgeName2";

	@Before
	public void setUp()
	{
		productModel1 = new ProductModel();
		variantProductModel1 = new VariantProductModel();
		variantProductModel2 = new VariantProductModel();
		categoryModel1 = new CategoryModel();
		badgeCategoryModel1 = new BadgeCategoryModel();
		badgeCategoryModel2 = new BadgeCategoryModel();
		baseStoreModel1 = new BaseStoreModel();
		badgeCategoryModel1.setCode(BADGE_NAME_1);
		badgeCategoryModel2.setCode(BADGE_NAME_2);
	}

	@Test
	public void test_NoBadgeSuperCategories()
	{
		productModel1.setSupercategories(Lists.newArrayList(categoryModel1));
		final Collection<BadgeCategoryModel> result = categoryProductBadgeStrategy.getBadgesForProduct(null, productModel1);
		assertNotNull("Collection of badge categories must not be null.", result);
		assertTrue("Collection of badge categories must be empty.", result.isEmpty());
	}

	@Test
	public void test_BadgeCategoryHasNullStores()
	{
		badgeCategoryModel1.setStores(null);
		productModel1.setSupercategories(Lists.newArrayList(categoryModel1,badgeCategoryModel1));
		final Collection<BadgeCategoryModel> result = categoryProductBadgeStrategy.getBadgesForProduct(baseStoreModel1, productModel1);
		assertNotNull("Collection of badge categories must not be null.", result);
		assertTrue("Collection of badge categories must be of size 1.", result.size() == 1);
		final Iterator<BadgeCategoryModel> iterator = result.iterator();
		assertTrue("Result contains incorrect badge categories.", iterator.hasNext() && iterator.next().getCode().equals(BADGE_NAME_1));
	}

	@Test
	public void test_BadgeCategoryLinkedToStore()
	{
		final BaseStoreModel baseStoreModel2 = new BaseStoreModel();
		badgeCategoryModel1.setStores(Lists.newArrayList(baseStoreModel2));
		badgeCategoryModel2.setStores(Lists.newArrayList(baseStoreModel1));
		variantProductModel1.setBaseProduct(variantProductModel2);
		variantProductModel2.setBaseProduct(productModel1);
		variantProductModel1.setSupercategories(Lists.newArrayList(categoryModel1));
		variantProductModel2.setSupercategories(Lists.newArrayList(badgeCategoryModel2,categoryModel1,badgeCategoryModel1));

		final Collection<BadgeCategoryModel> result = categoryProductBadgeStrategy.getBadgesForProduct(baseStoreModel1, variantProductModel1);
		assertNotNull("Collection of badge categories must not be null.", result);
		assertTrue("Collection of badge categories must be of size 1.", result.size() == 1);
		final Iterator<BadgeCategoryModel> iterator = result.iterator();
		assertTrue("Result contains incorrect badge categories.", iterator.hasNext() && iterator.next().getCode().equals(BADGE_NAME_2));
	}

	/**
	 * To avoid the odd behaviour that would result from the assert statement
	 * not being in the strategy
	 */
	@Test(expected = IllegalArgumentException.class)
	public void test_NoProduct()
	{
		categoryProductBadgeStrategy.getBadgesForProduct(baseStoreModel1,null);
	}

}
