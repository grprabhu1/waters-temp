/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.job;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import com.neoworks.hybris.model.cronjob.CronJobExclusionEntryModel;
import com.neoworks.hybris.model.cronjob.CronJobExclusionScheduleModel;
import com.neoworks.hybris.model.cronjob.ScheduledCronJobModel;

/**
 * The core of the scheduling code is not testable because we do not want to use the timeService for
 * retrieving the current time.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractScheduledJobPerformableTest
{
	@Spy
	@InjectMocks
	private final AbstractScheduledJobPerformableImpl scheduledJobPerformable = new AbstractScheduledJobPerformableImpl();

	@Mock
	private TimeService timeService;
	@Mock
	private ModelService modelService;
	@Mock
	private SessionService sessionService;
	@Mock
	private FlexibleSearchService flexibleSearchService;

	private ScheduledCronJobModel scheduledCronJobModel;
	private CronJobExclusionScheduleModel cronJobExclusionScheduleModel;

	@Before
	public void setUp()
	{
		scheduledCronJobModel = new ScheduledCronJobModel();
		cronJobExclusionScheduleModel = new CronJobExclusionScheduleModel();
		scheduledCronJobModel.setExclusionSchedule(cronJobExclusionScheduleModel);
	}

	@Test
	public void test_NoExclusionEntries()
	{
		when(scheduledJobPerformable.performInternal(scheduledCronJobModel)).thenReturn(new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED));

		cronJobExclusionScheduleModel.setEntries(Collections.<CronJobExclusionEntryModel>emptyList());
		final PerformResult result = scheduledJobPerformable.perform(scheduledCronJobModel);
		assertEquals("Job must have result FAILURE",CronJobResult.FAILURE,result.getResult());
		assertEquals("Job must have status ABORTED",CronJobStatus.ABORTED,result.getStatus());
		verify(modelService,never()).save(scheduledCronJobModel);
	}

	@Test
	public void test_NoExclusionSchedule()
	{
		when(scheduledJobPerformable.performInternal(scheduledCronJobModel)).thenReturn(new PerformResult(CronJobResult.ERROR, CronJobStatus.UNKNOWN));

		scheduledCronJobModel.setExclusionSchedule(null);
		final PerformResult result = scheduledJobPerformable.perform(scheduledCronJobModel);
		assertEquals("Job must have result ERROR",CronJobResult.ERROR,result.getResult());
		assertEquals("Job must have status UNKNOWN",CronJobStatus.UNKNOWN,result.getStatus());
		verify(modelService,never()).save(scheduledCronJobModel);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_NoJob()
	{
		when(scheduledJobPerformable.performInternal(scheduledCronJobModel)).thenReturn(new PerformResult(CronJobResult.ERROR, CronJobStatus.UNKNOWN));
		scheduledJobPerformable.perform(null);
	}

	/**
	 * A dummy implementation of the abstract class, so that we can mock out its abstract method when we make it a spy
	 */
	public static class AbstractScheduledJobPerformableImpl extends AbstractScheduledJobPerformable<ScheduledCronJobModel>
	{
		@Override
		protected PerformResult performInternal(final ScheduledCronJobModel cronJob)
		{
			return null;
		}
	}

}
