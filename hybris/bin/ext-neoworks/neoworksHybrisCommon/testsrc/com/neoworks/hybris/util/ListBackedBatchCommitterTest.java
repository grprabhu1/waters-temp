/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static junit.framework.Assert.assertTrue;

/**
 * @author Thomas O'Hagan (thomas.ohagan@neoworks.com)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ListBackedBatchCommitterTest
{
	private ListBackedBatchCommitter listBackedBatchCommitter;

	@Mock
	private ModelService modelService;

	private ItemModel item1;
	private ItemModel item2;
	private ItemModel item3;

	@Before
	public void setUp()
	{
		item1 = new ItemModel();
		item2 = new ItemModel();
		item3 = new ItemModel();
	}

	@Test
	public void test_Detach()
	{
		listBackedBatchCommitter = new ListBackedBatchCommitter(3,modelService,true);
		final InOrder inOrder = inOrder(modelService);
		listBackedBatchCommitter.add(item1, item2, item3);
		listBackedBatchCommitter.commit();
		//Mockito cannot verify mutable state. Cannot look at size/contents of set.
		inOrder.verify(modelService).saveAll(anyCollectionOf(Object.class));
		inOrder.verify(modelService, times(3)).detach(any(ItemModel.class));
		verify(modelService).detach(item1);
		verify(modelService).detach(item2);
		verify(modelService).detach(item3);
	}

	@Test
	public void test_Reset()
	{
		listBackedBatchCommitter = new ListBackedBatchCommitter(3,modelService,true);
		listBackedBatchCommitter.add(item1, item2, item3);
		listBackedBatchCommitter.reset();
		listBackedBatchCommitter.commit();
		verify(modelService,never()).saveAll(anyCollectionOf(Object.class));
		verify(modelService,never()).detach(any(ItemModel.class));
	}

	@Test
	public void test_ForceCommit()
	{
		listBackedBatchCommitter = new ListBackedBatchCommitter(3,modelService);
		listBackedBatchCommitter.add(item1, item2);
		listBackedBatchCommitter.commit();
		//Mockito cannot verify mutable state. Cannot look at size/contents of set.
		verify(modelService).saveAll(anyCollectionOf(Object.class));
		verify(modelService, never()).detach(any(ItemModel.class));
	}

	@Test
	public void test_IfReadyCommit_BelowThreshold()
	{
		listBackedBatchCommitter = new ListBackedBatchCommitter(3,modelService);
		listBackedBatchCommitter.add(item1,item2);
		listBackedBatchCommitter.commitIfReady();
		verify(modelService, never()).saveAll(anyCollectionOf(Object.class));
		verify(modelService, never()).detach(any(ItemModel.class));
	}

	@Test
	public void test_IfReadyCommit_MeetThreshold()
	{
		listBackedBatchCommitter = new ListBackedBatchCommitter(2,modelService);
		listBackedBatchCommitter.add(item1,item2);
		listBackedBatchCommitter.commitIfReady();
		//Mockito cannot verify mutable state. Cannot look at size/contents of set.
		verify(modelService).saveAll(anyCollectionOf(Object.class));
		verify(modelService, never()).detach(any(ItemModel.class));
	}
}
