/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.groovy.shell.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import com.neoworks.hybris.groovy.shell.ScriptingShell;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;
import static junit.framework.Assert.assertNotNull;

/**
 * @author Thomas O'Hagan (thomas.ohagan@neoworks.com)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GroovyScriptingShellTest
{
	private ScriptingShell scriptingShell;
	private Map<String,Object> context;
	private ByteArrayOutputStream outputStream;
	private static final String SUCCESS_SCRIPT = "print \"Hello world\"\n return \"RETURN\"";
	private static final String ERROR_SCRIPT = "print \"Hello world\"\n def x\n x.getX()\n return \"RETURN\"";
	private static final String CONTEXT_SCRIPT = "print ctx.getBean(\"dummyBean1\")\n return ctx.getBean(\"dummyBean2\")";

	@Mock
	private ApplicationContext applicationContext;

	@Before
	public void setUp()
   {
		context = new HashMap<>();
	   scriptingShell = new GroovyScriptingShell();
	   outputStream = new ByteArrayOutputStream();
	   context.put("out", new PrintStream(outputStream));
   }

	@Test
	public void test_ScriptExecutionSuccess()
	{
		try
		{
			final Object result = scriptingShell.runScript(SUCCESS_SCRIPT,context);
			assertNotNull("Result must not be null",result);
			assertEquals("Incorrect script result","RETURN", getScriptResult(result));
			assertEquals("Incorrect script output","Hello world",getScriptOutput(outputStream));
		}
		catch (final Exception ex)
		{
			fail("Unexpected exception thrown: " + ex.toString());
		}
	}

	@Test //Encoding exception should not be thrown
	public void test_ScriptExecutionWithError() throws UnsupportedEncodingException
	{
		try
		{
			scriptingShell.runScript(ERROR_SCRIPT, context);
			fail("Expected NullPointerException not thrown.");
		}
		catch (final NullPointerException ex)
		{
			assertEquals("Incorrect script output","Hello world",getScriptOutput(outputStream));
		}
	}

	@Test
	public void test_ContextAccessWithinScript()
	{
		final String beanMessage1 = "mess1";
		final String beanMessage2 = "mess2";

		when(applicationContext.getBean("dummyBean1")).thenReturn(beanMessage1);
		when(applicationContext.getBean("dummyBean2")).thenReturn(beanMessage2);
		context.put("ctx", applicationContext);

		try
		{
			final Object result = scriptingShell.runScript(CONTEXT_SCRIPT,context);
			assertNotNull("Result must not be null",result);
			assertEquals("Incorrect script result",beanMessage2, getScriptResult(result));
			assertEquals("Incorrect script output",beanMessage1,getScriptOutput(outputStream));
		}
		catch (final Exception ex)
		{
			fail("Unexpected exception thrown: " + ex.toString());
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_NullScript()
	{
		scriptingShell.runScript(null,context);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_NullContext()
	{
		scriptingShell.runScript(SUCCESS_SCRIPT,null);
	}

	public String getScriptOutput(final ByteArrayOutputStream outputStream) throws UnsupportedEncodingException
	{
		return outputStream.toString("UTF-8");
	}

	public String getScriptResult(final Object initialResult)
	{
		return String.valueOf(initialResult);
	}

}
