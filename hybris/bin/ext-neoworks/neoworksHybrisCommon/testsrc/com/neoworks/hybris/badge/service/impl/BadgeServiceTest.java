/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.badge.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.neoworks.hybris.badge.service.impl.DefaultBadgeService;
import com.neoworks.hybris.badge.strategy.BadgeCodeLookupStrategy;
import com.neoworks.hybris.badge.strategy.ProductBadgeStrategy;
import com.neoworks.hybris.model.badge.BadgeCategoryModel;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Mockito.when;

import static junit.framework.Assert.assertNotNull;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BadgeServiceTest
{
	@InjectMocks
	private final DefaultBadgeService badgeService = new DefaultBadgeService();

	@Mock//injected
	private BaseStoreService baseStoreService;
	@Mock//injected
	private BadgeCodeLookupStrategy badgeCodeLookupStrategy;

	@Mock
	private ProductBadgeStrategy productBadgeStrategy1;
	@Mock
	private ProductBadgeStrategy productBadgeStrategy2;

	private BaseStoreModel baseStoreModel;
	private BadgeCategoryModel badgeCategoryModel1;
	private BadgeCategoryModel badgeCategoryModel2;
	private BadgeCategoryModel badgeCategoryModel3;
	private ProductModel productModel1;
	private List<String> badgeCodes;
	private static final String CODE_1 = "code1";
	private static final String CODE_2 = "code2";
	private static final String CODE_3 = "code3";

	@Before
	public void setUp() throws Exception
	{
		baseStoreModel = new BaseStoreModel();
		badgeCategoryModel1 = new BadgeCategoryModel();
		badgeCategoryModel2 = new BadgeCategoryModel();
		badgeCategoryModel3 = new BadgeCategoryModel();
		productModel1 = new ProductModel();
		badgeCodes = Lists.newArrayList(CODE_1,CODE_2);
		final List<ProductBadgeStrategy> productBadgeStrategies = Lists.newArrayList(productBadgeStrategy1, productBadgeStrategy2);
		badgeService.setProductBadgeStrategies(productBadgeStrategies);
		when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);
	}

	@Test
	public void testNoBadgesForCodes()
	{
		when(badgeCodeLookupStrategy.getBadgesForCodes(baseStoreModel,badgeCodes)).thenReturn(null);
		final List<BadgeCategoryModel> result = badgeService.getBadgesForCodes(baseStoreModel, badgeCodes);
		assertNotNull("Result must not be null",result);
		assertTrue("Result must be empty.", result.isEmpty());
	}

	@Test
	public void testBadgesForCodesOrdering()
	{
		badgeCategoryModel1.setPriority(Long.valueOf(1));
		badgeCategoryModel2.setPriority(Long.valueOf(2));
		badgeCategoryModel3.setPriority(Long.valueOf(2));
		badgeCategoryModel1.setCode(CODE_3);
		badgeCategoryModel2.setCode(CODE_2);
		badgeCategoryModel3.setCode(CODE_1);

		final Set<BadgeCategoryModel> badges = Sets.newHashSet(badgeCategoryModel3, badgeCategoryModel2, badgeCategoryModel1);
		when(badgeCodeLookupStrategy.getBadgesForCodes(baseStoreModel,badgeCodes)).thenReturn(badges);

		final List<BadgeCategoryModel> result = badgeService.getBadgesForCodes(baseStoreModel, badgeCodes);
		assertNotNull("Result must not be null", result);
		assertTrue("Result must have size 3", result.size() == 3);
		assertTrue("Incorrect badge ordering returned.", result.get(0).getCode().equals(CODE_2));
		assertTrue("Incorrect badge ordering returned.",result.get(1).getCode().equals(CODE_1));
		assertTrue("Incorrect badge ordering returned.",result.get(2).getCode().equals(CODE_3));
	}

	@Test
	public void testBadgesForProduct()
	{
		badgeCategoryModel1.setPriority(Long.valueOf(3));
		badgeCategoryModel2.setPriority(Long.valueOf(1));
		badgeCategoryModel3.setPriority(Long.valueOf(4));
		badgeCategoryModel1.setCode(CODE_3);
		badgeCategoryModel2.setCode(CODE_2);
		badgeCategoryModel3.setCode(CODE_1);

		final Set<BadgeCategoryModel> badges1 = Sets.newHashSet(badgeCategoryModel3);
		final Set<BadgeCategoryModel> badges2 = Sets.newHashSet(badgeCategoryModel1, badgeCategoryModel2);
		when(productBadgeStrategy1.getBadgesForProduct(baseStoreModel, productModel1)).thenReturn(badges1);
		when(productBadgeStrategy2.getBadgesForProduct(baseStoreModel, productModel1)).thenReturn(badges2);

		final List<BadgeCategoryModel> result = badgeService.getBadgesForProduct(productModel1);
		assertNotNull("Result must not be null", result);
		assertTrue("Result must have size 3", result.size() == 3);
		assertTrue("Incorrect badge ordering returned" , result.get(0).getCode().equals(CODE_1));
		assertTrue("Incorrect badge ordering returned",result.get(1).getCode().equals(CODE_3));
		assertTrue("Incorrect badge ordering returned" ,result.get(2).getCode().equals(CODE_2));
	}

	@Test
	public void testNullProductBadgeStrategies()
	{
		badgeService.setProductBadgeStrategies(null);
		final List<BadgeCategoryModel> result = badgeService.getBadgesForProduct(productModel1);
		assertNotNull("Result must not be null",result);
		assertTrue("Result must be empty.", result.isEmpty());
	}

}
