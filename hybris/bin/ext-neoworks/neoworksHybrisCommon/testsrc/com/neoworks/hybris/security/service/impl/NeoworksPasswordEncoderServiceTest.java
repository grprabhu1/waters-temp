/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.security.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.persistence.security.PasswordEncoder;
import de.hybris.platform.persistence.security.PasswordEncoderFactory;
import de.hybris.platform.servicelayer.user.exceptions.PasswordEncoderNotFoundException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;

import com.neoworks.hybris.security.encoder.NeoworksSaltedMD5PasswordEncoder;

/**
 * @author Thomas O'Hagan (thomas.ohagan@neoworks.com)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class NeoworksPasswordEncoderServiceTest
{
	@InjectMocks
	private final NeoworksPasswordEncoderServiceImpl neoworksPasswordEncoderService = new NeoworksPasswordEncoderServiceImpl();

	@Mock
	private PasswordEncoderFactory encoderFactory;
	@Mock
	private NeoworksSaltedMD5PasswordEncoder neoworksSaltedMD5PasswordEncoder;
	@Mock
	private PasswordEncoder passwordEncoder;

	private UserModel userModel;
	private static final String SALT = "salt";
	private static final String PASSWORD = "hunter2";
	private static final String USER_UID = "uid";
	private static final String UNSALTED_ENCODING = "unsaltedEncoding";

	@Before
	public void setUp()
	{
		userModel = new UserModel();
		userModel.setUid(USER_UID);
	}

	@Test
	public void testUnsaltedEncoding()
	{
		final String UNSALTED = "unsalted";
		when(encoderFactory.getEncoder(UNSALTED_ENCODING)).thenReturn(passwordEncoder);
		when(passwordEncoder.encode(USER_UID,PASSWORD)).thenReturn(UNSALTED);

		final String result = neoworksPasswordEncoderService.encode(userModel,PASSWORD,UNSALTED_ENCODING,SALT);
		assertTrue("Encode returned incorrect result",result.equals(UNSALTED));
	}

	@Test
	public void testWithNeoworksSaltedPasswordEncoder()
	{
		final String SALTED_ENCODING = "saltedEncoding";
		final String SALTED = "salted";
		when(encoderFactory.getEncoder(SALTED_ENCODING)).thenReturn(neoworksSaltedMD5PasswordEncoder);
		when(neoworksSaltedMD5PasswordEncoder.encode(NeoworksSaltedMD5PasswordEncoder.SALT_PREFIX+SALT,PASSWORD)).thenReturn(SALTED);

		final String result = neoworksPasswordEncoderService.encode(userModel,PASSWORD,SALTED_ENCODING,SALT);
		assertTrue("Encode returned incorrect result", result.equals(SALTED));
	}

	@Test(expected = PasswordEncoderNotFoundException.class)
	public void testFailedEncoding()
	{
		when(encoderFactory.getEncoder(UNSALTED_ENCODING)).thenReturn(passwordEncoder);
		when(passwordEncoder.encode(USER_UID,PASSWORD)).thenThrow(new RuntimeException());
		neoworksPasswordEncoderService.encode(userModel, PASSWORD, UNSALTED_ENCODING, SALT);
	}

	@Test(expected = PasswordEncoderNotFoundException.class)
	public void testNoEncoderFactory()
	{
		neoworksPasswordEncoderService.setEncoderFactory(null);
		neoworksPasswordEncoderService.encode(userModel,PASSWORD,UNSALTED_ENCODING,SALT);
	}

}
