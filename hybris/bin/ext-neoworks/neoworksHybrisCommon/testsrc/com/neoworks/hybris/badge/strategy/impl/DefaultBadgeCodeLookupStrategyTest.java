/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.badge.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import com.neoworks.hybris.badge.strategy.impl.DefaultBadgeCodeLookupStrategy;
import com.neoworks.hybris.model.badge.BadgeCategoryModel;

/**
 * @author Thomas O'Hagan (thomas.ohagan@neoworks.com)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultBadgeCodeLookupStrategyTest
{
	@InjectMocks
	private final DefaultBadgeCodeLookupStrategy defaultBadgeCodeLookupStrategy = new DefaultBadgeCodeLookupStrategy();

	@Mock//injected
	private CatalogVersionService catalogVersionService;
	@Mock//injected
	private CategoryService categoryService;

	private BaseStoreModel baseStoreModel;
	private CategoryModel categoryModel1;
	private BadgeCategoryModel badgeCategoryModel1;
	private BadgeCategoryModel badgeCategoryModel2;
	private static final String CODE_1 = "code1";
	private static final String CODE_2 = "code2";

	@Before
	public void setUp()
	{
		baseStoreModel = new BaseStoreModel();
		categoryModel1 = new CategoryModel();
		badgeCategoryModel1 = new BadgeCategoryModel();
		badgeCategoryModel2 = new BadgeCategoryModel();
		final CatalogVersionModel catalogVersionModel = new CatalogVersionModel();
		categoryModel1.setCatalogVersion(catalogVersionModel);
		badgeCategoryModel1.setCatalogVersion(catalogVersionModel);
		badgeCategoryModel2.setCatalogVersion(catalogVersionModel);
		when(catalogVersionService.getSessionCatalogVersions()).thenReturn(Collections.singletonList(catalogVersionModel));
	}

	@Test
	public void test_BadgeCategoriesNotFoundForCodes()
	{
		when(categoryService.getCategoriesForCode(CODE_1)).thenReturn(Collections.singletonList(categoryModel1));
		when(categoryService.getCategoriesForCode(CODE_2)).thenReturn(Collections.<CategoryModel>emptyList());

		final Set<BadgeCategoryModel> result = defaultBadgeCodeLookupStrategy.getBadgesForCodes(baseStoreModel,Lists.newArrayList(CODE_1,CODE_2));
		assertNotNull("Result must not be null", result);
		assertTrue("Result must be empty", result.isEmpty());
	}

	@Test
	public void test_NullBaseStore()
	{
		when(categoryService.getCategoriesForCode(CODE_1)).thenReturn(Lists.newArrayList(badgeCategoryModel1,categoryModel1));
		badgeCategoryModel1.setStores(Lists.<BaseStoreModel>newArrayList());
		badgeCategoryModel1.setCode(CODE_2);

		final Set<BadgeCategoryModel> result = defaultBadgeCodeLookupStrategy.getBadgesForCodes(null,Collections.singletonList(CODE_1));
		assertNotNull("Result must not be null", result);
		assertTrue("Result has must have size 1", result.size() == 1);
		final Iterator<BadgeCategoryModel> iterator = result.iterator();
		assertTrue("Result contains incorrect badge categories.", iterator.hasNext() && iterator.next().getCode().equals(CODE_2));
	}

	@Test
	public void test_BadgeCategoryWithNonMatchingCatalogVersion()
	{
		badgeCategoryModel1.setCatalogVersion(new CatalogVersionModel());
		when(categoryService.getCategoriesForCode(CODE_1)).thenReturn(Lists.<CategoryModel>newArrayList(badgeCategoryModel1));
		badgeCategoryModel1.setStores(Lists.newArrayList(baseStoreModel));

		final Set<BadgeCategoryModel> result = defaultBadgeCodeLookupStrategy.getBadgesForCodes(baseStoreModel,Collections.singletonList(CODE_1));
		assertNotNull("Result must not be null", result);
		assertTrue("Result must be empty", result.isEmpty());
	}

	@Test
	public void test_BadgeCategoryHasNullBaseStore()
	{
		categoryModel1.setCatalogVersion(new CatalogVersionModel());
		when(categoryService.getCategoriesForCode(CODE_1)).thenReturn(Lists.newArrayList(categoryModel1,badgeCategoryModel1));
		badgeCategoryModel1.setStores(null);
		badgeCategoryModel1.setCode(CODE_2);

		final Set<BadgeCategoryModel> result = defaultBadgeCodeLookupStrategy.getBadgesForCodes(baseStoreModel,Collections.singletonList(CODE_1));
		assertNotNull("Result must not be null", result);
		assertTrue("Result must have size 1", result.size() == 1);
		final Iterator<BadgeCategoryModel> iterator = result.iterator();
		assertTrue("Result contains incorrect badge categories.", iterator.hasNext() && iterator.next().getCode().equals(CODE_2));
	}

	@Test
	public void test_BadgeCategoryLinkedToStore()
	{
		when(categoryService.getCategoriesForCode(CODE_1)).thenReturn(Lists.<CategoryModel>newArrayList(badgeCategoryModel1));
		badgeCategoryModel1.setStores(Collections.<BaseStoreModel>emptyList());
		badgeCategoryModel1.setCode(CODE_1);
		when(categoryService.getCategoriesForCode(CODE_2)).thenReturn(Lists.<CategoryModel>newArrayList(badgeCategoryModel2));
		badgeCategoryModel2.setStores(Lists.<BaseStoreModel>newArrayList(baseStoreModel));
		badgeCategoryModel2.setCode(CODE_2);

		final Set<BadgeCategoryModel> result = defaultBadgeCodeLookupStrategy.getBadgesForCodes(baseStoreModel,Lists.newArrayList(CODE_1,CODE_2));
		assertNotNull("Result must not be null", result);
		assertTrue("Result must have size 1", result.size() == 1);
		final Iterator<BadgeCategoryModel> iterator = result.iterator();
		assertTrue("Result contains incorrect badge categories.", iterator.hasNext() && iterator.next().getCode().equals(CODE_2));
	}

	@Test
	public void test_NoBadgeCodesSupplied()
	{
		final Set<BadgeCategoryModel> result = defaultBadgeCodeLookupStrategy.getBadgesForCodes(baseStoreModel, Collections.<String>emptyList());
		assertNull("Result must be null", result);
	}
}
