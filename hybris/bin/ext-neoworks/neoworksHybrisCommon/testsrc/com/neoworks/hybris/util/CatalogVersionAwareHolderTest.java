/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogTypeService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CatalogVersionAwareHolderTest
{
	@InjectMocks
	private final CatalogVersionAwareHolder<ItemModel> catalogVersionAwareHolder = new CatalogVersionAwareHolder<>();

	@Mock
	private CatalogTypeService catalogTypeService;
	@Mock
	private CatalogVersionAwareHolder.Modifier<ItemModel> modifier;

	private ItemModel item1;
	private ItemModel item2;
	private ItemModel item3;
	private ItemModel item4;

	private CatalogVersionModel catalogVersion1;
	private CatalogVersionModel catalogVersion2;

	@Before
	public void setUp()
	{
		item1 = new ItemModel();
		item2 = new ItemModel();
		item3 = new ItemModel();
		item4 = new ItemModel();

		catalogVersion1 = new CatalogVersionModel();
		catalogVersion2 = new CatalogVersionModel();

		when(catalogTypeService.isCatalogVersionAwareModel(item1)).thenReturn(true);
		when(catalogTypeService.isCatalogVersionAwareModel(item2)).thenReturn(true);
		when(catalogTypeService.isCatalogVersionAwareModel(item3)).thenReturn(true);
		when(catalogTypeService.isCatalogVersionAwareModel(item4)).thenReturn(true);

		when(catalogTypeService.getCatalogVersionForCatalogVersionAwareModel(item1)).thenReturn(catalogVersion1);
		when(catalogTypeService.getCatalogVersionForCatalogVersionAwareModel(item2)).thenReturn(catalogVersion2);
		when(catalogTypeService.getCatalogVersionForCatalogVersionAwareModel(item3)).thenReturn(catalogVersion1);
		when(catalogTypeService.getCatalogVersionForCatalogVersionAwareModel(item4)).thenReturn(catalogVersion2);
	}

	@Test
	public void test_ModifyAll()
	{
		final Calendar calendar = Calendar.getInstance();
		item1.setModifiedtime(calendar.getTime());
		calendar.add(Calendar.DATE, 1);
		item2.setModifiedtime(calendar.getTime());
		calendar.add(Calendar.DATE, 1);
		item3.setModifiedtime(calendar.getTime());
		calendar.add(Calendar.DATE, 1);
		item4.setModifiedtime(calendar.getTime());

		final InOrder inOrder = inOrder(modifier);

		catalogVersionAwareHolder.addAll(Lists.newArrayList(item4,item2,item1,item3));
		catalogVersionAwareHolder.modifyAll(modifier);

		inOrder.verify(modifier).modify(item1,catalogVersion1);
		inOrder.verify(modifier).modify(item2,catalogVersion2);
		inOrder.verify(modifier).modify(item3,catalogVersion1);
		inOrder.verify(modifier).modify(item4,catalogVersion2);
	}

	@Test
	public void test_Modify()
	{
		catalogVersionAwareHolder.addAll(Lists.newArrayList(item4,item2,item1,item3));
		catalogVersionAwareHolder.modify(catalogVersion2, modifier);
		verify(modifier).modify(item2, catalogVersion2);
		verify(modifier).modify(item4,catalogVersion2);
		catalogVersionAwareHolder.modify(catalogVersion1,modifier);
		verify(modifier).modify(item1,catalogVersion1);
		verify(modifier).modify(item3,catalogVersion1);
	}

	@Test
	public void test_Empty()
	{
		assertTrue("catalogVersionAwareHolder must be empty",catalogVersionAwareHolder.isEmpty());
		when(catalogTypeService.isCatalogVersionAwareModel(item3)).thenReturn(false);
		when(catalogTypeService.isCatalogVersionAwareModel(item4)).thenReturn(false);
		catalogVersionAwareHolder.addAll(Lists.newArrayList(item4,item2,item1,item3));
		assertTrue("catalogVersionAwareHolder must not be empty",catalogVersionAwareHolder.isNotEmpty());
		assertTrue("catalogVersionAwareHolder has incorrect size",catalogVersionAwareHolder.getSize() == 2L);
	}


}
