/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.impersonation.factory;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.neoworks.hybris.impersonation.factory.ImpersonationContextFactory;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ImpersonationContextFactoryTest
{
	@InjectMocks
	private final ImpersonationContextFactory contextFactory = new ImpersonationContextFactory();

	@Mock //injected
	private CatalogVersionService catalogVersionService;
	@Mock //injected
	private UserService userService;
	@Mock //injected
	private BaseSiteService baseSiteService;
	@Mock //injected
	private CommonI18NService i18nService;

	@Test
	public void test_FillWholeContext()
	{
		final String USER_ID = "user1";
		final String CURRENCY_ID = "EUR";
		final String BASE_SITE_ID = "fr";
		final String LANGUAGE_ID = "fr";
		final String CATALOG_NAME = "globalProductCatalog";
		final String ONLINE_NAME = "online";
		final String STAGED_NAME = "staged";
		final String PRODUCT_STAGED = CATALOG_NAME + "-" + STAGED_NAME;
		final String PRODUCT_ONLINE = CATALOG_NAME + "-" + ONLINE_NAME;

		final LanguageModel languageModel = new LanguageModel();
		final UserModel userModel = new UserModel();
		final BaseSiteModel baseSiteModel = new BaseSiteModel();
		final CurrencyModel currencyModel = new CurrencyModel();
		final CatalogVersionModel catalogVersionStaged = new CatalogVersionModel();
		final CatalogVersionModel catalogVersionOnline = new CatalogVersionModel();

		when(userService.getUserForUID(USER_ID)).thenReturn(userModel);
		when(i18nService.getLanguage(LANGUAGE_ID)).thenReturn(languageModel);
		when(baseSiteService.getBaseSiteForUID(BASE_SITE_ID)).thenReturn(baseSiteModel);
		when(i18nService.getCurrency(CURRENCY_ID)).thenReturn(currencyModel);
		when(catalogVersionService.getCatalogVersion(CATALOG_NAME,STAGED_NAME)).thenReturn(catalogVersionStaged);
		when(catalogVersionService.getCatalogVersion(CATALOG_NAME,ONLINE_NAME)).thenReturn(catalogVersionOnline);

		contextFactory.setSite(BASE_SITE_ID);
		contextFactory.setCurrency(CURRENCY_ID);
		contextFactory.setLanguage(LANGUAGE_ID);
		contextFactory.setUserId(USER_ID);
		contextFactory.setCatalogVersions(new String[]{PRODUCT_ONLINE,PRODUCT_STAGED});

		final ImpersonationContext result = contextFactory.createInstance();

		assertNotNull("Result must have non-null catalog versions",result.getCatalogVersions());
		assertTrue("Result's catalog versions must have size 2",result.getCatalogVersions().size() == 2);
		assertTrue("Result contains incorrect catalog versions",result.getCatalogVersions().contains(catalogVersionOnline));
		assertTrue("Result contains incorrect catalog versions",result.getCatalogVersions().contains(catalogVersionStaged));
		assertEquals("Result does not contain correct currency",result.getCurrency(),currencyModel);
		assertEquals("Result does not contain correct language",result.getLanguage(),languageModel);
		assertEquals("Result does not contain correct site",result.getSite(),baseSiteModel);
		assertEquals("Result does not contain correct user",result.getUser(),userModel);
	}

	@Test(expected = RuntimeException.class)
	public void test_BadlyFormattedCatalogVersion()
	{
		contextFactory.setCatalogVersions(new String[]{"globalProductCatalog-staged-1"});
		contextFactory.createInstance();
	}

	@Test
	public void test_FillNoneOfContext()
	{
		final ImpersonationContext result = contextFactory.createInstance();
		assertNull("Result must have null catalog versions", result.getCatalogVersions());
		assertNull("Result must have null currency",result.getCurrency());
		assertNull("Result must have null language",result.getLanguage());
		assertNull("Result must have null site",result.getSite());
		assertNull("Result must have null user",result.getUser());
	}

}
