/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.etl.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.type.TypeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.neoworks.hybris.etl.impl.DefaultEtlCronJobService;
import com.neoworks.hybris.etl.job.EtlJobContext;
import com.neoworks.hybris.etl.model.EtlCronJobModel;
import com.neoworks.hybris.etl.model.EtlCronJobParamModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Michael Allen (michael@neoworks.com)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultEtlCronJobServiceTest
{
	private final DefaultEtlCronJobService defaultEtlCronJobService = new DefaultEtlCronJobService();

	@Mock
	private ModelService modelService;
	@Mock
	private TypeService typeService;
	@Mock
	private EtlJobContext etlJobContext;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		defaultEtlCronJobService.setModelService(modelService);
		defaultEtlCronJobService.setTypeService(typeService);

		when(etlJobContext.isAborted()).thenReturn(false);
	}

	@Test
	public void testSetEtlJobStringParameter() throws Exception
	{
		final EtlCronJobModel etlCronJobModel = new EtlCronJobModel();
		final EtlCronJobParamModel param1 = new EtlCronJobParamModel();
		param1.setKey("testParam1");
		param1.setEtlCronJob(etlCronJobModel);
		etlCronJobModel.setEtlCronJobParams(Collections.singleton(param1));
		when(etlJobContext.getEtlCronJob()).thenReturn(etlCronJobModel);


		// When we have an existing param that needs updating.
		defaultEtlCronJobService.setEtlJobParameter(etlJobContext, "testParam1", "testParamValue-1");
		verify(modelService, times(1)).save(any(EtlCronJobParamModel.class));
		verify(modelService, times(0)).create(EtlCronJobParamModel._TYPECODE);
		assertEquals(param1.getKey(),"testParam1");
		assertEquals(param1.getValue(),"testParamValue-1");


		// When we the param that cannot be found and needs to be created.
		final EtlCronJobParamModel param2 = new EtlCronJobParamModel();
		when(modelService.create(EtlCronJobParamModel.class)).thenReturn(param2);
		final TypeModel typeModel =  new TypeModel();
		typeModel.setCode("java.lang.String");
		when(typeService.getTypeForCode(any(String.class))).thenReturn(typeModel);

		defaultEtlCronJobService.setEtlJobParameter(etlJobContext, "testParam2", "testParamValue-2");
		verify(modelService, times(1)).create(EtlCronJobParamModel.class);
		verify(modelService, times(1)).saveAll(any(EtlCronJobParamModel.class), any(EtlCronJobModel.class));
		assertEquals(param2.getKey(),"testParam2");
		assertEquals(param2.getValue(),"testParamValue-2");
		assertEquals(param2.getType().getCode(), ("java.lang.String"));
		assertEquals(param2.getEtlCronJob(),etlCronJobModel);
		assertNotNull(etlCronJobModel.getEtlCronJobParams());
		assertTrue(etlCronJobModel.getEtlCronJobParams().size() == 2);
	}


	@Test
	public void testSetLongEtlJobParameter() throws Exception
	{
		final EtlCronJobModel etlCronJobModel = new EtlCronJobModel();
		final EtlCronJobParamModel param1 = new EtlCronJobParamModel();
		param1.setKey("testParam1");
		param1.setEtlCronJob(etlCronJobModel);
		etlCronJobModel.setEtlCronJobParams(Collections.singleton(param1));
		when(etlJobContext.getEtlCronJob()).thenReturn(etlCronJobModel);


		// When we have an existing param that needs updating.
		defaultEtlCronJobService.setEtlJobParameter(etlJobContext, "testParam1", new Long(101));
		verify(modelService, times(1)).save(any(EtlCronJobParamModel.class));
		verify(modelService, times(0)).create(EtlCronJobParamModel.class);
		assertEquals(param1.getKey(), "testParam1");
		assertEquals(((Long)param1.getValue()).longValue(),101);


		// When we the param that cannot be found and needs to be created.
		final EtlCronJobParamModel param2 = new EtlCronJobParamModel();
		when(modelService.create(EtlCronJobParamModel.class)).thenReturn(param2);
		final TypeModel typeModel =  new TypeModel();
		typeModel.setCode("java.lang.Long");
		when(typeService.getTypeForCode(any(String.class))).thenReturn(typeModel);

		defaultEtlCronJobService.setEtlJobParameter(etlJobContext, "testParam2", new Long(102));
		verify(modelService, times(1)).create(EtlCronJobParamModel.class);
		verify(modelService, times(1)).saveAll(any(EtlCronJobParamModel.class), any(EtlCronJobModel.class));
		assertEquals(param2.getKey(),"testParam2");
		assertEquals(((Long)param2.getValue()).longValue(),102);
		assertEquals(param2.getType().getCode(), ("java.lang.Long"));
		assertEquals(param2.getEtlCronJob(),etlCronJobModel);
		assertNotNull(etlCronJobModel.getEtlCronJobParams());
		assertTrue(etlCronJobModel.getEtlCronJobParams().size() == 2);
	}
}
