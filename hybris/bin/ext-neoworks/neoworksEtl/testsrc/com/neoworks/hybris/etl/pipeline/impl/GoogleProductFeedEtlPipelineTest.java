/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.etl.pipeline.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.io.Resource;

import com.neoworks.hybris.etl.consumers.impl.SmooksDataConsumer;
import com.neoworks.hybris.etl.job.EtlJobContext;
import com.neoworks.hybris.etl.producers.impl.FlexibleSearchDataProducer;
import com.neoworks.hybris.etl.transformers.impl.PassthroughTransformer;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Michael Allen (michael@neoworks.com)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GoogleProductFeedEtlPipelineTest
{
	private final DefaultEtlPipeline defaultEtlPipeline = new DefaultEtlPipeline();

	@Mock
	private PassthroughTransformer dataTransformer;
	@Mock
	private EtlJobContext etlJobContext;


	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		defaultEtlPipeline.setAbortCheckInterval(100);
		defaultEtlPipeline.setBeanName("defaultEtlPipelineTestBean");
		defaultEtlPipeline.setLogInterval(1000);
		when(etlJobContext.isAborted()).thenReturn(false);
	}

	@Test
	public void testExecuteOnGoogleProductFeedEtlPipelineBean() throws Exception
	{
		// #### Start DataProducer
		final FlexibleSearchDataProducer dataProducer = new FlexibleSearchDataProducer();
		final FlexibleSearchService flexibleSearchService = mock(FlexibleSearchService.class);
		final ModelService modelService = mock(ModelService.class);
		dataProducer.setFlexibleSearchService(flexibleSearchService);
		dataProducer.setModelService(modelService);
		dataProducer.setQuery("DUMMY: SELECT {PK} FROM {Product}");

		final ProductData productData = new ProductData();
		productData.setCode("1002122");
		productData.setDescription("Some Description");

		final SearchResult searchResult = mock(SearchResult.class);
		final List<PK> results = mock(List.class);
		final Iterator<PK> resultIterator = mock(Iterator.class);
		when(flexibleSearchService.search(any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		when(searchResult.getResult()).thenReturn(results);
		when(results.iterator()).thenReturn(resultIterator);
		when(resultIterator.hasNext()).thenReturn(true).thenReturn(false);
		when(modelService.get(any(PK.class))).thenReturn(new ProductModel());
		// End DataProducer ####

		// #### Start DataConsumer
		final SmooksDataConsumer dataConsumer = new SmooksDataConsumer();
		final Resource smooksConfig = mock(Resource.class);
		final String smooksResultFileName = "productExport.xml";
		final String resultFileParamKey = "GeneratedFilePath";
		dataConsumer.setResultFileParamKey(resultFileParamKey);
		dataConsumer.setSmooksConfig(smooksConfig);
		dataConsumer.setSmooksResultFileName(smooksResultFileName);

		final String smooksConfigXml =
				"<smooks-resource-list xmlns=\"http://www.milyn.org/xsd/smooks-1.1.xsd\" xmlns:csv=\"http://www.milyn.org/xsd/smooks/csv-1.2.xsd\">\n" +
				"  <params>\n" +
				"    <param name=\"stream.filter.type\">SAX</param>\n" +
				"    <param name=\"inputType\">input.java</param>\n" +
				"    <param name=\"input.java\" type=\"input.type.actived\">de.hybris.platform.commercefacades.product.data.ProductData</param>\n" +
				"  </params>\n" +
				"</smooks-resource-list>";
		final InputStream stream = new ByteArrayInputStream(smooksConfigXml.getBytes(StandardCharsets.UTF_8));
		when(smooksConfig.getInputStream()).thenReturn(stream);
		// End DataConsumer ####

		// Data Transformer
		when(dataTransformer.transform(any(Object.class))).thenReturn(productData);

		// Configure the tested class.
		defaultEtlPipeline.setDataConsumer(dataConsumer);
		defaultEtlPipeline.setDataProducer(dataProducer);
		defaultEtlPipeline.setDataTransformer(dataTransformer);

		// Run the tested method.
		defaultEtlPipeline.execute(etlJobContext);

		// Validate Test
		final ProductData genProductData = getProductDataFromXml(smooksResultFileName);
		assertNotNull(genProductData);
		assertEquals(productData.getCode(), genProductData.getCode());
		assertEquals(productData.getDescription(), genProductData.getDescription());
	}

	private ProductData getProductDataFromXml(final String smooksResultFileName)
	{
		final XStream xstream = new XStream(new DomDriver());
		xstream.alias("de.hybris.platform.commercefacades.product.data.ProductData", ProductData.class);
		final StringBuffer contents = new StringBuffer();

		BufferedReader inReader = null;
		try
		{
			inReader = new BufferedReader(new FileReader(new File(FileUtils.getTempDirectory()+ File.separator + smooksResultFileName)));
			String readLine;
			while ((readLine = inReader.readLine()) != null)
			{
				contents.append(readLine);
			}
		} catch (final IOException exception) {

			fail(exception.getMessage());
		}
		finally
		{
			IOUtils.closeQuietly(inReader);
		}

		return (ProductData)xstream.fromXML(contents.toString());
	}
}
