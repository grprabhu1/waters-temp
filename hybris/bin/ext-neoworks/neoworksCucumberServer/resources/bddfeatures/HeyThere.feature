# Steps to use sample
# 1. ensure this feature file is in /resources/bddfeatures within neoworkscucumber
# 2. ensure HeyThereFromGroovy.groovy is in /resources/groovyscripts within neoworkscucumber (specified by neoworksCucumberServer.glue.paths)
# 3. HeyThereFromJava.java is in testsrc/com.neoworks.hybris.stepdefs (specified by neoworksCucumberServer.glue.paths)
# 4. run via http://localhost:9001/neoworksCucumberServer/run?tags=@Groovy
@Groovy @Wip
Feature: Cucumber Server

	Scenario: Mixing step defs between groovy and java
		Given groovy says hi
		And java says hi