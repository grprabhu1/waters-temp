/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package groovysteps

import cucumber.runtime.groovy.GroovyWorld

this.metaClass.mixin(cucumber.api.groovy.EN)
this.metaClass.mixin(cucumber.api.groovy.Hooks)

World {
    new GroovyWorld()
}

Given(~/^groovy says hi$/) { ->
	def sessionId = getProperty("sessionService").getCurrentSession().getSessionId()
	println "Hi from Groovy, your session id is: ${sessionId}"
}
