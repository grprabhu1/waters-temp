/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.stepdefs;

import cucumber.api.java.en.And;
import de.hybris.platform.servicelayer.session.SessionService;
import org.springframework.beans.factory.annotation.Required;

public class HeyFromJava
{
	private SessionService sessionService;

	@And("^java says hi$")
	public void java_says_hi() {
		System.out.println("Hi from java, your session id is: " + getSessionService().getCurrentSession().getSessionId());
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
