/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.audit.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.cluster.ClusterService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.Session;
import de.hybris.platform.servicelayer.session.SessionService;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.core.convert.ConversionService;

import com.google.common.collect.Lists;
import com.neoworks.hybris.audit.data.AuditRecordData;
import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.audit.persistence.Persister;
import com.neoworks.hybris.audit.service.AuditDataPopulator;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultAuditServiceTest
{
	@Mock //injected
	private SessionService sessionService;
	@Mock //injected
	private ClusterService clusterService;
	@Mock //injected
	private Persister persister;
	@Mock //injected
	private ConversionService conversionService;
	@Mock //injected
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;

	@Mock
	private AuditDataPopulator auditDataPopulator;
	@Mock
	private ItemModel relatedItem;
	@Mock
	private Session session;
	@Mock
	private Exception exception;
	@Mock
	private Object returnValue;

	@Spy
	@InjectMocks
	private final DefaultAuditServiceImpl defaultAuditService = new DefaultAuditServiceImpl();

	private Status status;
	private final String message = "Hi {}.";
	private final String param = "there";
	private final String clusterInfo = "I am cluster info.";
	private SystemArea systemArea;

	@Before
	public void setUp()
	{
		defaultAuditService.setAuditDataPopulators(Lists.newArrayList(auditDataPopulator));
		when(sessionService.getCurrentSession()).thenReturn(session);
		when(relatedItem.getPk()).thenReturn(PK.fromLong(1L));
		when(clusterService.getClusterId()).thenReturn(1);
		when(session.getSessionId()).thenReturn("session");
		when(exception.getMessage()).thenReturn("exceptionMessage");
		when(persister.canPersist(any(AuditRecordData.class))).thenReturn(true);
		when(conversionService.convert(returnValue, String.class)).thenReturn("returnValue");
		when(defaultAuditService.createAuditRecordData()).thenAnswer(new Answer<Object>()
		{
			@Override
			public AuditRecordData answer(final InvocationOnMock invocation) throws Throwable
			{
				return new AuditRecordData();
			}
		});
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(anyString(), anyString())).thenReturn(clusterInfo);
	}

	@Test
	public void test_Audit_Persist()
	{
		status = Status.SUCCESS;
		systemArea = SystemArea.GENERAL;
		final ArgumentCaptor<AuditRecordData> recordDataArgumentCaptor = ArgumentCaptor.forClass(AuditRecordData.class);
		final InOrder inOrder = inOrder(auditDataPopulator, persister);

		final String id = defaultAuditService.audit(systemArea, status, exception, 1L, relatedItem, message, param);
		inOrder.verify(auditDataPopulator).populate(any(AuditRecordData.class));
		inOrder.verify(persister).persist(recordDataArgumentCaptor.capture());
		final AuditRecordData record = recordDataArgumentCaptor.getValue();

		assertNotNull("Record must not be null", record);
		assertEquals("Hi there.", record.getMessage());
		assertEquals("Hi {}.", record.getUnformattedMessage());
		assertEquals(status, record.getStatus());
		assertEquals(systemArea, record.getSystemArea());
		assertEquals(Long.valueOf(1), record.getRelatedItem());
		assertEquals(clusterInfo, record.getClusterInfo());
		assertEquals("session", record.getJaloSessionId());
		assertEquals("session", record.getJaloSessionId());
		assertNotNull(record.getEntries());
		assertTrue(record.getEntries().isEmpty());
		assertEquals(Long.valueOf(1), record.getDuration());
		assertEquals(exception, record.getException());
		assertEquals("exceptionMessage", exception.getMessage());
		assertEquals(id, record.getRecordId().toString());
	}

	@Test
	public void test_Audit_NoPersist()
	{
		status = Status.FAILURE;
		systemArea = SystemArea.FULFILMENT;
		when(persister.canPersist(any(AuditRecordData.class))).thenReturn(false);

		defaultAuditService.audit(systemArea, status, exception, 1L, relatedItem, message, param);
		verify(auditDataPopulator).populate(any(AuditRecordData.class));
		verify(persister, never()).persist(any(AuditRecordData.class));
	}

	@Test
	public void test_BeginAudit_EndAudit() throws Exception
	{
		status = Status.SUCCESS;
		systemArea = SystemArea.GENERAL;

		final InOrder inOrder = inOrder(persister, auditDataPopulator);

		final DefaultAuditService.DefaultAuditContext ctx = (DefaultAuditService.DefaultAuditContext) defaultAuditService.beginAudit(systemArea, relatedItem, message, param);
		Thread.sleep(100);
		ctx.endAudit(status, returnValue);

		assertTrue(ctx.getAuditRecord().getDuration().longValue() >= 100);
		assertTrue(ctx.getAuditRecord().getExceptionMessage() == null);
		assertEquals("returnValue", ctx.getAuditRecord().getReturnedValue());
		inOrder.verify(auditDataPopulator).populate(ctx.getAuditRecord());
		inOrder.verify(persister).persist(ctx.getAuditRecord());
	}

	@Test
	public void test_BeginAudit_EndAuditWithError() throws Exception
	{
		final InOrder inOrder = inOrder(persister, auditDataPopulator);

		final DefaultAuditService.DefaultAuditContext ctx = (DefaultAuditService.DefaultAuditContext) defaultAuditService.beginAudit(SystemArea.GENERAL, null, null);
		Thread.sleep(200);
		ctx.endAudit(Status.FAILURE, null, exception);

		assertTrue(ctx.getAuditRecord().getDuration().longValue() >= 200);
		assertEquals("exceptionMessage", ctx.getAuditRecord().getExceptionMessage());
		inOrder.verify(auditDataPopulator).populate(ctx.getAuditRecord());
		inOrder.verify(persister).persist(ctx.getAuditRecord());
	}

	@Test
	public void test_auditPersistThrowsException()
	{
		doThrow(new RuntimeException("The persist failed")).when(persister).persist(any());
		final DefaultAuditService.DefaultAuditContext ctx = (DefaultAuditService.DefaultAuditContext) defaultAuditService.beginAudit(SystemArea.GENERAL, null, null);
		ctx.endAudit(Status.SUCCESS, null);

		// no exception should leak out of here
	}

	public static class DefaultAuditServiceImpl extends DefaultAuditService
	{
		@Override
		protected AuditRecordData createAuditRecordData()
		{
			/**
			 * Dummy implementation for spying, so that we can
			 * get the real behaviour of the class under test
			 */
			return null;
		}
	}

}
