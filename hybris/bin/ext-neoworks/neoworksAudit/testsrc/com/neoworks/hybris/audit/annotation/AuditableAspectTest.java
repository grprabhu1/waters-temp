/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.audit.annotation;

import de.hybris.bootstrap.annotations.UnitTest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.neoworks.hybris.audit.annotation.Auditable;
import com.neoworks.hybris.audit.annotation.AuditableAspect;
import com.neoworks.hybris.audit.enums.Status;
import com.neoworks.hybris.audit.enums.SystemArea;
import com.neoworks.hybris.audit.service.AuditContext;
import com.neoworks.hybris.audit.service.AuditService;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

/**
 * @author Conrad Koppitz (conrad@neoworks.com)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AuditableAspectTest
{
	@InjectMocks
	private final AuditableAspect aspect = new AuditableAspect();

	@Mock
	private AuditService auditService;

	@Mock
	private ProceedingJoinPoint pjp;

	@Mock
	private Auditable auditable;

	@Mock
	private AuditContext context;

	@Before
	public void setUp() throws Exception
	{
		doReturn(SystemArea.CHECKOUT).when(auditable).systemArea();
		when(auditable.message()).thenReturn("Foo!");

		when(auditService.beginAudit(SystemArea.CHECKOUT, null, "Foo!")).thenReturn(context);
	}

	@Test
	public void testAround() throws Throwable
	{
		final InOrder inOrder = inOrder(auditService, pjp, context);

		final Integer goodReturnValue = Integer.valueOf(1);
		when(pjp.proceed()).thenReturn(goodReturnValue);
		aspect.around(pjp, auditable);

		inOrder.verify(auditService, times(1)).beginAudit(SystemArea.CHECKOUT, null, "Foo!");
		inOrder.verify(pjp, times(1)).proceed();
		inOrder.verify(context, times(1)).endAudit(Status.SUCCESS, goodReturnValue);
	}

	@Test
	public void testAroundWithException() throws Throwable
	{
		final InOrder inOrder = inOrder(auditService, pjp, context);

		final Exception badException = new Exception("Bad exception.");
		when(pjp.proceed()).thenThrow(badException);

		// The aspect should rethrow any exceptions thrown by the pjp.

		Exception thrownException = null;

		try
		{
			aspect.around(pjp, auditable);
		}
		catch (final Exception e)
		{
			thrownException = e;
		}

		assertEquals(badException, thrownException);

		inOrder.verify(auditService, times(1)).beginAudit(SystemArea.CHECKOUT, null, "Foo!");
		inOrder.verify(pjp, times(1)).proceed();
		inOrder.verify(context, times(1)).endAudit(Status.FAILURE, null, badException);
	}
}
