/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.wiremockserver.controller.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.convert.converter.Converter;

import com.github.tomakehurst.wiremock.standalone.WireMockServerRunner;
import com.google.common.collect.ImmutableMap;
import com.neoworks.hybris.wiremockserver.configuration.ServerConfiguration;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultServerControllerTest
{
	@InjectMocks
	private final DefaultServerController controller = new DefaultServerController();

	@Mock
	private WireMockServerRunner runner;

	@Mock
	private ServerConfiguration config;

	@Mock
	private Converter<Map<String, String>, List<String>> paramsToCliArgsConverter;

	private final Map<String, String> rawParams = ImmutableMap.<String, String>builder()
			.put("booleanKey1", "true")
			.put("textKey", "textValue")
			.put("booleanKey2", "true")
			.build();

	private final String[] cliParams = {"--booleanKey1", "--textKey", "textValue", "--booleanKey2"};

	@Before
	public void setUp()
	{
		given(config.getParams()).willReturn(rawParams);
		given(paramsToCliArgsConverter.convert(rawParams)).willReturn(Arrays.asList(cliParams));
	}

	@Test
	public void startShouldStartServerWithAllArgsMinusBooleanValues()
	{
		controller.start(config);

		verify(runner).run(cliParams);
	}

	@Test
	public void stopShouldNotStopWhenServerNotRunning()
	{
		controller.stop(config);

		verify(runner, never()).stop();
	}

	@Test
	public void stopShouldStopWhenServerRunning()
	{
		given(runner.isRunning()).willReturn(true);

		controller.stop(config);

		verify(runner).stop();
	}


}
