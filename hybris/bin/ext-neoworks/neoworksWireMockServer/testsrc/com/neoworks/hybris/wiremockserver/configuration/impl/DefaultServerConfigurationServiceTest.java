/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.wiremockserver.configuration.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.util.config.ConfigIntf;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.neoworks.hybris.wiremockserver.configuration.ServerConfiguration;
import com.neoworks.hybris.wiremockserver.constants.NeoworksWireMockServerConstants;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultServerConfigurationServiceTest
{
	@InjectMocks
	private final DefaultServerConfigurationService service = new DefaultServerConfigurationService();

	@Mock
	private ConfigIntf configIntf;

	@Mock
	private Map<String, String> cliArgs;

	@Before
	public void setUp()
	{
		given(configIntf.getBoolean(NeoworksWireMockServerConstants.AUTO_START, false)).willReturn(false);
		given(configIntf.getParametersMatching(NeoworksWireMockServerConstants.CONFIG_PROPERTIES_REGEX, true)).willReturn(cliArgs);
	}

	@Test
	public void shouldReturnConfigWithCorrectValues()
	{
		final ServerConfiguration result = service.getConfig();

		assertThat(result.isServerRequired()).isFalse();
		assertThat(result.getParams()).isEqualTo(cliArgs);
	}

}
