/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.wiremockserver.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.neoworks.hybris.wiremockserver.configuration.ServerConfiguration;
import com.neoworks.hybris.wiremockserver.configuration.ServerConfigurationService;
import com.neoworks.hybris.wiremockserver.controller.ServerController;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultWireMockServerServiceTest
{
	@InjectMocks
	private final DefaultWireMockServerService service = new DefaultWireMockServerService();

	@Mock
	private ServerConfigurationService serverConfigurationService;

	@Mock
	private ServerController serverController;

	@Mock
	private ServerConfiguration config;

	@Before
	public void setUp()
	{
		given(serverConfigurationService.getConfig()).willReturn(config);

		service.afterPropertiesSet();

	}

	@Test
	public void shouldStartServerWhenRequired()
	{
		given(config.isServerRequired()).willReturn(true);

		service.startServer();

		verify(serverController).start(config);
	}

	@Test
	public void shouldNotStartServerWhenNotRequired()
	{
		given(config.isServerRequired()).willReturn(false);

		service.startServer();

		verify(serverController, never()).start(config);
	}

	@Test
	public void shouldStopServerWhenRequired()
	{
		given(config.isServerRequired()).willReturn(true);

		service.stopServer();

		verify(serverController).stop(config);
	}

	@Test
	public void shouldNotStopServerWhenNotRequired()
	{
		given(config.isServerRequired()).willReturn(false);

		service.stopServer();

		verify(serverController, never()).stop(config);
	}

}
