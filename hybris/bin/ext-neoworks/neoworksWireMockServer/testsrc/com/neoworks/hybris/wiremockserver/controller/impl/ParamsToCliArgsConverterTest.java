/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.wiremockserver.controller.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.List;

import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import static org.fest.assertions.Assertions.assertThat;

@UnitTest
public class ParamsToCliArgsConverterTest
{
	private final ParamsToCliArgsConverter converter = new ParamsToCliArgsConverter();

	@Test
	public void shouldConvertArgsFromKeyValueToCliFormat()
	{

		final List<String> result = converter.convert(ImmutableMap.of("key", "value"));

		assertThat(result).containsExactly("--key", "value");
	}

	@Test
	public void shouldConvertBooleanArgsFromKeyValueToCliFormat()
	{

		final List<String> result = converter.convert(ImmutableMap.of("key", "true"));

		assertThat(result).containsExactly("--key");
	}

	@Test
	public void shouldConvertMultipleArgsFromKeyValueToCliFormat()
	{

		final List<String> result = converter.convert(ImmutableMap.of("booleanKey1", "true", "textKey", "textValue", "booleanKey2", "true"));

		assertThat(result).containsExactly("--booleanKey1", "--textKey", "textValue", "--booleanKey2");
	}

}
