/*
 * Copyright (c) 2014-2018 Salmon Limited
 * Copyright (c) 2011-2014 Neoworks Limited
 * All rights reserved.
 */

package com.neoworks.hybris.wiremockserver.configuration.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.fest.assertions.MapAssert;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

@UnitTest
public class DefaultServerConfigurationTest
{
	private final DefaultServerConfiguration configuration = new DefaultServerConfiguration(true, ImmutableMap.of("key", "value"));

	@Test
	public void isServerRequired()
	{
		assertTrue(configuration.isServerRequired());
	}

	@Test
	public void getParams()
	{
		assertThat(configuration.getParams())
				.hasSize(1)
				.includes(MapAssert.entry("key", "value"));
	}

}
