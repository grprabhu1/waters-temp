#
# Dist README
#

Build process:

Unzip Hybris OOTB distribution commerce-suite-6.6.0.0-RELEASE.

Create soft links as follows:

commerce-suite-6.6.0.0-RELEASE/hybris/config -> $THIS_CODE_LOCATION/config
commerce-suite-6.6.0.0-RELEASE/hybris/bin/custom -> $THIS_CODE_LOCATION/hybris/bin/custom
commerce-suite-6.6.0.0-RELEASE/hybris/bin/ext-neoworks -> $THIS_CODE_LOCATION/hybris/bin/ext-neoworks

Build & run from commerce-suite-6.6.0.0-RELEASE/hybris/bin/platform

Steps for update:

1) install clean 1905 platform
2) make links for config folders
3) set db path in local.properties. For example:
  db.url=jdbc:hsqldb:file:/data/hsqldb/mydb;shutdown=true;hsqldb.tx=MVCC;hsqldb.log_size=256
  db.driver=org.hsqldb.jdbcDriver
  db.username=sa
  db.password=
4) run ant clean all
5) run platform and go to HAC
6) in HAC go to Platform -> Update. Unselect Create essential data and Localize types. Unselect all patches at the down of the page, skip only waterspatches - R0003-P001. Also select waterspatches in the list of extensions!!! Press Update.
7) you will receive an error with a red line at the top of the page during the upgrade process. After that stop and rerun the platform.
8) go to backoffice, to System -> Search and Navigation -> Facet Search Configurations. Select and run full indexer with Solr Config for Backoffice.
